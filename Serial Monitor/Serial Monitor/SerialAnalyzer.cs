﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Serial_Monitor
{
    public partial class SerialAnalyzer : Form
    {
        private SerialWindow[] serialWindows;

        Rectangle monitorDim;
        Rectangle windowDim;
        

        public SerialAnalyzer()
        {
            Console.WriteLine("hello");
            InitializeComponent();
            int numports = System.IO.Ports.SerialPort.GetPortNames().Length;
            serialWindows = new SerialWindow[numports];

            for (int i = 0; i < numports; i++)
            {
                var sw = new SerialWindow();
                sw.Parent = this.MonitorContainer;
                serialWindows[i] = sw;
               
            }
            //monitorHeight = this.MonitorContainer.Height;
            //monitorWidth = this.MonitorContainer.Width;
            //Rectangle rect;
            monitorDim = this.MonitorContainer.DisplayRectangle;
            windowDim = this.DisplayRectangle;
            


        }

        private void SerialAnalyzer_ResizeEnd(object sender, EventArgs e)
        {

            MonitorContainer.Width = DisplayRectangle.Width - (windowDim.Width - monitorDim.Width);
            MonitorContainer.Height = DisplayRectangle.Height - (windowDim.Height - 426);
        //    Console.WriteLine("{0} {1} {2}", windowDim.Height - monitorDim.Height,windowDim.Height , monitorDim.Height);
        //    Console.WriteLine("Monitor Height :{0}\nWindow Height:{1}\nMonitor width :{2}\nWindow Width:{3}", MonitorContainer.Height, DisplayRectangle.Height
         //       , MonitorContainer.Width, DisplayRectangle.Width);
            this.MonitorContainer.PerformLayout();
            this.MonitorContainer.Refresh();
        }

        private void SerialAnalyzer_SizeChanged(object sender, EventArgs e)
        {
            SerialAnalyzer_ResizeEnd(sender, e);
        }

        private void SerialAnalyzer_FormClosed(object sender, FormClosedEventArgs e)
        {
            foreach (var serWin in serialWindows)
            {
                serWin.disconnectSerialPort();
            }
        }
    }
}
