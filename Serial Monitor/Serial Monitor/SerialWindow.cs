﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using NLog;

namespace Serial_Monitor
{
    public partial class SerialWindow : UserControl
    {
        NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        public SerialWindow()
        {
            InitializeComponent();
            serial.WriteTimeout = 100;
        }

        private SerialPort serial = new SerialPort();
        private Options op ;
        //private OptionObject opo;
        private void btnOptions_Click(object sender, EventArgs e)
        {
            if (op == null||op.IsDisposed) op = new Options();
            op.optionWindowClosedEvent+=(o)=>{

                if (serial.IsOpen)
                {
                    try
                    {
                        serial.Close();
                        btnConnect.BackgroundImage = Serial_Monitor.Properties.Resources.disconnected_128;
                    }
                    catch (Exception Ex)
                    { Console.WriteLine(Ex.Message); }
                }

                serial.BaudRate = o.baudRate;
                serial.DataBits = o.dataBits;
                serial.PortName = o.portName;
                serial.Parity = o.parity;
                serial.StopBits = o.stopBits;
                lblComm.Text = o.portName;
             //   serial.ReceivedBytesThreshold = 1;
                serial.RtsEnable = true;
                serial.DtrEnable = true;
                

            
            };
           
            op.Show();
        }

      

        private void btnConnect_Click(object sender, EventArgs e)
        {
            if (serial.IsOpen)
            {
                try
                {

                    serial.DataReceived -= serial_DataReceived;
                    serial.Close();
                    btnConnect.BackgroundImage = Serial_Monitor.Properties.Resources.disconnected_128;
                }
                catch (Exception Ex)
                { Console.WriteLine(Ex.Message); }

            }
            else
            {
                try
                {
                    serial.DataReceived += serial_DataReceived;
                  
                    serial.Open();
                    btnConnect.BackgroundImage = Serial_Monitor.Properties.Resources.connected_128;
                   // System.Threading.Thread.Sleep(1000);
                    string ss = serial.ReadExisting();
                    Console.WriteLine(ss);
                }
                catch (Exception Ex)
                { Console.WriteLine(Ex.Message); }
 
            }

        }

        void serial_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
           
                SerialPort sp = (SerialPort)sender;
                

                

                this.BeginInvoke(new EventHandler((oo, eee) =>
                {
                    try
                    {

                        string indata = sp.ReadExisting();
                        txtComm.AppendText(indata);
                    }
                    catch (Exception ex)
                    { Console.WriteLine("Error Occured:{0}", ex.Message); };

                    
                }));


        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            if (serial.IsOpen)
            {
                try
                {
                    serial.DiscardOutBuffer();
                    serial.WriteLine(txtSend.Text);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error while writing to serial:{0}", ex.Message);
                }
            }
        }

        private void txtSend_TextChanged(object sender, EventArgs e)
        {

        }

        public void disconnectSerialPort()
        {
            try
            {
                if (serial.IsOpen)
                    serial.Close();
            }
            catch (Exception Ex)
            {
                logger.ErrorException("Error while closing Port", Ex);
            }
        }





    }
}
