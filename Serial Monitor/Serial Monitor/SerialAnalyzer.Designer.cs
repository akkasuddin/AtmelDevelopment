﻿namespace Serial_Monitor
{
    partial class SerialAnalyzer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MonitorContainer = new System.Windows.Forms.FlowLayoutPanel();
            this.SuspendLayout();
            // 
            // MonitorContainer
            // 
            this.MonitorContainer.AutoScroll = true;
            this.MonitorContainer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.MonitorContainer.Location = new System.Drawing.Point(12, 60);
            this.MonitorContainer.Name = "MonitorContainer";
            this.MonitorContainer.Size = new System.Drawing.Size(542, 426);
            this.MonitorContainer.TabIndex = 0;
            // 
            // SerialAnalyzer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(566, 498);
            this.Controls.Add(this.MonitorContainer);
            this.Name = "SerialAnalyzer";
            this.Text = "Serial Analyzer";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.SerialAnalyzer_FormClosed);
            this.ResizeEnd += new System.EventHandler(this.SerialAnalyzer_ResizeEnd);
            this.SizeChanged += new System.EventHandler(this.SerialAnalyzer_SizeChanged);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel MonitorContainer;

    }
}

