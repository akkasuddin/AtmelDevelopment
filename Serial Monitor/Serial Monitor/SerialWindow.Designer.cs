﻿namespace Serial_Monitor
{
    partial class SerialWindow
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txtComm = new System.Windows.Forms.TextBox();
            this.txtSend = new System.Windows.Forms.TextBox();
            this.btnOptions = new System.Windows.Forms.Button();
            this.btnSend = new System.Windows.Forms.Button();
            this.btnConnect = new System.Windows.Forms.Button();
            this.CommPort = new System.IO.Ports.SerialPort(this.components);
            this.lblComm = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtComm
            // 
            this.txtComm.BackColor = System.Drawing.SystemColors.MenuText;
            this.txtComm.ForeColor = System.Drawing.Color.LimeGreen;
            this.txtComm.Location = new System.Drawing.Point(3, 3);
            this.txtComm.Multiline = true;
            this.txtComm.Name = "txtComm";
            this.txtComm.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtComm.Size = new System.Drawing.Size(328, 276);
            this.txtComm.TabIndex = 0;
            // 
            // txtSend
            // 
            this.txtSend.BackColor = System.Drawing.SystemColors.MenuText;
            this.txtSend.ForeColor = System.Drawing.Color.LimeGreen;
            this.txtSend.Location = new System.Drawing.Point(3, 285);
            this.txtSend.Name = "txtSend";
            this.txtSend.Size = new System.Drawing.Size(156, 20);
            this.txtSend.TabIndex = 1;
            this.txtSend.TextChanged += new System.EventHandler(this.txtSend_TextChanged);
            // 
            // btnOptions
            // 
            this.btnOptions.BackgroundImage = global::Serial_Monitor.Properties.Resources.options_1_512;
            this.btnOptions.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnOptions.Location = new System.Drawing.Point(301, 279);
            this.btnOptions.Name = "btnOptions";
            this.btnOptions.Size = new System.Drawing.Size(30, 28);
            this.btnOptions.TabIndex = 4;
            this.btnOptions.UseVisualStyleBackColor = true;
            this.btnOptions.Click += new System.EventHandler(this.btnOptions_Click);
            // 
            // btnSend
            // 
            this.btnSend.BackgroundImage = global::Serial_Monitor.Properties.Resources.send_email;
            this.btnSend.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnSend.Location = new System.Drawing.Point(219, 279);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(54, 28);
            this.btnSend.TabIndex = 3;
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // btnConnect
            // 
            this.btnConnect.BackgroundImage = global::Serial_Monitor.Properties.Resources.disconnected_128;
            this.btnConnect.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnConnect.Location = new System.Drawing.Point(272, 279);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(29, 28);
            this.btnConnect.TabIndex = 2;
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // lblComm
            // 
            this.lblComm.AutoSize = true;
            this.lblComm.Location = new System.Drawing.Point(166, 286);
            this.lblComm.Name = "lblComm";
            this.lblComm.Size = new System.Drawing.Size(0, 13);
            this.lblComm.TabIndex = 5;
            // 
            // SerialWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblComm);
            this.Controls.Add(this.btnOptions);
            this.Controls.Add(this.btnSend);
            this.Controls.Add(this.btnConnect);
            this.Controls.Add(this.txtSend);
            this.Controls.Add(this.txtComm);
            this.Name = "SerialWindow";
            this.Size = new System.Drawing.Size(336, 310);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtComm;
        private System.Windows.Forms.TextBox txtSend;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.Button btnOptions;
        private System.IO.Ports.SerialPort CommPort;
        private System.Windows.Forms.Label lblComm;
    }
}
