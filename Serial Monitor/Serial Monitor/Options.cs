﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.Collections.ObjectModel;

namespace Serial_Monitor
{
    public delegate void OptionWindowClosed(OptionObject options);
    public partial class Options : Form
    {
        public event OptionWindowClosed optionWindowClosedEvent;
        OptionObject op = new OptionObject();
        ObservableCollection<string> ports = new ObservableCollection<string>();
        public Options()
        {
            InitializeComponent();
            op.baudRate = 115200;
            op.dataBits = 8;
            op.parity = Parity.None;
            op.stopBits = StopBits.One;
            

            cboBaudrate.Text = op.baudRate.ToString();
            cboDataBits.Text = op.dataBits.ToString();
            cboParity.Text = op.parity.ToString();
            cboStopBits.Text = op.stopBits.ToString();
            foreach (var p in SerialPort.GetPortNames())
            { 
                ports.Add(p);
            }
            cboPort.DataSource = ports;
            cboPort.Refresh();
            //cboPort. System.IO.Ports.SerialPort.GetPortNames();
            
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Options_Load(object sender, EventArgs e)
        {
            
        }

        private void cboPort_SelectedIndexChanged(object sender, EventArgs e)
        {
            op.portName = cboPort.Text;
        }

        private void cboBaudrate_SelectedIndexChanged(object sender, EventArgs e)
        {
            op.baudRate = Convert.ToInt32(cboBaudrate.Text);
        }

        private void cboDataBits_SelectedIndexChanged(object sender, EventArgs e)
        {
            op.dataBits = Convert.ToInt32(cboDataBits.Text);
        }

        private void cboParity_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch(cboParity.Text)
            {
                case "Even": op.parity = Parity.Even; break;
                case "Odd": op.parity = Parity.Odd; break;
                case "None": op.parity = Parity.None; break;
                case "Mark": op.parity = Parity.Mark; break;
                case "Space": op.parity = Parity.Space; break;
            }
        }

        private void cboStopBits_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (cboStopBits.Text)
            {
                case "None": op.stopBits = StopBits.None; break;
                case "One": op.stopBits = StopBits.One; break;
                case "OnePointFive": op.stopBits = StopBits.OnePointFive; break;
                case "Two": op.stopBits = StopBits.Two; break;
                
            }

        }

        private void Options_FormClosed(object sender, FormClosedEventArgs e)
        {
            if(optionWindowClosedEvent!=null)
                optionWindowClosedEvent(op);
        }





    }
}
