﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
namespace Serial_Monitor
{
    class Global
    {

    }
    public class OptionObject {
        
        public string portName { get; set;}
        public int baudRate { get; set; }
        public Parity parity { get; set; }
        public int dataBits { get; set; }
        public StopBits stopBits { get; set; }
        public OptionObject()
        {
            
        }
    
    }
}
