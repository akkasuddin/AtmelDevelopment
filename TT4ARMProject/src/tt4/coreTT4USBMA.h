/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm                                            **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc. and Blue Ocean Innovation Ltd.                            **
 **                                                                           **
 **  Copryright(c) 2010. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Blue Ocean Innovation Ltd.                        **
 **  Project#            :  449                                               **
 **  Creation date       :  08/20/2010                                        **
 **  Programmer          :  Michael J. Mariveles                              **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : coreTT4USBMA.h                                                    **
 **                                                                           **
 **  Description :                                                            **
 **    - this is the TT4USB-MA system core routines.                          **
 **                                                                           **
 ******************************************************************************/

#ifndef __CORETT4USBMA_H
#define __CORETT4USBMA_H


//----------------------------------
// Task Processing Status Flags...
// \ mjmariveles...
//----------------------------------

#define  TT4USBMA_CORE_LOOP    1
#define  INFINITE_LOOP         1  

// user task request status flags...
#define REQ_ACTIVE             1
#define REQ_DONE               0
// process task status flags...
#define PROC_BUSY              1
#define PROC_DONE              0

typedef union {
    
    struct {
        UINT8  Req_Marker      : 1; // LSB
        UINT8  Req_LCDSummary  : 1;
        UINT8  Req_StopDevice  : 1;
        UINT8  Req_CfgDevice   : 1;
        UINT8  Proc_Marker     : 1;
        UINT8  Proc_LCDSummary : 1;
        UINT8  Proc_StopDevice : 1;
        UINT8  Proc_CfgDevice  : 1; // MSB        
    }Task;    

    UINT8 Status;
    
} __sDEVICETASK;


void coreHwSystemInit(void);
void coreInit_LCDDisplayAndLED(void);
void coreDeviceStartUpDelayState(void);
void coreDeviceStopMode(void);
void coreClearGlobalVars(void);
void coreInitSleepModeState(void);
void coreDeviceSleepModeState(void);
void coreInitUnconfiguredState(void);
void coreDeviceUnconfiguredState(void);
void coreInitStartTT4(void);
void coreFirstPointActivationStatus(void);


#endif /* __CORETT4USBMA_H */

/*    End Of File    */


