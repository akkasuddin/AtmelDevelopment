/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm                                            **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc. and Blue Ocean Innovation Ltd.                            **
 **                                                                           **
 **  Copryright(c) 2010. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Blue Ocean Innovation Ltd.                        **
 **  Project#            :  449                                               **
 **  Creation date       :  08/20/2010                                        **
 **  Programmer          :  Michael J. Mariveles                              **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : appTT4_MA.h                                                       **
 **                                                                           **
 **  Description :                                                            **
 **    - Evaluates digital temperature sensor readout if within its specified **
 **      range.                                                               **
 **    - Logs the on-going datapoint readout values w/ alarm trigger time and **
 **      alarm status.                                                        **
 **                                                                           **
 ******************************************************************************/
 
#ifndef __APPTT4_MA_H
#define __APPTT4_MA_H

#define BUTTON_STATUS_NOT_DEPRESSED     0
#define BUTTON_STATUS_START_DEPRESSED   1
#define BUTTON_STATUS_START_RELEASED    2
#define BUTTON_STATUS_STOP_DEPRESSED    3
#define BUTTON_STATUS_STOP_RELEASED     4
#define BUTTON_STATUS_STR_STP_DEPRESSED 5
#define BUTTON_STATUS_STR_STP_RELEASED  6

#define BUTTON_DEBOUNCE_VALID_COUNT     500
#define BUTTON_RETRY_COUNT              3

#define BUTTON_START_STOP               0
#define BUTTON_STOP                     1
#define BUTTON_START                    2
#define BUTTON_INVALID                  3

//==========================================
// TT4USB-MA counter time-out definitions.. 
//==========================================
#define LCD_SUMMARY_EXIT_TIMEOUT        9    // 9 secs as advised by Peter 2/25/11 in Sensitech USA ...noted by mjm
#define CDC_EXIT_TIMEOUT                10   // 10 secs
#define HEART_BEAT_EXIT_TIMEOUT         1    // 1 sec
#define ALARM_CYCLE_TIMEOUT             2    // 2 secs
#define STOP_BUTTON_DELAY_TIMEOUT       3    // 3 secs
#define START_BUTTON_DELAY_TIMEOUT      3    // 3 secs
#define BOOTLOAD_BUTTON_DELAY_TIMEOUT   5    // 5 secs
#define BOOTLOAD_EXIT_TIMEOUT           10   // 10 secs

#define STOP_BUTTON_CFG_TIMEOUT          12   // 10 secs
#define STOP_BUTTON_CFG_WARNING           5   // 5 secs


//======================================
//    Public Function Declaration....
//======================================
UINT8 appTT4MA_TempAcquisitionMode(void);
void appTT4MA_Timer1Interrupt_Enable(void);

void appTT4MA_DisableStartButtonInterrupt(void);
void appTT4MA_EnableStartButtonInterrupt(void);

void appTT4MA_DisableStartStopButtonInterrupt(void);
void appTT4MA_EnableStartStopButtonInterrupt(void);

UINT8 appTT4MA_GetButtonStatus(void);
void appTT4MA_Timer1Interrupt_Disable(void);

void appTT4MA_Init(void);
void appTT4MA_RTCSystemCounterUpdate(void);

void appTT4MA_WakeUpInterrupts_Disable(void);
void appTT4MA_WakeUpInterrupts_Enable(void);

void appMKT_CalculateValues(void);

#endif /* __APPTT4_MA_H */

/*  End of File  */

