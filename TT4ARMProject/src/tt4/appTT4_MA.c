/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm                                            **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc. and Blue Ocean Innovation Ltd.                            **
 **                                                                           **
 **  Copryright(c) 2010. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Blue Ocean Innovation Ltd.                        **
 **  Project#            :  449                                               **
 **  Creation date       :  08/20/2010                                        **
 **  Programmer          :  Michael J. Mariveles                              **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : appTT4_MA.c                                                       **
 **                                                                           **
 **  Description :                                                            **
 **    - Evaluates digital temperature sensor readout if within its specified **
 **      range.                                                               **
 **    - Logs the on-going datapoint readout values w/ alarm trigger time and **
 **      alarm status.                                                        **
 **                                                                           **
 ******************************************************************************/
 
#include "hwPIC24F.h"
#include "hwSystem.h"
#include "devI2CEEPROM_24AA256.h"
#include "devI2CLCD_BU9796FS.h"
#include "devI2CTempSensor_TMP112.h"
#include "utility.h"
#include "commTT4Config.h"
#include "appTT4_MA.h"


//==============================================================================
//                      Local Function Declaration....
//==============================================================================
UINT8 appTT4MA_Process_Alarm_Status(UINT8 par_ubAlarmNumber, UINT8 par_ubAlarmType);

//==============================================================================
//                    Interrupts Implementaiton Routines
//==============================================================================

// Timer Interrupt assinged to TMR1...
void __attribute__ ((interrupt, auto_psv)) _T1Interrupt (void); 
// Change Notification Input Button Interrupt assinged to port RE0... 
void __attribute__ ((interrupt, auto_psv)) _CNInterrupt (void);

//==============================================================================
//                              EEPROM Data 
//==============================================================================

// EEPROM configuration control structure register...
extern __SENSITECH_CONFIGURATION_AREA   _gsSensitech_Configuration_Area;  
extern __USER_CONFIGURATION_AREA        _gsUser_Configuration_Area;
extern __ALARM_CONFIGURATION_AREA       _gsAlarmConfig[6];


// EEPROM control bit manipulator structure register...
extern __sBITS_GENCFGOPT_BYTE           _gsGenCfgOptByte;
extern __sBITS_EXTOPT_BYTE              _gsExtdOptByte;
// ideal temparature limits data...
extern __sBITS_USERCFG_DATA             _gsUserCfgAreaData;
// number of events control byte...
extern __sTRIG_EVENTS_STAT              _guwTrigEvents[7];

// temperature sensor data...
extern _sLOGTMP112TEMPDATA _gsLogTMP112;

// command byte bit manipulator control register....
extern __sBITS_COMMAND_BYTE _gsCommandByte;

// EEPROM memory size...
extern UINT16 guwEEPROM_MemorySize;


//----------------------------------
//  Interrupt Event Status Flag...
//----------------------------------

// Timer Interrupt Toggle Flag...
volatile UINT8 gubTimerEventFlag;
// Button depress seconds counter..
volatile UINT8 gubButtonEventFlag;

volatile UINT8 lubCountIncFlag;

//----------------------------------
//  Interrupt Event Counter Status...
//----------------------------------

// RAM Memory Current Time global storage...
extern volatile UINT32 gudwTT4MA_CurrentTime;

// temperary storage for EDS memory transfer buffer...
extern UINT8   gubMem;
extern UINT16  guwMem;
extern UINT32  gudwMem;


// measurement interval timer counter...
extern volatile UINT32 gudwMeasurementIntervalTimer;
// Triggered Alarm cycle timer counter...
extern volatile UINT8 gubTrigAlarmCycleTimer;
// Heartbeat Icon timer counter ...
extern volatile UINT8 gubHeartIconTimer;
// LCD summary timer counter register...
extern volatile UINT8 gubLCDSummaryTimer;
// Stop button depress delay timer counter register...
extern volatile UINT8 gubStopButtonDelayTimer;
// Start button depress delay timer counter register...
extern volatile UINT8 gubStartButtonDelayTimer;
// Stop button depress for re-configuration...
extern volatile UINT8 gubStopButtonReConfigTimer;
// StartUp Delay timer counter register...
extern volatile UINT32 gudwStartUpDelayTimer;
// Configuration mode timer counter register...
extern volatile UINT8 gubCFModeTimer;


// Button Depress status register...
volatile BOOL gbPreviousButtonDepressFlag;
// Button value register...
volatile UINT8 gubButtonValue;

// Alarm remaining and time elapse value...
extern UINT32 gudwAlarmTimeRemainingElapsed;
// Alarm remaining and time elapse flag...
extern BOOL gbAlarmTimeFlag;

// USB plugged/Unplugged status flag...
extern BOOL gbUSBActiveModeStatusFlag;

// Calculated MKT Temperature readings... new marketing requirement requested by Peter Nunes, 10312013 mjm 
volatile float gfMKT_RunningTemp __attribute__((persistent));           //set as persistent for reset recovery
volatile long double gudwMKT_Accumulator __attribute__((persistent));   //set as persistent for reset recovery
volatile long double gudwMKT_Value __attribute__((persistent));         //set as persistent for reset recovery


/*******************************************************************************
**  Function     : _T1Interrupt
**  Description  : timer interrupt handler
**  PreCondition : must initialize timer interrupt registers 
**  Side Effects : increments global variable in per second basis..
**                   - gudwMeasurementIntervalTimer
**                   - gudwStartUpDelayTimer
**                   - gubTrigAlarmCycleTimer
**                   - gubHeartIconTimer
**                   - gubLCDSummaryTimer
**                   - gubStopButtonDelayTimer
**                   - gubStartButtonDelayTimer
**                   - gubCFModeTimer
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void __attribute__ ((interrupt, auto_psv)) _T1Interrupt (void)
{
    // this Timer Event flag will be set every 1 sec update...
    gubTimerEventFlag = 1; 
    
    IFS0bits.T1IF = 0;    	
}    

/*******************************************************************************
**  Function     : _CNInterrupt
**  Description  : button input interrupt handler
**  PreCondition : must initialize interrupt registers
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void __attribute__ ((interrupt, auto_psv)) _CNInterrupt (void)
{
    // this Button Event flag will be set if a button is depressed...
    gubButtonEventFlag = 1; 
    
    // check Start/Stop buttons status.....
    IFS1bits.CNIF = 0;    
}
 
/*******************************************************************************
**  Function     : appTT4MA_Init
**  Description  : initializes the data
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void appTT4MA_RTCSystemCounterUpdate(void){
    
    // free running timer for temperature measurement interval counter....
    ++gudwMeasurementIntervalTimer;
    
    // StartUp delay timer counter...
    ++gudwStartUpDelayTimer;
    
    // Triggered Alarm cycle timer counter...
    ++gubTrigAlarmCycleTimer;
    
    // Heartbeat Icon timer counter ...
    ++gubHeartIconTimer;
    
    // LCD summary timer counter register...
    ++gubLCDSummaryTimer;
    
    // Stop button depress delay timer counter register...
    ++gubStopButtonDelayTimer;
    // Stop button re-config button depress timer counter register
    ++gubStopButtonReConfigTimer;
    // Start button depress delay timer counter register...
    ++gubStartButtonDelayTimer;
    
    // Configuration mode timer counter register...
    ++gubCFModeTimer;    
}


/*******************************************************************************
**  Function     : appTT4MA_Init
**  Description  : initializes the data
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void appTT4MA_Init(void){
}


/*******************************************************************************
**  Function     : appTT4MA_Process_Alarm_Status
**  Description  : it processes the alarm status states..
**  PreCondition : _gsAlarmConfig,_guwTrigEvents,_gsLogTMP112,_gsLogTMP112 must
**                 not be NULL.
**  Side Effects : none
**  Input        : par_ubAlarmNumber -set alarm number from 0 - 5
**                 par_ubAlarmType - set defined alarm type..
**  Output       : return status ALARM status
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
UINT8 appTT4MA_Process_Alarm_Status(UINT8 par_ubAlarmNumber, UINT8 par_ubAlarmType){
    
    // if Alarm is Enabled then process Alarm Status below...      
    switch(par_ubAlarmType){
    case(HTL_SE): // Alarm Type - 1
        // High Temp Limit - Single Event    
        // Condition: if Alarm goes out from its bands will be triggered...
        if(_gsLogTMP112.fTemp_F_Deg > _gsAlarmConfig[par_ubAlarmNumber].fRange1HighTempSetPoint){            
            
            // accumulate the total time of alarm events....
            ++_guwTrigEvents[par_ubAlarmNumber].udwTotalTime;
            
            // record the number of events...
            if(_guwTrigEvents[par_ubAlarmNumber].bStatusFlag==TRUE){                 
                // clear trig event flag....
                _guwTrigEvents[par_ubAlarmNumber].bStatusFlag = FALSE;
                // accumulate events counter register.... 
                ++_guwTrigEvents[par_ubAlarmNumber].uwNumOfEvents;
            }    

            // accumulate threshold counter register for time calculation....
            ++_gsAlarmConfig[par_ubAlarmNumber].udwCounter;
            lubCountIncFlag = 1;
            if ((_gsAlarmConfig[par_ubAlarmNumber].udwThreshhold > 0) && (_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints == 0))
               --_gsAlarmConfig[par_ubAlarmNumber].udwCounter;
            else
                if(_gsAlarmConfig[par_ubAlarmNumber].udwCounter >= _gsAlarmConfig[par_ubAlarmNumber].udwThreshhold){
                // set the Alarm bit ....
                    return(ALARM_TRIGGERED);
                }
        }
        else{
            // reset counter if temperature goes outside in its temperature bands...
            _gsAlarmConfig[par_ubAlarmNumber].udwCounter = 0;
            // set trig event flag...
            _guwTrigEvents[par_ubAlarmNumber].bStatusFlag = TRUE;
        }     
        
        break; // End of Alarm Type - 1 ...
        
    case(HTL_CE): // Alarm Type - 2
        // High Temp Limit - Cumulative Event            
        // Condition: if Alarm goes out from its bands will be triggered...
        if(_gsLogTMP112.fTemp_F_Deg > _gsAlarmConfig[par_ubAlarmNumber].fRange1HighTempSetPoint){ 

            // accumulate the total time of alarm events....
            ++_guwTrigEvents[par_ubAlarmNumber].udwTotalTime;

            // record the number of events...
            if(_guwTrigEvents[par_ubAlarmNumber].bStatusFlag==TRUE){                 
                // clear trig event flag....
                _guwTrigEvents[par_ubAlarmNumber].bStatusFlag = FALSE;
                // accumulate events counter register.... 
                ++_guwTrigEvents[par_ubAlarmNumber].uwNumOfEvents;
            }    

            // accumulate threshold counter register for time calculation....
            ++_gsAlarmConfig[par_ubAlarmNumber].udwCounter;
            lubCountIncFlag = 1;
            if ((_gsAlarmConfig[par_ubAlarmNumber].udwThreshhold > 0) && (_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints == 0))
                --_gsAlarmConfig[par_ubAlarmNumber].udwCounter;
            else
                if(_gsAlarmConfig[par_ubAlarmNumber].udwCounter >= _gsAlarmConfig[par_ubAlarmNumber].udwThreshhold){
                // set the Alarm bit ....
                    return(ALARM_TRIGGERED);
                }
        }
        else{
            // set trig event flag...
            _guwTrigEvents[par_ubAlarmNumber].bStatusFlag = TRUE;        
        }    
                
        break; // End of Alarm Type - 2 ...
        
    case(LTL_SE): // Alarm Type - 3
        // Low Temp Limit - Single Event
        // Condition: if Alarm goes out from its bands will be triggered...
        if(_gsLogTMP112.fTemp_F_Deg < _gsAlarmConfig[par_ubAlarmNumber].fRange1LowTempSetPoint){

            // accumulate the total time of alarm events....
            ++_guwTrigEvents[par_ubAlarmNumber].udwTotalTime;

            // record the number of events...
            if(_guwTrigEvents[par_ubAlarmNumber].bStatusFlag==TRUE){                 
                // clear trig event flag....
                _guwTrigEvents[par_ubAlarmNumber].bStatusFlag = FALSE;
                // accumulate events counter register.... 
                ++_guwTrigEvents[par_ubAlarmNumber].uwNumOfEvents;
            }    

            // accumulate threshold counter register for time calculation....
            ++_gsAlarmConfig[par_ubAlarmNumber].udwCounter;
            lubCountIncFlag = 1;
            if ((_gsAlarmConfig[par_ubAlarmNumber].udwThreshhold > 0) && (_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints == 0))
                --_gsAlarmConfig[par_ubAlarmNumber].udwCounter;
            else
                if(_gsAlarmConfig[par_ubAlarmNumber].udwCounter >= _gsAlarmConfig[par_ubAlarmNumber].udwThreshhold){
                // set the Alarm bit ....
                    return(ALARM_TRIGGERED);
                }
        }
        else{            
            // set trig event flag...
            _guwTrigEvents[par_ubAlarmNumber].bStatusFlag = TRUE; 
            // reset counter if temperature goes outside in its temperature bands...
            _gsAlarmConfig[par_ubAlarmNumber].udwCounter = 0;
        }     
                    
        break; // End of Alarm Type - 3 ...
        
    case(LTL_CE): // Alarm Type - 4
        // Low Temp Limit - Cummulative Event
        // Condition: if Alarm goes out from its bands will be triggered...
        if(_gsLogTMP112.fTemp_F_Deg < _gsAlarmConfig[par_ubAlarmNumber].fRange1LowTempSetPoint){

            // accumulate the total time of alarm events....
            ++_guwTrigEvents[par_ubAlarmNumber].udwTotalTime;

            // record the number of events...
            if(_guwTrigEvents[par_ubAlarmNumber].bStatusFlag==TRUE){                 
                // clear trig event flag....
                _guwTrigEvents[par_ubAlarmNumber].bStatusFlag = FALSE;
                // accumulate events counter register.... 
                ++_guwTrigEvents[par_ubAlarmNumber].uwNumOfEvents;
            }    

            // accumulate threshold counter register for time calculation....
            ++_gsAlarmConfig[par_ubAlarmNumber].udwCounter;
            lubCountIncFlag = 1;
            if ((_gsAlarmConfig[par_ubAlarmNumber].udwThreshhold > 0) && (_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints == 0))
                --_gsAlarmConfig[par_ubAlarmNumber].udwCounter;
            else
                if(_gsAlarmConfig[par_ubAlarmNumber].udwCounter >= _gsAlarmConfig[par_ubAlarmNumber].udwThreshhold){
                // set the Alarm bit ....
                    return(ALARM_TRIGGERED);
                }
        }
        else{        
            // set trig event flag...
            _guwTrigEvents[par_ubAlarmNumber].bStatusFlag = TRUE;                
        }     
                
        break; // End of Alarm Type - 4 ...
        
    case(STR_SE): // Alarm Type - 5
        // Single Temp Range - Single Event
        // Condition: if Alarm fall within its bands will be triggered...
        if((_gsLogTMP112.fTemp_F_Deg <= _gsAlarmConfig[par_ubAlarmNumber].fRange1HighTempSetPoint) &&
           (_gsLogTMP112.fTemp_F_Deg >= _gsAlarmConfig[par_ubAlarmNumber].fRange1LowTempSetPoint)){

            // accumulate the total time of alarm events....
            ++_guwTrigEvents[par_ubAlarmNumber].udwTotalTime;
               
            // record the number of events...
            if(_guwTrigEvents[par_ubAlarmNumber].bStatusFlag==TRUE){                 
                // clear trig event flag....
                _guwTrigEvents[par_ubAlarmNumber].bStatusFlag = FALSE;
                // accumulate events counter register.... 
                ++_guwTrigEvents[par_ubAlarmNumber].uwNumOfEvents;
            }    

            // accumulate threshold counter register for time calculation....
            ++_gsAlarmConfig[par_ubAlarmNumber].udwCounter;
            lubCountIncFlag = 1;
            if ((_gsAlarmConfig[par_ubAlarmNumber].udwThreshhold > 0) && (_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints == 0))
               --_gsAlarmConfig[par_ubAlarmNumber].udwCounter;
            else
                if(_gsAlarmConfig[par_ubAlarmNumber].udwCounter >= _gsAlarmConfig[par_ubAlarmNumber].udwThreshhold){
                // set the Alarm bit ....
                    return(ALARM_TRIGGERED);
                }
        }     
        else{
            // set trig event flag...
            _guwTrigEvents[par_ubAlarmNumber].bStatusFlag = TRUE;                    
            // reset counter if temperature goes outside in its temperature bands...
            _gsAlarmConfig[par_ubAlarmNumber].udwCounter = 0;
        } 
                        
        break; // End of Alarm Type - 5 ...
        
    case(STR_CE): // Alarm Type - 6
        // Single Temp Range - Cummulative Event    
        // Condition: if Alarm fall within its bands will be triggered...
        if((_gsLogTMP112.fTemp_F_Deg <= _gsAlarmConfig[par_ubAlarmNumber].fRange1HighTempSetPoint) &&
           (_gsLogTMP112.fTemp_F_Deg >= _gsAlarmConfig[par_ubAlarmNumber].fRange1LowTempSetPoint)){ 

            // accumulate the total time of alarm events....
            ++_guwTrigEvents[par_ubAlarmNumber].udwTotalTime;

            // record the number of events...
            if(_guwTrigEvents[par_ubAlarmNumber].bStatusFlag==TRUE){                 
                // clear trig event flag....
                _guwTrigEvents[par_ubAlarmNumber].bStatusFlag = FALSE;
                // accumulate events counter register.... 
                ++_guwTrigEvents[par_ubAlarmNumber].uwNumOfEvents;
            }    
         
            // accumulate threshold counter register for time calculation....
            ++_gsAlarmConfig[par_ubAlarmNumber].udwCounter;
            lubCountIncFlag = 1;
            if ((_gsAlarmConfig[par_ubAlarmNumber].udwThreshhold > 0) && (_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints == 0))
                --_gsAlarmConfig[par_ubAlarmNumber].udwCounter;
            else
                if(_gsAlarmConfig[par_ubAlarmNumber].udwCounter >= _gsAlarmConfig[par_ubAlarmNumber].udwThreshhold){
                // set the Alarm bit ....
                    return(ALARM_TRIGGERED);
                }
        }
        else{
            // set trig event flag...
            _guwTrigEvents[par_ubAlarmNumber].bStatusFlag = TRUE;                
        }
            
        break; // End of Alarm Type - 6...
        
    case(DTR_SE): // Alarm Type - 7
        // Dual Temp Range - Single Event    
        // Condition: if Alarm fall within its bands will be triggered...
        if( ((( _gsLogTMP112.fTemp_F_Deg <= _gsAlarmConfig[par_ubAlarmNumber].fRange1HighTempSetPoint)   &&
            (   _gsLogTMP112.fTemp_F_Deg >= _gsAlarmConfig[par_ubAlarmNumber].fRange1LowTempSetPoint ))) ||
            ((( _gsLogTMP112.fTemp_F_Deg <= _gsAlarmConfig[par_ubAlarmNumber].fRange2HighTempSetPoint)   &&
            (   _gsLogTMP112.fTemp_F_Deg >= _gsAlarmConfig[par_ubAlarmNumber].fRange2LowTempSetPoint )))  )
        {          

            // accumulate the total time of alarm events....
            ++_guwTrigEvents[par_ubAlarmNumber].udwTotalTime;

            // record the number of events...
            if(_guwTrigEvents[par_ubAlarmNumber].bStatusFlag==TRUE){                 
                // clear trig event flag....
                _guwTrigEvents[par_ubAlarmNumber].bStatusFlag = FALSE;
                // accumulate events counter register.... 
                ++_guwTrigEvents[par_ubAlarmNumber].uwNumOfEvents;
            }    

            // accumulate threshold counter register for time calculation....
            ++_gsAlarmConfig[par_ubAlarmNumber].udwCounter;
            lubCountIncFlag = 1;
            if ((_gsAlarmConfig[par_ubAlarmNumber].udwThreshhold > 0) && (_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints == 0))
               --_gsAlarmConfig[par_ubAlarmNumber].udwCounter;
            else
                if(_gsAlarmConfig[par_ubAlarmNumber].udwCounter >= _gsAlarmConfig[par_ubAlarmNumber].udwThreshhold){
                // set the Alarm bit ....
                    return(ALARM_TRIGGERED);
                }
        }
        else{
            // set trig event flag...
            _guwTrigEvents[par_ubAlarmNumber].bStatusFlag = TRUE;                    
            // reset counter if temperature goes outside in its temperature bands...
            _gsAlarmConfig[par_ubAlarmNumber].udwCounter = 0;
        } 
            
        break; // End of Alarm Type - 7...
        
    case(DTR_CE): // Alarm Type - 8
        // Dual Temp Range - Cummulative Event        
        // Condition: if Alarm fall within its bands will be triggered...
        if( ((( _gsLogTMP112.fTemp_F_Deg <= _gsAlarmConfig[par_ubAlarmNumber].fRange1HighTempSetPoint)   &&
            (   _gsLogTMP112.fTemp_F_Deg >= _gsAlarmConfig[par_ubAlarmNumber].fRange1LowTempSetPoint ))) ||
            ((( _gsLogTMP112.fTemp_F_Deg <= _gsAlarmConfig[par_ubAlarmNumber].fRange2HighTempSetPoint)   &&
            (   _gsLogTMP112.fTemp_F_Deg >= _gsAlarmConfig[par_ubAlarmNumber].fRange2LowTempSetPoint )))  )
        {          

            // accumulate the total time of alarm events....
            ++_guwTrigEvents[par_ubAlarmNumber].udwTotalTime;

            // record the number of events...
            if(_guwTrigEvents[par_ubAlarmNumber].bStatusFlag==TRUE){                 
                // clear trig event flag....
                _guwTrigEvents[par_ubAlarmNumber].bStatusFlag = FALSE;
                // accumulate events counter register.... 
                ++_guwTrigEvents[par_ubAlarmNumber].uwNumOfEvents;
            }    

            // accumulate threshold counter register for time calculation....
            ++_gsAlarmConfig[par_ubAlarmNumber].udwCounter;
            lubCountIncFlag = 1;
            // check if over its threshold time....
            if ((_gsAlarmConfig[par_ubAlarmNumber].udwThreshhold > 0) && (_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints == 0))
                --_gsAlarmConfig[par_ubAlarmNumber].udwCounter;
            else
                if(_gsAlarmConfig[par_ubAlarmNumber].udwCounter >= _gsAlarmConfig[par_ubAlarmNumber].udwThreshhold){
                // set the Alarm bit ....
                    return(ALARM_TRIGGERED);
                }
        }
        else{
            // set trig event flag...
            _guwTrigEvents[par_ubAlarmNumber].bStatusFlag = TRUE;                
        }
           
        break; // End of Alarm Type - 8...
        
    case(TIME_ONLY): // Alarm Type - 9
    
     /*___________________________________________________________________________
        
        Time Calculation of Time Only Alarm type..
        
        Conditions,
        1. aquire the preset time in seconds written by Turbo PC software from EERPOM of TT4 device ..
        2. from start time, track the time elapsed and compare to the preset time...
        
        Hence,
        
        T1 = Threshold * Measurement_Interval  =  preset time data from EEPROM
        T2 = Counter * Measurement_Interval    =  device running time elapsed
        
        Therefore,
        
        if( T1 > T2) trigger time
        
        Note: 
        since measurement interval is common then should be cancelled accordingly...
        
        \mjmariveles....
     ___________________________________________________________________________*/

        // in time only, accumulate coounter from start time till stop time...
        ++_gsAlarmConfig[par_ubAlarmNumber].udwCounter;
        lubCountIncFlag = 1;
        // accumulate the total time of alarm events....
        ++_guwTrigEvents[par_ubAlarmNumber].udwTotalTime;

        // Time only            
        if(_gsAlarmConfig[par_ubAlarmNumber].udwCounter > _gsAlarmConfig[par_ubAlarmNumber].udwThreshhold){    
            // time limit is reached then set the Alarm bit ....
            return(ALARM_TRIGGERED);
        }        
                    
        break; // End of Alarm Type - 9...
        
        
    case(MKT_HI): // Alarm Type - 10
        // MKT High Temp Limit - Single Event    
        
        
        // accumulate the total time same as Trip Lenght.... 
        ++_guwTrigEvents[par_ubAlarmNumber].udwTotalTime;
            
        // Condition: if Alarm goes out from its bands will be triggered...
        if(gfMKT_RunningTemp > _gsAlarmConfig[par_ubAlarmNumber].fRange1HighTempSetPoint){
                                                                 
            // set the Alarm bit ....
            return(ALARM_TRIGGERED);            
        }        
        
        break; // End of Alarm Type - 10 ...

    case(MKT_LO): // Alarm Type - 11
        // MKT Low Temp Limit - Single Event
        
        // accumulate the total time same as Trip Lenght.... 
        ++_guwTrigEvents[par_ubAlarmNumber].udwTotalTime;
        
        // Condition: if Alarm goes out from its bands will be triggered...
        if(gfMKT_RunningTemp < _gsAlarmConfig[par_ubAlarmNumber].fRange1LowTempSetPoint){
	        
	        //gfMKT_RunningTemp = 0.11;
	                                                                         
            // set the Alarm bit ....
            return(ALARM_TRIGGERED);            
        }        
                    
        break; // End of Alarm Type - 11 ...        
        
    } // End of appTT4MA_Process_Alarm_Status Switch statement...
    
    return(ALARM_OK);
}


/*******************************************************************************
**  Function     : appTT4MA_Check_Alarm_Status
**  Description  : chaecks alarm status
**  PreCondition : _gsUserCfgAreaData,_gsLogTMP112 structure data must not be NULL.
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void appTT4MA_Check_Alarm_Status(void){
    
    UINT8 lubCtr;
    UINT8 lubAlarmNum;
    UINT8 lubCurrentAlarmNum;
    UINT32 ludwTimeStamp;


    // Alarm Configuration Byte0 control variables...
    __sBITS_ALARM_CONFIG_BYTE0  _lsBitAlarm_ConfigB0;
    // Alarm Status Byte control variables...
    __sBITS_ALARM_STATUS _lsBitAlarm_Status;
    
    
    lubCountIncFlag = 0; //Keeps track of when the Alarm counter increments so we can store the value in EEPROM for reset reocovery
    //=====================
    //  Ideal Temp...
    //=====================
    if(_gsExtdOptByte.ExtOpt.Byte1.IdealTempRgViewEna==1){
    
        // Single Temp Range - Single Event
        // Condition: if Alarm fall within its bands will be triggered...
        if((_gsLogTMP112.fTemp_F_Deg <= _gsUserCfgAreaData.fTempIdealRangeHighValue) &&
           (_gsLogTMP112.fTemp_F_Deg >= _gsUserCfgAreaData.fTempIdealRangeLowValue)){

            // accumulate the total time of alarm events....
            ++_guwTrigEvents[IDEAL_TEMP].udwTotalTime;
               
            // record the number of events...
            if(_guwTrigEvents[IDEAL_TEMP].bStatusFlag==TRUE){                 
                // clear trig event flag....
                _guwTrigEvents[IDEAL_TEMP].bStatusFlag = FALSE;
                // accumulate events counter register.... 
                ++_guwTrigEvents[IDEAL_TEMP].uwNumOfEvents;
            }    
        }     
        else{
            // set trig event flag...
            _guwTrigEvents[IDEAL_TEMP].bStatusFlag = TRUE;                                
        }     
        
    } /*  End of Ideal Temp Label  */
    
   
    //=====================
    //  Alarm Status...
    //=====================
    
    // scan all Alarm status from 1 to 6.... (physical number array is from 0 to 5)
    for(lubCtr = ALARM_1 ; lubCtr <= ALARM_6; lubCtr++){

        // init alarm bit parser.. mjm 
        _lsBitAlarm_Status.ubByte = 0;
        
        //---------------------------------------------------
        // set Alarm Configuration and get Status bits...
        //---------------------------------------------------
        _lsBitAlarm_ConfigB0.ubByte = _gsAlarmConfig[lubCtr].ubConfigByte0;        
    
        if(_lsBitAlarm_ConfigB0.Bit.Enable == ALARM_ENABLED){        
                
            // check Alarm Status bit if triggered after process ...
            if(appTT4MA_Process_Alarm_Status(lubCtr,_lsBitAlarm_ConfigB0.Bit.Type) == ALARM_TRIGGERED){
                    
                // counter check if the alarm were already triggered before!...
                if(((_gsUser_Configuration_Area.ubAlarmStatus & (1<<lubCtr)) >> lubCtr) == ALARM_NOT_TRIG_BEFORE){  // skip if Alarm is triggered before...
                    
                    // add more delay if Mutli-Drop is plugged to the USB because 32MHz clock was enabled!.. mjm
                    // therefore it need a more delay between EEPROM access to prevent hangup problem...
                    if((_gsGenCfgOptByte.GenOpt.Byte1.MultDropFlag==1)&&(gbUSBActiveModeStatusFlag==TRUE)) utilDelay_ms(10); 
                    
                    // calculate time occured...
                    if(_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints>0){                        
                        // check if Alarm type is TimeOnly because it has a different triggering time reference... mjm    
                        ludwTimeStamp = _gsUser_Configuration_Area.udwUnit_Start_TimeStamp + ((UINT32)_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints * _gsUser_Configuration_Area.udwMeasurementInterval);
                    }    
                    else{    
                        // if the alarm happend at the 1st point then should be equal to start time stamp..
                        ludwTimeStamp = _gsUser_Configuration_Area.udwUnit_Start_TimeStamp; 
                    }                        

                    // update Alarm Triggered Flag and masked unused bit...                    
                    _lsBitAlarm_Status.ubByte = _gsUser_Configuration_Area.ubAlarmStatus & 0x3F;

                    // For some reason, a little delay is needed before writing the alarm timestamp
                    // into a microchip EEPROM. Atmel EEs do not need this extra delay - bfaller
                    utilDelay_ms(10);
                    //--------------------------------------------------------------
                    // scan triggered Alarm!...
                    //--------------------------------------------------------------                    
                    switch(lubCtr){
                    case(ALARM_1):
                        _lsBitAlarm_Status.Bit.Alarm1 = 1;
                        _gsUser_Configuration_Area.ubAlarmStatus = _lsBitAlarm_Status.ubByte;
                        // calculate Alarm 1 triggered time stamp...
                        _gsUser_Configuration_Area.udwAlarm1_TriggeredTimeStamp = ludwTimeStamp;
                        // Alarm 1 timestamp write to EEPROM...
                        gudwMem = _gsUser_Configuration_Area.udwAlarm1_TriggeredTimeStamp;
                        commTT4Cfg_FastWriteCfgEeprom(OFFSET_ALARM_1_TRIGGERED_TIMESTAMP,(const void *)&gudwMem);
                        break;
                    case(ALARM_2):
                        _lsBitAlarm_Status.Bit.Alarm2 = 1;
                        _gsUser_Configuration_Area.ubAlarmStatus = _lsBitAlarm_Status.ubByte;
                        // calculate Alarm 2 triggered time stamp...
                        _gsUser_Configuration_Area.udwAlarm2_TriggeredTimeStamp = ludwTimeStamp;
                        // Alarm 2 timestamp write to EEPROM...
                        gudwMem = _gsUser_Configuration_Area.udwAlarm2_TriggeredTimeStamp;
                        commTT4Cfg_FastWriteCfgEeprom(OFFSET_ALARM_2_TRIGGERED_TIMESTAMP,(const void *)&gudwMem);
                        break;
                    case(ALARM_3):
                        _lsBitAlarm_Status.Bit.Alarm3 = 1;
                        _gsUser_Configuration_Area.ubAlarmStatus = _lsBitAlarm_Status.ubByte;
                        // calculate Alarm 3 triggered time stamp...
                        _gsUser_Configuration_Area.udwAlarm3_TriggeredTimeStamp = ludwTimeStamp;
                        // Alarm 3 timestamp write to EEPROM...
                        gudwMem = _gsUser_Configuration_Area.udwAlarm3_TriggeredTimeStamp;
                        commTT4Cfg_FastWriteCfgEeprom(OFFSET_ALARM_3_TRIGGERED_TIMESTAMP,(const void *)&gudwMem);
                        break;
                    case(ALARM_4):
                        _lsBitAlarm_Status.Bit.Alarm4 = 1;
                        _gsUser_Configuration_Area.ubAlarmStatus = _lsBitAlarm_Status.ubByte;
                        // calculate Alarm 4 triggered time stamp...
                        _gsUser_Configuration_Area.udwAlarm4_TriggeredTimeStamp = ludwTimeStamp;
                        // Alarm 4 timestamp write to EEPROM...
                        gudwMem = _gsUser_Configuration_Area.udwAlarm4_TriggeredTimeStamp;
                        commTT4Cfg_FastWriteCfgEeprom(OFFSET_ALARM_4_TRIGGERED_TIMESTAMP,(const void *)&gudwMem);
                        break;
                    case(ALARM_5):
                        _lsBitAlarm_Status.Bit.Alarm5 = 1;
                        _gsUser_Configuration_Area.ubAlarmStatus = _lsBitAlarm_Status.ubByte;
                        // calculate Alarm 5 triggered time stamp...
                        _gsUser_Configuration_Area.udwAlarm5_TriggeredTimeStamp = ludwTimeStamp;
                        // Alarm 5 timestamp write to EEPROM...
                        gudwMem = _gsUser_Configuration_Area.udwAlarm5_TriggeredTimeStamp;
                        commTT4Cfg_FastWriteCfgEeprom(OFFSET_ALARM_5_TRIGGERED_TIMESTAMP,(const void *)&gudwMem);
                        break;
                    case(ALARM_6):
                        _lsBitAlarm_Status.Bit.Alarm6 = 1;
                        _gsUser_Configuration_Area.ubAlarmStatus = _lsBitAlarm_Status.ubByte;
                        // calculate Alarm 6 triggered time stamp...
                        _gsUser_Configuration_Area.udwAlarm6_TriggeredTimeStamp = ludwTimeStamp;
                        // Alarm 6 timestamp write to EEPROM...
                        gudwMem = _gsUser_Configuration_Area.udwAlarm6_TriggeredTimeStamp;
                        commTT4Cfg_FastWriteCfgEeprom(OFFSET_ALARM_6_TRIGGERED_TIMESTAMP,(const void *)&gudwMem);
                        break;                    
                    }  


                    // add more delay if Mutli-Drop is plugged to the USB because 32MHz clock was enabled!.. mjm
                    // therefore it need a more delay between EEPROM access to prevent hangup problem...
                    if((_gsGenCfgOptByte.GenOpt.Byte1.MultDropFlag==1)&&(gbUSBActiveModeStatusFlag==TRUE)) utilDelay_ms(10); 

                    //------------------------------------------------------------
                    // log alarm status registers in real time interval mode...   
                    //------------------------------------------------------------                     
                    gubMem = _gsUser_Configuration_Area.ubAlarmStatus; 
                    commTT4Cfg_FastWriteCfgEeprom(OFFSET_ALARM_STATUS,(const void *)&gubMem);

                    // add more delay if Mutli-Drop is plugged to the USB because 32MHz clock was enabled!.. mjm
                    // therefore it need a more delay between EEPROM access to prevent hangup problem...
                    if((_gsGenCfgOptByte.GenOpt.Byte1.MultDropFlag==1)&&(gbUSBActiveModeStatusFlag==TRUE)) utilDelay_ms(10);                     
                    
                } /* End of Skip Alarm if Triggered Before */   
                                   
            } /* End of Process Alarm Status */

            if (lubCountIncFlag == 1)
            {
                lubCountIncFlag = 0;
                 switch(lubCtr){
                 case(ALARM_1):
                     gudwMem = _gsAlarmConfig[lubCtr].udwCounter;
                     commTT4Cfg_FastWriteCfgEeprom(OFFSET_ALARM_1_COUNTER,(const void *)&gudwMem);
                     break;
                 case(ALARM_2):
                     gudwMem = _gsAlarmConfig[lubCtr].udwCounter;
                     commTT4Cfg_FastWriteCfgEeprom(OFFSET_ALARM_2_COUNTER,(const void *)&gudwMem);
                     break;
                  case(ALARM_3):
                     gudwMem = _gsAlarmConfig[lubCtr].udwCounter;
                     commTT4Cfg_FastWriteCfgEeprom(OFFSET_ALARM_3_COUNTER,(const void *)&gudwMem);
                     break;
                 case(ALARM_4):
                     gudwMem = _gsAlarmConfig[lubCtr].udwCounter;
                     commTT4Cfg_FastWriteCfgEeprom(OFFSET_ALARM_4_COUNTER,(const void *)&gudwMem);
                     break;
                  case(ALARM_5):
                     gudwMem = _gsAlarmConfig[lubCtr].udwCounter;
                     commTT4Cfg_FastWriteCfgEeprom(OFFSET_ALARM_5_COUNTER,(const void *)&gudwMem);
                     break;
                 case(ALARM_6):
                     gudwMem = _gsAlarmConfig[lubCtr].udwCounter;
                     commTT4Cfg_FastWriteCfgEeprom(OFFSET_ALARM_6_COUNTER,(const void *)&gudwMem);
                     break;
                 }
            }

            
            
            //--------------------------------------------------------------
            // check remaining time or elapse time bits if enabled!...
            //--------------------------------------------------------------
            if((_gsUser_Configuration_Area.ubRemAlarmDispCfgByte & MASK_ALARM_TIME_REM_ELP_DISP_ENA) == 1){
                        
                // check alarm time flag if valid!...
                if(gbAlarmTimeFlag==TRUE){
                            
                    // get current alarm triggered!...
                    lubCurrentAlarmNum = lubCtr+1;
                            
                    // check which Alarm Number Triggered to be displayed!...
                    lubAlarmNum = (_gsUser_Configuration_Area.ubRemAlarmDispCfgByte  & MASK_ALARM_TIME_REM_ELP_ALM_NUM) >> 4;
                            
                    // validate the specified Alarm number ...
                    if(lubAlarmNum==lubCurrentAlarmNum){
                        
                        // check alarm status if triggered!...
                        if(_gsUser_Configuration_Area.ubAlarmStatus & (1<<(lubAlarmNum-1))){
                            // set to lock the bit!!...    
                            gbAlarmTimeFlag = FALSE;
                        }        
                                
                        // check which alarm time feature is being set!...
                        if((_gsUser_Configuration_Area.ubRemAlarmDispCfgByte & MASK_ALARM_TIME_REM_ELP_SELECT) == ALARM_TIME_SELECT_ELAPSE_TIME){
                            // increment timer for Time Elapsed option...
                            gudwAlarmTimeRemainingElapsed = _gsAlarmConfig[lubAlarmNum-1].udwCounter * _gsUser_Configuration_Area.udwMeasurementInterval;
                        }
                        else{
                            // decrement timer for Time Remaining Option...
                            if(_gsAlarmConfig[lubAlarmNum-1].udwThreshhold >= _gsAlarmConfig[lubAlarmNum-1].udwCounter){
                                gudwAlarmTimeRemainingElapsed = _gsUser_Configuration_Area.udwMeasurementInterval * (_gsAlarmConfig[lubAlarmNum-1].udwThreshhold - _gsAlarmConfig[lubAlarmNum-1].udwCounter);
                            }
                            else{
                                gudwAlarmTimeRemainingElapsed = 0;
                            }
                        }
                                
                    } /* End of Display Alarm Remaining Time or Elapsed Time Value to LCD */
                    
                } /* End of Check Alarm and do not Display Alarm Remaining Time or Elapsed Time */                        
            } /* End of Display Alarm Remaining Time or Elapsed Time */
        } /* End of Alarm scan enabled */            
    }  /* End of Alarm scan loop */  
} /* End Of Alarm Status Check! */

    
/*******************************************************************************
**  Function     : appTT4MA_TempAcquisitionMode
**  Description  : reads digital temperature data and saves to EERPOM
**  PreCondition : TMP112 temperatrure sensor must initialized before calling 
**                 this routine.
**  Side Effects : none
**  Input        : none
**  Output       : returns datapoint memory storage status if FULL
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
UINT8 appTT4MA_TempAcquisitionMode(void){
    
    //-----------------------------------------------------------------------
    //  Temperature Unit should be strictly in F degree always 
    //  in this routine as required by Sensitech ...
    //
    //  \ mjmariveles... 11022010
    //-----------------------------------------------------------------------
    
    
    // get the raw temperature data from temperature sensor...
    // this binary data is unformatted...     
    devSensor_ReadTemp_OneShotMode();
    
    // convert raw temperature data to final temperature data...
    devSensor_AquireFinalTemperatureValues(); 
    
    if(_gsCommandByte.Bit.MarkerFlag == 1){
        // detected a start button depression set marker        
        devSensor_MarkSensitechTempData();
    }    
    
    // Note: this is a new marketing requirement requested by Peter Nunes, 10312013 mjm...
    // after acquiring temperature, calculate the accumolated MKT...
    appMKT_CalculateValues(); 


    // add more delay if Mutli-Drop is plugged to the USB because 32MHz clock was enabled!.. mjm
    // therefore it need a more delay between EEPROM access to prevent hangup problem...
    if((_gsGenCfgOptByte.GenOpt.Byte1.MultDropFlag==1)&&(gbUSBActiveModeStatusFlag==TRUE)) utilDelay_ms(10); 

    //----------------------------------------------------------------------        
    // log datapoint index registers in real time interval mode...    
    //----------------------------------------------------------------------        
    guwMem = _gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints + 1; 
    commTT4Cfg_FastWriteCfgEeprom(OFFSET_NUMBER_OF_STORED_DATAPOINTS,(const void *)&guwMem);
        
    // add more delay if Mutli-Drop is plugged to the USB because 32MHz clock was enabled!.. mjm
    // therefore it need a more delay between EEPROM access to prevent hangup problem...
    if((_gsGenCfgOptByte.GenOpt.Byte1.MultDropFlag==1)&&(gbUSBActiveModeStatusFlag==TRUE)) utilDelay_ms(10); 
    
    //----------------------------------------------------------------------        
    // save datapoint to EEPROM...        
    // Note: the Sensitech Binary Temparature Data is always in Fahrenheit value...
    //----------------------------------------------------------------------        
    commTT4Cfg_WriteEeprom_RawDataPoint(_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints,
                                        devSensor_GetSensitechBinaryTempData());

    // add more delay if Mutli-Drop is plugged to the USB because 32MHz clock was enabled!.. mjm
    // therefore it need a more delay between EEPROM access to prevent hangup problem...
    if((_gsGenCfgOptByte.GenOpt.Byte1.MultDropFlag==1)&&(gbUSBActiveModeStatusFlag==TRUE)) utilDelay_ms(10); 
    
    
    //----------------------------------------------------------------------        
    // clear the marker status flag bit after saving the data point to EEPROM...
    //----------------------------------------------------------------------        
    _gsCommandByte.Bit.MarkerFlag = 0;    
    devSensor_UnMarkSensitechTempData();
    
    
    //----------------------------------------------------------------------        
    // validate temperature results if within limits or if alarm were trip..
    //----------------------------------------------------------------------        
    appTT4MA_Check_Alarm_Status();
    

    //---------------------------------------------------------------
    // Increment datapoint index countner for the next entry...
    //---------------------------------------------------------------
    ++_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints;
    
    if(_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints >= guwEEPROM_MemorySize){ // requested by peter per discussion of end customer request..   022311
        // memory full...
        return(MEMORY_FULL);
    }    
    
    
    // not memory full...
    return(!MEMORY_FULL);    
}

/*******************************************************************************
**  Function     : appTT4MA_Timer1Interrupt_Enable
**  Description  : will initialize 1sec timer interrupt for temperature reading.
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void appTT4MA_Timer1Interrupt_Enable(void)
{    
    T1CONbits.TON = 1;    
    T1CONbits.TCKPS = 0; // prescaler set to div 1...
    T1CONbits.TGATE = 0;
    T1CONbits.TCS = 1;   // 0-fsys/2 .. 1-source SOSC 32khz 
    T1CONbits.TSYNC = 0;    
    IPC0bits.T1IP = TIMER_ASSIGNED_INTERRUPT_PRIORITY;
    TMR1 = 0;
    //PR1 = 4096; // 4096 x 8 x (1/32768) = 1sec 
    //PR1 = 32768u; // 32768 / 1 / 32768 = 1sec 
    PR1 = 32767u; // 32768 / 1 / 32768 = 1sec 
    //RCFGCALbits.CAL = 0;
                  
    IFS0bits.T1IF = 0;
    IEC0bits.T1IE = 1;    
}

/*******************************************************************************
**  Function     : appTT4MA_Timer1Interrupt_Disable
**  Description  : disables 1sec timer interrupt 
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void appTT4MA_Timer1Interrupt_Disable(void)
{
    IEC0bits.T1IE = 0;
    IFS0bits.T1IF = 0;

    T1CONbits.TON = 0;
    IPC0bits.T1IP = 0;
}

/*******************************************************************************
**  Function     : appTT4MA_EnableStartButtonInterrupt
**  Description  : initializes change notification interrupt of START Button.
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void appTT4MA_EnableStartButtonInterrupt(void){

	// Start Button...CN58 is START button
	CNEN4bits.CN58IE = 1;	    // Input Change Notification CN58 Interrupt Enable	
	
	IFS1bits.CNIF = 0;		    // Input Change Notification Interrupt Flag Status Bit ,  clear IF =0
	IPC4bits.CNIP = CN_ASSIGNED_INTERRUPT_PRIORITY;		// Input Change Notification Interrupt Priority Bit 0-disabled 1-7 priority range... set INT to low priority as #1 
	                            // Note: wakeup button interrupt priority is level 1
	IEC1bits.CNIE = 1;		    // Input Change Notification Interrupt Enable Bit  1= enable / 0=disable    
}


/*******************************************************************************
**  Function     : appTT4MA_DisableStartButtonInterrupt
**  Description  : disables change notification interrupt of START Button.
**  PreCondition : initialize first the change notification interrupt of START Button.
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void appTT4MA_DisableStartButtonInterrupt(void){

    IEC1bits.CNIE = 0;		    // Input Change Notification Interrupt Enable Bit  1= enable / 0=disable    
	IPC4bits.CNIP = 0;	   	    // Input Change Notification Interrupt Priority Bit 0-disabled 1-7 priority range... set INT to low priority as #1 
	                            // Note: wakeup button interrupt priority is level 1

	// Start Button...CN58 is START button
	CNEN4bits.CN58IE = 0;	    // Input Change Notification CN58 Interrupt Enable	
	
	IFS1bits.CNIF = 0;		    // Input Change Notification Interrupt Flag Status Bit ,  clear IF =0	
}

/*******************************************************************************
**  Function     : appTT4MA_EnableStartStopButtonInterrupt
**  Description  : initializes change notification interrupt of STOP Button.
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void appTT4MA_EnableStartStopButtonInterrupt(void){

	// Start Button...CN58 is START button
	CNEN4bits.CN58IE = 1;	    // Input Change Notification CN58 Interrupt Enable	
	
    // Stop Button...CN59 is STOP button
	CNEN4bits.CN59IE = 1;	    // Input Change Notification CN59 Interrupt Enable	

	IFS1bits.CNIF = 0;		    // Input Change Notification Interrupt Flag Status Bit ,  clear IF =0
	IPC4bits.CNIP = CN_ASSIGNED_INTERRUPT_PRIORITY;		// Input Change Notification Interrupt Priority Bit 0-disabled 1-7 priority range... set INT to low priority as #1 
	                            // Note: wakeup button interrupt priority is level 1
	IEC1bits.CNIE = 1;		    // Input Change Notification Interrupt Enable Bit  1= enable / 0=disable    

}

/*******************************************************************************
**  Function     : appTT4MA_DisableStartStopButtonInterrupt
**  Description  : disables change notification interrupt of STOP Button.
**  PreCondition : initialize first the change notification interrupt of STOP Button.
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void appTT4MA_DisableStartStopButtonInterrupt(void){

    IEC1bits.CNIE = 0;		    // Input Change Notification Interrupt Enable Bit  1= enable / 0=disable    
	IPC4bits.CNIP = 0;	   	    // Input Change Notification Interrupt Priority Bit 0-disabled 1-7 priority range... set INT to low priority as #1 
	                            // Note: wakeup button interrupt priority is level 1

	// Start Button...CN58 is START button
	CNEN4bits.CN58IE = 0;	    // Input Change Notification CN58 Interrupt Enable	
	
    // Stop Button...CN59 is STOP button
	CNEN4bits.CN59IE = 0;	    // Input Change Notification CN58 Interrupt Enable	

	IFS1bits.CNIF = 0;		    // Input Change Notification Interrupt Flag Status Bit ,  clear IF =0	

}

/*******************************************************************************
**  Function     : appTT4MA_WakeUpInterrupts_Enable
**  Description  : initializes wakeup interrupts that would be used in STOP mode
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void appTT4MA_WakeUpInterrupts_Enable(void){

	// Start Button...CN58 is START button
	CNEN4bits.CN58IE = 1;	    // Input Change Notification CN58 Interrupt Enable	

	// Stop Button...CN59 is STOP button
	CNEN4bits.CN59IE = 1;	    // Input Change Notification CN59 Interrupt Enable	
    
	// Vbus Input port Detection...CN49 ..
	CNEN4bits.CN49IE = 1;	    // Input Change Notification CN58 Interrupt Enable	
	
	IFS1bits.CNIF = 0;		    // Input Change Notification Interrupt Flag Status Bit ,  clear IF =0
	IPC4bits.CNIP = CN_ASSIGNED_INTERRUPT_PRIORITY;		// Input Change Notification Interrupt Priority Bit 0-disabled 1-7 priority range... set INT to low priority as #1 
	                            // Note: wakeup button interrupt priority is level 1
	IEC1bits.CNIE = 1;		    // Input Change Notification Interrupt Enable Bit  1= enable / 0=disable    

}

/*******************************************************************************
**  Function     : appTT4MA_WakeUpInterrupts_Disable
**  Description  : disables wakeup interrupts that would be used in STOP mode
**  PreCondition : initialize wakeup interrupt must called first
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void appTT4MA_WakeUpInterrupts_Disable(void){

    IEC1bits.CNIE = 0;		    // Input Change Notification Interrupt Enable Bit  1= enable / 0=disable    
	IPC4bits.CNIP = 0;	   	    // Input Change Notification Interrupt Priority Bit 0-disabled 1-7 priority range... set INT to low priority as #1 
	                            // Note: wakeup button interrupt priority is level 1

	// Start Button...CN59 is START button
	CNEN4bits.CN59IE = 0;	    // Input Change Notification CN59 Interrupt Enable	
	// Start Button...CN58 is START button
	CNEN4bits.CN58IE = 0;	    // Input Change Notification CN58 Interrupt Enable	
	// Vbus Input port Detection...CN49 ..
	CNEN4bits.CN49IE = 0;	    // Input Change Notification CN58 Interrupt Enable	
	
	IFS1bits.CNIF = 0;		    // Input Change Notification Interrupt Flag Status Bit ,  clear IF =0	

}


/*******************************************************************************
**  Function     : appTT4MA_GetButtonStatus
**  Description  : input control and status button handler
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : returns the button status
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
UINT8 appTT4MA_GetButtonStatus(void){
    
    volatile UINT8 lubButtonMaskBits;
    volatile UINT8 lubTempButtonValue;
    register INT16 lwDebounceCount;
    register UINT8 lubRetryCount;
    

  //=====================  
    LABEL_BUTTON_RETRY:
  //===================== 
    
    utilDelay_ms(50); // 50msec delay..
    
    // read temperary button masked bits..
    lubTempButtonValue = TT4_BUTTON_MASK;
    
    if(lubTempButtonValue==0x3){ 
        // neither button released nor no button event...
        if(gbPreviousButtonDepressFlag == TRUE){
            
            gbPreviousButtonDepressFlag = FALSE;

            // check and return what button is detected...
            switch(gubButtonValue){
            case(BUTTON_STATUS_STR_STP_DEPRESSED) :
                return(BUTTON_STATUS_STR_STP_RELEASED); // configuration mode...
            case(BUTTON_STATUS_STOP_DEPRESSED) :
                return(BUTTON_STATUS_STOP_RELEASED); // run mode stop...
            case(BUTTON_STATUS_START_DEPRESSED) :
                return(BUTTON_STATUS_START_RELEASED); //run mode start...
            }
            
        }    
        else{
            gbPreviousButtonDepressFlag = FALSE;
            return(BUTTON_STATUS_NOT_DEPRESSED);
        } 
    }    
    
    lubRetryCount = 0;
    lwDebounceCount = 0;
    
    // conduct button debounce check...
    do{       
        // read detected button masked bits.. 
        lubButtonMaskBits = TT4_BUTTON_MASK;        
        
        if(lubTempButtonValue == lubButtonMaskBits){
            ++lwDebounceCount;
        }    
        else{
            --lwDebounceCount;
        }    
            
        if(lwDebounceCount<0){            
            if(++lubRetryCount > BUTTON_RETRY_COUNT) return(BUTTON_STATUS_NOT_DEPRESSED);           
            goto LABEL_BUTTON_RETRY;
        }   
    
    }while(lwDebounceCount<BUTTON_DEBOUNCE_VALID_COUNT); // validate button counts..

    // at this point the button value depressed is valid!...mjm
    gbPreviousButtonDepressFlag = TRUE;
    
    // check and return what button is detected...
    switch(lubButtonMaskBits){
        case(BUTTON_START_STOP) :
            gubButtonValue = BUTTON_STATUS_STR_STP_DEPRESSED;
            return(BUTTON_STATUS_STR_STP_DEPRESSED); // configuration mode...
        case(BUTTON_STOP) :
            gubButtonValue = BUTTON_STATUS_STOP_DEPRESSED;
            return(BUTTON_STATUS_STOP_DEPRESSED); // run mode stop...
        case(BUTTON_START) :
            gubButtonValue = BUTTON_STATUS_START_DEPRESSED;
            return(BUTTON_STATUS_START_DEPRESSED); //run mode start...
    }
    
    // unknown button value...
    gbPreviousButtonDepressFlag = FALSE;    
    // if none of the above then return invalid...
    return(BUTTON_STATUS_NOT_DEPRESSED);    
}    


/*******************************************************************************
**  Function     : appMKT_CalculateValues
**  Description  : calculates the running MKT value...
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  103113  Michael J. Mariveles  1st release.    
*******************************************************************************/
#define CELSIUS_KELVIN_CONST        273.15             // Kelvin to Celsius constant...
#define FAHRENHEIT_KELVIN_CONST     459.67             // Kelvin to Fahrenheit constant...
#define MKT_UNIVERSAL_GAS_CONSTANT  8.314472           // universal gas constant... 

/*
// debugging vars...
long double temp_gudwMKT_Value;
long double temp_gudwMKT_Accumulator;
long double temp_gudwKelvinTemp;
long double temp_gudwActEngeryConst;
long double temp_gudwDataPoints;
long double temp_gudwRunTemp;
long double temp_gudwFtemp;
*/

void appMKT_CalculateValues(void){
	
	//register long double ludwActivationEngeryConstant = _gsUser_Configuration_Area.fMKTActivationEnergy / MKT_UNIVERSAL_GAS_CONSTANT;	
	//register long double ludwNumOfDatapoints = (double)_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints + 1;
	//register long double ludwKelvinTemp;
	//register long double ludwTempDataC = TEMP_C(_gsLogTMP112.fTemp_F_Deg);
	
	long double ludwActivationEngeryConstant = _gsUser_Configuration_Area.fMKTActivationEnergy / MKT_UNIVERSAL_GAS_CONSTANT;	
	long double ludwNumOfDatapoints = (long double)_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints + 1;
	long double ludwKelvinTemp;
	//long double ludwTempDataC = _gsLogTMP112.fTemp_C_Deg;// has already a //TEMP_C(_gsLogTMP112.fTemp_F_Deg);	
	
	
	
	//temp_gudwActEngeryConst = ludwActivationEngeryConstant;// debug vars for deletion.. mjm 02212014
	//temp_gudwDataPoints = ludwNumOfDatapoints;// debug vars for deletion.. mjm 02212014
	//temp_gudwFtemp = _gsLogTMP112.fTemp_F_Deg;
	
	
	
    //---------------------------------------- 
    // convert C to K Temperature...
    //---------------------------------------- 
    //ludwKelvinTemp = ludwTempDataC + CELSIUS_KELVIN_CONST;    
    ludwKelvinTemp = (_gsLogTMP112.fTemp_F_Deg + FAHRENHEIT_KELVIN_CONST) * (5.0/9.0);
                
    // accumulate the natural logarithmic sum...    
    gudwMKT_Accumulator += exp( -1.0 * (ludwActivationEngeryConstant / ludwKelvinTemp) );

	// calculate running MKT in Kelvin Unit....
	gudwMKT_Value = ludwActivationEngeryConstant / (-1.0 * log(gudwMKT_Accumulator/ludwNumOfDatapoints));
	
	
	
	//temp_gudwMKT_Value = gudwMKT_Value; // debug vars for deletion.. mjm 02212014
	//temp_gudwMKT_Accumulator = gudwMKT_Accumulator; // debug vars for deletion.. mjm 02212014
	//temp_gudwKelvinTemp = ludwKelvinTemp; // debug vars for deletion.. mjm 02212014
	
	
	
	// Temperature Unit conversion 0-C or 1-F
    if(_gsGenCfgOptByte.GenOpt.Byte1.F_C_Unit_Select==0){        
        // Mean Kinetic Temperature to Celsius Unit...
        gudwMKT_Value -= CELSIUS_KELVIN_CONST;        
        
        // update running MKT temp should be always in F degress....
        gfMKT_RunningTemp = TEMP_F(gudwMKT_Value);

    }    
    else{
        // Mean Kinetic Temperature to Fahrenheit Unit...
        gudwMKT_Value = (1.8 * gudwMKT_Value) - FAHRENHEIT_KELVIN_CONST;
        
        gfMKT_RunningTemp = gudwMKT_Value;
    }
    
    // round off to single decimal to correlate the standard decimal MKT Alarm value...
    gfMKT_RunningTemp = devSensor_RoundingOFF_Temp(gfMKT_RunningTemp);

    //temp_gudwRunTemp = gfMKT_RunningTemp; // debug vars for deletion.. mjm 02212014	
   
} 



/*

#define ACT_ENERGY_CONST (double)-9982.64
#define FAR_KELVIN_CONST 459.67    // Kelvin to Fahrenheit constant...
#define CEN_KELVIN_CONST 273.15    // Kelvin to Centigrade constant...

long double gfTemperatureStdDev;
long double gfMKT;

void PDF_Summary_Calculate_StdDevAndMKT(void){    

    long double lfTempXM;
    long double lfTempXMTotal;
    long double lfKelvinTemp;
    long double lfExp;
    long double lfTempVal;
    long double lfDd_CAveTemp;
    
    register UINT16 luwCtr;
    UINT8 lubMark;
    float lfTempData;

    lfTempXMTotal = 0.0;
    lfTempXM = 0.0;
    lfExp = 0.0;    
    
    // turn on RED LED, in large datapoint e.g 16k then it looks the device is 
    // hanged-up because all LED's were turned off but a long calculation is 
    // running in somehow it takes about aleast a 2secs...
    TT4_RED_LED_ON;
    
    // Note: double decimal digit will be available only in C Deg unit, ST marketing requirement... mjm 09252013..
    lfDd_CAveTemp = TEMP_C(_gsLogMinMaxTempData.fAveTemp_F_Deg);

    for(luwCtr=0;luwCtr<_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints;luwCtr++){

        // retrieve F temperature data from EEPROM...        
        commTT4Cfg_CacheMem_RetrieveTempData(luwCtr,&lubMark,&lfTempData);

        //--------------------------------------------- 
        // calculate Temperature standard deviation...
        //--------------------------------------------- 
        
        lfTempVal = (long double)lfTempData;
        
        // Temperature Unit conversion 0-C or 1-F
        if(_gsGenCfgOptByte.GenOpt.Byte1.F_C_Unit_Select==0){
            // X - M  C temperature data...
            //lfTempXM = TEMP_C(lfTempVal) - _gsLogMinMaxTempData.fAveTemp_C_Deg;            
            
            // Note: double decimal digit will be available only in C Deg unit, ST marketing requirement... mjm 09252013..
            if(_gsGenCfgOptByte.GenOpt.Byte1.CDegDoubleDecimal == 1){
                // Double decimal C Deg Temp..
                lfTempXM = TEMP_C(lfTempVal) - lfDd_CAveTemp;
            }
            else{
                // Single decimal C Deg Temp..
                lfTempXM = TEMP_C(lfTempVal) - _gsLogMinMaxTempData.fAveTemp_C_Deg;
            }           
            
        }
        else{
            // X - M  F temperature data...
            lfTempXM = lfTempVal - _gsLogMinMaxTempData.fAveTemp_F_Deg;
        }    
        
        // take the square to eliminate negative sign...
        lfTempXM *= lfTempXM;
        
        // summation of sqaures..
        lfTempXMTotal += lfTempXM;    
        
        
        //---------------------------------------- 
        // calculate Mean Kinetic Temperature...
        //---------------------------------------- 
        lfKelvinTemp = (5.0/9.0) * (lfTempData + FAR_KELVIN_CONST);        
                
        // accumulate the natural logarithmic sum...
        lfExp += exp(ACT_ENERGY_CONST / lfKelvinTemp);
    }
    
    // calculate temperature's standard deviation value...    
    if(_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints>1)
        gfTemperatureStdDev = sqrt(lfTempXMTotal/(_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints-1));            
    else
        gfTemperatureStdDev = 0.0;
    
    gfMKT = ACT_ENERGY_CONST / log(lfExp/_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints);
    
    // Temperature Unit conversion 0-C or 1-F
    if(_gsGenCfgOptByte.GenOpt.Byte1.F_C_Unit_Select==0){        
        // Mean Kinetic Temperature to Centigrade Unit...
        gfMKT -= CEN_KELVIN_CONST;        
    }    
    else{
        // Mean Kinetic Temperature to Centigrade Unit...
        gfMKT = 1.8 * gfMKT - FAR_KELVIN_CONST;        
    }    
}
*/

/*  End of File  */ 
