/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm                                            **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc. and Blue Ocean Innovation Ltd.                            **
 **                                                                           **
 **  Copryright(c) 2010. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Blue Ocean Innovation Ltd.                        **
 **  Project#            :  449                                               **
 **  Creation date       :  08/20/2010                                        **
 **  Programmer          :  Michael J. Mariveles                              **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : coreTT4USBMA.h                                                    **
 **                                                                           **
 **  Description :                                                            **
 **    - this is the TT4USB-MA system core routines.                          **
 **                                                                           **
 ******************************************************************************/
 
#include "hwPIC24F.h"
#include "hwSystem.h"
#include "devI2CLCD_BU9796FS.h"
#include "devI2CTempSensor_TMP112.h"
#include "devSPIFLASH_W25Q80.h"
#include "drvSPI.h"
#include "utility.h"
#include "commTT4Config.h"
#include "USB.h"                      
#include "hwUSB_TT4Tasks.h"
#include "appTTV.h"
#include "appTT4_MA.h"
#include "drvRTC.h"
#include "drvUSB.h"
#include "coreTT4USBMA.h"


#define  START_TIME_DELAY  500


// temperary storage for memory transfer buffer...
extern UINT8   gubMem;
extern UINT16  guwMem;
extern UINT32  gudwMem;



// EEPROM configuration control structure register...
extern __ALARM_CONFIGURATION_AREA      _gsAlarmConfig[6];
extern __SENSITECH_CONFIGURATION_AREA  _gsSensitech_Configuration_Area;
extern __USER_CONFIGURATION_AREA       _gsUser_Configuration_Area;
// EEPROM control bit manipulator structure register...
extern __sBITS_GENCFGOPT_BYTE _gsGenCfgOptByte;
extern __sBITS_EXTOPT_BYTE    _gsExtdOptByte;

// command byte bit manipulator control register....
extern __sBITS_COMMAND_BYTE   _gsCommandByte;

// extreme temperature points global storage....
extern _sLOGMINMAXTEMPDATA    _gsLogMinMaxTempData;

// Timer Interrupt Toggle Flag...
extern volatile UINT8 gubTimerEventFlag;
// Button depress seconds counter..
extern volatile UINT8 gubButtonEventFlag;

extern UINT8 gubLCDSummaryStatus;
extern UINT8 gubAlarmDisplayStatus;

// EEPROM memory size...
extern UINT16 guwEEPROM_MemorySize;

extern UINT8 gubHeartBeatOn;

// monitors the file existence after configuration ....
BOOL gbConfigFileStatusFlag __attribute__((persistent));    //set as persistent for reset recovery
BOOL gbInitMBRStatusFlag __attribute__((persistent));       //set as persistent for reset recovery

// LCD blink toggle bit...
extern UINT8 gbBlinkToggleBit;

// TTV/PDF file write flag... mjm
extern UINT8 gubStartDeviceStatusFlag;
// PDF and TTV file generation status...
extern BOOL gbPDFFileResetStatusFlag;
extern BOOL gbTTVFileResetStatusFlag;
extern BOOL gbUSBFileGenSuccessStatusFlag;

// PDF file generation success status flag...
extern BOOL gbPDFFileGenSuccessStatusFlag;
// TTV file generation success status flag...
extern BOOL gbTTVFileGenSuccessStatusFlag;


// Global USB Descriptor control..
extern _eUSB_Descriptors _geUSB_SetDescriptor;

// configuration was monitoring var...
BOOL gbTT4USB_Config_OK __attribute__((persistent));       //set as persistent for reset recovery

//----------------------------------------------------
// Timer1 counter timer global storage registers..
//----------------------------------------------------

// measurement interval timer counter...
volatile UINT32 gudwMeasurementIntervalTimer __attribute__((persistent));//set as persistent for reset recovery

// Triggered Alarm cycle timer counter...
volatile UINT8 gubTrigAlarmCycleTimer __attribute__((persistent));      //set as persistent for reset recovery
// Heartbeat Icon timer counter ...
volatile UINT8 gubHeartIconTimer __attribute__((persistent));           //set as persistent for reset recovery
// LCD summary timer counter register...
volatile UINT8 gubLCDSummaryTimer;
// Stop button depress delay timer counter register...
volatile UINT8 gubStopButtonDelayTimer;
// Stop button depress for re-configuration...
volatile UINT8 gubStopButtonReConfigTimer;
// Start button depress delay timer counter register...
volatile UINT8 gubStartButtonDelayTimer;
// StartUp Delay timer counter register...
volatile UINT32 gudwStartUpDelayTimer __attribute__((persistent));       //set as persistent for reset recovery
// Configuration mode timer counter register...
volatile UINT8 gubCFModeTimer;

// Button Depress status register...
extern volatile BOOL gbPreviousButtonDepressFlag;
// Button value register...
extern volatile BOOL gbButtonValue;

// PDF global filename storage...
extern char gstrPDFFilename[64];
// TTV global filename storage...
extern char gstrTTVFilename[64];

// Stop mode status register...
UINT8 gubStopDeviceStatus __attribute__((persistent));       //set as persistent for reset recovery

// Blink flag global status flag...
extern BOOL gbUpdateLCDFlag;

// USB plugged/Unplugged status flag...
extern BOOL gbUSBActiveModeStatusFlag;

// USB MSD read/write control register...
extern volatile UINT32 gudwReadBlock;  // mjm
extern volatile UINT32 gudwWriteBlock; // mjm
extern volatile UINT16 guwWriteSector; // mjm 

// PC time is a constant reference time to synchronize startup delay...
extern UINT32 gudwTT4MA_PCTime;
BOOL gbTT4AutoStartActive;
BOOL gbTT4AutoStartExpired;

// core tasking control byte... mjm
__sDEVICETASK _gsTT4Core __attribute__((persistent));       //set as persistent for reset recovery

// Sensor 1 Temperature Calibration Offset...
extern __sSENSOR1_TEMP_CAL_OFFSET  _gsSensor1TempCalOffset;

//reset recovery variables
extern BOOL gbResetRecoveryFlag;

// RAM Memory Current Time global storage for reset recovery
extern volatile UINT32 gudwTT4MA_CurrentTime;
//==============================================================================
//  TT4USB Multi-Alarm Core
//==============================================================================


/*******************************************************************************
**  Function     : coreClearGlobalVars
**  Description  : clears the global array variables... 
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void coreClearGlobalVars(void){

    // clear User configuration Area...
    memset((void *)&_gsUser_Configuration_Area,0,sizeof(__USER_CONFIGURATION_AREA));
    // clear Sensitech Configuration Area...
    memset((void *)&_gsSensitech_Configuration_Area,0,sizeof(__SENSITECH_CONFIGURATION_AREA));
   
    // clear Alarm 1 Configuration Area...
    memset((void *)&_gsAlarmConfig[ALARM_1],0,sizeof(__ALARM_CONFIGURATION_AREA));
    // clear Alarm 1 Configuration Area...
    memset((void *)&_gsAlarmConfig[ALARM_2],0,sizeof(__ALARM_CONFIGURATION_AREA));
    // clear Alarm 1 Configuration Area...
    memset((void *)&_gsAlarmConfig[ALARM_3],0,sizeof(__ALARM_CONFIGURATION_AREA));
    // clear Alarm 1 Configuration Area...
    memset((void *)&_gsAlarmConfig[ALARM_4],0,sizeof(__ALARM_CONFIGURATION_AREA));
    // clear Alarm 1 Configuration Area...
    memset((void *)&_gsAlarmConfig[ALARM_5],0,sizeof(__ALARM_CONFIGURATION_AREA));
    // clear Alarm 1 Configuration Area...
    memset((void *)&_gsAlarmConfig[ALARM_6],0,sizeof(__ALARM_CONFIGURATION_AREA));
    
    
    // clear Extreme Data Points Data Storage...
    memset((void *)&_gsLogMinMaxTempData,0,sizeof(_sLOGMINMAXTEMPDATA));
    // clear General Configuration Options Byte...
    memset((void *)&_gsGenCfgOptByte,0,sizeof(__sBITS_GENCFGOPT_BYTE));
    // clear Extended Options Byte...
    memset((void *)&_gsExtdOptByte,0,sizeof(__sBITS_EXTOPT_BYTE));
    // clear Command Byte...
    memset((void *)&_gsCommandByte,0,sizeof(__sBITS_COMMAND_BYTE));
    // clear USB descriptors...
    memset((void *)&_geUSB_SetDescriptor,0,sizeof(_eUSB_Descriptors));
        
}

/*******************************************************************************
**  Function     : coreInit_LCDDisplayAndLED
**  Description  : initializes LCD display and LED
**  PreCondition : system HW initialization should be done first
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void coreInit_LCDDisplayAndLED(void){
    
    devLCD_Init();
    utilDelay_ms(50); 
    
     //Display version only if unit is in unconfigured state
    commTT4Cfg_ReadCfgEeprom(OFFSET_COMMAND_BYTE,(void *)&gubMem);
    if(gubMem == CMD_BYTE_UNCONFIG)
    {
        TT4_RED_LED_ON;
        TT4_GREEN_LED_ON;

        devLCD_Display_AllSegments(ON_SEGS);
        devLCD_UpdateDisplay();

        utilDelay_ms(1000);

        devLCD_Display_AllSegments(OFF_SEGS);
        devLCD_UpdateDisplay();
        TT4_RED_LED_OFF;
        TT4_GREEN_LED_OFF;

        utilDelay_ms(START_TIME_DELAY);
        devLCD_Display_SwVersion();    
        devLCD_UpdateDisplay();
        utilDelay_ms(START_TIME_DELAY);
    }
}

/*******************************************************************************
**  Function     : coreHwSystemInit 
**  Description  : initializes system HW controls
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void coreHwSystemInit(void){
    
    // force to disable the watch dog timer....
    RCONbits.SWDTEN = 0;
    guwUtilSysClk_mDly = UTIL_MDELAY_SYSCLK_32MHZ; // from POR.. mjm
    
    // must delay 200msecs from POR ,so that the system initilization 
    // will work properly.. 
    // noted by MPLAB to start intialize the device properly!...    
    utilDelay_ms(200);

    //--------------------------------------------------
    // initialize system ...
    //--------------------------------------------------
    hwInit_OSCCtrlRegister();    
    utilDelay_ms(50);        
    hwInit_REFOSCCtrlRegister();    
    utilDelay_ms(50);    
    //hwInit_RESETCtrlRegister();     //skip during reset recovery
    utilDelay_ms(50);    
    hwInit_CPUCtrlRegister();              
    utilDelay_ms(50);    
    hwInit_TimerCtrlRegister();
    utilDelay_ms(50);    
    hwInit_IOPorts();
    utilDelay_ms(50);    
    hwInit_PeripheralPINSel();
    utilDelay_ms(50);        
    hwInit_USBControlRegisters();    
    utilDelay_ms(50);

    //------------------------------
    // Button input control...
    //------------------------------
	TT4_STOP_BUTTON_INIT;
	TT4_START_BUTTON_INIT;
    
    //------------------------------
    // LED output control...
    //------------------------------    
    
    // GREEN LED Output Control..    
    TT4_GREEN_LED_INIT; // configure _RE3 as digital output port...
    Nop();
    TT4_GREEN_LED_OFF;    // GREEN LED OFF...
    Nop();
    
    
    // RED LED Output Control..
    TT4_RED_LED_INIT; // configure _RE2 as digital output port...
    Nop();
    TT4_RED_LED_OFF;    // RED LED OFF...
    Nop();

    //------------------------------    
    // USB VBus port detect
    //------------------------------    
    TT4_USB_VBUS_IO_INIT;
    Nop();

    //------------------------------    
    // TT4 Measurement Port Toggle...
    //------------------------------    
    MI_TOGGLE_PORT_INIT;
    Nop();
    MI_TOGGLE_PORT = 0;   
    
    drvSPI2_Manual_Init();
    Nop();
    
    // 1MB FLASH is in power down in temperature reading mode..            
    // this would wakes up only at the USB mode....
    Nop();
    devFLASH_Manual_PowerDown();     
    Nop();
    utilDelay_ms(100); 
    
    // initialize system peripheral SPI...
    // Note: after initializing the SPI peripheral a manual mode 
    // is not available anymore at this point... mjm
    devFLASH_Init();    
    
    // TT4 Configuration module..
    commTT4Cfg_InitControlVariables();
    commTT4Cfg_InitEeprom();
    
    // initialize Sensor device....
    if(RCON&0x0003)
         devSensor_InitControlVars();    //skip during reset recovery
    devSensor_ResetI2C3();
    devSensor_Init();       
    
    // initialize external RAM device...    
    drvSPI1_Init();
    
    // initialize all LCD and LED sequence...
    coreInit_LCDDisplayAndLED();
    
    
}

/*******************************************************************************
**  Function     : coreDeviceUnconfiguredState
**  Description  : a device mode in unconfigured state
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void coreDeviceUnconfiguredState(void){
    
    register UINT8 lubButtonStatus;
    register BOOL lbCFModeActive;    
    register BOOL lbBLEntryInit;

    // initialize control variables...
    lbCFModeActive = FALSE;    
    lbBLEntryInit = TRUE;
    // set Global USB Descriptor control..
    _geUSB_SetDescriptor = _eMSD_;


    // Timer1 interrupt disabled ...
    appTT4MA_Timer1Interrupt_Disable();    
    // Enable WakeUp Interrupt detection of START,STOP,USB Vbus....
    appTT4MA_WakeUpInterrupts_Enable();
    utilDelay_ms(10);              


    // turn-off LED's                    
    TT4_RED_LED_OFF;
    TT4_GREEN_LED_OFF;
    // cleat LCD display...
    devLCD_Display_AllSegments(OFF_SEGS);
    // Update LCD Display...         
    devLCD_UpdateDisplay();
        
    // trap here...            
    devLCD_AllSegBlinkOn();

    // pre-delay before sleep mode...
    utilDelay_ms(10);

    do{
        //-----------------------------------------------------------------------------
        // to ensure the effecient RTC timing accuracy must clear all interrupt flags 
        // before going to sleep mode...
        // 
        // by: mjmariveles... 12162012..
        //
        //-----------------------------------------------------------------------------        
        // clear button CN interrupt flag...
        IFS1bits.CNIF = 0;    
        // clear button interrupt toggle flag confirmation.. 
        gubButtonEventFlag = 0;         
         
        // clear RTC Timer1 event interrupt flag...
        IFS0bits.T1IF = 0; 
        // clear RTC Timer1 event interrupt toggle flag confirmation.. 
        gubTimerEventFlag = 0; 
        //-----------------------------------------------------------------------------
        
        
        // clear reset control registers preparation for wakeUp...
        RCON = 0; 
        // sleep MCU and wakessUp if any interrupt occuring events...
        Sleep();
        // post-delay after wakeup
        Nop();Nop();Nop();     
        
  //      utilDelay_ms(10);
        // update RTC system counters....
        appTT4MA_RTCSystemCounterUpdate();  
        
        // while in CF mode all button event were blocked!!
        if(lbCFModeActive == FALSE){        
        
            //=============================
            // Detect Button Events...
            //=============================

            // read button input status...
            lubButtonStatus = appTT4MA_GetButtonStatus(); 
                        
            //-----------------------------
            // START_STOP Button Depressed...
            //-----------------------------
            if(lubButtonStatus == BUTTON_STATUS_STR_STP_DEPRESSED){                

                // enable timer interrupt...
                appTT4MA_Timer1Interrupt_Enable();                    
                // disable timer interrupt...
                appTT4MA_WakeUpInterrupts_Disable();
                utilDelay_ms(10);
        
                //----------------------------------------------------
                // Set USB to CDC mode ready for configuration...
                //----------------------------------------------------
                
                // set Global USB Descriptor control..
                _geUSB_SetDescriptor = _eCDC_;

                // disable the All segment continouos blinking....
                devLCD_Init();
                // clear display...
                devLCD_Display_AllSegments(OFF_SEGS);
                // display CF...
                devLCD_Display_CF(1);
                // Update LCD Display...         
                devLCD_UpdateDisplay();                
                
                // reset timer...
                gubCFModeTimer = 0;
                // if CF is active then prevent all button depression event...
                lbCFModeActive = TRUE;
        
            }            

            //-----------------------------
            // STOP Button Depressed...
            //-----------------------------
            if(lubButtonStatus == BUTTON_STATUS_STOP_DEPRESSED){
                
                if(lbBLEntryInit==TRUE){
                    lbBLEntryInit = FALSE;
                    // reset timer for counting-up...
                    gubStopButtonDelayTimer = 0;

                    // enable timer interrupt...
                    appTT4MA_Timer1Interrupt_Enable();                    
                    // disable timer interrupt...
                    appTT4MA_WakeUpInterrupts_Disable();
                    utilDelay_ms(10);
                }
        
                if(gubStopButtonDelayTimer >= BOOTLOAD_BUTTON_DELAY_TIMEOUT){ 

                    //--------------------------
                    // Assert Bootloader mode...
                    //--------------------------
                    
                    // disable the All segment continouos blinking....
                    devLCD_Init();
                    // clear display...
                    devLCD_Display_AllSegments(OFF_SEGS);
                    // display USB...
                    devLCD_Display_USB(1); 
                    // update LCD segments... 
                    devLCD_UpdateDisplay();
                    
                    // reset BL timer....
                    gubStopButtonDelayTimer = 0;
                    
                    //----------------------------------------------------
                    // trap the STOP button bootloader event here!...
                    //----------------------------------------------------
                    do{
                        
                        utilDelay_ms(1000);
                        // update RTC system counters....
                        appTT4MA_RTCSystemCounterUpdate();  
                        
                        // read button input status if the user still holding the Stop Button...
                        if(appTT4MA_GetButtonStatus()!=BUTTON_STATUS_STOP_DEPRESSED) break; 
                        
                        // at this point the user must still hold the Stop Button until plugged to the USB...        
                        if(TT4_USB_VBUS_IO == USB_PORT_PLUGGED){ 
                                        
                            // Steps...                                        
                            // 1 - Press Hold STOP Button if the 449 display appears....
                            // 2 - Plug To USB (don't release START until LED flashing alternately)....
                            // 3 - RUN the PC Software BootLoader 
                            // 4 - Locate the HEX file ...
                            // 5 - Click Program Device...

                            // disable Timer1 interrupt for seconds counter...
                            appTT4MA_Timer1Interrupt_Disable();                           
                                        
                            // Disable Start Button and Vbus detection WakeUp Interrupt....
                            appTT4MA_WakeUpInterrupts_Disable();
                                            
                            // clear display...
                            devLCD_Display_AllSegments(OFF_SEGS);
                            // display bL...
                            devLCD_Display_Boot(1);          
                            // update LCD segments... 
                            devLCD_UpdateDisplay();                              
                            
                            utilDelay_ms(200);
                                                                   
                            // the user should press hold till the device resets and 2 LED lit on!...mjm
                            // if the 2 the LED is on then still do not release the button till you plugged to the USB 
                            // and now you are in Bootloader Mode... mjm
                            RCON = 0x00;
                            __asm__("reset");                                                
                        }
                                
                    }while(gubStopButtonDelayTimer <= BOOTLOAD_EXIT_TIMEOUT); // 5secs time-out timer...                          
                    
                    // Here, the bootloader was terminated!... then...
                    // set the BL flag if ever the user will attempt to depress again the STOP Button!
                    lbBLEntryInit = TRUE;
                    // set Global USB Descriptor control..
                    _geUSB_SetDescriptor = _eMSD_;                    
                    // set to LCD to unconfigured mode display...            
                    devLCD_AllSegBlinkOn();                                           

                    // disable timer interrupt...
                    appTT4MA_Timer1Interrupt_Disable();                    
                    // enable timer interrupt...
                    appTT4MA_WakeUpInterrupts_Enable();
                    utilDelay_ms(10);

                }
            }

            //-----------------------------
            // STOP Button Released...
            //-----------------------------
            if(lubButtonStatus == BUTTON_STATUS_STOP_RELEASED){
        
                // set Global USB Descriptor control..
                _geUSB_SetDescriptor = _eMSD_;
                // set the BL flag if ever the user will attempt to depress again the STOP Button!
                lbBLEntryInit = TRUE;
                // set to LCD to unconfigured mode display...            
                devLCD_AllSegBlinkOn();                       
                
                // disable timer interrupt...
                appTT4MA_Timer1Interrupt_Disable();                    
                // enable timer interrupt...
                appTT4MA_WakeUpInterrupts_Enable();
                utilDelay_ms(10);
                
            }
        
        }
        else{
        
            if(gubCFModeTimer >= CDC_EXIT_TIMEOUT){                 
                // reset BL..
                lbBLEntryInit = TRUE;
                // reset CF ... 
                lbCFModeActive = FALSE;
                // set Global USB Descriptor control..
                _geUSB_SetDescriptor = _eMSD_;
                // set to LCD to unconfigured mode display...            
                devLCD_AllSegBlinkOn();                                       
                
                // disable timer interrupt...
                appTT4MA_Timer1Interrupt_Disable();                    
                // enable timer interrupt...
                appTT4MA_WakeUpInterrupts_Enable();
                utilDelay_ms(10);
            }    
        }
        
        
        
        
        // if Vbus detected then identify which USB modes....
        if(TT4_USB_VBUS_IO == USB_PORT_PLUGGED){                    

            // disable timer interrupt...
            appTT4MA_Timer1Interrupt_Disable();
            // enable timer interrupt...
            appTT4MA_WakeUpInterrupts_Disable();
                                
            // Call USB Device function status....
            hwUSBTT4Tasks();            

            // make it sure the residual voltage at the Vbus should be discharged....
            while(TT4_USB_VBUS_IO != USB_PORT_UNPLUGGED);                            
            
            // if previously the USB was in CF mode?... 
            if(lbCFModeActive == TRUE){

                // Note: There are 2 conditions will happen after configuration mode...
                //       1 - if the configuration is successful then it will goes back to Device Sleep Mode status...
                //       2 - if the configuration is not successful then it will reset the device and goes back to Device Unconfigured status...
                // \ mjmariveles...
                
                // reset CF ... 
                lbCFModeActive = FALSE;
                
                if( (_gsSensitech_Configuration_Area.ubCommandByte == CMD_BYTE_SLEEP) || (_gsSensitech_Configuration_Area.ubCommandByte == CMD_BYTE_RUNNING) ){
                    // exit device unconfigured loop....
                    break;
                }     
            
            }            
            
           // reset BL..
           lbBLEntryInit = TRUE;
           // reset CF ... 
           lbCFModeActive = FALSE;
           // set Global USB Descriptor control..
           _geUSB_SetDescriptor = _eMSD_;
           // set to LCD to unconfigured mode display...            
           devLCD_AllSegBlinkOn();        

           // enable wakeup interrupt...
           appTT4MA_WakeUpInterrupts_Enable();
           utilDelay_ms(10);
                    
        }        

    }while(INFINITE_LOOP); // infinite UnConfigured Mode Loop!...
    
}


/*******************************************************************************
**  Function     : coreDeviceSleepModeState
**  Description  : a device mode in sleep mode state
**  PreCondition : the device must be configured prior calling sleep mode
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void coreDeviceSleepModeState(void){
    
    register UINT8 lubButtonStatus;
    register BOOL lbCFModeActive;
    register BOOL lbStartModeActive;

    // clear LCD display...
    devLCD_Display_AllSegments(OFF_SEGS);    
    // load Segment register and send to I2C ...
    devLCD_UpdateDisplay();

    // Enable WakeUp Interrupt detection of START,STOP,USB Vbus....
    appTT4MA_WakeUpInterrupts_Enable();
    utilDelay_ms(10);

    // initialize control variables...
    lbCFModeActive = FALSE;
    lbStartModeActive = TRUE;
    // set Global USB Descriptor control..
    _geUSB_SetDescriptor = _eMSD_;
    
    do{
        //-----------------------------------------------------------------------------
        // to ensure the effecient RTC timing accuracy must clear all interrupt flags 
        // before going to sleep mode...
        // 
        // by: mjmariveles... 12162012..
        //
        //-----------------------------------------------------------------------------        
        // clear button CN interrupt flag...
        IFS1bits.CNIF = 0;    
        // clear button interrupt toggle flag confirmation.. 
        gubButtonEventFlag = 0;         
         
        // clear RTC Timer1 event interrupt flag...
        IFS0bits.T1IF = 0; 
        // clear RTC Timer1 event interrupt toggle flag confirmation.. 
        gubTimerEventFlag = 0; 
        //-----------------------------------------------------------------------------
        
        // clear reset control registers preparation for wakeUp...
        RCON = 0;

        //This is a part of the reset recovery.
        //It takes about 2 seconds to execute bootloader and initialization codes.
        //If the unit is in sleep mode and the user presses the start button within 2 seconds from reset,
        //do not go to sleep immediately because the unit will not detect the button press and will not
        //wake up unless the timer interrupt is initiated or the start button is released.
        if(gbResetRecoveryFlag == FALSE)
            // sleep MCU and wakessUp if any interrupt occuring events...
            Sleep();
        
        //clear reset recovery flag
        gbResetRecoveryFlag = FALSE;
        
        // post-delay after wakeup
        Nop();Nop();Nop();        
        
    //    utilDelay_ms(10);
        // update RTC system counters....
        appTT4MA_RTCSystemCounterUpdate();          

        // while in CF mode all button event were blocked!!
        if(lbCFModeActive == FALSE){        
        
            // read button input status...
            lubButtonStatus = appTT4MA_GetButtonStatus(); 
        
            //=============================
            // Detect Button Events...
            //=============================
                        
            //-----------------------------
            // START_STOP Button Depress...
            //-----------------------------
            if(lubButtonStatus == BUTTON_STATUS_STR_STP_DEPRESSED){

                // enable timer interrupt...
                appTT4MA_Timer1Interrupt_Enable();                    
                // disable wakeup interrupt...
                appTT4MA_WakeUpInterrupts_Disable();
                utilDelay_ms(10);
        
                //----------------------------------------------------
                // Set USB to CDC mode ready for configuration...
                //----------------------------------------------------
                
                // set Global USB Descriptor control..
                _geUSB_SetDescriptor = _eCDC_;
                // display CF...
                devLCD_Display_CF(1);
                // Update LCD Display Task...
                devLCD_UpdateDisplay(); 
                
                // reset timer...
                gubCFModeTimer = 0;
                // if CF is active then prevent all button depression event...
                lbCFModeActive = TRUE;        
            }            

            //-----------------------------
            // START Button Depressed...
            //-----------------------------
            if(lubButtonStatus == BUTTON_STATUS_START_DEPRESSED){
        
                // initiate start key 3 secs delay... mjm
                if(_gsGenCfgOptByte.GenOpt.Byte1.StartKeyDlyEna==1){
                    
                    if(lbStartModeActive==TRUE){
                        // enable timer interrupt...
                        appTT4MA_Timer1Interrupt_Enable();                    
                        // disable wakeup interrupt...
                        appTT4MA_WakeUpInterrupts_Disable();
                        utilDelay_ms(10);

                        lbStartModeActive=FALSE;
                        // reset Start button timer...
                        gubStartButtonDelayTimer = 0;
                    }
                
                    if(gubStartButtonDelayTimer >= START_BUTTON_DELAY_TIMEOUT){
                        // exit sleep mode loop...
                        break;
                    }     
                }
                else{
                    // here, no delay and exit sleep mode immediately...
                    // exit sleep mode loop...
                    break;
                }
            } 
            
            //-----------------------------
            // START Button Released...
            //-----------------------------
            if(lubButtonStatus == BUTTON_STATUS_START_RELEASED){
                // initiate start key 3 secs delay... mjm
                if(_gsGenCfgOptByte.GenOpt.Byte1.StartKeyDlyEna==1){                     
                    lbStartModeActive = TRUE;                    
                    // disable timer interrupt...
                    appTT4MA_Timer1Interrupt_Disable();
                    // enable wakeup interrupt...
                    appTT4MA_WakeUpInterrupts_Enable();   
                    utilDelay_ms(10);             
                }    
            }
        }
        else{
        
            if(gubCFModeTimer >= CDC_EXIT_TIMEOUT){                 
                // reset CF ... 
                lbCFModeActive = FALSE;
                // set Global USB Descriptor control..
                _geUSB_SetDescriptor = _eMSD_;               
                // clear display...
                devLCD_Display_AllSegments(OFF_SEGS);                 
                // Update LCD Display Task...
                devLCD_UpdateDisplay(); 
                
                // disable timer interrupt...
                appTT4MA_Timer1Interrupt_Disable();
                // enable wakeup interrupt...
                appTT4MA_WakeUpInterrupts_Enable();  
                utilDelay_ms(10);              
            }    
        }
        
        // if Vbus detected then identify which USB modes....
        if(TT4_USB_VBUS_IO == USB_PORT_PLUGGED){                    
      
            // enable timer interrupt...
            appTT4MA_Timer1Interrupt_Disable();                                
            // disable wakeup interrupt...
            appTT4MA_WakeUpInterrupts_Disable();
            utilDelay_ms(10);
                                
            // Call USB Device function status....
            hwUSBTT4Tasks();            
            
            // make it sure the residual voltage at the Vbus should be discharged....
            while(TT4_USB_VBUS_IO != USB_PORT_UNPLUGGED);                            
            
            // if previously the USB was in CF mode?... 
            if(lbCFModeActive == TRUE){

                // Note: There are 2 conditions will happen after configuration mode...
                //       1 - if the configuration is successful then it will goes back to Device Sleep Mode status...
                //       2 - if the configuration is not successful then it will reset the device and goes back to Device Unconfigured status...
                // \ mjmariveles...
                
                // reset CF ... 
                lbCFModeActive = FALSE;
                
                if(gbTT4USB_Config_OK == TRUE){
                
                    // check if the command byte is in running mode...
                    if((_gsSensitech_Configuration_Area.ubCommandByte == CMD_BYTE_RUNNING)||(_gsSensitech_Configuration_Area.ubCommandByte == CMD_BYTE_SLEEP)){
                        // exit sleep mode loop....
                        break;
                    }
                    // check if the command byte is in sleep mode otherwise system reset...              
                    else{  
                        // if the configuration settings not successful then reset and go back to Unconfigured State!...mjm                                    
                        RCON = 0x00;
                        __asm__("reset");                
                    }    
                
                }  
                 
            }            
            
            // reset start button...
            lbStartModeActive = TRUE;
            // reset CF ... 
            lbCFModeActive = FALSE;
            // set Global USB Descriptor control..
            _geUSB_SetDescriptor = _eMSD_;                        
            // clear display...
            devLCD_Display_AllSegments(OFF_SEGS);
            // Update LCD Display Task...
            devLCD_UpdateDisplay(); 
            // enable wakeup interrupt...
            appTT4MA_WakeUpInterrupts_Enable();              
        }        

    }while(INFINITE_LOOP); // infinite UnConfigured Mode Loop!...

}

/*******************************************************************************
**  Function     : coreDeviceStartUpDelayState
**  Description  : a device mode in startup delay state
**  PreCondition : the device must be configured prion entering this state
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void coreDeviceStartUpDelayState(void){

    register UINT8 lubButtonStatus;
    register BOOL lbCFModeActive;
    register UINT32 ludwRtcTime;
    register UINT32 ludwRemainingStartUpTime;

    // initialize control variables...
    lbCFModeActive = FALSE;    
    // set Global USB Descriptor control..
    _geUSB_SetDescriptor = _eMSD_;
    
    // Configuration status flag...
    gbTT4USB_Config_OK = FALSE;


    gudwStartUpDelayTimer = 0;
    gubTimerEventFlag = 0;

    // disable all interrupts when entering USB mode due to clock switching...
    appTT4MA_WakeUpInterrupts_Enable();   
    
    // enable Timer1 interrupt for seconds counter...
    appTT4MA_Timer1Interrupt_Enable();
    
    utilDelay_ms(10);

    // get the RTCC system clock actual time...
    // Note: 1.) the global current time in seconds counter will be updated
    //           and synchronized in RTCC system clock....
    //       2.) this is the starting reference time from the Sleep Mode...
    //
    // \mjmariveles.....
    //drvRTC_ReadRTCC_UpdateCurrentTimeSecs();        
    
    // check if the Autostart Time is greater than the PC current time that was being set
    // by the Turbo validate during configuring the device...
    if(gbTT4AutoStartActive==TRUE){
        
        // get the accomulated time...
        ludwRtcTime = drvRTC_ReadRTCC_CurrentTime();

        // validate startup delay time if greater than the RTC time...
        if((_gsUser_Configuration_Area.udwStartUpDelay+gudwTT4MA_PCTime) <= ludwRtcTime){             
            // Autostart time already expired when entering Autostart mode...
            gbTT4AutoStartExpired = TRUE;
            // clear display...
            devLCD_Display_AllSegments(OFF_SEGS);       
            //  Update LCD Display Task...     
            devLCD_UpdateDisplay();
            return;
        }
        else{
            // Autostart time not yet expired when entering Autostart mode...
            gbTT4AutoStartExpired = FALSE;
            // Auto StartUp Delay mode...
            ludwRemainingStartUpTime = (_gsUser_Configuration_Area.udwStartUpDelay + gudwTT4MA_PCTime) - ludwRtcTime;
        }
    }
    else{
        // manual StartUp Delay mode...
        ludwRemainingStartUpTime = _gsUser_Configuration_Area.udwStartUpDelay;
    }
    
    // clear display...
    devLCD_Display_AllSegments(OFF_SEGS);
    // display hourglass Icon if enabled!...
    devLCD_Display_StartUPIcon(_gsGenCfgOptByte.GenOpt.Byte0.StartUpDlyIcoEna);
    //  Update LCD Display Task...     
    devLCD_UpdateDisplay();    

    // pre-delay before sleep mode...
    utilDelay_ms(10);
    
    do{
        //-----------------------------------------------------------------------------
        // to ensure the effecient RTC timing accuracy must clear all interrupt flags 
        // before going to sleep mode...
        // 
        // by: mjmariveles... 12162012..
        //
        //-----------------------------------------------------------------------------        
        // clear button CN interrupt flag...
        IFS1bits.CNIF = 0;    
        // clear button interrupt toggle flag confirmation.. 
        gubButtonEventFlag = 0;         
         
        // clear RTC Timer1 event interrupt flag...
        IFS0bits.T1IF = 0; 
        // clear RTC Timer1 event interrupt toggle flag confirmation.. 
        gubTimerEventFlag = 0; 
        //-----------------------------------------------------------------------------
        
        // clear reset control registers preparation for wakeUp...
        RCON = 0; 
        // sleep MCU and wakessUp if any interrupt occuring events...
        Sleep();
        // post-delay after wakeup
        Nop();Nop();Nop(); 
        
   //     utilDelay_ms(10);
        // update RTC system counters....
        appTT4MA_RTCSystemCounterUpdate();  
        
        //========================================================================
        //  Timer Event Interrupt Flag... 
        //========================================================================
        if(gubTimerEventFlag == 1){
            gubTimerEventFlag = 0;
            // check the blink mode if enabled!...
            devLCD_GetBlinkStatus();        
       //     gbUpdateLCDFlag = TRUE;
        }     
           
        // while in CF mode all button event were blocked!!
        if(lbCFModeActive == FALSE){        

            //=============================
            // Detect Button Events...
            //=============================
        
            // read button input status...
            if(gubButtonEventFlag==1){
                lubButtonStatus = appTT4MA_GetButtonStatus(); 
                // clear global button event interrupt flag...
                gubButtonEventFlag = 0;
            }
                
            //-----------------------------
            // START_STOP Button Depress...
            //-----------------------------
            if(lubButtonStatus == BUTTON_STATUS_STR_STP_DEPRESSED){

                //----------------------------------------------------
                // Set USB to CDC mode ready for configuration...
                //----------------------------------------------------
                
                // set Global USB Descriptor control..
                _geUSB_SetDescriptor = _eCDC_;
                // clear display...
                devLCD_Display_AllSegments(OFF_SEGS);
                // display CF...
                devLCD_Display_CF(1);
                // load Segment register...
                devLCD_UpdateDisplay();
                // reset timer...
                gubCFModeTimer = 0;
                // if CF is active then prevent all button depression event...
                lbCFModeActive = TRUE;        
            }            


        }
        else{
        
            if(gubCFModeTimer >= CDC_EXIT_TIMEOUT){     
                
                //reset button event interrupt flag...
                gubButtonEventFlag = 0;
                lubButtonStatus = BUTTON_STATUS_NOT_DEPRESSED;
                
                // reset CF ... 
                lbCFModeActive = FALSE;
                // set Global USB Descriptor control..
                _geUSB_SetDescriptor = _eMSD_;               
                // clear display...
                devLCD_Display_AllSegments(OFF_SEGS);                 
                // display hourglass Icon if enabled!...
                devLCD_Display_StartUPIcon(_gsGenCfgOptByte.GenOpt.Byte0.StartUpDlyIcoEna);                
                // load Segment register...
                devLCD_UpdateDisplay();                
            }    
        }
        
        
        if(lbCFModeActive == FALSE){
            //  Update LCD Display Task...         
            if(gbUpdateLCDFlag == TRUE){            
                // system running icon...
                devLCD_Display_RunningIcon();
                // load Segment register...
                devLCD_UpdateDisplay();
            }    
        }    
        
        // allow only CDC mode ...
        if(lbCFModeActive == TRUE){
        
            // if Vbus detected then identify which USB modes....
            if(TT4_USB_VBUS_IO == USB_PORT_PLUGGED){                    

                //reset button event interrupt flag...
                gubButtonEventFlag = 0;
                lubButtonStatus = BUTTON_STATUS_NOT_DEPRESSED;

                // disable timer interrupt...
                appTT4MA_Timer1Interrupt_Disable(); 
                // disable wakeup interrupt...
                appTT4MA_WakeUpInterrupts_Disable();
                utilDelay_ms(10);
                                
                // Call USB Device function status....  
                hwUSBTT4Tasks();            
     
                // make it sure the residual voltage at the Vbus should be discharged....
                while(TT4_USB_VBUS_IO != USB_PORT_UNPLUGGED);                
            
                // if previously the USB was in CF mode?... 
                if(gbTT4USB_Config_OK == TRUE){

                    // Note: There are 2 conditions will happen after configuration mode...
                    //       1 - if the configuration is successful then it will goes back to Device Sleep Mode status...
                    //       2 - if the configuration is not successful then it will reset the device and goes back to Device Unconfigured status...
                    // \ mjmariveles...
                                
                    // check if the command byte is in running mode...
                    if((_gsSensitech_Configuration_Area.ubCommandByte == CMD_BYTE_RUNNING)||(_gsSensitech_Configuration_Area.ubCommandByte == CMD_BYTE_SLEEP)){
                        // exit sleep mode loop....
                        break;
                    }
                    else{ 
                        // if the configuration settings not successful then reset and go back to Unconfigured State!...mjm                                    
                        RCON = 0x00;
                        __asm__("reset");                
                    }                       
                }            
            
                // reset CF ... 
                lbCFModeActive = FALSE;
                // set Global USB Descriptor control..
                _geUSB_SetDescriptor = _eMSD_;                        
                // clear display...
                devLCD_Display_AllSegments(OFF_SEGS);
                // display hourglass Icon if enabled!...
                devLCD_Display_StartUPIcon(_gsGenCfgOptByte.GenOpt.Byte0.StartUpDlyIcoEna);
                // load Segment register...
                devLCD_UpdateDisplay();
                // enable timer interrupt...
                appTT4MA_Timer1Interrupt_Enable();
                // enable wakeup interrupt...
                appTT4MA_WakeUpInterrupts_Enable();
                utilDelay_ms(10);
            }                
        }
    
    }while(gudwStartUpDelayTimer <= ludwRemainingStartUpTime);
    

    // clear display...
    devLCD_Display_AllSegments(OFF_SEGS);       
    //  Update LCD Display Task...     
    devLCD_UpdateDisplay();
}





/*******************************************************************************
**  Function     : coreDeviceStopMode
**  Description  : a device mode in stop
**  PreCondition : this mode will be called only when running the device 
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void coreDeviceStopMode(void){

    UINT8 lubButtonStatus;     
    BOOL lbButtonOnePass;
    BOOL lbButtonResetAlarmMode;

    // clear all segments...    
    devLCD_Display_AllSegments(OFF);

    // USB flag in MSD mode...                  
    devLCD_Display_StopAlarmDevice(1); 

    // Update LCD Display Task...     
    devLCD_UpdateDisplay();

    // set Global USB Descriptor control..
    _geUSB_SetDescriptor = _eMSD_;        

    if(gubStopDeviceStatus == _DEVICE_STOP_STATUS_GO_USB_){
        
        // preventing multi-drop in false operation then
        // Multi-drop will is no longer operable in the 
        // Device was asserted in stop mode... mjm
        gubStopDeviceStatus=_DEVICE_STOP_STATUS_SLEEP_;
            
        // USB MSD mode...
        hwUSBTT4Tasks();        
        
        // make it sure the residual voltage at the Vbus should be discharged....
        while(TT4_USB_VBUS_IO != USB_PORT_UNPLUGGED);        
    } 
        
    utilDelay_ms(10);
    
    // initlialize process task flag...     
    _gsTT4Core.Status = 0;
    // Configuration status flag...
    gbTT4USB_Config_OK = FALSE;
    
    gubLCDSummaryStatus = 0;
    
    lbButtonOnePass = TRUE;
    
    //reset button event interrupt flag...
    gubButtonEventFlag = 0;
    lubButtonStatus = BUTTON_STATUS_NOT_DEPRESSED;
    
        
    if((_gsExtdOptByte.ExtOpt.Byte2.DispAlrmNumTrgAlrm == 1)&&(_gsUser_Configuration_Area.ubAlarmStatus & 0x3F)){
        if(_gsUser_Configuration_Area.ubAlarmStatus & 0x3F){
            // activate timer interrupt....
            appTT4MA_Timer1Interrupt_Enable();   
            lbButtonResetAlarmMode = FALSE;
        }
    }
    else{
        // de-activate timer interrupt....
        appTT4MA_Timer1Interrupt_Disable();
        lbButtonResetAlarmMode = TRUE;
    }
    
    
    // enable wakeup interrupt always enabled...
    appTT4MA_WakeUpInterrupts_Enable();
    // pre-delay before sleep mode...
    utilDelay_ms(10);
    
    do{ 
        MI_TOGGLE_PORT = 0; // debug port high...    
        
        //-----------------------------------------------------------------------------
        // to ensure the effecient RTC timing accuracy must clear all interrupt flags 
        // before going to sleep mode...
        // 
        // by: mjmariveles... 12162012..
        //
        //-----------------------------------------------------------------------------        
        // clear button CN interrupt flag...
        IFS1bits.CNIF = 0;    
        // clear button interrupt toggle flag confirmation.. 
        gubButtonEventFlag = 0;         
         
        // clear RTC Timer1 event interrupt flag...
        IFS0bits.T1IF = 0; 
        // clear RTC Timer1 event interrupt toggle flag confirmation.. 
        gubTimerEventFlag = 0; 
        //-----------------------------------------------------------------------------
        
        // clear reset control registers preparation for wakeUp...
        RCON = 0; 
        // sleep MCU and wakessUp if any interrupt occuring events...
        Sleep();
        // post-delay after wakeup
        Nop();Nop();Nop();
        
 //       utilDelay_ms(10);
        // update RTC system counters....
        appTT4MA_RTCSystemCounterUpdate();          
        
        MI_TOGGLE_PORT = 1; // debug port high...    

       if((lubButtonStatus == BUTTON_STATUS_STOP_DEPRESSED) &&(_gsGenCfgOptByte.GenOpt.Byte0.StopReConfigEna == 1))
           while(gubTimerEventFlag==0);
       else
           gubStopButtonReConfigTimer = 0;

        //========================================================================
        //  Timer Event Interrupt Flag... 
        //========================================================================
        if(gubTimerEventFlag == 1){     

            // CF is dominant so if thisis enabled then all periodic should be disabled temporarily!...
            if(_gsTT4Core.Task.Proc_CfgDevice != PROC_BUSY){
                
                //------------------------------
                // LCD Summary time-out....
                //------------------------------
                if(_gsTT4Core.Task.Req_LCDSummary == REQ_ACTIVE){
                    
                    if(_gsGenCfgOptByte.GenOpt.Byte1.LCDSumDatDispEna == 1){
                        
                        _gsTT4Core.Task.Proc_LCDSummary = PROC_BUSY;
                
                        if(gubLCDSummaryTimer >= LCD_SUMMARY_EXIT_TIMEOUT){	                    
                            
                            if((_gsExtdOptByte.ExtOpt.Byte2.DispAlrmNumTrgAlrm != 1)||((_gsExtdOptByte.ExtOpt.Byte2.DispAlrmNumTrgAlrm == 1)&&(_gsUser_Configuration_Area.ubAlarmStatus <= 0))){
                                // activate timer interrupt....
                                appTT4MA_Timer1Interrupt_Disable();
                                lbButtonResetAlarmMode = TRUE;
                            } 
                    
                            // restore to Main LCD status display.... mjm                                
                            // clear all segments...    
                            devLCD_Display_AllSegments(OFF);
                            // stop mode icons...
                            devLCD_Display_StopAlarmDevice(1);                             
                            // Update LCD Display Task...     
                            devLCD_UpdateDisplay();

                            // reset global LCD summary to start...
                            gubLCDSummaryStatus = 0;                                              
                            
                            // clear LCD summary task flag... 
                            _gsTT4Core.Task.Req_LCDSummary = REQ_DONE;
                            _gsTT4Core.Task.Proc_LCDSummary = PROC_DONE;
                        }
                    }   
                }     
            
                //------------------------------------------
                // Triggered Alarm Cycling time-out....
                //------------------------------------------                                
                if(_gsExtdOptByte.ExtOpt.Byte2.DispAlrmNumTrgAlrm == 1){
                    
                    if(_gsTT4Core.Task.Req_LCDSummary == REQ_DONE){
                        
                        if(gubTrigAlarmCycleTimer >= ALARM_CYCLE_TIMEOUT){    
                            gubTrigAlarmCycleTimer = 0;                        
                            devLCD_Display_CyclingTriggeredAlarm();
                        }                
                    }
                }
            
            } /* CDC dominant control region... */

            //------------------------------
            // CDC Mode time-out
            //------------------------------
            if(_gsTT4Core.Task.Req_CfgDevice == REQ_ACTIVE){
                
                _gsTT4Core.Task.Proc_CfgDevice = PROC_BUSY;
                
                if(gubCFModeTimer >= CDC_EXIT_TIMEOUT){

                    if((_gsExtdOptByte.ExtOpt.Byte2.DispAlrmNumTrgAlrm != 1)||((_gsExtdOptByte.ExtOpt.Byte2.DispAlrmNumTrgAlrm == 1)&&(_gsUser_Configuration_Area.ubAlarmStatus <= 0))){
                        // activate timer interrupt....
                        appTT4MA_Timer1Interrupt_Disable();
                        lbButtonResetAlarmMode = TRUE;
                    } 

                    //reset button event interrupt flag...
                    gubButtonEventFlag = 0;
                    lubButtonStatus = BUTTON_STATUS_NOT_DEPRESSED;
                
                    // set Global USB Descriptor control..
                    _geUSB_SetDescriptor = _eMSD_;               
                    // clear display...
                    devLCD_Display_AllSegments(OFF_SEGS);
                    // stop mode icons...
                    devLCD_Display_StopAlarmDevice(1);                     
                    // Update LCD Display Task...     
                    devLCD_UpdateDisplay();
                    
                    // reset CF ...                 
                    _gsTT4Core.Task.Req_CfgDevice = REQ_DONE;
                    _gsTT4Core.Task.Proc_CfgDevice = PROC_DONE;                    
                }   
            }
            
        }  /*  - End of 1Hz periodic tasks -  */
        

        //========================================================================
        //
        //  User Request Button Event or USB Event ... < Real Time Tasks > ....
        //
        //========================================================================     
            
        // while in CF mode all button event were blocked!!
        if(_gsTT4Core.Task.Proc_CfgDevice != PROC_BUSY){        
            
            // read button input status...
            if((gubButtonEventFlag==1) || (lubButtonStatus != BUTTON_STATUS_NOT_DEPRESSED))
                lubButtonStatus = appTT4MA_GetButtonStatus();
        
            //==================================================================
            // Detect Button Events...
            //==================================================================
                        
            //-----------------------------
            // START_STOP Button Depress...
            //-----------------------------
            if(lubButtonStatus == BUTTON_STATUS_STR_STP_DEPRESSED){

                if(_gsTT4Core.Task.Proc_LCDSummary != PROC_BUSY){

                    //----------------------------------------------------
                    // Set USB to CDC mode ready for configuration...
                    //----------------------------------------------------                
                  
                    // valid only in no alarm mode...
                    if(lbButtonResetAlarmMode == TRUE){
                        lbButtonResetAlarmMode = FALSE;
                        if((_gsExtdOptByte.ExtOpt.Byte2.DispAlrmNumTrgAlrm != 1)||((_gsExtdOptByte.ExtOpt.Byte2.DispAlrmNumTrgAlrm == 1)&&(_gsUser_Configuration_Area.ubAlarmStatus <= 0))){
                            // activate timer interrupt....
                            appTT4MA_Timer1Interrupt_Enable();                               
                        }                            
                    }
                
                    // set Global USB Descriptor control..
                    _geUSB_SetDescriptor = _eCDC_;
                    // clear all segments...    
                    devLCD_Display_AllSegments(OFF);                    
                    // display CF...
                    devLCD_Display_CF(1);
                    // Update LCD Display Task...     
                    devLCD_UpdateDisplay();
                    // reset timer...
                    gubCFModeTimer = 0;
                    // if CF is active then prevent all button depression event...                
                    // set tasks flag...
                    _gsTT4Core.Task.Req_CfgDevice = REQ_ACTIVE;
                    _gsTT4Core.Task.Proc_CfgDevice = PROC_BUSY;
                }
                
            }  /* End of START/STOP button depress control events */          
            
            
            //-----------------------------
            // START Button Depressed...
            //-----------------------------
            if(lubButtonStatus == BUTTON_STATUS_START_DEPRESSED){
            
                if(lbButtonOnePass==TRUE){                    
                    lbButtonOnePass=FALSE;
                    
                    // LCD summary bit if enabled!...
                    if(_gsGenCfgOptByte.GenOpt.Byte1.LCDSumDatDispEna == 1){

                        // valid only in no alarm mode...
                        if(lbButtonResetAlarmMode == TRUE){
                            lbButtonResetAlarmMode = FALSE;
                            if((_gsExtdOptByte.ExtOpt.Byte2.DispAlrmNumTrgAlrm != 1)||((_gsExtdOptByte.ExtOpt.Byte2.DispAlrmNumTrgAlrm == 1)&&(_gsUser_Configuration_Area.ubAlarmStatus <= 0))){
                                // activate timer interrupt....
                                appTT4MA_Timer1Interrupt_Enable();                               
                            }                            
                        }

                            
                        // set task status flag...    
                        _gsTT4Core.Task.Req_LCDSummary = REQ_ACTIVE;                        
                            
                        // reset LCD summary timer every time when button is depressed...
                        gubLCDSummaryTimer = 0;
    
                   	    if(devLCD_Display_Summary() == LCD_SUMMARY_DONE){

                            if((_gsExtdOptByte.ExtOpt.Byte2.DispAlrmNumTrgAlrm != 1)||((_gsExtdOptByte.ExtOpt.Byte2.DispAlrmNumTrgAlrm == 1)&&(_gsUser_Configuration_Area.ubAlarmStatus <= 0))){
                                // activate timer interrupt....
                                appTT4MA_Timer1Interrupt_Disable();
                                lbButtonResetAlarmMode = TRUE;
                            } 
                                    
                            // restore to Main LCD status display.... mjm
                            // clear all segments...    
                            devLCD_Display_AllSegments(OFF);       
                            // stop mode icons...
                            devLCD_Display_StopAlarmDevice(1);
                            // Update LCD Display Task...     
                            devLCD_UpdateDisplay();

                            // reset global LCD summary to start...
                            gubLCDSummaryStatus = 0;
                                
                            // clear LCD summary task flag... 
                            _gsTT4Core.Task.Proc_LCDSummary = PROC_DONE;
                            _gsTT4Core.Task.Req_LCDSummary = REQ_DONE;
                        }                
                    } 
                }
                
            } /* End of LCD summary button depress control events */

            if(lubButtonStatus==BUTTON_STATUS_NOT_DEPRESSED)
                  gubStopButtonReConfigTimer = 0;        
                        
            //-----------------------------
            // START Button Released...
            //-----------------------------
            if(lubButtonStatus == BUTTON_STATUS_START_RELEASED){                
                lbButtonOnePass = TRUE;
                //reset button event interrupt flag...
                gubButtonEventFlag = 0;
                lubButtonStatus = BUTTON_STATUS_NOT_DEPRESSED;
                
            } /* End of LCD summary button release control events */
            
            //-----------------------------
            // STOP Button Depressed...
            //-----------------------------
            if(lubButtonStatus == BUTTON_STATUS_STOP_DEPRESSED){
                 if (_gsGenCfgOptByte.GenOpt.Byte0.StopReConfigEna == 1) {
                    appTT4MA_Timer1Interrupt_Enable();
                    if (gubStopButtonReConfigTimer > STOP_BUTTON_CFG_TIMEOUT)  //If we are here it is time to re-configure
                    {
                        _gsSensitech_Configuration_Area.ubCommandByte = CMD_BYTE_SLEEP; //Put the device to sleep after we re-configure
                        gubMem = _gsSensitech_Configuration_Area.ubCommandByte;
                        commTT4Cfg_FastWriteCfgEeprom(OFFSET_COMMAND_BYTE,(const void *)&gubMem); //save it to EEPROM
                        guwMem = 0xffff; //Fill the software version location with FFs when we reconfigure on stop button
                        commTT4Cfg_FastWriteCfgEeprom(OFFSET_SOFTWARE_VERSION_NUMBER, (const void *) &guwMem);
                        gudwTT4MA_PCTime = drvRTC_ReadRTCC_CurrentTime();   //update the "reconfigure time"
                       // goto LABEL_DEVICE_SLEEP_MODE_STATUS;     //jump to the beginning
                        devLCD_Display_AllSegments(OFF);
                        devLCD_Display_SEt(1);

                        gbTT4USB_Config_OK = TRUE;
                        
                    }
                    else if (gubStopButtonReConfigTimer >= STOP_BUTTON_CFG_WARNING)
                    {

                        if (gubStopButtonReConfigTimer % 2 == 0 ) //Flash the display after 5 seconds
                            // clear all segments...
                            devLCD_Display_AllSegments(OFF);
                        else
                        {    devLCD_Display_AllSegments(OFF);
                            // display CF...
                            devLCD_Display_CF(1);
                        }
                    }
                   
                }
                
            }/* End of STOP button depress control events */
            

            //-----------------------------
            // STOP Button Released...
            //-----------------------------
            if(lubButtonStatus == BUTTON_STATUS_STOP_RELEASED){
                //reset button event interrupt flag...
                gubButtonEventFlag = 0;
                lubButtonStatus = BUTTON_STATUS_NOT_DEPRESSED;
                
                if ((gubStopButtonReConfigTimer > STOP_BUTTON_CFG_TIMEOUT) && (_gsGenCfgOptByte.GenOpt.Byte0.StopReConfigEna))
                {
                    appTT4MA_Timer1Interrupt_Disable();
                    gubStopButtonReConfigTimer = 0;
                    break;
                }

                gubStopButtonReConfigTimer = 0;
                devLCD_Display_AllSegments(OFF);
                devLCD_Display_StopAlarmDevice(1); 

                // reset task flags...
                _gsTT4Core.Task.Req_StopDevice = REQ_DONE;    
                _gsTT4Core.Task.Proc_StopDevice = PROC_DONE;                
                
            }/* End of STOP button release control events */
            
            
        } /*  End of Button Events... */    
        
        
        //  Update LCD Display Task...         
        if(gbUpdateLCDFlag == TRUE) devLCD_UpdateDisplay();
        
        //========================================================================
        //  USB Event Tasks... 
        //========================================================================                

        // if Vbus detected then identify which USB modes....
        if(TT4_USB_VBUS_IO == USB_PORT_PLUGGED){

            if((_gsExtdOptByte.ExtOpt.Byte2.DispAlrmNumTrgAlrm != 1)||((_gsExtdOptByte.ExtOpt.Byte2.DispAlrmNumTrgAlrm == 1)&&(_gsUser_Configuration_Area.ubAlarmStatus <= 0))){
                lbButtonResetAlarmMode = TRUE;
            } 
            
            // disable timer interrupt...
            appTT4MA_Timer1Interrupt_Disable();
            // disable wakeup interrupt...
            appTT4MA_WakeUpInterrupts_Disable();
            utilDelay_ms(10);
                                
            // Call USB Device function status....
            hwUSBTT4Tasks();            

            // make it sure the residual voltage at the Vbus should be discharged....
            while(TT4_USB_VBUS_IO != USB_PORT_UNPLUGGED);

            // if previously the USB was in CF mode?... 
            if(gbTT4USB_Config_OK == TRUE){

                // Note: There are 2 conditions will happen after configuration mode...
                //       1 - if the configuration is successful then it will goes back to Device Sleep Mode status...
                //       2 - if the configuration is not successful then it will reset the device and goes back to Device Unconfigured status...
                // \ mjmariveles...
                
                // check if the command byte is in running mode...
                if((_gsSensitech_Configuration_Area.ubCommandByte == CMD_BYTE_RUNNING)||(_gsSensitech_Configuration_Area.ubCommandByte == CMD_BYTE_SLEEP)){
                    // exit sleep mode loop....
                    break;
                }
                else{ 
                    // if the configuration settings not successful then reset and go back to Unconfigured State!...mjm                                    
                    RCON = 0x00;
                    __asm__("reset");                
                }      
                 
            }            

            // reset CF ...                 
            _gsTT4Core.Task.Req_CfgDevice = REQ_DONE;
            _gsTT4Core.Task.Proc_CfgDevice = PROC_DONE;
            
            
            //reset button event interrupt flag...
            gubButtonEventFlag = 0;
            lubButtonStatus = BUTTON_STATUS_NOT_DEPRESSED;

                        
            // set Global USB Descriptor control..
            _geUSB_SetDescriptor = _eMSD_;                        
            // clear display...
            devLCD_Display_AllSegments(OFF_SEGS);
            // stop mode icons...
            devLCD_Display_StopAlarmDevice(1);                         
            // Update LCD Display Task...     
            devLCD_UpdateDisplay();

       //     if((_gsExtdOptByte.ExtOpt.Byte2.DispAlrmNumTrgAlrm != 1)||((_gsExtdOptByte.ExtOpt.Byte2.DispAlrmNumTrgAlrm == 1)&&(_gsUser_Configuration_Area.ubAlarmStatus <= 0))){
                // de-activate timer interrupt....
        //       appTT4MA_Timer1Interrupt_Disable();
        //        lbButtonResetAlarmMode = TRUE;
      //      }
       //     else{
                // Enable timer 1 interrupt...
                appTT4MA_Timer1Interrupt_Enable();                
      //      }
            
            // enable wakeup interrupt...
            appTT4MA_WakeUpInterrupts_Enable();
            utilDelay_ms(10);
        }           
        
     
    }while(INFINITE_LOOP);    


}


/*******************************************************************************
**  Function     : coreFirstPointActivationStatus
**  Description  : logs all device status and taking its 1st point  
**  PreCondition : either sleep mode or startup delay mode will be called first.
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void coreFirstPointActivationStatus(void){
    
    //-----------------------------------------------------------------------
    // Reset global control timers counters...
    //-----------------------------------------------------------------------
    gudwMeasurementIntervalTimer = 0;
    gubTrigAlarmCycleTimer = 0;
    gubHeartIconTimer = 0;
    gubLCDSummaryTimer = 0;

    //-----------------------------------------------------------------------
    // device take the start timestamp before taking the first datapoint...
    //-----------------------------------------------------------------------
    // \mjmariveles.....
    drvRTC_ReadRTCC_UpdateCurrentTimeSecs();
    
    // check if Autostart is active and validate its time 
    if((gbTT4AutoStartActive==TRUE)&&(gbTT4AutoStartExpired==FALSE)){
        _gsUser_Configuration_Area.udwUnit_Start_TimeStamp = _gsUser_Configuration_Area.udwStartUpDelay + gudwTT4MA_PCTime - 2ul; // minus 2 because it started from zero..
    }
    else{
        // get RTCC value to log device start time stamp before procedding the TT4 temeprature measurement...
        _gsUser_Configuration_Area.udwUnit_Start_TimeStamp = drvRTC_ReadRTCC_CurrentTime();
    }    

    gudwMem = drvRTC_ReadRTCC_CurrentTime();
    commTT4Cfg_FastWriteCfgEeprom(OFFSET_UNIT_START_TIMESTAMP,(const void *)&gudwMem);

    //-----------------------------------------------------------------------
    // device start acquiring first temperature data point....    
    // \ mjmariveles...
    //-----------------------------------------------------------------------
    // clear alarm status byte
    _gsUser_Configuration_Area.ubAlarmStatus = 0;    
    // this will be always zero when the unit starts from the beginning..            
    _gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints = 0;         
    appTT4MA_TempAcquisitionMode();
    gubHeartBeatOn = devLCD_Display_HeartBeat(1);
    gubHeartIconTimer = 0;
    devLCD_Display_ReadTemperature();
    devLCD_Display_SystemRunningIcons();
    // Update LCD Display Task...
    devLCD_UpdateDisplay(); 
    
    // initialize TTv & PDF file generation status flag...
    gbUSBFileGenSuccessStatusFlag = FALSE;    
    // initialize PDF file generation success status flag...
    gbPDFFileGenSuccessStatusFlag = FALSE;  
    // TTV file generation success status flag...
    gbTTVFileGenSuccessStatusFlag = FALSE;
    
}


/*******************************************************************************
**  Function     : coreInitStartTT4
**  Description  : initializing the global core loop vars prior executing the 1st point 
**  PreCondition : either sleep mode or startup delay mode should be called first.
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void coreInitStartTT4(void){
    
    //---------------------------------------------------------------------------
    // update EEPROM command byte to running Mode...    
    //---------------------------------------------------------------------------
    _gsSensitech_Configuration_Area.ubCommandByte = CMD_BYTE_RUNNING; // 2 - Running mode bit value...    
    _gsCommandByte.ubByte = _gsSensitech_Configuration_Area.ubCommandByte;
    gubMem = _gsSensitech_Configuration_Area.ubCommandByte;
    commTT4Cfg_FastWriteCfgEeprom(OFFSET_COMMAND_BYTE,(const void *)&gubMem);    
        
    // intialize button control registers...
    gbPreviousButtonDepressFlag = FALSE;
    
    // initlialize process task flag...     
    _gsTT4Core.Status = 0;
    // start min/max temperature data flag...     
    _gsLogMinMaxTempData.ubPrevTempDataFlag = 0;
    
    // set TTV/PDF file status generation flag ....
    gubStartDeviceStatusFlag = 1;

    utilDelay_ms(3);
        
    // force set to 10sec to acquire the 1st temperature reading and display...      
    gudwMeasurementIntervalTimer = 0;
    // start min/max temperature data flag...     
    _gsLogMinMaxTempData.ubPrevTempDataFlag = 0;
    // set timer event flag... 
    gubTimerEventFlag = 0;  
    
    // Set default to MSD mode...
    _geUSB_SetDescriptor = _eMSD_;
    gubStopDeviceStatus = _DEVICE_STOP_STATUS_GO_USB_;    

    // initlialize process task flag...     
    _gsTT4Core.Status = 0;
    
    // init blink bit when startup delay is initiated...
    gbBlinkToggleBit = 1;
    
    // display global control bits..
    gubLCDSummaryStatus = 0;
    gubAlarmDisplayStatus = 0;

    // TT4USB-MA EERPOM settings...
    commTT4Cfg_ClearEEPROMSpecificData();        
    
    // initialize EEPROM memory size...
    switch(_gsSensitech_Configuration_Area.ubSizeOfEEProm){
    case(EEPROM_CODE_MEM_SIZE_2K) :
          guwEEPROM_MemorySize = EEPROM_PHYSICAL_MEM_SIZE_2K;
          break;
    case(EEPROM_CODE_MEM_SIZE_4K) :
          guwEEPROM_MemorySize = EEPROM_PHYSICAL_MEM_SIZE_4K;
          break;
    case(EEPROM_CODE_MEM_SIZE_8K) :
          guwEEPROM_MemorySize = EEPROM_PHYSICAL_MEM_SIZE_8K;
          break;
    case(EEPROM_CODE_MEM_SIZE_16K) :
          guwEEPROM_MemorySize = EEPROM_PHYSICAL_MEM_SIZE_16K;
          break;
    default:    
          guwEEPROM_MemorySize = EEPROM_PHYSICAL_MEM_SIZE_2K;
          break; 
    }
    
}


/*******************************************************************************
**  Function     : coreInitSleepModeState
**  Description  : initializes sleep mode working vars and retrieves programmed EEPROM data 
**  PreCondition : device EEPROM configuration must be done first prior calling this module  
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void coreInitSleepModeState(void){
    
    //--------------------------------------------
    // Note: at this point the device 
    //       was configured successfully and 
    //       loads the EEPROM information to 
    //       establish the its preset values
    //       from TurboValidate....
    //
    // \mjmariveles...
    //--------------------------------------------
    
    // set default delay clock speed at 16Mhz...
    guwUtilSysClk_mDly = UTIL_MDELAY_SYSCLK_16MHZ;
   
    // clear all segments...    
    devLCD_Display_AllSegments(OFF);                    
 
    // re-initialize CPU control registers....
    hwInit_CPUCtrlRegister_USBMode(); 

    // reset config device not ok!...
    gbTT4USB_Config_OK = FALSE;
 
    // the device not yet started then must clear the bit to inform the system not to 
    // generate TTV/PDF files at the moment, unless the device had been started.......mjm
    gubStartDeviceStatusFlag = 0;
    
    //reset button event interrupt flag...
    gubButtonEventFlag = 0;    
    
    // reset files from start...
    gbConfigFileStatusFlag = TRUE;
    
    // clear PDF global string filename...
    util_InitLongFileName((char *)&gstrPDFFilename[0]);
    // clear TTV global string filename...
    util_InitLongFileName((char *)&gstrTTVFilename[0]);
    
    // get configuration status from EEPROM and load to TT4USB-MA memory..
    commTT4Cfg_LoadMem_EEPROM_ConfigurationStatus();
    utilDelay_ms(10);
    
}

/*******************************************************************************
**  Function     : coreInitUnconfiguredState
**  Description  : intializes unconfigured state control vars 
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void coreInitUnconfiguredState(void){    
    
    // set system oscillator control register...
    OSCCON = 0x3302;       
    // set clock divisor 2 to operate at 16Mhz system clock...     
    CLKDIV = 0x0040; // 32MHz / 2 = 16Mhz System Clock... 
    Nop();Nop();Nop();
    guwUtilSysClk_mDly = UTIL_MDELAY_SYSCLK_16MHZ;

    // initialize structure variables...
    coreClearGlobalVars();

    // set Global USB Descriptor control..
    _geUSB_SetDescriptor = _eMSD_;
    // initizlize USB status flag...
    gbUSBActiveModeStatusFlag = FALSE;
    
    // MSD file transfer control vars...
    guwWriteSector = 0;
    gudwWriteBlock = gudwReadBlock = 0xFFFFFFFF; // reset block mjm...
        
    // configuration was not called yet... then set to false..
    gbTT4USB_Config_OK = FALSE;
    
    // intialize button control registers...
    gbPreviousButtonDepressFlag = FALSE;
    
    //reset button event interrupt flag...
    gubButtonEventFlag = 0;    

    // init system blink icon..
    gbBlinkToggleBit = 1;

     // the device not yet started then must clear the bit to inform the system not to 
    // generate TTV/PDF files at the moment, unless the device had been started.......mjm
    gubStartDeviceStatusFlag = 0;
    
    // initialize TTv & PDF file generation status flag...
    gbUSBFileGenSuccessStatusFlag = FALSE;
    
    // reset files from start...
    gbConfigFileStatusFlag = FALSE;
    gbInitMBRStatusFlag = TRUE;

    // from reset state, write protocol ID number to EEPROM at offset 0 ...
    guwMem = HDSK_SEND_PROTOCOL_ID;
    commTT4Cfg_FastWriteCfgEeprom(OFFSET_PROTOCOL_ID,(const void *)&guwMem);

    // from reset state, write firmware version number to EEPROM at offset 2 ...
    guwMem = (TT4USB_MA_SW_VERSION_1<<1) | TT4USB_MA_SW_VERSION_2;
    guwMem = (TT4USB_MA_SW_VERSION_1<<1);
    guwMem = TT4USB_MA_SW_VERSION_1;
    commTT4Cfg_FastWriteCfgEeprom(OFFSET_FIRMWARE_VER,(const void *)&guwMem);
    
    //--------------------------------------------------------------------------
    //  intializes default system time...
    //--------------------------------------------------------------------------
    // initialize system clock RTCC registers...
    // set to January 1, 2010... 00:00:00 (12:00 am)
    //set recovery flag to FALSE to initialize RTCC with default values.
    gbResetRecoveryFlag = FALSE;
    drvRTC_SetupRTCC();       

    //--------------------------------------------------------------------------
    // initialize Command Byte from reset state...
    //--------------------------------------------------------------------------
    // clear comamnd byte bits...
    _gsCommandByte.ubByte = 0;
    // Command Byte status bit forced assigned to unconfigured state...    
    // update memory and  write to EEPROM...    
    _gsSensitech_Configuration_Area.ubCommandByte = CMD_BYTE_UNCONFIG; // 16 - unconfigured mode bit value...    
    gubMem = _gsSensitech_Configuration_Area.ubCommandByte;
    commTT4Cfg_FastWriteCfgEeprom(OFFSET_COMMAND_BYTE,(const void *)&gubMem);
    
    //--------------------------------------------------------------------------
    // Initialize 3 point Certificate Data in EEPROM...
    //--------------------------------------------------------------------------
    TTV_3PointCertClearEEPROMData();    
    
    // initialize CPU control registers....
    hwInit_CPUCtrlRegister_USBMode();      
    
    // update the temperature calibration offset range levels....
    _gsSensor1TempCalOffset.fTempLevel_1_FDeg = RANGE_LEVEL_1;
    _gsSensor1TempCalOffset.fTempLevel_2_FDeg = RANGE_LEVEL_2;
    _gsSensor1TempCalOffset.fTempLevel_3_FDeg = RANGE_LEVEL_3;
    _gsSensor1TempCalOffset.fTempLevel_4_FDeg = RANGE_LEVEL_4;
    
}


/*    End Of File    */



