/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm                                            **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc. and Blue Ocean Innovation Ltd.                            **
 **                                                                           **
 **  Copryright(c) 2010. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Blue Ocean Innovation Ltd.                        **
 **  Project#            :  449                                               **
 **  Creation date       :  08/20/2010                                        **
 **  Programmer          :  Michael J. Mariveles                              **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : commTT4Config.c                                                   **
 **                                                                           **
 **  Description :                                                            **
 **    - Setups all EEPROM configuration data.                                **
 **    - Sensitech,User,Alarm configuration area were taken here.             **
 **    - EEPROM formatted read/write routine handlers.                        **
 **    - TT4USB-MA configuration status loader.                               **
 **    - Post data logging routine after the device is stopped or taking last **
 **      point in multi-drop.                                                 **
 **    - USB serial number loader.                                            **
 **                                                                           **
 ******************************************************************************/
 
#include "hwPIC24F.h"
#include "hwSystem.h"
#include "devI2CEEPROM_24AA256.h"
#include "utility.h"
#include "commTT4Config.h"
#include "devI2CTempSensor_TMP112.h"
#include "drvRTC.h"


//------------------------------
//  16bit Temp data ...
//------------------------------
typedef union {
    
    struct {
        UINT16  Temperature: 12;  // LSB   
        UINT16  Marker     :  4;  // MSB   
    } uwEeprom;
    
    struct {
        UINT16  Lo: 8;  // LSB   
        UINT16  Hi: 8;  // MSB   
    } uwWord;

    struct {
        UINT16  XX0      :  11;  // LSB           
        UINT16  Negative :   1;  // 
        UINT16  XX1      :   4;  // MSB           
    } uwSign;

    UINT16 uwWordData; 
    
} _sTT4EEDATAFORMAT;   


// temperary storage for EDS memory transfer buffer...
extern UINT8   gubMem;
extern UINT16  guwMem;
extern UINT32  gudwMem;


// EEPROM configuration control structure register...
__SENSITECH_CONFIGURATION_AREA  _gsSensitech_Configuration_Area __attribute__((persistent));    //set as persistent for reset recovery
__USER_CONFIGURATION_AREA       _gsUser_Configuration_Area __attribute__((persistent));         //set as persistent for reset recovery
__ALARM_CONFIGURATION_AREA      _gsAlarmConfig[6] __attribute__((persistent));                  //set as persistent for reset recovery

 
// EEPROM control bit manipulator structure register...
__sBITS_GENCFGOPT_BYTE _gsGenCfgOptByte __attribute__((persistent));    //set as persistent for reset recovery
__sBITS_EXTOPT_BYTE    _gsExtdOptByte __attribute__((persistent));      //set as persistent for reset recovery
__sBITS_USERCFG_DATA   _gsUserCfgAreaData __attribute__((persistent));  //set as persistent for reset recovery

// Sensor 1 Temperature Calibration Offset...
__sSENSOR1_TEMP_CAL_OFFSET  _gsSensor1TempCalOffset;

// EEPROM memory size...
UINT16 guwEEPROM_MemorySize __attribute__((persistent));    //for reset recovery

// download time storage...
volatile UINT32 gudwDeviceStoppedDownloadTime;
// the time when the device was plugged to the USB to download temperature datapoints...
volatile UINT32 gudwDeviceReadOnTime;


// number of events control byte...
__sTRIG_EVENTS_STAT _guwTrigEvents[7] __attribute__((persistent));    //set as persistent for reset recovery

// data point cache memory as image storage from EEPROM...
volatile __eds__ UINT16 guwEdsCacheMem[16001u] __attribute__((space(eds)));

// RAM Memory Current Time global storage...
extern volatile UINT32 gudwTT4MA_CurrentTime;

// command byte bit manipulator control register....
__sBITS_COMMAND_BYTE _gsCommandByte __attribute__((persistent));    //set as persistent for reset recovery

// TT4USB-MA USB Serial Number declaration with a multi-byte character...  mjmariveles...
__sTT4USBMA_SN _gsTT4USBMA_SerialNumber;

// Alarm remaining and time elapse value...
extern UINT32 gudwAlarmTimeRemainingElapsed;
// Alarm remaining and time elapse flag...
extern BOOL gbAlarmTimeFlag;

// Stop mode status register...
extern UINT8 gubStopDeviceStatus;


/*******************************************************************************
**  Function     : commTT4Cfg_InitEeprom
**  Description  : intializes I/O control 32K EEPROM ..
**  PreCondition : setup the assigned I/O port
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void commTT4Cfg_InitEeprom(void){

    devEEPROM_Init();
}

/*******************************************************************************
**  Function     : commTT4Cfg_InitControlVariables
**  Description  : initializes global control variables
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void commTT4Cfg_InitControlVariables(void){
    
}


/*******************************************************************************
**  Function     : commTT4Cfg_Load_SensitechConfig_Values
**  Description  : intializes global Sensitech Configuration structure data..
**  PreCondition : none
**  Side Effects : updates the entire global Sensitech Configuration structure data.
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/ 
void commTT4Cfg_Load_SensitechConfig_Values(void){
    
    INT8 sbData = 0;
    
    //-------------------------------------------------------------   
    // Sensitech Configuration Area.....
    //-------------------------------------------------------------   
    
    // read protocol ID...    
    commTT4Cfg_FastReadCfgEeprom(OFFSET_PROTOCOL_ID,(void *)&guwMem);
    _gsSensitech_Configuration_Area.uwProtocolID = guwMem;

    // manufacturing serial number...    
    commTT4Cfg_FastReadCfgEeprom(OFFSET_MANUFACTURING_SERIAL_NUM,(void *)&gudwMem);
    _gsSensitech_Configuration_Area.udwManufacturingSerialNumber = gudwMem;        
    commTT4Cfg_USBLoadSerialNumber(_gsSensitech_Configuration_Area.udwManufacturingSerialNumber);

    // load customer number of resets....
    commTT4Cfg_FastReadCfgEeprom(OFFSET_CUSTOMER_RESETS,(void *)&guwMem);    
    _gsSensitech_Configuration_Area.uwNumberOfCustomerResets = guwMem;

    // read EEPROM size value...
    commTT4Cfg_FastReadCfgEeprom(OFFSET_EEPROM_SIZE,(void *)&gubMem);    
    // validate if greater than 16k EEPROM code value... 
    // 0 - 2K EEPROM size
    // 1 - 4K EEPROM size
    // 2 - 8K EEPROM size
    // 3 - 16K EEPROM size
    // 4 - 32K EEPROM size (not applicable)
    if(gubMem>=4) gubMem = 3; // limit EEPROM size..        
    _gsSensitech_Configuration_Area.ubSizeOfEEProm = gubMem;
    

    // Read number of stored datapoints...    
    commTT4Cfg_FastReadCfgEeprom(OFFSET_NUMBER_OF_STORED_DATAPOINTS,(void *)&guwMem);
    _gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints = guwMem;

    // Read command byte..     
    commTT4Cfg_FastReadCfgEeprom(OFFSET_COMMAND_BYTE,(void *)&gubMem);
    _gsSensitech_Configuration_Area.ubCommandByte = gubMem;
    
    //Read Timezone/DST shift byte
    commTT4Cfg_FastReadCfgEeprom(OFFSET_TIMEZONE, (void *)&gubMem);
    _gsSensitech_Configuration_Area.pdfTimeZone = (INT8)gubMem;
    
    // Read Extended Option Byte 1..     
    commTT4Cfg_FastReadCfgEeprom(OFFSET_EXTENDED_BYTE_OPTION_1,(void *)&gubMem);
    _gsSensitech_Configuration_Area.ubExtendedOptionByte1 = gubMem;
    _gsExtdOptByte.Word.Byte1 = _gsSensitech_Configuration_Area.ubExtendedOptionByte1;

    // Read Extended Option Byte 2..     
    commTT4Cfg_FastReadCfgEeprom(OFFSET_EXTENDED_BYTE_OPTION_2,(void *)&gubMem);
    _gsSensitech_Configuration_Area.ubExtendedOptionByte2 = gubMem;
    _gsExtdOptByte.Word.Byte2 = _gsSensitech_Configuration_Area.ubExtendedOptionByte2;

    // Read Sensor 1 Calibration Offset range (A) -22F to -0F...  
    commTT4Cfg_FastReadCfgEeprom(SENSOR1_CAL_OFFSET_A,(void *)&gubMem);
    sbData = (INT8)gubMem; // type cast unsigned byte to singed byte...    
    _gsSensitech_Configuration_Area.fSensor1CalOffset_Range_A = (float)sbData; // convert to float...
    _gsSensitech_Configuration_Area.fSensor1CalOffset_Range_A /= 10.0; // divide to get the decimal digit result...
    // save global var Range (A) in F Deg...
    _gsSensor1TempCalOffset.fRangeA_FDeg = _gsSensitech_Configuration_Area.fSensor1CalOffset_Range_A;

    // Read Sensor 1 Calibration Offset range (B) 0F to +122F...  
    commTT4Cfg_FastReadCfgEeprom(SENSOR1_CAL_OFFSET_B,(void *)&gubMem);
    sbData = (INT8)gubMem; // type cast unsigned byte to singed byte...    
    _gsSensitech_Configuration_Area.fSensor1CalOffset_Range_B = (float)sbData; // convert to float...
    _gsSensitech_Configuration_Area.fSensor1CalOffset_Range_B /= 10.0; // divide to get the decimal digit result...
    // save global var Range (B) in F Deg...
    _gsSensor1TempCalOffset.fRangeB_FDeg = _gsSensitech_Configuration_Area.fSensor1CalOffset_Range_B;
    
    // Read Sensor 1 Calibration Offset range (C) +122F to +158F...  
    commTT4Cfg_FastReadCfgEeprom(SENSOR1_CAL_OFFSET_C,(void *)&gubMem);
    sbData = (INT8)gubMem; // type cast unsigned byte to singed byte...    
    _gsSensitech_Configuration_Area.fSensor1CalOffset_Range_C = (float)sbData; // convert to float...
    _gsSensitech_Configuration_Area.fSensor1CalOffset_Range_C /= 10.0; // divide to get the decimal digit result...
    // save global var Range (C) in F Deg...
    _gsSensor1TempCalOffset.fRangeC_FDeg = _gsSensitech_Configuration_Area.fSensor1CalOffset_Range_C;

}
    

/*******************************************************************************
**  Function     : commTT4Cfg_Load_UserConfig_Values
**  Description  : intializes global User Configuration structure data..
**  PreCondition : none
**  Side Effects : updates the entire global User Configuration structure data.
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/   
#define cMKT_DEFAULT   83144.0  // MKT activation energy is zero value then use this constant as default.. as per Peter Nunes requirement, 10312013 mjm
//#define cMKT_ACTIVE_ENERGY_MAX   125000ul
//#define cMKT_ACTIVE_ENERGY_MIN   42000ul

// temporary max min values requested by Peter ... 11/01/2013
#define cMKT_ACTIVE_ENERGY_MAX   500000.0
#define cMKT_ACTIVE_ENERGY_MIN   1.0

void commTT4Cfg_Load_UserConfig_Values(void){
    
    _sDWORDBITDATA _sl4Byte;
    register float fTempValue;
        
    //-------------------------------------------------------------   
    // User Configuration Area .....
    //-------------------------------------------------------------       
    
    // Read measurement interval...
    commTT4Cfg_FastReadCfgEeprom(OFFSET_MEASUREMENT_INTERVAL,(void *)&gudwMem);
    _sl4Byte.DwData = gudwMem;
    _sl4Byte.Byte.B3 = 0; // clear the 4th byte since it is a 3 byte data info only..
    _gsUser_Configuration_Area.udwMeasurementInterval = _sl4Byte.DwData;

    // Read start-up delay...     
    commTT4Cfg_FastReadCfgEeprom(OFFSET_STARTUP_DELAY,(void *)&gudwMem);
    _sl4Byte.DwData = gudwMem;
    _sl4Byte.Byte.B3 = 0; // clear the 4th byte since it is a 3 byte data info only..
    _gsUser_Configuration_Area.udwStartUpDelay = _sl4Byte.DwData;

    // Read general configuration options 1 (byte 0)...
    commTT4Cfg_FastReadCfgEeprom(OFFSET_GENERAL_CONFIG_OPTIONS_BYTE_0,(void *)&gubMem);
    _gsUser_Configuration_Area.ubGeneralConfigOptions1_Byte0 = gubMem;
    _gsGenCfgOptByte.Word.Byte0 = _gsUser_Configuration_Area.ubGeneralConfigOptions1_Byte0;

    // Read general configuration options 1 (byte 1)...    
    commTT4Cfg_FastReadCfgEeprom(OFFSET_GENERAL_CONFIG_OPTIONS_BYTE_1,(void *)&gubMem);
    _gsUser_Configuration_Area.ubGeneralConfigOptions1_Byte1 = gubMem;
    _gsGenCfgOptByte.Word.Byte1 = _gsUser_Configuration_Area.ubGeneralConfigOptions1_Byte1;

    // read trip number or customer serial number..    
    commTT4Cfg_FastReadCfgEeprom(OFFSET_TRIP_NUMBER,(void *)&gudwMem);    
    _gsUser_Configuration_Area.udwTripNumber = gudwMem;
        
    // read MKT Alarm Acivation Energy....
    commTT4Cfg_FastReadCfgEeprom(OFFSET_MKT_ACT,(void *)&gudwMem); // added new marketing requirement requested by Peter Nunes.. mjm 10312013..  
    _gsUser_Configuration_Area.fMKTActivationEnergy = gudwMem;    
    
    if((_gsUser_Configuration_Area.fMKTActivationEnergy > cMKT_ACTIVE_ENERGY_MAX)||(_gsUser_Configuration_Area.fMKTActivationEnergy < cMKT_ACTIVE_ENERGY_MIN))
    	_gsUser_Configuration_Area.fMKTActivationEnergy = cMKT_DEFAULT; // load default if out of range...
    
        
    // Read temperature Ideal Range High..     
    commTT4Cfg_FastReadCfgEeprom(OFFSET_TEMPERATURE_IDEAL_RANGE_HIGH,(void *)&guwMem);
    _gsUser_Configuration_Area.uwTemperatureIdealRangeHigh = guwMem;
    fTempValue = commTT4Cfg_ConvertToFinalTempData(_gsUser_Configuration_Area.uwTemperatureIdealRangeHigh);
    _gsUserCfgAreaData.fTempIdealRangeHighValue = fTempValue;
    
    // Read temperature Ideal Range Low.. 
    commTT4Cfg_FastReadCfgEeprom(OFFSET_TEMPERATURE_IDEAL_RANGE_LOW,(void *)&guwMem);    
    _gsUser_Configuration_Area.uwTemperatureIdealRangeLow = guwMem;
    fTempValue = commTT4Cfg_ConvertToFinalTempData(_gsUser_Configuration_Area.uwTemperatureIdealRangeLow);
    _gsUserCfgAreaData.fTempIdealRangeLowValue = fTempValue;

    // Read Alarm Status Byte.. 
    commTT4Cfg_FastReadCfgEeprom(OFFSET_ALARM_STATUS,(void *)&gubMem);
    _gsUser_Configuration_Area.ubAlarmStatus = gubMem;
    
    // Read remaining alarm config status Byte.. 
    commTT4Cfg_FastReadCfgEeprom(OFFSET_RE_ALM_DISP_CONFIG_BYTE,(void *)&gubMem);
    _gsUser_Configuration_Area.ubRemAlarmDispCfgByte = gubMem;
    
    // Read LCD contrast control Byte.. 
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LCD_CONTRAST_ADJUST,(void *)&gubMem);
    // LCD power have only 4 combinations 0,1,2,3
    if(gubMem > 3)gubMem = 3;
    _gsUser_Configuration_Area.ubLCD_ContrastAdjust = gubMem;
    
    
}    
    
/*******************************************************************************
**  Function     : commTT4Cfg_Load_UserInformation_Values
**  Description  : none
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/    
void commTT4Cfg_Load_UserInformation_Values(void){    
    
    //-------------------------------------------------------------   
    // load User Information Area EEPROM values.....
    //-------------------------------------------------------------           
}    
    

/*******************************************************************************
**  Function     : commTT4Cfg_Load_AlarmConfig_Values
**  Description  : intializes global Alrm Configuration structure data..
**  PreCondition : none
**  Side Effects : updates the entire global Alrm Configuration structure data.
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void commTT4Cfg_Load_AlarmConfig_Values(void){

    _sDWORDBITDATA _sl4Byte;
    register float fTempValue;
    UINT16 luwSensitechBinTempValue;


    //-------------------------------------------------------------   
    // Alarm Configuration Area default values.....
    //-------------------------------------------------------------       


    //-----------------------------------
    // Alarm 1 Configuration Settings...
    //-----------------------------------   
    
        commTT4Cfg_FastReadCfgEeprom(OFFSET_ALARM_1_CONFIG_BYTE_0,(void *)&gubMem);
        _gsAlarmConfig[ALARM_1].ubConfigByte0 = gubMem;
        
        /////////////////////  FOR DELETION  //////////////////////////
        // debug mjm MKT forced alarm 1 to MKT high alarm..  mjm 11/08/2013
        // 1010 0001 = 0xA1
        //_gsAlarmConfig[ALARM_1].ubConfigByte0 = 0xA1;
        ///////////////////////////////////////////////////////////////
        
        commTT4Cfg_FastReadCfgEeprom(OFFSET_ALARM_1_RANGE1_HIGH,(void *)&luwSensitechBinTempValue);         
        fTempValue = commTT4Cfg_ConvertToFinalTempData(luwSensitechBinTempValue);
        _gsAlarmConfig[ALARM_1].fRange1HighTempSetPoint = fTempValue;
        
        commTT4Cfg_FastReadCfgEeprom(OFFSET_ALARM_1_RANGE1_LOW,(void *)&luwSensitechBinTempValue);
        fTempValue = commTT4Cfg_ConvertToFinalTempData(luwSensitechBinTempValue);
        _gsAlarmConfig[ALARM_1].fRange1LowTempSetPoint = fTempValue;
        
        commTT4Cfg_FastReadCfgEeprom(OFFSET_ALARM_1_RANGE2_HIGH,(void *)&luwSensitechBinTempValue);
        fTempValue = commTT4Cfg_ConvertToFinalTempData(luwSensitechBinTempValue);
        _gsAlarmConfig[ALARM_1].fRange2HighTempSetPoint = fTempValue;
        
        commTT4Cfg_FastReadCfgEeprom(OFFSET_ALARM_1_RANGE2_LOW,(void *)&luwSensitechBinTempValue);
        fTempValue = commTT4Cfg_ConvertToFinalTempData(luwSensitechBinTempValue);
        _gsAlarmConfig[ALARM_1].fRange2LowTempSetPoint = fTempValue;
        
        commTT4Cfg_FastReadCfgEeprom(OFFSET_ALARM_1_COUNTER,(void *)&gudwMem);
        _sl4Byte.DwData = gudwMem;
        _sl4Byte.Byte.B3 = 0; // clear the 4th byte since it is a 3 byte data info only..        
        _gsAlarmConfig[ALARM_1].udwCounter = _sl4Byte.DwData;
        
        commTT4Cfg_FastReadCfgEeprom(OFFSET_ALARM_1_THRESHHOLD_VALUE,(void *)&gudwMem);
        _sl4Byte.DwData = gudwMem;
        _sl4Byte.Byte.B3 = 0; // clear the 4th byte since it is a 3 byte data info only..        
        _gsAlarmConfig[ALARM_1].udwThreshhold = _sl4Byte.DwData;
        


    //-----------------------------------
    // Alarm 2 Configuration Settings...
    //-----------------------------------        

        commTT4Cfg_FastReadCfgEeprom(OFFSET_ALARM_2_CONFIG_BYTE_0,(void *)&gubMem);
        _gsAlarmConfig[ALARM_2].ubConfigByte0 = gubMem;
        
        /////////////////////  FOR DELETION  //////////////////////////
        // debug mjm MKT forced alarm 2 to MKT low alarm.. mjm 11/08/2013
        // 1010 0001 = 0xB1
        //_gsAlarmConfig[ALARM_2].ubConfigByte0 = 0xB1;
        ///////////////////////////////////////////////////////////////
        
        commTT4Cfg_FastReadCfgEeprom(OFFSET_ALARM_2_RANGE1_HIGH,(void *)&luwSensitechBinTempValue);
        fTempValue = commTT4Cfg_ConvertToFinalTempData(luwSensitechBinTempValue);
        _gsAlarmConfig[ALARM_2].fRange1HighTempSetPoint = fTempValue;

        commTT4Cfg_FastReadCfgEeprom(OFFSET_ALARM_2_RANGE1_LOW,(void *)&luwSensitechBinTempValue);
        fTempValue = commTT4Cfg_ConvertToFinalTempData(luwSensitechBinTempValue);
        _gsAlarmConfig[ALARM_2].fRange1LowTempSetPoint = fTempValue;
        
        commTT4Cfg_FastReadCfgEeprom(OFFSET_ALARM_2_RANGE2_HIGH,(void *)&luwSensitechBinTempValue);
        fTempValue = commTT4Cfg_ConvertToFinalTempData(luwSensitechBinTempValue);
        _gsAlarmConfig[ALARM_2].fRange2HighTempSetPoint = fTempValue;
        
        commTT4Cfg_FastReadCfgEeprom(OFFSET_ALARM_2_RANGE2_LOW,(void *)&luwSensitechBinTempValue);
        fTempValue = commTT4Cfg_ConvertToFinalTempData(luwSensitechBinTempValue);
        _gsAlarmConfig[ALARM_2].fRange2LowTempSetPoint = fTempValue;
        
        commTT4Cfg_FastReadCfgEeprom(OFFSET_ALARM_2_COUNTER,(void *)&gudwMem);
        _sl4Byte.DwData = gudwMem;
        _sl4Byte.Byte.B3 = 0; // clear the 4th byte since it is a 3 byte data info only..        
        _gsAlarmConfig[ALARM_2].udwCounter = _sl4Byte.DwData;
        
        commTT4Cfg_FastReadCfgEeprom(OFFSET_ALARM_2_THRESHHOLD_VALUE,(void *)&gudwMem);
        _sl4Byte.DwData = gudwMem;
        _sl4Byte.Byte.B3 = 0; // clear the 4th byte since it is a 3 byte data info only..        
        _gsAlarmConfig[ALARM_2].udwThreshhold = _sl4Byte.DwData;


    //-----------------------------------
    // Alarm 3 Configuration Settings...
    //-----------------------------------        
        commTT4Cfg_FastReadCfgEeprom(OFFSET_ALARM_3_CONFIG_BYTE_0,(void *)&gubMem);
        _gsAlarmConfig[ALARM_3].ubConfigByte0 = gubMem;
        
        commTT4Cfg_FastReadCfgEeprom(OFFSET_ALARM_3_RANGE1_HIGH,(void *)&luwSensitechBinTempValue);
        fTempValue = commTT4Cfg_ConvertToFinalTempData(luwSensitechBinTempValue);
        _gsAlarmConfig[ALARM_3].fRange1HighTempSetPoint = fTempValue;
        
        commTT4Cfg_FastReadCfgEeprom(OFFSET_ALARM_3_RANGE1_LOW,(void *)&luwSensitechBinTempValue);
        fTempValue = commTT4Cfg_ConvertToFinalTempData(luwSensitechBinTempValue);
        _gsAlarmConfig[ALARM_3].fRange1LowTempSetPoint = fTempValue;
        
        commTT4Cfg_FastReadCfgEeprom(OFFSET_ALARM_3_RANGE2_HIGH,(void *)&luwSensitechBinTempValue);
        fTempValue = commTT4Cfg_ConvertToFinalTempData(luwSensitechBinTempValue);
        _gsAlarmConfig[ALARM_3].fRange2HighTempSetPoint = fTempValue;
        
        commTT4Cfg_FastReadCfgEeprom(OFFSET_ALARM_3_RANGE2_LOW,(void *)&luwSensitechBinTempValue);
        fTempValue = commTT4Cfg_ConvertToFinalTempData(luwSensitechBinTempValue);
        _gsAlarmConfig[ALARM_3].fRange2LowTempSetPoint = fTempValue;

        commTT4Cfg_FastReadCfgEeprom(OFFSET_ALARM_3_COUNTER,(void *)&gudwMem);
        _sl4Byte.DwData = gudwMem;
        _sl4Byte.Byte.B3 = 0; // clear the 4th byte since it is a 3 byte data info only..        
        _gsAlarmConfig[ALARM_3].udwCounter = _sl4Byte.DwData;
        
        commTT4Cfg_FastReadCfgEeprom(OFFSET_ALARM_3_THRESHHOLD_VALUE,(void *)&gudwMem);
        _sl4Byte.DwData = gudwMem;
        _sl4Byte.Byte.B3 = 0; // clear the 4th byte since it is a 3 byte data info only..        
        _gsAlarmConfig[ALARM_3].udwThreshhold = _sl4Byte.DwData;

    
    //-----------------------------------
    // Alarm 4 Configuration Settings...
    //-----------------------------------        
        commTT4Cfg_FastReadCfgEeprom(OFFSET_ALARM_4_CONFIG_BYTE_0,(void *)&gubMem);
        _gsAlarmConfig[ALARM_4].ubConfigByte0 = gubMem;
        
        commTT4Cfg_FastReadCfgEeprom(OFFSET_ALARM_4_RANGE1_HIGH,(void *)&luwSensitechBinTempValue);
        fTempValue = commTT4Cfg_ConvertToFinalTempData(luwSensitechBinTempValue);
        _gsAlarmConfig[ALARM_4].fRange1HighTempSetPoint = fTempValue;
        
        commTT4Cfg_FastReadCfgEeprom(OFFSET_ALARM_4_RANGE1_LOW,(void *)&luwSensitechBinTempValue);
        fTempValue = commTT4Cfg_ConvertToFinalTempData(luwSensitechBinTempValue);
        _gsAlarmConfig[ALARM_4].fRange1LowTempSetPoint = fTempValue;
        
        commTT4Cfg_FastReadCfgEeprom(OFFSET_ALARM_4_RANGE2_HIGH,(void *)&luwSensitechBinTempValue);
        fTempValue = commTT4Cfg_ConvertToFinalTempData(luwSensitechBinTempValue);
        _gsAlarmConfig[ALARM_4].fRange2HighTempSetPoint = fTempValue;
        
        commTT4Cfg_FastReadCfgEeprom(OFFSET_ALARM_4_RANGE2_LOW,(void *)&luwSensitechBinTempValue);
        fTempValue = commTT4Cfg_ConvertToFinalTempData(luwSensitechBinTempValue);
        _gsAlarmConfig[ALARM_4].fRange2LowTempSetPoint = fTempValue;
        
        commTT4Cfg_FastReadCfgEeprom(OFFSET_ALARM_4_COUNTER,(void *)&gudwMem);
        _sl4Byte.DwData = gudwMem;
        _sl4Byte.Byte.B3 = 0; // clear the 4th byte since it is a 3 byte data info only..        
        _gsAlarmConfig[ALARM_4].udwCounter = _sl4Byte.DwData;
        
        commTT4Cfg_FastReadCfgEeprom(OFFSET_ALARM_4_THRESHHOLD_VALUE,(void *)&gudwMem);
        _sl4Byte.DwData = gudwMem;
        _sl4Byte.Byte.B3 = 0; // clear the 4th byte since it is a 3 byte data info only..        
        _gsAlarmConfig[ALARM_4].udwThreshhold = _sl4Byte.DwData;
        
        
    //-----------------------------------
    // Alarm 5 Configuration Settings...
    //-----------------------------------        
        commTT4Cfg_FastReadCfgEeprom(OFFSET_ALARM_5_CONFIG_BYTE_0,(void *)&gubMem);
        _gsAlarmConfig[ALARM_5].ubConfigByte0 = gubMem;
        
        commTT4Cfg_FastReadCfgEeprom(OFFSET_ALARM_5_RANGE1_HIGH,(void *)&luwSensitechBinTempValue);
        fTempValue = commTT4Cfg_ConvertToFinalTempData(luwSensitechBinTempValue);
        _gsAlarmConfig[ALARM_5].fRange1HighTempSetPoint = fTempValue;
        
        commTT4Cfg_FastReadCfgEeprom(OFFSET_ALARM_5_RANGE1_LOW,(void *)&luwSensitechBinTempValue);
        fTempValue = commTT4Cfg_ConvertToFinalTempData(luwSensitechBinTempValue);
        _gsAlarmConfig[ALARM_5].fRange1LowTempSetPoint = fTempValue;
        
        commTT4Cfg_FastReadCfgEeprom(OFFSET_ALARM_5_RANGE2_HIGH,(void *)&luwSensitechBinTempValue);
        fTempValue = commTT4Cfg_ConvertToFinalTempData(luwSensitechBinTempValue);
        _gsAlarmConfig[ALARM_5].fRange2HighTempSetPoint = fTempValue;
        
        commTT4Cfg_FastReadCfgEeprom(OFFSET_ALARM_5_RANGE2_LOW,(void *)&luwSensitechBinTempValue);
        fTempValue = commTT4Cfg_ConvertToFinalTempData(luwSensitechBinTempValue);
        _gsAlarmConfig[ALARM_5].fRange2LowTempSetPoint = fTempValue;
        
        commTT4Cfg_FastReadCfgEeprom(OFFSET_ALARM_5_COUNTER,(void *)&gudwMem);
        _sl4Byte.DwData = gudwMem;
        _sl4Byte.Byte.B3 = 0; // clear the 4th byte since it is a 3 byte data info only..        
        _gsAlarmConfig[ALARM_5].udwCounter = _sl4Byte.DwData;
        
        commTT4Cfg_FastReadCfgEeprom(OFFSET_ALARM_5_THRESHHOLD_VALUE,(void *)&gudwMem);
        _sl4Byte.DwData = gudwMem;
        _sl4Byte.Byte.B3 = 0; // clear the 4th byte since it is a 3 byte data info only..        
        _gsAlarmConfig[ALARM_5].udwThreshhold = _sl4Byte.DwData;
        
        
    //-----------------------------------
    // Alarm 6 Configuration Settings...
    //-----------------------------------        
        commTT4Cfg_FastReadCfgEeprom(OFFSET_ALARM_6_CONFIG_BYTE_0,(void *)&gubMem);
        _gsAlarmConfig[ALARM_6].ubConfigByte0 = gubMem;
        
        commTT4Cfg_FastReadCfgEeprom(OFFSET_ALARM_6_RANGE1_HIGH,(void *)&luwSensitechBinTempValue);
        fTempValue = commTT4Cfg_ConvertToFinalTempData(luwSensitechBinTempValue);
        _gsAlarmConfig[ALARM_6].fRange1HighTempSetPoint = fTempValue;
        
        commTT4Cfg_FastReadCfgEeprom(OFFSET_ALARM_6_RANGE1_LOW,(void *)&luwSensitechBinTempValue);
        fTempValue = commTT4Cfg_ConvertToFinalTempData(luwSensitechBinTempValue);
        _gsAlarmConfig[ALARM_6].fRange1LowTempSetPoint = fTempValue;
        
        commTT4Cfg_FastReadCfgEeprom(OFFSET_ALARM_6_RANGE2_HIGH,(void *)&luwSensitechBinTempValue);
        fTempValue = commTT4Cfg_ConvertToFinalTempData(luwSensitechBinTempValue);
        _gsAlarmConfig[ALARM_6].fRange2HighTempSetPoint = fTempValue;
        
        commTT4Cfg_FastReadCfgEeprom(OFFSET_ALARM_6_RANGE2_LOW,(void *)&luwSensitechBinTempValue);
        fTempValue = commTT4Cfg_ConvertToFinalTempData(luwSensitechBinTempValue);
        _gsAlarmConfig[ALARM_6].fRange2LowTempSetPoint = fTempValue;
        
        commTT4Cfg_FastReadCfgEeprom(OFFSET_ALARM_6_COUNTER,(void *)&gudwMem);
        _sl4Byte.DwData = gudwMem;
        _sl4Byte.Byte.B3 = 0; // clear the 4th byte since it is a 3 byte data info only..        
        _gsAlarmConfig[ALARM_6].udwCounter = _sl4Byte.DwData;
        
        commTT4Cfg_FastReadCfgEeprom(OFFSET_ALARM_6_THRESHHOLD_VALUE,(void *)&gudwMem);
        _sl4Byte.DwData = gudwMem;
        _sl4Byte.Byte.B3 = 0; // clear the 4th byte since it is a 3 byte data info only..        
        _gsAlarmConfig[ALARM_6].udwThreshhold = _sl4Byte.DwData;
       
    
}


/*******************************************************************************
**  Function     : commTT4Cfg_Load_PasswordConfig_Values
**  Description  : none
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/   
void commTT4Cfg_Load_PasswordConfig_Values(void){
       
    //-------------------------------------------------------------   
    // Password Configuration Page default values.....
    //-------------------------------------------------------------              

}

/*******************************************************************************
**  Function     :
**  Description  :
**  PreCondition :
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  021214  Derrick L. Ezenwelu
*******************************************************************************/

void commTT4Cfg_Load_UserName_Values(void) {

    //-------------------------------------------------------------
    //User Name default values.....
    //-------------------------------------------------------------
        memset(_gsUser_Configuration_Area.uwUserName, 0x00, sizeof(_gsUser_Configuration_Area.uwUserName));
	commTT4Cfg_FastReadCfgEeprom(OFFSET_USERNAME,&_gsUser_Configuration_Area.uwUserName[0]);
       
        if (_gsUser_Configuration_Area.uwUserName[0] == 0xFF)//if nothing in the location then set the userName to NULL
            memset(_gsUser_Configuration_Area.uwUserName, 0x00, sizeof(_gsUser_Configuration_Area.uwUserName));

}

/*******************************************************************************
**  Function     : commTT4Cfg_Load_ThreePointCertifate_Values
**  Description  : none
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/   
void commTT4Cfg_Load_ThreePointCertifate_Values(void){

    //-------------------------------------------------------------   
    // Three Point Certificate Area default values.....
    //-------------------------------------------------------------            

}


/*******************************************************************************
**  Function     : commTT4Cfg_LoadMem_EEPROM_ConfigurationStatus
**  Description  : none
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void commTT4Cfg_LoadMem_EEPROM_ConfigurationStatus(void){

        
    //-------------------------------------
    //  Load All EEPROM Status Data...
    //-------------------------------------   
    
    commTT4Cfg_Load_SensitechConfig_Values();
    commTT4Cfg_Load_UserConfig_Values();
    commTT4Cfg_Load_UserInformation_Values();    
    commTT4Cfg_Load_AlarmConfig_Values();
    commTT4Cfg_Load_UserName_Values();
    commTT4Cfg_Load_PasswordConfig_Values();
    commTT4Cfg_Load_ThreePointCertifate_Values();
    
}    

/*******************************************************************************
**  Function     : commTT4Cfg_WriteCfgEeprom
**  Description  : EEPROM byte write option for configuration data. 
**  PreCondition : none
**  Side Effects : none
**  Input        : par_udwEEData - EEPROM offset address.
**                 par_ubData - byte data to write
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void commTT4Cfg_WriteCfgEeprom(UINT32 par_udwEEData, UINT8 *par_ubData){

    devEEPROM_PageWrite(EE_OFFSET(par_udwEEData),par_ubData,EE_BYTES(par_udwEEData));
    utilDelay_ms(10);
}

/*******************************************************************************
**  Function     : commTT4Cfg_FastWriteCfgEeprom
**  Description  : EEPROM any data format write option for configuration data. 
**  PreCondition : none
**  Side Effects : none
**  Input        : par_udwEEData - EEPROM offset address.
**                 par_ubData - byte data to write
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void commTT4Cfg_FastWriteCfgEeprom(UINT32 par_udwEEData, const void *par_ubData){
    
    UINT8 *lubDataPtr;
    
    lubDataPtr = (UINT8 *)par_ubData;
    
    devEEPROM_PageWrite(EE_OFFSET(par_udwEEData),lubDataPtr,EE_BYTES(par_udwEEData));
    utilDelay_ms(10);
}


/*******************************************************************************
**  Function     : commTT4Cfg_FastReadCfgEeprom
**  Description  : EEPROM any data format reading option for configuration data. 
**  PreCondition : none
**  Side Effects : none
**  Input        : par_udwEEData - EEPROM offset address.
**                 par_ubData - byte data to read
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void commTT4Cfg_FastReadCfgEeprom(UINT32 par_udwEEData, void *par_vData){
    
    UINT8  lubDataTemp;
    UINT8  *lubDataPtr;
    
    register UINT16 luwStartingOffset;
    register UINT8  lubNumberOfBytes;
    register UINT8  lubByteCtr;
    
    lubDataPtr = (UINT8 *)par_vData;
    
    // decode starting offset...
    luwStartingOffset = EE_OFFSET(par_udwEEData);   
    
    // decode number of bytes...
    lubNumberOfBytes = EE_BYTES(par_udwEEData);
    
    lubByteCtr = 0;
    
    do{
        
        // fetch eeprom data...
        devEEPROM_RandomRead(luwStartingOffset,&lubDataTemp);
        
        // load eeprom data to pointer data..
        *lubDataPtr = lubDataTemp;
        
        // increment pointer address...
        ++lubDataPtr;
        
        // increment EEPROM offset address..
        ++luwStartingOffset;
        
        ++lubByteCtr;
        
    }while(lubByteCtr<lubNumberOfBytes);
}



/*******************************************************************************
**  Function     : commTT4Cfg_ReadCfgEeprom
**  Description  : EEPROM byte format reading option for configuration data. 
**  PreCondition : none
**  Side Effects : none
**  Input        : par_udwEEData - EEPROM offset address.
**                 par_ubData - byte data to read
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void commTT4Cfg_ReadCfgEeprom(UINT32 par_udwEEData, UINT8 *par_ubData){
    
    UINT8  lubDataTemp;
    register UINT16 luwStartingOffset;
    register UINT8  lubNumberOfBytes;
    register UINT8  lubByteCtr;
    
    // decode starting offset...
    luwStartingOffset = EE_OFFSET(par_udwEEData);
    // decode number of bytes...
    lubNumberOfBytes = EE_BYTES(par_udwEEData);
    
    lubByteCtr = 0;
    
    do{
        
        // fetch eeprom data...
        devEEPROM_RandomRead(luwStartingOffset,&lubDataTemp);
        
        // load eeprom data to pointer data..
        *par_ubData = lubDataTemp;
        
        // increment pointer address...
        ++par_ubData;
        
        // increment EEPROM offset address..
        ++luwStartingOffset;
        
        ++lubByteCtr;
        
    }while(lubByteCtr<lubNumberOfBytes);
}


/*******************************************************************************
**  Function     : commTT4Cfg_WriteEeprom_RawDataPoint
**  Description  : write's the datapoint to specific EEPROM offset address
**  PreCondition : the temperature data to be written should be Sensitech format
**  Side Effects : none
**  Input        : par_uwDataPointNumber - datapoint number
**                 par_uwRawData - Sensitech binary temperature data format 
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void commTT4Cfg_WriteEeprom_RawDataPoint(UINT16 par_uwDataPointNumber, UINT16 par_uwRawData){
    
    UINT8 lubData[2] = {0,0};    
    
    _sTT4EEDATAFORMAT _sDataPoint;
    
    // upload to EEPROM datapoints cache internal memory...
    guwEdsCacheMem[par_uwDataPointNumber] = par_uwRawData;
    
    // write to EEPROM external memory as permanent storage...
    _sDataPoint.uwWordData = par_uwRawData;
    
    // extract and load MSB ...
    lubData[0] = (UINT8)(_sDataPoint.uwWord.Lo); 
    // extract and load LSB ...
    lubData[1] = (UINT8)(_sDataPoint.uwWord.Hi);

    devEEPROM_PageWrite(OFFSET_DATAPOINTS_STARTING_ADDRRESS+(par_uwDataPointNumber*OFFSET_DATAPOINTS_DATA_SIZE),(UINT8 *)&lubData,OFFSET_DATAPOINTS_DATA_SIZE);    
    
    utilDelay_ms(6);
}


/*******************************************************************************
**  Function     : commTT4Cfg_ReadEeprom_RawDataPoint
**  Description  : reads's the datapoint from a specific EEPROM offset address
**  PreCondition : the temperature data to be written should be Sensitech format
**  Side Effects : none
**  Input        : par_uwDataPointNumber - datapoint number
**  Output       : par_uwRawData - Sensitech binary temperature data format 
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void commTT4Cfg_ReadEeprom_RawDataPoint(UINT16 par_uwDataPointNumber, UINT16 *par_uwRawData){

    _sTT4EEDATAFORMAT _sDataPoint;
    UINT8 lubData;
    
    // LSB..
    devEEPROM_RandomRead(OFFSET_DATAPOINTS_STARTING_ADDRRESS+(par_uwDataPointNumber*OFFSET_DATAPOINTS_DATA_SIZE),&lubData);
    _sDataPoint.uwWord.Lo = lubData;
    // MSB..
    devEEPROM_RandomRead(OFFSET_DATAPOINTS_STARTING_ADDRRESS+((par_uwDataPointNumber*OFFSET_DATAPOINTS_DATA_SIZE)+1),&lubData);
    _sDataPoint.uwWord.Hi = lubData;
        
    *par_uwRawData = _sDataPoint.uwWordData;
}

/*
// for debugging purposes only...  this is a dead code!... mjm..
void commTT4Cfg_CacheMem_UploadEepromDatapoints(void){
    
    register UINT16 luwCtr;        
    register UINT8 lbToggle = 1; //debug..
    
    TT4_RED_LED_ON;
    
    luwCtr = 0;
    do{ 
               
        //commTT4Cfg_ReadEeprom_RawDataPoint(luwCtr,&luwRawDataPoint);  
        //guwEdsCacheMem[luwCtr] = luwRawDataPoint;
        //++luwCtr;        
       
        if(luwCtr<150){        
            if(lbToggle==1){
                lbToggle = 0;
                guwEdsCacheMem[luwCtr] = 0xF3E8; // 100.0 F deg       
            }    
            else{
                lbToggle = 1;
                guwEdsCacheMem[luwCtr] = 0xF000; // 0.0 F deg       
            }
        }
        else{
            if(lbToggle==1){
                lbToggle = 0;
                guwEdsCacheMem[luwCtr] = 0x03E8; // 100.0 F deg       
            }    
            else{
                lbToggle = 1;
                guwEdsCacheMem[luwCtr] = 0x0000; // 0.0 F deg       
            }        
        }
         
                
            //if(lbToggle==1){
            //    lbToggle = 0;
            //    guwEdsCacheMem[luwCtr] = 0x03E8; // 100.0 F deg       
            //}    
            //else{
            //    lbToggle = 1;
            //    guwEdsCacheMem[luwCtr] = 0x0000; // 0.0 F deg       
            //}        
        
        
        ++luwCtr;
        
    }while(luwCtr < _gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints);    
    
    TT4_RED_LED_OFF;
    
}
*/


/*******************************************************************************
**  Function     : commTT4Cfg_CacheMem_GetDataPointNum
**  Description  : acquires the binary Sensitech temperature data from cache mem
**  PreCondition : none
**  Side Effects : none
**  Input        : par_DataPointNumber - datapoint number to read from EDS RAM
**  Output       : returns the two bytes of Sensitech binary temperature data  
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
UINT16 commTT4Cfg_CacheMem_GetDataPointNum(UINT16 par_DataPointNumber){

    return(guwEdsCacheMem[par_DataPointNumber]);
}

/*******************************************************************************
**  Function     : commTT4Cfg_RetrieveTempDataFromEeprom
**  Description  : retrieves the temperature datapoint value
**  PreCondition : none
**  Side Effects : none
**  Input        : par_uwDataPointNumber - datapoint number
**  Output       : par_ubMarkerStatus - temperature sign value 
**                 par_fTempData - Sensitech binary temperature data
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void commTT4Cfg_RetrieveTempDataFromEeprom(UINT16 par_uwDataPointNumber, UINT8 *par_ubMarkerStatus, float *par_fTempData){
    
    UINT16 luwBitValue;        
    _sTT4EEDATAFORMAT _sDataPoint;   
    
    commTT4Cfg_ReadEeprom_RawDataPoint(par_uwDataPointNumber,&luwBitValue);
    
    _sDataPoint.uwWordData = luwBitValue;
    
    *par_fTempData = commTT4Cfg_ConvertToFinalTempData(_sDataPoint.uwEeprom.Temperature);
    
    *par_ubMarkerStatus = (UINT8)_sDataPoint.uwEeprom.Marker;
}


/*******************************************************************************
**  Function     : commTT4Cfg_CacheMem_RetrieveTempData
**  Description  : 
**  PreCondition : 
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void commTT4Cfg_CacheMem_RetrieveTempData(UINT16 par_uwDataPointNumber, UINT8 *par_ubMarkerStatus, float *par_fTempData){
    
    UINT16 luwBitValue;        
    _sTT4EEDATAFORMAT _sDataPoint;       
    
    luwBitValue = guwEdsCacheMem[par_uwDataPointNumber];
    
    _sDataPoint.uwWordData = luwBitValue;
    
    *par_fTempData = commTT4Cfg_ConvertToFinalTempData(_sDataPoint.uwEeprom.Temperature);
    
    *par_ubMarkerStatus = (UINT8)_sDataPoint.uwEeprom.Marker;
}

/*******************************************************************************
**  Function     : commTT4Cfg_ConvertToFinalTempData
**  Description  : 
**  PreCondition : 
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
float commTT4Cfg_ConvertToFinalTempData(UINT16 par_uwBitValue){
    
    UINT16 luwBitValue;        
    float fFahrenheitValue;
    _sTT4EEDATAFORMAT _sDataPoint;   
    
    _sDataPoint.uwWordData = par_uwBitValue;
    
    if(_sDataPoint.uwSign.Negative == 1){
        
        luwBitValue = ~(_sDataPoint.uwEeprom.Temperature - 1) & 0x0FFF;
        
        fFahrenheitValue = (float)luwBitValue / -10.0;            
    }
    else{
            
        fFahrenheitValue = (float)_sDataPoint.uwEeprom.Temperature / 10.0;
    }
    
    return(fFahrenheitValue);
}

/*******************************************************************************
**  Function     : commTT4Cfg_USBLoadSerialNumber
**  Description  : write's serial number to USB descriptor.
**  PreCondition : must initialize first the USB module
**  Side Effects : none
**  Input        : par_udwTT4MA_SN - 4 bytes serial number data
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void commTT4Cfg_USBLoadSerialNumber(UINT32 par_udwTT4MA_SN){
    
    char lstrSN[12];
    
    _gsTT4USBMA_SerialNumber.bLength = sizeof(_gsTT4USBMA_SerialNumber);
    _gsTT4USBMA_SerialNumber.bDscType = 0x03;    
    utilULongNumToString(par_udwTT4MA_SN,(UINT8 *)&lstrSN[0]); // 10 digits...
    utilByteToWordString((UINT16 *)&_gsTT4USBMA_SerialNumber.string[0],(UINT8 *)&lstrSN[0],10); // 10 digits...
}    

/*******************************************************************************
**  Function     : commTT4Cfg_USBLoadStringSerialNumber
**  Description  : converts USB serial number string data.
**  PreCondition : must write first the SN to USB descriptor before calling this routine.
**  Side Effects : none
**  Input        : par_udwTT4MA_SN - SN string data
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void commTT4Cfg_USBLoadStringSerialNumber(char *par_strTT4MA_SN){
    
    char lstrSN[12];
    UINT8 ubCtr; 
    
    _gsTT4USBMA_SerialNumber.bLength = sizeof(_gsTT4USBMA_SerialNumber);
    _gsTT4USBMA_SerialNumber.bDscType = 0x03;    
    
    ubCtr = 0;
    do{
        lstrSN[ubCtr] = par_strTT4MA_SN[ubCtr];
        ++ubCtr;
    }while(ubCtr<10);
    lstrSN[10] = 0;
    
    utilByteToWordString((UINT16 *)&_gsTT4USBMA_SerialNumber.string[0],(UINT8 *)&lstrSN[0],10); // 10 digits...
}    


/*******************************************************************************
**  Function     : commTT4Cfg_Initialize
**  Description  : none
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void commTT4Cfg_Initialize(void){
    
}


/*******************************************************************************
**  Function     : commTT4Cfg_EERPOM_PostDataLogging
**  Description  : logs all the TT4-MA status after stopping the device
**  PreCondition : this routine must called whenever the device is Stopped 
**                 or memory full.
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void commTT4Cfg_EERPOM_PostDataLogging(void){
        
    // log device stop time stamp...
    if((_gsGenCfgOptByte.GenOpt.Byte1.MultDropFlag==1)&&(gubStopDeviceStatus!=_DEVICE_STOP_STATUS_SLEEP_))
        _gsUser_Configuration_Area.udwUnit_Stop_TimeStamp = _gsUser_Configuration_Area.udwUnit_Start_TimeStamp + ((UINT32)(_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints-1) * _gsUser_Configuration_Area.udwMeasurementInterval);
    else
        _gsUser_Configuration_Area.udwUnit_Stop_TimeStamp =  gudwDeviceStoppedDownloadTime; // new marketing requirement to use the actual stop time, requested by Joseph Zhao 04192013... mjm
   
    // log device stopped time stamp...    
    gudwMem = _gsUser_Configuration_Area.udwUnit_Stop_TimeStamp;
    commTT4Cfg_FastWriteCfgEeprom(OFFSET_UNIT_STOP_TIMESTAMP,(const void *)&gudwMem);
    
    // log alarm status registers...    
    gubMem = _gsUser_Configuration_Area.ubAlarmStatus; 
    commTT4Cfg_FastWriteCfgEeprom(OFFSET_ALARM_STATUS,(const void *)&gubMem);

    // log all alarm counter register...    
    gudwMem = _gsAlarmConfig[ALARM_1].udwCounter;
    commTT4Cfg_FastWriteCfgEeprom(OFFSET_ALARM_1_COUNTER,(const void *)&gudwMem);
    gudwMem = _gsAlarmConfig[ALARM_2].udwCounter;
    commTT4Cfg_FastWriteCfgEeprom(OFFSET_ALARM_2_COUNTER,(const void *)&gudwMem);
    gudwMem = _gsAlarmConfig[ALARM_3].udwCounter;
    commTT4Cfg_FastWriteCfgEeprom(OFFSET_ALARM_3_COUNTER,(const void *)&gudwMem);
    gudwMem = _gsAlarmConfig[ALARM_4].udwCounter;
    commTT4Cfg_FastWriteCfgEeprom(OFFSET_ALARM_4_COUNTER,(const void *)&gudwMem);
    gudwMem = _gsAlarmConfig[ALARM_5].udwCounter;
    commTT4Cfg_FastWriteCfgEeprom(OFFSET_ALARM_5_COUNTER,(const void *)&gudwMem);
    gudwMem = _gsAlarmConfig[ALARM_6].udwCounter;
    commTT4Cfg_FastWriteCfgEeprom(OFFSET_ALARM_6_COUNTER,(const void *)&gudwMem);        
    
}

/*******************************************************************************
**  Function     : commTT4Cfg_ClearEEPROMSpecificData
**  Description  : clears specific structure data that would written to EEPROM.
**  PreCondition : this must be called only during the device just started prior 
**                 entering to the main core loop.
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void commTT4Cfg_ClearEEPROMSpecificData(void){
    
    // claer alarm status byte
    _gsUser_Configuration_Area.ubAlarmStatus = 0;

    // clear all alarm counter registers...
    _gsAlarmConfig[ALARM_1].udwCounter = 0;
    _gsAlarmConfig[ALARM_2].udwCounter = 0;
    _gsAlarmConfig[ALARM_3].udwCounter = 0;
    _gsAlarmConfig[ALARM_4].udwCounter = 0;
    _gsAlarmConfig[ALARM_5].udwCounter = 0;
    _gsAlarmConfig[ALARM_6].udwCounter = 0;
    
    // clear all number of events counter registers...
    _guwTrigEvents[ALARM_1].udwTotalTime = 0;
    _guwTrigEvents[ALARM_1].uwNumOfEvents = 0;
    _guwTrigEvents[ALARM_1].bStatusFlag = TRUE;
    _guwTrigEvents[ALARM_2].udwTotalTime = 0;
    _guwTrigEvents[ALARM_2].uwNumOfEvents = 0;
    _guwTrigEvents[ALARM_2].bStatusFlag = TRUE;
    _guwTrigEvents[ALARM_3].udwTotalTime = 0;
    _guwTrigEvents[ALARM_3].uwNumOfEvents = 0;
    _guwTrigEvents[ALARM_3].bStatusFlag = TRUE;
    _guwTrigEvents[ALARM_4].udwTotalTime = 0;
    _guwTrigEvents[ALARM_4].uwNumOfEvents = 0;
    _guwTrigEvents[ALARM_4].bStatusFlag = TRUE;
    _guwTrigEvents[ALARM_5].udwTotalTime = 0;
    _guwTrigEvents[ALARM_5].uwNumOfEvents = 0;
    _guwTrigEvents[ALARM_5].bStatusFlag = TRUE;
    _guwTrigEvents[ALARM_6].udwTotalTime = 0;
    _guwTrigEvents[ALARM_6].uwNumOfEvents = 0;
    _guwTrigEvents[ALARM_6].bStatusFlag = TRUE;
    // clear ideal temp logging time logging...
    _guwTrigEvents[IDEAL_TEMP].udwTotalTime = 0; // if this is missing Ideal Temp Range calculation goes wrong.. 071311 mjm
    _guwTrigEvents[IDEAL_TEMP].uwNumOfEvents = 0;    
    _guwTrigEvents[IDEAL_TEMP].bStatusFlag = TRUE;

    
    // clear all alarm time stamps...
    _gsUser_Configuration_Area.udwAlarm1_TriggeredTimeStamp = 0;
    _gsUser_Configuration_Area.udwAlarm2_TriggeredTimeStamp = 0;
    _gsUser_Configuration_Area.udwAlarm3_TriggeredTimeStamp = 0;
    _gsUser_Configuration_Area.udwAlarm4_TriggeredTimeStamp = 0;
    _gsUser_Configuration_Area.udwAlarm5_TriggeredTimeStamp = 0;
    _gsUser_Configuration_Area.udwAlarm6_TriggeredTimeStamp = 0;
    
    // alarm remaining time/elapsed...
    gudwAlarmTimeRemainingElapsed = 0;    
    gbAlarmTimeFlag = TRUE;
    
}


    
/*   End of File   */



