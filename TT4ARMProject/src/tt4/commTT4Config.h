/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm                                            **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc. and Blue Ocean Innovation Ltd.                            **
 **                                                                           **
 **  Copryright(c) 2010. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Blue Ocean Innovation Ltd.                        **
 **  Project#            :  449                                               **
 **  Creation date       :  08/20/2010                                        **
 **  Programmer          :  Michael J. Mariveles                              **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : commTT4Config.h                                                   **
 **                                                                           **
 **  Description :                                                            **
 **    - Setups all EEPROM configuration data.                                **
 **    - Sensitech,User,Alarm configuration area were taken here.             **
 **    - EEPROM formatted read/write routine handlers.                        **
 **    - TT4USB-MA configuration status loader.                               **
 **    - Post data logging routine after the device is stopped or taking last **
 **      point in multi-drop.                                                 **
 **    - USB serial number loader.                                            **
 **                                                                           **
 ******************************************************************************/

#ifndef __COMMTT4CONFIG_H
#define __COMMTT4CONFIG_H

//---------------------------------------
// LCD POR Firmware version number...
//---------------------------------------

#define TT4USB_MA_SW_VERSION_1    0     // increment header version number if used for improvement counts, must set the ENA_HEADER_VERSION_NUM...
#define TT4USB_MA_SW_VERSION_2    56    // increment this number for a bug count in version number...
// Official Version : v1.00.054
// release date: 10302013
// by: Micahel J. Mariveles

// display topmost LCD version number..
#define ENA_HEADER_VERSION_NUM    0  // 0-disable : 1-enable

// debug version display information...
#define DEBUG_VERSION   1 // clear this in final version..., if this is enabled then it shows a (-) minus sign reset mode version number...
#define DEBUG_NUMBER    9 // must increment this numner when a newly released debug version
                          // the last debug version checksum is 0xE88B as  -47(2) .. note by: mjmariveles 12192012..


//////////////////////////////////////////////////////////////////
//  
//  SOME DEFINED COMMUNICATION CODES..
//
//////////////////////////////////////////////////////////////////

// Host login status...
#define HOST_NOT_YET_LOGIN                  0x00        // 1 Byte
#define HOST_LOGIN_SUCCESSFUL               0x01        // 1 Byte
#define COMM_HDSK_TASK_DONE                 0x01        // 1 Byte
#define COMM_HDSK_TASK_NOT_DONE             0x00        // 1 Byte

// Handshake access levels...
#define HDSK_ACCESS_LEVEL1                  0x00        // 1 Byte
#define HDSK_ACCESS_LEVEL2                  0x01        // 1 Byte
#define HDSK_ACCESS_LEVEL3                  0x02        // 1 Byte

// Command access levels...
#define CMD_ACCESS_LEVEL1                   0x03        // 1 Byte
#define CMD_ACCESS_LEVEL2                   0x04        // 1 Byte
#define CMD_ACCESS_LEVEL3                   0x05        // 1 Byte

// Command request - 1st access level  
#define TT4CMD_STATUS_LEVEL1_OPEN           0x00
#define TT4CMD_STATUS_LEVEL1_CLOSE          0x01
// Paramater value - 2nd access level 
#define TT4CMD_STATUS_LEVEL2_OPEN           0x03
#define TT4CMD_STATUS_LEVEL2_CLOSE          0x04
//  value - 3rd access level 
#define TT4CMD_STATUS_LEVEL3_OPEN           0x05
#define TT4CMD_STATUS_LEVEL3_CLOSE          0x06

// TT4 process comamnd status codes..   
#define TT4C_PROC_CMD_STAT_DONE             0x00
#define TT4C_PROC_CMD_STAT_NOT_DONE         0x01
#define TT4C_PROC_CMD_STAT_OPEN             0x02
#define TT4C_PROC_CMD_STAT_CLOSE            0x03
#define TT4C_PROC_CMD_STAT_IN               0x04
#define TT4C_PROC_CMD_STAT_OUT              0x05



//////////////////////////////////////////////////////////////////
//  
//  SENSITECH OFFICIAL COMMUNICATION PROTOCOL CODES..
//
//////////////////////////////////////////////////////////////////

//===============================================================
// Handshake Protocol Command Codes...
//===============================================================
#define HDSK_RCV_OK                         0x00        // 1 Byte
#define HDSK_SEND_PROTOCOL_ID               0x0006      // 2 Bytes
#define HDSK_RCV_ACK                        0x55        // 1 Byte
#define HDSK_SEND_ACK_BCK                   0x00        // 1 Byte
#define HDSK_RCV_4B_LOGIN_VAL               0x7A5E8AA1  // 4 Bytes
#define HDSK_NOT_COMPLETE                   0x00        // 1 Byte

//===============================================================
// Sensitech TT4 Command status list of valid status Codes...
//===============================================================
#define TT4_CMD_SUCCESSFUL                  0x00
#define TT4_CMD_UNKNOWN                     0x02
#define TT4_BAD_INIT                        0x07
#define TT4_BAD_PARAM                       0x11

//===============================================================
//   Sensitech TT4 Commands codes...
//===============================================================

// TT4 Command Set... (Original)
#define TT4CMD_PACKETZED_DWNLD              0x20
#define TT4CMD_PRI_SEC_DATA_ONLY_DWNLD      0x2A
#define TT4CMD_WRITE_CERT_DATA              0x2B
#define TT4CMD_READ_CERT_DATA               0x2C
#define TT4CMD_PACKETZED_DWNLD_MA           0x2D
#define TT4CMD_GET_ID                       0x33
#define TT4CMD_SKIP_ID                      0x37
#define TT4CMD_TERMINATE_COMM               0x38
#define TT4CMD_MOVE_UNIT_STAT_SP            0x44
#define TT4CMD_WRITE_SP                     0x45
#define TT4CMD_READ_SP                      0x46
#define TT4CMD_LOGIN_SENSITECH              0x47
#define TT4CMD_LOGIN_USER                   0x48
#define TT4CMD_C0PY_SP_TO_ST_CFG            0x50
#define TT4CMD_C0PY_SP_TO_DAT_STOR          0x51
#define TT4CMD_RESET_UNIT                   0x53
#define TT4CMD_COPY_SP_TO_USER_CFG          0x60
#define TT4CMD_COPY_USER_CFG_TO_SP          0x61
#define TT4CMD_COPY_SP_TO_USER_INFO         0x62
#define TT4CMD_COPY_USER_INFO_TO_SP         0x63
#define TT4CMD_COPY_TEMP_DATA_PG_TO_SP      0x64
#define TT4CMD_SET_CLOCK                    0x65
#define TT4CMD_CLR_DATA_MEM_PTR             0x66
#define TT4CMD_INIT_UNIT                    0x67
#define TT4CMD_START_UNIT                   0x68
#define TT4CMD_STOP_UNIT                    0x69
#define TT4CMD_COPY_ST_CFG_PG_TO_SP         0x6A
#define TT4CMD_HI_SPEED_DATA_DWNLD          0x6E

//===============================================================
// TT4 Multi-Alarm Command Set... (new commands)
//===============================================================
#define TT4CMD_COPY_ALM_CFG_PG_TO_SP        0x54
#define TT4CMD_COPY_SP_CONT_TO_ALM_CFG_PG   0x55
#define TT4CMD_COPY_PSWD_PG_TO_SP           0x56
#define TT4CMD_COPY_SP_CONT_TP_PSWD_PG      0x57
#define TT4CMD_COPY_3PT_CERT_PG_TO_SP       0x58
#define TT4CMD_SP_CONT_TO_3PT_CERT_PG       0x59
#define TT4CMD_READ_1B_FROM_RAM_MEM         0x72
#define TT4CMD_WRITE_1B_TO_RAM_MEM          0x73
#define TT4CMD_EEPROM_READ_COMMAND          0x74
#define TT4CMD_EEPROM_WRITE_COMMAND         0x75
#define TT4CMD_GET_MONI_INFO_COMMAND        0x76


#define SHIPPING_LABEL_SIZE            20
#define SHIPPING_LABEL_VALUE_SIZE      32
#define NO_OF_LABEL                    10
//===================================================================================
//
//   TT4 Multi-Alarm EERPOM Data definitions...   
//                          
//   Reference file: TT4_MultiAlarm_Memory_Map_7-29-2010.xls
//   Provided by: Sensitech 
//
//   \mjmariveles  08272010 
//
//===================================================================================
// 2 Bytes Format : 0xHHLL
// MSB : 0xHH - Offset
// LSB : 0xLL - Number of Bytes
#define EEHIGHTBYTEPOSITION  8

//-----------------------------------------------------------------------------------
//      Sensitech Configuration Area... 
//-----------------------------------------------------------------------------------
// Protocol ID Offset and Size definition...
#define L_BYTES1             2ul
#define H_OFFSET1            0ul
#define OFFSET_PROTOCOL_ID                   (H_OFFSET1<<EEHIGHTBYTEPOSITION)|(L_BYTES1)

// Firmware version number Offset and Size definition...
#define L_BYTES2             2ul
#define H_OFFSET2            (H_OFFSET1+L_BYTES1)
#define OFFSET_FIRMWARE_VER                  (H_OFFSET2<<EEHIGHTBYTEPOSITION)|(L_BYTES2)

// Manufacturing Serial Number Offset and Size definition...
#define L_BYTES3             4ul
#define H_OFFSET3            (H_OFFSET2+L_BYTES2)
#define OFFSET_MANUFACTURING_SERIAL_NUM      (H_OFFSET3<<EEHIGHTBYTEPOSITION)|(L_BYTES3)

// Number of Customer Reset Offset and Size definition...
#define L_BYTES4             2ul
#define H_OFFSET4            (H_OFFSET3+L_BYTES3)
#define OFFSET_CUSTOMER_RESETS               (H_OFFSET4<<EEHIGHTBYTEPOSITION)|(L_BYTES4)

// User Information Character Set Offset and Size definition...
#define L_BYTES5             1ul
#define H_OFFSET5            (H_OFFSET4+L_BYTES4)
#define OFFSET_USER_INFO_CHARSET             (H_OFFSET5<<EEHIGHTBYTEPOSITION)|(L_BYTES5)

// Not Used Offset and Size definition...
#define L_BYTES6             2ul
#define H_OFFSET6            (H_OFFSET5+L_BYTES5)
#define OFFSET_NOT_USED_0                    (H_OFFSET6<<EEHIGHTBYTEPOSITION)|(L_BYTES6)

// Sensor Type ID Offset and Size definition...
#define L_BYTES7             1ul
#define H_OFFSET7            (H_OFFSET6+L_BYTES6)
#define OFFSET_SENSOR_TYPE_ID                (H_OFFSET7<<EEHIGHTBYTEPOSITION)|(L_BYTES7)

// Size of EEPROM Offset and Size definition...
#define L_BYTES8             1ul
#define H_OFFSET8            (H_OFFSET7+L_BYTES7)
#define OFFSET_EEPROM_SIZE                   (H_OFFSET8<<EEHIGHTBYTEPOSITION)|(L_BYTES8)

// Sensor Config Primary Offset and Size definition...
#define L_BYTES9             1ul
#define H_OFFSET9            (H_OFFSET8+L_BYTES8)
#define OFFSET_SENSOR_CONFIG_PRIMARY         (H_OFFSET9<<EEHIGHTBYTEPOSITION)|(L_BYTES9)

// Number of stored data points Offset and Size definition...
#define L_BYTES10            2ul
#define H_OFFSET10           (H_OFFSET9+L_BYTES9)
#define OFFSET_NUMBER_OF_STORED_DATAPOINTS   (H_OFFSET10<<EEHIGHTBYTEPOSITION)|(L_BYTES10)

// Sensor Config Secondary Offset and Size definition...
#define L_BYTES11            1ul
#define H_OFFSET11           (H_OFFSET10+L_BYTES10)
#define OFFSET_SENSOR_CONFIG_SECONDARY       (H_OFFSET11<<EEHIGHTBYTEPOSITION)|(L_BYTES11)

// First Activation Timestamp Offset and Size definition...
#define L_BYTES12            4ul
#define H_OFFSET12           (H_OFFSET11+L_BYTES11)
#define OFFSET_FIRST_ACTIVATION_TIMESTAMP    (H_OFFSET12<<EEHIGHTBYTEPOSITION)|(L_BYTES12)

// Time zone shift byte
#define L_BYTES_TZ            1ul
#define H_OFFSET_TZ           (H_OFFSET12+L_BYTES12)
#define OFFSET_TIMEZONE                     (H_OFFSET_TZ<<EEHIGHTBYTEPOSITION)|(L_BYTES_TZ)

// Not used 1 Offset and Size definition...
#define L_BYTES13            1ul
#define H_OFFSET13           (H_OFFSET_TZ+L_BYTES_TZ)
#define OFFSET_NOT_USED_1                    (H_OFFSET13<<EEHIGHTBYTEPOSITION)|(L_BYTES13)

// Extended option byte1 Offset and Size definition...
#define L_BYTES14            1ul
#define H_OFFSET14           (H_OFFSET13+L_BYTES13)
#define OFFSET_EXTENDED_BYTE_OPTION_1        (H_OFFSET14<<EEHIGHTBYTEPOSITION)|(L_BYTES14)

// Extended option byte2 Offset and Size definition...
#define L_BYTES15            1ul
#define H_OFFSET15           (H_OFFSET14+L_BYTES14)
#define OFFSET_EXTENDED_BYTE_OPTION_2        (H_OFFSET15<<EEHIGHTBYTEPOSITION)|(L_BYTES15)

// Timestamp configuration time Offset and Size definition...
#define L_BYTES16            4ul
#define H_OFFSET16           (H_OFFSET15+L_BYTES15)
#define OFFSET_TIMESTAMP_CONFIG_TIME         (H_OFFSET16<<EEHIGHTBYTEPOSITION)|(L_BYTES16)

// Command byte Offset and Size definition...
#define L_BYTES17            1ul
#define H_OFFSET17           (H_OFFSET16+L_BYTES16)
#define OFFSET_COMMAND_BYTE                  (H_OFFSET17<<EEHIGHTBYTEPOSITION)|(L_BYTES17)

// 12 byte alphanumeric serial number (for future use) offset and Size definition...
#define L_BYTES18            12ul
#define H_OFFSET18           (H_OFFSET17+L_BYTES17)
#define OFFSET_12BYTE_ALPHNUM_SERIAL_NUMBER  (H_OFFSET18<<EEHIGHTBYTEPOSITION)|(L_BYTES18)

// Sensor 1 Calibration Offset range -22F to 0F...
#define L_BYTES19            1ul
#define H_OFFSET19           (H_OFFSET18+L_BYTES18)
#define SENSOR1_CAL_OFFSET_A                 (H_OFFSET19<<EEHIGHTBYTEPOSITION)|(L_BYTES19)

// Sensor 1 Calibration Offset -0F to 122F...
#define L_BYTES20            1ul
#define H_OFFSET20           (H_OFFSET19+L_BYTES19)
#define SENSOR1_CAL_OFFSET_B                 (H_OFFSET20<<EEHIGHTBYTEPOSITION)|(L_BYTES20)

// Sensor 1 Calibration Offset 122F to 158F...
#define L_BYTES21            1ul
#define H_OFFSET21           (H_OFFSET20+L_BYTES20)
#define SENSOR1_CAL_OFFSET_C                 (H_OFFSET21<<EEHIGHTBYTEPOSITION)|(L_BYTES21)

// Not used 3 Offset and Size definition...
#define L_BYTES22            17ul
#define H_OFFSET22           (H_OFFSET21+L_BYTES21)
#define OFFSET_NOT_USED_3                    (H_OFFSET22<<EEHIGHTBYTEPOSITION)|(L_BYTES22)


//-----------------------------------------------------------------------------------
//      User Configuration Area...      
//-----------------------------------------------------------------------------------

// Measurement Interval Offset and Size definition...
#define L_BYTES23            3ul
#define H_OFFSET23           (H_OFFSET22+L_BYTES22)
#define OFFSET_MEASUREMENT_INTERVAL          (H_OFFSET23<<EEHIGHTBYTEPOSITION)|(L_BYTES23)

// Startup delay Offset and Size definition...
#define L_BYTES24            3ul
#define H_OFFSET24           (H_OFFSET23+L_BYTES23)
#define OFFSET_STARTUP_DELAY                 (H_OFFSET24<<EEHIGHTBYTEPOSITION)|(L_BYTES24)

// General Configuration options byte0 Offset and Size definition...
#define L_BYTES25            1ul
#define H_OFFSET25           (H_OFFSET24+L_BYTES24)
#define OFFSET_GENERAL_CONFIG_OPTIONS_BYTE_0 (H_OFFSET25<<EEHIGHTBYTEPOSITION)|(L_BYTES25)

// General Configuration options byte1 Offset and Size definition...
#define L_BYTES26            1ul
#define H_OFFSET26           (H_OFFSET25+L_BYTES25)
#define OFFSET_GENERAL_CONFIG_OPTIONS_BYTE_1 (H_OFFSET26<<EEHIGHTBYTEPOSITION)|(L_BYTES26)

// Software version number Offset and Size definition...
#define L_BYTES27            2ul
#define H_OFFSET27           (H_OFFSET26+L_BYTES26)
#define OFFSET_SOFTWARE_VERSION_NUMBER       (H_OFFSET27<<EEHIGHTBYTEPOSITION)|(L_BYTES27)

// Trip Number (customer programmable S/N) Offset and Size definition...
#define L_BYTES28            4ul
#define H_OFFSET28           (H_OFFSET27+L_BYTES27)
#define OFFSET_TRIP_NUMBER                   (H_OFFSET28<<EEHIGHTBYTEPOSITION)|(L_BYTES28)

// MKT Activation Energy...
#define L_BYTES_MKT          4ul
#define H_OFFSET_MKT         (H_OFFSET28+L_BYTES28)
#define OFFSET_MKT_ACT                       (H_OFFSET_MKT<<EEHIGHTBYTEPOSITION)|(L_BYTES_MKT) // added MKT constant , new marketing requirement 10312013 mjm... 

// Not used 4 Offset and Size definition...
#define L_BYTES29            14ul
//#define H_OFFSET29           (H_OFFSET28+L_BYTES28)
#define H_OFFSET29           (H_OFFSET_MKT+L_BYTES_MKT)
#define OFFSET_NOT_USED_4                    (H_OFFSET29<<EEHIGHTBYTEPOSITION)|(L_BYTES29) 

// Temperature Ideal Range High Offset and Size definition...
#define L_BYTES30            2ul
#define H_OFFSET30           (H_OFFSET29+L_BYTES29)
#define OFFSET_TEMPERATURE_IDEAL_RANGE_HIGH  (H_OFFSET30<<EEHIGHTBYTEPOSITION)|(L_BYTES30)

// Temperature Ideal Range Low Offset and Size definition...
#define L_BYTES31            2ul
#define H_OFFSET31           (H_OFFSET30+L_BYTES30)
#define OFFSET_TEMPERATURE_IDEAL_RANGE_LOW   (H_OFFSET31<<EEHIGHTBYTEPOSITION)|(L_BYTES31)

// Alarm Status Offset and Size definition...
#define L_BYTES32            1ul
#define H_OFFSET32           (H_OFFSET31+L_BYTES31)
#define OFFSET_ALARM_STATUS                  (H_OFFSET32<<EEHIGHTBYTEPOSITION)|(L_BYTES32)

// Alarm 1 Triggerred Time Stamp Offset and Size definition...
#define L_BYTES33            4ul
#define H_OFFSET33           (H_OFFSET32+L_BYTES32)
#define OFFSET_ALARM_1_TRIGGERED_TIMESTAMP   (H_OFFSET33<<EEHIGHTBYTEPOSITION)|(L_BYTES33)

// Alarm 2 Triggerred Time Stamp Offset and Size definition...
#define L_BYTES34            4ul
#define H_OFFSET34           (H_OFFSET33+L_BYTES33)
#define OFFSET_ALARM_2_TRIGGERED_TIMESTAMP   (H_OFFSET34<<EEHIGHTBYTEPOSITION)|(L_BYTES34)

// Alarm 3 Triggerred Time Stamp Offset and Size definition...
#define L_BYTES35            4ul
#define H_OFFSET35           (H_OFFSET34+L_BYTES34)
#define OFFSET_ALARM_3_TRIGGERED_TIMESTAMP   (H_OFFSET35<<EEHIGHTBYTEPOSITION)|(L_BYTES35)

// Alarm 4 Triggerred Time Stamp Offset and Size definition...
#define L_BYTES36            4ul
#define H_OFFSET36           (H_OFFSET35+L_BYTES35)
#define OFFSET_ALARM_4_TRIGGERED_TIMESTAMP   (H_OFFSET36<<EEHIGHTBYTEPOSITION)|(L_BYTES36)

// Alarm 5 Triggerred Time Stamp Offset and Size definition...
#define L_BYTES37            4ul
#define H_OFFSET37           (H_OFFSET36+L_BYTES36)
#define OFFSET_ALARM_5_TRIGGERED_TIMESTAMP   (H_OFFSET37<<EEHIGHTBYTEPOSITION)|(L_BYTES37)

// Alarm 6 Triggerred Time Stamp Offset and Size definition...
#define L_BYTES38            4ul
#define H_OFFSET38           (H_OFFSET37+L_BYTES37)
#define OFFSET_ALARM_6_TRIGGERED_TIMESTAMP   (H_OFFSET38<<EEHIGHTBYTEPOSITION)|(L_BYTES38)

// Remaining/Elapsed Alarm Display Configuration Byte definition...
#define L_BYTES39            1ul
#define H_OFFSET39           (H_OFFSET38+L_BYTES38)
#define OFFSET_RE_ALM_DISP_CONFIG_BYTE       (H_OFFSET39<<EEHIGHTBYTEPOSITION)|(L_BYTES39)

// Not Used 5 Offset and Size definition...
#define L_BYTES40            2ul
#define H_OFFSET40           (H_OFFSET39+L_BYTES39)
#define OFFSET_NOT_USED_5                    (H_OFFSET40<<EEHIGHTBYTEPOSITION)|(L_BYTES40)

// New Battery Time Stamp Offset and Size definition...
#define L_BYTES41            4ul
#define H_OFFSET41           (H_OFFSET40+L_BYTES40)
#define OFFSET_NEW_BATTERY_TIMESTAMP         (H_OFFSET41<<EEHIGHTBYTEPOSITION)|(L_BYTES41)

// Unit Start Timestamp Offset and Size definition...
#define L_BYTES42            4ul
#define H_OFFSET42           (H_OFFSET41+L_BYTES41)
#define OFFSET_UNIT_START_TIMESTAMP          (H_OFFSET42<<EEHIGHTBYTEPOSITION)|(L_BYTES42)

// Unit Stop Timestamp Offset and Size definition...
#define L_BYTES43            4ul
#define H_OFFSET43           (H_OFFSET42+L_BYTES42)
#define OFFSET_UNIT_STOP_TIMESTAMP           (H_OFFSET43<<EEHIGHTBYTEPOSITION)|(L_BYTES43)

// Not Used 6 Offset and Size definition...
#define L_BYTES44            16ul         
#define H_OFFSET44           (H_OFFSET43+L_BYTES43)
#define OFFSET_NOT_USED_6                    (H_OFFSET44<<EEHIGHTBYTEPOSITION)|(L_BYTES44)

// LCD Contrast Adjust Offset and Size definition...
#define L_BYTES45            1ul
#define H_OFFSET45           (H_OFFSET44+L_BYTES44)
#define OFFSET_LCD_CONTRAST_ADJUST           (H_OFFSET45<<EEHIGHTBYTEPOSITION)|(L_BYTES45)

// Not Used 7 Offset and Size definition...
#define L_BYTES46            3ul
#define H_OFFSET46           (H_OFFSET45+L_BYTES45)
#define OFFSET_NOT_USED_7                    (H_OFFSET46<<EEHIGHTBYTEPOSITION)|(L_BYTES46)


//-----------------------------------------------------------------------------------
//      User Information Area...      
//-----------------------------------------------------------------------------------

// User Information page 0 Offset and Size definition...
#define L_BYTES47            30ul  //temporarily using this location for now to enter the programmer' name
#define H_OFFSET47           (H_OFFSET46+L_BYTES46)
#define OFFSET_USER_INFO_PAGE_0              (H_OFFSET47<<EEHIGHTBYTEPOSITION)|(L_BYTES47)

#define L_BYTESNU             2ul
#define H_OFFSETNU           (H_OFFSET47+L_BYTES47)   //will combine this back with User info 0 at some point
#define OFFSET_USER_NOT_USED                 (H_OFFSETNU<<EEHIGHTBYTEPOSITION)|(L_BYTESNU)

// User Information page 1  Offset and Size definition...
#define L_BYTES48            32ul
#define H_OFFSET48           (H_OFFSETNU+L_BYTESNU)
#define OFFSET_USER_INFO_PAGE_1               (H_OFFSET48<<EEHIGHTBYTEPOSITION)|(L_BYTES48)

// User Information page 2  Offset and Size definition...
#define L_BYTES49            32ul
#define H_OFFSET49           (H_OFFSET48+L_BYTES48)
#define OFFSET_USER_INFO_PAGE_2               (H_OFFSET49<<EEHIGHTBYTEPOSITION)|(L_BYTES49)

// User Information page 3  Offset and Size definition...
#define L_BYTES50            32ul
#define H_OFFSET50           (H_OFFSET49+L_BYTES49)
#define OFFSET_USER_INFO_PAGE_3               (H_OFFSET50<<EEHIGHTBYTEPOSITION)|(L_BYTES50)


//-----------------------------------------------------------------------------------
//      Alarm Configuration Area...      
//-----------------------------------------------------------------------------------

// Alarm 1 Configuration Byte 0 Offset and Size definition...
#define L_BYTES51            1ul
#define H_OFFSET51           (H_OFFSET50+L_BYTES50)
#define OFFSET_ALARM_1_CONFIG_BYTE_0          (H_OFFSET51<<EEHIGHTBYTEPOSITION)|(L_BYTES51)

// Alarm 1 Configuration Byte 1 Offset and Size definition...
#define L_BYTES52            1ul
#define H_OFFSET52           (H_OFFSET51+L_BYTES51)
#define OFFSET_ALARM_1_CONFIG_BYTE_1          (H_OFFSET52<<EEHIGHTBYTEPOSITION)|(L_BYTES52)

// Alarm 1 Range#1 High temp setpoint Offset and Size definition...
#define L_BYTES53            2ul
#define H_OFFSET53           (H_OFFSET52+L_BYTES52)
#define OFFSET_ALARM_1_RANGE1_HIGH (H_OFFSET53<<EEHIGHTBYTEPOSITION)|(L_BYTES53)

// Alarm 1 Range#1 Low temp setpoint Offset and Size definition...
#define L_BYTES54            2ul
#define H_OFFSET54           (H_OFFSET53+L_BYTES53)
#define OFFSET_ALARM_1_RANGE1_LOW  (H_OFFSET54<<EEHIGHTBYTEPOSITION)|(L_BYTES54)

// Alarm 1 Range#2 High temp setpoint Offset and Size definition...
#define L_BYTES55            2ul
#define H_OFFSET55           (H_OFFSET54+L_BYTES54)
#define OFFSET_ALARM_1_RANGE2_HIGH     (H_OFFSET55<<EEHIGHTBYTEPOSITION)|(L_BYTES55)

// Alarm 1 Range#2 Low temp setpoint Offset and Size definition...
#define L_BYTES56            2ul
#define H_OFFSET56           (H_OFFSET55+L_BYTES55)
#define OFFSET_ALARM_1_RANGE2_LOW      (H_OFFSET56<<EEHIGHTBYTEPOSITION)|(L_BYTES56)

// Alarm 1 counter Offset and Size definition...
#define L_BYTES57            3ul
#define H_OFFSET57           (H_OFFSET56+L_BYTES56)
#define OFFSET_ALARM_1_COUNTER          (H_OFFSET57<<EEHIGHTBYTEPOSITION)|(L_BYTES57)

// Alarm 1 Threshhold value Offset and Size definition...
#define L_BYTES58            3ul
#define H_OFFSET58           (H_OFFSET57+L_BYTES57)
#define OFFSET_ALARM_1_THRESHHOLD_VALUE       (H_OFFSET58<<EEHIGHTBYTEPOSITION)|(L_BYTES58)

// Alarm 2 configuration byte 0 Offset and Size definition...
#define L_BYTES59            1ul
#define H_OFFSET59           (H_OFFSET58+L_BYTES58)
#define OFFSET_ALARM_2_CONFIG_BYTE_0          (H_OFFSET59<<EEHIGHTBYTEPOSITION)|(L_BYTES59)

// Alarm 2 configuration byte 1 Offset and Size definition...
#define L_BYTES60            1ul
#define H_OFFSET60           (H_OFFSET59+L_BYTES59)
#define OFFSET_ALARM_2_CONFIG_BYTE_1          (H_OFFSET60<<EEHIGHTBYTEPOSITION)|(L_BYTES60)

// Alarm 2 Range#1 High temp setpoint Offset and Size definition...
#define L_BYTES61            2ul
#define H_OFFSET61           (H_OFFSET60+L_BYTES60)
#define OFFSET_ALARM_2_RANGE1_HIGH (H_OFFSET61<<EEHIGHTBYTEPOSITION)|(L_BYTES61)

// Alarm 2 Range#1 Low temp setpoint Offset and Size definition...
#define L_BYTES62            2ul
#define H_OFFSET62           (H_OFFSET61+L_BYTES61)
#define OFFSET_ALARM_2_RANGE1_LOW  (H_OFFSET62<<EEHIGHTBYTEPOSITION)|(L_BYTES62)

// Alarm 2 Range#2 High temp setpoint Offset and Size definition...
#define L_BYTES63            2ul
#define H_OFFSET63           (H_OFFSET62+L_BYTES62)
#define OFFSET_ALARM_2_RANGE2_HIGH     (H_OFFSET63<<EEHIGHTBYTEPOSITION)|(L_BYTES63)

// Alarm 2 Range#2 High temp setpoint Offset and Size definition...
#define L_BYTES64            2ul
#define H_OFFSET64           (H_OFFSET63+L_BYTES63)
#define OFFSET_ALARM_2_RANGE2_LOW      (H_OFFSET64<<EEHIGHTBYTEPOSITION)|(L_BYTES64)

// Alarm 2 counter Offset and Size definition...
#define L_BYTES65            3ul
#define H_OFFSET65           (H_OFFSET64+L_BYTES64)
#define OFFSET_ALARM_2_COUNTER                (H_OFFSET65<<EEHIGHTBYTEPOSITION)|(L_BYTES65)

// Alarm 2 Threshhold value Offset and Size definition...
#define L_BYTES66            3ul
#define H_OFFSET66           (H_OFFSET65+L_BYTES65)
#define OFFSET_ALARM_2_THRESHHOLD_VALUE       (H_OFFSET66<<EEHIGHTBYTEPOSITION)|(L_BYTES66)

// Alarm 3 configuration byte 0 Offset and Size definition...
#define L_BYTES67            1ul
#define H_OFFSET67           (H_OFFSET66+L_BYTES66)
#define OFFSET_ALARM_3_CONFIG_BYTE_0          (H_OFFSET67<<EEHIGHTBYTEPOSITION)|(L_BYTES67)

// Alarm 3 configuration byte 1 Offset and Size definition...
#define L_BYTES68            1ul
#define H_OFFSET68           (H_OFFSET67+L_BYTES67)
#define OFFSET_ALARM_3_CONFIG_BYTE_1          (H_OFFSET68<<EEHIGHTBYTEPOSITION)|(L_BYTES68)

// Alarm 3 Range#1 High temp setpoint Offset and Size definition...
#define L_BYTES69            2ul
#define H_OFFSET69           (H_OFFSET68+L_BYTES68)
#define OFFSET_ALARM_3_RANGE1_HIGH (H_OFFSET69<<EEHIGHTBYTEPOSITION)|(L_BYTES69)

// Alarm 3 Range#1 High temp setpoint Offset and Size definition...
#define L_BYTES70            2ul
#define H_OFFSET70           (H_OFFSET69+L_BYTES69)
#define OFFSET_ALARM_3_RANGE1_LOW  (H_OFFSET70<<EEHIGHTBYTEPOSITION)|(L_BYTES70)

// Alarm 3 Range#2 High temp setpoint Offset and Size definition...
#define L_BYTES71            2ul
#define H_OFFSET71           (H_OFFSET70+L_BYTES70)
#define OFFSET_ALARM_3_RANGE2_HIGH     (H_OFFSET71<<EEHIGHTBYTEPOSITION)|(L_BYTES71)

// Alarm 3 Range#2 Low temp setpoint Offset and Size definition...
#define L_BYTES72            2ul          
#define H_OFFSET72           (H_OFFSET71+L_BYTES71)
#define OFFSET_ALARM_3_RANGE2_LOW      (H_OFFSET72<<EEHIGHTBYTEPOSITION)|(L_BYTES72)

// Alarm 3 counter Offset and Size definition...
#define L_BYTES73            3ul
#define H_OFFSET73           (H_OFFSET72+L_BYTES72)
#define OFFSET_ALARM_3_COUNTER                (H_OFFSET73<<EEHIGHTBYTEPOSITION)|(L_BYTES73)

// Alarm 3 Threshhold value Offset and Size definition...
#define L_BYTES74            3ul
#define H_OFFSET74           (H_OFFSET73+L_BYTES73)
#define OFFSET_ALARM_3_THRESHHOLD_VALUE       (H_OFFSET74<<EEHIGHTBYTEPOSITION)|(L_BYTES74)

// Alarm 4 Configuration Byte 0 Offset and Size definition...
#define L_BYTES75            1ul
#define H_OFFSET75           (H_OFFSET74+L_BYTES74)
#define OFFSET_ALARM_4_CONFIG_BYTE_0          (H_OFFSET75<<EEHIGHTBYTEPOSITION)|(L_BYTES75)

// Alarm 4 Configuration Byte 1 Offset and Size definition...
#define L_BYTES76            1ul
#define H_OFFSET76           (H_OFFSET75+L_BYTES75)
#define OFFSET_ALARM_4_CONFIG_BYTE_1          (H_OFFSET76<<EEHIGHTBYTEPOSITION)|(L_BYTES76)

// Alarm 4 Range#1 High temp setpoint Offset and Size definition...
#define L_BYTES77            2ul
#define H_OFFSET77           (H_OFFSET76+L_BYTES76)
#define OFFSET_ALARM_4_RANGE1_HIGH (H_OFFSET77<<EEHIGHTBYTEPOSITION)|(L_BYTES77)

// Alarm 4 Range#1 Low temp setpoint Offset and Size definition...
#define L_BYTES78            2ul
#define H_OFFSET78           (H_OFFSET77+L_BYTES77)
#define OFFSET_ALARM_4_RANGE1_LOW (H_OFFSET78<<EEHIGHTBYTEPOSITION)|(L_BYTES78)

// Alarm 4 Range#2 High temp setpoint Offset and Size definition...
#define L_BYTES79            2ul
#define H_OFFSET79           (H_OFFSET78+L_BYTES78)
#define OFFSET_ALARM_4_RANGE2_HIGH     (H_OFFSET79<<EEHIGHTBYTEPOSITION)|(L_BYTES79)

// Alarm 4 Range#2 Low temp setpoint Offset and Size definition...
#define L_BYTES80            2ul
#define H_OFFSET80           (H_OFFSET79+L_BYTES79)
#define OFFSET_ALARM_4_RANGE2_LOW      (H_OFFSET80<<EEHIGHTBYTEPOSITION)|(L_BYTES80)

// Alarm 4 counter Offset and Size definition...
#define L_BYTES81            3ul
#define H_OFFSET81           (H_OFFSET80+L_BYTES80)
#define OFFSET_ALARM_4_COUNTER                (H_OFFSET81<<EEHIGHTBYTEPOSITION)|(L_BYTES81)

// Alarm 4 Threshhold value Offset and Size definition...
#define L_BYTES82            3ul
#define H_OFFSET82           (H_OFFSET81+L_BYTES81)
#define OFFSET_ALARM_4_THRESHHOLD_VALUE       (H_OFFSET82<<EEHIGHTBYTEPOSITION)|(L_BYTES82)

// Alarm 5 Configuration Byte 0 Offset and Size definition...
#define L_BYTES83            1ul
#define H_OFFSET83           (H_OFFSET82+L_BYTES82)
#define OFFSET_ALARM_5_CONFIG_BYTE_0          (H_OFFSET83<<EEHIGHTBYTEPOSITION)|(L_BYTES83)

// Alarm 5 Configuration Byte 1 Offset and Size definition...
#define L_BYTES84            1ul
#define H_OFFSET84           (H_OFFSET83+L_BYTES83)
#define OFFSET_ALARM_5_CONFIG_BYTE_1          (H_OFFSET84<<EEHIGHTBYTEPOSITION)|(L_BYTES84)

// Alarm 5 Range#1 High temp setpoint Offset and Size definition...
#define L_BYTES85            2ul
#define H_OFFSET85           (H_OFFSET84+L_BYTES84)
#define OFFSET_ALARM_5_RANGE1_HIGH (H_OFFSET85<<EEHIGHTBYTEPOSITION)|(L_BYTES85)

// Alarm 5 Range#1 Low temp setpoint Offset and Size definition...
#define L_BYTES86            2ul
#define H_OFFSET86           (H_OFFSET85+L_BYTES85)
#define OFFSET_ALARM_5_RANGE1_LOW  (H_OFFSET86<<EEHIGHTBYTEPOSITION)|(L_BYTES86)

// Alarm 5 Range#2 High temp setpoint Offset and Size definition...
#define L_BYTES87            2ul
#define H_OFFSET87           (H_OFFSET86+L_BYTES86)
#define OFFSET_ALARM_5_RANGE2_HIGH     (H_OFFSET87<<EEHIGHTBYTEPOSITION)|(L_BYTES87)

// Alarm 5 Range#2 Low temp setpoint Offset and Size definition...
#define L_BYTES88            2ul
#define H_OFFSET88           (H_OFFSET87+L_BYTES87)
#define OFFSET_ALARM_5_RANGE2_LOW      (H_OFFSET88<<EEHIGHTBYTEPOSITION)|(L_BYTES88)

// Alarm 5 counter setpoint Offset and Size definition...
#define L_BYTES89            3ul
#define H_OFFSET89           (H_OFFSET88+L_BYTES88)
#define OFFSET_ALARM_5_COUNTER                (H_OFFSET89<<EEHIGHTBYTEPOSITION)|(L_BYTES89)

// Alarm 5 Threshhold value Offset and Size definition...
#define L_BYTES90            3ul
#define H_OFFSET90           (H_OFFSET89+L_BYTES89)
#define OFFSET_ALARM_5_THRESHHOLD_VALUE       (H_OFFSET90<<EEHIGHTBYTEPOSITION)|(L_BYTES90)

// Alarm 6 Configuration Byte 0  Offset and Size definition...
#define L_BYTES91            1ul
#define H_OFFSET91           (H_OFFSET90+L_BYTES90)
#define OFFSET_ALARM_6_CONFIG_BYTE_0          (H_OFFSET91<<EEHIGHTBYTEPOSITION)|(L_BYTES91)

// Alarm 6 Configuration Byte 1 Offset and Size definition...
#define L_BYTES92            1ul
#define H_OFFSET92           (H_OFFSET91+L_BYTES91)
#define OFFSET_ALARM_6_CONFIG_BYTE_1          (H_OFFSET92<<EEHIGHTBYTEPOSITION)|(L_BYTES92)

// Alarm 6 Range#1 High temp setpoint Offset and Size definition...
#define L_BYTES93            2ul
#define H_OFFSET93           (H_OFFSET92+L_BYTES92)
#define OFFSET_ALARM_6_RANGE1_HIGH (H_OFFSET93<<EEHIGHTBYTEPOSITION)|(L_BYTES93)

// Alarm 6 Range#1 Low temp setpoint Offset and Size definition...
#define L_BYTES94            2ul
#define H_OFFSET94           (H_OFFSET93+L_BYTES93)
#define OFFSET_ALARM_6_RANGE1_LOW (H_OFFSET94<<EEHIGHTBYTEPOSITION)|(L_BYTES94)

// Alarm 6 Range#2 High temp setpoint Offset and Size definition...
#define L_BYTES95            2ul
#define H_OFFSET95           (H_OFFSET94+L_BYTES94)
#define OFFSET_ALARM_6_RANGE2_HIGH    (H_OFFSET95<<EEHIGHTBYTEPOSITION)|(L_BYTES95)

// Alarm 6 Range#2 low temp setpoint Offset and Size definition...
#define L_BYTES96            2ul
#define H_OFFSET96           (H_OFFSET95+L_BYTES95)
#define OFFSET_ALARM_6_RANGE2_LOW     (H_OFFSET96<<EEHIGHTBYTEPOSITION)|(L_BYTES96)

// Alarm 6 counter Offset and Size definition...
#define L_BYTES97            3ul
#define H_OFFSET97           (H_OFFSET96+L_BYTES96)
#define OFFSET_ALARM_6_COUNTER               (H_OFFSET97<<EEHIGHTBYTEPOSITION)|(L_BYTES97)

// Alarm 6 Threshhold value Offset and Size definition...
#define L_BYTES98            3ul
#define H_OFFSET98           (H_OFFSET97+L_BYTES97)
#define OFFSET_ALARM_6_THRESHHOLD_VALUE      (H_OFFSET98<<EEHIGHTBYTEPOSITION)|(L_BYTES98)



//-----------------------------------------------------------------------------------
//      Page Password...      
//-----------------------------------------------------------------------------------

// Configuration Password String Offset and Size definition...
#define L_BYTES99            16ul
#define H_OFFSET99           (H_OFFSET98+L_BYTES98)
#define OFFSET_CONFIGURATION_PASSWORD_STRING (H_OFFSET99<<EEHIGHTBYTEPOSITION)|(L_BYTES99)

//  Offset and Size definition...
#define L_BYTES100            16ul
#define H_OFFSET100           (H_OFFSET99+L_BYTES99)
#define OFFSET_DOWNLOAD_PASSWORD_STRING      (H_OFFSET100<<EEHIGHTBYTEPOSITION)|(L_BYTES100)


//-----------------------------------------------------------------------------------
//      Page Spare...      
//-----------------------------------------------------------------------------------
// User/Programmer string Offset and Size definition...
#define L_BYTES_USER            30ul
#define H_OFFSET_USER           (H_OFFSET100+L_BYTES100)
#define OFFSET_USERNAME                      (H_OFFSET_USER<<EEHIGHTBYTEPOSITION)|(L_BYTES_USER)

// Not used 8 Offset and Size definition...
#define L_BYTES101             2ul
#define H_OFFSET101           (H_OFFSET_USER+L_BYTES_USER)
#define OFFSET_NOT_USED_8                    (H_OFFSET101<<EEHIGHTBYTEPOSITION)|(L_BYTES101)



//-----------------------------------------------------------------------------------
//      Three Point Certificate Area...      
//-----------------------------------------------------------------------------------

// Sensor 1 Tester Name Offset and Size definition...
#define L_BYTES102            22ul
#define H_OFFSET102           (H_OFFSET101+L_BYTES101)
#define OFFSET_SENSOR_1_TESTER_NAME          (H_OFFSET102<<EEHIGHTBYTEPOSITION)|(L_BYTES102)

// Sensor 1 Test Date Offset and Size definition...
#define L_BYTES103            10ul
#define H_OFFSET103           (H_OFFSET102+L_BYTES102)
#define OFFSET_SENSOR_1_TEST_DATE            (H_OFFSET103<<EEHIGHTBYTEPOSITION)|(L_BYTES103)

// Sensor 1 Test Time Offset and Size definition...
#define L_BYTES104           11ul
#define H_OFFSET104          (H_OFFSET103+L_BYTES103)
#define OFFSET_SENSOR_1_TEST_TIME            (H_OFFSET104<<EEHIGHTBYTEPOSITION)|(L_BYTES104)

// Sensor 1 Published tolerance setpoint 1 Offset and Size definition...
#define L_BYTES105           1ul
#define H_OFFSET105          (H_OFFSET104+L_BYTES104)
#define OFFSET_SENSOR_1_PUB_TOL_SETPOINT_1   (H_OFFSET105<<EEHIGHTBYTEPOSITION)|(L_BYTES105)

// Sensor 1 Published tolerance setpoint 2  Offset and Size definition...
#define L_BYTES106           1ul
#define H_OFFSET106          (H_OFFSET105+L_BYTES105)
#define OFFSET_SENSOR_1_PUB_TOL_SETPOINT_2   (H_OFFSET106<<EEHIGHTBYTEPOSITION)|(L_BYTES106)

// Sensor 1 Published tolerance setpoint 3 Offset and Size definition...
#define L_BYTES107           1ul
#define H_OFFSET107          (H_OFFSET106+L_BYTES106)
#define OFFSET_SENSOR_1_PUB_TOL_SETPOINT_3   (H_OFFSET107<<EEHIGHTBYTEPOSITION)|(L_BYTES107)

// Sensor 1 resulting tolerance setpoint 1 Offset and Size definition...
#define L_BYTES108           1ul
#define H_OFFSET108          (H_OFFSET107+L_BYTES107)
#define OFFSET_SENSOR_1_RES_TOL_SETPOINT1    (H_OFFSET108<<EEHIGHTBYTEPOSITION)|(L_BYTES108)

// Sensor 1 resulting tolerance setpoint 2 Offset and Size definition...
#define L_BYTES109           1ul
#define H_OFFSET109          (H_OFFSET108+L_BYTES108)
#define OFFSET_SENSOR_1_RES_TOL_SETPOINT2    (H_OFFSET109<<EEHIGHTBYTEPOSITION)|(L_BYTES109)

// Sensor 1 resulting tolerance setpoint 3 Offset and Size definition...
#define L_BYTES110           1ul
#define H_OFFSET110          (H_OFFSET109+L_BYTES109)
#define OFFSET_SENSOR_1_RES_TOL_SETPOINT3    (H_OFFSET110<<EEHIGHTBYTEPOSITION)|(L_BYTES110)

// Sensor 1 NIST Standard value setpoint 1 Offset and Size definition...
#define L_BYTES111           2ul
#define H_OFFSET111          (H_OFFSET110+L_BYTES110)
#define OFFSET_SENSOR_1_NIST_STD_SETPOINT_1  (H_OFFSET111<<EEHIGHTBYTEPOSITION)|(L_BYTES111)

// Sensor 1 NIST Standard value setpoint 2 Offset and Size definition...
#define L_BYTES112           2ul
#define H_OFFSET112          (H_OFFSET111+L_BYTES111)
#define OFFSET_SENSOR_1_NIST_STD_SETPOINT_2  (H_OFFSET112<<EEHIGHTBYTEPOSITION)|(L_BYTES112)

// Sensor 1 NIST Standard value setpoint 3 Offset and Size definition...
#define L_BYTES113           2ul
#define H_OFFSET113          (H_OFFSET112+L_BYTES112)
#define OFFSET_SENSOR_1_NIST_STD_SETPOINT_3  (H_OFFSET113<<EEHIGHTBYTEPOSITION)|(L_BYTES113)
        
// Sensor 1 monitor value setpoint 1 Offset and Size definition...
#define L_BYTES114           2ul
#define H_OFFSET114          (H_OFFSET113+L_BYTES113)
#define OFFSET_SENSOR_1_MON_VAL_SETPOINT_1   (H_OFFSET114<<EEHIGHTBYTEPOSITION)|(L_BYTES114)

// Sensor 1 monitor value setpoint 2 Offset and Size definition...
#define L_BYTES115           2ul
#define H_OFFSET115          (H_OFFSET114+L_BYTES114)
#define OFFSET_SENSOR_1_MON_VAL_SETPOINT_2   (H_OFFSET115<<EEHIGHTBYTEPOSITION)|(L_BYTES115)

// Sensor 1 monitor value setpoint 3 Offset and Size definition...
#define L_BYTES116           2ul
#define H_OFFSET116          (H_OFFSET115+L_BYTES115)
#define OFFSET_SENSOR_1_MON_VAL_SETPOINT_3   (H_OFFSET116<<EEHIGHTBYTEPOSITION)|(L_BYTES116)

// Not used 9 Offset and Size definition...
#define L_BYTES117           3ul
#define H_OFFSET117          (H_OFFSET116+L_BYTES116)
#define OFFSET_NOT_USED_9                    (H_OFFSET117<<EEHIGHTBYTEPOSITION)|(L_BYTES117)

// Sensor 1, 30 character filename Offset and Size definition...
#define L_BYTES118           30ul
#define H_OFFSET118          (H_OFFSET117+L_BYTES117)
#define OFFSET_SENSOR_1_30_CHAR_FILENAME     (H_OFFSET118<<EEHIGHTBYTEPOSITION)|(L_BYTES118)

// Sensor 1 accuracy test pass Offset and Size definition...
#define L_BYTES119           1ul
#define H_OFFSET119          (H_OFFSET118+L_BYTES118)
#define OFFSET_SENSOR_1_ACCURACY_TEST        (H_OFFSET119<<EEHIGHTBYTEPOSITION)|(L_BYTES119)

// Not Used 10 Offset and Size definition...
#define L_BYTES120           1ul
#define H_OFFSET120          (H_OFFSET119+L_BYTES119)
#define OFFSET_NOT_USED_10                   (H_OFFSET120<<EEHIGHTBYTEPOSITION)|(L_BYTES120)

// Sensor 1, test chamber station number Offset and Size definition...
#define L_BYTES121           1ul
#define H_OFFSET121          (H_OFFSET120+L_BYTES120)
#define OFFSET_SENSOR_1_TEST_CHAM_STATION_NUM (H_OFFSET121<<EEHIGHTBYTEPOSITION)|(L_BYTES121)

// Sensor 1, test chamber serial number Offset and Size definition...
#define L_BYTES122           8ul
#define H_OFFSET122          (H_OFFSET121+L_BYTES121)
#define OFFSET_SENSOR_1_TEST_CHAM_SERIAL_NUM (H_OFFSET122<<EEHIGHTBYTEPOSITION)|(L_BYTES122)

// Sensor 1, logger name Offset and Size definition...
#define L_BYTES123           20ul
#define H_OFFSET123          (H_OFFSET122+L_BYTES122)
#define OFFSET_SENSOR_1_LOGGER_NAME          (H_OFFSET123<<EEHIGHTBYTEPOSITION)|(L_BYTES123)

// Not used 11 Offset and Size definition...
#define L_BYTES124           3ul
#define H_OFFSET124          (H_OFFSET123+L_BYTES123)
#define OFFSET_NOT_USED_11                   (H_OFFSET124<<EEHIGHTBYTEPOSITION)|(L_BYTES124)

// Sensor 1, setpoint 1 sample time Offset and Size definition...
#define L_BYTES125           8ul
#define H_OFFSET125          (H_OFFSET124+L_BYTES124)
#define OFFSET_SENSOR_1_SETPOINT_1_SAMPLE_TIME (H_OFFSET125<<EEHIGHTBYTEPOSITION)|(L_BYTES125)

// Sensor 1, setpoint 2 sample Offset and Size definition...
#define L_BYTES126           8ul
#define H_OFFSET126          (H_OFFSET125+L_BYTES125)
#define OFFSET_SENSOR_1_SETPOINT_2_SAMPLE_TIME (H_OFFSET126<<EEHIGHTBYTEPOSITION)|(L_BYTES126)

// Sensor 1, setpoint 3 sample Offset and Size definition...
#define L_BYTES127           8ul
#define H_OFFSET127          (H_OFFSET126+L_BYTES126)
#define OFFSET_SENSOR_1_SETPOINT_3_SAMPLE_TIME (H_OFFSET127<<EEHIGHTBYTEPOSITION)|(L_BYTES127)

// Not used 12 Offset and Size definition...
#define L_BYTES128           8ul
#define H_OFFSET128          (H_OFFSET127+L_BYTES127)
#define OFFSET_NOT_USED_12                     (H_OFFSET128<<EEHIGHTBYTEPOSITION)|(L_BYTES128)

// Sensor 2, Tester name Offset and Size definition...
#define L_BYTES129           22ul
#define H_OFFSET129          (H_OFFSET128+L_BYTES128)
#define OFFSET_SENSOR_2_TESTER_NAME            (H_OFFSET129<<EEHIGHTBYTEPOSITION)|(L_BYTES129)

// Sensor 2, Test Date Offset and Size definition...
#define L_BYTES130           10ul
#define H_OFFSET130          (H_OFFSET129+L_BYTES129)
#define OFFSET_SENSOR_2_TEST_DATE              (H_OFFSET130<<EEHIGHTBYTEPOSITION)|(L_BYTES130)

// Sensor 2, test time Offset and Size definition...
#define L_BYTES131           11ul
#define H_OFFSET131          (H_OFFSET130+L_BYTES130)
#define OFFSET_SENSOR_2_TEST_TIME              (H_OFFSET131<<EEHIGHTBYTEPOSITION)|(L_BYTES131)

// Sensor 2, published tolerance setpoint 1 Offset and Size definition...
#define L_BYTES132           1ul
#define H_OFFSET132          (H_OFFSET131+L_BYTES131)
#define OFFSET_SENSOR_2_PUB_TOL_SETPOINT_1     (H_OFFSET132<<EEHIGHTBYTEPOSITION)|(L_BYTES132)

// Sensor 2, published tolerance setpoint 2 Offset and Size definition...
#define L_BYTES133           1ul
#define H_OFFSET133          (H_OFFSET132+L_BYTES132)
#define OFFSET_SENSOR_2_PUB_TOL_SETPOINT_2     (H_OFFSET133<<EEHIGHTBYTEPOSITION)|(L_BYTES133)

// Sensor 2, published tolerance setpoint 3 Offset and Size definition...
#define L_BYTES134           1ul
#define H_OFFSET134          (H_OFFSET133+L_BYTES133)
#define OFFSET_SENSOR_2_PUB_TOL_SETPOINT_3     (H_OFFSET134<<EEHIGHTBYTEPOSITION)|(L_BYTES134)

// Sensor 2, resulting tolerance setpoint 1 Offset and Size definition...
#define L_BYTES135           1ul
#define H_OFFSET135          (H_OFFSET134+L_BYTES134)
#define OFFSET_SENSOR_2_RES_TOL_SETPOINT_1     (H_OFFSET135<<EEHIGHTBYTEPOSITION)|(L_BYTES135)

// Sensor 2, resulting tolerance setpoint 2 Offset and Size definition...
#define L_BYTES136           1ul
#define H_OFFSET136          (H_OFFSET135+L_BYTES135)
#define OFFSET_SENSOR_2_RES_TOL_SETPOINT_2     (H_OFFSET136<<EEHIGHTBYTEPOSITION)|(L_BYTES136)

// Sensor 2, resulting tolerance setpoint 3 Offset and Size definition...
#define L_BYTES137           1ul
#define H_OFFSET137          (H_OFFSET136+L_BYTES136)
#define OFFSET_SENSOR_2_RES_TOL_SETPOINT_3     (H_OFFSET137<<EEHIGHTBYTEPOSITION)|(L_BYTES137)

// Sensor 2, NIST Standard value setpoint 1 Offset and Size definition...
#define L_BYTES138           2ul
#define H_OFFSET138          (H_OFFSET137+L_BYTES137)
#define OFFSET_SENSOR_2_NIST_STD_SETPOINT_1    (H_OFFSET138<<EEHIGHTBYTEPOSITION)|(L_BYTES138)

// Sensor 2, NIST Standard value setpoint 2 Offset and Size definition...
#define L_BYTES139           2ul
#define H_OFFSET139          (H_OFFSET138+L_BYTES138)
#define OFFSET_SENSOR_2_NIST_STD_SETPOINT_2    (H_OFFSET139<<EEHIGHTBYTEPOSITION)|(L_BYTES139)

// Sensor 2, NIST Standard value setpoint 3 Offset and Size definition...
#define L_BYTES140           2ul
#define H_OFFSET140          (H_OFFSET139+L_BYTES139)
#define OFFSET_SENSOR_2_NIST_STD_SETPOINT_3    (H_OFFSET140<<EEHIGHTBYTEPOSITION)|(L_BYTES140)

// Sensor 2, monitor value setpoint 1 Offset and Size definition...
#define L_BYTES141           2ul
#define H_OFFSET141          (H_OFFSET140+L_BYTES140)
#define OFFSET_SENSOR_2_MON_VAL_SETPOINT_1     (H_OFFSET141<<EEHIGHTBYTEPOSITION)|(L_BYTES141)

// Sensor 2, monitor value setpoint 2 Offset and Size definition...
#define L_BYTES142           2ul
#define H_OFFSET142          (H_OFFSET141+L_BYTES141)
#define OFFSET_SENSOR_2_MON_VAL_SETPOINT_2     (H_OFFSET142<<EEHIGHTBYTEPOSITION)|(L_BYTES142)

// Sensor 2, monitor value setpoint 3 Offset and Size definition...
#define L_BYTES143           2ul
#define H_OFFSET143          (H_OFFSET142+L_BYTES142)
#define OFFSET_SENSOR_2_MON_VAL_SETPOINT_3     (H_OFFSET143<<EEHIGHTBYTEPOSITION)|(L_BYTES143)

// Not used 13 Offset and Size definition...
#define L_BYTES144           3ul
#define H_OFFSET144          (H_OFFSET143+L_BYTES143)
#define OFFSET_NOT_USED_13                     (H_OFFSET144<<EEHIGHTBYTEPOSITION)|(L_BYTES144)

// Sensor 2, 30 character filename Offset and Size definition...
#define L_BYTES145           30ul
#define H_OFFSET145          (H_OFFSET144+L_BYTES144)
#define OFFSET_SENSOR_2_30_CHAR_FILENAME       (H_OFFSET145<<EEHIGHTBYTEPOSITION)|(L_BYTES145)

// Sensor 2, Accuracy test pass Offset and Size definition...
#define L_BYTES146           1ul
#define H_OFFSET146          (H_OFFSET145+L_BYTES145)
#define OFFSET_SENSOR_2_ACCURACY_TEST          (H_OFFSET146<<EEHIGHTBYTEPOSITION)|(L_BYTES146)

// Not used 14 Offset and Size definition...
#define L_BYTES147           1ul
#define H_OFFSET147          (H_OFFSET146+L_BYTES146)
#define OFFSET_NOT_USED_14                     (H_OFFSET147<<EEHIGHTBYTEPOSITION)|(L_BYTES147)

// Sensor 2, Test Chamber Station Number Offset and Size definition...
#define L_BYTES148           1ul
#define H_OFFSET148          (H_OFFSET147+L_BYTES147)
#define OFFSET_SENSOR_2_TEST_CHAM_STATION_NUM  (H_OFFSET148<<EEHIGHTBYTEPOSITION)|(L_BYTES148)
        
// Sensor 2, Test Chamber Serial number (8byte character string) Offset and Size definition...
#define L_BYTES149           8ul 
#define H_OFFSET149          (H_OFFSET148+L_BYTES148)
#define OFFSET_SENSOR_2_TEST_CHAM_SERIAL_NUM   (H_OFFSET149<<EEHIGHTBYTEPOSITION)|(L_BYTES149)

// Sensor 2, Logger Name (20 byte character string) Offset and Size definition...
#define L_BYTES150           20ul
#define H_OFFSET150          (H_OFFSET149+L_BYTES149)
#define OFFSET_SENSOR_2_LOGGER_NAME            (H_OFFSET150<<EEHIGHTBYTEPOSITION)|(L_BYTES150)

// Not used 15 Offset and Size definition...
#define L_BYTES151           3ul
#define H_OFFSET151          (H_OFFSET150+L_BYTES150)
#define OFFSET_NOT_USED_15                     (H_OFFSET151<<EEHIGHTBYTEPOSITION)|(L_BYTES151)

// Sensor 2, Setpoint 1 sample time Offset and Size definition...
#define L_BYTES152           8ul
#define H_OFFSET152          (H_OFFSET151+L_BYTES151)
#define OFFSET_SENSOR_2_SETPOINT_1_SAMPLE_TIME (H_OFFSET152<<EEHIGHTBYTEPOSITION)|(L_BYTES152)

// Sensor 2, Setpoint 2 sample time Offset and Size definition...
#define L_BYTES153           8ul
#define H_OFFSET153          (H_OFFSET152+L_BYTES152)
#define OFFSET_SENSOR_2_SETPOINT_2_SAMPLE_TIME (H_OFFSET153<<EEHIGHTBYTEPOSITION)|(L_BYTES153)

// Sensor 2, Setpoint 3 sample time Offset and Size definition...
#define L_BYTES154           8ul
#define H_OFFSET154          (H_OFFSET153+L_BYTES153)
#define OFFSET_SENSOR_2_SETPOINT_3_SAMPLE_TIME (H_OFFSET154<<EEHIGHTBYTEPOSITION)|(L_BYTES154)

// Not used 16 Offset and Size definition...
#define L_BYTES155           8ul
#define H_OFFSET155          (H_OFFSET154+L_BYTES154)
#define OFFSET_NOT_USED_16                     (H_OFFSET155<<EEHIGHTBYTEPOSITION)|(L_BYTES155)

// Datapoints starting offset address and Size definition...
#define OFFSET_DATAPOINTS_STARTING_ADDRRESS    (H_OFFSET155+L_BYTES155)

// Datapoints data size, 2 bytes
#define OFFSET_DATAPOINTS_DATA_SIZE    2

//Extended Memory for Shipping Info


#define L_BYTES194           128ul
#define H_OFFSET194          (OFFSET_DATAPOINTS_STARTING_ADDRRESS + 32000ul + 31040ul)
#define OFFSET_EXTENDED_UNUSED                       (H_OFFSET194<<EEHIGHTBYTEPOSITION)|(L_BYTES194)

#define L_BYTES195           38ul
#define H_OFFSET195          (H_OFFSET194+L_BYTES194)
#define OFFSET_LABEL1_TAG                             (H_OFFSET195<<EEHIGHTBYTEPOSITION)|(L_BYTES195)

#define L_BYTES196           38ul
#define H_OFFSET196          (H_OFFSET195+L_BYTES195)
#define OFFSET_LABEL1_LOV                             (H_OFFSET196<<EEHIGHTBYTEPOSITION)|(L_BYTES196)

#define L_BYTES197           20ul
#define H_OFFSET197          (H_OFFSET196+L_BYTES196)
#define OFFSET_LABEL1_LABEL                           (H_OFFSET197<<EEHIGHTBYTEPOSITION)|(L_BYTES197)

#define L_BYTES198           38ul
#define H_OFFSET198          (H_OFFSET197+L_BYTES197)
#define OFFSET_LABEL1_VALUE                           (H_OFFSET198<<EEHIGHTBYTEPOSITION)|(L_BYTES198)

#define L_BYTES199           1ul
#define H_OFFSET199          (H_OFFSET198+L_BYTES198)
#define OFFSET_LABEL1_FLEX_TAG_TYPE                   (H_OFFSET199<<EEHIGHTBYTEPOSITION)|(L_BYTES199)

#define L_BYTES200           25ul
#define H_OFFSET200          (H_OFFSET199+L_BYTES199)
#define OFFSET_EXTENDED_UNUSED4                       (H_OFFSET200<<EEHIGHTBYTEPOSITION)|(L_BYTES200)

#define L_BYTES201           38ul
#define H_OFFSET201          (H_OFFSET200+L_BYTES200)
#define OFFSET_LABEL2_TAG                             (H_OFFSET201<<EEHIGHTBYTEPOSITION)|(L_BYTES201)

#define L_BYTES202           38ul
#define H_OFFSET202          (H_OFFSET201+L_BYTES201)
#define OFFSET_LABEL2_LOV                             (H_OFFSET202<<EEHIGHTBYTEPOSITION)|(L_BYTES202)

#define L_BYTES203           20ul
#define H_OFFSET203          (H_OFFSET202+L_BYTES202)
#define OFFSET_LABEL2_LABEL                           (H_OFFSET203<<EEHIGHTBYTEPOSITION)|(L_BYTES203)

#define L_BYTES204           38ul
#define H_OFFSET204          (H_OFFSET203+L_BYTES203)
#define OFFSET_LABEL2_VALUE                           (H_OFFSET204<<EEHIGHTBYTEPOSITION)|(L_BYTES204)

#define L_BYTES205           1ul
#define H_OFFSET205          (H_OFFSET204+L_BYTES204)
#define OFFSET_LABEL2_FLEX_TAG_TYPE                   (H_OFFSET205<<EEHIGHTBYTEPOSITION)|(L_BYTES205)

#define L_BYTES206           25ul
#define H_OFFSET206          (H_OFFSET205+L_BYTES205)
#define OFFSET_EXTENDED_UNUSED5                       (H_OFFSET206<<EEHIGHTBYTEPOSITION)|(L_BYTES206)

#define L_BYTES207           38ul
#define H_OFFSET207          (H_OFFSET206+L_BYTES206)
#define OFFSET_LABEL3_TAG                             (H_OFFSET207<<EEHIGHTBYTEPOSITION)|(L_BYTES207)

#define L_BYTES208           38ul
#define H_OFFSET208          (H_OFFSET207+L_BYTES207)
#define OFFSET_LABEL3_LOV                             (H_OFFSET208<<EEHIGHTBYTEPOSITION)|(L_BYTES208)

#define L_BYTES209           20ul
#define H_OFFSET209          (H_OFFSET208+L_BYTES208)
#define OFFSET_LABEL3_LABEL                           (H_OFFSET209<<EEHIGHTBYTEPOSITION)|(L_BYTES209)

#define L_BYTES210           38ul
#define H_OFFSET210          (H_OFFSET209+L_BYTES209)
#define OFFSET_LABEL3_VALUE                           (H_OFFSET210<<EEHIGHTBYTEPOSITION)|(L_BYTES210)

#define L_BYTES211           1ul
#define H_OFFSET211          (H_OFFSET210+L_BYTES210)
#define OFFSET_LABEL3_FLEX_TAG_TYPE                   (H_OFFSET211<<EEHIGHTBYTEPOSITION)|(L_BYTES211)

#define L_BYTES212           25ul
#define H_OFFSET212          (H_OFFSET211+L_BYTES211)
#define OFFSET_EXTENDED_UNUSED6                       (H_OFFSET212<<EEHIGHTBYTEPOSITION)|(L_BYTES212)

#define L_BYTES213           38ul
#define H_OFFSET213          (H_OFFSET212+L_BYTES212)
#define OFFSET_LABEL4_TAG                             (H_OFFSET213<<EEHIGHTBYTEPOSITION)|(L_BYTES213)

#define L_BYTES214           38ul
#define H_OFFSET214          (H_OFFSET213+L_BYTES213)
#define OFFSET_LABEL4_LOV                             (H_OFFSET214<<EEHIGHTBYTEPOSITION)|(L_BYTES214)

#define L_BYTES215           20ul
#define H_OFFSET215          (H_OFFSET214+L_BYTES214)
#define OFFSET_LABEL4_LABEL                           (H_OFFSET215<<EEHIGHTBYTEPOSITION)|(L_BYTES215)

#define L_BYTES216           38ul
#define H_OFFSET216          (H_OFFSET215+L_BYTES215)
#define OFFSET_LABEL4_VALUE                           (H_OFFSET216<<EEHIGHTBYTEPOSITION)|(L_BYTES216)

#define L_BYTES217           1ul
#define H_OFFSET217          (H_OFFSET216+L_BYTES216)
#define OFFSET_LABEL4_FLEX_TAG_TYPE                   (H_OFFSET217<<EEHIGHTBYTEPOSITION)|(L_BYTES217)

#define L_BYTES218           25ul
#define H_OFFSET218          (H_OFFSET217+L_BYTES217)
#define OFFSET_EXTENDED_UNUSED7                       (H_OFFSET218<<EEHIGHTBYTEPOSITION)|(L_BYTES218)

#define L_BYTES219           38ul
#define H_OFFSET219          (H_OFFSET218+L_BYTES218)
#define OFFSET_LABEL5_TAG                             (H_OFFSET219<<EEHIGHTBYTEPOSITION)|(L_BYTES219)

#define L_BYTES220           38ul
#define H_OFFSET220          (H_OFFSET219+L_BYTES219)
#define OFFSET_LABEL5_LOV                             (H_OFFSET220<<EEHIGHTBYTEPOSITION)|(L_BYTES220)

#define L_BYTES221           20ul
#define H_OFFSET221          (H_OFFSET220+L_BYTES220)
#define OFFSET_LABEL5_LABEL                           (H_OFFSET221<<EEHIGHTBYTEPOSITION)|(L_BYTES221)

#define L_BYTES222           38ul
#define H_OFFSET222          (H_OFFSET221+L_BYTES221)
#define OFFSET_LABEL5_VALUE                           (H_OFFSET222<<EEHIGHTBYTEPOSITION)|(L_BYTES222)

#define L_BYTES223           1ul
#define H_OFFSET223          (H_OFFSET222+L_BYTES222)
#define OFFSET_LABEL5_FLEX_TAG_TYPE                   (H_OFFSET223<<EEHIGHTBYTEPOSITION)|(L_BYTES223)

#define L_BYTES224           25ul
#define H_OFFSET224          (H_OFFSET223+L_BYTES223)
#define OFFSET_EXTENDED_UNUSED8                       (H_OFFSET224<<EEHIGHTBYTEPOSITION)|(L_BYTES224)

#define L_BYTES225           38ul
#define H_OFFSET225          (H_OFFSET224+L_BYTES224)
#define OFFSET_LABEL6_TAG                             (H_OFFSET225<<EEHIGHTBYTEPOSITION)|(L_BYTES225)

#define L_BYTES226           38ul
#define H_OFFSET226          (H_OFFSET225+L_BYTES225)
#define OFFSET_LABEL6_LOV                             (H_OFFSET226<<EEHIGHTBYTEPOSITION)|(L_BYTES226)

#define L_BYTES227           20ul
#define H_OFFSET227          (H_OFFSET226+L_BYTES226)
#define OFFSET_LABEL6_LABEL                           (H_OFFSET227<<EEHIGHTBYTEPOSITION)|(L_BYTES227)

#define L_BYTES228           38ul
#define H_OFFSET228          (H_OFFSET227+L_BYTES227)
#define OFFSET_LABEL6_VALUE                           (H_OFFSET228<<EEHIGHTBYTEPOSITION)|(L_BYTES228)

#define L_BYTES229           1ul
#define H_OFFSET229          (H_OFFSET228+L_BYTES228)
#define OFFSET_LABEL6_FLEX_TAG_TYPE                   (H_OFFSET229<<EEHIGHTBYTEPOSITION)|(L_BYTES229)

#define L_BYTES230           25ul
#define H_OFFSET230          (H_OFFSET229+L_BYTES229)
#define OFFSET_EXTENDED_UNUSED9                       (H_OFFSET230<<EEHIGHTBYTEPOSITION)|(L_BYTES230)

#define L_BYTES231           38ul
#define H_OFFSET231          (H_OFFSET230+L_BYTES230)
#define OFFSET_LABEL7_TAG                             (H_OFFSET231<<EEHIGHTBYTEPOSITION)|(L_BYTES231)

#define L_BYTES232           38ul
#define H_OFFSET232          (H_OFFSET231+L_BYTES231)
#define OFFSET_LABEL7_LOV                             (H_OFFSET232<<EEHIGHTBYTEPOSITION)|(L_BYTES232)

#define L_BYTES233           20ul
#define H_OFFSET233          (H_OFFSET232+L_BYTES232)
#define OFFSET_LABEL7_LABEL                           (H_OFFSET233<<EEHIGHTBYTEPOSITION)|(L_BYTES233)

#define L_BYTES234           38ul
#define H_OFFSET234          (H_OFFSET233+L_BYTES233)
#define OFFSET_LABEL7_VALUE                           (H_OFFSET234<<EEHIGHTBYTEPOSITION)|(L_BYTES234)

#define L_BYTES235           1ul
#define H_OFFSET235          (H_OFFSET234+L_BYTES234)
#define OFFSET_LABEL7_FLEX_TAG_TYPE                   (H_OFFSET235<<EEHIGHTBYTEPOSITION)|(L_BYTES235)

#define L_BYTES236           25ul
#define H_OFFSET236          (H_OFFSET235+L_BYTES235)
#define OFFSET_EXTENDED_UNUSED10                       (H_OFFSET236<<EEHIGHTBYTEPOSITION)|(L_BYTES236)

#define L_BYTES237           38ul
#define H_OFFSET237          (H_OFFSET236+L_BYTES236)
#define OFFSET_LABEL8_TAG                             (H_OFFSET237<<EEHIGHTBYTEPOSITION)|(L_BYTES237)

#define L_BYTES238           38ul
#define H_OFFSET238          (H_OFFSET237+L_BYTES237)
#define OFFSET_LABEL8_LOV                             (H_OFFSET238<<EEHIGHTBYTEPOSITION)|(L_BYTES238)

#define L_BYTES239           20ul
#define H_OFFSET239          (H_OFFSET238+L_BYTES238)
#define OFFSET_LABEL8_LABEL                           (H_OFFSET239<<EEHIGHTBYTEPOSITION)|(L_BYTES239)

#define L_BYTES240           38ul
#define H_OFFSET240          (H_OFFSET239+L_BYTES239)
#define OFFSET_LABEL8_VALUE                           (H_OFFSET240<<EEHIGHTBYTEPOSITION)|(L_BYTES240)

#define L_BYTES241           1ul
#define H_OFFSET241          (H_OFFSET240+L_BYTES240)
#define OFFSET_LABEL8_FLEX_TAG_TYPE                   (H_OFFSET241<<EEHIGHTBYTEPOSITION)|(L_BYTES241)

#define L_BYTES242           25ul
#define H_OFFSET242          (H_OFFSET241+L_BYTES241)
#define OFFSET_EXTENDED_UNUSED11                       (H_OFFSET242<<EEHIGHTBYTEPOSITION)|(L_BYTES242)

#define L_BYTES243           38ul
#define H_OFFSET243          (H_OFFSET242+L_BYTES242)
#define OFFSET_LABEL9_TAG                             (H_OFFSET243<<EEHIGHTBYTEPOSITION)|(L_BYTES243)

#define L_BYTES244           38ul
#define H_OFFSET244          (H_OFFSET243+L_BYTES243)
#define OFFSET_LABEL9_LOV                             (H_OFFSET244<<EEHIGHTBYTEPOSITION)|(L_BYTES244)

#define L_BYTES245           20ul
#define H_OFFSET245          (H_OFFSET244+L_BYTES244)
#define OFFSET_LABEL9_LABEL                           (H_OFFSET245<<EEHIGHTBYTEPOSITION)|(L_BYTES245)

#define L_BYTES246           38ul
#define H_OFFSET246          (H_OFFSET245+L_BYTES245)
#define OFFSET_LABEL9_VALUE                           (H_OFFSET246<<EEHIGHTBYTEPOSITION)|(L_BYTES246)

#define L_BYTES247           1ul
#define H_OFFSET247          (H_OFFSET246+L_BYTES246)
#define OFFSET_LABEL9_FLEX_TAG_TYPE                   (H_OFFSET247<<EEHIGHTBYTEPOSITION)|(L_BYTES247)

#define L_BYTES248           25ul
#define H_OFFSET248          (H_OFFSET247+L_BYTES247)
#define OFFSET_EXTENDED_UNUSED12                       (H_OFFSET248<<EEHIGHTBYTEPOSITION)|(L_BYTES248)

#define L_BYTES249           38ul
#define H_OFFSET249          (H_OFFSET248+L_BYTES248)
#define OFFSET_LABEL10_TAG                             (H_OFFSET249<<EEHIGHTBYTEPOSITION)|(L_BYTES249)

#define L_BYTES250           38ul
#define H_OFFSET250          (H_OFFSET249+L_BYTES249)
#define OFFSET_LABEL10_LOV                             (H_OFFSET250<<EEHIGHTBYTEPOSITION)|(L_BYTES250)

#define L_BYTES251           20ul
#define H_OFFSET251          (H_OFFSET250+L_BYTES250)
#define OFFSET_LABEL10_LABEL                           (H_OFFSET251<<EEHIGHTBYTEPOSITION)|(L_BYTES251)

#define L_BYTES252           38ul
#define H_OFFSET252          (H_OFFSET251+L_BYTES251)
#define OFFSET_LABEL10_VALUE                           (H_OFFSET252<<EEHIGHTBYTEPOSITION)|(L_BYTES252)

#define L_BYTES253           1ul
#define H_OFFSET253          (H_OFFSET252+L_BYTES252)
#define OFFSET_LABEL10_FLEX_TAG_TYPE                   (H_OFFSET253<<EEHIGHTBYTEPOSITION)|(L_BYTES253)

#define L_BYTES254           25ul
#define H_OFFSET254          (H_OFFSET253+L_BYTES253)
#define OFFSET_EXTENDED_UNUSED13                       (H_OFFSET254<<EEHIGHTBYTEPOSITION)|(L_BYTES254)



// Decode EEPROM Data...
#define EE_OFFSET(A)         (A>>EEHIGHTBYTEPOSITION)
#define EE_BYTES(B)          (B&0x00FF)



//------------------------------------
//  Reference Time from ..
//------------------------------------
typedef struct __attribute__((packed)) SENSITECH_CONFIGURATION_AREA {    
    UINT16  uwProtocolID;
    UINT16  uwFirmwareVer;
    UINT32  udwManufacturingSerialNumber;
    UINT16  uwNumberOfCustomerResets;
    UINT8   ubUserInformationCharSet;
    //UINT16 uwUnUsedBlock0; // leave this comment as marker...    
    UINT8   uwSensortypeID;   
    UINT8   ubSizeOfEEProm;
    UINT8   uwSensorConfigPrimary;   
    UINT16  uwNumberOfStoredDataPoints;   
    UINT8   uwSensorConfigSecondary;   
    UINT32  udwFirstActivationTimeStamp;
    INT8    pdfTimeZone;
    //UINT16 uwUnUsedBlock1; // leave this comment as marker...    
    UINT8   ubExtendedOptionByte1;
    UINT8   ubExtendedOptionByte2;
    UINT32  udwTimeStampConfigTime;
    UINT8   ubCommandByte;
    UINT8   ub12ByteAlpNumSerialNumber[12];
    float   fSensor1CalOffset_Range_A;
    float   fSensor1CalOffset_Range_B;
    float   fSensor1CalOffset_Range_C;    
    //UINT8  ubUnUsedBlock3[17];  // leave this comment as marker...    
} __SENSITECH_CONFIGURATION_AREA;


typedef struct __attribute__((packed)) USER_CONFIGURATION_AREA {    
    UINT32  udwMeasurementInterval;
    UINT32  udwStartUpDelay;
    UINT8   ubGeneralConfigOptions1_Byte0;
    UINT8   ubGeneralConfigOptions1_Byte1;
    UINT16  uwSoftwareVersionNumber;    
    UINT32  udwTripNumber; // customer progeammable serial number...
    float   fMKTActivationEnergy; // MKT Alarm activation energy.. added new marketing requirement requested by Peter Nunes.. mjm 10312013..  
    //UINT8  NotUsed0;     // leave this comment as marker...    
    UINT16  uwTemperatureIdealRangeHigh;
    UINT16  uwTemperatureIdealRangeLow;
    UINT8   ubAlarmStatus;
    UINT32  udwAlarm1_TriggeredTimeStamp;
    UINT32  udwAlarm2_TriggeredTimeStamp;
    UINT32  udwAlarm3_TriggeredTimeStamp;
    UINT32  udwAlarm4_TriggeredTimeStamp;
    UINT32  udwAlarm5_TriggeredTimeStamp;
    UINT32  udwAlarm6_TriggeredTimeStamp;    
    UINT8   ubRemAlarmDispCfgByte;    
    //UINT32  NotUsed1;    // leave this comment as marker...    
    UINT32  udwNewBatteryTimeStamp;
    UINT32  udwUnit_Start_TimeStamp;
    UINT32  udwUnit_Stop_TimeStamp;
    //UINT8  NotUsed2;    // leave this comment as marker...    
    UINT8   ubLCD_ContrastAdjust;
    UINT8   uwUserName[31];
    //UINT8  NotUsed3;    // leave this comment as marker...     
} __USER_CONFIGURATION_AREA;

 

// Alarm Bits ....

// Alarm Types
#define HTL_SE      1   // High Temp Limit   - Single Event
#define HTL_CE      2   // High Temp Limit   - Cummulative Event
#define LTL_SE      3   // Low Temp Limit    - Single Event
#define LTL_CE      4   // Low Temp Limit    - Cummulative Event       
#define STR_SE      5   // Single Temp Range - Single Event
#define STR_CE      6   // Single Temp Range - Cummulative Event        
#define DTR_SE      7   // Dual Temp Range   - Single Event
#define DTR_CE      8   // Dual Temp Range   - Cummulative Event
#define TIME_ONLY   9   // Time only
#define MKT_HI      10  // MKT High Alarm Limit   ... new marketing requirement requested by Peter Nunes ..10312013 mjm
#define MKT_LO      11  // MKT Low Alarm Limit    ... new marketing requirement requested by Peter Nunes ..10312013 mjm


// Alarm Status 
#define ALARM_ENABLED           1   // Alarm Enable bit..
#define ALARM_TRIGGERED         1   // Alarm Triggered bit..
#define ALARM_OK                0   // Alarm is not Triggered..
#define ALARM_NOT_TRIG_BEFORE   0   // Alarm not triggered previously...
#define MEMORY_FULL             1   // if datapoint reached 16k points...
#define MAXIMUM_DATAPOINTS      16000u

// Alarm Configuration Byte 0 Bits...
typedef union {
    
    struct {
        UINT8 Enable : 1; // LSB
        UINT8 UnUsed : 3;
        UINT8 Type   : 4; // MSB
    }Bit; 
    
    UINT8 ubByte;    
    
} __sBITS_ALARM_CONFIG_BYTE0;  


// Alarm Configuration Area Bits...
// Note: this will be declared in array form with 6 elements... 
typedef struct __attribute__((packed)) {        
    
    UINT8  ubConfigByte0;
    UINT8  ubConfigByte1;
    float  fRange1HighTempSetPoint;  // used in Single Event high limit... 
    float  fRange1LowTempSetPoint;   // used in Single Event low limit... 
    float  fRange2HighTempSetPoint;  // used in Dual temp high limit... 
    float  fRange2LowTempSetPoint;   // used in Dual temp low limit... 
    UINT32 udwCounter;
    UINT32 udwThreshhold;
    
} __ALARM_CONFIGURATION_AREA;


#define ALARM_1     0
#define ALARM_2     1
#define ALARM_3     2
#define ALARM_4     3
#define ALARM_5     4
#define ALARM_6     5
#define IDEAL_TEMP  6

// Alarm Status Bits...
typedef union {
    
    struct {
        UINT8 Alarm1 : 1; // LSB
        UINT8 Alarm2 : 1;
        UINT8 Alarm3 : 1;
        UINT8 Alarm4 : 1;
        UINT8 Alarm5 : 1;
        UINT8 Alarm6 : 1; // MSB
        UINT8 XXXXXX : 2; // unused
    }Bit; 
    
    UINT8 ubByte;    
    
} __sBITS_ALARM_STATUS;  


//------------------------------------
//  Command Byte Status Definition..
//------------------------------------
#define CMD_BYTE_STOPPED    1
#define CMD_BYTE_RUNNING    2
#define CMD_BYTE_SLEEP      4
#define CMD_BYTE_STRDLY     8
#define CMD_BYTE_UNCONFIG   16
#define CMD_BYTE_MARKED     128 // marker flag detected...

// marker status bits...
#define MARKER_SET  0xF
#define MARKER_CLR  0x0
// display LCD summary done...
#define LCD_SUMMARY_DONE    1

// temperature unit
#define DEG_F_UNIT  1
#define DEG_C_UNIT  0

// Alarm Configuration Byte 0 Bits...
typedef union {
    
    struct {
        UINT8 MonitorStatus : 5; // LSB
        UINT8 UnUsed        : 2;
        UINT8 MarkerFlag    : 1; // MSB
    }Bit; 
    
    UINT8 ubByte;    
    
} __sBITS_COMMAND_BYTE;  


// PDF Report option bits description...
#define DO_NOT_CREATE_PDF_REPORT                0
#define CREATE_FULL_PDF_REPORT                  1
#define CREATE_PDF_WO_ALM_COND_INFO             2
#define CREATE_PDF_WO_ALM_COND_ALM_STAT_INFO    3
#define CREATE_PDF_WO_ALM_COND_ALM_STAT_LOG_RES 4

// Sensitech Config Area Extended Byte Option Bits...
typedef union {
    
    struct {
        
        struct {
            UINT8  RH_add1             : 1; // LSB
            UINT8  RH_add2             : 1;
            UINT8  RH_TempCompDis      : 1; 
            UINT8  IdealTempRgViewEna  : 1; 
            UINT8  PrnMonCertValdtn    : 1; 
            UINT8  CfgPassEna          : 1; 
            UINT8  DwnldPassEna        : 1; 
            UINT8  HeartBeatIcoEna     : 1; // MSB
        }Byte1; 

        struct {
            UINT8  DispAlrmNumTrgAlrm  : 1; // LSB
            UINT8  DisTTVFileCreat     : 1;
            UINT8  PdfTimezoneShift    : 1;
            UINT8  PdfDaylightSTime    : 1;
            UINT8  DisablePassword     : 1; //Disable_password - NOT USED
            UINT8  PDFReptCreaCfg      : 3; // MSB
        }Byte2; 
        
    }ExtOpt;    
    
    struct {        
        UINT8 Byte1;  // LSB    
        UINT8 Byte2;  // MSB    
    }Word;
    
    UINT16 uWordData;    
    
} __sBITS_EXTOPT_BYTE;  



// User Config Area General Configuration Option Bits...
typedef union {
    
    struct {
        
        struct {
            UINT8  BaudAdjEna          : 1; // LSB
            UINT8  BlinkModeEna        : 1;
            UINT8  StopKeyEna          : 1; 
            UINT8  AlarmBellIco        : 1; 
            UINT8  StartUpDlyIcoEna    : 1;
            UINT8  StopReConfigEna     : 1;  //new request to for re_config when press hold for 10 seconds
            UINT8  UnUsed              : 1; // MSB
            UINT8  ShippingInfoEna     : 1;
        }Byte0; 

        struct {
            UINT8  StopKeyDlyEna       : 1; // LSB
            UINT8  MultDropFlag        : 1;
            UINT8  F_C_Unit_Select     : 1; 
            UINT8  AlarmBellNotifEna   : 1; 
            UINT8  LCDSumDatDispEna    : 1; 
            UINT8  StartKeyDlyEna      : 1; 
            UINT8  CDegDoubleDecimal   : 1; // new marketing requirement double decimal bit enable... 8/22/2013
            //UINT8  UnUsed             : 1; 
            UINT8  DispCurntTempEna    : 1; // MSB
        }Byte1; 
        
    }GenOpt;    
    
    struct {        
        UINT8 Byte0;  // LSB    
        UINT8 Byte1;  // MSB    
    }Word;
    
    UINT16 uWordData;    
    
} __sBITS_GENCFGOPT_BYTE;  

// User Config Area Ideal Range Temperature Data...
typedef struct {
    float fTempIdealRangeHighValue;    
    float fTempIdealRangeLowValue;    
} __sBITS_USERCFG_DATA; 

// User Config Remaining/Elapsed Alarm Display Configuration Byte Masked bits...
#define MASK_ALARM_TIME_REM_ELP_DISP_ENA        1
#define MASK_ALARM_TIME_REM_ELP_SELECT          2 
#define MASK_ALARM_TIME_REM_ELP_ALM_NUM         0xF0 
#define MASK_ALARM_TIME_SELECT_ELAPSE           3
#define ALARM_TIME_SELECT_ELAPSE                3 
#define ALARM_TIME_REM_ELP_DISP_ENA             1

// User Config Remaining/Elapsed Alarm Display Configuration Byte Definition...
#define ALARM_TIME_SELECT_REMAINING_TIME        0
#define ALARM_TIME_SELECT_ELAPSE_TIME           2


// USB serial number storage varialble...
typedef struct __attribute__((packed)) {
    BYTE bLength;
    BYTE bDscType;
    WORD string[10];
} __sTT4USBMA_SN;

// Alarm trigger number of events...  
typedef struct __attribute__((packed)) {
    UINT32  udwTotalTime;
    UINT16  uwNumOfEvents;
    BOOL    bStatusFlag;
} __sTRIG_EVENTS_STAT;


// EEPROM selectable sizes definition...
#define EEPROM_CODE_MEM_SIZE_2K       0
#define EEPROM_CODE_MEM_SIZE_4K       1
#define EEPROM_CODE_MEM_SIZE_8K       2
#define EEPROM_CODE_MEM_SIZE_16K      3
// EEPROM physical memory sizes definition...
#define EEPROM_PHYSICAL_MEM_SIZE_2K       1920u // as a 2K memory setting the actual capacity is only 1920, Peter requested this change for backward compatibility... 04/01/2011 ..mjm
#define EEPROM_PHYSICAL_MEM_SIZE_4K       4000u
#define EEPROM_PHYSICAL_MEM_SIZE_8K       8000u
#define EEPROM_PHYSICAL_MEM_SIZE_16K      16000u


// Sensor 1 temperature calibration offset...  
typedef struct __attribute__((packed)) {
    
    float   fRangeA_FDeg;    
    float   fRangeB_FDeg;    
    float   fRangeC_FDeg; 
    
    float   fTempLevel_1_FDeg;
    float   fTempLevel_2_FDeg;
    float   fTempLevel_3_FDeg;
    float   fTempLevel_4_FDeg;    
       
} __sSENSOR1_TEMP_CAL_OFFSET;


// global function declarations....
void commTT4Cfg_ClearEeprom(void);
void commTT4Cfg_Load_AlarmConfig_Values(void);
void commTT4Cfg_Load_SensitechConfig_Values(void);
void commTT4Cfg_Load_UserConfig_Values(void);
void commTT4Cfg_Load_UserInformation_Values(void);    
void commTT4Cfg_Load_PasswordConfig_Values(void);
void commTT4Cfg_Load_UserName_Values(void);
void commTT4Cfg_Load_ThreePointCertifate_Values(void);
void commTT4Cfg_LoadCfgDefaultValues(void);
void commTT4Cfg_WriteCfgEeprom(UINT32 par_udwEEData, UINT8 *par_ubData);
void commTT4Cfg_ReadCfgEeprom(UINT32 par_udwEEData, UINT8 *par_ubData);
void commTT4Cfg_WriteEeprom_RawDataPoint(UINT16 par_uwDataPointNumber, UINT16 par_uwRawData);
void commTT4Cfg_ReadEeprom_RawDataPoint(UINT16 par_uwDataPointNumber, UINT16 *par_uwRawData);
void commTT4Cfg_InitEeprom(void);
void commTT4Cfg_InitControlVariables(void);
void commTT4Cfg_RetrieveTempDataFromEeprom(UINT16 par_uwDataPointNumber, UINT8 *par_ubMarkerStatus, float *par_fTempData);
void commTT4Cfg_LoadMem_EEPROM_ConfigurationStatus(void);
void commTT4Cfg_Initialize(void);
float commTT4Cfg_ConvertToFinalTempData(UINT16 par_uwBitValue);
void commTT4Cfg_EERPOM_PostDataLogging(void);
void commTT4Cfg_FastWriteCfgEeprom(UINT32 par_udwEEData, const void *par_ubData);
void commTT4Cfg_FastReadCfgEeprom(UINT32 par_udwEEData, void *par_vData);
void commTT4Cfg_ClearEEPROMSpecificData(void);
void commTT4Cfg_ForDebugging_LoadPresetEEPROMBitValues(void);
void commTT4Cfg_USBLoadSerialNumber(UINT32 par_udwTT4MA_SN);
void commTT4Cfg_USBLoadStringSerialNumber(char *par_strTT4MA_SN);
void commTT4Cfg_CacheMem_UploadEepromDatapoints(void);
UINT16 commTT4Cfg_CacheMem_GetDataPointNum(UINT16 par_DataPointNumber);
void commTT4Cfg_CacheMem_RetrieveTempData(UINT16 par_uwDataPointNumber, UINT8 *par_ubMarkerStatus, float *par_fTempData);


#endif /* __COMMTT4CONFIG_H */

/* ------------ End Of File ------------ */
