/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm                                            **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc. and Relisource Technology Ltd.                            **
 **                                                                           **
 **  Copryright(c) 2015. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Relisource Technology Ltd.                        **
 **  Creation date       :  02/20/2015                                        **
 **  Programmer          :  Faijur Rahman		                              **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : drvI2C_Wrapper.c                                                  **
 **                                                                           **
 **  Description :                                                            **
 **    - This file includes required API for legacy code of TT4MA USB related **
 **		 to I2C so that legacy code can call functions from ASF.			  **
 **                                                                           **
 **  History :                                                                ** 
 **    ver 1.00.001  -  02/20/2015                                            **
 **                                                                           **
 ******************************************************************************/

#include "asf.h"
#include "TT4USB_Board.h"
#include "drvI2C_Wrapper.h"


//******************************************************************************
//		TEMP Related functions
//******************************************************************************
twi_packet_t temp_packet_tx, temp_packet_rx;

uint32_t temp_i2c_init(void)
{
	temp_packet_tx.chip        = TEMP_ADDRESS;
	temp_packet_tx.addr[0]     = TEMP_MEM_ADDR >> 8;
	temp_packet_tx.addr[1]     = TEMP_MEM_ADDR;
	temp_packet_tx.addr_length = TEMP_MEM_ADDR_LENGTH;
	
	temp_packet_rx.chip        = TEMP_ADDRESS;
	temp_packet_rx.addr[0]     = TEMP_MEM_ADDR >> 8;
	temp_packet_rx.addr[1]     = TEMP_MEM_ADDR;
	temp_packet_rx.addr_length = TEMP_MEM_ADDR_LENGTH;

	flexcom_enable(TEMP_TWI_FLEX_BASE);
	flexcom_set_opmode(TEMP_TWI_FLEX_BASE, FLEXCOM_TWI);
	
	twi_master_options_t opt;
	opt.master_clk = sysclk_get_cpu_hz();
	opt.speed      = TWI_CLK;
	opt.chip       = TEMP_ADDRESS;
		
	return twi_master_setup(TEMP_TWI_BASE, &opt);
}

uint32_t temp_i2c_write(uint8_t * data_tx, uint8_t len)
{
	temp_packet_tx.buffer  = (uint8_t *) data_tx;
	temp_packet_tx.length  = len;
	return twi_master_write(TEMP_TWI_BASE, &temp_packet_tx);
}

uint32_t temp_i2c_read(uint8_t * data_rx, uint8_t len)
{
	temp_packet_rx.buffer  = (uint8_t *) data_rx;
	temp_packet_rx.length  = len;
	return twi_master_read(TEMP_TWI_BASE, &temp_packet_rx);
}

//******************************************************************************
//		EEPROM Related functions
//******************************************************************************
twi_packet_t eeprom_packet_tx, eeprom_packet_rx;

uint32_t eeprom_i2c_init(void)
{
	eeprom_packet_tx.chip        = EEPROM_ADDRESS;
	eeprom_packet_tx.addr[0]     = EEPROM_MEM_ADDR >> 8;
	eeprom_packet_tx.addr[1]     = EEPROM_MEM_ADDR;
	eeprom_packet_tx.addr_length = EEPROM_MEM_ADDR_LENGTH;
	
	eeprom_packet_rx.chip        = EEPROM_ADDRESS;
	eeprom_packet_rx.addr[0]     = EEPROM_MEM_ADDR >> 8;
	eeprom_packet_rx.addr[1]     = EEPROM_MEM_ADDR;
	eeprom_packet_rx.addr_length = EEPROM_MEM_ADDR_LENGTH;

	flexcom_enable(EEPROM_TWI_FLEX_BASE);
	flexcom_set_opmode(EEPROM_TWI_FLEX_BASE, FLEXCOM_TWI);
	
	twi_master_options_t opt;
	opt.master_clk = sysclk_get_cpu_hz();
	opt.speed      = TWI_CLK;
	opt.chip       = EEPROM_ADDRESS;
		
	return twi_master_setup(EEPROM_TWI_BASE, &opt);
}

uint32_t eeprom_i2c_write(uint8_t * data_tx, uint8_t len, uint16_t addr)
{
	eeprom_packet_tx.addr[0] = addr >> 8;
	eeprom_packet_tx.addr[1] = addr;
	eeprom_packet_tx.buffer  = (uint8_t *) data_tx;
	eeprom_packet_tx.length  = len;
	return twi_master_write(EEPROM_TWI_BASE, &eeprom_packet_tx);
}

uint32_t eeprom_i2c_read(uint8_t * data_rx, uint8_t len, uint16_t addr)
{
	eeprom_packet_rx.addr[0] = addr >> 8;
	eeprom_packet_rx.addr[1] = addr;
	eeprom_packet_rx.buffer  = (uint8_t *) data_rx;
	eeprom_packet_rx.length  = len;
	return twi_master_read(EEPROM_TWI_BASE, &eeprom_packet_rx);
}


//******************************************************************************
//		LCD Related functions
//******************************************************************************
twi_packet_t lcd_packet_tx, lcd_packet_rx;

uint32_t lcd_i2c_init(void)
{
	lcd_packet_tx.chip        = LCD_ADDRESS;
	lcd_packet_tx.addr[0]     = LCD_MEM_ADDR >> 8;
	lcd_packet_tx.addr[1]     = LCD_MEM_ADDR;
	lcd_packet_tx.addr_length = LCD_MEM_ADDR_LENGTH;
	
	lcd_packet_rx.chip        = LCD_ADDRESS;
	lcd_packet_rx.addr[0]     = LCD_MEM_ADDR >> 8;
	lcd_packet_rx.addr[1]     = LCD_MEM_ADDR;
	lcd_packet_rx.addr_length = LCD_MEM_ADDR_LENGTH;

	flexcom_enable(LCD_TWI_FLEX_BASE);
	flexcom_set_opmode(LCD_TWI_FLEX_BASE, FLEXCOM_TWI);
	
	twi_master_options_t opt;
	opt.master_clk = sysclk_get_cpu_hz();
	opt.speed      = TWI_CLK;
	opt.chip       = LCD_ADDRESS;
		
	return twi_master_setup(LCD_TWI_BASE, &opt);
}

uint32_t lcd_i2c_write(uint8_t * data_tx, uint8_t len)
{
	lcd_packet_tx.buffer  = (uint8_t *) data_tx;
	lcd_packet_tx.length  = len;
	return twi_master_write(LCD_TWI_BASE, &lcd_packet_tx);
}

uint32_t lcd_i2c_read(uint8_t * data_rx, uint8_t len)
{
	lcd_packet_rx.buffer  = (uint8_t *) data_rx;
	lcd_packet_rx.length  = len;
	return twi_master_read(LCD_TWI_BASE, &lcd_packet_rx);
}
	  