#ifdef I2C_LEGACY_CODE


/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm                                            **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc. and Blue Ocean Innovation Ltd.                            **
 **                                                                           **
 **  Copryright(c) 2010. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Blue Ocean Innovation Ltd.                        **
 **  Project#            :  449                                               **
 **  Creation date       :  08/20/2010                                        **
 **  Programmer          :  Michael J. Mariveles                              **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : drvI2C.h                                                          **
 **                                                                           **
 **  Description :                                                            **
 **    - I2C communication driver.                                            **
 **                                                                           **
 ******************************************************************************/

#ifndef __DRVI2C_H
#define __DRVI2C_H
#include "types.h"
// ACK response bit...
#define ACK_OK  0
#define NO_ACK  1

// ACK type status...
#define ACK_LOW     0
#define ACK_HIGH    1

void drvI2C_uDelay(UINT16 par_uwData);

// I2C3 functions...
void drvI2C3_SendByte(UINT8 par_ubByteData);
void drvI2C3_ReadByte(UINT8 *par_ubByte);
UINT8 drvI2C3_ReadACK(UINT8 par_ubAckType);
void drvI2C3_WriteACK(UINT8 par_ubAckType);
void drvI2C3_SetSDAIn(void);
void drvI2C3_SetSDAOut(void);
void drvI2C3_StartBit(void);
void drvI2C3_StopBit(void);
void drvI2C3_Init(void);
void drvI2C3_StopStartBit(void);

// I2C2 functions...
void drvI2C2_SendByte(UINT8 par_ubByteData);
void drvI2C2_ReadByte(UINT8 *par_ubByte);
UINT8 drvI2C2_ReadACK(UINT8 par_ubAckType);
void drvI2C2_WriteACK(UINT8 par_ubAckType);
void drvI2C2_SendACK(UINT8 par_ubAckType);
void drvI2C2_SetSDAIn(void);
void drvI2C2_SetSDAOut(void);
void drvI2C2_StartBit(void);
void drvI2C2_StopBit(void);
void drvI2C2_Init(void);
void drvI2C2_StopStartBit(void);

// I2C1 functions...
void drvI2C1_SendByte(UINT8 par_ubByteData);
void drvI2C1_ReadByte(UINT8 *par_ubByte);
UINT8 drvI2C1_ReadACK(UINT8 par_ubAckType);
void drvI2C1_SetSDAIn(void);
void drvI2C1_SetSDAOut(void);
void drvI2C1_StartBit(void);
void drvI2C1_StopBit(void);
void drvI2C1_Init(void);
void drvI2C1_StopStartBit(void);

#endif /* __DRVI2C_H */

/* ------------ End Of File ------------ */
#endif