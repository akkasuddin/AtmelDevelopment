/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm                                            **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc. and Relisource Technology Ltd.                            **
 **                                                                           **
 **  Copryright(c) 2015. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Relisource Technology Ltd.                        **
 **  Creation date       :  02/20/2015                                        **
 **  Programmer          :  Faijur Rahman		                              **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : drvI2C_Wrapper.h                                                  **
 **                                                                           **
 **  Description :                                                            **
 **    - This file includes required API for legacy code of TT4MA USB related **
 **		 to I2C so that legacy code can call functions from ASF.			  **
 **                                                                           **
 **  History :                                                                ** 
 **    ver 1.00.001  -  02/20/2015                                            **
 **                                                                           **
 ******************************************************************************/

#ifndef I2C_WRAPPER_H
#define I2C_WRAPPER_H

#include "types.h"

#define WAIT_TIME				10
#define TWI_CLK					100000


//TEMPERATURE SENSOR RELATED DEFINITIONS
#define TEMP_ADDRESS			0x48
#define TEMP_MEM_ADDR			0
#define TEMP_MEM_ADDR_LENGTH	0
#define TEMP_WAIT_TIME			WAIT_TIME
#define TEMP_TWI_CLK			TWI_CLK

uint32_t temp_i2c_init(void);
uint32_t temp_i2c_write(uint8_t * data_tx, uint8_t len);
uint32_t temp_i2c_read(uint8_t * data_rx, uint8_t len);


//EEPROM RELATED DEFINITIONS
#define EEPROM_ADDRESS			0x50
#define EEPROM_MEM_ADDR			0
#define EEPROM_MEM_ADDR_LENGTH	2
#define EEPROM_WAIT_TIME		WAIT_TIME
#define EEPROM_TWI_CLK			TWI_CLK

uint32_t eeprom_i2c_init(void);
uint32_t eeprom_i2c_write(uint8_t * data_tx, uint8_t len, uint16_t addr);
uint32_t eeprom_i2c_read(uint8_t * data_rx, uint8_t len, uint16_t addr);


//LCD RELATED DEFINITIONS
#define LCD_ADDRESS				0x3E
#define LCD_MEM_ADDR			0
#define LCD_MEM_ADDR_LENGTH		0
#define LCD_WAIT_TIME			WAIT_TIME
#define LCD_TWI_CLK				TWI_CLK

uint32_t lcd_i2c_init(void);
uint32_t lcd_i2c_write(uint8_t * data_tx, uint8_t len);
uint32_t lcd_i2c_read(uint8_t * data_rx, uint8_t len);

#endif // I2C_WRAPPER_H
