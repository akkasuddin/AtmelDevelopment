#ifdef I2C_LEGACY_CODE

/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm                                            **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc. and Blue Ocean Innovation Ltd.                            **
 **                                                                           **
 **  Copryright(c) 2010. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Blue Ocean Innovation Ltd.                        **
 **  Project#            :  449                                               **
 **  Creation date       :  08/20/2010                                        **
 **  Programmer          :  Michael J. Mariveles                              **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : drvI2C.c                                                          **
 **                                                                           **
 **  Description :                                                            **
 **    - I2C communication driver.                                            **
 **                                                                           **
 ******************************************************************************/

#include "hwPIC24F.h"
#include  <p24fxxxx.h>
#include "hwSystem.h"
#include "drvI2C.h"



// I2C2 Control : 32k EEPROM...
#define _SCL2_CFG_OUT   _TRISF5 = 0;   // 0 - output mode
#define _SDA2_CFG_IN    _TRISF4 = 1;   // 1 - input mode
#define _SDA2_CFG_OUT   _TRISF4 = 0;   // 0 - output mode
#define _SDA2_CTRL      _RF4
#define _SCL2_CTRL      _RF5

// I2C1 Control : LCD Display sensor...
#define _SCL1_CFG_OUT   _TRISD10 = 0;   // 0 - output mode
#define _SDA1_CFG_IN    _TRISD9  = 1;   // 1 - input mode
#define _SDA1_CFG_OUT   _TRISD9  = 0;   // 0 - output mode
#define _SDA1_CTRL      _RD9
#define _SCL1_CTRL      _RD10

// I2C3 Control : Temperature sensor...
#define _SCL3_CFG_OUT   _TRISE6  = 0;   // 0 - output mode
#define _SDA3_CFG_IN    _TRISE7  = 1;   // 1 - input mode
#define _SDA3_CFG_OUT   _TRISE7  = 0;   // 0 - output mode
#define _SDA3_CTRL      _RE7
#define _SCL3_CTRL      _RE6



#define PORT_HIGH      1
#define PORT_LOW       0

#define ACK_PASS    1
#define ACK_FAIL    0


// SDA input ACK debounce count..
#define SDA_INPUT_ACK_DEBOUNCE_COUNT    1

// intialization delay..
#define INT_I2C_DLY    5

// start delay..
#define STR_I2C_DLY    10 //5

// stop delay..
#define STP_I2C_DLY1    5
#define STP_I2C_DLY2    5

// begin send byte delay..
#define BEGIN_SNDBYTE_I2C_DLY   5
#define END_SNDBYTE_I2C_DLY     3
#define PW_SCL_DLY              5
#define PW_SDA_DLY              3


//----------------------------------------------------------------------
// exclusive for Temperature sensor I2C communication baud rate to 
// match-up the Psoc3 TMP112 simulator tool to verify the accuracy
// of TT4USB-MA , this change was reqested by Eric Day
//
//  \mjm 12022012...
//----------------------------------------------------------------------

// intialization delay..
#define SEN_INT_I2C_DLY    10

// start delay..
#define SEN_STR_I2C_DLY    10

// stop delay..
#define SEN_STP_I2C_DLY1    10
#define SEN_STP_I2C_DLY2    10

// begin send byte delay..
#define SEN_BEGIN_SNDBYTE_I2C_DLY   10
#define SEN_END_SNDBYTE_I2C_DLY     6
#define SEN_PW_SCL_DLY              10
#define SEN_PW_SDA_DLY              6



void drvI2C_uDelay(UINT16 par_uwData){

    while(--par_uwData) Nop();
}

/*---------------------------------------------------------
  I2C2 32k EEPROM Control...
---------------------------------------------------------*/


/*******************************************************************************
**  Function     : drvI2C2_Init
**  Description  : intializes I2C port #2 to control 32K EEPROM. 
**  PreCondition : setup the control I/O's prior calling this routine..
**  Side Effects : changes the state of an I/O
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/     
void drvI2C2_Init(void){    
    
    drvI2C_uDelay(20); 
    
    _SDA2_CFG_OUT;
    Nop();
    _SDA2_CTRL = 0;    
    Nop();
    _SCL2_CFG_OUT;
    Nop();
    _SCL2_CTRL = 0;
    
    drvI2C_uDelay(INT_I2C_DLY);     
    _SDA2_CTRL = 1;
    drvI2C_uDelay(INT_I2C_DLY); 
    _SCL2_CTRL = 1;    

}

/*******************************************************************************
**  Function     : drvI2C2_StopStartBit
**  Description  : sets I2C port#2 the stop to start bit from succeeding calls.
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void drvI2C2_StopStartBit(void){ 
    
    drvI2C_uDelay(STR_I2C_DLY); 
    _SDA2_CTRL = 1;    
    Nop();    
    _SCL2_CTRL = 1;    
    drvI2C_uDelay(STR_I2C_DLY); 
}

/*******************************************************************************
**  Function     : drvI2C2_StartBit
**  Description  : sets I2C port#2 the start bit.
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void drvI2C2_StartBit(void){
    
    Nop();
    _SDA2_CFG_OUT;
    Nop();

    drvI2C_uDelay(STR_I2C_DLY); 
    _SDA2_CTRL = 0;
    drvI2C_uDelay(STR_I2C_DLY); 
    _SCL2_CTRL = 0;
    Nop();    
    
}

/*******************************************************************************
**  Function     : drvI2C2_StopBit
**  Description  : sets I2C port#2 the stop bit
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void drvI2C2_StopBit(void){

    Nop();
    _SDA2_CFG_OUT;
    Nop();

    drvI2C_uDelay(STP_I2C_DLY1);
    _SCL2_CTRL = 1;
    drvI2C_uDelay(STP_I2C_DLY2);     
    _SDA2_CTRL = 1;
    drvI2C_uDelay(STP_I2C_DLY2); 

}

/*******************************************************************************
**  Function     : drvI2C2_SendByte
**  Description  : write's byte data to I2C port#2
**  PreCondition : must call first the start bit
**  Side Effects : none
**  Input        : par_ubByteData - byte data value to write
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void drvI2C2_SendByte(UINT8 par_ubByteData){

    register UINT8 lubCtr;    
    
    drvI2C_uDelay(BEGIN_SNDBYTE_I2C_DLY); 
        
    lubCtr = 8;     
    // loop 8x as 1 byte is equal to 8 bit..    
    do{  
        // decrement bit data position...
        --lubCtr;

        // get SDA bit data value...
        _SDA2_CTRL = (par_ubByteData & (0x1 << lubCtr)) >> lubCtr; 
                
        // SCL clcok pulse start...
        drvI2C_uDelay(PW_SDA_DLY);
        _SCL2_CTRL = PORT_HIGH;
        drvI2C_uDelay(PW_SCL_DLY);
        _SCL2_CTRL = PORT_LOW;
        Nop();Nop();
        
    }while(lubCtr!=0);    
    
    drvI2C_uDelay(END_SNDBYTE_I2C_DLY);
    _SDA2_CTRL = PORT_LOW;
    
}

/*******************************************************************************
**  Function     : drvI2C2_ReadACK
**  Description  : read's ACK bit from I2C port#2
**  PreCondition : none
**  Side Effects : none
**  Input        : par_ubAckType - ACK type bit
**  Output       : return ACK bit status
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
UINT8 drvI2C2_ReadACK(UINT8 par_ubAckType){

    register UINT8 lubCtr;
    register UINT8 lubDebounce;
    register UINT8 lubACKStatus;    
    
    lubACKStatus = ACK_FAIL;
    lubDebounce = 0;
    
    Nop();
    _SDA2_CFG_IN;
    Nop();    
    
    for(lubCtr = 0 ; lubCtr < 255 ; lubCtr++){

        if(_SDA2_CTRL == par_ubAckType){            
                        
            lubDebounce++;     
            
            if(lubDebounce > SDA_INPUT_ACK_DEBOUNCE_COUNT){                
                
                lubACKStatus = ACK_PASS;
                break;
            }     
        }     
    }
    
    // SCL clcok pulse start...    
    Nop();Nop();    
    _SCL2_CTRL = PORT_HIGH;
    drvI2C_uDelay(PW_SCL_DLY);
    _SCL2_CTRL = PORT_LOW;
    Nop();Nop();    

    Nop();
    _SDA2_CFG_OUT;
    Nop();
    
    _SDA2_CTRL = PORT_LOW;
    Nop();
    
    if(lubACKStatus == ACK_PASS) return(par_ubAckType);

    return(!par_ubAckType);

}

/*******************************************************************************
**  Function     : drvI2C2_SetSDAOut
**  Description  : configures SDA as output for writing byte data..
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void drvI2C2_SetSDAOut(void){

    Nop();
    _SDA2_CFG_OUT;
    Nop();        

}

/*******************************************************************************
**  Function     : drvI2C2_SetSDAIn
**  Description  : configures SDA as input for reading byte data..
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void drvI2C2_SetSDAIn(void){

    Nop();
    _SDA2_CFG_IN;
    Nop();
    
}

/*******************************************************************************
**  Function     : drvI2C2_ReadByte
**  Description  : read's byte data from I2C port#2
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : par_ubByte - output byte data received
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void drvI2C2_ReadByte(UINT8 *par_ubByte){
    
    register UINT8 lubCtr;   
    register UINT8 lubSDAByteData;    
    register UINT8 lubSDABit;   
        
    drvI2C_uDelay(BEGIN_SNDBYTE_I2C_DLY); 
    
    _SDA2_CFG_IN;    
    Nop();
   
    // reset SDA Byte data value...   
    lubSDAByteData = 0;         

    // set to bit 8th position...
    lubCtr = 8;     
        
    // loop 8x as 1 byte is equal to 8 bit..            
    do{                                               
        // decrement bit data position...
        --lubCtr; 
        
        lubSDABit = _SDA2_CTRL;
        
        // Accumulate the bit shifting data...
        lubSDAByteData |= lubSDABit << lubCtr;      
                
        // SCL clcok pulse start...
        drvI2C_uDelay(PW_SDA_DLY);
        _SCL2_CTRL = PORT_HIGH;
        drvI2C_uDelay(PW_SCL_DLY);
        _SCL2_CTRL = PORT_LOW; 
        Nop();

    }while(lubCtr!=0);
    
    // pass by reference Byte Data...
    *par_ubByte = lubSDAByteData;    
}


/*******************************************************************************
**  Function     : drvI2C2_WriteACK
**  Description  : write's ACK bit
**  PreCondition : for succeeding page write mode..
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void drvI2C2_WriteACK(UINT8 par_ubAckType){

    Nop();
    _SDA2_CFG_OUT;
    Nop();Nop();    
    
    // SCL clcok pulse start...        
    _SDA2_CTRL = par_ubAckType; 
    Nop();Nop();    
    _SCL2_CTRL = PORT_HIGH;    
    drvI2C_uDelay(PW_SCL_DLY); 
    _SCL2_CTRL = PORT_LOW;     
    Nop();Nop();    

}





/*------------------------------------------------------------------------------
  I2C1 LCD Display Control...
-------------------------------------------------------------------------------*/

/*******************************************************************************
**  Function     : drvI2C1_Init
**  Description  : intializes I2C port #1 to control LCD output..
**  PreCondition : setup the control I/O's prior calling this routine..
**  Side Effects : changes the state of an I/O
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void drvI2C1_Init(void){    
    
    drvI2C_uDelay(20); 
    
    _SDA1_CFG_OUT;
    Nop();
    _SDA1_CTRL = 0;    
    Nop();
    _SCL1_CFG_OUT;
    Nop();
    _SCL1_CTRL = 0;
    
    drvI2C_uDelay(INT_I2C_DLY);     
    _SDA1_CTRL = 1;
    drvI2C_uDelay(INT_I2C_DLY); 
    _SCL1_CTRL = 1;    

}

/*******************************************************************************
**  Function     : drvI2C1_StopStartBit
**  Description  : sets I2C port#2 the stop to start bit from succeeding calls.
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void drvI2C1_StopStartBit(void){ // used only in random read mode..
    
    drvI2C_uDelay(STR_I2C_DLY); 
    _SDA1_CTRL = 1;    
    Nop();    
    _SCL1_CTRL = 1;    
    drvI2C_uDelay(STR_I2C_DLY); 
}

/*******************************************************************************
**  Function     : drvI2C1_StartBit
**  Description  : sets I2C port#1 the start bit.
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void drvI2C1_StartBit(void){
    
    drvI2C_uDelay(STR_I2C_DLY); 
    _SDA1_CTRL = 0;
    drvI2C_uDelay(STR_I2C_DLY); 
    _SCL1_CTRL = 0;
    Nop();    
    
}

/*******************************************************************************
**  Function     : drvI2C1_StopBit
**  Description  : sets I2C port#2 the stop bit
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void drvI2C1_StopBit(void){

    drvI2C_uDelay(STP_I2C_DLY1);
    _SCL1_CTRL = 1;
    drvI2C_uDelay(STP_I2C_DLY2);     
    _SDA1_CTRL = 1;
    drvI2C_uDelay(STP_I2C_DLY2); 

}

/*******************************************************************************
**  Function     : drvI2C1_SendByte
**  Description  : write's byte data to I2C port#1
**  PreCondition : must call first the start bit
**  Side Effects : none
**  Input        : par_ubByteData - byte data value to write
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void drvI2C1_SendByte(UINT8 par_ubByteData){

    UINT8 lubCtr;
    
    drvI2C_uDelay(BEGIN_SNDBYTE_I2C_DLY);          // 5 usecs inter packet delay...
        
    lubCtr = 8;     
    // loop 8x as 1 byte is equal to 8 bit..    
    do{  
        // decrement bit data position...
        --lubCtr;

        // get SDA bit data value...
        _SDA1_CTRL = (par_ubByteData & (0x1 << lubCtr)) >> lubCtr;
                
        // SCL clcok pulse start...
        drvI2C_uDelay(PW_SDA_DLY);
        _SCL1_CTRL = PORT_HIGH; 
        drvI2C_uDelay(PW_SCL_DLY);
        _SCL1_CTRL = PORT_LOW;
        Nop();Nop();
        
    }while(lubCtr!=0);    
    
    drvI2C_uDelay(END_SNDBYTE_I2C_DLY); 
    _SDA1_CTRL = PORT_LOW;  
    
}

/*******************************************************************************
**  Function     : drvI2C1_ReadACK
**  Description  : read's ACK bit from I2C port#1
**  PreCondition : none
**  Side Effects : none
**  Input        : par_ubAckType - ACK type bit
**  Output       : return ACK bit status
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
UINT8 drvI2C1_ReadACK(UINT8 par_ubAckType){

    UINT8 lubCtr;
    UINT8 lubDebounce;
    UINT8 lubACKStatus;    
    
    lubACKStatus = ACK_FAIL;
    lubDebounce = 0;
    
    Nop();
    _SDA1_CFG_IN;
    Nop();    
    
    for(lubCtr = 0 ; lubCtr < 255 ; lubCtr++){

        if(_SDA1_CTRL == par_ubAckType){            
                        
            lubDebounce++;     
            
            if(lubDebounce > SDA_INPUT_ACK_DEBOUNCE_COUNT){                
                
                lubACKStatus = ACK_PASS;
                break;
            }     
        }     
    }
    
    // SCL clcok pulse start...    
    Nop();Nop();    
    _SCL1_CTRL = PORT_HIGH; 
    drvI2C_uDelay(PW_SCL_DLY);
    _SCL1_CTRL = PORT_LOW; 
    Nop();Nop();    

    Nop();
    _SDA1_CFG_OUT;
    Nop();
    
    _SDA1_CTRL = PORT_LOW;  
    Nop();
    
    if(lubACKStatus == ACK_PASS) return(par_ubAckType);

    return(!par_ubAckType);

}

/*******************************************************************************
**  Function     : drvI2C1_SetSDAOut
**  Description  : configures SDA as output for writing byte data..
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void drvI2C1_SetSDAOut(void){

    Nop();
    _SDA1_CFG_OUT;
    Nop();        

}

/*******************************************************************************
**  Function     : drvI2C1_SetSDAIn
**  Description  : configures SDA as input for reading byte data..
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void drvI2C1_SetSDAIn(void){

    Nop();
    _SDA1_CFG_IN;
    Nop();
    
}

/*******************************************************************************
**  Function     : drvI2C1_ReadByte
**  Description  : read's byte data from I2C port#1
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : par_ubByte - byte data value
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void drvI2C1_ReadByte(UINT8 *par_ubByte){
    
    UINT8 lubCtr;   
    UINT8 lubSDAByteData;    
    UINT8 lubSDABit;   
        
    drvI2C_uDelay(BEGIN_SNDBYTE_I2C_DLY); 
    
    _SDA1_CFG_IN;
    Nop();
   
    // reset SDA Byte data value...   
    lubSDAByteData = 0;         

    // set to bit 8th position...
    lubCtr = 8;     
        
    // loop 8x as 1 byte is equal to 8 bit..            
    do{                                               
        // decrement bit data position...
        --lubCtr; 
        
        lubSDABit = _SDA1_CTRL;
        
        // Accumulate the bit shifting data...
        lubSDAByteData |= lubSDABit << lubCtr;      
                
        // SCL clcok pulse start...
        drvI2C_uDelay(PW_SDA_DLY); 
        _SCL1_CTRL = PORT_HIGH;    
        drvI2C_uDelay(PW_SCL_DLY); 
        _SCL1_CTRL = PORT_LOW;   
        Nop();

    }while(lubCtr!=0);
    
    // pass by reference Byte Data...
    *par_ubByte = lubSDAByteData;    
}






/*------------------------------------------------------------------------------
  I2C3 Temperature Sensor Control...
------------------------------------------------------------------------------*/

/*******************************************************************************
**  Function     : drvI2C3_Init
**  Description  : intializes I2C port #3 to control TMP112 temperature sensor..
**  PreCondition : setup the control I/O's prior calling this routine..
**  Side Effects : changes the state of an I/O
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void drvI2C3_Init(void){    
    
    drvI2C_uDelay(20); 
    
    _SDA3_CFG_OUT;
    Nop();Nop();Nop();
    _SDA3_CTRL = 0;    
    Nop();Nop();Nop();
    _SCL3_CFG_OUT;
    Nop();Nop();Nop();
    _SCL3_CTRL = 0;
    
    drvI2C_uDelay(SEN_INT_I2C_DLY);     
    _SDA3_CTRL = 1;
    drvI2C_uDelay(SEN_INT_I2C_DLY); 
    _SCL3_CTRL = 1;    

}


/*******************************************************************************
**  Function     : drvI2C3_StopStartBit
**  Description  : sets I2C port#3 the stop to start bit from succeeding calls.
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void drvI2C3_StopStartBit(void){ 
    
    drvI2C_uDelay(SEN_STR_I2C_DLY); 
    _SDA3_CTRL = 1;    
    Nop();Nop();Nop();    
    _SCL3_CTRL = 1;    
    drvI2C_uDelay(SEN_STR_I2C_DLY); 
}

/*******************************************************************************
**  Function     : drvI2C3_StartBit
**  Description  : sets I2C port#3 the stop to start bit from succeeding calls.
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void drvI2C3_StartBit(void){
    
    Nop();Nop();Nop();
    _SDA3_CFG_OUT;
    Nop();Nop();Nop();

    drvI2C_uDelay(SEN_STR_I2C_DLY); 
    _SDA3_CTRL = 0;
    drvI2C_uDelay(SEN_STR_I2C_DLY); 
    _SCL3_CTRL = 0;
    Nop();Nop();Nop();   
    
}


/*******************************************************************************
**  Function     : drvI2C3_StopBit
**  Description  : sets I2C port#3 the stop bit.
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void drvI2C3_StopBit(void){

    Nop();Nop();Nop();
    _SDA3_CFG_OUT;
    Nop();Nop();Nop();

    drvI2C_uDelay(SEN_STP_I2C_DLY1);
    _SCL3_CTRL = 1;
    drvI2C_uDelay(SEN_STP_I2C_DLY2);     
    _SDA3_CTRL = 1;
    drvI2C_uDelay(SEN_STP_I2C_DLY2); 

}


/*******************************************************************************
**  Function     : drvI2C3_SendByte
**  Description  : write's byte data to I2C port#3
**  PreCondition : must call first the start bit
**  Side Effects : none
**  Input        : par_ubByteData - byte data value to write
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void drvI2C3_SendByte(UINT8 par_ubByteData){

    register UINT8 lubCtr;    
    
    drvI2C_uDelay(SEN_BEGIN_SNDBYTE_I2C_DLY);
        
    lubCtr = 8;     
    // loop 8x as 1 byte is equal to 8 bit..    
    do{  
        // decrement bit data position...
        --lubCtr;

        // get SDA bit data value...
        _SDA3_CTRL = (par_ubByteData & (0x1 << lubCtr)) >> lubCtr; 
                
        // SCL clcok pulse start...
        drvI2C_uDelay(SEN_PW_SDA_DLY);
        _SCL3_CTRL = PORT_HIGH; 
        drvI2C_uDelay(SEN_PW_SCL_DLY);
        _SCL3_CTRL = PORT_LOW; 
        Nop();Nop();Nop();Nop();
        
    }while(lubCtr!=0);    
    
    drvI2C_uDelay(SEN_END_SNDBYTE_I2C_DLY); 
    _SDA3_CTRL = PORT_LOW;  
    
}


/*******************************************************************************
**  Function     : drvI2C3_ReadACK
**  Description  : read's ACK bit from I2C port#3
**  PreCondition : none
**  Side Effects : none
**  Input        : par_ubAckType - ACK type bit
**  Output       : return ACK bit status
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
UINT8 drvI2C3_ReadACK(UINT8 par_ubAckType){

    register UINT8 lubCtr;
    register UINT8 lubDebounce;
    register UINT8 lubACKStatus;    
    
    lubACKStatus = ACK_FAIL;
    lubDebounce = 0;
    
    Nop();Nop();Nop();
    _SDA3_CFG_IN;
    Nop();Nop();Nop();    
    
    for(lubCtr = 0 ; lubCtr < 255 ; lubCtr++){ 

        if(_SDA3_CTRL == par_ubAckType){            
                        
            lubDebounce++;     
            
            if(lubDebounce > SDA_INPUT_ACK_DEBOUNCE_COUNT){                
                
                lubACKStatus = ACK_PASS;
                break;
            }     
        }     
    }
    
    // SCL clcok pulse start...    
    Nop();Nop();Nop();Nop();    
    _SCL3_CTRL = PORT_HIGH;    
    drvI2C_uDelay(SEN_PW_SCL_DLY); 
    _SCL3_CTRL = PORT_LOW;     
    Nop();Nop();Nop();Nop();    

    Nop();Nop();Nop();
    _SDA3_CFG_OUT;
    Nop();Nop();Nop();
    
    _SDA3_CTRL = PORT_LOW;     
    Nop();Nop();Nop();
    
    if(lubACKStatus == ACK_PASS) return(par_ubAckType);

    return(!par_ubAckType);

}

/*******************************************************************************
**  Function     : drvI2C1_SetSDAOut
**  Description  : configures SDA as output for writing byte data..
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void drvI2C3_SetSDAOut(void){

    Nop();Nop();Nop();
    _SDA3_CFG_OUT;
    Nop();Nop();Nop();       

}


/*******************************************************************************
**  Function     : drvI2C3_SetSDAIn
**  Description  : configures SDA as input for reading byte data..
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void drvI2C3_SetSDAIn(void){

    Nop();Nop();Nop();
    _SDA3_CFG_IN;
    Nop();Nop();Nop();
    
}


/*******************************************************************************
**  Function     : drvI2C3_ReadByte
**  Description  : read's byte data from I2C port#3
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : par_ubByte - byte data value
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void drvI2C3_ReadByte(UINT8 *par_ubByte){
    
    register UINT8 lubCtr;   
    register UINT8 lubSDAByteData;    
    register UINT8 lubSDABit;   
        
    drvI2C_uDelay(SEN_BEGIN_SNDBYTE_I2C_DLY); 
    
    _SDA3_CFG_IN;    
    Nop();Nop();Nop();
   
    // reset SDA Byte data value...   
    lubSDAByteData = 0;         

    // set to bit 8th position...
    lubCtr = 8;     
        
    // loop 8x as 1 byte is equal to 8 bit..            
    do{                                               
        // decrement bit data position...
        --lubCtr; 
        
        lubSDABit = _SDA3_CTRL;
        
        // Accumulate the bit shifting data...
        lubSDAByteData |= lubSDABit << lubCtr;      
                
        // SCL clcok pulse start...
        drvI2C_uDelay(SEN_PW_SDA_DLY); 
        _SCL3_CTRL = PORT_HIGH;   
        drvI2C_uDelay(SEN_PW_SCL_DLY);
        _SCL3_CTRL = PORT_LOW;  
        Nop();Nop();Nop();

    }while(lubCtr!=0);
    
    // pass by reference Byte Data...
    *par_ubByte = lubSDAByteData;    
}


/*******************************************************************************
**  Function     : drvI2C3_WriteACK
**  Description  : write's ACK bit
**  PreCondition : none
**  Side Effects : none
**  Input        : par_ubAckType - ACK bit value
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void drvI2C3_WriteACK(UINT8 par_ubAckType){

    Nop();
    _SDA3_CFG_OUT;
    Nop();Nop();Nop();Nop();    
    
    // SCL clcok pulse start...        
    _SDA3_CTRL = par_ubAckType; 
    Nop();Nop();Nop();Nop();    
    _SCL3_CTRL = PORT_HIGH;    
    drvI2C_uDelay(SEN_PW_SCL_DLY); 
    _SCL3_CTRL = PORT_LOW;    
    Nop();Nop();Nop();Nop();    

}

/*   End Of File   */


#endif