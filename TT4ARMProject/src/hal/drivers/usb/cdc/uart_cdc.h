/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm                                            **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc. and Relisource Technology Ltd.                            **
 **                                                                           **
 **  Copryright(c) 2015. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Relisource Technology Ltd.                        **
 **  Creation date       :  04/02/2015	                                      **
 **  Programmer          :  Akkas Uddin Haque	                              **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : uart_cdc.h                                                        **
 **                                                                           **
 **  Description :                                                            **
 **    - This file includes all API header files for the CDC module.          **
 **                                                                           **
 **  History :                                                                ** 
 **    ver 1.00.001  -  04/02/2015	                                          **
 **                                                                           **
 ******************************************************************************/ 

#ifndef UART_CDC_H_
#define UART_CDC_H_

/*! \brief Called by CDC interface
 * Callback running when CDC device have received data
 */
void uart_rx_notify(uint8_t port);

/*! \brief Configures communication line
 *
 * \param cfg      line configuration
 */
void uart_config(uint8_t port, usb_cdc_line_coding_t * cfg);

/*! \brief Opens communication line
 */
//void uart_open(uint8_t port);

/*! \brief Closes communication line
 */
//void uart_close(uint8_t port);

              
#endif /* UART_CDC_H_ */