/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm                                            **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc. and Blue Ocean Innovation Ltd.                            **
 **                                                                           **
 **  Copryright(c) 2010. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Blue Ocean Innovation Ltd.                        **
 **  Project#            :  449                                               **
 **  Creation date       :  08/20/2010                                        **
 **  Programmer          :  Michael J. Mariveles                              **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : drvUSB.h                                                          **
 **                                                                           **
 **  Description :                                                            **
 **    - USB descriptors global call setup.                                   **
 **                                                                           **
 ******************************************************************************/

#ifndef __DRVUSB_H
#define __DRVUSB_H

// Global USB Modes...  \mjmariveles ... 11022010
typedef enum _EUSB_DESC { _eMSD_ = 0, _eCDC_ = 1 } _eUSB_Descriptors;    
void drvUSB_LoadDeviceDescriptors(void);

#endif /* __DRVUSB_H */

/*   End Of File   */

