/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB Variants-MultiAlarm                                            **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc. and Blue Ocean Innovation Ltd.                            **
 **                                                                           **
 **  Copryright(c) 2010. All rights reserved to Sensitech Inc.                **
 **                                                                           **
 **  File : drvSPI.h                                                          **
 **                                                                           **
 **  Description :                                                            **
 **    - SPI communication driver.                                            **
 **                                                                           **
 ******************************************************************************/

#ifndef __DRVSPI_H
#define __DRVSPI_H

#include "drvSPI_Wrapper.h"

void drvSPI_uDelay(UINT16 par_uwData);

void drvSPI2_ByteWrite(UINT8 par_ubByteData);
void drvSPI2_ByteRead(UINT8 *par_ubByteData);

#endif /* __DRVSPI_H */
