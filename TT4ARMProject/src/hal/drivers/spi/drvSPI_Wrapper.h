/*
 * drvSPI_Wrapper.h
 *
 * Created: 20-Feb-15 7:53:47 PM
 *  Author: samahmud
 */ 


#ifndef DRVSPI_WRAPPER_H_
#define DRVSPI_WRAPPER_H_

#include "utility.h"
#include "asf.h"
#include "stdio_serial.h"
#include "conf_board.h"
#include "conf_clock.h"
#include "conf_spi_master.h"
#if (SAMG55)
#include "flexcom.h"
#endif

/// @cond 0
/**INDENT-OFF**/
#ifdef __cplusplus
extern "C" {
#endif
/**INDENT-ON**/
/// @endcond

/* Chip select. */
#define SPI_CHIP_SEL 0
#define SPI_CHIP_PCS spi_get_pcs(SPI_CHIP_SEL)

/* Clock polarity. */
#define SPI_CLK_POLARITY 0

/* Clock phase. */
#define SPI_CLK_PHASE 1

/* Delay before SPCK. */
#define SPI_DLYBS 00//0x40

/* Delay between consecutive transfers. */
#define SPI_DLYBCT 00//0x10


/* SPI clock setting (Hz). */
static uint32_t gs_ul_spi_clock = 500000;

/* SPI clock configuration. */
static const uint32_t gs_ul_clock_configurations[] =
{ 500000, 1000000, 2000000, 5000000 };

#define SPI_READ_WRITE_BUF_SIZE 20


void spi_wrapper_master_initialize(uint8_t configuration);
void spi_master_write_byte(uint8_t data);
void spi_master_read_byte(uint8_t *data);
status_code_t spi_wrapper_flash_instruction(uint8_t *write_data, uint8_t *read_data, size_t len);
void drvSPI2_wrapper_ByteWrite(uint8_t par_ubByteData);
void drvSPI2_wrapper_ByteRead(uint8_t *par_ubByteData);
status_code_t drvSPI2_wrapper_LastByteWrite(uint8_t par_ubByteData);
status_code_t drvSPI2_wrapper_LastByteRead(uint8_t *par_ubByteData);

void drvSPI_uDelay(UINT16 par_uwData);

void drvSPI2_ByteWrite(UINT8 par_ubByteData);
void drvSPI2_ByteRead(UINT8 *par_ubByteData);

#endif /* DRVSPI_WRAPPER_H_ */