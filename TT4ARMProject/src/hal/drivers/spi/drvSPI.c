/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm                                            **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc. and Blue Ocean Innovation Ltd.                            **
 **                                                                           **
 **  Copryright(c) 2010. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Blue Ocean Innovation Ltd.                        **
 **  Project#            :  449                                               **
 **  Creation date       :  08/20/2010                                        **
 **  Programmer          :  Michael J. Mariveles                              **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : drvSPI.c                                                          **
 **                                                                           **
 **  Description :                                                            **
 **    - SPI communication driver.                                            **
 **                                                                           **
 ******************************************************************************/

#include "drvSPI.h"
#include "drvSPI_Wrapper.h"
#include "spi_master.h"


void drvSPI_uDelay(UINT16 par_uwData){
    
    delay_us(par_uwData);
}


/*******************************************************************************
**  Function     : drvSPI2_ByteWrite
**  Description  : writes byte data to the peripheral SPI .
**  PreCondition : must call first the peripheral SPI start bit.  
**  Side Effects : none
**  Input        : par_ubByteData
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  
*******************************************************************************/
void drvSPI2_ByteWrite(UINT8 par_ubByteData){

	drvSPI2_wrapper_ByteWrite(par_ubByteData);
}

/*******************************************************************************
**  Function     : drvSPI2_ByteRead
**  Description  : reads byte data from the peripheral SPI .
**  PreCondition : must call first the peripheral SPI start bit.  
**  Side Effects : none
**  Input        : none
**  Output       : par_ubByteData - byte data value
** _____________________________________________________________________________
**  Date    Author                Changes
**  
*******************************************************************************/
void drvSPI2_ByteRead(UINT8 *par_ubByteData){

	drvSPI2_wrapper_ByteRead(par_ubByteData);
}



/*    End of file    */
