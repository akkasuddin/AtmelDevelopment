/*
 * drvSPI_wrapper.c
 *
 * Created: 20-Feb-15 7:58:30 PM
 *  Author: samahmud
 */ 


#include "drvSPI_Wrapper.h"
#include "spi.h"
#include "spi_master.h"

#define FLASH_CHIP_ID 0

spi_status_t spi_wrapper_write(Spi *p_spi, uint16_t us_data, uint8_t uc_last);

void spi_wrapper_master_initialize(uint8_t configuration)
{
	gs_ul_spi_clock = gs_ul_clock_configurations[configuration];

	flexcom_enable(BOARD_FLEXCOM_SPI);
	flexcom_set_opmode(BOARD_FLEXCOM_SPI, FLEXCOM_SPI);

	spi_reset(SPI_MASTER_BASE);
	spi_set_master_mode(SPI_MASTER_BASE);
	spi_disable_mode_fault_detect(SPI_MASTER_BASE);
	spi_disable_loopback(SPI_MASTER_BASE);
	spi_set_peripheral_chip_select_value(SPI_MASTER_BASE, FLASH_CHIP_ID);
	spi_set_fixed_peripheral_select(SPI_MASTER_BASE);
	spi_disable_peripheral_select_decode(SPI_MASTER_BASE);
	spi_set_delay_between_chip_select(SPI_MASTER_BASE, CONFIG_SPI_MASTER_DELAY_BCS);
	spi_disable(SPI_MASTER_BASE);
	spi_set_clock_polarity(SPI_MASTER_BASE, SPI_CHIP_SEL, SPI_CLK_POLARITY);
	spi_set_clock_phase(SPI_MASTER_BASE, SPI_CHIP_SEL, SPI_CLK_PHASE);
	spi_configure_cs_behavior(SPI_MASTER_BASE, SPI_CHIP_SEL, SPI_CS_KEEP_LOW);
	spi_set_bits_per_transfer(SPI_MASTER_BASE, SPI_CHIP_SEL, SPI_CSR_BITS_8_BIT);
	spi_set_baudrate_div(SPI_MASTER_BASE, SPI_CHIP_SEL, (sysclk_get_cpu_hz() / gs_ul_spi_clock));
	spi_set_transfer_delay(SPI_MASTER_BASE, SPI_CHIP_SEL, SPI_DLYBS, SPI_DLYBCT);
	spi_enable(SPI_MASTER_BASE);
}

void spi_master_write_byte(uint8_t data)
{
	spi_write(SPI_MASTER_BASE, data, 0, 0);
}

void spi_master_read_byte(uint8_t *data)
{
	uint8_t uc_pcs;
	spi_write(SPI_MASTER_BASE, 0, 0, 0);
	spi_read(SPI_MASTER_BASE, data, &uc_pcs);	
}

spi_status_t spi_wrapper_write(Spi *p_spi, uint16_t us_data, uint8_t uc_last)
{
	uint32_t timeout = SPI_TIMEOUT;
	uint32_t value0;

	while (!(p_spi->SPI_SR & SPI_SR_TDRE)) {
		if (!timeout--) {
			return SPI_ERROR_TIMEOUT;
		}
	}

	value0 = SPI_TDR_TD(us_data);
	
	if (uc_last) {
		value0 |= SPI_TDR_LASTXFER;
	}

	p_spi->SPI_TDR = value0;

	return SPI_OK;
}

/**
 * \brief Receive a sequence of bytes from an SPI device.
 *
 * All bytes sent out on SPI bus are sent as value 0.
 *
 * \param p_spi     Base address of the SPI instance.
 * \param data      Data buffer to read.
 * \param len       Length of data to be read.
 *
 * \pre SPI device must be selected with spi_select_device() first.
 */
status_code_t spi_wrapper_flash_instruction(uint8_t *write_data, uint8_t *read_data, size_t len)
{
	uint32_t timeout = SPI_TIMEOUT;
	uint8_t val;
	uint32_t i = 0;
	
	if (len > SPI_READ_WRITE_BUF_SIZE)
	{
		return ERR_ABORTED;
	}

	while (len) {
		timeout = SPI_TIMEOUT;
		while (!spi_is_tx_ready(SPI_MASTER_BASE)) {
			if (!timeout--) {
				return ERR_TIMEOUT;
			}
		}
		spi_write_single(SPI_MASTER_BASE, write_data[i]);

		if(1 == len)
		{
			spi_set_lastxfer(SPI_MASTER_BASE);
		}
		
		timeout = SPI_TIMEOUT;
		while (!spi_is_rx_ready(SPI_MASTER_BASE)) {
			if (!timeout--) {
				return ERR_TIMEOUT;
			}
		}
		spi_read_single(SPI_MASTER_BASE, &val);

		read_data[i] = val;
		i++;
		len--;
	}
	
	while (!spi_is_tx_empty(SPI_MASTER_BASE));

	return STATUS_OK;
}

void drvSPI2_wrapper_ByteWrite(uint8_t par_ubByteData){
	spi_write_packet(SPI_MASTER_BASE, &par_ubByteData, 1);	
}

void drvSPI2_wrapper_ByteRead(uint8_t *par_ubByteData){

	spi_read_packet(SPI_MASTER_BASE, par_ubByteData, 1);
}

status_code_t drvSPI2_wrapper_LastByteWrite(uint8_t par_ubByteData){
	
	uint32_t timeout = SPI_TIMEOUT;

	timeout = SPI_TIMEOUT;
	while (!spi_is_tx_ready(SPI_MASTER_BASE)) {
		if (!timeout--) {
			return ERR_TIMEOUT;
		}
	}
	spi_write_single(SPI_MASTER_BASE, par_ubByteData);
	
	spi_set_lastxfer(SPI_MASTER_BASE);
	
	while (!spi_is_tx_empty(SPI_MASTER_BASE));

	return STATUS_OK;
}

status_code_t  drvSPI2_wrapper_LastByteRead(uint8_t *par_ubByteData){
	
	uint32_t timeout = SPI_TIMEOUT;

	timeout = SPI_TIMEOUT;
	while (!spi_is_tx_ready(SPI_MASTER_BASE)) {
		if (!timeout--) {
			return ERR_TIMEOUT;
		}
	}
	spi_write_single(SPI_MASTER_BASE, 0xFF);

	spi_set_lastxfer(SPI_MASTER_BASE);
	
	timeout = SPI_TIMEOUT;
	while (!spi_is_rx_ready(SPI_MASTER_BASE)) {
		if (!timeout--) {
			return ERR_TIMEOUT;
		}
	}
	spi_read_single(SPI_MASTER_BASE, par_ubByteData);

	return STATUS_OK;
}


void drvSPI_uDelay(UINT16 par_uwData){
	
	delay_us(par_uwData);
}


/*******************************************************************************
**  Function     : drvSPI2_ByteWrite
**  Description  : writes byte data to the peripheral SPI .
**  PreCondition : must call first the peripheral SPI start bit.
**  Side Effects : none
**  Input        : par_ubByteData
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**
*******************************************************************************/
void drvSPI2_ByteWrite(UINT8 par_ubByteData){

	drvSPI2_wrapper_ByteWrite(par_ubByteData);
}

/*******************************************************************************
**  Function     : drvSPI2_ByteRead
**  Description  : reads byte data from the peripheral SPI .
**  PreCondition : must call first the peripheral SPI start bit.
**  Side Effects : none
**  Input        : none
**  Output       : par_ubByteData - byte data value
** _____________________________________________________________________________
**  Date    Author                Changes
**
*******************************************************************************/
void drvSPI2_ByteRead(UINT8 *par_ubByteData){

	drvSPI2_wrapper_ByteRead(par_ubByteData);
}

/*    End of file    */