/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm                                            **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc. and Blue Ocean Innovation Ltd.                            **
 **                                                                           **
 **  Copryright(c) 2010. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Blue Ocean Innovation Ltd.                        **
 **  Project#            :  449                                               **
 **  Creation date       :  08/20/2010                                        **
 **  Programmer          :  Michael J. Mariveles                              **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : drvRTC.c                                                          **
 **                                                                           **
 **  Description :                                                            **
 **    - PIC24F Real Time Clock hw module setup and data/time format          **
 **      manipulator.                                                         **
 **                                                                           **
 ******************************************************************************/
 
#include "hwPIC24F.h"
#include "hwSystem.h"
#include "rtcc.h"
#include "utility.h"
#include "drvRTC.h"
#include "devI2CLCD_BU9796FS.h"



// RAM Memory Current Time global storage...
volatile UINT32 gudwTT4MA_CurrentTime __attribute__((persistent));  //set as persistent for reset recovery
UINT32 gudwTT4MA_PCTime;


/*******************************************************************************
**  Function     : drvRTC_SetupRTCC
**  Description  : initialize RTCC module
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : returns time in seconds
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
UINT32 drvRTC_SetupRTCC(void){

    _UTIL_DATE_TIME_24HR _sDateTime;
    UINT32 ludwCurrentTimeInSecs;
    
    //rtccTimeDate RtccTimeDate;

    //----------------------       
    // initialize RTCC... 
    //----------------------       
    RTCCInit();
    //RtccInitClock();       //turn on clock source     

    RCFGCALbits.RTCOE    = 1;      // enable RTCC port..
    PADCFG1bits.RTSECSEL = 1;   // RTCC output port select :  RTC=1 / ALM=0
    ALCFGRPTbits.ALRMEN  = 0;   // disable alarm... 
    
    //mRtccSetIntPriority(4); 	//set interrupt priority to 4 
    //mRtccSetInt(1);
    //RtccWrOn();            //enable RTCC peripheral
    
    utilDelay_ms(100);    

    //----------------------       
    // read RTCC Time...
    //----------------------           
    RTCCProcessEvents();
    _sDateTime.Sec   = (UINT32)RTCCGetBinSec();
    _sDateTime.Hour  = (UINT32)RTCCGetBinHour();
    _sDateTime.Min   = (UINT32)RTCCGetBinMin();
    _sDateTime.Day   = (UINT32)RTCCGetBinDay();
    _sDateTime.Mon   = (UINT32)RTCCGetBinMonth();
    _sDateTime.Year  = (UINT32)(RTCCGetBinYear() + 2000);

    //_sDateTime.Sec   =  RtccTimeDate.f.hour = 1;
    //_sDateTime.Hour  =  RtccTimeDate.f.min  = 1;
    //_sDateTime.Min   =  RtccTimeDate.f.sec  = 0;
    //_sDateTime.Day   =  RtccTimeDate.f.mday = 4;
    //_sDateTime.Mon   =  RtccTimeDate.f.mon  = 4;    
    //RtccTimeDate.f.year = 9;
    //_sDateTime.Year  = RtccTimeDate.f.year + 2000;    
    
    
    // update current time in seconds is now synchronized with RTCC system time...
    ludwCurrentTimeInSecs = drvRTC_ConvDateTimeToSecs(&_sDateTime);

    // return equivalent number of seconds...
    return(ludwCurrentTimeInSecs);
    
}

/*******************************************************************************
**  Function     : drvRTC_Set_CurrentTimeSecs
**  Description  : set the current time in seconds
**  PreCondition : none
**  Side Effects : none
**  Input        : par_udwTimeInSecs - number in seconds
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/     
void drvRTC_Set_CurrentTimeSecs(UINT32 par_udwTimeInSecs){

    signed char day;    
    signed char hour;
    signed char minute;
    signed char month;
    signed char year;
    signed char seconds;
    _UTIL_DATE_TIME_24HR _sDateTime;
    
    drvRTC_ConvSecsToDateTime(par_udwTimeInSecs,&_sDateTime);       
        
    seconds = (signed char)_sDateTime.Sec;
    hour    = (signed char)_sDateTime.Hour;
    minute  = (signed char)_sDateTime.Min;
    day     = (signed char)_sDateTime.Day;
    month   = (signed char)_sDateTime.Mon;
    year    = (signed char)(_sDateTime.Year - 2000); 

    RTCCOff();
    RTCCSetBinHour( hour );
    RTCCSetBinMin( minute );
    RTCCSetBinSec( seconds );
    RTCCSetBinMonth( month );
    RTCCSetBinYear( year );
    RTCCSetBinDay( day );
    RTCCCalculateWeekDay();
    RTCCSet();  
    
}

/*******************************************************************************
**  Function     : drvRTC_IncCurrentTimeSecs
**  Description  : increment the time in seconds when called
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : returns the time in seconds 
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
UINT32 drvRTC_IncCurrentTimeSecs(void){

    return(++gudwTT4MA_CurrentTime);    
}

/*******************************************************************************
**  Function     : drvRTC_GetCurrentTimeSecs
**  Description  : reads the time in seconds
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : returns the time in seconds
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
UINT32 drvRTC_GetCurrentTimeSecs(void){

    return(gudwTT4MA_CurrentTime);    
}

/*******************************************************************************
**  Function     : drvRTC_2Digit_BCDToBIN
**  Description  : converts BCD number to BIN number
**  PreCondition : none
**  Side Effects : none
**  Input        : par_ubTensDigit - number in tens digit
**                 par_ubOnesDigit - number in ones digit
**  Output       : returns the converted BIN number
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
UINT8 drvRTC_2Digit_BCDToBIN(UINT8 par_ubTensDigit, UINT8 par_ubOnesDigit){    
    
    return((par_ubTensDigit*10)+par_ubOnesDigit);
}

/*******************************************************************************
**  Function     : drvRTC_2Digit_BINToBCD
**  Description  : converts BIN number to BCD number
**  PreCondition : none
**  Side Effects : none
**  Input        : par_ubBinNum - DEC number in BIN 
**  Output       : par_ubBCDDigits - BCD number output
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void drvRTC_2Digit_BINToBCD(UINT8 par_ubBinNum, UINT8 par_ubBCDDigits[2]){    
    
    par_ubBCDDigits[1] = par_ubBinNum / 10; // tens digit..
    par_ubBCDDigits[0] = par_ubBinNum % 10; // ones digit..
}

/*******************************************************************************
**  Function     : drvRTC_ReadRTCC_CurrentTime
**  Description  : reads the RTCC time register
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : returns the current time in seconds
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
UINT32 drvRTC_ReadRTCC_CurrentTime(void){

    _UTIL_DATE_TIME_24HR _sDateTime;
    UINT32 ludwCurrentTimeInSecs;
    
    RTCCProcessEvents();
    _sDateTime.Sec   = (UINT32)RTCCGetBinSec();
    _sDateTime.Hour  = (UINT32)RTCCGetBinHour();
    _sDateTime.Min   = (UINT32)RTCCGetBinMin();
    _sDateTime.Day   = (UINT32)RTCCGetBinDay();
    _sDateTime.Mon   = (UINT32)RTCCGetBinMonth();
    _sDateTime.Year  = (UINT32)(RTCCGetBinYear() + 2000);
    
    ludwCurrentTimeInSecs = drvRTC_ConvDateTimeToSecs(&_sDateTime);

    // return the equivalent total seconds of that date-time... \\mjmariveles   
    return(ludwCurrentTimeInSecs);    
        
}

/*******************************************************************************
**  Function     : drvRTC_ConvertBCDDateTimeToBIN
**  Description  : converts BCD HR,MIN,SEC,DAY,MONTH,YR to BIN format
**  PreCondition : none
**  Side Effects : none
**  Input        : par_sBcdToDecVal - time is BCD format
**  Output       : par_sDateTimeDECVal - time in BIN format
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
UINT32 drvRTC_ConvertBCDDateTimeToBIN(_sRTCC_BCD_VAL par_sBcdToDecVal, _UTIL_DATE_TIME_24HR *par_sDateTimeDECVal){

    par_sDateTimeDECVal->Hour = drvRTC_2Digit_BCDToBIN(par_sBcdToDecVal.hr1,par_sBcdToDecVal.hr0);
    par_sDateTimeDECVal->Min  = drvRTC_2Digit_BCDToBIN(par_sBcdToDecVal.min1,par_sBcdToDecVal.min0);
    par_sDateTimeDECVal->Sec  = drvRTC_2Digit_BCDToBIN(par_sBcdToDecVal.sec1,par_sBcdToDecVal.sec0);
    par_sDateTimeDECVal->Day  = drvRTC_2Digit_BCDToBIN(par_sBcdToDecVal.day1,par_sBcdToDecVal.day0);  
    par_sDateTimeDECVal->Mon  = drvRTC_2Digit_BCDToBIN(par_sBcdToDecVal.mon1,par_sBcdToDecVal.mon0);    
    par_sDateTimeDECVal->Year = drvRTC_2Digit_BCDToBIN(par_sBcdToDecVal.yr1,par_sBcdToDecVal.yr0);
    par_sDateTimeDECVal->Year += 2000;
    
    return(drvRTC_ConvDateTimeToSecs(par_sDateTimeDECVal)); 
}

/*******************************************************************************
**  Function     : drvRTC_ReadRTCC_UpdateCurrentTimeSecs
**  Description  : reads the time RTCC register and updates global time in seconds..
**  PreCondition : none
**  Side Effects : updates global current time in seconds
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void drvRTC_ReadRTCC_UpdateCurrentTimeSecs(void){
 
    _UTIL_DATE_TIME_24HR _sDateTime;    
    
    RTCCProcessEvents();
    _sDateTime.Sec   = (UINT32)RTCCGetBinSec();
    _sDateTime.Hour  = (UINT32)RTCCGetBinHour();
    _sDateTime.Min   = (UINT32)RTCCGetBinMin();
    _sDateTime.Day   = (UINT32)RTCCGetBinDay();
    _sDateTime.Mon   = (UINT32)RTCCGetBinMonth();
    _sDateTime.Year  = (UINT32)(RTCCGetBinYear() + 2000);

    // return the equivalent total seconds of that date-time... \\mjmariveles   
    gudwTT4MA_CurrentTime = drvRTC_ConvDateTimeToSecs(&_sDateTime);    
 
}


/*******************************************************************************
**  Function     : drvRTC_ReadRTCC_CurrentTimeInSecs
**  Description  : reads the time RTCC register ..
**  PreCondition : none
**  Side Effects : updates global current time in seconds
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
/*
UINT32 drvRTC_ReadRTCC_CurrentTimeInSecs(void){

    rtccTimeDate RtccTimeDateVal;
    RtccReadTimeDate(&RtccTimeDateVal);
    
    return(0);
}
*/

#define SECOND_OF_DAY	  86400	
static const UINT8 DayOfMon[12] = {31,28,31,30,31,30,31,31,30,31,30,31};

/*******************************************************************************
**  Function     : drvRTC_ConvSecsToDateTime
**  Description  : converts time in seconds to date format.
**  PreCondition : none
**  Side Effects : none
**  Input        : lSec - seconds in number
**  Output       : tTime - date structure data format output
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void drvRTC_ConvSecsToDateTime(UINT32 lSec, _UTIL_DATE_TIME_24HR *tTime)
{
	UINT32 i,j,Day;
	UINT32 lDay;

    /* change to base on day time */
	lDay = lSec / SECOND_OF_DAY;		 
	lSec = lSec % SECOND_OF_DAY;

	i = 1970;
	while(lDay > 365)
	{
	  if(((i%4==0)&&(i%100!=0)) || (i%400==0))    /* leap year */ 
	    lDay -= 366;
	  else
		lDay -= 365;
	  i++;
	}
	if((lDay == 365) && !(((i%4==0)&&(i%100!=0)) || (i%400==0)))  /* ordinary year */
	{	
	  lDay -= 365;
	  i++;
	}
	tTime->Year = i;     /* get year */
	for(j=0;j<12;j++)     /* get month */
	{
	  if((j==1) && (((i%4==0)&&(i%100!=0)) || (i%400==0)))
		Day = 29;
	  else
		Day = DayOfMon[j];
	  if(lDay >= Day) lDay -= Day;
	  else break;
	}
	tTime->Mon  = j+1;
	tTime->Day  = lDay+1;
	tTime->Hour = lSec / 3600;
	tTime->Min  = (lSec % 3600) / 60;
	tTime->Sec  = (lSec % 3600) % 60;
}

/*******************************************************************************
**  Function     : drvRTC_ConvDateTimeToSecs
**  Description  : convert date time to seconds format..
**  PreCondition : none
**  Side Effects : none
**  Input        : date_time - date structure format
**  Output       : returns time in seconds
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
unsigned long drvRTC_ConvDateTimeToSecs(_UTIL_DATE_TIME_24HR *date_time)
{
	UINT32 Year, Mon, Day, Hour, Min, Sec;
	UINT32 i, Cyear=0;
	UINT32 CountDay=0;
	
	Year = date_time->Year;
	Mon = date_time->Mon;
	Day = date_time->Day;
	Hour = date_time->Hour;
	Min = date_time->Min;
	Sec = date_time->Sec;

    /* Stat. leap year number for 1970 to currently year */
	for(i=1970; i<Year; i++)      
	{               
		if(((i%4==0) && (i%100!=0)) || (i%400==0))  Cyear++;
	}
	CountDay = Cyear * 366 + (Year-1970-Cyear) * 365;
	for(i=1; i<Mon; i++)
	{
		if((i==2) && (((Year%4==0)&&(Year%100!=0)) || (Year%400==0)))
			CountDay += 29;
		else 
			CountDay += DayOfMon[i-1];
	}
	CountDay += (Day-1);

	CountDay = CountDay*SECOND_OF_DAY + (unsigned long)Hour*3600 + (unsigned long)Min*60 + Sec;
    return CountDay;
}


/*    End of File    */ 

