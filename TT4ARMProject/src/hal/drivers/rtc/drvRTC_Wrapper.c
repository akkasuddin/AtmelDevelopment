/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm                                            **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc. and Relisource Technology Ltd.                            **
 **                                                                           **
 **  Copryright(c) 2015. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Relisource Technology Ltd.                        **
 **  Creation date       :  02/23/2015                                        **
 **  Programmer          :  Faijur Rahman		                              **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : drvRTC_Wrapper.c                                                  **
 **                                                                           **
 **  Description :                                                            **
 **    - This file includes required API for legacy code of TT4MA USB related **
 **		 to RTC so that legacy code can call functions from ASF.			  **
 **                                                                           **
 **  History :                                                                ** 
 **    ver 1.00.001  -  02/23/2015                                            **
 **                                                                           **
 ******************************************************************************/


