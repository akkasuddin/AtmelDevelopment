/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm                                            **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc. and Blue Ocean Innovation Ltd.                            **
 **                                                                           **
 **  Copryright(c) 2010. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Blue Ocean Innovation Ltd.                        **
 **  Project#            :  449                                               **
 **  Creation date       :  08/20/2010                                        **
 **  Programmer          :  Michael J. Mariveles                              **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : drvRTC.h                                                          **
 **                                                                           **
 **  Description :                                                            **
 **    - PIC24F Real Time Clock hw module setup and data/time format          **
 **      manipulator.                                                         **
 **                                                                           **
 ******************************************************************************/

#ifndef __DRVRTC_H
#define __DRVRTC_H

typedef struct _date_time24Hr
{	
    UINT32   Year;
    UINT32   Mon;
    UINT32   Day;

    UINT32   Hour;
    UINT32   Min;
    UINT32   Sec;
} _UTIL_DATE_TIME_24HR;


// BCD to decimal values decoder...
typedef struct {
    
    UINT8 hr1;
    UINT8 hr0;
    UINT8 min1;
    UINT8 min0;
    UINT8 sec1;
    UINT8 sec0;
    UINT8 day1;
    UINT8 day0;
    UINT8 mon1;
    UINT8 mon0;
    UINT8 yr1;
    UINT8 yr0;
    UINT8 dayw;
    
} _sRTCC_BCD_VAL;

UINT32 drvRTC_SetupRTCC(void);
void drvRTC_Set_CurrentTimeSecs(UINT32 par_udwTimeInSecs);
UINT8 drvRTC_2Digit_BCDToBIN(UINT8 par_ubTensDigit, UINT8 par_ubOnesDigit);
void drvRTC_2Digit_BINToBCD(UINT8 par_ubBinNum, UINT8 par_ubBCDDigits[2]);
UINT32 drvRTC_ConvertBCDDateTimeToBIN(_sRTCC_BCD_VAL par_sBcdToDecVal, _UTIL_DATE_TIME_24HR *par_sDateTimeDECVal);
UINT32 drvRTC_ReadRTCC_CurrentTime(void);
void drvRTC_ConvSecsToDateTime(UINT32 lSec, _UTIL_DATE_TIME_24HR *tTime);
unsigned long drvRTC_ConvDateTimeToSecs(_UTIL_DATE_TIME_24HR *date_time);
UINT32 drvRTC_IncCurrentTimeSecs(void);
void drvRTC_ReadRTCC_UpdateCurrentTimeSecs(void);
UINT32 drvRTC_GetCurrentTimeSecs(void);
//UINT32 drvRTC_ReadRTCC_CurrentTimeInSecs(void);

#endif /* __DRVRTC_H */

/*  End of File  */

