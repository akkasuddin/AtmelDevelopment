
/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm                                            **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc. and Relisource Technology Ltd.                            **
 **                                                                           **
 **  Copryright(c) 2015. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Relisource Technology Ltd.                        **
 **  Creation date       :  02/2/2015                                        **
 **  Programmer          :  Faijur Rahman		                              **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : board_init.h                                                      **
 **                                                                           **
 **  Description :                                                            **
 **  - This file includes initialization function of TT4 USB Variants Board.  **
 **                                                                           **
 **  History :                                                                ** 
 **    ver 1.00.001  -  02/22/2015                                            **
 **                                                                           **
 ******************************************************************************/

#include <compiler.h>
#include <board.h>
#include <conf_board.h>
#include <ioport.h>
#include "TT4USB_Board.h"


/**
 * \brief Set peripheral mode for IOPORT pins.
 * It will configure port mode and disable pin mode (but enable peripheral).
 * \param port IOPORT port to configure
 * \param masks IOPORT pin masks to configure
 * \param mode Mode masks to configure for the specified pin (\ref ioport_modes)
 */
#define ioport_set_port_peripheral_mode(port, masks, mode) \
	do {\
		ioport_set_port_mode(port, masks, mode);\
		ioport_disable_port(port, masks);\
	} while (0)

/**
 * \brief Set peripheral mode for one single IOPORT pin.
 * It will configure port mode and disable pin mode (but enable peripheral).
 * \param pin IOPORT pin to configure
 * \param mode Mode masks to configure for the specified pin (\ref ioport_modes)
 */
#define ioport_set_pin_peripheral_mode(pin, mode) \
	do {\
		ioport_set_pin_mode(pin, mode);\
		ioport_disable_pin(pin);\
	} while (0)



void TT4USB_Board_init(void)
{

#undef CONF_BOARD_KEEP_WATCHDOG_AT_INIT
#ifndef CONF_BOARD_KEEP_WATCHDOG_AT_INIT
	WDT->WDT_MR = WDT_MR_WDDIS;
#endif
	ioport_init();

	//******************************************************************************
	//	Initialize LEDs
	//******************************************************************************
	/* Initialize LED0, turned off */
	ioport_set_pin_dir(RED_LED_PIN, IOPORT_DIR_OUTPUT);
	ioport_set_pin_level(RED_LED_PIN, IOPORT_PIN_LEVEL_HIGH);
	ioport_set_pin_dir(GREEN_LED_GPIO, IOPORT_DIR_OUTPUT);
	ioport_set_pin_level(GREEN_LED_GPIO, IOPORT_PIN_LEVEL_HIGH);

#ifdef CONF_BOARD_RED_LED
	ioport_set_pin_dir(RED_LED_PIN, IOPORT_DIR_OUTPUT);
	ioport_set_pin_level(RED_LED_PIN, IOPORT_PIN_LEVEL_HIGH);
#endif

#ifdef CONF_BOARD_GREEN_LED	
	ioport_set_pin_dir(GREEN_LED_GPIO, IOPORT_DIR_OUTPUT);
	ioport_set_pin_level(GREEN_LED_GPIO, IOPORT_PIN_LEVEL_HIGH);
#endif

	//******************************************************************************
	//	Initialize Buttons
	//******************************************************************************
	ioport_set_pin_dir(START_BTN_PIN, IOPORT_DIR_INPUT);
	ioport_set_pin_mode(START_BTN_PIN, IOPORT_MODE_PULLUP);
	ioport_set_pin_dir(STOP_BTN_PIN, IOPORT_DIR_INPUT);
	ioport_set_pin_mode(STOP_BTN_PIN, IOPORT_MODE_PULLUP);
	
#if defined(CONF_BOARD_START_BUTTON)

#endif

#if defined(CONF_BOARD_STOP_BUTTON)

#endif

	//******************************************************************************
	//	Initialize TWI of Temperature Sensor
	//******************************************************************************
#if defined(CONF_BOARD_TEMP_TWI)
	ioport_set_pin_peripheral_mode(TEMP_TWI_DATA_GPIO, TEMP_TWI_DATA_FLAGS);
	ioport_set_pin_peripheral_mode(TEMP_TWI_CLK_GPIO, TEMP_TWI_CLK_FLAGS);
#endif

	//******************************************************************************
	//	Initialize TWI of EEPROM
	//******************************************************************************
#if defined(CONF_BOARD_EEPROM_TWI)
	ioport_set_pin_peripheral_mode(EEPROM_TWI_DATA_GPIO, EEPROM_TWI_DATA_FLAGS);
	ioport_set_pin_peripheral_mode(EEPROM_TWI_CLK_GPIO, EEPROM_TWI_CLK_FLAGS);
#endif

	//******************************************************************************
	//	Initialize TWI of LCD
	//******************************************************************************
#if defined(CONF_BOARD_LCD_TWI)
	ioport_set_pin_peripheral_mode(LCD_TWI_DATA_GPIO, LCD_TWI_DATA_FLAGS);
	ioport_set_pin_peripheral_mode(LCD_TWI_CLK_GPIO, LCD_TWI_CLK_FLAGS);
#endif

	//******************************************************************************
	//	Initialize SPI of Flash Chip
	//******************************************************************************

#if defined(CONF_BOARD_FLASH_SPI)
	ioport_set_pin_peripheral_mode(FLASH_SPI_MISO_GPIO, FLASH_SPI_MISO_FLAGS);
	ioport_set_pin_peripheral_mode(FLASH_SPI_MOSI_GPIO, FLASH_SPI_MOSI_FLAGS);
	ioport_set_pin_peripheral_mode(FLASH_SPI_SPCK_GPIO, FLASH_SPI_SPCK_FLAGS);

#ifdef CONF_BOARD_SPI_NPCS0
	ioport_set_pin_peripheral_mode(FLASH_SPI_NPCS0_GPIO, FLASH_SPI_NPCS0_FLAGS);
#endif
#endif

	//******************************************************************************
	//	Initialize  of USART of Gang Programmer 
	//******************************************************************************
#if defined (CONF_BOARD_GANG_UART)
	ioport_set_port_peripheral_mode(GANG_USART_PORT, GANG_USART_PINS, GANG_USART_FLAGS);
#endif

	//******************************************************************************
	//	Initialize of USART of Console
	//******************************************************************************
#if defined (CONF_BOARD_CONSOLE_UART)
	ioport_set_port_peripheral_mode(CONSOLE_USART_PORT, CONSOLE_USART_PINS, CONSOLE_USART_FLAGS);
#endif

	//******************************************************************************
	//	Initialize USB
	//******************************************************************************
#if defined(CONF_BOARD_USB)
	// TODO: configure the USB's +D and -D
	ioport_set_pin_peripheral_mode(PIN_USB_DM , TWI4_DATA_FLAGS);
	//ioport_set_pin_peripheral_mode(PIN_USB_DP , TWI4_CLK_FLAGS); // TODO jdw

	#  if defined(CONF_BOARD_USB_VBUS_DETECT)
		gpio_configure_pin(USB_VBUS_PIN, USB_VBUS_FLAGS);
	#  endif
#endif
	//******************************************************************************
	//	Initialize ADC of Dry Ice
	//******************************************************************************

	
}