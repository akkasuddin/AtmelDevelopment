/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm                                            **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc. and Relisource Technology Ltd.                            **
 **                                                                           **
 **  Copryright(c) 2015. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Relisource Technology Ltd.                        **
 **  Creation date       :  02/20/2015                                        **
 **  Programmer          :  Faijur Rahman		                              **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : TT4USBVariantsBoard.h                                             **
 **                                                                           **
 **  Description :                                                            **
 **  - This file includes required API definitions of TT4 USB Variants Board. **
 **                                                                           **
 **  History :                                                                ** 
 **    ver 1.00.001  -  02/20/2015                                            **
 **                                                                           **
 ******************************************************************************/ 

#ifndef TT4USB_BOARD_H
#define TT4USB_BOARD_H

#include <conf_board.h>
#include <compiler.h>
#include "asf.h"


void TT4USB_Board_init(void);
#define TT4USB_BOARD_NAME		"TT4USB Variants Board"

//******************************************************************************
//	Definitions of Resonator
//******************************************************************************
#define TT4USB_VARIANT_BOARD_FREQ_SLCK_XTAL      BOARD_FREQ_SLCK_XTAL
#define TT4USB_VARIANT_BOARD_FREQ_SLCK_BYPASS    BOARD_FREQ_SLCK_BYPASS
#define TT4USB_VARIANT_BOARD_FREQ_MAINCK_XTAL    BOARD_FREQ_MAINCK_XTAL
#define TT4USB_VARIANT_BOARD_FREQ_MAINCK_BYPASS  BOARD_FREQ_MAINCK_BYPASS
#define TT4USB_VARIANT_BOARD_MCK                 BOARD_MCK 
#define TT4USB_VARIANT_BOARD_OSC_STARTUP_US      BOARD_OSC_STARTUP_US


//******************************************************************************
//	Definitions of LEDs
//******************************************************************************
//	RED LED
#define RED_LED_NAME		"RED LED"
#define RED_LED_GPIO		(PIO_PA23_IDX)
#define RED_LED_PIN			IOPORT_CREATE_PIN(PIOA, 23)
#define RED_LED_ALL_FLAG	{PIO_PA23, PIOA, ID_PIOA, PIO_OUTPUT_1, PIO_DEFAULT}
#define RED_LED_PIN_MASK	PIO_PA23
#define RED_LED_PIN_PIO		PIOA
#define RED_LED_PIN_ID		ID_PIOA
#define RED_LED_PIN_TYPE	PIO_OUTPUT_1
#define RED_LED_PIN_ATTR	PIO_DEFAULT

//	GREEN LED
#define GREEN_LED_NAME		"GREEN LED"
#define GREEN_LED_GPIO		(PIO_PA24_IDX)
#define GREEN_LED_PIN		IOPORT_CREATE_PIN(PIOA, 24)
#define GREEN_LED_ALL_FLAG	{PIO_PA24, PIOA, ID_PIOA, PIO_OUTPUT_1, PIO_DEFAULT}
#define GREEN_LED_PIN_MASK	PIO_PA24
#define GREEN_LED_PIN_PIO	PIOA
#define GREEN_LED_PIN_ID	ID_PIOA
#define GREEN_LED_PIN_TYPE	PIO_OUTPUT_1
#define GREEN_LED_PIN_ATTR	PIO_DEFAULT


//******************************************************************************
//	Definitions of Buttons
//******************************************************************************
//	START BUTTON
#define START_BTN_NAME		"START BUTTON"
#define START_BTN_GPIO		(PIO_PA17_IDX)
#define START_BTN_PIN		IOPORT_CREATE_PIN(PIOA, 17)
#define START_BTN_FLAGS		(PIO_INPUT | PIO_PULLUP | PIO_DEBOUNCE | PIO_IT_RISE_EDGE)
#define START_BTN			{PIO_PA17, PIOA, ID_PIOA, PIO_INPUT, PIO_PULLUP | PIO_DEBOUNCE | PIO_IT_RISE_EDGE}
#define START_BTN_MASK		PIO_PA17
#define START_BTN_PIO		PIOA
#define START_BTN_PIO_ID	ID_PIOA
#define START_BTN_TYPE		PIO_INPUT
#define START_BTN_ATTR		(PIO_PULLUP | PIO_DEBOUNCE | PIO_IT_RISE_EDGE)//pull-up + de-bounce + interrupt on rising edge

// STOP BUTTON
#define STOP_BTN_NAME		"STOP BUTTON"
#define STOP_BTN_GPIO		(PIO_PA18_IDX)
#define STOP_BTN_PIN		IOPORT_CREATE_PIN(PIOA, 18)
#define STOP_BTN_FLAGS		(PIO_INPUT | PIO_PULLUP | PIO_DEBOUNCE | PIO_IT_RISE_EDGE)
#define STOP_BTN			{PIO_PA18, PIOA, ID_PIOA, PIO_INPUT, PIO_PULLUP | PIO_DEBOUNCE | PIO_IT_RISE_EDGE}
#define STOP_BTN_MASK		PIO_PA18
#define STOP_BTN_PIO		PIOA
#define STOP_BTN_PIO_ID 	ID_PIOA
#define STOP_BTN_TYPE		PIO_INPUT
#define STOP_BTN_ATTR		(PIO_PULLUP | PIO_DEBOUNCE | PIO_IT_RISE_EDGE)//pull-up + de-bounce + interrupt on rising edge


//******************************************************************************
//	Definition of TWI0 of Temperature Sensor
//******************************************************************************
#define TEMP_TWI_FLEX_BASE		FLEXCOM0
#define TEMP_TWI_BASE			TWI0
#define TEMP_TWI_IRQn			TWI0_IRQn
#define TEMP_TWI_IRQ_Handler	TWI0_Handler

#define TEMP_TWI_DATA_GPIO		PIO_PA9_IDX
#define TEMP_TWI_DATA_FLAGS		IOPORT_MODE_MUX_A
#define TEMP_TWI_CLK_GPIO		PIO_PA10_IDX
#define TEMP_TWI_CLK_FLAGS		IOPORT_MODE_MUX_A


//******************************************************************************
//	Definition of TWI3 of EEPROM
//******************************************************************************
#define EEPROM_TWI_FLEX_BASE	FLEXCOM3
#define EEPROM_TWI_BASE			TWI3
#define EEPROM_TWI_IRQn			TWI3_IRQn
#define EEPROM_TWI_IRQ_Handler	TWI3_Handler

#define EEPROM_TWI_DATA_GPIO	PIO_PA3_IDX
#define EEPROM_TWI_DATA_FLAGS	IOPORT_MODE_MUX_A
#define EEPROM_TWI_CLK_GPIO		PIO_PA4_IDX
#define EEPROM_TWI_CLK_FLAGS	IOPORT_MODE_MUX_A


//******************************************************************************
//	Definition of TWI4 of LCD
//******************************************************************************
#define LCD_TWI_FLEX_BASE	FLEXCOM4
#define LCD_TWI_BASE		TWI4
#define LCD_TWI_IRQn        TWI4_IRQn
#define LCD_TWI_IRQ_Handler TWI4_Handler

#define LCD_TWI_DATA_GPIO   PIO_PB10_IDX
#define LCD_TWI_DATA_FLAGS  IOPORT_MODE_MUX_A
#define LCD_TWI_CLK_GPIO    PIO_PB11_IDX
#define LCD_TWI_CLK_FLAGS   IOPORT_MODE_MUX_A


//******************************************************************************
//	Definition of SPI5 of Flash Chip
//******************************************************************************
#define FLASH_SPI_MISO_GPIO        (PIO_PA12_IDX)
#define FLASH_SPI_MISO_FLAGS       (IOPORT_MODE_MUX_A)
#define FLASH_SPI_MOSI_GPIO        (PIO_PA13_IDX)
#define FLASH_SPI_MOSI_FLAGS       (IOPORT_MODE_MUX_A)
#define FLASH_SPI_SPCK_GPIO        (PIO_PA14_IDX)
#define FLASH_SPI_SPCK_FLAGS       (IOPORT_MODE_MUX_A)
#define FLASH_SPI_NPCS0_GPIO       (PIO_PA11_IDX)
#define FLASH_SPI_NPCS0_FLAGS      (IOPORT_MODE_MUX_A)
#define FLASH_SPI_NPCS1_GPIO       (PIO_PA5_IDX)
#define FLASH_SPI_NPCS1_FLAGS      (IOPORT_MODE_MUX_B)


//******************************************************************************
//	Definitions of USART6 of Gang Programmer 
//******************************************************************************
#define GANG_USART_BASE		USART6
#define GANG_USART_ID		ID_FLEXCOM6
#define GANG_USART_PINS		(PIO_PB1B_RXD6| PIO_PB0B_TXD6)
#define GANG_USART_FLAGS	(IOPORT_MODE_MUX_B)
#define GANG_USART_PORT		IOPORT_PIOA
#define GANG_USART_MASK		(PIO_PB1B_RXD6 | PIO_PB0B_TXD6)
#define GANG_USART_PIO		PIOA
#define GANG_USART_ID_PIO	ID_PIOA
#define GANG_USART_TYPE		PIO_PERIPH_B
#define GANG_USART_ATTR		PIO_DEFAULT


//******************************************************************************
//	Definitions of USART7 of Console 
//******************************************************************************
#define CONSOLE_USART_BASE		USART7
#define CONSOLE_USART_ID		ID_FLEXCOM7
#define CONSOLE_USART_PINS		(PIO_PA27B_RXD7| PIO_PA28B_TXD7)
#define CONSOLE_USART_FLAGS		(IOPORT_MODE_MUX_B)
#define CONSOLE_USART_PORT		IOPORT_PIOA
#define CONSOLE_USART_MASK		(PIO_PA27B_RXD7 | PIO_PA28B_TXD7)
#define CONSOLE_USART_PIO		PIOA
#define CONSOLE_USART_ID_PIO	ID_PIOA
#define CONSOLE_USART_TYPE		PIO_PERIPH_B
#define CONSOLE_USART_ATTR		PIO_DEFAULT


//******************************************************************************
//	Definitions of USB
//******************************************************************************
#define USB_PIN_VBUS		{PIO_PB4, PIOB, ID_PIOB, PIO_INPUT, PIO_PULLUP}
#define USB_VBUS_FLAGS		(PIO_INPUT | PIO_DEBOUNCE | PIO_IT_EDGE)
#define USB_VBUS_PIN_IRQn	(PIOB_IRQn)
#define USB_VBUS_PIN		(PIO_PB4_IDX)
#define USB_VBUS_PIO_ID		(ID_PIOB)
#define USB_VBUS_PIO_MASK	(PIO_PB4)
#define USB_PIN_DM			{PIO_PA21}
#define USB_PIN_DP			{PIO_PA22}


//******************************************************************************
//	Definitions of ADC of Dry Ice
//******************************************************************************



#endif  /* TT4USB_BOARD_H */
