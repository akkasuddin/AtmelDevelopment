/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm                                            **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc. and Relisource Technology Ltd.                            **
 **                                                                           **
 **  Copryright(c) 2015. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Sensitech Inc.				                      **
 **  Creation date       :  01/13/2015                                        **
 **  Programmer          :  Jon Waisnor			                              **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : pic2sam_types.h                                                   **
 **                                                                           **
 **  Description :                                                            **
 **    - This file includes all API header files for the TT4 module.          **
 **                                                                           **
 **  History :                                                                ** 
 **    ver 1.00.001  -   01/13/2015                                           **
 **                                                                           **
 ******************************************************************************/ 
#ifndef PIC2SAM_TYPES_H_
#define PIC2SAM_TYPES_H_

#include "stdint-gcc.h"
#include "stdbool.h"

#define UINT32	uint32_t
#define INT32	int32_t
#define UINT16	uint16_t
#define INT16	int16_t
#define WORD	uint16_t
#define UINT8	uint8_t
#define INT8	int8_t
#define BYTE	uint8_t
#define BOOL	bool
#define ROM		const

#define TRUE	1
#define FALSE	0

typedef unsigned long DWORD;

extern void Nop();


#endif /* PIC2SAM_TYPES_H_ */