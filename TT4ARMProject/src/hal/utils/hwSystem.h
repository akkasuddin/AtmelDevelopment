
#ifndef __HWSYSTEM_H
#define __HWSYSTEM_H

#include "types.h"

//---------------------------------------
// LED control macros...
//---------------------------------------
#define TT4_RED_LED_INIT        _TRISE2 = 0;Nop();     // RED LED...
#define TT4_GREEN_LED_INIT      _TRISE3 = 0;Nop();     // GREEN LED...

#define TT4_RED_LED             _RE2                   // RED LED...
#define TT4_GREEN_LED           _RE3                   // GREEN LED...

#define TT4_RED_LED_ON          _RE2 = 0;Nop();        // RED LED ON...
#define TT4_GREEN_LED_ON        _RE3 = 0;Nop();        // GREEN LED ON...

#define TT4_RED_LED_OFF         _RE2 = 1;Nop();        // RED LED OFF...
#define TT4_GREEN_LED_OFF       _RE3 = 1;Nop();        // GREEN LED OFF...

//---------------------------------------
// Input port Button control macros...
//---------------------------------------
#define TT4_STOP_BUTTON_INIT    _TRISE1 = 1;Nop();CNPU4bits.CN59PUE = 1;Nop();  // stop button set input mode and pullup enabled...
#define TT4_START_BUTTON_INIT   _TRISE0 = 1;Nop();CNPU4bits.CN58PUE = 1;Nop();  // start button set input mode and pullup enabled......

#define TT4_STOP_BUTTON         _RE1                // stop button input...
#define TT4_START_BUTTON        _RE0                // start button input...
#define TT4_BUTTON_MASK         (PORTE & 0x0003)     // button masked bit value...

//---------------------------------------
// TT4 VBUS Detection port assignement...
//---------------------------------------
#define TT4_USB_VBUS_IO_INIT    _TRISD0 = 1;Nop(); 
#define TT4_USB_VBUS_IO         _RD0               

//---------------------------------------
// USB status bits definition ...
//---------------------------------------
#define _DEVICE_STOP_STATUS_GO_USB_     1
#define _DEVICE_STOP_STATUS_SLEEP_      0
#define USB_PORT_PLUGGED                1
#define USB_PORT_UNPLUGGED              0  

//---------------------------------------
// RTC Input Detection port macro...
//---------------------------------------
#define MI_TOGGLE_PORT_INIT    _TRISD11 = 0;Nop(); 
#define MI_TOGGLE_PORT         _RD11               


//---------------------------------------
// Bit Status...
//---------------------------------------
#define OFF         0
#define ON          1
#define BIT_SET     ON
#define BIT_CLR     OFF


//---------------------------------------
// TT4 Interrupt Priority Assignments...
// Range: 0-7 only
//---------------------------------------
#define INTERRUPT_PRIORITY_LEVEL_0      0 // disabled...
#define INTERRUPT_PRIORITY_LEVEL_1      1
#define INTERRUPT_PRIORITY_LEVEL_2      2
#define INTERRUPT_PRIORITY_LEVEL_3      3
#define INTERRUPT_PRIORITY_LEVEL_4      4
#define INTERRUPT_PRIORITY_LEVEL_5      5
#define INTERRUPT_PRIORITY_LEVEL_6      6
#define INTERRUPT_PRIORITY_LEVEL_7      7

#define CN_ASSIGNED_INTERRUPT_PRIORITY           INTERRUPT_PRIORITY_LEVEL_1 // lowest priority
//#define SPI1_ASSIGNED_INTERRUPT_PRIORITY         INTERRUPT_PRIORITY_LEVEL_2
#define I2C2_ASSIGNED_INTERRUPT_PRIORITY         INTERRUPT_PRIORITY_LEVEL_2
#define SPI2_ASSIGNED_INTERRUPT_PRIORITY         INTERRUPT_PRIORITY_LEVEL_3
#define RTC_ASSIGNED_INTERRUPT_PRIORITY          INTERRUPT_PRIORITY_LEVEL_4
#define TIMER_ASSIGNED_INTERRUPT_PRIORITY        INTERRUPT_PRIORITY_LEVEL_6
#define USB_ASSIGNED_INTERRUPT_PRIORITY          INTERRUPT_PRIORITY_LEVEL_5 // 
#define CPU_ASSIGNED_INTERRUPT_PRIORITY          INTERRUPT_PRIORITY_LEVEL_0 // highest priority


typedef union {
    
    struct  {
        UINT8 N0 : 4;  // half Byte LSB
        UINT8 N1 : 4;  // half Byte MSB
    }Nibble;
    
    UINT8 Byte;
    
}_s8BITBCD;

typedef union {
    
    struct  {
        UINT8 Lo;  // Byte LSB
        UINT8 Hi;  // Byte LSB
    }Byte;
    
    struct  {
        UINT16 N0 : 4;  // Nibble LSB
        UINT16 N1 : 4;
        UINT16 N2 : 4;
        UINT16 N3 : 4;  // Nibble MSB
    }Nibble;

    UINT16 Word;
    
}_s16BITBCD;


typedef union {
    
    struct  {
        UINT16 D0  : 1;  // LSB
        UINT16 D1  : 1;
        UINT16 D2  : 1;
        UINT16 D3  : 1;  
        UINT16 D4  : 1;  
        UINT16 D5  : 1;  
        UINT16 D6  : 1;
        UINT16 D7  : 1;
        UINT16 D8  : 1;  
        UINT16 D9  : 1;  
        UINT16 D10 : 1;  
        UINT16 D11 : 1;
        UINT16 D12 : 1;
        UINT16 D13 : 1;  
        UINT16 D14 : 1;  
        UINT16 D15 : 1;  // MSB
    }Bit;
    
    struct  {        
        UINT16 Lo : 8;  // LSB    
        UINT16 Hi : 8;  // MSB    
    }Byte;

    UINT16 Word;
    
}_sWORDBITDATA;

typedef union {
    
    struct  {
        UINT8 D0  : 1;  // LSB
        UINT8 D1  : 1;
        UINT8 D2  : 1;
        UINT8 D3  : 1;  
        UINT8 D4  : 1;  
        UINT8 D5  : 1;  
        UINT8 D6  : 1;
        UINT8 D7  : 1; // MSB    
    }Bit;
    
    struct  {        
        UINT8 Lo : 4;  // LSB    
        UINT8 Hi : 4;  // MSB    
    }Nibble;

    UINT8 Byte;
    
}_sBYTEBITDATA;


typedef union {
    

    struct {        
        UINT8 B0;  // LSB    
        UINT8 B1;
        UINT8 B2;
        UINT8 B3;  // MSB    
    }Byte;
    
    struct {        
        UINT16 Lo;  // LSB    
        UINT16 Hi;  // MSB    
    }Word;

    UINT32 DwData;
    
}_sDWORDBITDATA;


void hwInit_CPUCtrlRegister(void);
void hwInit_OSCCtrlRegister(void);
void hwInit_RESETCtrlRegister(void);
void hwInit_REFOSCCtrlRegister(void); 
void hwInit_IOPorts(void);
void hwInit_PeripheralPINSel(void);
void hwInit_TimerCtrlRegister(void);
void hwInit_CPUSleepReset(void);
void hwInit_CPUCtrlRegister_USBMode(void);
void hwInit_USBControlRegisters(void);

#endif /* __HWSYSTEM_H */
