/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm                                            **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc. and Relisource Technology Ltd.                            **
 **                                                                           **
 **  Copryright(c) 2015. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Relisource Technology Ltd.                        **
 **  Creation date       :  02/27/2015                                        **
 **  Programmer          :  Faijur Rahman		                              **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : types.h                                                           **
 **                                                                           **
 **  Description :                                                            **
 **    - This file includes all required header files for data types.         **
 **                                                                           **
 **  History :                                                                ** 
 **    ver 1.00.001  -   02/27/2015                                           **
 **                                                                           **
 ******************************************************************************/ 
#ifndef TYPES_H_
#define TYPES_H_

#include "stdint-gcc.h"
#include "stdbool.h"
#include "pic2sam_types.h"

#endif /* TYPES_H_ */