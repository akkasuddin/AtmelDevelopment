/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm                                            **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc. and Relisource Technology Ltd.                            **
 **                                                                           **
 **  Copryright(c) 2015. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Sensitech Inc.				                      **
 **  Creation date       :  01/13/2015                                        **
 **  Programmer          :  Jon Waisnor			                              **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : pic2sam_types.c                                                   **
 **                                                                           **
 **  Description :                                                            **
 **    - This file includes all API header files for the TT4 module.          **
 **                                                                           **
 **  History :                                                                ** 
 **    ver 1.00.001  -   01/13/2015                                           **
 **                                                                           **
 ******************************************************************************/ 

#include "pic2sam_types.h"

void Nop(void)
{
	
}

