/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm                                            **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc. and Blue Ocean Innovation Ltd.                            **
 **                                                                           **
 **  Copryright(c) 2010. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Blue Ocean Innovation Ltd.                        **
 **  Project#            :  449                                               **
 **  Creation date       :  08/20/2010                                        **
 **  Programmer          :  Michael J. Mariveles                              **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : utility.h                                                         **
 **                                                                           **
 **  Description :                                                            **
 **    - utility routines.                                                    **
 **                                                                           **
 ******************************************************************************/
 
#ifndef __UTILITY_H
#define __UTILITY_H

// ******************************************************************************
#include "asf.h"
#include "types.h"
#define __eds__ const

#define TT4_RED_LED_ON nop();
#define TT4_GREEN_LED_ON nop();
#define TT4_RED_LED_OFF nop();
#define TT4_GREEN_LED_OFF nop();

// ******************************************************************************

// system clock millisec delay counts... mjm
#define UTIL_MDELAY_SYSCLK_32MHZ     2300u // cycle per 1ms...
#define UTIL_MDELAY_SYSCLK_16MHZ     1150u // cycle per 1ms...
#define UTIL_MDELAY_SYSCLK_8MHZ      575u  // cycle per 1ms...
#define UTIL_MDELAY_SYSCLK_4MHZ      287u  // cycle per 1ms...

// system clock selector register for control of accurate delay in specified clock speed...
extern volatile UINT16 guwUtilSysClk_mDly;

void utilDelay_ms(UINT16 par_uwMilliSec);
void utilArrayVarInitializer(UINT8 *par_vVar, UINT16 par_uwSize, UINT8 par_uwResetValue);
void util_EDS_ArrayVarInitializer(__eds__ UINT8 *par_vVar, UINT16 par_uwSize, UINT8 par_uwResetValue);
void utilExtRAM_QuickString_Save(const char *par_ubString, UINT16 *par_uwStartAddr);
void utilExtRAM_QuickString_Retrieve(char *par_ubString, UINT16 par_uwNumOfBytes, UINT16 *par_uwStartAddr);
void utilConcatByteData(const void *par_vDataPtrIn, UINT8 par_lubDataSize, UINT8 *par_lubPrevSize, UINT8 *par_ubDataBytesOut);
void utilULongNumToString(UINT32 par_udwNumber, UINT8 *par_ubStr);
void utilByteToWordString(UINT16 *par_uwStr, UINT8 *par_ubStr, UINT8 par_ubLength);
void utilReadEEPROM_DataToString(UINT16 par_uwOffset, UINT16 par_uwNumOfChar , char *par_String);
void utilWriteEEPROM_DataToString(UINT16 par_uwOffset, UINT16 par_uwNumOfChar , const char *par_String);
void utilWriteEEPROM_ByteValue(UINT16 par_uwOffset, UINT16 par_uwNumOfChar , const void* par_vPtr);
void utilReadEEPROM_ByteValue(UINT16 par_uwOffset, UINT16 par_uwNumOfChar , void* par_vPtr);
void util_SaveLongFileName(char * par_dest_StrFname, const char * par_src_StrFname);
void util_InitLongFileName(char * par_dest_StrFname);
float utilDbDec_RoundOFF(float par_fReal);

#endif /* __UTILITY_H */
