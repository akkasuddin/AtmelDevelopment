/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm                                            **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc. and Blue Ocean Innovation Ltd.                            **
 **                                                                           **
 **  Copryright(c) 2010. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Blue Ocean Innovation Ltd.                        **
 **  Project#            :  449                                               **
 **  Creation date       :  08/20/2010                                        **
 **  Programmer          :  Michael J. Mariveles                              **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : utility.c                                                         **
 **                                                                           **
 **  Description :                                                            **
 **    - utility routines.                                                    **
 **                                                                           **
 ******************************************************************************/

//#include "hwPIC24F.h"
//#include "hwSystem.h"
//#include "24AA256.h"
#include "utility.h"

/*******************************************************************************
**  Function     : utilDelay_ms
**  Description  : milliseconds delay 
**  PreCondition : none
**  Side Effects : none
**  Input        : par_uwMilliSec - number in seconds value
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/

// system clock selector register for control of accurate delay in specified clock speed...
volatile UINT16 guwUtilSysClk_mDly;

void utilDelay_ms(UINT16 par_uwMilliSec){

    register UINT16 luwMicroSec;
    
    do{          
    	// 4000 loop counts is 1 milli secs at 32Mhz System clock...
    	luwMicroSec = guwUtilSysClk_mDly;
        while(--luwMicroSec);                 
        
    }while(--par_uwMilliSec);     	 
}

/*******************************************************************************
**  Function     : utilDelay_ns
**  Description  : nanoseconds delay 
**  PreCondition : none
**  Side Effects : none
**  Input        : par_uwNanoSec - numnber in nano seconds value
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void utilDelay_ns(UINT16 par_uwNanoSec){
    
    while(par_uwNanoSec) par_uwNanoSec--; 

}

/*******************************************************************************
**  Function     : utilArrayVarInitializer
**  Description  : stores value in a Array data
**  PreCondition : none
**  Side Effects : none
**  Input        : par_uwSize - 
**                 par_uwResetValue - 
**  Output       : par_vVar - 
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void utilArrayVarInitializer(UINT8 *par_vVar, UINT16 par_uwSize, UINT8 par_uwResetValue){
    
    UINT16 lubCtr;
    
    // initilaize array index counter
    lubCtr = 0;
    do{    
        
        // load pointer data...
        *par_vVar = par_uwResetValue;
        // increment pointer address...
        ++par_vVar;        
        
        // increment array index counter..
        ++lubCtr;
        
    }while(lubCtr<par_uwSize); 
    
}

/*******************************************************************************
**  Function     : util_InitLongFileName
**  Description  : clear array data used for long filename 
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void util_InitLongFileName(char * par_dest_StrFname){
    
    register UINT16 luwCtr;
    
    // initilaize array index counter
    luwCtr = 0;
    do{            
        // load pointer data...
        par_dest_StrFname[luwCtr] = 0;
        
        // increment index...
        ++luwCtr;                
    }while(luwCtr<60); 
    
    par_dest_StrFname[0] = 'X';
    par_dest_StrFname[1] = 'X';    
    par_dest_StrFname[2] = 'X';    
}

/*******************************************************************************
**  Function     : util_SaveLongFileName
**  Description  : copies filename to a string
**  PreCondition : none
**  Side Effects : none
**  Input        : par_src_StrFname - source filename
**  Output       : par_dest_StrFname - destination filename
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void util_SaveLongFileName(char * par_dest_StrFname, const char * par_src_StrFname){

    register UINT16 luwCtr;
    
    // initilaize array index counter
    luwCtr = 0;
    do{            
        // load pointer data...
        par_dest_StrFname[luwCtr] = par_src_StrFname[luwCtr];
        
        if(par_src_StrFname[luwCtr]==0) return;
        
        // increment index...
        ++luwCtr;                
    }while(luwCtr<64); 
    
    // truncate file name if morethan 64 bytes...
    par_dest_StrFname[luwCtr] = 0; // null terminator...

}

/*******************************************************************************
**  Function     : util_EDS_ArrayVarInitializer
**  Description  : initializes EDS memory array
**  PreCondition : none
**  Side Effects : none
**  Input        : par_uwSize - memory size number
**                 par_uwResetValue - value to be written 
**  Output       : par_vVar - array output values
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void util_EDS_ArrayVarInitializer(__eds__ UINT8 *par_vVar, UINT16 par_uwSize, UINT8 par_uwResetValue){
    
    UINT16 luwCtr;
    
    // initilaize array index counter
    luwCtr = 0;
    do{    
        
        // load pointer data...
        //par_vVar[luwCtr] = par_uwResetValue;
        // increment index...
        ++luwCtr;        
        
    }while(luwCtr<par_uwSize); 
    
}

/*******************************************************************************
**  Function     : utilConcatByteData
**  Description  : concatenates array of byte data
**  PreCondition : none
**  Side Effects : none
**  Input        : par_vDataPtrIn - array of data bytes
**                 par_lubDataSize - size of byte to be concatenated 
**
**  Output       : par_ubDataBytesOut - arrary of concateneated data bytes
**                 par_lubPrevSize - increments the previous size as marker
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void utilConcatByteData(const void *par_vDataPtrIn, UINT8 par_lubDataSize, UINT8 *par_lubPrevSize, UINT8 *par_ubDataBytesOut){
    
    UINT8 *lubDataPtr;
    UINT8 lubCtr; 
    
    lubDataPtr = (UINT8 *)par_vDataPtrIn;
    
    lubCtr = 0;
    do{
        par_ubDataBytesOut[lubCtr] = *lubDataPtr;
        ++lubDataPtr;
        ++lubCtr;
    }while(lubCtr<par_lubDataSize);
    
    *par_lubPrevSize += lubCtr;
}

/*******************************************************************************
**  Function     : utilByteToWordString
**  Description  : copies the byte data array to a word data array.
**  PreCondition : none
**  Side Effects : none
**  Input        : par_ubStr - byte string to be converted
**                 par_ubLength - size of bytes to be copied.                    
**  Output       : par_uwStr - word string to be copied
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void utilByteToWordString(UINT16 *par_uwStr, UINT8 *par_ubStr, UINT8 par_ubLength){
    
    register UINT8 lubCtr = 0;
    
    do{
        par_uwStr[lubCtr] = (UINT16)par_ubStr[lubCtr];
    
    }while(++lubCtr < par_ubLength);
    
    // null string terminator...
    par_uwStr[lubCtr] = 0;
}

/*******************************************************************************
**  Function     : utilULongNumToString
**  Description  : converts UnsignedLong to String value
**  PreCondition : none
**  Side Effects : none
**  Input        : par_udwNumber - unsigned long number
**  Output       : par_ubStr - converted to string value
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void utilULongNumToString(UINT32 par_udwNumber, UINT8 *par_ubStr){

    const char lcNumChars[10] = {'0','1','2','3','4','5','6','7','8','9'};
    register UINT32 ludwNumber;  
	char lcTemp[10], lcZeros[10];
	register UINT8 lubCtr,lubNoZeroCtr;
	register UINT8 lubReversed;
	register UINT8 lubDigit;
	BOOL lbZeroDetectFlag; 
	
	// if zero return immediately...
	if(par_udwNumber<=0){
		par_ubStr[0] = '0';
		par_ubStr[1] = 0;
		return;
	}
    
    // convert number to numeric characters...
    lubCtr = 0;    
    ludwNumber = par_udwNumber;    
    while(lubCtr<10){                
		lubDigit = (UINT8)(ludwNumber % 10);
		lcTemp[lubCtr] = lcNumChars[lubDigit];
		++lubCtr;
		ludwNumber /= 10;
    }    
	lcTemp[lubCtr] = 0; // null string terminator... 
	
	// reversing numeric characters...
	lubReversed=0;
	while(lcTemp[lubReversed]!=0){
		lcZeros[--lubCtr] = lcTemp[lubReversed];		
		++lubReversed;		
	}	
    // set the last character to null string terminator...
    lcZeros[lubReversed] = 0;     
    
    // load to string parameter ...
    if(lcZeros[0] != '0'){
        // MSB is not 0 then the length must be 10 chars absolutely!... mjm
        lubNoZeroCtr = 0;
        while(lubNoZeroCtr<10){
             par_ubStr[lubNoZeroCtr] = lcZeros[lubNoZeroCtr];
             ++lubNoZeroCtr;
        }     
    }
    else{            
        // remove zero characters at MSB...
        lubNoZeroCtr = lubCtr = 0;
        lbZeroDetectFlag = FALSE; 
        while(lubCtr<10){ 
            if((lcZeros[lubCtr]!='0')||(lbZeroDetectFlag==TRUE)){
                // if have reached the 1st non-zero number then enable this flag...
                lbZeroDetectFlag=TRUE; 
                par_ubStr[lubNoZeroCtr] = lcZeros[lubCtr];
                ++lubNoZeroCtr;
            }    
            ++lubCtr;
        }
    }
    
    // set the null string terminator...
    par_ubStr[lubNoZeroCtr] = 0;
    
}

/*******************************************************************************
**  Function     : utilReadEEPROM_DataToString
**  Description  : reads EEPROM byte data then converts to string data
**  PreCondition : none
**  Side Effects : none
**  Input        : par_uwOffset - EEPROM offset value
**                 par_uwNumOfChar - number characters
**  Output       : par_String - string value
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void utilReadEEPROM_DataToString(UINT16 par_uwOffset, UINT16 par_uwNumOfChar , char *par_String){
    
    register UINT16 luwCtr;    
    UINT8 lubData;
    
    luwCtr = 0;
    do{
        
        //devEEPROM_RandomRead(par_uwOffset+luwCtr,&lubData); 
        
        if(lubData==0) break;
        
        par_String[luwCtr] = lubData;                
        
        ++luwCtr;
        
    }while(luwCtr<par_uwNumOfChar);
    
    par_String[luwCtr] = 0;// Null terminator...    
}

/*******************************************************************************
**  Function     : utilWriteEEPROM_DataToString
**  Description  : writes byte data to EEPROM from a string format
**  PreCondition : none
**  Side Effects : none
**  Input        : par_uwOffset - EEPROM offset to be written
**                 par_uwNumOfChar - number of character to be written 
**                 par_String - the string data to be writen
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void utilWriteEEPROM_DataToString(UINT16 par_uwOffset, UINT16 par_uwNumOfChar , const char *par_String){
    
    register UINT16 luwCtr;    
    register UINT8 lubData;
    register UINT8 lubDetectLock = 1;
    
    luwCtr = 0;
    do{ 
        
        if(lubDetectLock==1){
            if(par_String[luwCtr]==0){
                lubData = 0; // null
                lubDetectLock=0;
            }    
            else
                lubData = par_String[luwCtr];
        }    
        else lubData = 0; // null
        
        //devEEPROM_ByteWrite(par_uwOffset+luwCtr,lubData); 
        
        ++luwCtr;
        
        utilDelay_ms(7);
        
    }while(luwCtr<par_uwNumOfChar);
    
}

/*******************************************************************************
**  Function     : utilReadEEPROM_ByteValue
**  Description  : reads EEPROM byte data
**  PreCondition : none
**  Side Effects : none
**  Input        : par_uwOffset - EEPROM starting offset to read
**                 par_uwNumOfChar - number of characters to read
**  Output       : par_vPtr - array of byte data from EEPROM
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void utilReadEEPROM_ByteValue(UINT16 par_uwOffset, UINT16 par_uwNumOfChar , void* par_vPtr){
    
    register UINT16 luwCtr;    
    UINT8 lubData;
    UINT8 *lptr;
    
    lptr = (UINT8*)par_vPtr;
    
    luwCtr = 0;
    do{
        
        //devEEPROM_RandomRead(par_uwOffset+luwCtr,&lubData); 
        
        //lptr[luwCtr] = lubData;                
        *lptr = lubData;
        
        ++lptr;
        
        ++luwCtr;
        
    }while(luwCtr<par_uwNumOfChar);
    
}

/*******************************************************************************
**  Function     : utilWriteEEPROM_ByteValue
**  Description  : writes array byte data to EEPROM
**  PreCondition : none
**  Side Effects : none
**  Input        : par_uwOffset - EEROM offset
**                 par_uwNumOfChar - number of character to write
**                 par_vPtr - array data to be written
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void utilWriteEEPROM_ByteValue(UINT16 par_uwOffset, UINT16 par_uwNumOfChar , const void* par_vPtr){
    
    register UINT16 luwCtr;    
    register UINT8 lubData;
    UINT8 *lptr;
    
    lptr = (UINT8*)par_vPtr;
    
    luwCtr = 0;
    do{ 
        
        lubData = lptr[luwCtr];
        
        //devEEPROM_ByteWrite(par_uwOffset+luwCtr,lubData); 
        
        ++luwCtr;
        
        utilDelay_ms(7);
        
    }while(luwCtr<par_uwNumOfChar);
    
}


/*******************************************************************************
**  Function     : utilDbDec_RoundOFF
**  Description  : a real number to be round off into 2 decimal places..
**  PreCondition : none
**  Side Effects : none
**  Input        : par_fReal - real number input..
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  090213  Michael J. Mariveles  1st release.    
*******************************************************************************/
float utilDbDec_RoundOFF(float par_fReal){
    
    float lfReal = par_fReal;    
    unsigned luReal;
    float lfNumber,lfDecimalValue;
    
    // take two decimal...
    lfReal *= 100.0;
    
    // clear the decimal...
    luReal = lfReal;    
    lfNumber = luReal;    
    
    // extract the decimal...
    lfDecimalValue = lfReal - lfNumber;
    
    //evaluate the decimal if morethan 0.5...
    if(lfDecimalValue >= 0.5) lfReal += 1.0;  
    
    return(lfReal/100.0);    
}
/*  End Of File  */





