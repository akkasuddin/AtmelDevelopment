/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm                                            **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc. and Relisource Technology Ltd.                            **
 **                                                                           **
 **  Copryright(c) 2015. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Relisource Technology Ltd.                        **
 **  Creation date       :  02/20/2015                                        **
 **  Programmer          :  Faijur Rahman		                              **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : hal.h                                                             **
 **                                                                           **
 **  Description :                                                            **
 **    - This file includes all API header files for the hal module.          **
 **                                                                           **
 **  History :                                                                ** 
 **    ver 1.00.001  -  02/20/2015                                            **
 **                                                                           **
 ******************************************************************************/ 

#ifndef HAL_H
#define HAL_H

/*
 * This file includes all API header files for the HAL.
 */
#include "hwSystem.h"
#include "types.h"
#include "utility.h"

#include "TT4USB_Board.h"
#include "devGreenLED.h"
#include "devRedLED.h"
#include "devButton.h"
#include "devI2CLCD_BU9796FS.h"
#include "devI2CEEPROM_24AA256.h"
#include "devSPIFLASH_W25Q80.h"
#include "devI2CTempSensor_TMP112.h"

#endif // HAL_H
