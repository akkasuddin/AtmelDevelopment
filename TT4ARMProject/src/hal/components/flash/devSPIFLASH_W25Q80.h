/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm                                            **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc. and Blue Ocean Innovation Ltd.                            **
 **                                                                           **
 **  Copryright(c) 2010. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : devSPIFLASH_W25Q80.h                                              **
 **                                                                           **
 **  Description :                                                            **
 **    - external 1MB SPI Flash device driver.                                **
 **                                                                           **
 ******************************************************************************/

#ifndef __DEVSPIFLASH_W25Q80_H
#define __DEVSPIFLASH_W25Q80_H

#include <string.h>

#define MEDIA_SECTOR_SIZE         512u
#define SPIFLASH_BLOCK_SIZE         4096u
#define SPIFLASH_PAGE_SIZE          256u
#define SPIFLASH_PAGES_PER_BLOCK    16u
#define SPIFLASH_FAT_SECTOR_SIZE    512u
#define SPIFLASH_TOTAL_FAT_SECTORS  2001u
#define SPIFLASH_FAT_DISK_SIZE      SPIFLASH_TOTAL_FAT_SECTORS  * SPIFLASH_FAT_SECTOR_SIZE


#define FLASH_ERASE_BLOCK_SIZE  4096 // winbond W25Q80BV spi flash minimum sector erase size is 4096...
#define FLASH_PAGE_BUFFER_SIZE  256  // write buffer size is 256..
#define FLASH_MAX_BLOCK_PAGES   16  // write buffer size is 256..


#define SPIFLASH_STATUS_BUSY  0x1

//------------------------------------------------------------
// Instruction set table#1 : Erase,Program Instructions
//------------------------------------------------------------
#define SPIFLASH_WRITE_ENABLE             0x06
#define SPIFLASH_WRITE_ENA_VOL_STAT_REGS  0x50
#define SPIFLASH_WRITE_DISABLE            0x04
#define SPIFLASH_READ_STATUS_REGISTER_1   0x05
#define SPIFLASH_READ_STATUS_REGISTER_2   0x35
#define SPIFLASH_WRITE_STATUS_REGISTER    0x01
#define SPIFLASH_PAGE_PROGRAM             0x02
#define SPIFLASH_QUAD_PAGE_PROGRAM        0x32
#define SPIFLASH_SECTOR_ERASE_4KB         0x20
#define SPIFLASH_BLOCK_ERASE_32KB         0x52
#define SPIFLASH_BLOCK_ERASE_64KB         0xD8
#define SPIFLASH_CHIP_ERASE               0xC7 // 0X60
#define SPIFLASH_ERASE_PROG_SUSPEND       0x75
#define SPIFLASH_ERASE_PROG_RESUME        0x7A
#define SPIFLASH_POWER_DOWN               0xB9
#define SPIFLASH_CONT_READ_MODE_RESET     0xFF

//------------------------------------------------------------
// Instruction set table#2 : READ Instructions
//------------------------------------------------------------
#define SPIFLASH_READ_DATA              0x03
#define SPIFLASH_FAST_READ              0x0B
#define SPIFLASH_FAST_READ_DUAL_O       0x3B
#define SPIFLASH_FAST_READ_QUAD_O       0x6B
#define SPIFLASH_FAST_READ_DUAL_IO      0xBB
#define SPIFLASH_FAST_READ_QUAD_IO      0xEB
#define SPIFLASH_WORD_READ_QUAD_IO      0xE7
#define SPIFLASH_OCTAL_WORD_READ_IO     0xE3
#define SPIFLASH_SET_BURST_WRAP         0x77

//------------------------------------------------------------
// Instruction set table#3 : ID,Security Instructions
//------------------------------------------------------------
#define SPIFLASH_RELPWRDWN_DEVID        0xAB
#define SPIFLASH_MANFACTUR_DEVID        0x90
#define SPIFLASH_MANF_DULAIO_DEVID      0x92
#define SPIFLASH_MANF_QUADAIO_DEVID     0x94
#define SPIFLASH_JEDEC_ID               0x9F
#define SPIFLASH_READ_UNIQUE_ID         0x4B
#define SPIFLASH_ERASE_SECUR_REGS       0x44
#define SPIFLASH_PRG_SECUR_REGS         0x42
#define SPIFLASH_READ_SECUR_REGS        0x48



typedef union  {

    struct  {
        UINT8 B0; // LSB...
        UINT8 B1;
        UINT8 B2;
        UINT8 B3; // MSB...       
    }Byte;
    
    UINT32 udwAddress;
    
}sSPIFLASH_ADDRESS;


typedef union  {

    struct  {
        UINT8 BUSY  : 1;    // LSB
        UINT8 WEL   : 1;
        UINT8 BP    : 3;
        UINT8 TB    : 1;
        UINT8 SEC   : 1;
        UINT8 SRP0  : 1;    // MSB
    }Bit;
    
    UINT8 Byte;
    
}STAT_REG_RSR1;



typedef union  {

    struct  {
        UINT8 SRP1  : 1;
        UINT8 QE    : 1;
        UINT8 LB    : 4;
        UINT8 CMP   : 1;
        UINT8 SUS   : 1;
    }Bit;
    
    UINT8 Byte;
    
}STAT_REG_RSR2;

typedef struct{        
    UINT32 udwOffset;
    UINT32 udwNumber;
    UINT32 udwStartAddr;
    UINT32 udwSectorAddr;
} SPIFLASH_BLOCK;


void devFLASH_Init(void);
UINT8 devFLASH_ReadStatusRegister1(void);
UINT8 devFLASH_ReadStatusRegister2(void);
UINT8 devFLASH_CheckWriteEnableBit(void);
UINT8 devFLASH_CheckBusyBit(void);
void devFLASH_WriteEnable(void);
void devFLASH_ClearChip(void);
void devFLASH_PowerDown(void);
void devFLASH_PowerUp(void);
void devFLASH_Write4096Bytes(UINT32 par_udwBlockNumber);
void devFLASH_Read4096Bytes(UINT32 par_udwBlockEraseAddr);
void devFLASH_CreateMBR(void);
void devFLASH_BootSectorFill_Block0(void);
void devFLASH_BootSectorFill_Block1(void);
void devFLASH_BootSectorFill_Block2(void);
void devFLASH_Buffer_ReadBlock(UINT32 par_udwBlockNum);
void devFLASH_Buffer_WriteBlock(UINT32 par_udwBlockNum);
void devFLASH_Buffer_EraseBlock(UINT32 par_udwBlockNum);
void devFLASH_Sector_ReadBlock(UINT32 par_udwSectorNum, UINT8 *par_ubBuffer);
void devFLASH_GetBlockStatus(UINT32 par_udwSectorNum);
UINT32 devFLASH_GetSectorStatus(UINT32 par_udwBlockNum);

#endif /* __DEVSPIFLASH_W25Q80_H */
