/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB Variants-MultiAlarm                                   **
 **                                                                           **
 **                                                                           **
 **  Copryright(c) 2010. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : devSPIFLASH_W25Q80.c                                              **
 **                                                                           **
 **  Description :                                                            **
 **    - external 1MB SPI Flash device driver.                                **
 **                                                                           **
 ******************************************************************************/

#include "drvSPI_Wrapper.h"
#include "devSPIFLASH_W25Q80.h"

STAT_REG_RSR1 gSTAT_REG_RSR1;
STAT_REG_RSR2 gSTAT_REG_RSR2;

uint8_t spi_write_buff[SPI_READ_WRITE_BUF_SIZE];
uint8_t spi_read_buff[SPI_READ_WRITE_BUF_SIZE];

extern volatile UINT8 gubFlashBlockSizeBuffer[FLASH_ERASE_BLOCK_SIZE + 1] __attribute__((far));

volatile SPIFLASH_BLOCK gsSpiFlashBlock;  // mjm 

/*******************************************************************************
**  Function     : devFLASH_Init
**  Description  : initialize external 1MB flash memory..
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  
*******************************************************************************/        
void devFLASH_Init(void){
	
	spi_wrapper_master_initialize(1);
	
    gSTAT_REG_RSR1.Byte = 0;
    gSTAT_REG_RSR2.Byte = 0;
}

/*******************************************************************************
**  Function     : devFLASH_ReadStatusRegister1
**  Description  : read's the low byte status register
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : return's low byte status register byte
** _____________________________________________________________________________
**  Date    Author                Changes
**  
*******************************************************************************/
UINT8 devFLASH_ReadStatusRegister1(void){

    gSTAT_REG_RSR1.Byte = 0;

	spi_write_buff[0] = SPIFLASH_READ_STATUS_REGISTER_1;
	spi_wrapper_flash_instruction(spi_write_buff, spi_read_buff, 2);
	gSTAT_REG_RSR1.Byte = spi_read_buff[1];

    return(gSTAT_REG_RSR1.Byte);
}


/*******************************************************************************
**  Function     : devFLASH_ReadStatusRegister2
**  Description  : read's the high byte status register
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : return's high byte status register
** _____________________________________________________________________________
**  Date    Author                Changes
**  
*******************************************************************************/
UINT8 devFLASH_ReadStatusRegister2(void){

	spi_write_buff[0] = SPIFLASH_READ_STATUS_REGISTER_2;
	spi_wrapper_flash_instruction(spi_write_buff, spi_read_buff, 2);
	gSTAT_REG_RSR1.Byte = spi_read_buff[1];

    return(gSTAT_REG_RSR2.Byte);
}


/*******************************************************************************
**  Function     : devFLASH_CheckWriteEnableBit
**  Description  : checks the write enable register
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : return's the write enable bit status
**                 0 - write disabled
**                 1 - write enabled
** _____________________________________________________________________________
**  Date    Author                Changes
**  
*******************************************************************************/
UINT8 devFLASH_CheckWriteEnableBit(void){

	devFLASH_ReadStatusRegister1();
	
    if(gSTAT_REG_RSR1.Bit.WEL == 1)
		return(1);
    
	return(0);
}

/*******************************************************************************
**  Function     : devFLASH_CheckBusyBit
**  Description  : check's the busy bit status register
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : return's the busy bit status register
**                 0 - not busy
**                 1 - busy  
** _____________________________________________________________________________
**  Date    Author                Changes
**  
*******************************************************************************/
UINT8 devFLASH_CheckBusyBit(void){

	devFLASH_ReadStatusRegister1();

    if(gSTAT_REG_RSR1.Bit.BUSY == 1)
		return(1);
    
    return(0);
}


/*******************************************************************************
**  Function     : devFLASH_WriteEnable
**  Description  : set's write enable to external flash memory
**  PreCondition : none
**  Side Effects : once set the flash memory device is always be set write data.
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  
*******************************************************************************/
void devFLASH_WriteEnable(void){

	spi_write_buff[0] = SPIFLASH_WRITE_ENABLE;
	spi_wrapper_flash_instruction(spi_write_buff, spi_read_buff, 1);
}



/*******************************************************************************
**  Function     : devFLASH_ClearChip
**  Description  : erases 1MB data of external flash memory
**  PreCondition : none
**  Side Effects : all erased data in the 1MB external flash cannot be recovered..
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  
*******************************************************************************/
void devFLASH_ClearChip(void){
    
    // before Write or Erase must set first the write enable bit...
    devFLASH_WriteEnable();

    spi_write_buff[0] = SPIFLASH_CHIP_ERASE;
    spi_wrapper_flash_instruction(spi_write_buff, spi_read_buff, 1);

    while(devFLASH_CheckBusyBit() == 1);
}


/*******************************************************************************
**  Function     : devFLASH_PowerDown
**  Description  : calls power down register to save power.
**  PreCondition : none
**  Side Effects : could operate the flash until wakeup register is called.
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  
*******************************************************************************/
void devFLASH_PowerDown(void){

	spi_write_buff[0] = SPIFLASH_POWER_DOWN;
	spi_wrapper_flash_instruction(spi_write_buff, spi_read_buff, 1);    
}


/*******************************************************************************
**  Function     : devFLASH_PowerUp
**  Description  : release power down register and wakes-up the system
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  
*******************************************************************************/
void devFLASH_PowerUp(void){

	spi_write_buff[0] = SPIFLASH_RELPWRDWN_DEVID;
	spi_wrapper_flash_instruction(spi_write_buff, spi_read_buff, 5);
}


/*******************************************************************************
**  Function     : devFLASH_BootSectorFill_Block0
**  Description  : write the MBR fixed data to bootsector 
**  PreCondition : none
**  Side Effects : updates the global block size buffer 
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void devFLASH_BootSectorFill_Block0(void){

    gubFlashBlockSizeBuffer[0]    = 235;
    gubFlashBlockSizeBuffer[1]    = 60;
    gubFlashBlockSizeBuffer[2]    = 144;
    gubFlashBlockSizeBuffer[3]    = 77;
    gubFlashBlockSizeBuffer[4]    = 83;
    gubFlashBlockSizeBuffer[5]    = 68;
    gubFlashBlockSizeBuffer[6]    = 79;
    gubFlashBlockSizeBuffer[7]    = 83;
    gubFlashBlockSizeBuffer[8]    = 53;
    gubFlashBlockSizeBuffer[9]    = 46;
    gubFlashBlockSizeBuffer[10]   = 48;
    gubFlashBlockSizeBuffer[11]   = 0;
    gubFlashBlockSizeBuffer[12]   = 2;
    gubFlashBlockSizeBuffer[13]   = 1;
    gubFlashBlockSizeBuffer[14]   = 4;
    gubFlashBlockSizeBuffer[15]   = 0;
    gubFlashBlockSizeBuffer[16]   = 2;
    gubFlashBlockSizeBuffer[17]   = 0;
    gubFlashBlockSizeBuffer[18]   = 2;
    gubFlashBlockSizeBuffer[19]   = 1;
    gubFlashBlockSizeBuffer[20]   = 8;
    gubFlashBlockSizeBuffer[21]   = 248;
    gubFlashBlockSizeBuffer[22]   = 6;
    gubFlashBlockSizeBuffer[23]   = 0;
    gubFlashBlockSizeBuffer[24]   = 1;
    gubFlashBlockSizeBuffer[25]   = 0;
    gubFlashBlockSizeBuffer[26]   = 1;
    gubFlashBlockSizeBuffer[27]   = 0;
    gubFlashBlockSizeBuffer[28]   = 0;
    gubFlashBlockSizeBuffer[29]   = 0;
    gubFlashBlockSizeBuffer[30]   = 0;
    gubFlashBlockSizeBuffer[31]   = 0;
    gubFlashBlockSizeBuffer[32]   = 0;
    gubFlashBlockSizeBuffer[33]   = 0;
    gubFlashBlockSizeBuffer[34]   = 0;
    gubFlashBlockSizeBuffer[35]   = 0;
    gubFlashBlockSizeBuffer[36]   = 0;
    gubFlashBlockSizeBuffer[37]   = 0;
    gubFlashBlockSizeBuffer[38]   = 41;
    gubFlashBlockSizeBuffer[39]   = 80;
    gubFlashBlockSizeBuffer[40]   = 24;
    gubFlashBlockSizeBuffer[41]   = 53;
    gubFlashBlockSizeBuffer[42]   = 56;
    gubFlashBlockSizeBuffer[43]   = 78;
    gubFlashBlockSizeBuffer[44]   = 79;
    gubFlashBlockSizeBuffer[45]   = 32;
    gubFlashBlockSizeBuffer[46]   = 78;
    gubFlashBlockSizeBuffer[47]   = 65;
    gubFlashBlockSizeBuffer[48]   = 77;
    gubFlashBlockSizeBuffer[49]   = 69;
    gubFlashBlockSizeBuffer[50]   = 32;
    gubFlashBlockSizeBuffer[51]   = 32;
    gubFlashBlockSizeBuffer[52]   = 32;
    gubFlashBlockSizeBuffer[53]   = 32;
    gubFlashBlockSizeBuffer[54]   = 70;
    gubFlashBlockSizeBuffer[55]   = 65;
    gubFlashBlockSizeBuffer[56]   = 84;
    gubFlashBlockSizeBuffer[57]   = 49;
    gubFlashBlockSizeBuffer[58]   = 50;
    gubFlashBlockSizeBuffer[59]   = 32;
    gubFlashBlockSizeBuffer[60]   = 32;
    gubFlashBlockSizeBuffer[61]   = 32;
    gubFlashBlockSizeBuffer[62]   = 51;
    gubFlashBlockSizeBuffer[63]   = 201;
    gubFlashBlockSizeBuffer[64]   = 142;
    gubFlashBlockSizeBuffer[65]   = 209;
    gubFlashBlockSizeBuffer[66]   = 188;
    gubFlashBlockSizeBuffer[67]   = 240;
    gubFlashBlockSizeBuffer[68]   = 123;
    gubFlashBlockSizeBuffer[69]   = 142;
    gubFlashBlockSizeBuffer[70]   = 217;
    gubFlashBlockSizeBuffer[71]   = 184;
    gubFlashBlockSizeBuffer[72]   = 0;
    gubFlashBlockSizeBuffer[73]   = 32;
    gubFlashBlockSizeBuffer[74]   = 142;
    gubFlashBlockSizeBuffer[75]   = 192;
    gubFlashBlockSizeBuffer[76]   = 252;
    gubFlashBlockSizeBuffer[77]   = 189;
    gubFlashBlockSizeBuffer[78]   =   0;
    gubFlashBlockSizeBuffer[79]   = 124;
    gubFlashBlockSizeBuffer[80]   =  56;
    gubFlashBlockSizeBuffer[81]   =  78;
    gubFlashBlockSizeBuffer[82]   =  36;
    gubFlashBlockSizeBuffer[83]   = 125;
    gubFlashBlockSizeBuffer[84]   =  36;
    gubFlashBlockSizeBuffer[85]   = 139;
    gubFlashBlockSizeBuffer[86]   = 193;
    gubFlashBlockSizeBuffer[87]   = 153;
    gubFlashBlockSizeBuffer[88]   = 232;
    gubFlashBlockSizeBuffer[89]   =  60;
    gubFlashBlockSizeBuffer[90]   =   1;
    gubFlashBlockSizeBuffer[91]   = 114;
    gubFlashBlockSizeBuffer[92]   =  28;
    gubFlashBlockSizeBuffer[93]   = 131;
    gubFlashBlockSizeBuffer[94]   = 235;
    gubFlashBlockSizeBuffer[95]   =  58;
    gubFlashBlockSizeBuffer[96]   = 102;
    gubFlashBlockSizeBuffer[97]   = 161;
    gubFlashBlockSizeBuffer[98]   =  28;
    gubFlashBlockSizeBuffer[99]   = 124;
    gubFlashBlockSizeBuffer[100]  =  38;
    gubFlashBlockSizeBuffer[101]  = 102;
    gubFlashBlockSizeBuffer[102]  =  59;
    gubFlashBlockSizeBuffer[103]  =   7;
    gubFlashBlockSizeBuffer[104]  =  38;
    gubFlashBlockSizeBuffer[105]  = 138;
    gubFlashBlockSizeBuffer[106]  =  87;
    gubFlashBlockSizeBuffer[107]  = 252;
    gubFlashBlockSizeBuffer[108]  = 117;
    gubFlashBlockSizeBuffer[109]  =   6;
    gubFlashBlockSizeBuffer[110]  = 128;
    gubFlashBlockSizeBuffer[111]  = 202;
    gubFlashBlockSizeBuffer[112]  =   2;
    gubFlashBlockSizeBuffer[113]  = 136;
    gubFlashBlockSizeBuffer[114]  =  86;
    gubFlashBlockSizeBuffer[115]  =   2;
    gubFlashBlockSizeBuffer[116]  = 128;
    gubFlashBlockSizeBuffer[117]  = 195;
    gubFlashBlockSizeBuffer[118]  =  16;
    gubFlashBlockSizeBuffer[119]  = 115;
    gubFlashBlockSizeBuffer[120]  = 235;
    gubFlashBlockSizeBuffer[121]  =  51;
    gubFlashBlockSizeBuffer[122]  = 201;
    gubFlashBlockSizeBuffer[123]  = 138;
    gubFlashBlockSizeBuffer[124]  =  70;
    gubFlashBlockSizeBuffer[125]  =  16;
    gubFlashBlockSizeBuffer[126]  = 152;
    gubFlashBlockSizeBuffer[127]  = 247;
    gubFlashBlockSizeBuffer[128]  = 102;
    gubFlashBlockSizeBuffer[129]  =  22;
    gubFlashBlockSizeBuffer[130]  =   3;
    gubFlashBlockSizeBuffer[131]  =  70;
    gubFlashBlockSizeBuffer[132]  =  28;
    gubFlashBlockSizeBuffer[133]  =  19;
    gubFlashBlockSizeBuffer[134]  =  86;
    gubFlashBlockSizeBuffer[135]  =  30;
    gubFlashBlockSizeBuffer[136]  =   3;
    gubFlashBlockSizeBuffer[137]  =  70;
    gubFlashBlockSizeBuffer[138]  =  14;
    gubFlashBlockSizeBuffer[139]  =  19;
    gubFlashBlockSizeBuffer[140]  = 209;
    gubFlashBlockSizeBuffer[141]  = 139;
    gubFlashBlockSizeBuffer[142]  = 118;
    gubFlashBlockSizeBuffer[143]  =  17;
    gubFlashBlockSizeBuffer[144]  =  96;
    gubFlashBlockSizeBuffer[145]  = 137;
    gubFlashBlockSizeBuffer[146]  =  70;
    gubFlashBlockSizeBuffer[147]  = 252;
    gubFlashBlockSizeBuffer[148]  = 137;
    gubFlashBlockSizeBuffer[149]  =  86;
    gubFlashBlockSizeBuffer[150]  = 254;
    gubFlashBlockSizeBuffer[151]  = 184;
    gubFlashBlockSizeBuffer[152]  =  32;
    gubFlashBlockSizeBuffer[153]  =   0;
    gubFlashBlockSizeBuffer[154]  = 247;
    gubFlashBlockSizeBuffer[155]  = 230;
    gubFlashBlockSizeBuffer[156]  = 139;
    gubFlashBlockSizeBuffer[157]  =  94;
    gubFlashBlockSizeBuffer[158]  =  11;
    gubFlashBlockSizeBuffer[159]  =   3;
    gubFlashBlockSizeBuffer[160]  = 195;
    gubFlashBlockSizeBuffer[161]  =  72;
    gubFlashBlockSizeBuffer[162]  = 247;
    gubFlashBlockSizeBuffer[163]  = 243;
    gubFlashBlockSizeBuffer[164]  =   1;
    gubFlashBlockSizeBuffer[165]  =  70;
    gubFlashBlockSizeBuffer[166]  = 252;
    gubFlashBlockSizeBuffer[167]  =  17;
    gubFlashBlockSizeBuffer[168]  =  78;
    gubFlashBlockSizeBuffer[169]  = 254;
    gubFlashBlockSizeBuffer[170]  =  97;
    gubFlashBlockSizeBuffer[171]  = 191;
    gubFlashBlockSizeBuffer[172]  =   0;
    gubFlashBlockSizeBuffer[173]  =   0;
    gubFlashBlockSizeBuffer[174]  = 232;
    gubFlashBlockSizeBuffer[175]  = 230;
    gubFlashBlockSizeBuffer[176]  =   0;
    gubFlashBlockSizeBuffer[177]  = 114;
    gubFlashBlockSizeBuffer[178]  =  57;
    gubFlashBlockSizeBuffer[179]  =  38;
    gubFlashBlockSizeBuffer[180]  =  56;
    gubFlashBlockSizeBuffer[181]  =  45;
    gubFlashBlockSizeBuffer[182]  = 116;
    gubFlashBlockSizeBuffer[183]  =  23;
    gubFlashBlockSizeBuffer[184]  =  96;
    gubFlashBlockSizeBuffer[185]  = 177;
    gubFlashBlockSizeBuffer[186]  =  11;
    gubFlashBlockSizeBuffer[187]  = 190;
    gubFlashBlockSizeBuffer[188]  = 161;
    gubFlashBlockSizeBuffer[189]  = 125;
    gubFlashBlockSizeBuffer[190]  = 243;
    gubFlashBlockSizeBuffer[191]  = 166;
    gubFlashBlockSizeBuffer[192]  =  97;
    gubFlashBlockSizeBuffer[193]  = 116;
    gubFlashBlockSizeBuffer[194]  =  50;
    gubFlashBlockSizeBuffer[195]  =  78;
    gubFlashBlockSizeBuffer[196]  = 116;
    gubFlashBlockSizeBuffer[197]  =   9;
    gubFlashBlockSizeBuffer[198]  = 131;
    gubFlashBlockSizeBuffer[199]  = 199;
    gubFlashBlockSizeBuffer[200]  =  32;
    gubFlashBlockSizeBuffer[201]  =  59;
    gubFlashBlockSizeBuffer[202]  = 251;
    gubFlashBlockSizeBuffer[203]  = 114;
    gubFlashBlockSizeBuffer[204]  = 230;
    gubFlashBlockSizeBuffer[205]  = 235;
    gubFlashBlockSizeBuffer[206]  = 220;
    gubFlashBlockSizeBuffer[207]  = 160;
    gubFlashBlockSizeBuffer[208]  = 251;
    gubFlashBlockSizeBuffer[209]  = 125;
    gubFlashBlockSizeBuffer[210]  = 180;
    gubFlashBlockSizeBuffer[211]  = 125;
    gubFlashBlockSizeBuffer[212]  = 139;
    gubFlashBlockSizeBuffer[213]  = 240;
    gubFlashBlockSizeBuffer[214]  = 172;
    gubFlashBlockSizeBuffer[215]  = 152;
    gubFlashBlockSizeBuffer[216]  =  64;
    gubFlashBlockSizeBuffer[217]  = 116;
    gubFlashBlockSizeBuffer[218]  =  12;
    gubFlashBlockSizeBuffer[219]  =  72;
    gubFlashBlockSizeBuffer[220]  = 116;
    gubFlashBlockSizeBuffer[221]  =  19;
    gubFlashBlockSizeBuffer[222]  = 180;
    gubFlashBlockSizeBuffer[223]  =  14;
    gubFlashBlockSizeBuffer[224]  = 187;
    gubFlashBlockSizeBuffer[225]  =   7;
    gubFlashBlockSizeBuffer[226]  =   0;
    gubFlashBlockSizeBuffer[227]  = 205;
    gubFlashBlockSizeBuffer[228]  =  16;
    gubFlashBlockSizeBuffer[229]  = 235;
    gubFlashBlockSizeBuffer[230]  = 239;
    gubFlashBlockSizeBuffer[231]  = 160;
    gubFlashBlockSizeBuffer[232]  = 253;
    gubFlashBlockSizeBuffer[233]  = 125;
    gubFlashBlockSizeBuffer[234]  = 235;
    gubFlashBlockSizeBuffer[235]  = 230;
    gubFlashBlockSizeBuffer[236]  = 160;
    gubFlashBlockSizeBuffer[237]  = 252;
    gubFlashBlockSizeBuffer[238]  = 125;
    gubFlashBlockSizeBuffer[239]  = 235;
    gubFlashBlockSizeBuffer[240]  = 225;
    gubFlashBlockSizeBuffer[241]  = 205;
    gubFlashBlockSizeBuffer[242]  =  22;
    gubFlashBlockSizeBuffer[243]  = 205;
    gubFlashBlockSizeBuffer[244]  =  25;
    gubFlashBlockSizeBuffer[245]  =  38;
    gubFlashBlockSizeBuffer[246]  = 139;
    gubFlashBlockSizeBuffer[247]  =  85;
    gubFlashBlockSizeBuffer[248]  =  26;
    gubFlashBlockSizeBuffer[249]  =  82;
    gubFlashBlockSizeBuffer[250]  = 176;
    gubFlashBlockSizeBuffer[251]  =   1;
    gubFlashBlockSizeBuffer[252]  = 187;
    gubFlashBlockSizeBuffer[253]  =   0;
    gubFlashBlockSizeBuffer[254]  =   0;
    gubFlashBlockSizeBuffer[255]  = 232;
    gubFlashBlockSizeBuffer[256]  =  59;
    gubFlashBlockSizeBuffer[257]  =   0;
    gubFlashBlockSizeBuffer[258]  = 114;
    gubFlashBlockSizeBuffer[259]  = 232;
    gubFlashBlockSizeBuffer[260]  =  91;
    gubFlashBlockSizeBuffer[261]  = 138;
    gubFlashBlockSizeBuffer[262]  =  86;
    gubFlashBlockSizeBuffer[263]  =  36;
    gubFlashBlockSizeBuffer[264]  = 190;
    gubFlashBlockSizeBuffer[265]  =  11;
    gubFlashBlockSizeBuffer[266]  = 124;
    gubFlashBlockSizeBuffer[267]  = 139;
    gubFlashBlockSizeBuffer[268]  = 252;
    gubFlashBlockSizeBuffer[269]  = 199;
    gubFlashBlockSizeBuffer[270]  =  70;
    gubFlashBlockSizeBuffer[271]  = 240;
    gubFlashBlockSizeBuffer[272]  =  61;
    gubFlashBlockSizeBuffer[273]  = 125;
    gubFlashBlockSizeBuffer[274]  = 199;
    gubFlashBlockSizeBuffer[275]  =  70;
    gubFlashBlockSizeBuffer[276]  = 244;
    gubFlashBlockSizeBuffer[277]  =  41;
    gubFlashBlockSizeBuffer[278]  = 125;
    gubFlashBlockSizeBuffer[279]  = 140;
    gubFlashBlockSizeBuffer[280]  = 217;
    gubFlashBlockSizeBuffer[281]  = 137;
    gubFlashBlockSizeBuffer[282]  =  78;
    gubFlashBlockSizeBuffer[283]  = 242;
    gubFlashBlockSizeBuffer[284]  = 137;
    gubFlashBlockSizeBuffer[285]  =  78;
    gubFlashBlockSizeBuffer[286]  = 246;
    gubFlashBlockSizeBuffer[287]  = 198;
    gubFlashBlockSizeBuffer[288]  =   6;
    gubFlashBlockSizeBuffer[289]  = 150;
    gubFlashBlockSizeBuffer[290]  = 125;
    gubFlashBlockSizeBuffer[291]  = 203;
    gubFlashBlockSizeBuffer[292]  = 234;
    gubFlashBlockSizeBuffer[293]  =   3;
    gubFlashBlockSizeBuffer[294]  =   0;
    gubFlashBlockSizeBuffer[295]  =   0;
    gubFlashBlockSizeBuffer[296]  =  32;
    gubFlashBlockSizeBuffer[297]  =  15;
    gubFlashBlockSizeBuffer[298]  = 182;
    gubFlashBlockSizeBuffer[299]  = 200;
    gubFlashBlockSizeBuffer[300]  = 102;
    gubFlashBlockSizeBuffer[301]  = 139;
    gubFlashBlockSizeBuffer[302]  =  70;
    gubFlashBlockSizeBuffer[303]  = 248;
    gubFlashBlockSizeBuffer[304]  = 102;
    gubFlashBlockSizeBuffer[305]  =   3;
    gubFlashBlockSizeBuffer[306]  =  70;
    gubFlashBlockSizeBuffer[307]  =  28;
    gubFlashBlockSizeBuffer[308]  = 102;
    gubFlashBlockSizeBuffer[309]  = 139;
    gubFlashBlockSizeBuffer[310]  = 208;
    gubFlashBlockSizeBuffer[311]  = 102;
    gubFlashBlockSizeBuffer[312]  = 193;
    gubFlashBlockSizeBuffer[313]  = 234;
    gubFlashBlockSizeBuffer[314]  =  16;
    gubFlashBlockSizeBuffer[315]  = 235;
    gubFlashBlockSizeBuffer[316]  =  94;
    gubFlashBlockSizeBuffer[317]  =  15;
    gubFlashBlockSizeBuffer[318]  = 182;
    gubFlashBlockSizeBuffer[319]  = 200;
    gubFlashBlockSizeBuffer[320]  =  74;
    gubFlashBlockSizeBuffer[321]  =  74;
    gubFlashBlockSizeBuffer[322]  = 138;
    gubFlashBlockSizeBuffer[323]  =  70;
    gubFlashBlockSizeBuffer[324]  =  13;
    gubFlashBlockSizeBuffer[325]  =  50;
    gubFlashBlockSizeBuffer[326]  = 228;
    gubFlashBlockSizeBuffer[327]  = 247;
    gubFlashBlockSizeBuffer[328]  = 226;
    gubFlashBlockSizeBuffer[329]  =   3;
    gubFlashBlockSizeBuffer[330]  =  70;
    gubFlashBlockSizeBuffer[331]  = 252;
    gubFlashBlockSizeBuffer[332]  =  19;
    gubFlashBlockSizeBuffer[333]  =  86;
    gubFlashBlockSizeBuffer[334]  = 254;
    gubFlashBlockSizeBuffer[335]  = 235;
    gubFlashBlockSizeBuffer[336]  =  74;
    gubFlashBlockSizeBuffer[337]  =  82;
    gubFlashBlockSizeBuffer[338]  =  80;
    gubFlashBlockSizeBuffer[339]  =   6;
    gubFlashBlockSizeBuffer[340]  =  83;
    gubFlashBlockSizeBuffer[341]  = 106;
    gubFlashBlockSizeBuffer[342]  =   1;
    gubFlashBlockSizeBuffer[343]  = 106;
    gubFlashBlockSizeBuffer[344]  =  16;
    gubFlashBlockSizeBuffer[345]  = 145;
    gubFlashBlockSizeBuffer[346]  = 139;
    gubFlashBlockSizeBuffer[347]  =  70;
    gubFlashBlockSizeBuffer[348]  =  24;
    gubFlashBlockSizeBuffer[349]  = 150;
    gubFlashBlockSizeBuffer[350]  = 146;
    gubFlashBlockSizeBuffer[351]  =  51;
    gubFlashBlockSizeBuffer[352]  = 210;
    gubFlashBlockSizeBuffer[353]  = 247;
    gubFlashBlockSizeBuffer[354]  = 246;
    gubFlashBlockSizeBuffer[355]  = 145;
    gubFlashBlockSizeBuffer[356]  = 247;
    gubFlashBlockSizeBuffer[357]  = 246;
    gubFlashBlockSizeBuffer[358]  =  66;
    gubFlashBlockSizeBuffer[359]  = 135;
    gubFlashBlockSizeBuffer[360]  = 202;
    gubFlashBlockSizeBuffer[361]  = 247;
    gubFlashBlockSizeBuffer[362]  = 118;
    gubFlashBlockSizeBuffer[363]  =  26;
    gubFlashBlockSizeBuffer[364]  = 138;
    gubFlashBlockSizeBuffer[365]  = 242;
    gubFlashBlockSizeBuffer[366]  = 138;
    gubFlashBlockSizeBuffer[367]  = 232;
    gubFlashBlockSizeBuffer[368]  = 192;
    gubFlashBlockSizeBuffer[369]  = 204;
    gubFlashBlockSizeBuffer[370]  =   2;
    gubFlashBlockSizeBuffer[371]  =  10;
    gubFlashBlockSizeBuffer[372]  = 204;
    gubFlashBlockSizeBuffer[373]  = 184;
    gubFlashBlockSizeBuffer[374]  =   1;
    gubFlashBlockSizeBuffer[375]  =   2;
    gubFlashBlockSizeBuffer[376]  = 128;
    gubFlashBlockSizeBuffer[377]  = 126;
    gubFlashBlockSizeBuffer[378]  =   2;
    gubFlashBlockSizeBuffer[379]  =  14;
    gubFlashBlockSizeBuffer[380]  = 117;
    gubFlashBlockSizeBuffer[381]  =   4;
    gubFlashBlockSizeBuffer[382]  = 180;
    gubFlashBlockSizeBuffer[383]  =  66;
    gubFlashBlockSizeBuffer[384]  = 139;
    gubFlashBlockSizeBuffer[385]  = 244;
    gubFlashBlockSizeBuffer[386]  = 138;
    gubFlashBlockSizeBuffer[387]  =  86;
    gubFlashBlockSizeBuffer[388]  =  36;
    gubFlashBlockSizeBuffer[389]  = 205;
    gubFlashBlockSizeBuffer[390]  =  19;
    gubFlashBlockSizeBuffer[391]  =  97;
    gubFlashBlockSizeBuffer[392]  =  97;
    gubFlashBlockSizeBuffer[393]  = 114;
    gubFlashBlockSizeBuffer[394]  =  11;
    gubFlashBlockSizeBuffer[395]  =  64;
    gubFlashBlockSizeBuffer[396]  = 117;
    gubFlashBlockSizeBuffer[397]  =   1;
    gubFlashBlockSizeBuffer[398]  =  66;
    gubFlashBlockSizeBuffer[399]  =   3;
    gubFlashBlockSizeBuffer[400]  =  94;
    gubFlashBlockSizeBuffer[401]  =  11;
    gubFlashBlockSizeBuffer[402]  =  73;
    gubFlashBlockSizeBuffer[403]  = 117;
    gubFlashBlockSizeBuffer[404]  =   6;
    gubFlashBlockSizeBuffer[405]  = 248;
    gubFlashBlockSizeBuffer[406]  = 195;
    gubFlashBlockSizeBuffer[407]  =  65;
    gubFlashBlockSizeBuffer[408]  = 187;
    gubFlashBlockSizeBuffer[409]  =   0;
    gubFlashBlockSizeBuffer[410]  =   0;
    gubFlashBlockSizeBuffer[411]  =  96;
    gubFlashBlockSizeBuffer[412]  = 102;
    gubFlashBlockSizeBuffer[413]  = 106;
    gubFlashBlockSizeBuffer[414]  =   0;
    gubFlashBlockSizeBuffer[415]  = 235;
    gubFlashBlockSizeBuffer[416]  = 176;
    gubFlashBlockSizeBuffer[417]  =  78;
    gubFlashBlockSizeBuffer[418]  =  84;
    gubFlashBlockSizeBuffer[419]  =  76;
    gubFlashBlockSizeBuffer[420]  =  68;
    gubFlashBlockSizeBuffer[421]  =  82;
    gubFlashBlockSizeBuffer[422]  =  32;
    gubFlashBlockSizeBuffer[423]  =  32;
    gubFlashBlockSizeBuffer[424]  =  32;
    gubFlashBlockSizeBuffer[425]  =  32;
    gubFlashBlockSizeBuffer[426]  =  32;
    gubFlashBlockSizeBuffer[427]  =  32;
    gubFlashBlockSizeBuffer[428]  =  13;
    gubFlashBlockSizeBuffer[429]  =  10;
    gubFlashBlockSizeBuffer[430]  =  82;
    gubFlashBlockSizeBuffer[431]  = 101;
    gubFlashBlockSizeBuffer[432]  = 109;
    gubFlashBlockSizeBuffer[433]  = 111;
    gubFlashBlockSizeBuffer[434]  = 118;
    gubFlashBlockSizeBuffer[435]  = 101;
    gubFlashBlockSizeBuffer[436]  =  32;
    gubFlashBlockSizeBuffer[437]  = 100;
    gubFlashBlockSizeBuffer[438]  = 105;
    gubFlashBlockSizeBuffer[439]  = 115;
    gubFlashBlockSizeBuffer[440]  = 107;
    gubFlashBlockSizeBuffer[441]  = 115;
    gubFlashBlockSizeBuffer[442]  =  32;
    gubFlashBlockSizeBuffer[443]  = 111;
    gubFlashBlockSizeBuffer[444]  = 114;
    gubFlashBlockSizeBuffer[445]  =  32;
    gubFlashBlockSizeBuffer[446]  = 111;
    gubFlashBlockSizeBuffer[447]  = 116;
    gubFlashBlockSizeBuffer[448]  = 104;
    gubFlashBlockSizeBuffer[449]  = 101;
    gubFlashBlockSizeBuffer[450]  = 114;
    gubFlashBlockSizeBuffer[451]  =  32;
    gubFlashBlockSizeBuffer[452]  = 109;
    gubFlashBlockSizeBuffer[453]  = 101;
    gubFlashBlockSizeBuffer[454]  = 100;
    gubFlashBlockSizeBuffer[455]  = 105;
    gubFlashBlockSizeBuffer[456]  =  97;
    gubFlashBlockSizeBuffer[457]  =  46;
    gubFlashBlockSizeBuffer[458]  = 255;
    gubFlashBlockSizeBuffer[459]  =  13;
    gubFlashBlockSizeBuffer[460]  =  10;
    gubFlashBlockSizeBuffer[461]  =  68;
    gubFlashBlockSizeBuffer[462]  = 105;
    gubFlashBlockSizeBuffer[463]  = 115;
    gubFlashBlockSizeBuffer[464]  = 107;
    gubFlashBlockSizeBuffer[465]  =  32;
    gubFlashBlockSizeBuffer[466]  = 101;
    gubFlashBlockSizeBuffer[467]  = 114;
    gubFlashBlockSizeBuffer[468]  = 114;
    gubFlashBlockSizeBuffer[469]  = 111;
    gubFlashBlockSizeBuffer[470]  = 114;
    gubFlashBlockSizeBuffer[471]  = 255;
    gubFlashBlockSizeBuffer[472]  =  13;
    gubFlashBlockSizeBuffer[473]  =  10;
    gubFlashBlockSizeBuffer[474]  =  80;
    gubFlashBlockSizeBuffer[475]  = 114;
    gubFlashBlockSizeBuffer[476]  = 101;
    gubFlashBlockSizeBuffer[477]  = 115;
    gubFlashBlockSizeBuffer[478]  = 115;
    gubFlashBlockSizeBuffer[479]  =  32;
    gubFlashBlockSizeBuffer[480]  =  97;
    gubFlashBlockSizeBuffer[481]  = 110;
    gubFlashBlockSizeBuffer[482]  = 121;
    gubFlashBlockSizeBuffer[483]  =  32;
    gubFlashBlockSizeBuffer[484]  = 107;
    gubFlashBlockSizeBuffer[485]  = 101;
    gubFlashBlockSizeBuffer[486]  = 121;
    gubFlashBlockSizeBuffer[487]  =  32;
    gubFlashBlockSizeBuffer[488]  = 116;
    gubFlashBlockSizeBuffer[489]  = 111;
    gubFlashBlockSizeBuffer[490]  =  32;
    gubFlashBlockSizeBuffer[491]  = 114;
    gubFlashBlockSizeBuffer[492]  = 101;
    gubFlashBlockSizeBuffer[493]  = 115;
    gubFlashBlockSizeBuffer[494]  = 116;
    gubFlashBlockSizeBuffer[495]  =  97;
    gubFlashBlockSizeBuffer[496]  = 114;
    gubFlashBlockSizeBuffer[497]  = 116;
    gubFlashBlockSizeBuffer[498]  =  13;
    gubFlashBlockSizeBuffer[499]  =  10;
    gubFlashBlockSizeBuffer[500]  =   0;
    gubFlashBlockSizeBuffer[501]  =   0;
    gubFlashBlockSizeBuffer[502]  =   0;
    gubFlashBlockSizeBuffer[503]  =   0;
    gubFlashBlockSizeBuffer[504]  =   0;
    gubFlashBlockSizeBuffer[505]  =   0;
    gubFlashBlockSizeBuffer[506]  =   0;
    gubFlashBlockSizeBuffer[507]  = 172;
    gubFlashBlockSizeBuffer[508]  = 203;
    gubFlashBlockSizeBuffer[509]  = 216;
    gubFlashBlockSizeBuffer[510]  =  85;
    gubFlashBlockSizeBuffer[511]  = 170;
    gubFlashBlockSizeBuffer[512]  =   0;
    gubFlashBlockSizeBuffer[513]  =   0;
    gubFlashBlockSizeBuffer[2048]  = 248;
    gubFlashBlockSizeBuffer[2049]  = 255;
    gubFlashBlockSizeBuffer[2050]  = 255;
    
 }
 
/*******************************************************************************
**  Function     : devFLASH_BootSectorFill_Block1
**  Description  : updates block 1 bootsector 
**  PreCondition : none
**  Side Effects : updates the global block size buffer. 
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/   
 void devFLASH_BootSectorFill_Block1(void){

    gubFlashBlockSizeBuffer[1024]  = 248;
    gubFlashBlockSizeBuffer[1025]  = 255;
    gubFlashBlockSizeBuffer[1026]  = 255;    
 }


/*******************************************************************************
**  Function     : devFLASH_BootSectorFill_Block2
**  Description  : updates block 2 bootsector 
**  PreCondition : none
**  Side Effects : updates the global block size buffer 
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/    
 void devFLASH_BootSectorFill_Block2(void){
 
    gubFlashBlockSizeBuffer[0]    = 84;
    gubFlashBlockSizeBuffer[1]    = 84;
    gubFlashBlockSizeBuffer[2]    = 52;
    gubFlashBlockSizeBuffer[3]    = 85;
    gubFlashBlockSizeBuffer[4]    = 83;
    gubFlashBlockSizeBuffer[5]    = 66;
    gubFlashBlockSizeBuffer[6]    = 45;
    gubFlashBlockSizeBuffer[7]    = 77;
    gubFlashBlockSizeBuffer[8]    = 65;
    gubFlashBlockSizeBuffer[9]    = 32;
    gubFlashBlockSizeBuffer[10]   = 32;
    gubFlashBlockSizeBuffer[11]   =  8;

    gubFlashBlockSizeBuffer[22]   = 99;
    gubFlashBlockSizeBuffer[23]   = 90;
    gubFlashBlockSizeBuffer[24]   = 92;
    gubFlashBlockSizeBuffer[25]   = 62;
    
 }


/*******************************************************************************
**  Function     : devFLASH_Read4096Bytes
**  Description  : read's 4096 bytes and dump's to the global block size buffer
**  PreCondition : none
**  Side Effects : it erases 4096 bytes of data in a specific block starting address..
**  Input        : par_udwBlockEraseAddr - starting block erase address
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  
*******************************************************************************/
void devFLASH_Read4096Bytes(UINT32 par_udwBlockEraseAddr){
    
    UINT32 *ludwPhysicalSectorAddress;
    UINT8  lubByteData[4] = {0,0,0,0};
    UINT16 luwSector_Offset;
    UINT8  lubFetchByteData;
    
    // get pointer address reference...
    ludwPhysicalSectorAddress = (UINT32 *)lubByteData;
	
	// calculate the starting address...
   	*ludwPhysicalSectorAddress = par_udwBlockEraseAddr * FLASH_ERASE_BLOCK_SIZE;

    // wait here if serial flash is busy!
    while(devFLASH_CheckBusyBit()==1);

    // CS low...

    // send program command...
    drvSPI2_ByteWrite(SPIFLASH_READ_DATA); 
    // send 24bit address command...    
    drvSPI2_ByteWrite(lubByteData[2]);       
    drvSPI2_ByteWrite(lubByteData[1]);       
    drvSPI2_ByteWrite(lubByteData[0]);
    
    // get data from a specified data buffer size...
	for(luwSector_Offset = 0 ; luwSector_Offset < FLASH_ERASE_BLOCK_SIZE - 1 ; luwSector_Offset++){
    	
        // read byte data from the external flash device...
        drvSPI2_ByteRead(&lubFetchByteData);        
        // update sector data...mjm 
        gubFlashBlockSizeBuffer[luwSector_Offset] = lubFetchByteData;
    }
	
	drvSPI2_wrapper_LastByteRead(&lubFetchByteData);
	// update sector data...mjm
	gubFlashBlockSizeBuffer[luwSector_Offset] = lubFetchByteData;
 
    // CS high...    
}


/*******************************************************************************
**  Function     : devFLASH_Write4096Bytes
**  Description  : write's 4096 bytes of data to the flash memory
**  PreCondition : none
**  Side Effects : overwrite's the data in the flash if exists...
**  Input        : par_udwBlockNumber - block number to write [0..256]
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  
*******************************************************************************/
void devFLASH_Write4096Bytes(UINT32 par_udwBlockNumber){
    
    
    //-------------------------------------------------
    // Note: before using this module must 
    //       initialize the global flash buffer data
    //       " gubFlashBlockSizeBuffer ". Must fill 
    //       the 4096 data buffer before calling this
    //       routine. 
    //
    //   \ mjmariveles..
    //-------------------------------------------------

    UINT32 *ludwBlockStartAddress;
    UINT8  lubByteData[4] = {0,0,0,0};
    UINT8  lubFetchByteData;
    
    UINT16 luwPageCtr;
    UINT16 luwByteCtr;    
    
    // get pointer address reference for spliting up the address in 1 byte format...
    ludwBlockStartAddress = (UINT32 *)lubByteData;
        
   	// set the 4096 block starting address...
   	*ludwBlockStartAddress = par_udwBlockNumber * FLASH_ERASE_BLOCK_SIZE;  

    //----------------------------------------------------------------------
    //   perform block erase...    
    //----------------------------------------------------------------------    

    // check here if still busy before asserting other request...
    while(devFLASH_CheckBusyBit()==1);   

    // before Write or Erase must set first the write enable bit...
    devFLASH_WriteEnable();    

    // CS low...

    // block erase command..
    spi_write_buff[0] = SPIFLASH_SECTOR_ERASE_4KB;
    // copy 24bit address command...
    spi_write_buff[1] = lubByteData[2];
    spi_write_buff[2] = lubByteData[1];
    spi_write_buff[3] = lubByteData[0];
	spi_wrapper_flash_instruction(spi_write_buff, spi_read_buff, 4);
    // CS high...

    // check here if still busy before asserting other request...
    while(devFLASH_CheckBusyBit() == 1);   
    
    //----------------------------------------------------------------------
    //   write 4096 zero's...    
    //----------------------------------------------------------------------    
    
    // get data from a 4096 block buffer...
	for(luwPageCtr = 0 ; luwPageCtr < 16 ; luwPageCtr++){
        
        // before Write or Erase must set first the write enable bit...
        devFLASH_WriteEnable();

        // CS low...
    
        // send program command...
        drvSPI2_ByteWrite(SPIFLASH_PAGE_PROGRAM); // page program...    
    
        // send 24bit address command...    
        drvSPI2_ByteWrite(lubByteData[2]);       
        drvSPI2_ByteWrite(lubByteData[1]);       
        drvSPI2_ByteWrite(lubByteData[0]);
    
        // write byte data...
        drvSPI2_ByteWrite(gubFlashBlockSizeBuffer[256 * luwPageCtr]);                
    
        // as a requirement of W25Q80BV must have a delay of 50usecs 
        // before writing to the next byte!.. mjm
        //Delay10us(5); // set to 10 x 5 = 50usec for sure!    
    
        // start 2nd byte writing...
        for(luwByteCtr = 1; luwByteCtr < 256 - 1; luwByteCtr++){        
            // load data...            
            lubFetchByteData = gubFlashBlockSizeBuffer[(256 * luwPageCtr) + luwByteCtr];
            // write byte data...
            drvSPI2_ByteWrite(lubFetchByteData);
        }
		
		lubFetchByteData = gubFlashBlockSizeBuffer[(256 * luwPageCtr) + luwByteCtr];
		// write byte data...
		drvSPI2_wrapper_ByteWrite(lubFetchByteData);
    
        // CS high...
    
        // check here if still busy before asserting other request...
        while(devFLASH_CheckBusyBit()==1);   
        
        // increment per 256 offset of spi flash address... 
        *ludwBlockStartAddress += 256;
    }
}


/*******************************************************************************
**  Function     : devFLASH_CreateMBR
**  Description  : create's MBR to a flash device
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void devFLASH_CreateMBR(void){
    
    UINT8 lubBlockAddress;

    //------------------------------------------------------
    //  This is a FAT file system bootsector image loader.
    //  To clean up all residual files after configuring 
    //  the device then this function must be called. 
    //
    //  \ mjmariveles..
    //------------------------------------------------------
    
    TT4_RED_LED_ON;
    TT4_GREEN_LED_ON;
    
    // reset flash to 255's...
    devFLASH_ClearChip(); 
    
    //----------------------------------------------------------------------
    //   perform bootsector data writing to 4096 buffer...    
    //----------------------------------------------------------------------        
    
    // write boot sector data to 4096 buffer...        
    memset((void *)&gubFlashBlockSizeBuffer[0],0,4096u);
    // clear the 1st 6 block to zero from address 0 - 24576...
    for(lubBlockAddress = 0 ; lubBlockAddress < 6 ; lubBlockAddress++) devFLASH_Write4096Bytes((UINT32)lubBlockAddress);
    
    // write boot sector data to 4096 buffer...    
    devFLASH_BootSectorFill_Block0();    
    // write block 0
    devFLASH_Write4096Bytes(0);
    
    // clear 4096 buffer...            
    memset((void *)&gubFlashBlockSizeBuffer[0],0,4096u);
    // write boot sector data to 4096 buffer...
    devFLASH_BootSectorFill_Block1();    
    // write block 1
    devFLASH_Write4096Bytes(1);

    // clear 4096 buffer...
    memset((void *)&gubFlashBlockSizeBuffer[0],0,4096u);
    // write boot sector data to 4096 buffer...
    devFLASH_BootSectorFill_Block2();    
    // write block 2
    devFLASH_Write4096Bytes(2);
    
    TT4_RED_LED_OFF;
    TT4_GREEN_LED_OFF;
}

/*******************************************************************************
**  Function     : devFLASH_Buffer_ReadBlock
**  Description  : read's 4096 bytes of data from a block number
**  PreCondition : none
**  Side Effects : none
**  Input        : par_udwBlockNum - block number [0..256]
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  
*******************************************************************************/
void devFLASH_Buffer_ReadBlock(UINT32 par_udwBlockNum){
    
    volatile UINT32 *ludwBlockStartAddress;    
    volatile UINT8  lubByteData[4] = {0,0,0,0};
    volatile UINT16 luwSector_Offset;
    
    // get pointer address reference for spliting up the address in 1 byte format...
    ludwBlockStartAddress = (UINT32 *)lubByteData;
    // calculate block address...
    *ludwBlockStartAddress = par_udwBlockNum * FLASH_ERASE_BLOCK_SIZE;    	   	

   	// check and wait of spi flash is busy!!...
   	while(devFLASH_CheckBusyBit()==1);   

    // CS low...

    // send program command...
    drvSPI2_ByteWrite(SPIFLASH_READ_DATA); 
    // send 24bit address command...    
    drvSPI2_ByteWrite(lubByteData[2]);       
    drvSPI2_ByteWrite(lubByteData[1]);       
    drvSPI2_ByteWrite(lubByteData[0]);
    
    // copy all data from specific block addres....    
    luwSector_Offset = 0 ;	
    while(luwSector_Offset < FLASH_ERASE_BLOCK_SIZE - 1){
        // read byte data from the external flash device...
        drvSPI2_ByteRead((UINT8*)&gubFlashBlockSizeBuffer[luwSector_Offset]);
        // update sector data...mjm         
        ++luwSector_Offset;
    }
	
	drvSPI2_wrapper_LastByteRead((UINT8*)&gubFlashBlockSizeBuffer[luwSector_Offset]);
	 
    // CS high...
}

/*******************************************************************************
**  Function     : devFLASH_Buffer_WriteBlock
**  Description  : write's 4096 bytes of data to a block number
**  PreCondition : none
**  Side Effects : none
**  Input        : par_udwBlockNum - block number [0..256]
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  
*******************************************************************************/
void devFLASH_Buffer_WriteBlock(UINT32 par_udwBlockNum){

    volatile UINT32 *ludwBlockStartAddress;    
    volatile UINT8  lubByteData[4] = {0,0,0,0};    
    volatile UINT16 luwPageCtr;
    volatile UINT16 luwByteCtr;    
    
    // get pointer address reference for spliting up the address in 1 byte format...
    ludwBlockStartAddress = (UINT32 *)lubByteData;
    // calculate block address...
    *ludwBlockStartAddress = par_udwBlockNum * FLASH_ERASE_BLOCK_SIZE;    	   	

    // get data from a 4096 block buffer...
    luwPageCtr = 0 ;	
    while(luwPageCtr < SPIFLASH_PAGES_PER_BLOCK){    
        
        // check here if still busy before asserting other request...
        while(devFLASH_CheckBusyBit()==1);   

        // before Write or Erase must set first the write enable bit...
        devFLASH_WriteEnable();

        // CS low...
    
        // send program command...
        drvSPI2_ByteWrite(SPIFLASH_PAGE_PROGRAM); // page program...    
    
        // send 24bit address command...    
        drvSPI2_ByteWrite(lubByteData[2]);       
        drvSPI2_ByteWrite(lubByteData[1]);       
        drvSPI2_ByteWrite(lubByteData[0]);
		
        // start 2nd byte writing...
        luwByteCtr = 0 ;	
        while(luwByteCtr < SPIFLASH_PAGE_SIZE - 1){                        
            // write byte data...
            drvSPI2_ByteWrite(gubFlashBlockSizeBuffer[(SPIFLASH_PAGE_SIZE * luwPageCtr) + luwByteCtr]);                                
            ++luwByteCtr;
        }
		
		drvSPI2_wrapper_LastByteWrite(gubFlashBlockSizeBuffer[(SPIFLASH_PAGE_SIZE * luwPageCtr) + luwByteCtr]);
    
        // CS high...
    
        // increment page...
        ++luwPageCtr;
        
        // increment per 256 offset of spi flash address... 
        *ludwBlockStartAddress += SPIFLASH_PAGE_SIZE;        
    }
}

/*******************************************************************************
**  Function     : devFLASH_Buffer_EraseBlock
**  Description  : erases 4096 bytes of data to a block number
**  PreCondition : none
**  Side Effects : none
**  Input        : par_udwBlockNum - block number [0..256]
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  
*******************************************************************************/
void devFLASH_Buffer_EraseBlock(UINT32 par_udwBlockNum){
    
    volatile UINT32 *ludwBlockStartAddress;    
    volatile UINT8  lubByteData[4] = {0,0,0,0};        
    
    // get pointer address reference for spliting up the address in 1 byte format...
    ludwBlockStartAddress = (UINT32 *)lubByteData;
    // calculate block address...
    *ludwBlockStartAddress = par_udwBlockNum * FLASH_ERASE_BLOCK_SIZE;

   	// check and wait of spi flash is busy!!...
   	while(devFLASH_CheckBusyBit()==1);   

    // before Write or Erase must set first the write enable bit...
    devFLASH_WriteEnable();    

    // CS low...
	
	spi_write_buff[0] = SPIFLASH_SECTOR_ERASE_4KB;
	spi_write_buff[1] = lubByteData[2];
	spi_write_buff[2] = lubByteData[1];
	spi_write_buff[3] = lubByteData[0];
	spi_wrapper_flash_instruction(spi_write_buff, spi_read_buff, 4);  
}

/*******************************************************************************
**  Function     : devFLASH_Sector_ReadBlock
**  Description  : write's the 256 bytes internal buffer size to flash memory.
**  PreCondition : none 
**  Side Effects : none
**  Input        : par_udwSectorNum - sector numner [0..16]
**  Output       : par_ubBuffer - 256 bytes buffer data
** _____________________________________________________________________________
**  Date    Author                Changes
**  
*******************************************************************************/
void devFLASH_Sector_ReadBlock(UINT32 par_udwSectorNum, UINT8 *par_ubBuffer){
    
    volatile UINT32 *ludwPhysicalSectorAddress;
    volatile UINT8  lubByteData[4] = {0,0,0,0};    
    volatile UINT16 luwSector_Offset;    

    // get pointer address reference for spliting up the address in 1 byte format...
    ludwPhysicalSectorAddress = (UINT32 *)lubByteData;
	// calculate the starting address...
   	*ludwPhysicalSectorAddress =  par_udwSectorNum * MEDIA_SECTOR_SIZE; 
   	
   	// check and wait of spi flash is busy!!...
   	while(devFLASH_CheckBusyBit()==1);   

    // CS low...

    // send program command...
    drvSPI2_ByteWrite(SPIFLASH_READ_DATA); 
    // send 24bit address command...    
    drvSPI2_ByteWrite(lubByteData[2]);       
    drvSPI2_ByteWrite(lubByteData[1]);       
    drvSPI2_ByteWrite(lubByteData[0]);
        
    luwSector_Offset = 0;
    while(luwSector_Offset < MEDIA_SECTOR_SIZE - 1){
        // read byte data from the external flash device...        
        drvSPI2_ByteRead((UINT8*)&par_ubBuffer[luwSector_Offset]);
        ++luwSector_Offset;
    }
	
	drvSPI2_wrapper_LastByteRead((UINT8*)&par_ubBuffer[luwSector_Offset]);
	
    // CS high...
}

/*******************************************************************************
**  Function     : devFLASH_GetBlockStatus
**  Description  : calculates block address from the initial sector number. 
**  PreCondition : none
**  Side Effects : modifies the global block address parameters.
**  Input        : par_udwSectorNum - sector number [0..16]
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void devFLASH_GetBlockStatus(UINT32 par_udwSectorNum){
    
    // calculate the physical sector address... 
    gsSpiFlashBlock.udwSectorAddr = par_udwSectorNum * MEDIA_SECTOR_SIZE; // 512
    // block erase size...
    gsSpiFlashBlock.udwOffset = gsSpiFlashBlock.udwSectorAddr % FLASH_ERASE_BLOCK_SIZE; // 4096
    gsSpiFlashBlock.udwNumber = gsSpiFlashBlock.udwSectorAddr / FLASH_ERASE_BLOCK_SIZE; // 4096	 
    gsSpiFlashBlock.udwStartAddr = gsSpiFlashBlock.udwNumber * FLASH_ERASE_BLOCK_SIZE; // 4096	 
    
}

/*******************************************************************************
**  Function     : devFLASH_GetSectorStatus
**  Description  : calculates block size to get the sector address number. 
**  PreCondition : none
**  Side Effects : none
**  Input        : par_udwBlockNum - block number [0..256]
**  Output       : return's the sector address location 
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
UINT32 devFLASH_GetSectorStatus(UINT32 par_udwBlockNum){

    return(par_udwBlockNum * (FLASH_ERASE_BLOCK_SIZE / MEDIA_SECTOR_SIZE));
}


/* End of File */

