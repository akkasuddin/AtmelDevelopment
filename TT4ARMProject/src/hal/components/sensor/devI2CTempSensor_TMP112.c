/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm                                            **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc. and Blue Ocean Innovation Ltd.                            **
 **                                                                           **
 **  Copryright(c) 2010. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Blue Ocean Innovation Ltd.                        **
 **  Project#            :  449                                               **
 **  Creation date       :  08/20/2010                                        **
 **  Programmer          :  Michael J. Mariveles                              **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : devI2CTempSensor_TMP112.c                                         **
 **                                                                           **
 **  Description :                                                            **
 **    - external digital temperature sensor device driver.                   **
 **                                                                           **
 ******************************************************************************/
 
//#include "hwPIC24F.h"
#include "hwSystem.h"
#include "drvI2C.h"
#include "devI2CTempSensor_TMP112.h"
#include "utility.h"
#include "commTT4Config.h"
#include "drvI2C_Wrapper.h"

// Temperatue Sensor I2C Device Address if A0 Pin held to GND...
#define TMP_SENSOR_W_ADDR               0x90 // 10010000 Write Address...
#define TMP_SENSOR_R_ADDR               0x91 // 10010001 Read Address...

// Temperature ADC resolution constants...
#define TMP_RESOLUTION_VALUE            0.0625 
#define TMP_12BIT_MIN_TEMP_VALUE        0xC90   
#define TMP_12BIT_MAX_TEMP_VALUE        0x7FF
#define TMP_13BIT_MIN_TEMP_VALUE        0x1C90   
#define TMP_13BIT_MAX_TEMP_VALUE        0x960

// Pointer register settings
#define TMP_PTR_TEMP_REGISTER           0x00 // Read Only...
#define TMP_PTR_CONFIG_REGISTER         0x01 // Read/Write...
#define TMP_PTR_TLOW_REGISTER           0x02 // Read/Write...
#define TMP_PTR_THIGH_REGISTER          0x03 // Read/Write...

// Conversion rate settings...
#define TMP_CFG_CR_025HZ                0x00 // 0.25Hz 
#define TMP_CFG_CR_1HZ                  0x01 // 1Hz 
#define TMP_CFG_CR_4HZ                  0x02 // 4Hz (default)
#define TMP_CFG_CR_8HZ                  0x03 // 8Hz 

// Extended mode settings...
#define TMP_CFG_EM_12BITS               0x00 // 12Bits of temperature data  
#define TMP_CFG_EM_13BITS               0x01 // 13Bits of temperature data  

// Alert Bit settings...
#define TMP_CFG_AL_ABOVE                0x00 // equal or over setpoint temperature
#define TMP_CFG_AL_BELOW                0x01 // equal or under setpoint temperature

// Shutdown Mode settings...
#define TMP_CFG_SD_DIS                  0x00 // disable shutdown mode..
#define TMP_CFG_SD_ENA                  0x01 // 0.5uA sleep mode current..

// Thermostat Mode settings...
#define TMP_CFG_TM_COMP                 0x00 // comparator mode..
#define TMP_CFG_TM_INTR                 0x01 // interrupt mode..

// Alert Pin Output Polarity settings...
#define TMP_CFG_POL_ALOW                0x00 // Alert Pin active Low...
#define TMP_CFG_POL_AHIGH               0x01 // Alert Pin active High...

// Converter Resolution (Read Only Values)...
#define TMP_CFG_R10_12BITRES            0x03 // 12bit resolution...
 
// One shot/conversion ready ...
#define TMP_CFG_OS_DIS                  0x00 // continouos conversion...
#define TMP_CFG_OS_ENA                  0x01 // single conversion...

// High and Low limit registers settings...
#define TMP_CFG_TM_COMP_MODE            0x00 // compator mode ...
#define TMP_CFG_TM_INTR_MODE            0x01 // interrupt mode...

// Fault Queue settings ...
#define TMP_CFG_F_1X                    0x00 // 1x consecutive faults ...
#define TMP_CFG_F_2X                    0x01 // 2x consecutive faults ...
#define TMP_CFG_F_4X                    0x02 // 4x consecutive faults ...
#define TMP_CFG_F_6X                    0x03 // 6x consecutive faults ...

// 16bit Data Register alignment...
#define TMP_12BIT_DATA_VALUE(DATA12)    DATA12 >> 4 // shifted 4x        
#define TMP_13BIT_DATA_VALUE(DATA13)    DATA13 >> 3 // shifted 3x        

#define TMP_DEG_C_VALUE(TEMPBITDATA)    TEMPBITDATA * TMP_RESOLUTION_VALUE // temperature in C degrees


/* Temperature Conversion MACRO's */
/*
// Fahrenheit to Centigrade Temperature Conversion...
#define TEMP_C(F)   ((5.0/9.0) * (F - 32.0)) 
// Centigrade to Fahrenheit Temperature Conversion...
#define TEMP_F(C)   ((C*(9.0/5.0)) + 32.0) 
// A 1 unit temperature degree in C
#define ONE_DEG_C 1.0
// A 1 unit temperature degree in F
#define ONE_DEG_F 1.0
*/
// temperature unit in degree C
#define TEMP_UNIT_C 'C'
// temperature unit in degree C
#define TEMP_UNIT_F 'F'



// digital sensor temperature data storage...
_sLOGTMP112TEMPDATA _gsLogTMP112 __attribute__((persistent));           //set as persistent for reset recovery

// extreme temperature points global storage....
_sLOGMINMAXTEMPDATA _gsLogMinMaxTempData __attribute__((persistent));   //set as persistent for reset recovery

// EEPROM configuration control structure register...
extern __SENSITECH_CONFIGURATION_AREA  _gsSensitech_Configuration_Area; 

// EEPROM control bit manipulator structure register...
extern __sBITS_GENCFGOPT_BYTE _gsGenCfgOptByte; 

// Sensor 1 Temperature Calibration Offset...
extern __sSENSOR1_TEMP_CAL_OFFSET  _gsSensor1TempCalOffset; 

uint8_t temp_buff[20];

//-------------------------------------------------
//  16bit Temp Configuration Register Bitfields...
//-------------------------------------------------
typedef union {
    
    struct  {
        UINT16 XX   : 4;  // don't care .. LSB
        UINT16 EM   : 1;
        UINT16 AL   : 1;
        UINT16 CR   : 2;
        UINT16 SD   : 1;
        UINT16 TM   : 1;
        UINT16 POL  : 1;
        UINT16 F    : 2;
        UINT16 R    : 2;        
        UINT16 OS   : 1;  // MSB
    }Bit;
    
    _sWORDBITDATA sWCfg; // split word data into byte data...
    
    UINT16 Word;
    
}_sTEMPCONFIGREGS;




//--------------------------------------------------------------
//  Temperature Sensor Function Implementation...
//--------------------------------------------------------------




/*******************************************************************************
**  Function     : devSensor_InitControlVars
**  Description  : clears control variables...
**  PreCondition : none
**  Side Effects : clears all global vars associated to this rouitne
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void devSensor_InitControlVars(void){    
    
    // let's embed 4bit marks in the temperature data MSB location...    
    _gsLogTMP112.uwSensitechBinaryTempData = 0;
    _gsLogTMP112.uwSensorDigitalTempData = 0;
    _gsLogTMP112.fCurrentTempData = 0.0;    
    _gsLogTMP112.fTemp_C_Deg = 0.0;    
    _gsLogTMP112.fTemp_F_Deg = 0.0;   
}    

/*******************************************************************************
**  Function     : devSensor_GetFahrenheit_TempData
**  Description  : acquires Fahrenheit temperature value
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : returns formatted Fahrenheit temperature value
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
float devSensor_GetFahrenheit_TempData(void){
    return(_gsLogTMP112.fTemp_F_Deg);
}

/*******************************************************************************
**  Function     : devSensor_GetCelsius_TempData
**  Description  : acquires Celsius temperature value to display in LCD w/ 
**                 single decimal rounding off digits... 
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
float devSensor_GetCelsius_TempData(void){  
    return(_gsLogTMP112.fTemp_C_Deg);
}

/*******************************************************************************
**  Function     : devSensor_GetCurrentTempData
**  Description  : acquires current temperature value
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
float devSensor_GetCurrentTempData(void){
    // the result can be F or C ...
    return(_gsLogTMP112.fCurrentTempData);
}


/*******************************************************************************
**  Function     : devSensor_GetDigitalTempData
**  Description  : acquires TMP112 raw digital temperature data
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
UINT16 devSensor_GetDigitalTempData(void){     
    return(_gsLogTMP112.uwSensorDigitalTempData);
}


/*******************************************************************************
**  Function     : devSensor_GetSensitechBinaryTempData
**  Description  : acquires temperature data with Sensitech temp data format.
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
UINT16 devSensor_GetSensitechBinaryTempData(void){
    return(_gsLogTMP112.uwSensitechBinaryTempData);
}

/*******************************************************************************
**  Function     : devSensor_MarkSensitechTempData
**  Description  : stamps a (+) sign marking of the Sensitech temperature data in EEPROM
**  PreCondition : none
**  Side Effects : changes (+) sign of global var in Sensitech temperature. 
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void devSensor_MarkSensitechTempData(void){    
    
    // let's embbed 4bit marks in the temperature data MSB location...
    _gsLogTMP112.uwSensitechBinaryTempData |= 0xF000; 
}    


/*******************************************************************************
**  Function     : devSensor_UnMarkSensitechTempData
**  Description  : unmarks (-) sign in the Sensitech temperature EEPROM data
**  PreCondition : none
**  Side Effects : changes (-) sign of global var in Sensitech temperature. 
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void devSensor_UnMarkSensitechTempData(void){    
    
    // let's remove 4bit marks in the temperature data MSB location...
    _gsLogTMP112.uwSensitechBinaryTempData &= 0x0FFF; 
}    

/*******************************************************************************
**  Function     : devSensor_ResetI2C3
**  Description  : initializes TMP112 I2C port
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void devSensor_ResetI2C3(void){    
	
#ifdef TEMP_LEGACY_CODE
    
    Nop();
    drvI2C3_Init();    
    Nop();
    
    // 10 milli-secs delay...    
    utilDelay_ms(10);
    
    drvI2C3_StopBit();    
    
    // 10 milli-secs delay...    
    utilDelay_ms(10);
   
#endif 
}    

/*******************************************************************************
**  Function     : devSensor_Init
**  Description  : initializes TMP112 sensor registers
**  PreCondition : must initialize first the I2C port
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void devSensor_Init(void){    
    
    _sTEMPCONFIGREGS lsTempConfigRegs;

    //---------------------------------------------------------------
    // Step 1 - configure sensor....
    //---------------------------------------------------------------
    
    // initialize value to zero..
    lsTempConfigRegs.Word = 0;   
	devSensor_InitControlVars();
	
	// Setup configuration Registers...
	lsTempConfigRegs.Bit.OS  = TMP_CFG_OS_ENA;
	lsTempConfigRegs.Bit.R   = TMP_CFG_R10_12BITRES;
	lsTempConfigRegs.Bit.F   = TMP_CFG_F_2X;
	lsTempConfigRegs.Bit.POL = TMP_CFG_POL_AHIGH;
	lsTempConfigRegs.Bit.TM  = TMP_CFG_TM_COMP_MODE;
	lsTempConfigRegs.Bit.SD  = TMP_CFG_SD_ENA; // for One Shot mode...
	lsTempConfigRegs.Bit.CR  = TMP_CFG_CR_4HZ;
	lsTempConfigRegs.Bit.AL  = TMP_CFG_AL_ABOVE;
	lsTempConfigRegs.Bit.EM  = TMP_CFG_EM_12BITS; // temperature resolution...
	
	temp_buff[0] = TMP_PTR_CONFIG_REGISTER;
	temp_buff[1] = lsTempConfigRegs.sWCfg.Byte.Hi;
	temp_buff[2] = lsTempConfigRegs.sWCfg.Byte.Lo;
	
	temp_i2c_init();
	temp_i2c_write(temp_buff,3);
	


#ifdef TEMP_LEGACY_CODE

    do{
        
        drvI2C3_StartBit();            
       
        // Send Temperature Sensor Write Address...
        drvI2C3_SendByte(TMP_SENSOR_W_ADDR);
        if(drvI2C3_ReadACK(ACK_LOW) != ACK_OK) continue;            

        // Send Pointer to Configuration Register...        
        drvI2C3_SendByte(TMP_PTR_CONFIG_REGISTER);
        if(drvI2C3_ReadACK(ACK_LOW) != ACK_OK) continue;            

        // Setup configuration Registers...
        lsTempConfigRegs.Bit.OS  = TMP_CFG_OS_ENA; 
        lsTempConfigRegs.Bit.R   = TMP_CFG_R10_12BITRES;
        lsTempConfigRegs.Bit.F   = TMP_CFG_F_2X;
        lsTempConfigRegs.Bit.POL = TMP_CFG_POL_AHIGH;
        lsTempConfigRegs.Bit.TM  = TMP_CFG_TM_COMP_MODE;
        lsTempConfigRegs.Bit.SD  = TMP_CFG_SD_ENA; // for One Shot mode... 
        lsTempConfigRegs.Bit.CR  = TMP_CFG_CR_4HZ;
        lsTempConfigRegs.Bit.AL  = TMP_CFG_AL_ABOVE;
        lsTempConfigRegs.Bit.EM  = TMP_CFG_EM_12BITS; // temperature resolution...
        
        // Send MSB Byte1 Configuration Register Value...        
        drvI2C3_SendByte(lsTempConfigRegs.sWCfg.Byte.Hi);        
        if(drvI2C3_ReadACK(ACK_LOW) != ACK_OK) continue;            

        // Send LSB Byte2 Configuration Register Value...        
        drvI2C3_SendByte(lsTempConfigRegs.sWCfg.Byte.Lo);        
        if(drvI2C3_ReadACK(ACK_LOW) != ACK_OK) continue;            

        drvI2C3_StopBit();
        
        break;
        
    }while(1);    
    
#endif
}


/*******************************************************************************
**  Function     : devSensor_ReadTemp_OneShotMode
**  Description  : acquire TMP112 digital temp data in one shot mode..
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
UINT16 devSensor_ReadTemp_OneShotMode(void){

    UINT8 lubData;        
    _sWORDBITDATA lsTempData;

    _sTEMPCONFIGREGS lsTempConfigRegs;


    //---------------------------------------------------------------------------------
    // Step 1 (Write Mode) - configure sensor and set OS bit to armmed temp reading ...
    //---------------------------------------------------------------------------------
	// Setup configuration Registers...
	lsTempConfigRegs.Bit.OS  = TMP_CFG_OS_ENA;
	lsTempConfigRegs.Bit.R   = TMP_CFG_R10_12BITRES;
	lsTempConfigRegs.Bit.F   = TMP_CFG_F_2X;
	lsTempConfigRegs.Bit.POL = TMP_CFG_POL_AHIGH;
	lsTempConfigRegs.Bit.TM  = TMP_CFG_TM_INTR_MODE;
	lsTempConfigRegs.Bit.SD  = TMP_CFG_SD_ENA; // for One Shot mode...
	lsTempConfigRegs.Bit.CR  = TMP_CFG_CR_4HZ;
	lsTempConfigRegs.Bit.AL  = TMP_CFG_AL_ABOVE;
	lsTempConfigRegs.Bit.EM  = TMP_CFG_EM_12BITS; // temperature resolution...
	
	temp_buff[0] = TMP_PTR_CONFIG_REGISTER;
	temp_buff[1] = lsTempConfigRegs.sWCfg.Byte.Hi;
	temp_buff[2] = lsTempConfigRegs.sWCfg.Byte.Lo;
	temp_i2c_write(temp_buff,3);
   
#ifdef TEMP_LEGACY_CODE

    // Arm the temperature sensor for the next cycle....... 
    do{
        
        drvI2C3_StartBit();            
       
       // Send Temperature Sensor Write Address...
        drvI2C3_SendByte(TMP_SENSOR_W_ADDR);
        if(drvI2C3_ReadACK(ACK_LOW) != ACK_OK) continue;            

        // Send Pointer to Configuration Register...        
        drvI2C3_SendByte(TMP_PTR_CONFIG_REGISTER);
        if(drvI2C3_ReadACK(ACK_LOW) != ACK_OK) continue;            

        // Setup configuration Registers...
        lsTempConfigRegs.Bit.OS  = TMP_CFG_OS_ENA; 
        lsTempConfigRegs.Bit.R   = TMP_CFG_R10_12BITRES;
        lsTempConfigRegs.Bit.F   = TMP_CFG_F_2X;
        lsTempConfigRegs.Bit.POL = TMP_CFG_POL_AHIGH;
        lsTempConfigRegs.Bit.TM  = TMP_CFG_TM_INTR_MODE;        
        lsTempConfigRegs.Bit.SD  = TMP_CFG_SD_ENA; // for One Shot mode... 
        lsTempConfigRegs.Bit.CR  = TMP_CFG_CR_4HZ;
        lsTempConfigRegs.Bit.AL  = TMP_CFG_AL_ABOVE;
        lsTempConfigRegs.Bit.EM  = TMP_CFG_EM_12BITS; // temperature resolution...
        
        // Send MSB Byte1 Configuration Register Value...        
        drvI2C3_SendByte(lsTempConfigRegs.sWCfg.Byte.Hi);        
        if(drvI2C3_ReadACK(ACK_LOW) != ACK_OK) continue;            

        // Send LSB Byte2 Configuration Register Value...        
        drvI2C3_SendByte(lsTempConfigRegs.sWCfg.Byte.Lo);        
        if(drvI2C3_ReadACK(ACK_LOW) != ACK_OK) continue;            

        drvI2C3_StopBit();
        
        break;
        
    }while(1);
	
#endif    
    
    //---------------------------------------------------------------
    // Step 2 (ADC Samples)...
    //---------------------------------------------------------------    
    utilDelay_ms(60);
    
    //---------------------------------------------------------------
    // Step 3 (Read Mode) - Set the Temperature Pointer Address...
    //---------------------------------------------------------------

    // initialize value to zero..
    lsTempData.Word = 0;    
	temp_buff[0] = TMP_PTR_TEMP_REGISTER;
	temp_i2c_write(temp_buff,1);
	
#ifdef TEMP_LEGACY_CODE
    do{
        
        drvI2C3_StartBit();            
       
       // Send Temperature Sensor Write Address...
        drvI2C3_SendByte(TMP_SENSOR_W_ADDR);
        if(drvI2C3_ReadACK(ACK_LOW) != ACK_OK) continue;            

        // Send Pointer to Configuration Register...        
        drvI2C3_SendByte(TMP_PTR_TEMP_REGISTER);
        if(drvI2C3_ReadACK(ACK_LOW) != ACK_OK) continue;            

        drvI2C3_StopBit();
        
        break;
        
    }while(1);  
#endif  

    //---------------------------------------------------------------
    // Step 4 - Read the 2byte Register Temperature value....
    //---------------------------------------------------------------
    
	temp_i2c_read(temp_buff,2);
	lsTempData.Word = 0;
	lsTempData.Byte.Hi = temp_buff[0];
	lsTempData.Byte.Lo = temp_buff[1];
	
#ifdef TEMP_LEGACY_CODE
    do{
        
        drvI2C3_StartBit();            
       
        // Send Temperature Read Sensor Address...
        drvI2C3_SendByte(TMP_SENSOR_R_ADDR);
        if(drvI2C3_ReadACK(ACK_LOW) != ACK_OK) continue;            

        // Send Pointer to Configuration Register...        
        lsTempData.Word = 0;
                
        // Read MSB Byte1 Configuration Register Value...        
        drvI2C3_ReadByte(&lubData);        
        lsTempData.Byte.Hi = lubData;        
        drvI2C3_WriteACK(ACK_LOW);
        
        // Read LSB Byte2 Configuration Register Value...        
        drvI2C3_ReadByte(&lubData);        
        lsTempData.Byte.Lo = lubData;        
        drvI2C3_WriteACK(ACK_LOW);

        drvI2C3_StopBit();
        
        break;
        
    }while(1);
#endif
    
    // dummy data for testing... 
    //lsTempData.Word = 0x00C0; // force assignment debug 0.75C... mjm 10242012
    //lsTempData.Word = 0x1900; // force assignment debug 25C... mjm 10242012
    //lsTempData.Word = 0xE700; // force assignment debug -25C... mjm 10242012
    //lsTempData.Word = 0xE320; // force assignment debug -28.888886C or -20F... mjm 10242012
    //lsTempData.Word = 0x4190; // force assignment debug 65.555549C or 150F... mjm 10242012
    //lsTempData.Word = 0x6400; // force assignment debug max temp 100C or 212F... mjm 10242012
    
    // (+)    TempC / 0.0625 = Xdec = Xhex
    // (-) | -TempC / 0.0625 | = ~Xdec + 1 = Xhex
    
    //lsTempData.Word = 0x0110; // D2 D1 D0 X .. 1.06 C
    //lsTempData.Word = 0xF4B0; // D2 D1 D0 X .. 

    
    // Sensor's digital temperature binary data...
    // currently, the preset format is 12bit for compatibility issues,
    // since the marker bit were loacated at the MSB nibble...
    // Note: sensor's acquired temperature data is in C degress by default...
    //       pls not the returned data is in C degrees unformatted digital data..
    // \ mjmariveles...
    _gsLogTMP112.uwSensorDigitalTempData = TMP_12BIT_DATA_VALUE(lsTempData.Word);        
    
    //_gsLogTMP112.uwSensorDigitalTempData = 0x190;// force assignment debug 25C... mjm 10242012    
 

    // temperature resolution must coincide the 12bit format configuration...
    return(_gsLogTMP112.uwSensorDigitalTempData); 

}

/*******************************************************************************
**  Function     : devSensor_AquireFinalTemperatureValues
**  Description  : acquires temperature data and formats to Sensitech temp data 
**                 and save to EEPROM.
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : return current temperature data in either format C or F degrees
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
float devSensor_AquireFinalTemperatureValues(void){    
        
    float lfTempValue_C_Deg, lfTempValue_F_Deg;        
    BOOL lbTempLimitFlag;
    
    //-----------------------------------------------------    
    // Format the Sensor raw digital temperature data...
    // Note: its value is in C degress by default, then the
    //       calculation below were in C degress result.. 
    //-----------------------------------------------------    
    
    // convert F Deg to C Deg to check high/low limits in C as for TMP112 sensor specifications...
    lfTempValue_C_Deg = devSensor_ConvertDigitalTemp();
    
    // Since the offset were in F Degrees format then convert raw C temp to F...
    //lfTempValue_F_Deg = TEMP_F(lfTempValue_C_Deg); // original algorithm C to F formula... mjmariveles.
    
    // a C Deg temperature data was converted to F Deg directly with a single decimal format...
    lfTempValue_F_Deg = devSensor_SBRFAlgo_Round_10F();
    
    
    //-------------------------------------------------------------------------------
    // Max/Min Temperature Range Check...
    //-------------------------------------------------------------------------------
    // Note: 12bit unsigned value format max range is 4096
    //       _____________________________________________________
    //       Singed        |      -2048 ...   0  ...  +2047     |
    //       Unsigned      |        0   ............   4096     |
    //       _____________________________________________________ 
    //           Ranges    |  Int Value   |  F deg   |  C deg   |
    //       _____________________________________________________
    //        max (+)temp  | +2047 (7FFh) |  +204.7  |  +95.9   |  
    //        min (-)temp  | -2048 (FFFh) |  -204.8  |  -131.5  |
    //
    // noted by: mjmariveles
    //-------------------------------------------------------------------------------
    
    // since the default temperature acquisition value is in C then we'll use C as reference.... 
    // noted by: mjmariveles
    
    // init temp limit flag...
    lbTempLimitFlag = FALSE;
    
    // check maximum +C deg temperature limit....
    // Max Range: +95.9 C or +204.7 F
    if(lfTempValue_C_Deg >= TMP112_TEMP_DEVICE_MIN_LIMIT_C_DEG){
         lfTempValue_C_Deg = TMP112_TEMP_DEVICE_MIN_LIMIT_C_DEG;
         lbTempLimitFlag = TRUE;
    }     
    
    // check maximum -C deg temperature limit.... 
    // Min Range: -131.5 C or -204.8 F
    if(lfTempValue_C_Deg <= TMP112_TEMP_DEVICE_MAX_LIMIT_C_DEG){ 
        lfTempValue_C_Deg = TMP112_TEMP_DEVICE_MAX_LIMIT_C_DEG;
        lbTempLimitFlag = TRUE;
    }


    // check if not over limit before adding offsets otherwise skip adding offset values...
    if(lbTempLimitFlag == FALSE){
    
        //-----------------------------------------------------------------------------
        //    
        // Sensor 1 Calibration Offsets was Implemented by Eric Day 09/28/2012...
        // This offset feature must be compatible with the new TT4-MA Turbo validate
        // that can add offset..
        //
        // Procedure of adding offset:
        //  - since the offset is F degrees taken from the EEPROM then must convert
        //    the current temperature reading to F degrees prior adding offset..     
        //
        //  \mjmariveles... 10/08/2012
        //
        //-----------------------------------------------------------------------------
        
        // Add offset to F degrees equivalent of current temperature reading...
        devSensor_UpdateTempOffset(&lfTempValue_F_Deg);
        
        // after adding F Degrees offset then switch back to C Degrees format to process..
        lfTempValue_C_Deg = TEMP_C(lfTempValue_F_Deg);
        
    }

    //-------------------------------------------------------------------------------
    //  save to F deg conversion variable...
    //-------------------------------------------------------------------------------

    //-----------------------------------------------------------------------------
    // Note: After getting the final C deg temp then proceed to rounding off..
    //       Take the absolute number for rounding off tenths decimals...    
    //-----------------------------------------------------------------------------
    // Note: at  this point the temperature value in digits format is NNN.NNNN ,
    //       then round off to C Degrees with NNN.N decimal format..    
    _gsLogTMP112.fTemp_C_Deg = devSensor_RoundingOFF_Temp(lfTempValue_C_Deg);       
     
    
    //-------------------------------------------------------------------------------
    //  taking a single decimal digit rounding off value of F degrees 
    //  prior saving to EEPROM...    mjmariveles... 10052012
    //-------------------------------------------------------------------------------
    // Note: at  this point the temperature value in digits format is NNN.NNNN ,
    //       then round off to F Degrees with NNN.N decimal format..    
    _gsLogTMP112.fTemp_F_Deg = devSensor_RoundingOFF_Temp(lfTempValue_F_Deg); 
    

    //------------------------------------------------------------------------------ 
    // Formatting to Sensitech Binary Temperature Data Format in F... 
    //------------------------------------------------------------------------------ 
    _gsLogTMP112.uwSensitechBinaryTempData = devSensor_ConvertToSensitechBinaryTempData(_gsLogTMP112.fTemp_F_Deg);
    

    //------------------------------------------------------------------------------ 
    // Update all global temperature data and running totals!.. 
    //------------------------------------------------------------------------------ 
    devSensor_UpdateTempDataAndRunningTotals();


    //------------------------------------------------------------------------------    
    // Note: returned Unit will be changed depends on the Unit parameter settings...
    //------------------------------------------------------------------------------    
    
    // check desired temperature unit to be returned back!....     
    if(_gsGenCfgOptByte.GenOpt.Byte1.F_C_Unit_Select == 1){        
        // convert C to F..
        _gsLogTMP112.fCurrentTempData = _gsLogTMP112.fTemp_F_Deg;
    }
    else{
        _gsLogTMP112.fCurrentTempData = _gsLogTMP112.fTemp_C_Deg;
    }
    
    // return the final temperature value...
    return(_gsLogTMP112.fCurrentTempData);
}


/*******************************************************************************
**  Function     : devSensor_UpdateTempOffset
**  Description  : validates the offset ranges in three specified windows..
**  PreCondition : must have the F degress offset values from EEPROM
**  Side Effects : none
**  Input        : none
**  Output       : par_fTempData - reads back the temperature data in F degrees 
**                                 with offset.
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void devSensor_UpdateTempOffset(float *par_fTempData){
    
    float lfTempData_F = 0.0;
    
    lfTempData_F = *par_fTempData;

    if((lfTempData_F >= _gsSensor1TempCalOffset.fTempLevel_1_FDeg)&&(lfTempData_F < _gsSensor1TempCalOffset.fTempLevel_2_FDeg)){
        // add Sensor 1 Offset range (A) -22F to -0F...
        lfTempData_F += _gsSensor1TempCalOffset.fRangeA_FDeg;
    } 
    else if((lfTempData_F >= _gsSensor1TempCalOffset.fTempLevel_2_FDeg)&&(lfTempData_F < _gsSensor1TempCalOffset.fTempLevel_3_FDeg)){
        // add Sensor 1 Offset range (B) +0F to +122F...
        lfTempData_F += _gsSensor1TempCalOffset.fRangeB_FDeg;    
    }
    else if((lfTempData_F >= _gsSensor1TempCalOffset.fTempLevel_3_FDeg)&&(lfTempData_F <= _gsSensor1TempCalOffset.fTempLevel_4_FDeg)){
        // add Sensor 1 Offset range (C) +122F to +158F...    
        lfTempData_F += _gsSensor1TempCalOffset.fRangeC_FDeg;
    }
    
    *par_fTempData = lfTempData_F;
    
}

/*******************************************************************************
**  Function     : devSensor_UpdateTempDataAndRunningTotals
**  Description  : calculates running totals 
**  PreCondition : must have first the previous temperature values to calculate 
**                 running totals data..
**  Side Effects : updates global temperature structure data.
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void devSensor_UpdateTempDataAndRunningTotals(void){

    //------------------------------------------------------------------------------    
    // get the Extreme Minimum and Maximum Temperature data....
    //------------------------------------------------------------------------------    
    if(_gsLogMinMaxTempData.ubPrevTempDataFlag == 1){
    
        // log maximum temp data...
        if(_gsLogTMP112.fTemp_F_Deg > _gsLogMinMaxTempData.fExtremeHighTemp_F_Deg){    
           _gsLogMinMaxTempData.fExtremeHighTemp_C_Deg = _gsLogTMP112.fTemp_C_Deg;
           _gsLogMinMaxTempData.fExtremeHighTemp_F_Deg = _gsLogTMP112.fTemp_F_Deg;
           _gsLogMinMaxTempData.uwExtremeHighDpNum = _gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints;
        }

        // log minimum temp data...
        if(_gsLogTMP112.fTemp_F_Deg < _gsLogMinMaxTempData.fExtremeLowTemp_F_Deg){    
           _gsLogMinMaxTempData.fExtremeLowTemp_C_Deg = _gsLogTMP112.fTemp_C_Deg;
           _gsLogMinMaxTempData.fExtremeLowTemp_F_Deg = _gsLogTMP112.fTemp_F_Deg;
           _gsLogMinMaxTempData.uwExtremeLowDpNum = _gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints;
        }
    }
    else{
        _gsLogMinMaxTempData.fExtremeHighTemp_C_Deg = _gsLogMinMaxTempData.fExtremeLowTemp_C_Deg = _gsLogTMP112.fTemp_C_Deg;  
        _gsLogMinMaxTempData.fExtremeHighTemp_F_Deg = _gsLogMinMaxTempData.fExtremeLowTemp_F_Deg = _gsLogTMP112.fTemp_F_Deg;  
        _gsLogMinMaxTempData.uwExtremeHighDpNum = _gsLogMinMaxTempData.uwExtremeLowDpNum = 0;
        _gsLogMinMaxTempData.fAveTemp_C_Deg = 0;
        _gsLogMinMaxTempData.fAveTemp_F_Deg = 0;
        _gsLogMinMaxTempData.fRunningTotalTemp_C_Deg = 0;
        _gsLogMinMaxTempData.fRunningTotalTemp_F_Deg = 0;
        _gsLogMinMaxTempData.ubPrevTempDataFlag = 1;
    }
    
    
    //------------------------------------------------------------------------------    
    // accumulate Temperature running totals and calculate the average temp....
    //------------------------------------------------------------------------------        
    _gsLogMinMaxTempData.fRunningTotalTemp_C_Deg += (long double)_gsLogTMP112.fTemp_C_Deg;
    _gsLogMinMaxTempData.fRunningTotalTemp_F_Deg += (long double)_gsLogTMP112.fTemp_F_Deg; // F deg is the major UNIT!...mjm
    
    if(_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints>0){
        _gsLogMinMaxTempData.fAveTemp_C_Deg = _gsLogMinMaxTempData.fRunningTotalTemp_C_Deg / (long double)(_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints+1);
        _gsLogMinMaxTempData.fAveTemp_F_Deg = _gsLogMinMaxTempData.fRunningTotalTemp_F_Deg / (long double)(_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints+1);
    }
    else{
        _gsLogMinMaxTempData.fAveTemp_C_Deg = (long double)_gsLogTMP112.fTemp_C_Deg;
        _gsLogMinMaxTempData.fAveTemp_F_Deg = (long double)_gsLogTMP112.fTemp_F_Deg;
    }
        
}


/*******************************************************************************
**  Function     : devSensor_GetTemp
**  Description  : converts raw TMP112 temp data to formatted value in C degrees
**  PreCondition : must get a raw temperature data from TMP112 sensor device 
**  Side Effects : none
**  Input        : none
**  Output       : returns formatted TMP112 digital temperature data...
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
float devSensor_ConvertDigitalTemp(void){
    
    float lfTempValue;    
    UINT16 luwBitValue;            
    _sWORDBITDATA _sluwData;
    

    //-----------------------------------------------------    
    // Format the Sensor raw digital temperature data...
    // Note: its value is in C degress by default, then the
    //       calculation below were in C degress result.. 
    //-----------------------------------------------------    

    // acquire the raw temperature data bitcount value...  
    _sluwData.Word = _gsLogTMP112.uwSensorDigitalTempData;    
    
    // check the 12th significant bit if temperature data is negative...
    // from 0 - 11 bit, 11th is the 12th bit...
    if(_sluwData.Bit.D11 == 1){
        
        // as per procedure must subtract 1 then negate for two's complement...
        // to acquire the negative temperature value..
        luwBitValue = ~(_sluwData.Word - 1) & 0x07FF;
    
        // calculate positive temperature reading here!..   
        // temperature sensor default unit is in C degree..
        lfTempValue = (float)(luwBitValue) * TMP_RESOLUTION_VALUE * -1.0;    
    }
    else{
        
        // calculate positive temperature reading here!..   
        // temperature sensor default unit is in C degree..
        lfTempValue = (float)(_sluwData.Word) * TMP_RESOLUTION_VALUE;
    }
    
    return(lfTempValue);

}


/*******************************************************************************
**  Function     : devSensor_SBRFAlgo_Round_10F
**  Description  : implementing SBRF Round 10F algorithm. 
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : returns formatted TMP112 digital temperature data...
** _____________________________________________________________________________
**  Date    Author                Changes
**  120312  Michael J. Mariveles  1st release.    
*******************************************************************************/

float devSensor_SBRFAlgo_Round_10F(void){
    
    float lfTempReading;    
    INT16 _temp_reading;
    INT32 temp;
    
    _sWORDBITDATA _sluwData;

    //-----------------------------------------------------    
    //
    //  if (read_data[0] & 0x80)		// Negative number
	//  _temp_reading = ~(0xF000 | _temp_reading) + 1;
    //  TMP112 provides 0.0625 / count. So cur_meas_data * 0.0625
    //  is measurement in Celsius.
    //  Let, C = count * 0.0625 >= 0
    //  Then, Fahrenheit, F = C * 9/5 + 32
    //  Then, 100F		 = 100 * [C * 9/5 + 32]
    //	 =>   100F		 = 100 * [(count * 5 / 80) * 9 / 5 + 32]
    // 	 =>   100F		 = count * 90 / 8 + 3200
    //   =>	 round(10F)	 = (count * 90 / 8 + 3200 + 5) / 10
    //
    // temp = _temp_reading;
    // temp = (temp * 90) / 8;
    // if (read_data[0] & 0x80)	// negative number
    //           	temp *= -1;
    // _temp_reading = (temp + 3205) / 10;
    //
    // Note by: Micheal 12/012012,  from Eric Day's SBRF algorithm document...    	
    //
    //-----------------------------------------------------    

    // acquire the raw temperature data bitcount value...  
    _sluwData.Word = _gsLogTMP112.uwSensorDigitalTempData;    
    
    _temp_reading = _gsLogTMP112.uwSensorDigitalTempData;    
    
    // check the 12th significant bit if temperature data is negative...
    // if 11th bit is high then the number is negative...
    if(_sluwData.Bit.D11 == 1) _temp_reading = ~(0xF000 | _temp_reading) + 1;
    
    temp = _temp_reading;
    temp = (temp * 90) / 8;
    
    // check the 12th significant bit if temperature data is negative...
    // if 11th bit is high then the number is negative...
    if(_sluwData.Bit.D11 == 1) temp *= -1;
    
    _temp_reading = (temp + 3205) / 10; // 10F round
    
    // final temperature F degrees....
    lfTempReading = (float)_temp_reading / 10.0;
    
    return(lfTempReading);
    
}

/*******************************************************************************
**  Function     : devSensor_RoundingOFF
**  Description  : rounds-off temperature value into single decimal digit
**  PreCondition : none
**  Side Effects : none
**  Input        : par_fRealValue - formatted temperature with real number. 
**  Output       : returns rounded - off temperature data into a single decimal digit
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
float devSensor_RoundingOFF_Temp(float par_fRealValue){

    register float lfRoundOff;
    register float lfFixedDigitValue;    
    register INT16 lwFixedDigitValue;
    BOOL lbNegValFlag;
    
    //-----------------------------------------------------------------------------
    // Note: After getting the final C deg temp then proceed to rounding off..
    //       Take the absolute number for rounding off tenths decimals...    
    //-----------------------------------------------------------------------------
    
    
    if(par_fRealValue < 0){
        par_fRealValue *= -1.0;
        lbNegValFlag = TRUE;
    }
    else{
        lbNegValFlag = FALSE;
    }
    
    // truncate C deg to single decimal digit...    
    lwFixedDigitValue = (INT16)(par_fRealValue * 10.0);    
    lfFixedDigitValue = par_fRealValue * 10.0;    
    // get the decimal difference...
    lfRoundOff = lfFixedDigitValue - (float)lwFixedDigitValue;
    // do the rounding if greather than 0.5;
    if(lfRoundOff >= 0.5) lwFixedDigitValue++;
    lfFixedDigitValue = lwFixedDigitValue;
    lfFixedDigitValue /= 10.0;
    // rounded off temperature value...
    par_fRealValue = lfFixedDigitValue;
    
    // if negative is true then revert to its original value...
    if(lbNegValFlag == TRUE) par_fRealValue *= -1.0;
    
    return(par_fRealValue);

}

/*
float devSensor_DoubleRoundingOFF_Temp(float par_fRealValue){

    //register float lfRoundOff;
    
    register INT32 ldDigit1;
    register INT32 ldDigit2;
    register float lfDigit1;
    register float lfDigit2;
    register INT32 ldRound1;
    register float lfRound1;
    register INT32 ldRound2;
    register float lfRound2;
    float lfFinalRoundValue;

    
    //-----------------------------------------------------------------------------
    // Note: After getting the final C deg temp then proceed to rounding off..
    //       Take the absolute number for rounding off tenths decimals...    
    //
    // \mjmariveles 12032012...
    //-----------------------------------------------------------------------------
    
    //lfRoundOff =  -21.8499;    
    
    ldRound1 = (INT32)(par_fRealValue * 1000.0);
    ldDigit1 = ldRound1;
    
    lfDigit1 = (float)ldDigit1 / 10.0;    
    ldDigit2 = (INT32)lfDigit1;
    lfDigit2 = lfDigit1 - ldDigit2;
    
    if(par_fRealValue<0){
        if(lfDigit2<= -0.5) --ldRound1;
    }
    else{
        if(lfDigit2>= 0.5) ++ldRound1;
    }    
    
    lfRound1 = (float)ldRound1 / 100.0;
    ldRound2 = (INT32)lfRound1;
    lfRound2 = lfRound1 - ldRound2;
 
    if(par_fRealValue<0){
        if(lfDigit2<= -0.5) --ldRound2;
    }
    else{
        if(lfDigit2>= 0.5) ++ldRound2;
    }    
    
    lfFinalRoundValue = ldRound2 / 10.0;
    
    return(lfFinalRoundValue);

}
*/

/*******************************************************************************
**  Function     : devSensor_ConvertToSensitechBinaryTempData
**  Description  : converts TMP112 temp data to Sensitech binary temp data.
**  PreCondition : none
**  Side Effects : none
**  Input        : par_fFahrenheitValue - TMP112 formatted temp datain F degrees
**  Output       : returns binary Sensitech temp data
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
UINT16 devSensor_ConvertToSensitechBinaryTempData(float par_fFahrenheitValue){
    
    UINT16 luwBitValue;
    float fFahrenheitValue;

    fFahrenheitValue = par_fFahrenheitValue;
    
    if( fFahrenheitValue >= 0 ){

        // scaled up to 10 to preserve the tenths of a degree...
        fFahrenheitValue *= 10.0; 
        
        luwBitValue = (UINT16)fFahrenheitValue; 
    }
    else{
        
        // scaled up to 10 to preserve the tenths of a degree...
        // get the absolute value by eliminating the negative sign...
        fFahrenheitValue *= -10.0; 
        
        // convert to integer...
        luwBitValue = (UINT16)fFahrenheitValue;         
        
        luwBitValue = ~luwBitValue + 1; // negate for 2's complement..
    }

    // masked the 12 bit!...
    luwBitValue &= 0x0FFF;
    
    return(luwBitValue);
    
}
/* End Of File */


