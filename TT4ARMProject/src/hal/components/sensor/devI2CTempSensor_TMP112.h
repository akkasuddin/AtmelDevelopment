/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm                                            **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc. and Blue Ocean Innovation Ltd.                            **
 **                                                                           **
 **  Copryright(c) 2010. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Blue Ocean Innovation Ltd.                        **
 **  Project#            :  449                                               **
 **  Creation date       :  08/20/2010                                        **
 **  Programmer          :  Michael J. Mariveles                              **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : devI2CTempSensor_TMP112.h                                         **
 **                                                                           **
 **  Description :                                                            **
 **    - external digital temperature sensor device driver.                   **
 **                                                                           **
 ******************************************************************************/
 
#ifndef __DEVI2CTEMPSENSOR_TMP112_H
#define __DEVI2CTEMPSENSOR_TMP112_H


/* Temperature Conversion MACRO's */

// Fahrenheit to Centigrade Temperature Conversion...
#define TEMP_C(F)   ((5.0/9.0) * (F - 32.0)) 
// Centigrade to Fahrenheit Temperature Conversion...
#define TEMP_F(C)   ((C*(9.0/5.0)) + 32.0) 
// A 1 unit temperature degree in C
#define ONE_DEG_C 1.0
// A 1 unit temperature degree in F
#define ONE_DEG_F 1.0


//-------------------------------------------------
//  Sensor 1 Temperature Calibration Offsets...
//
//  This Offset ranges was defiend by Eric Day 
//  dated 09/28/2012 email...
//
// Defined Ranges in F deg:
//
// Sensor 1 Offset range (A) -22F to -0F...
// Sensor 1 Offset range (B) +0F to +122F...
// Sensor 1 Offset range (C) +122F to +158F...
//
//-------------------------------------------------
#define RANGE_LEVEL_1   -22.0 
#define RANGE_LEVEL_2   0.0
#define RANGE_LEVEL_3   122.0
#define RANGE_LEVEL_4   158.0

//-------------------------------------------------
// TMP112 Device Temperature max/min limits...
//-------------------------------------------------
#define TMP112_TEMP_DEVICE_MIN_LIMIT_C_DEG   95.9 // Max Range: +95.9 C or +204.7 F
#define TMP112_TEMP_DEVICE_MAX_LIMIT_C_DEG -131.5 // Min Range: -131.5 C or -204.8 F


//-----------------------------------
//  Temperature data Information...
//-----------------------------------
typedef struct {
    UINT16  uwSensorDigitalTempData;   // Sensor's Binary format Temperature Data...
    UINT16  uwSensitechBinaryTempData; // Sensitech's Binary format Temperature Data...
    float   fCurrentTempData;    
    float   fTemp_C_Deg;
    float   fTemp_F_Deg;    
} _sLOGTMP112TEMPDATA;

typedef struct {
    UINT8   ubPrevTempDataFlag;    
    long double fRunningTotalTemp_C_Deg;
    long double fRunningTotalTemp_F_Deg;
    long double fAveTemp_C_Deg; 
    long double fAveTemp_F_Deg; 
    float   fExtremeHighTemp_C_Deg;
    float   fExtremeLowTemp_C_Deg;
    float   fExtremeHighTemp_F_Deg;
    float   fExtremeLowTemp_F_Deg;    
    UINT16  uwExtremeLowDpNum;
    UINT16  uwExtremeHighDpNum;
} _sLOGMINMAXTEMPDATA;    


void devSensor_Init(void);
UINT16 devSensor_ReadTemp(void);
UINT16 devSensor_ReadTemp_OneShotMode(void);
void devSensor_ResetI2C3(void);
float devSensor_BitCountToCDegTemp(UINT16 par_uwBitCountData);
void devSensor_PowerDown(void);
void devSensor_InitControlVars(void);
float devSensor_GetCurrentTempData(void);
float devSensor_GetCelsius_TempData(void);
float devSensor_GetFahrenheit_TempData(void);
float devSensor_AquireFinalTemperatureValues(void);
UINT16 devSensor_GetSensitechBinaryTempData(void);
UINT16 devSensor_GetDigitalTempData(void);
UINT16 devSensor_ConvertToSensitechBinaryTempData(float par_fFahrenheitValue);
void devSensor_UnMarkSensitechTempData(void);
void devSensor_MarkSensitechTempData(void);
float devSensor_RoundingOFF_Temp(float par_fRealValue);
float devSensor_ConvertDigitalTemp(void);
void devSensor_UpdateTempDataAndRunningTotals(void);
void devSensor_UpdateTempOffset(float *par_fTempData);

float devSensor_SBRFAlgo_Round_10F(void); 
//float devSensor_DoubleRoundingOFF_Temp(float par_fRealValue); // commented out by Michael.. 12032012

#endif /* __DEVI2CTEMPSENSOR_TMP112_H */
