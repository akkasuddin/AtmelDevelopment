/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm Variant                                    **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc.															  **
 **                                                                           **
 **  Copryright(c) 2015. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Relisource Technology Ltd.						  **
 **  Creation date       :  2/27/2015 1:28:10 AM                              **
 **  Programmer          :  Faijur Rahman									  **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : devButton.c                                                       **
 **                                                                           **
 **  Description :                                                            **
 **    - Routines related to buttons							              **
 **                                                                           **
 ******************************************************************************/
 
#include "devButton.h"
#include "TT4USB_Board.h"

static void Start_Btn_Handler(uint32_t id, uint32_t mask)
{
	if(btn_callback[START_BUTTON_ID] != NULL)
	{
		btn_callback[START_BUTTON_ID](); //Call registered callback function
	}
}

static void Stop_Btn_Handler(uint32_t id, uint32_t mask)
{
	if(btn_callback[STOP_BUTTON_ID] != NULL)
	{
		btn_callback[STOP_BUTTON_ID](); //Call registered callback function
	}
}

void devRegister_Button_Callback(uint8_t id, btn_calback_t callback)
{
	if(id < MAX_BUTTON_ID)
	{
		btn_callback[id] = callback;	
	}	
}

void devUnRegister_Button_Callback(uint8_t id)
{
	if(id < MAX_BUTTON_ID)
	{
		btn_callback[id] = NULL;
	}
}

void devButton_Init(void)
{
	// Configure Start Button
#ifdef CONF_BOARD_START_BUTTON
	pmc_enable_periph_clk(START_BTN_PIO_ID);
	ioport_set_pin_dir(START_BTN_PIN, IOPORT_DIR_INPUT);
	ioport_set_pin_mode(START_BTN_PIN, IOPORT_MODE_PULLUP);
	pio_set_debounce_filter(START_BTN_PIO, START_BTN_MASK, BTN_DEBOUNCE_DLY);
	pio_handler_set(START_BTN_PIO, START_BTN_PIO_ID, START_BTN_MASK, START_BTN_ATTR, Start_Btn_Handler);// Interrupt on rising edge
	NVIC_EnableIRQ((IRQn_Type) START_BTN_PIO_ID);
	pio_handler_set_priority(START_BTN_PIO, (IRQn_Type) START_BTN_PIO_ID, BTN_IRQ_PRIOR_PIO);
	pio_enable_interrupt(START_BTN_PIO, START_BTN_MASK);
#endif

	// Configure Stop Button
#ifdef CONF_BOARD_STOP_BUTTON
	pmc_enable_periph_clk(STOP_BTN_PIO_ID);
	ioport_set_pin_dir(STOP_BTN_PIN, IOPORT_DIR_INPUT);
	ioport_set_pin_mode(STOP_BTN_PIN, IOPORT_MODE_PULLUP);
	pio_set_debounce_filter(STOP_BTN_PIO, STOP_BTN_MASK, BTN_DEBOUNCE_DLY);
	pio_handler_set(STOP_BTN_PIO, STOP_BTN_PIO_ID, STOP_BTN_MASK, STOP_BTN_ATTR, Stop_Btn_Handler);// Interrupt on rising edge
	NVIC_EnableIRQ((IRQn_Type) STOP_BTN_PIO_ID);
	pio_handler_set_priority(STOP_BTN_PIO, (IRQn_Type) STOP_BTN_PIO_ID, BTN_IRQ_PRIOR_PIO);
	pio_enable_interrupt(STOP_BTN_PIO, STOP_BTN_MASK);
#endif
	
	for(int i=0; i<MAX_BUTTON_ID; i++)
	{
		btn_callback[STOP_BUTTON_ID] = NULL; //Initialize the callback array
	}
}