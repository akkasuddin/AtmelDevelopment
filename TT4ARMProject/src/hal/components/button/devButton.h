/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm Variant                                    **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc.															  **
 **                                                                           **
 **  Copryright(c) 2015. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Relisource Technology Ltd.						  **
 **  Creation date       :  2/27/2015 1:28:10 AM                              **
 **  Programmer          :  Faijur Rahman									  **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : devButton.h                                                       **
 **                                                                           **
 **  Description :                                                            **
 **    - Routines related to buttons							              **
 **                                                                           **
 ******************************************************************************/

#ifndef DEV_BUTTON_H_
#define DEV_BUTTON_H_

#include "stdint-gcc.h"
#include "stdbool.h"

#define BTN_IRQ_PRIOR_PIO 0
#define BTN_DEBOUNCE_DLY  10

enum Button_ID
{
	START_BUTTON_ID,
	STOP_BUTTON_ID,	
	MAX_BUTTON_ID
};
typedef void(* btn_calback_t)(void);
volatile btn_calback_t btn_callback[MAX_BUTTON_ID];//Array of button functions pointers

void devButton_Init(void);
void devUnRegister_Button_Callback(uint8_t id);
void devRegister_Button_Callback(uint8_t id, btn_calback_t callback);

#endif /* DEV_BUTTON_H_ */