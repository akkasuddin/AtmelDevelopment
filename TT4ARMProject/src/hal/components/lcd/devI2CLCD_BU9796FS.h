/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm                                            **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc. and Blue Ocean Innovation Ltd.                            **
 **                                                                           **
 **  Copryright(c) 2010. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Blue Ocean Innovation Ltd.                        **
 **  Project#            :  449                                               **
 **  Creation date       :  08/20/2010                                        **
 **  Programmer          :  Michael J. Mariveles                              **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : devI2CLCD_BU9796FS.h                                              **
 **                                                                           **
 **  Description :                                                            **
 **    - external LCD IC device driver.                                       **
 **                                                                           **
 ******************************************************************************/
 
#ifndef __DEVI2CLCD_BU9796FS_H
#define __DEVI2CLCD_BU9796FS_H

#include "types.h"

#define ON_SEGS     0xFF
#define OFF_SEGS    0x00

//------------------------------
//  LCD Segment Bitfields...
//------------------------------
typedef union {
    
    struct  {
        UINT8 D0 : 1;  // Bit0      LSB
        UINT8 D1 : 1;  // Bit1
        UINT8 D2 : 1;  // Bit2
        UINT8 D3 : 1;  // Bit3
        UINT8 D4 : 1;  // Bit4
        UINT8 D5 : 1;  // Bit5
        UINT8 D6 : 1;  // Bit6
        UINT8 D7 : 1;  // Bit7      MSB
    }Bit;
    
    UINT8 Byte;
    
}_sBYTEBITS;

typedef union {
    
    struct  {
        DWORD D0    : 4;  //  Group SEG3      LSB
        DWORD D1    : 4;  //  Group SEG2
        DWORD D2    : 4;  //  Group SEG1
        DWORD D3    : 4;  //  Group SEG0
        DWORD Sign  : 1;  //  (-) or (+) sign temp value..
        DWORD X0    : 3;  
        DWORD X3    : 4;  
        DWORD X1    : 4;
        DWORD X2    : 4;  // Nibble MSB        
    }BCD;
    
    struct  {
        UINT16 Value;   // LSB
        UINT16 Sign;    // MSB
    }wData;

    DWORD DWord;
    
}_s5DIGITBCD;


void devLCD_Init(void);
void devLCD_UpdateDisplay(void);
void devLCD_InitSEGData(void);
void devLCD_SetDisp_1_7SEG_0(UINT8 par_ubOnOff);
void devLCD_SetDisp7SEG_1(UINT8 par_ubNum, UINT8 par_ubOnOff);
void devLCD_SetDisp7SEG_2(UINT8 par_ubNum, UINT8 par_ubOnOff);
void devLCD_SetDisp7SEG_3(UINT8 par_ubNum, UINT8 par_ubOnOff);
void devLCD_SetDisp7SEG_4(UINT8 par_ubNum, UINT8 par_ubOnOff);
_s5DIGITBCD devLCD_DEC_To_BCD(UINT16 par_uwDecVal);
void devLCD_Get7SEGDecoder( _sBYTEBITS *par_spSetToSeg,
                            UINT8 par_ubNum, 
                            UINT8 par_ubOnOff);
void devLCD_DisplayALL_SegON(void);
void devLCD_DisplayALL_SegOFF(void);
void devLCD_Display_SwVersion(void);
void devLCD_Display_ReadTemperature(void);
_s5DIGITBCD devLCD_FinalTemperatureValueToBCD(float par_fTempValue);
void devLCD_Set7SegDisplay_BCD(_s5DIGITBCD ls5DigitBCD);
void devLCD_Set7SegDisplay_Full_BCD(_s5DIGITBCD ls5DigitBCD);
void devLCD_SetUnitIcon(UINT8 par_ubOnSeg);
void devLCD_Display_ArrowMark(UINT8 par_ubBitFlag);
void devLCD_Display_AllSegments(UINT8 par_ubLCDAllSegOnOff);
void devLCD_Display_StopAlarmDevice(UINT8 par_ubStop);
void devLCD_AllSegBlinkOn(void);
void devLCD_AllSegBlinkOff(void);
void devLCD_Display_RunningIcon(void);
UINT8 devLCD_Display_Summary(void);
void devLCD_Display_Firmware_Update(void);
void devLCD_Display_StartUPIcon(UINT8 par_ubFlag);
void devLCD_Display_Boot(UINT8 par_ubStatus);
void devLCD_Display_USB(UINT8 par_ubStatus);
void devLCD_Display_CF(UINT8 par_ubStatus);
void devLCD_Display_CFg(UINT8 par_ubStatus);
void devLCD_Display_SEt(UINT8 par_ubStatus);
void devLCD_Display_RemainingElapse_Alarm(void);
void devLCD_Display_RemainingElapseTime(UINT32 par_udwTimeInSecs);
void devLCD_Display_ElapseThreshold_Alarm(void);
void devLCD_Display_CyclingTriggeredAlarm(void);
UINT8 devLCD_Display_HeartBeat(UINT8 par_ubSegOn);
void devLCD_Display_SystemRunningIcons(void);
void devLCD_GetBlinkStatus(void);

#endif /* __DEVI2CLCD_BU9796FS_H */
