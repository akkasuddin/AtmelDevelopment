/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm                                            **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc. and Blue Ocean Innovation Ltd.                            **
 **                                                                           **
 **  Copryright(c) 2010. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Blue Ocean Innovation Ltd.                        **
 **  Project#            :  449                                               **
 **  Creation date       :  08/20/2010                                        **
 **  Programmer          :  Michael J. Mariveles                              **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : devI2CLCD_BU9796FS.c                                              **
 **                                                                           **
 **  Description :                                                            **
 **    - external LCD IC device driver.                                       **
 **                                                                           **
 ******************************************************************************/

//#include "hwPIC24F.h"
#include "drvI2C.h"
#include "drvI2C_Wrapper.h"
#include "devI2CLCD_BU9796FS.h"
#include "types.h"
#include "devI2CTempSensor_TMP112.h"
#include "utility.h"
#include "commTT4Config.h"




//----------------------------------------------
// 7 segment display specifications...
//----------------------------------------------
#define TOTAL_DIGITS            0x04
#define TOTAL_DECIMAL_PLACEMENT 0x03
#define TENS_DIGIT_MAX          1000u
#define TENS_DIGIT_DIVISOR      10u

//----------------------------------------------
//  Bit Setting for Command or write to RAM  
//----------------------------------------------
#define LCD_SLAVE_ADDR          0x7C // LCD slave address
#define LCD_RAM                 0x00 // Next data will be written to DDRAM..  
#define LCD_CMD                 0x01 // Comamnd Data

//------------------------------
//  Set LCD Display Mode 1...
//------------------------------
#define DISCTL_PWRFR_NORMMODE   0x00 // Normal Mode
#define DISCTL_PWRFR_PWRMODE1   0x01 // level 1 current consumption  
#define DISCTL_PWRFR_PWRMODE2   0x02 // level 2 current consumption
#define DISCTL_PWRFR_PWRMODE3   0x03 // level 3current consumption
#define DISCTL_LCDDRV_LI        0x00 // Line inversion
#define DISCTL_LCDDRV_FI        0x01 // Frame inversion
#define DISCTL_PWRSR_PWRMODE1   0x00 // LCD contrast level 1 
#define DISCTL_PWRSR_PWRMODE2   0x01 // LCD contrast level 2
#define DISCTL_PWRSR_NORMMODE   0x02 // LCD contrast level 3
#define DISCTL_PWRSR_PWRHIGH    0x03 // LCD contrast level 4
#define DISCTL_PRESET           0x01 // DISCTL register preset value ... 
#define LCD_CMD_DISCTL(CMD0,PWRFR,LCDDRV,PWRSR)        (CMD0<<7)|(DISCTL_PRESET<<5)|(PWRFR<<3)|(LCDDRV<<2)|(PWRSR)

//------------------------------
//  Set LCD Drive Mode...
//------------------------------
#define MODESET_DISP_OFF        0x00 // Display ON
#define MODESET_DISP_ON         0x01 // Display OFF
#define MODESET_BIAS_1_3        0x00 // 1/3 Bias
#define MODESET_BIAS_1_2        0x01 // 1/2 Bias
#define MODESET_PRESET          0x04 // MODESET register preset value ...  
#define LCD_CMD_MODESET(CMD1,DISPON,SETBIAS)           (CMD1<<7)|(MODESET_PRESET<<4)|(DISPON<<3)|(SETBIAS<<2)

//------------------------------
//  Set LCD Display Mode 2...
//------------------------------
#define ADSET_SEG0              0x00 // starting address..
#define LCD_CMD_ADSET(CMD2,ADDRESS)                    (CMD2<<7)|ADDRESS              

//------------------------------
//  Set IC Operation..
//------------------------------
#define ICSET_SWRES_NOP         0x00 // no SW reset operation
#define ICSET_SWRES_EXEC        0x01 // execute SW reset..
#define ICSET_OSCMODE_INTRN     0x00 // use internal OSC..
#define ICSET_OSCMODE_EXTRN     0x01 // use external OSC..
#define ICSET_PRESET            0x1A // ICSET register preset value ...  
#define LCD_CMD_ICSET(CMD3,SWRES,OSCMODE)              (CMD3<<7)|(ICSET_PRESET<<2)|(SWRES<<1)|OSCMODE

//------------------------------
//  Set Blink Mode...
//------------------------------
#define BLKCTL_BLINK_OFF        0x00 // blink OFF
#define BLKCTL_BLINK_05HZ       0x01 // blinking freq @ 0.5Hz or 2secs interval..
#define BLKCTL_BLINK_1HZ        0x02 // blinking freq @ 1Hz or 1sec interval..
#define BLKCTL_BLINK_2HZ        0x03 // blinking freq @ 2Hz or 0.5secs interval..
#define BLKCTL_PRESET           0x1C // BLKCTL register preset value ... 
#define LCD_CMD_BLKCTL(CMD4,BLINKHZ)                   (CMD4<<7)|(BLKCTL_PRESET<<2)|BLINKHZ

//------------------------------
//  Set Pixel Condition...
//------------------------------
#define APCTL_APON_NORM         0x00 // all display set normal
#define APCTL_APON_ALLPIXON     0x01 // all pixel ON
#define APCTL_APOFF_NORM        0x00 // all display set normal
#define APCTL_APOFF_ALLPIXOFF   0x01 // all pixel OFF
#define APCTL_PRESET            0x1F // APCTL register preset value ... 
#define LCD_CMD_APCTL(CMD5,APON,APOFF)                 (CMD5<<7)|(APCTL_PRESET<<2)|(APON<<1)|APOFF




//---------------------------------------------
// LCD Segments RAM data storage registers...
//---------------------------------------------
_sBYTEBITS gubLCDSEGData[10];

//---------------------------------------------
// LCD Segment On/Off Control...
//---------------------------------------------
#define SEG_ON                 0x01 // SEG BIT HIGH = ON
#define SEG_OFF                0x00 // SEG BIT LOW = OFF

//--------------------------------------
//  LCD Raw Data Segment Bit Mapping...
//--------------------------------------
// LCD PAD1 - SEG0
#define  SEG0_1A        gubLCDSEGData[0].Bit.D7    // COM0
#define  SEG0_1B        gubLCDSEGData[0].Bit.D6    // COM1
#define  SEG0_1C        gubLCDSEGData[0].Bit.D5    // COM2
#define  SEG0_X3        gubLCDSEGData[0].Bit.D4    // COM3
// LCD PAD2 - SEG1
#define  SEG1_2A        gubLCDSEGData[0].Bit.D3    // COM0
#define  SEG1_2B        gubLCDSEGData[0].Bit.D2    // COM1
#define  SEG1_2C        gubLCDSEGData[0].Bit.D1    // COM2
#define  SEG1_X3        gubLCDSEGData[0].Bit.D0    // COM3
// LCD PAD3 - SEG2
#define  SEG2_3A        gubLCDSEGData[1].Bit.D7    // COM0
#define  SEG2_3B        gubLCDSEGData[1].Bit.D6    // COM1
#define  SEG2_3C        gubLCDSEGData[1].Bit.D5    // COM2
#define  SEG2_X3        gubLCDSEGData[1].Bit.D4    // COM3
// LCD PAD4 - SEG3
#define  SEG3_4A        gubLCDSEGData[1].Bit.D3    // COM0
#define  SEG3_4B        gubLCDSEGData[1].Bit.D2    // COM1
#define  SEG3_4C        gubLCDSEGData[1].Bit.D1    // COM2
#define  SEG3_X3        gubLCDSEGData[1].Bit.D0    // COM3
// LCD PAD5 - SEG4
#define  SEG4_5A        gubLCDSEGData[2].Bit.D7    // COM0
#define  SEG4_5B        gubLCDSEGData[2].Bit.D6    // COM1
#define  SEG4_5C        gubLCDSEGData[2].Bit.D5    // COM2
#define  SEG4_X3        gubLCDSEGData[2].Bit.D4    // COM3
// LCD PAD6 - SEG5
#define  SEG5_6A        gubLCDSEGData[2].Bit.D3    // COM0
#define  SEG5_6B        gubLCDSEGData[2].Bit.D2    // COM1
#define  SEG5_6C        gubLCDSEGData[2].Bit.D1    // COM2
#define  SEG5_X3        gubLCDSEGData[2].Bit.D0    // COM3
// LCD PAD7 - SEG6
#define  SEG6_7A        gubLCDSEGData[3].Bit.D7    // COM0
#define  SEG6_7B        gubLCDSEGData[3].Bit.D6    // COM1
#define  SEG6_7C        gubLCDSEGData[3].Bit.D5    // COM2
#define  SEG6_X3        gubLCDSEGData[3].Bit.D4    // COM3
// LCD PAD8 - SEG7
#define  SEG7_8A        gubLCDSEGData[3].Bit.D3    // COM0
#define  SEG7_8B        gubLCDSEGData[3].Bit.D2    // COM1
#define  SEG7_8C        gubLCDSEGData[3].Bit.D1    // COM2
#define  SEG7_X3        gubLCDSEGData[3].Bit.D0    // COM3
// LCD PAD9 - SEG8
#define  SEG8_9A        gubLCDSEGData[4].Bit.D7    // COM0
#define  SEG8_9B        gubLCDSEGData[4].Bit.D6    // COM1
#define  SEG8_9C        gubLCDSEGData[4].Bit.D5    // COM2
#define  SEG8_X3        gubLCDSEGData[4].Bit.D4    // COM3
// LCD PAD10 - SEG9
#define  SEG9_10A       gubLCDSEGData[4].Bit.D3    // COM0
#define  SEG9_10B       gubLCDSEGData[4].Bit.D2    // COM1
#define  SEG9_10C       gubLCDSEGData[4].Bit.D1    // COM2
#define  SEG9_X3        gubLCDSEGData[4].Bit.D0    // COM3
// LCD PAD11 - SEG10
#define  SEG10_11A      gubLCDSEGData[5].Bit.D7   // COM0
#define  SEG10_11B      gubLCDSEGData[5].Bit.D6   // COM1
#define  SEG10_11C      gubLCDSEGData[5].Bit.D5   // COM2
#define  SEG10_X3       gubLCDSEGData[5].Bit.D4   // COM3
// LCD PAD12 - SEG11
#define  SEG11_12A      gubLCDSEGData[5].Bit.D3   // COM0
#define  SEG11_12B      gubLCDSEGData[5].Bit.D2   // COM1
#define  SEG11_12C      gubLCDSEGData[5].Bit.D1   // COM2
#define  SEG11_X3       gubLCDSEGData[5].Bit.D0   // COM3
// LCD PAD13 - SEG12
#define  SEG12_13A      gubLCDSEGData[6].Bit.D7   // COM0
#define  SEG12_13B      gubLCDSEGData[6].Bit.D6   // COM1
#define  SEG12_13C      gubLCDSEGData[6].Bit.D5   // COM2
#define  SEG12_X3       gubLCDSEGData[6].Bit.D4   // COM3
// LCD PAD14 - SEG13
#define  SEG13_14A      gubLCDSEGData[6].Bit.D3   // COM0
#define  SEG13_14B      gubLCDSEGData[6].Bit.D2   // COM1
#define  SEG13_X2       gubLCDSEGData[6].Bit.D1   // COM2
#define  SEG13_X3       gubLCDSEGData[6].Bit.D0   // COM3
// LCD PAD15 - SEG14
#define  SEG14_15A      gubLCDSEGData[7].Bit.D7   // COM0
#define  SEG14_15B      gubLCDSEGData[7].Bit.D6   // COM1
#define  SEG14_15C      gubLCDSEGData[7].Bit.D5   // COM2
#define  SEG14_X3       gubLCDSEGData[7].Bit.D4   // COM3
// LCD PAD16 - SEG15
#define  SEG15_16A      gubLCDSEGData[7].Bit.D3   // COM0
#define  SEG15_16B      gubLCDSEGData[7].Bit.D2   // COM1
#define  SEG15_16C      gubLCDSEGData[7].Bit.D1   // COM2
#define  SEG15_X3       gubLCDSEGData[7].Bit.D0   // COM3
// LCD PAD17 - SEG16
#define  SEG16_17A      gubLCDSEGData[8].Bit.D7   // COM0
#define  SEG16_17B      gubLCDSEGData[8].Bit.D6   // COM1
#define  SEG16_17C      gubLCDSEGData[8].Bit.D5   // COM2
#define  SEG16_X3       gubLCDSEGData[8].Bit.D4   // COM3
// LCD PAD18 - SEG17
#define  SEG17_18A      gubLCDSEGData[8].Bit.D3   // COM0
#define  SEG17_18B      gubLCDSEGData[8].Bit.D2   // COM1
#define  SEG17_18C      gubLCDSEGData[8].Bit.D1   // COM2
#define  SEG17_X3       gubLCDSEGData[8].Bit.D0   // COM3
// LCD PAD19 - SEG18
#define  SEG18_19A      gubLCDSEGData[9].Bit.D7   // COM0
#define  SEG18_19B      gubLCDSEGData[9].Bit.D6   // COM1
#define  SEG18_19C      gubLCDSEGData[9].Bit.D5   // COM2
#define  SEG18_X3       gubLCDSEGData[9].Bit.D4   // COM3
// LCD PAD20 - SEG19
#define  SEG19_20A      gubLCDSEGData[9].Bit.D3   // COM0
#define  SEG19_20B      gubLCDSEGData[9].Bit.D2   // COM1
#define  SEG19_20C      gubLCDSEGData[9].Bit.D1   // COM2
#define  SEG19_X3       gubLCDSEGData[9].Bit.D0   // COM3

//-----------------------------------------
//  LCD Formatted TT4 Segment Assignment...
//-----------------------------------------
// LCD PAD1 - SEG0
#define  SEG_ALARM          SEG0_1A        
#define  SEG_TIME           SEG0_1B
#define  SEG_ELAPSED        SEG0_1C
//#define  SEG0_X3
// LCD PAD2 - SEG1
#define  SEG_SUN            SEG1_2A        
#define  SEG_HEART          SEG1_2B        
#define  SEG_HRGLASS        SEG1_2C        
//#define  SEG1_X3        
// LCD PAD3 - SEG2
#define  SEG_REMAINING      SEG2_3A        
#define  SEG_AVG            SEG2_3B        
#define  SEG_ARROW          SEG2_3C        
//#define  SEG2_X3        
// LCD PAD4 - SEG3
#define  SEG_HIGH           SEG3_4A        
#define  SEG_MINUS          SEG3_4B        
#define  SEG_LOW            SEG3_4C
//#define  SEG3_X3        
// LCD PAD5 - SEG4
#define  SEG_S0A            SEG4_5A        
#define  SEG_S0B            SEG4_5B        
#define  SEG_DOT5           SEG4_5C        
//#define  SEG4_X3        
// LCD PAD6 - SEG5
#define  SEG_S1A            SEG5_6A        
#define  SEG_S1F            SEG5_6B        
#define  SEG_S1E            SEG5_6C        
//#define  SEG5_X3        
// LCD PAD7 - SEG6
#define  SEG_S1G            SEG6_7A        
#define  SEG_S1C            SEG6_7B        
#define  SEG_S1D            SEG6_7C        
//#define  SEG6_X3        
// LCD PAD8 - SEG7
#define  SEG_S1B            SEG7_8A        
#define  SEG_S2E            SEG7_8B        
#define  SEG_DOT8           SEG7_8C        
//#define  SEG7_X3        
// LCD PAD9 - SEG8
#define  SEG_S2F            SEG8_9A        
#define  SEG_S2G            SEG8_9B       
#define  SEG_S2D            SEG8_9C       
//#define  SEG8_X3       
// LCD PAD10 - SEG9
#define  SEG_S2A            SEG9_10A      
#define  SEG_S2B            SEG9_10B      
#define  SEG_S2C            SEG9_10C      
//#define  SEG9_X3       
// LCD PAD11 - SEG10
#define  SEG_COL            SEG10_11A     
#define  SEG_S3E            SEG10_11B     
#define  SEG_DOT11          SEG10_11C     
//#define  SEG10_X3      
// LCD PAD12 - SEG11
#define  SEG_S3F            SEG11_12A     
#define  SEG_S3G            SEG11_12B     
#define  SEG_S3D            SEG11_12C     
//#define  SEG11_X3      
// LCD PAD13 - SEG12
#define  SEG_S3A            SEG12_13A     
#define  SEG_S3B            SEG12_13B      
#define  SEG_S3C            SEG12_13C      
//#define  SEG12_X3       
// LCD PAD14 - SEG13
#define  SEG_DEG            SEG13_14A      
#define  SEG_PERCENT        SEG13_14B      
//#define  SEG13_X2       
//#define  SEG13_X3       
// LCD PAD15 - SEG14
#define  SEG_UNIT_A         SEG14_15A      
#define  SEG_UNIT_B         SEG14_15B      
#define  SEG_UNIT_C         SEG14_15C      
//#define  SEG14_X3       
// LCD PAD16 - SEG15
#define  SEG_MIN            SEG15_16A      
#define  SEG_HR             SEG15_16B      
#define  SEG_DAY            SEG15_16C      
//#define  SEG15_X3       
// LCD PAD17 - SEG16
#define  SEG_X_A            SEG16_17A      
#define  SEG_X_CHK_B        SEG16_17B      
#define  SEG_STOP           SEG16_17C      
//#define  SEG16_X3       
// LCD PAD18 - SEG17
#define  SEG_S4C            SEG17_18A      
#define  SEG_BELL           SEG17_18B      
#define  SEG_CHECK          SEG17_18C      
//#define  SEG17_X3       
// LCD PAD19 - SEG18
#define  SEG_S4A            SEG18_19A      
#define  SEG_S4F            SEG18_19B      
#define  SEG_S4B            SEG18_19C      
//#define  SEG18_X3       
// LCD PAD20 - SEG19
#define  SEG_S4G            SEG19_20A      
#define  SEG_S4E            SEG19_20B      
#define  SEG_S4D            SEG19_20C      
//#define  SEG19_X3       


// EEPROM configuration control structure register...
extern  __USER_CONFIGURATION_AREA    _gsUser_Configuration_Area;
extern  __ALARM_CONFIGURATION_AREA   _gsAlarmConfig[6];
// EEPROM control bit manipulator structure register...
extern __sBITS_GENCFGOPT_BYTE       _gsGenCfgOptByte;
extern __sBITS_EXTOPT_BYTE          _gsExtdOptByte;

// extreme temperature points global storage....
extern _sLOGMINMAXTEMPDATA _gsLogMinMaxTempData;

// update LCD global status flag...
extern BOOL gbUpdateLCDFlag __attribute__((persistent));       //set as persistent for reset recovery
// LCD blink toggle bit...
UINT8 gbBlinkToggleBit __attribute__((persistent));     //set as persistent for reset recovery


UINT8 gubLCDSummaryStatus;
UINT8 gubAlarmDisplayStatus;

// Alarm remaining and time elapse value...
UINT32 gudwAlarmTimeRemainingElapsed;
// Alarm remaining and time elapse flag...
BOOL gbAlarmTimeFlag;

static uint8_t lcd_tx_data[20], lcd_rx_data[20];
//----------------------------------------------
//  LCD Control Function Implementation...
//----------------------------------------------


/*******************************************************************************
**  Function     : devLCD_Init
**  Description  : initializes LCD device registers..
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void devLCD_Init(void)
{ 
	lcd_i2c_init();// Initialize LCD I2C driver
    
	lcd_tx_data[0] = 0b11101010; // ICSET : Set IC Operation...
	lcd_tx_data[1] = 0b11000000; // MODESET : Mode Set...
	lcd_tx_data[2] = 0b00000000; // ADSET : Address Set...
	for(int i=0; i<10; i++)
	{
		lcd_tx_data[i+3] = 0;  // Clear RAM..
	}
	lcd_i2c_write(lcd_tx_data,13);
	
	
	lcd_tx_data[0] = 0b11101000; // ICSET : IC Set Operation...
	lcd_tx_data[1] = 0b10111100; // DISCTL : Display Control. Low Power Mode
	lcd_tx_data[2] = 0b11110000; // BLKCTL : Blink Control...	
	lcd_tx_data[3] = 0b11111100; // APCTL : All Pixel Control...
	lcd_tx_data[4] = 0b11001000; // MODESET : Mode Set...
	lcd_i2c_write(lcd_tx_data,5);
	devLCD_InitSEGData();
	
	
#ifdef LCD_LEGACY_CODE

    register UINT8 lubData;
    register UINT8 lubCtr;
    register UINT8 lubStatus;
    register UINT8 lubLCDData;
    
    Nop();
    drvI2C1_Init();    
    Nop();

    utilDelay_ms(10);
    
    drvI2C1_StopBit();
    
    utilDelay_ms(10);

    //--------------
    // initialize...
    //--------------
    do{
        
        drvI2C1_StartBit();            
       
       // Send Slave Address...
        drvI2C1_SendByte(LCD_SLAVE_ADDR);
        if(drvI2C1_ReadACK(ACK_LOW) != ACK_OK) continue;            

        // ICSET : Set IC Operation...
        lubLCDData = 0b11101010;        
        drvI2C1_SendByte(lubLCDData);
        if(drvI2C1_ReadACK(ACK_LOW) != ACK_OK) continue;            

        // MODESET : Mode Set...
        lubLCDData = 0b11000000;
        drvI2C1_SendByte(lubLCDData);        
        if(drvI2C1_ReadACK(ACK_LOW) != ACK_OK) continue;            

        // ADSET : Address Set...        
        lubLCDData = 0b00000000;
        drvI2C1_SendByte(lubLCDData);                
        if(drvI2C1_ReadACK(ACK_LOW) != ACK_OK) continue;            

        // Clear RAM.. 
        lubStatus = 0x00;
        for(lubCtr=0;lubCtr<10;lubCtr++){  // 10 = 19 seg lines / 2           
            // Send data @ address 0h - 13h...
            lubData = 0;
            drvI2C1_SendByte(lubData);
            if(drvI2C1_ReadACK(ACK_LOW) != ACK_OK){
                lubStatus = 1;
                break;                    
            }     
        }
        
        if(lubStatus==1) continue;

        drvI2C1_StopBit();
        
        break;
        
    }while(1);    
        
    // 7 milli-secs delay...    
    utilDelay_ms(7);

    //--------------------
    // Dison sequence...
    //--------------------
    do{
        
        drvI2C1_StartBit();            
       
       // Send Slave Address...
        drvI2C1_SendByte(LCD_SLAVE_ADDR);
        if(drvI2C1_ReadACK(ACK_LOW) != ACK_OK) continue;            

        // ICSET : IC Set Operation...
        lubLCDData = 0b11101000;        
        drvI2C1_SendByte(lubLCDData);
        if(drvI2C1_ReadACK(ACK_LOW) != ACK_OK) continue;            

        // DISCTL : Display Control...
        lubLCDData = 0b10111100; // Low Power Mode
        drvI2C1_SendByte(lubLCDData);        
        if(drvI2C1_ReadACK(ACK_LOW) != ACK_OK) continue;            

        // BLKCTL : Blink Control...        
        lubLCDData = 0b11110000;
        drvI2C1_SendByte(lubLCDData);                
        if(drvI2C1_ReadACK(ACK_LOW) != ACK_OK) continue;            

        // APCTL : All Pixel Control...        
        lubLCDData = 0b11111100;
        drvI2C1_SendByte(lubLCDData);                
        if(drvI2C1_ReadACK(ACK_LOW) != ACK_OK) continue;            

        // MODESET : Mode Set...        
        lubLCDData = 0b11001000;
        drvI2C1_SendByte(lubLCDData);                
        if(drvI2C1_ReadACK(ACK_LOW) != ACK_OK) continue;            

        drvI2C1_StopBit();
        
        break;
        
    }while(1);    
    
    // 7 milli-secs delay...    
    utilDelay_ms(7);

    // clear global segment data registers...
    devLCD_InitSEGData();
    
#endif    
}



/*******************************************************************************
**  Function     : devLCD_AllSegBlinkOn
**  Description  : enables the blink register to blink all segments On/Off
**  PreCondition : none
**  Side Effects : segments will always blink after calling this module..
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void devLCD_AllSegBlinkOn(void)
{    
    
   
    lcd_tx_data[0] = LCD_CMD_BLKCTL(LCD_CMD,BLKCTL_BLINK_1HZ); // Set blink control.  0.5sec ON/OFF
    lcd_tx_data[1] = LCD_CMD_APCTL(LCD_CMD,APCTL_APON_ALLPIXON,APCTL_APOFF_NORM); // Set IC operation...
	lcd_i2c_write(lcd_tx_data,2);
		   
#ifdef LCD_LEGACY_CODE
    UINT8 lubData;

    do{        
        drvI2C1_StartBit();            
       
       // Send slave address...
        drvI2C1_SendByte(LCD_SLAVE_ADDR);
        if(drvI2C1_ReadACK(ACK_LOW) != ACK_OK) continue;            

        // Set blink control...
        lubData = LCD_CMD_BLKCTL(LCD_CMD,BLKCTL_BLINK_1HZ); // 0.5sec ON/OFF
        drvI2C1_SendByte(lubData);        
        if(drvI2C1_ReadACK(ACK_LOW) != ACK_OK) continue;            

        // Set IC operation...
        lubData = LCD_CMD_APCTL(LCD_CMD,APCTL_APON_ALLPIXON,APCTL_APOFF_NORM);
        drvI2C1_SendByte(lubData);
        if(drvI2C1_ReadACK(ACK_LOW) != ACK_OK) continue;                    

        drvI2C1_StopBit();
        
        break;
        
    }while(1);    
    
#endif
}



/*******************************************************************************
**  Function     : devLCD_AllSegBlinkOff
**  Description  : disables the blink register to stop all segments blinking
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void devLCD_AllSegBlinkOff(void)
{    
    
	lcd_tx_data[0] = LCD_CMD_BLKCTL(LCD_CMD,BLKCTL_BLINK_OFF); // Set blink control...
	lcd_i2c_write(lcd_tx_data,1);
	
#ifdef LCD_LEGACY_CODE
    UINT8 lubData;

    do{        
        drvI2C1_StartBit();            
       
       // Send slave address...
        drvI2C1_SendByte(LCD_SLAVE_ADDR);
        if(drvI2C1_ReadACK(ACK_LOW) != ACK_OK) continue;            

        // Set blink control...
        lubData = LCD_CMD_BLKCTL(LCD_CMD,BLKCTL_BLINK_OFF);
        drvI2C1_SendByte(lubData);        
        if(drvI2C1_ReadACK(ACK_LOW) != ACK_OK) continue;            

        drvI2C1_StopBit();
        
        break;
        
    }while(1);    
#endif    
    
}



/*******************************************************************************
**  Function     : devLCD_InitSEGData
**  Description  : clears all SEG data to RAM buffer
**  PreCondition : none
**  Side Effects : update's global segment control variables..
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void devLCD_InitSEGData(void){
    
    UINT8 lubCtr;
    
    for(lubCtr=0;lubCtr<10;lubCtr++){
        gubLCDSEGData[lubCtr].Byte = 0x00;
    }
    
}

/*******************************************************************************
**  Function     : devLCD_SetDisp7SEG_1
**  Description  : turn's ON digit number at the SEG1 location
**  PreCondition : none
**  Side Effects : update's global segment control variables..
**  Input        : par_ubNum - digit number
**                 par_ubOnOff - 0-OFF / 1-ON
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void devLCD_SetDisp7SEG_1(UINT8 par_ubNum, UINT8 par_ubOnOff){
    
    _sBYTEBITS lspSetToSeg;
    
    // pass by reference Segment bit field value...                          
    devLCD_Get7SEGDecoder(&lspSetToSeg,par_ubNum,par_ubOnOff);
    
    // load to SEG registers..
    SEG_S1A  = lspSetToSeg.Bit.D0;
    SEG_S1B  = lspSetToSeg.Bit.D1;
    SEG_S1C  = lspSetToSeg.Bit.D2;
    SEG_S1D  = lspSetToSeg.Bit.D3;
    SEG_S1E  = lspSetToSeg.Bit.D4;
    SEG_S1F  = lspSetToSeg.Bit.D5;
    SEG_S1G  = lspSetToSeg.Bit.D6;
}

/*******************************************************************************
**  Function     : devLCD_SetDisp7SEG_2
**  Description  : turn's ON digit number at the SEG2 location
**  PreCondition : none
**  Side Effects : update's global segment control variables..
**  Input        : par_ubNum - digit number
**                 par_ubOnOff - 0-OFF / 1-ON
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void devLCD_SetDisp7SEG_2(UINT8 par_ubNum, UINT8 par_ubOnOff){    
    
    _sBYTEBITS lspSetToSeg;
    
    // pass by reference Segment bit field value...                          
    devLCD_Get7SEGDecoder(&lspSetToSeg,par_ubNum,par_ubOnOff);
    
    // load to SEG registers..
    SEG_S2A = lspSetToSeg.Bit.D0;
    SEG_S2B = lspSetToSeg.Bit.D1;
    SEG_S2C = lspSetToSeg.Bit.D2;
    SEG_S2D = lspSetToSeg.Bit.D3;
    SEG_S2E = lspSetToSeg.Bit.D4;
    SEG_S2F = lspSetToSeg.Bit.D5;
    SEG_S2G = lspSetToSeg.Bit.D6;

}

/*******************************************************************************
**  Function     : devLCD_SetDisp7SEG_3
**  Description  : turn's ON digit number at the SEG3 location
**  PreCondition : none
**  Side Effects : update's global segment control variables..
**  Input        : par_ubNum - digit number
**                 par_ubOnOff - 0-OFF / 1-ON
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void devLCD_SetDisp7SEG_3(UINT8 par_ubNum, UINT8 par_ubOnOff){
    
    _sBYTEBITS lspSetToSeg;
    
    // pass by reference Segment bit field value...                          
    devLCD_Get7SEGDecoder(&lspSetToSeg,par_ubNum,par_ubOnOff);
    
    // load to SEG registers..
    SEG_S3A = lspSetToSeg.Bit.D0;
    SEG_S3B = lspSetToSeg.Bit.D1;
    SEG_S3C = lspSetToSeg.Bit.D2;
    SEG_S3D = lspSetToSeg.Bit.D3;
    SEG_S3E = lspSetToSeg.Bit.D4;
    SEG_S3F = lspSetToSeg.Bit.D5;
    SEG_S3G = lspSetToSeg.Bit.D6;
    
}

/*******************************************************************************
**  Function     : devLCD_SetDisp7SEG_4
**  Description  : turn's ON digit number at the SEG4 location
**  PreCondition : none
**  Side Effects : update's global segment control variables..
**  Input        : par_ubNum - digit number
**                 par_ubOnOff - 0-OFF / 1-ON
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void devLCD_SetDisp7SEG_4(UINT8 par_ubNum, UINT8 par_ubOnOff){
    
    _sBYTEBITS lspSetToSeg;
    
    // pass by reference Segment bit field value...                          
    devLCD_Get7SEGDecoder(&lspSetToSeg,par_ubNum,par_ubOnOff);
    
    // load to SEG registers..
    SEG_S4A = lspSetToSeg.Bit.D0;
    SEG_S4B = lspSetToSeg.Bit.D1;
    SEG_S4C = lspSetToSeg.Bit.D2;
    SEG_S4D = lspSetToSeg.Bit.D3;
    SEG_S4E = lspSetToSeg.Bit.D4;
    SEG_S4F = lspSetToSeg.Bit.D5;
    SEG_S4G = lspSetToSeg.Bit.D6;
    
}


/*******************************************************************************
**  Function     : devLCD_Set7SegDisplay_BCD
**  Description  : converts binary number to BCD w/o trailing zero and displays to LCD
**  PreCondition : none
**  Side Effects : update's global segment control variables..
**  Input        : ls5DigitBCD - BCD structure data
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void devLCD_Set7SegDisplay_BCD(_s5DIGITBCD ls5DigitBCD){    
    
    // check the most significant digit if not equal to zero...  
    if(ls5DigitBCD.BCD.D3==0){
        // check the middle digit if not equal to zero...      
        if(ls5DigitBCD.BCD.D2>0)    
            devLCD_SetDisp7SEG_1(ls5DigitBCD.BCD.D2,1); // set Group 1 segment digit..                
        else    
            devLCD_SetDisp7SEG_1(ls5DigitBCD.BCD.D2,0); // clear Group 1 segment digit.. 
    }
    else{
        devLCD_SetDisp7SEG_1(ls5DigitBCD.BCD.D2,1); // set Group 1 segment digit..
    }    

    // set Group 0 segment digit.. 
    devLCD_SetDisp_1_7SEG_0(ls5DigitBCD.BCD.D3);   

    // set Group 2 segment digit.. 
    devLCD_SetDisp7SEG_2(ls5DigitBCD.BCD.D1,1);   
    
     // set decimal dot..        
    SEG_DOT11 = 1;
    
    // set Group 3 segment digit.. 
    devLCD_SetDisp7SEG_3(ls5DigitBCD.BCD.D0,1);     
    
    // set temperature Unit Icon status...
    devLCD_SetUnitIcon(1);
    
    // set temperature if below zero status and set the (-)sign as desired indication....
    SEG_MINUS = ls5DigitBCD.BCD.Sign;
        
    // send to I2C1 communications to update display..     
    //devLCD_UpdateDisplay();                         
    gbUpdateLCDFlag = TRUE;

}

/*******************************************************************************
**  Function     : devLCD_Set7SegDisplay_Full_BCD
**  Description  : converts binary number to BCD w/ trailing zero and displays to LCD
**  PreCondition : none
**  Side Effects : update's global segment control variables..
**  Input        : ls5DigitBCD - BCD structure data
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void devLCD_Set7SegDisplay_Full_BCD(_s5DIGITBCD ls5DigitBCD){
    
    // set Group 0 segment digit.. 
    devLCD_SetDisp_1_7SEG_0(ls5DigitBCD.BCD.D3);   
    
    // set Group 1 segment digit.. 
    devLCD_SetDisp7SEG_1(ls5DigitBCD.BCD.D2,1);    
    
    // set Group 2 segment digit.. 
    devLCD_SetDisp7SEG_2(ls5DigitBCD.BCD.D1,1);   
    
     // set decimal dot..        
    SEG_DOT11 = 1;
    
    // set Group 3 segment digit.. 
    devLCD_SetDisp7SEG_3(ls5DigitBCD.BCD.D0,1);     
    
    // set temperature Unit Icon status...
    devLCD_SetUnitIcon(1);
    
    // set temperature if below zero status and set the (-)sign as desired indication....
    SEG_MINUS = ls5DigitBCD.BCD.Sign;
        
    // send to I2C1 communications to update display..     
    //devLCD_UpdateDisplay();                         
    gbUpdateLCDFlag = TRUE;

}


/*******************************************************************************
**  Function     : devLCD_SetUnitIcon
**  Description  : display's F or C temperature Unit icon  
**  PreCondition : none
**  Side Effects : update's global segment control variables..
**  Input        : par_ubOnSeg - segment on/off control
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void devLCD_SetUnitIcon(UINT8 par_ubOnSeg){

    // set deg icon..
    SEG_DEG = par_ubOnSeg;                                    

    if(_gsGenCfgOptByte.GenOpt.Byte1.F_C_Unit_Select == 0){
        // set C icon.. 
        SEG_UNIT_A = par_ubOnSeg;
        SEG_UNIT_B = 0;                                     
        SEG_UNIT_C = par_ubOnSeg;                                     
    }
    else{
        // set F icon.. 
        SEG_UNIT_A = par_ubOnSeg;                                     
        SEG_UNIT_B = par_ubOnSeg;                                     
        SEG_UNIT_C = 0;                                     
    }    
}



/*******************************************************************************
**  Function     : devLCD_SetDisp_1_7SEG_0
**  Description  : turn on/off SEG0 as 1 digit display 
**  PreCondition : none
**  Side Effects : update's global segment control variables..
**  Input        : par_ubOnOff - segment on/off control
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void devLCD_SetDisp_1_7SEG_0(UINT8 par_ubOnOff){
        
    _sBYTEBITS lspSetToSeg;
    
    // pass by reference Segment bit field value...                          
    devLCD_Get7SEGDecoder(&lspSetToSeg,1,par_ubOnOff);
    
    // load to SEG registers..
    SEG_S0A = lspSetToSeg.Bit.D1;
    SEG_S0B = lspSetToSeg.Bit.D2;
    
}

/*******************************************************************************
**  Function     : devLCD_Display_AllSegments
**  Description  : display's all segments ON/OFF status
**  PreCondition : none
**  Side Effects : update's global segment control variables..
**  Input        : par_ubLCDAllSegOnOff - status bit
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void devLCD_Display_AllSegments(UINT8 par_ubLCDAllSegOnOff){
    
    UINT8 lubCtr;
    
    lubCtr = 0;
    do{
        gubLCDSEGData[lubCtr].Byte = par_ubLCDAllSegOnOff; // should be 1-on or 0-off only...
        ++lubCtr;
    }while(lubCtr<10);
    
    gbUpdateLCDFlag = TRUE;
}


/*******************************************************************************
**  Function     : devLCD_Display_ArrowMark
**  Description  : displays ARROW icon 
**  PreCondition : none
**  Side Effects : update's global segment control variables..
**  Input        : par_ubBitFlag - segment on/off control
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void devLCD_Display_ArrowMark(UINT8 par_ubBitFlag){
    
    SEG_ARROW = par_ubBitFlag;
    gbUpdateLCDFlag = TRUE;
}

/*******************************************************************************
**  Function     : devLCD_Get7SEGDecoder
**  Description  : converts binary number to 7 SEG display..
**  PreCondition : none
**  Side Effects : update's global segment control variables..
**  Input        : par_ubNum - binary number
**                 par_ubOnOff - segment on/off
**  Output       : par_spSetToSeg - 7 segment structure data 
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void devLCD_Get7SEGDecoder( _sBYTEBITS *par_spSetToSeg,
                            UINT8 par_ubNum, 
                            UINT8 par_ubOnOff){    
                                  
    _sBYTEBITS lspSetToSeg;
    
    if(par_ubOnOff <= 0){
        lspSetToSeg.Bit.D0 = SEG_OFF; // SEG A
        lspSetToSeg.Bit.D1 = SEG_OFF; // SEG B
        lspSetToSeg.Bit.D2 = SEG_OFF; // SEG C
        lspSetToSeg.Bit.D3 = SEG_OFF; // SEG D
        lspSetToSeg.Bit.D4 = SEG_OFF; // SEG E
        lspSetToSeg.Bit.D5 = SEG_OFF; // SEG F
        lspSetToSeg.Bit.D6 = SEG_OFF; // SEG G   
    }
    else{
    
        switch(par_ubNum){
            case(0) :     
            case('0') :
                lspSetToSeg.Bit.D0 = SEG_ON;    // SEG A
                lspSetToSeg.Bit.D1 = SEG_ON;    // SEG B
                lspSetToSeg.Bit.D2 = SEG_ON;    // SEG C
                lspSetToSeg.Bit.D3 = SEG_ON;    // SEG D
                lspSetToSeg.Bit.D4 = SEG_ON;    // SEG E
                lspSetToSeg.Bit.D5 = SEG_ON;    // SEG F
                lspSetToSeg.Bit.D6 = SEG_OFF;   // SEG G   
            break;            
            case(1) :     
            case('1') :
                lspSetToSeg.Bit.D0 = SEG_OFF;   // SEG A
                lspSetToSeg.Bit.D1 = SEG_ON;    // SEG B
                lspSetToSeg.Bit.D2 = SEG_ON;    // SEG C
                lspSetToSeg.Bit.D3 = SEG_OFF;   // SEG D
                lspSetToSeg.Bit.D4 = SEG_OFF;   // SEG E
                lspSetToSeg.Bit.D5 = SEG_OFF;   // SEG F
                lspSetToSeg.Bit.D6 = SEG_OFF;   // SEG G    
            break;
            case(2) :
            case('2') :     
                lspSetToSeg.Bit.D0 = SEG_ON;    // SEG A
                lspSetToSeg.Bit.D1 = SEG_ON;    // SEG B
                lspSetToSeg.Bit.D2 = SEG_OFF;   // SEG C
                lspSetToSeg.Bit.D3 = SEG_ON;    // SEG D
                lspSetToSeg.Bit.D4 = SEG_ON;    // SEG E
                lspSetToSeg.Bit.D5 = SEG_OFF;   // SEG F
                lspSetToSeg.Bit.D6 = SEG_ON;    // SEG G    
            break;
            case(3) :
            case('3') :     
                lspSetToSeg.Bit.D0 = SEG_ON;    // SEG A
                lspSetToSeg.Bit.D1 = SEG_ON;    // SEG B
                lspSetToSeg.Bit.D2 = SEG_ON;    // SEG C
                lspSetToSeg.Bit.D3 = SEG_ON;    // SEG D
                lspSetToSeg.Bit.D4 = SEG_OFF;   // SEG E
                lspSetToSeg.Bit.D5 = SEG_OFF;   // SEG F
                lspSetToSeg.Bit.D6 = SEG_ON;    // SEG G    
                break;
            case(4) :
            case('4') :     
                lspSetToSeg.Bit.D0 = SEG_OFF;   // SEG A
                lspSetToSeg.Bit.D1 = SEG_ON;    // SEG B
                lspSetToSeg.Bit.D2 = SEG_ON;    // SEG C
                lspSetToSeg.Bit.D3 = SEG_OFF;   // SEG D
                lspSetToSeg.Bit.D4 = SEG_OFF;   // SEG E
                lspSetToSeg.Bit.D5 = SEG_ON;    // SEG F
                lspSetToSeg.Bit.D6 = SEG_ON;    // SEG G    
            break;
            case(5) :
            case('5') :     
                lspSetToSeg.Bit.D0 = SEG_ON;    // SEG A
                lspSetToSeg.Bit.D1 = SEG_OFF;   // SEG B
                lspSetToSeg.Bit.D2 = SEG_ON;    // SEG C
                lspSetToSeg.Bit.D3 = SEG_ON;    // SEG D
                lspSetToSeg.Bit.D4 = SEG_OFF;   // SEG E
                lspSetToSeg.Bit.D5 = SEG_ON;    // SEG F
                lspSetToSeg.Bit.D6 = SEG_ON;    // SEG G    
            break;
            case(6) :
            case('6') :     
                lspSetToSeg.Bit.D0 = SEG_ON;    // SEG A
                lspSetToSeg.Bit.D1 = SEG_OFF;   // SEG B
                lspSetToSeg.Bit.D2 = SEG_ON;    // SEG C
                lspSetToSeg.Bit.D3 = SEG_ON;    // SEG D
                lspSetToSeg.Bit.D4 = SEG_ON;    // SEG E
                lspSetToSeg.Bit.D5 = SEG_ON;    // SEG F
                lspSetToSeg.Bit.D6 = SEG_ON;    // SEG G    
            break;
            case(7) :     
            case('7') :
                lspSetToSeg.Bit.D0 = SEG_ON;    // SEG A
                lspSetToSeg.Bit.D1 = SEG_ON;    // SEG B
                lspSetToSeg.Bit.D2 = SEG_ON;    // SEG C
                lspSetToSeg.Bit.D3 = SEG_OFF;   // SEG D
                lspSetToSeg.Bit.D4 = SEG_OFF;   // SEG E
                lspSetToSeg.Bit.D5 = SEG_OFF;   // SEG F
                lspSetToSeg.Bit.D6 = SEG_OFF;   // SEG G    
            break;
            case(8) :
            case('8') :     
                lspSetToSeg.Bit.D0 = SEG_ON;    // SEG A
                lspSetToSeg.Bit.D1 = SEG_ON;    // SEG B
                lspSetToSeg.Bit.D2 = SEG_ON;    // SEG C
                lspSetToSeg.Bit.D3 = SEG_ON;    // SEG D
                lspSetToSeg.Bit.D4 = SEG_ON;    // SEG E
                lspSetToSeg.Bit.D5 = SEG_ON;    // SEG F
                lspSetToSeg.Bit.D6 = SEG_ON;    // SEG G    
            break;
            case(9) :
            case('9') :     
                lspSetToSeg.Bit.D0 = SEG_ON;    // SEG A
                lspSetToSeg.Bit.D1 = SEG_ON;    // SEG B
                lspSetToSeg.Bit.D2 = SEG_ON;    // SEG C
                lspSetToSeg.Bit.D3 = SEG_ON;    // SEG D
                lspSetToSeg.Bit.D4 = SEG_OFF;   // SEG E
                lspSetToSeg.Bit.D5 = SEG_ON;    // SEG F
                lspSetToSeg.Bit.D6 = SEG_ON;    // SEG G    
            break;
        }
    
    }
    
    *par_spSetToSeg = lspSetToSeg;

}


/*******************************************************************************
**  Function     : devLCD_Display_SwVersion
**  Description  : display's LCD software version 
**  PreCondition : none
**  Side Effects : update's global segment control variables..
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void devLCD_Display_SwVersion(void){

    _s5DIGITBCD ls5DigitBCD;
    
    // Software version with fixed bug code...
    // convert decimal number to BCD format...
    ls5DigitBCD = devLCD_DEC_To_BCD(TT4USB_MA_SW_VERSION_2);
    
    // load to 7 segment encoder...
    devLCD_SetDisp7SEG_1(ls5DigitBCD.BCD.D2,SEG_ON); // MSB
    devLCD_SetDisp7SEG_2(ls5DigitBCD.BCD.D1,SEG_ON); 
    devLCD_SetDisp7SEG_3(ls5DigitBCD.BCD.D0,SEG_ON); // LSB
    
    // activate (-) sign for debug version mode...
    if(DEBUG_VERSION==1){        
        devLCD_SetDisp7SEG_4(DEBUG_NUMBER,1);    
        SEG_MINUS = 1;    
    }
    else{
        // Software version with feature change code... 
        if(ENA_HEADER_VERSION_NUM) devLCD_SetDisp7SEG_4(TT4USB_MA_SW_VERSION_1,1);    
    }        
    
    gbUpdateLCDFlag = TRUE;
}


/*******************************************************************************
**  Function     : devLCD_Display_Firmware_Update
**  Description  : display's LCD 449 project number 
**  PreCondition : none
**  Side Effects : update's global segment control variables..
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void devLCD_Display_Firmware_Update(void){
    
    // TT4USB Firmwrae Update Indication LCD Displays 449...
    devLCD_SetDisp7SEG_1(4,SEG_ON); // MSB
    devLCD_SetDisp7SEG_2(4,SEG_ON); 
    devLCD_SetDisp7SEG_3(9,SEG_ON); // LSB     
    
    gbUpdateLCDFlag = TRUE;

}

/*******************************************************************************
**  Function     : devLCD_GetBlinkStatus
**  Description  : temperature reading display blink mode in LCD
**  PreCondition : none
**  Side Effects : update's global segment control variables..
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void devLCD_GetBlinkStatus(void){
    
    if(_gsGenCfgOptByte.GenOpt.Byte0.BlinkModeEna==1){ 
        gbBlinkToggleBit ^= 1;
        gbUpdateLCDFlag = TRUE;
    }    
    else    
        gbBlinkToggleBit = 1;            
}        

/*******************************************************************************
**  Function     : devLCD_Display_SystemRunningIcons
**  Description  : display's LCD system running icons
**  PreCondition : none
**  Side Effects : update's global segment control variables..
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void devLCD_Display_SystemRunningIcons(void){        
    
    if(_gsUser_Configuration_Area.ubAlarmStatus & 0x3F){
        
        SEG_SUN = 1;
        
        if(_gsGenCfgOptByte.GenOpt.Byte1.AlarmBellNotifEna==1){
                
            // Alarm Triggered Icon Status!...        
        	if(_gsGenCfgOptByte.GenOpt.Byte0.AlarmBellIco == 0){
                // bell icon            		
                if(!gubLCDSummaryStatus) 
                    SEG_BELL = gbBlinkToggleBit;   
                else
                    SEG_BELL = 1;   
        	}
        	else{
                // X icon...
                if(!gubLCDSummaryStatus){ 
            	    SEG_X_CHK_B = gbBlinkToggleBit;
            	    SEG_X_A = gbBlinkToggleBit;  
            	}
            	else{
            	    SEG_X_CHK_B = 1;
            	    SEG_X_A = 1;
            	}
            	SEG_CHECK = 0;         
        	}        
        }
    }    
    else{
        
        if(!gubLCDSummaryStatus) 
            SEG_SUN = gbBlinkToggleBit;
        else
            SEG_SUN = 1;
	    
	    if(_gsGenCfgOptByte.GenOpt.Byte1.AlarmBellNotifEna==1){
        
            // Alarm Not Triggered Icon Status!...        
        	if(_gsGenCfgOptByte.GenOpt.Byte0.AlarmBellIco == 0){
                // bell icon
                SEG_BELL = 0;   
        	}
        	else{
                // check icon...
            	SEG_X_CHK_B = 1;
            	SEG_CHECK = 1;
            }	   		
        }
    }
    
    gbUpdateLCDFlag = TRUE;    
}

/*******************************************************************************
**  Function     : devLCD_Display_ReadTemperature
**  Description  : display temperature reading to LCD
**  PreCondition : none
**  Side Effects : update's global segment control variables..
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void devLCD_Display_ReadTemperature(void){
    
    _s5DIGITBCD ls5DigitBCD;    
    
    if(_gsGenCfgOptByte.GenOpt.Byte1.DispCurntTempEna == 1){
        // reformat final temperature data to BCD form..
        if(_gsGenCfgOptByte.GenOpt.Byte1.F_C_Unit_Select == 1){        
            ls5DigitBCD = devLCD_FinalTemperatureValueToBCD(devSensor_GetFahrenheit_TempData());
        }
        else{            
            ls5DigitBCD = devLCD_FinalTemperatureValueToBCD(devSensor_GetCelsius_TempData());
        }
    }
 
    if(_gsGenCfgOptByte.GenOpt.Byte1.DispCurntTempEna == 1){
        // display final temperature value to 7 segment LCD panel... 
        devLCD_Set7SegDisplay_BCD(ls5DigitBCD);        
    }
    else{        
        gbUpdateLCDFlag = TRUE;
    }     
}

/*******************************************************************************
**  Function     : devLCD_Display_RemainingElapse_Alarm
**  Description  : display's remaining time and elapse time to LCD
**  PreCondition : none
**  Side Effects : update's global segment control variables..
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void devLCD_Display_RemainingElapse_Alarm(void){
    
    // This routine is a supplemantary information in LCD summary display
    // if the Alarm elapse/remaining time bit enabled... mjm

    UINT8 lubAlarmNum;
    
    // enable segments...
    SEG_ALARM = 1;
    SEG_TIME = 1;

    // get the alarm number...
    lubAlarmNum = (_gsUser_Configuration_Area.ubRemAlarmDispCfgByte  & MASK_ALARM_TIME_REM_ELP_ALM_NUM) >> 4;
    
    // display Alarm number...
    devLCD_SetDisp7SEG_4(lubAlarmNum,1);    
        
    if((_gsUser_Configuration_Area.ubRemAlarmDispCfgByte & MASK_ALARM_TIME_REM_ELP_SELECT) == ALARM_TIME_SELECT_ELAPSE_TIME){        
        //------------------------------------
        //  Display Alarm Elapse Time...
        //  Note: this is a CountUp timer...
        //------------------------------------        
        SEG_ELAPSED = 1;        
    }
    else{        
        //-----------------------------------
        //  Display Alarm Remaining Time...
        //  Note: this is a CountDown timer...
        //-----------------------------------        
        SEG_REMAINING = 1;        
    }
    
    devLCD_Display_RemainingElapseTime(gudwAlarmTimeRemainingElapsed);
    
}

/*******************************************************************************
**  Function     : devLCD_Display_ElapseThreshold_Alarm
**  Description  : display's elapse threshold time to LCD 
**  PreCondition : none
**  Side Effects : update's global segment control variables..
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void devLCD_Display_ElapseThreshold_Alarm(void){

    register UINT8 lubAlarmNum;
    register UINT32 ludwTimeInSecs;    
    
    SEG_MIN = 0;
    SEG_HR = 0;
    SEG_DAY = 0;
    SEG_ELAPSED = 0;      

    // get the alarm number...
    lubAlarmNum = (_gsUser_Configuration_Area.ubRemAlarmDispCfgByte  & 0xF0) >> 4;
    
    devLCD_SetDisp7SEG_4(lubAlarmNum,1);
    
    ludwTimeInSecs = _gsAlarmConfig[lubAlarmNum-1].udwThreshhold * _gsUser_Configuration_Area.udwMeasurementInterval;    
    
    devLCD_Display_RemainingElapseTime(ludwTimeInSecs);
}

/*******************************************************************************
**  Function     : devLCD_Display_RemainingElapseTime
**  Description  : validates time range and display's remaining time elapse.
**  PreCondition : none
**  Side Effects : update's global segment control variables..
**  Input        : par_udwTimeInSecs - time in seconds
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void devLCD_Display_RemainingElapseTime(UINT32 par_udwTimeInSecs){

    _s5DIGITBCD ls5DigitBCD;    
    register float lfMin;
    register float lfHr;
    register float lfDay;
    
    register float  lfTempTime;
    register UINT16 luwTempTime;    
    register UINT8  lubDigit;    
    register UINT8  lubDigitOn1,lubDigitOn2,lubDigitOn3;

    lfMin = (float)par_udwTimeInSecs / 60.0;     // 1 min  = 60 secs
    lfHr  = (float)par_udwTimeInSecs / 3600.0;   // 1 hr   = 60 mins
    lfDay = (float)par_udwTimeInSecs / 86400.0;  // 24 hrs = 1 day...

    // check time ranges...
    if((lfMin <= 9.5) && (lfMin >= 0.0)){ // 2 digits...
        
        SEG_MIN = 1;  
        
        // less than 0.5 considered to be zero!
        if(lfMin < 0.5){        

            lubDigitOn3 = 1;
            SEG_DOT11   = 0;
            lubDigitOn2 = 0;
            lubDigitOn1 = 0;        
            
            ls5DigitBCD.BCD.D0 = 0; // zero mins...
            ls5DigitBCD.BCD.D1 = 0;
            ls5DigitBCD.BCD.D2 = 0;
        }
        else{
            
            // extract 10's decimal digit!...
            lfTempTime = lfMin * 10.0;
            luwTempTime = (UINT16)lfTempTime;

            // convert decimal number to 3 digit BCD format...
            ls5DigitBCD = devLCD_DEC_To_BCD(luwTempTime);
            
            if(ls5DigitBCD.BCD.D0 <= 4){
                ls5DigitBCD.DWord >>= 4;
                lubDigitOn3 = 1;                
                SEG_DOT11   = 0;
                lubDigitOn2 = 0;                
                lubDigitOn1 = 0;                 
            }
            else{
                ls5DigitBCD.BCD.D0 = 5; // zero mins...     
                lubDigitOn3 = 1;                
                SEG_DOT11   = 1;
                lubDigitOn2 = 1;                
                lubDigitOn1 = 0;
            }
        }
    }
    else if((lfMin < 100.0) && (lfMin > 9.5)){ // 3 digits...

        SEG_MIN = 1;  
        
        luwTempTime = (UINT16)lfMin;
        lfTempTime = lfMin - (float)luwTempTime;
        
        if(lfTempTime >= 0.5){
            
            lubDigitOn3 = 1;
            SEG_DOT11   = 1;
            lubDigitOn2 = 1;
            lubDigitOn1 = 1;
            
            ls5DigitBCD.BCD.D0 = 5;               
            lubDigit = (UINT8)(luwTempTime % 10);         
            ls5DigitBCD.BCD.D1 = lubDigit;
            lubDigit = (UINT8)(luwTempTime / 10);
            ls5DigitBCD.BCD.D2 = lubDigit;
        }
        else{

            lubDigitOn3 = 1;
            SEG_DOT11   = 0;
            lubDigitOn2 = 1;
            lubDigitOn1 = 0;
            
            lubDigit = (UINT8)(luwTempTime % 10);         
            ls5DigitBCD.BCD.D0 = lubDigit;
            lubDigit = (UINT8)(luwTempTime / 10);
            ls5DigitBCD.BCD.D1 = lubDigit;
            ls5DigitBCD.BCD.D2 = 0;
        }
    }
    else if((lfHr < 10.0) && (lfHr >= 1.5)){
       
        SEG_HR = 1;
        
        luwTempTime = (UINT16)lfHr;
        lfTempTime = lfHr - (float)luwTempTime;
        
        if(lfTempTime >= 0.5){

            lubDigitOn1 = 0;
            lubDigitOn2 = 1;
            SEG_DOT11   = 1;
            lubDigitOn3 = 1;
            
            ls5DigitBCD.BCD.D0 = 5;
            ls5DigitBCD.BCD.D1 = luwTempTime;
            ls5DigitBCD.BCD.D2 = 0;
        }
        else{
            
            lubDigitOn1 = 0;
            lubDigitOn2 = 0;
            SEG_DOT11   = 0;
            lubDigitOn3 = 1;
            
            ls5DigitBCD.BCD.D0 = luwTempTime;       
            ls5DigitBCD.BCD.D1 = 0;
            ls5DigitBCD.BCD.D2 = 0;
        }
        
    }        
    else if((lfHr < 72.0) && (lfHr >= 10.0)){

        SEG_HR = 1;
        
        luwTempTime = (UINT16)lfHr;
        lfTempTime = lfHr - (float)luwTempTime;
        
        if(lfTempTime >= 0.5){
            
            lubDigitOn3 = 1;
            SEG_DOT11   = 1;
            lubDigitOn2 = 1;
            lubDigitOn1 = 1;
            
            ls5DigitBCD.BCD.D0 = 5;               
            lubDigit = (UINT8)(luwTempTime % 10);         
            ls5DigitBCD.BCD.D1 = lubDigit;
            lubDigit = (UINT8)(luwTempTime / 10);
            ls5DigitBCD.BCD.D2 = lubDigit;
        }
        else{

            lubDigitOn3 = 1;
            SEG_DOT11   = 0;
            lubDigitOn2 = 1;
            lubDigitOn1 = 0;
            
            lubDigit = (UINT8)(luwTempTime % 10);         
            ls5DigitBCD.BCD.D0 = lubDigit;
            lubDigit = (UINT8)(luwTempTime / 10);
            ls5DigitBCD.BCD.D1 = lubDigit;
            ls5DigitBCD.BCD.D2 = 0;
        }
        
    }
    else if(lfHr >= 72.0){
        
        SEG_DAY = 1;     

        luwTempTime = (UINT16)lfDay;        
        
        if(luwTempTime <= 9.0){
            
            lubDigitOn3 = 1;
            SEG_DOT11   = 0;
            lubDigitOn2 = 0;
            lubDigitOn1 = 0;
            
            ls5DigitBCD.BCD.D0 = luwTempTime;            
            ls5DigitBCD.BCD.D1 = 0;
            ls5DigitBCD.BCD.D2 = 0;
        }    
        
        if((luwTempTime <= 99.0)&&(luwTempTime >= 10.0)){
            
            lubDigitOn3 = 1;
            SEG_DOT11   = 0;
            lubDigitOn2 = 1;
            lubDigitOn1 = 0;
            
            lubDigit = (UINT8)(luwTempTime % 10);         
            ls5DigitBCD.BCD.D0 = lubDigit;
            lubDigit = (UINT8)(luwTempTime / 10);
            ls5DigitBCD.BCD.D1 = lubDigit;
            ls5DigitBCD.BCD.D2 = 0;
        }
        
        if(luwTempTime >= 100.0){
                        
            lubDigitOn3 = 1;
            SEG_DOT11   = 0;
            lubDigitOn2 = 1;
            lubDigitOn1 = 1;
            
            lubDigit = (UINT8)(luwTempTime % 10);         
            ls5DigitBCD.BCD.D0 = lubDigit;
            lubDigit = (UINT8)((luwTempTime - ((luwTempTime/100) * 100)) / 10);
            ls5DigitBCD.BCD.D1 = lubDigit;
            lubDigit = (UINT8)(luwTempTime / 100);
            ls5DigitBCD.BCD.D2 = lubDigit;
        }
    }
    
    // always clear deg unit icon if showing the time...
    devLCD_SetUnitIcon(0);
        
    // display elapse or remaining time....  ...              1,2,3
    devLCD_SetDisp7SEG_1(ls5DigitBCD.BCD.D2,lubDigitOn1);  // Digit 1..              
    devLCD_SetDisp7SEG_2(ls5DigitBCD.BCD.D1,lubDigitOn2);  // Digit 2..          
    devLCD_SetDisp7SEG_3(ls5DigitBCD.BCD.D0,lubDigitOn3);  // Digit 3..       
    
    // send to I2C1 communications to update display..     
    gbUpdateLCDFlag = TRUE;
    Nop();                     
}

/*******************************************************************************
**  Function     : devLCD_Display_CyclingTriggeredAlarm
**  Description  : display's cycling triggered alarm ...
**  PreCondition : done
**  Side Effects : update's global segment control variables..
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void devLCD_Display_CyclingTriggeredAlarm(void){

    static UINT8 lubNextAlarmNumber = ALARM_1;
    UINT8 lubAlarmTrigStatus;       
         
    
    
        if(_gsUser_Configuration_Area.ubAlarmStatus & 0x3F){
    
            if(gubAlarmDisplayStatus == 0){
                // reset global alarm display status bit...  
                lubNextAlarmNumber = 0;
            }
            
          //=======================
            LABEL_ALARM_WRAPAROUND:
          //=======================    
            
            switch(lubNextAlarmNumber){
            case(ALARM_1):       
                // set global alarm display status bit...  
                gubAlarmDisplayStatus = 1;
                                 
                lubNextAlarmNumber = ALARM_2;      
                //check alarm enable..
                if(_gsAlarmConfig[ALARM_1].ubConfigByte0&0x1)
                    if(_gsUser_Configuration_Area.ubAlarmStatus&0x01){
                        lubAlarmTrigStatus = 1;
                        break;
                    }     
                // fallthough if alarm is disabled or not triggered!...
            case(ALARM_2):
                lubNextAlarmNumber = ALARM_3;                            
                //check alarm enable..
                if(_gsAlarmConfig[ALARM_2].ubConfigByte0&0x1)
                    if(_gsUser_Configuration_Area.ubAlarmStatus&0x02){
                        lubAlarmTrigStatus = 2;
                        break;
                    }     
                // fallthough if alarm is disabled or not triggered!...
            case(ALARM_3):
                lubNextAlarmNumber = ALARM_4;                            
                //check alarm enable..
                if(_gsAlarmConfig[ALARM_3].ubConfigByte0&0x1)
                    if(_gsUser_Configuration_Area.ubAlarmStatus&0x04){
                        lubAlarmTrigStatus = 3;
                        break;
                    }     
                // fallthough if alarm is disabled or not triggered!...                
            case(ALARM_4):
                lubNextAlarmNumber = ALARM_5;
                //check alarm enable..
                if(_gsAlarmConfig[ALARM_4].ubConfigByte0&0x1)                
                    if(_gsUser_Configuration_Area.ubAlarmStatus&0x08){
                        lubAlarmTrigStatus = 4;
                        break;
                    }     
                // fallthough if alarm is disabled or not triggered!... 
            case(ALARM_5):
                lubNextAlarmNumber = ALARM_6;
                //check alarm enable..
                if(_gsAlarmConfig[ALARM_5].ubConfigByte0&0x1)                
                    if(_gsUser_Configuration_Area.ubAlarmStatus&0x10){
                        lubAlarmTrigStatus = 5;
                        break;
                    }     
                // fallthough if alarm is disabled or not triggered!... 
            case(ALARM_6):
                lubNextAlarmNumber = ALARM_1;                            
                //check alarm enable..
                if(_gsAlarmConfig[ALARM_6].ubConfigByte0&0x1)                
                    if(_gsUser_Configuration_Area.ubAlarmStatus&0x20){
                        lubAlarmTrigStatus = 6;
                        break;
                    }  
                goto LABEL_ALARM_WRAPAROUND;       
                // fallthough if alarm is disabled or not triggered!...                 
            }

            // get the alarm number...        
            devLCD_SetDisp7SEG_4(lubAlarmTrigStatus,1);    
            // send to I2C1 communications to update display..                 
            gbUpdateLCDFlag = TRUE;
        }    
 
}

/*******************************************************************************
**  Function     : devLCD_Display_HeartBeat
**  Description  : display's blinking heartbeat icon
**  PreCondition : none
**  Side Effects : update's global segment control variables..
**  Input        : par_ubSegOn - segment on/off
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
UINT8 devLCD_Display_HeartBeat(UINT8 par_ubSegOn){
   
    if(_gsExtdOptByte.ExtOpt.Byte1.HeartBeatIcoEna == 1){ 
        SEG_HEART = par_ubSegOn;        
        gbUpdateLCDFlag = TRUE;
        return (par_ubSegOn);
    }
    else
        return (0);
}    

/*******************************************************************************
**  Function     : devLCD_Display_StartUPIcon
**  Description  : displays hourglass startup display icon
**  PreCondition : none
**  Side Effects : update's global segment control variables..
**  Input        : par_ubFlag - segment on/off
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void devLCD_Display_StartUPIcon(UINT8 par_ubFlag){
    
    SEG_SUN = 1;
    
    if(par_ubFlag)
        SEG_HRGLASS = 1;
    else
        SEG_HRGLASS = 0;
    
    gbUpdateLCDFlag = TRUE;
}

/*******************************************************************************
**  Function     : devLCD_Display_RunningIcon
**  Description  : display's SUN icon to LCD
**  PreCondition : none
**  Side Effects : update's global segment control variables..
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void devLCD_Display_RunningIcon(void){

    if(_gsGenCfgOptByte.GenOpt.Byte0.BlinkModeEna==1){ 
        SEG_SUN ^= 1;    
        gbUpdateLCDFlag = TRUE;
    }    
}
    
/*******************************************************************************
**  Function     : devLCD_Display_Summary
**  Description  : display's LCD summary
**  PreCondition : none
**  Side Effects : update's global segment control variables..
**  Input        : none
**  Output       : return's LCD summary status level
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
UINT8 devLCD_Display_Summary(void){
    
    _s5DIGITBCD ls5DigitBCD;
    static UINT8 lubSummaryLevel = 0;        
    register float lfTempValue;
    
    if(gubLCDSummaryStatus==0){
        lubSummaryLevel = 0;
    }
    
    switch(lubSummaryLevel){    
    case(0): // level 1..
        // reset global LCD status bit...
        gubLCDSummaryStatus = 1;
    
        ++lubSummaryLevel;
        
        if((_gsUser_Configuration_Area.ubRemAlarmDispCfgByte & MASK_ALARM_TIME_REM_ELP_DISP_ENA) == ALARM_TIME_REM_ELP_DISP_ENA){
            devLCD_Display_RemainingElapse_Alarm();                                            
            break;    
        }
        
        // if Remaining Time or Elapse Time were not enabled then pass thru to the next sequence... mjm
        // fallthrough....    
    case(1): // level 2..
    
        // note: if level 1 is not enabled then Elapse Time Threshold will not be processed!... mjm    
        if((_gsUser_Configuration_Area.ubRemAlarmDispCfgByte & MASK_ALARM_TIME_SELECT_ELAPSE) == ALARM_TIME_SELECT_ELAPSE){
            devLCD_Display_ElapseThreshold_Alarm();                    
            ++lubSummaryLevel;
            break;    
        }   
        
        ++lubSummaryLevel;     
        // if Elapse Time was not enabled then pass thru to the next sequence... mjm
        // fallthrough....    
    case(2): // level 3..
        
        // if selected Remaining time must turn-off the SEG at this level...        
        // clear all elaspe and remaining time segments ....
        SEG_ALARM = 0;
        SEG_TIME = 0;        
        SEG_HR = 0;
        SEG_MIN = 0;
        SEG_DAY = 0;
        SEG_ELAPSED = 0;
        SEG_REMAINING = 0;
        devLCD_SetDisp7SEG_4(8,0); // clear segsments...
        
        // LCD sumary segments starts..
        SEG_AVG  = 1;
        SEG_HIGH = 0;
        SEG_LOW  = 0;        
        
        if(_gsGenCfgOptByte.GenOpt.Byte1.F_C_Unit_Select == DEG_F_UNIT)
            lfTempValue = _gsLogMinMaxTempData.fAveTemp_F_Deg;
        else
            lfTempValue = _gsLogMinMaxTempData.fAveTemp_C_Deg;
        
        ls5DigitBCD = devLCD_FinalTemperatureValueToBCD(lfTempValue);
        
        // display final temperature value to 7 segment LCD panel... 
        devLCD_Set7SegDisplay_BCD(ls5DigitBCD);        
        
        gbUpdateLCDFlag = TRUE;
        
        ++lubSummaryLevel;
        
        break;    
    case(3): // level 4..
        
        SEG_AVG  = 0;
        SEG_HIGH = 1;

        if(_gsGenCfgOptByte.GenOpt.Byte1.F_C_Unit_Select == DEG_F_UNIT)
            lfTempValue = _gsLogMinMaxTempData.fExtremeHighTemp_F_Deg;
        else
            lfTempValue = _gsLogMinMaxTempData.fExtremeHighTemp_C_Deg;
        
        ls5DigitBCD = devLCD_FinalTemperatureValueToBCD(lfTempValue);
        
        // display final temperature value to 7 segment LCD panel... 
        devLCD_Set7SegDisplay_BCD(ls5DigitBCD);        
        
        gbUpdateLCDFlag = TRUE;

        ++lubSummaryLevel;
        break;    
    case(4): // level 5..

        SEG_HIGH = 0;
        SEG_LOW  = 1;

        if(_gsGenCfgOptByte.GenOpt.Byte1.F_C_Unit_Select == DEG_F_UNIT)
            lfTempValue = _gsLogMinMaxTempData.fExtremeLowTemp_F_Deg;
        else
            lfTempValue = _gsLogMinMaxTempData.fExtremeLowTemp_C_Deg;

        ls5DigitBCD = devLCD_FinalTemperatureValueToBCD(lfTempValue);
        
        // display final temperature value to 7 segment LCD panel... 
        devLCD_Set7SegDisplay_BCD(ls5DigitBCD);        
        
        gbUpdateLCDFlag = TRUE;
        
        ++lubSummaryLevel;        
        break;
    case(5):  // level 5..   
    
        // clear all LCD summary icons...
        SEG_LOW  = 0;
        devLCD_SetDisp7SEG_4(8,0);
        gbUpdateLCDFlag = TRUE;

        lubSummaryLevel = 0;    
        // LCD summary complete!..
        return(LCD_SUMMARY_DONE);    
    }
        
    // showing LCD summary not yet done!..
    return(!LCD_SUMMARY_DONE);
}



/*******************************************************************************
**  Function     : devLCD_Display_CF
**  Description  : display's CF segments to LCD
**  PreCondition : none
**  Side Effects : update's global segment control variables..
**  Input        : par_ubStatus - segment on/off
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void devLCD_Display_CF(UINT8 par_ubStatus){
    
    if(par_ubStatus==1){              
        // set CF display..
        
        // this is SEG letter C
        SEG_S1A = 1;                                     
        SEG_S1B = 0;                                     
        SEG_S1C = 0;                                     
        SEG_S1D = 1;                                     
        SEG_S1E = 1;                                     
        SEG_S1F = 1;                                     
        SEG_S1G = 0;                                     
        
        // this is SEG letter F
        SEG_S2A = 1;
        SEG_S2B = 0;
        SEG_S2C = 0;
        SEG_S2D = 0;
        SEG_S2E = 1;
        SEG_S2F = 1;
        SEG_S2G = 1;

    }
    else{
        // clear CF display..
        
        // this is SEG letter F
        SEG_S1A = 0;                                     
        SEG_S1B = 0;                                     
        SEG_S1C = 0;                                     
        SEG_S1D = 0;                                     
        SEG_S1E = 0;                                     
        SEG_S1F = 0;                                     
        SEG_S1G = 0;                                     

        SEG_S2A = 0;
        SEG_S2B = 0;
        SEG_S2C = 0;
        SEG_S2D = 0;
        SEG_S2E = 0;
        SEG_S2F = 0;
        SEG_S2G = 0;    
    }

    gbUpdateLCDFlag = TRUE;
}

void devLCD_Display_SEt(UINT8 par_ubStatus){

    if(par_ubStatus==1){
        // set CF display..

        // this is SEG letter S
        SEG_S1A = 1;
        SEG_S1B = 0;
        SEG_S1C = 1;
        SEG_S1D = 1;
        SEG_S1E = 0;
        SEG_S1F = 1;
        SEG_S1G = 1;

        // this is SEG letter E
        SEG_S2A = 1;
        SEG_S2B = 0;
        SEG_S2C = 0;
        SEG_S2D = 1;
        SEG_S2E = 1;
        SEG_S2F = 1;
        SEG_S2G = 1;
          // this is SEG letter t
        SEG_S3A = 0;
        SEG_S3B = 0;
        SEG_S3C = 0;
        SEG_S3D = 1;
        SEG_S3E = 1;
        SEG_S3F = 1;
        SEG_S3G = 1;

    }
    else{
        // clear CF display..
   
        SEG_S1A = 0;
        SEG_S1B = 0;
        SEG_S1C = 0;
        SEG_S1D = 0;
        SEG_S1E = 0;
        SEG_S1F = 0;
        SEG_S1G = 0;

        SEG_S2A = 0;
        SEG_S2B = 0;
        SEG_S2C = 0;
        SEG_S2D = 0;
        SEG_S2E = 0;
        SEG_S2F = 0;
        SEG_S2G = 0;

        SEG_S3A = 0;
        SEG_S3B = 0;
        SEG_S3C = 0;
        SEG_S3D = 0;
        SEG_S3E = 0;
        SEG_S3F = 0;
        SEG_S3G = 0;

    }

    gbUpdateLCDFlag = TRUE;
}

void devLCD_Display_CFg(UINT8 par_ubStatus){

    if(par_ubStatus==1){
        // set CF display..

        // this is SEG letter C
        SEG_S1A = 1;
        SEG_S1B = 0;
        SEG_S1C = 0;
        SEG_S1D = 1;
        SEG_S1E = 1;
        SEG_S1F = 1;
        SEG_S1G = 0;

        // this is SEG letter F
        SEG_S2A = 1;
        SEG_S2B = 0;
        SEG_S2C = 0;
        SEG_S2D = 0;
        SEG_S2E = 1;
        SEG_S2F = 1;
        SEG_S2G = 1;

         // this is SEG letter g
        SEG_S3A = 1;
        SEG_S3B = 1;
        SEG_S3C = 1;
        SEG_S3D = 1;
        SEG_S3E = 0;
        SEG_S3F = 1;
        SEG_S3G = 1;
    }
    else{
        // clear CF display..

        // this is SEG letter F
        SEG_S1A = 0;
        SEG_S1B = 0;
        SEG_S1C = 0;
        SEG_S1D = 0;
        SEG_S1E = 0;
        SEG_S1F = 0;
        SEG_S1G = 0;

        SEG_S2A = 0;
        SEG_S2B = 0;
        SEG_S2C = 0;
        SEG_S2D = 0;
        SEG_S2E = 0;
        SEG_S2F = 0;
        SEG_S2G = 0;

         // this is SEG letter g
        SEG_S3A = 0;
        SEG_S3B = 0;
        SEG_S3C = 0;
        SEG_S3D = 0;
        SEG_S3E = 0;
        SEG_S3F = 0;
        SEG_S3G = 0;
    }

    gbUpdateLCDFlag = TRUE;
}


/*******************************************************************************
**  Function     : devLCD_Display_USB
**  Description  : display's USB segments to LCD
**  PreCondition : none
**  Side Effects : update's global segment control variables..
**  Input        : par_ubStatus - segment on/off status
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void devLCD_Display_USB(UINT8 par_ubStatus){
    
    if(par_ubStatus==1){              
        // set USB display..
        
        // this is SEG letter U
        SEG_S1A = 0;                                     
        SEG_S1B = 1;                                     
        SEG_S1C = 1;                                     
        SEG_S1D = 1;                                     
        SEG_S1E = 1;                                     
        SEG_S1F = 1;                                     
        SEG_S1G = 0;                                     
        
        // this is SEG letter S
        SEG_S2A = 1;
        SEG_S2B = 0;
        SEG_S2C = 1;
        SEG_S2D = 1;
        SEG_S2E = 0;
        SEG_S2F = 1;
        SEG_S2G = 1;
   
        // this is SEG letter b
        SEG_S3A = 0;
        SEG_S3B = 0;
        SEG_S3C = 1;
        SEG_S3D = 1;
        SEG_S3E = 1;
        SEG_S3F = 1;
        SEG_S3G = 1;
        
    }
    else{
        // clear USb display..
        SEG_S1A = 0;                                     
        SEG_S1B = 0;                                     
        SEG_S1C = 0;                                     
        SEG_S1D = 0;                                     
        SEG_S1E = 0;                                     
        SEG_S1F = 0;                                     
        SEG_S1G = 0;                                     

        SEG_S2A = 0;
        SEG_S2B = 0;
        SEG_S2C = 0;
        SEG_S2D = 0;
        SEG_S2E = 0;
        SEG_S2F = 0;
        SEG_S2G = 0;    
        
        SEG_S3A = 0;
        SEG_S3B = 0;
        SEG_S3C = 0;
        SEG_S3D = 0;
        SEG_S3E = 0;
        SEG_S3F = 0;
        SEG_S3G = 0;            
    }
    
    gbUpdateLCDFlag = TRUE;
}    


/*******************************************************************************
**  Function     : devLCD_Display_Boot
**  Description  : display's BL bootloader sign to LCD
**  PreCondition : none
**  Side Effects : update's global segment control variables..
**  Input        : par_ubStatus - segment on/off
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void devLCD_Display_Boot(UINT8 par_ubStatus){
    
    if(par_ubStatus==1){              
        // set USB display..
        
        // this is SEG letter b
        SEG_S2A = 0;
        SEG_S2B = 0;
        SEG_S2C = 1;
        SEG_S2D = 1;
        SEG_S2E = 1;
        SEG_S2F = 1;
        SEG_S2G = 1;
   
        // this is SEG letter L
        SEG_S3A = 0;
        SEG_S3B = 0;
        SEG_S3C = 0;
        SEG_S3D = 1;
        SEG_S3E = 1;
        SEG_S3F = 1;
        SEG_S3G = 0;
        
    }
    else{

        SEG_S2A = 0;
        SEG_S2B = 0;
        SEG_S2C = 0;
        SEG_S2D = 0;
        SEG_S2E = 0;
        SEG_S2F = 0;
        SEG_S2G = 0;    
        
        SEG_S3A = 0;
        SEG_S3B = 0;
        SEG_S3C = 0;
        SEG_S3D = 0;
        SEG_S3E = 0;
        SEG_S3F = 0;
        SEG_S3G = 0;            
    }
    
    gbUpdateLCDFlag = TRUE;
}    

/*******************************************************************************
**  Function     : devLCD_Display_StopAlarmDevice
**  Description  : display's stop when alarm comes on to LCD
**  PreCondition : none
**  Side Effects : update's global segment control variables..
**  Input        : par_ubStop - segment on/off
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void devLCD_Display_StopAlarmDevice(UINT8 par_ubStop){
    
    //temperature acquisition halted!... mjm
    // turn-off SUN icon.. 
    SEG_SUN = !par_ubStop;
    
    // STOP display..        
    SEG_STOP = par_ubStop;    
    
    if(_gsUser_Configuration_Area.ubAlarmStatus & 0x3F){
        
        if(_gsGenCfgOptByte.GenOpt.Byte1.AlarmBellNotifEna==1){
            
            // Alarm Triggered Icon Status!...        
            if(_gsGenCfgOptByte.GenOpt.Byte0.AlarmBellIco == 0){
                // bell icon                
                SEG_BELL = 1;   
            }
            else{
                // X icon...
                SEG_X_CHK_B = 1;            
                SEG_X_A = 1;  
                SEG_CHECK = 0;
            }
        }
   }
   
   gbUpdateLCDFlag = TRUE;
}


/*******************************************************************************
**  Function     : devLCD_DEC_To_BCD
**  Description  : convert's DEC to BCD
**  PreCondition : none
**  Side Effects : none
**  Input        : par_uwDecVal - decimal value
**  Output       : return's 7 segment structure data _s5DIGITBCD..
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
_s5DIGITBCD devLCD_DEC_To_BCD(UINT16 par_uwDecVal){    
    
    _s5DIGITBCD ls5DigitBcd;
    UINT16 luwQuotient;
    UINT16 luwRemainder;
    UINT16 luwBinary;
    
    UINT16 luwDigitPlacement;
    UINT8  lubDigitCtr;    
    
    // reset 7 SEG decimal digit data...
    ls5DigitBcd.DWord = 0;    
    
    // initialize variables...    
    luwDigitPlacement = TENS_DIGIT_MAX;
    luwBinary = par_uwDecVal;
    
    for( lubDigitCtr = 0 ; lubDigitCtr < TOTAL_DIGITS; lubDigitCtr++ ){
        
        //-----------------------------------------------------------------------------
        // extract and convert integer number to BCD form for a 7 segment display... 
        //-----------------------------------------------------------------------------  
        
        // get integer division quotient...
        luwQuotient = luwBinary / luwDigitPlacement;
        // get integer modulud remainder...
        luwRemainder  = luwBinary % luwDigitPlacement;
        
        // assign to LCD digits...
        ls5DigitBcd.DWord |= luwQuotient << (TOTAL_DIGITS * (TOTAL_DECIMAL_PLACEMENT-lubDigitCtr));

        // shitfting digit placement..
        luwDigitPlacement /= TENS_DIGIT_DIVISOR;
        
        // take the previous modulus remainder...
        luwBinary = luwRemainder;
    
    }
    
    //-----------------------------------------------------
    //
    // Notes: 7 segment arrangement return value...
    //
    //      ls5DigitBcd.BCD.D0 - Digit SEG3 group 
    //      ls5DigitBcd.BCD.D1 - Digit SEG2 group 
    //      ls5DigitBcd.BCD.D2 - Digit SEG1 group 
    //      ls5DigitBcd.BCD.D3 - Digit SEG0 group 1 only..
    //
    // \mjmariveles..
    //-----------------------------------------------------
    
    return(ls5DigitBcd);
    
}

/*******************************************************************************
**  Function     : devLCD_FinalTemperatureValueToBCD
**  Description  : converts real temperature data to 7 segment display..
**  PreCondition : none
**  Side Effects : update's global segment control variables..
**  Input        : par_fTempValue - temperature real value
**  Output       : return's 7 segment structure data _s5DIGITBCD..
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
_s5DIGITBCD devLCD_FinalTemperatureValueToBCD(float par_fTempValue){    
    
    _s5DIGITBCD ls5DigitBcd;
    UINT16 luwQuotient;
    UINT16 luwRemainder;
    UINT16 luwBinary;
    
    UINT16 luwDigitPlacement;
    UINT8  lubDigitCtr;    
    UINT16 luwIntegerTempValue;
    
    // reset 7 SEG decimal digit data...
    ls5DigitBcd.DWord = 0;    
    
    // check if negative...
    // sure if less than zero should be negative!..
    if(par_fTempValue < 0){ 
        
        // get rid of the tens decimal digit and signed number...
        par_fTempValue = par_fTempValue * -10.0;     
        luwIntegerTempValue = par_fTempValue;  
        
        // turn-on the signed bit...
        ls5DigitBcd.BCD.Sign = 1;            
    }    
    else{
        
        // get rid of the tens decimal digit...
        par_fTempValue = par_fTempValue * 10.0;         
        luwIntegerTempValue = par_fTempValue;         
        
        // turn-off the signed bit...    
        ls5DigitBcd.BCD.Sign = 0;
    }   
    
    
    // initialize variables...    
    luwDigitPlacement = TENS_DIGIT_MAX;
    luwBinary = luwIntegerTempValue;
    
    for( lubDigitCtr = 0 ; lubDigitCtr < TOTAL_DIGITS; lubDigitCtr++ ){
        
        //-----------------------------------------------------------------------------
        // extract and convert integer number to BCD form for a 7 segment display... 
        //-----------------------------------------------------------------------------  
        
        // get integer division quotient...
        luwQuotient = luwBinary / luwDigitPlacement;
        // get integer modulud remainder...
        luwRemainder  = luwBinary % luwDigitPlacement;
        
        // assign to LCD digits...
        ls5DigitBcd.DWord |= luwQuotient << (TOTAL_DIGITS * (TOTAL_DECIMAL_PLACEMENT-lubDigitCtr));

        // shitfting digit placement..
        luwDigitPlacement /= TENS_DIGIT_DIVISOR;
        
        // take the previous modulus remainder...
        luwBinary = luwRemainder;
    
    }
    
    //-----------------------------------------------------
    //
    // Notes: 7 segment arrangement return value...
    //
    //      ls5DigitBcd.BCD.D0 - Digit SEG3 group 
    //      ls5DigitBcd.BCD.D1 - Digit SEG2 group 
    //      ls5DigitBcd.BCD.D2 - Digit SEG1 group 
    //      ls5DigitBcd.BCD.D3 - Digit SEG0 group 1 only..
    //
    //-----------------------------------------------------
    
    return(ls5DigitBcd);
    
}
    
/*******************************************************************************
**  Function     : devLCD_UpdateDisplay
**  Description  : send's Segment data to LCD device...
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/  
void devLCD_UpdateDisplay(void)
{    
  	lcd_tx_data[0] = 0b11101000; // ICSET : IC Set Operation...
  	lcd_tx_data[1] = 188 + 3;//_gsUser_Configuration_Area.ubLCD_ContrastAdjust;  // DISCTL : Display Control. Low Power Mode
  	lcd_tx_data[2] = 0b11110000; // BLKCTL : Blink Control...
  	lcd_tx_data[3] = 0b11111100; // APCTL : All Pixel Control...
  	lcd_tx_data[4] = 0b11001000; // MODESET : Mode Set...
    
  	lcd_tx_data[5] = 0b00000000; // ADSET : Address Set...
  	for(int i=0; i<10; i++)
  	{
	  	lcd_tx_data[i+6] = gubLCDSEGData[i].Byte;  // Send data @ address 0h - 13h...
  	}
  	lcd_i2c_write(lcd_tx_data,16);
  	gbUpdateLCDFlag = FALSE;
    
    
#ifdef LCD_LEGACY_CODE

    register UINT8 lubData;
    register UINT8 lubCtr;
    register UINT8 lubStatus;
    register UINT8 lubLCDData;
    
    
    //-----------------------
    // RAM write sequence...
    //-----------------------
    do{
        
        drvI2C1_StartBit();        
        
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        // Reconfigure LCD registers...    
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
       
       // Send Slave Address...
        drvI2C1_SendByte(LCD_SLAVE_ADDR);
        if(drvI2C1_ReadACK(ACK_LOW) != ACK_OK) continue;            

        // ICSET : Set IC Operation Operation...
        lubLCDData = 0b11101000;        
        drvI2C1_SendByte(lubLCDData);
        if(drvI2C1_ReadACK(ACK_LOW) != ACK_OK) continue;            

        // DISCTL : Display Control...        
        lubLCDData = 188 + gsUser_Configuration_Area.ubLCD_ContrastAdjust;        
        drvI2C1_SendByte(lubLCDData);        
        if(drvI2C1_ReadACK(ACK_LOW) != ACK_OK) continue;            

        // BLKCTL : Blink Control...        
        lubLCDData = 0b11110000;
        drvI2C1_SendByte(lubLCDData);                
        if(drvI2C1_ReadACK(ACK_LOW) != ACK_OK) continue;            

        // APCTL : All Pixel Control...        
        lubLCDData = 0b11111100;
        drvI2C1_SendByte(lubLCDData);                
        if(drvI2C1_ReadACK(ACK_LOW) != ACK_OK) continue;            

        // MODESET : Mode Set...        
        lubLCDData = 0b11001000;
        drvI2C1_SendByte(lubLCDData);                
        if(drvI2C1_ReadACK(ACK_LOW) != ACK_OK) continue;            
        
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        // Write LCD data to RAM...
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        // ADSET : Address Set...        
        lubLCDData = 0b00000000;
        drvI2C1_SendByte(lubLCDData);                
        if(drvI2C1_ReadACK(ACK_LOW) != ACK_OK) continue;            

        // load SEG bits from addr 0h-13h to RAM.. 
        lubStatus = 0;
        for(lubCtr=0;lubCtr<10;lubCtr++){    
                    
            // Send data @ address 0h - 13h...
            lubData = gubLCDSEGData[lubCtr].Byte;
            drvI2C1_SendByte(lubData);
            
            if(drvI2C1_ReadACK(ACK_LOW) != ACK_OK){
                lubStatus = 1; // bad transmission!...
                break;                    
            }
        }
        
        if(lubStatus == 1) continue;

        drvI2C1_StopBit();
        
        break;
        
    }while(1);    
    
    // disable LCD update after its task is finished...
    gbUpdateLCDFlag = FALSE;
    
#endif
    
}


/*    End of File   */





    
    

