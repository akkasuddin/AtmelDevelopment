/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm                                            **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc. and Blue Ocean Innovation Ltd.                            **
 **                                                                           **
 **  Copryright(c) 2010. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Blue Ocean Innovation Ltd.                        **
 **  Project#            :  449                                               **
 **  Creation date       :  08/20/2010                                        **
 **  Programmer          :  Michael J. Mariveles                              **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : devI2CEEPROM_24AA256.c                                            **
 **                                                                           **
 **  Description :                                                            **
 **    - external 32kB EEPROM device driver.                                  **
 **                                                                           **
 ******************************************************************************/

//#include "hwPIC24F.h"
#include "types.h"
#include "drvI2C_Wrapper.h"
#include "devI2CEEPROM_24AA256.h"

// 24AA256 EEPROM 32k byte I2C command...
#define I2C2_CMD_WR 0xA0
#define I2C2_CMD_RD 0xA1


/*******************************************************************************
**  Function     : devEEPROM_Init
**  Description  : initializes EEPROM I2C communication driver.
**  PreCondition : must configure first the control I/O's of the assigned I2C driver
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void devEEPROM_Init(void){

    eeprom_i2c_init();
}    
    
/*******************************************************************************
**  Function     : devEEPROM_ByteWrite
**  Description  : write byte data to specific EEPPOM offset address.
**  PreCondition : none
**  Side Effects : none
**  Input        : par_uwWordAddress - EEPROM offset address 
**                 par_ubByte - byte data to write
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/    
void devEEPROM_ByteWrite(UINT16 par_uwWordAddress,UINT8 par_ubByte)
{
	eeprom_i2c_write(&par_ubByte, 1, par_uwWordAddress);

#ifdef EEPROM_LEGACY_CODE

    UINT16 *luwWordAddress;
    UINT8  lubByteData[2] = {0,0};
  
    luwWordAddress = (UINT16 *)lubByteData;
    
    *luwWordAddress = par_uwWordAddress;   
	 
    do{
        
        drvI2C2_StartBit();            
            
        drvI2C2_SendByte(I2C2_CMD_WR); // send write command        
        if(drvI2C2_ReadACK(ACK_LOW) != ACK_OK) continue;            
            
        drvI2C2_SendByte(lubByteData[1]); // send word address MSB Byte
        if(drvI2C2_ReadACK(ACK_LOW) != ACK_OK) continue;            
        drvI2C2_SendByte(lubByteData[0]); // send word address LSB Byte          
        if(drvI2C2_ReadACK(ACK_LOW) != ACK_OK) continue;
        
        drvI2C2_SendByte(par_ubByte); // send Data  0x89        
        if(drvI2C2_ReadACK(ACK_LOW) != ACK_OK) continue;       
        
        drvI2C2_StopBit();
        
        break;
            
    }while(1);
      
#endif 
 
}        

/*******************************************************************************
**  Function     : devEEPROM_PageWrite
**  Description  : EEPROM page write option
**  PreCondition : none
**  Side Effects : none
**  Input        : par_uwWordAddress - EEPROM starting offset address
**                 par_ubByte - array of bytes value to write
**                 par_uwNumOfBytes - number of bytes to write
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void devEEPROM_PageWrite(UINT16 par_uwWordAddress,UINT8 *par_ubByte,UINT16 par_uwNumOfBytes)
{
	if(par_uwNumOfBytes<=64) eeprom_i2c_write(par_ubByte, par_uwNumOfBytes, par_uwWordAddress);
	else eeprom_i2c_write(par_ubByte, 64, par_uwWordAddress);
	
#ifdef EEPROM_LEGACY_CODE

    UINT16 *luwWordAddress;
    UINT8  lubByteData[2] = {0,0};    
    register UINT8 lubPageOffset;   // max = 63 bytes..    
    UINT8 lubData; 
  
    luwWordAddress = (UINT16 *)lubByteData;

    do{
        
        drvI2C2_StartBit();            
            
        drvI2C2_SendByte(I2C2_CMD_WR); // send write command        
        if(drvI2C2_ReadACK(ACK_LOW) != ACK_OK) continue;            
        
        *luwWordAddress = par_uwWordAddress;    
            
        drvI2C2_SendByte(lubByteData[1]); // send word address MSB Byte
        if(drvI2C2_ReadACK(ACK_LOW) != ACK_OK) continue;            
        drvI2C2_SendByte(lubByteData[0]); // send word address LSB Byte          
        if(drvI2C2_ReadACK(ACK_LOW) != ACK_OK) continue;
        
        // reset page offset....
        lubPageOffset = 0;      
        
        do{
            lubData = par_ubByte[lubPageOffset];
                        
            drvI2C2_SendByte(lubData); 
            if(drvI2C2_ReadACK(ACK_LOW) != ACK_OK) continue;       

            // if less than or equal 63 byte then stop here...
            if(lubPageOffset == (par_uwNumOfBytes-1)){ 
                drvI2C2_StopBit();
                return;
            }    
            
        }while(++lubPageOffset <= 63); // on-chip page buffer size 0-63 bytes only...
        
        drvI2C2_StopBit();
        
        break;
            
    }while(1);
	
#endif

}

/*******************************************************************************
**  Function     : devEEPROM_RandomRead
**  Description  : reads EEPROM array of byte data
**  PreCondition : none
**  Side Effects : none
**  Input        : par_uwWordAddress - offset address
**  Output       : par_ubByte - array of bytes value
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void devEEPROM_RandomRead(UINT16 par_uwWordAddress,UINT8 *par_ubByte)
{
	eeprom_i2c_read(par_ubByte, 1, par_uwWordAddress);//Send address

#ifdef EEPROM_LEGACY_CODE
	
    UINT16 *luwWordAddress;
    UINT8  lubByteData[2] = {0,0};    
    UINT8  lubByteVal; 
  
    luwWordAddress = (UINT16 *)lubByteData;
           

    do{
        
        drvI2C2_StartBit();            
        
        // Send write command..    
        drvI2C2_SendByte(I2C2_CMD_WR); 
        if(drvI2C2_ReadACK(ACK_LOW) != ACK_OK) continue;            
        
        // Send Word address..
        *luwWordAddress = par_uwWordAddress;    
         
        drvI2C2_SendByte(lubByteData[1]); // send word address MSB Byte
        if(drvI2C2_ReadACK(ACK_LOW) != ACK_OK) continue;            
        drvI2C2_SendByte(lubByteData[0]); // send word address LSB Byte          
        if(drvI2C2_ReadACK(ACK_LOW) != ACK_OK) continue;
        
        // initiate start bit again for Bit reading..
        drvI2C2_StopStartBit();             
        drvI2C2_StartBit();        
        
        // send read command...
        drvI2C2_SendByte(I2C2_CMD_RD); 
        if(drvI2C2_ReadACK(ACK_LOW) != ACK_OK) continue;            
        
        // read data byte...                
        drvI2C2_ReadByte(&lubByteVal); 
        if(drvI2C2_ReadACK(ACK_HIGH) != NO_ACK) continue;       
                        
        drvI2C2_SetSDAOut();
        
        drvI2C2_StopBit();
        
        break;
            
    }while(1);
    
    *par_ubByte = lubByteVal;
	
#endif

}

/*   End Of File   */


