/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm                                            **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc. and Blue Ocean Innovation Ltd.                            **
 **                                                                           **
 **  Copryright(c) 2010. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Blue Ocean Innovation Ltd.                        **
 **  Project#            :  449                                               **
 **  Creation date       :  08/20/2010                                        **
 **  Programmer          :  Michael J. Mariveles                              **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : devI2CEEPROM_24AA256.h                                            **
 **                                                                           **
 **  Description :                                                            **
 **    - external 32kB EEPROM device driver.                                  **
 **                                                                           **
 ******************************************************************************/
 
#ifndef __DEVI2CEEPROM_24AA256_H
#define __DEVI2CEEPROM_24AA256_H

void devEEPROM_ByteWrite(UINT16 par_uwWordAddress,UINT8 par_ubByte);
void devEEPROM_PageWrite(UINT16 par_uwWordAddress,UINT8 *par_ubByte,UINT16 par_uwNumOfBytes);
void devEEPROM_SeqRead(UINT16 par_uwWordAddress,UINT8 *par_ubByte,UINT16 par_uwNumOfBytes);
void devEEPROM_RandomRead(UINT16 par_uwWordAddress,UINT8 *par_ubByte);
void devEEPROM_Init(void);

#endif /* __DEVI2CEEPROM_24AA256_H */
