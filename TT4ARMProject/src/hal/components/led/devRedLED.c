/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm Variant                                    **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc.															  **
 **                                                                           **
 **  Copryright(c) 2015. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Sensitech Inc.									  **
 **  Project#            :  Issue #865                                        **
 **  Creation date       :  2/6/2015 2:22:24 PM                               **
 **  Programmer          :  Jon Waisnor										  **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : devRedLED.c                                                       **
 **                                                                           **
 **  Description :                                                            **
 **    - Routines to init, on, off and toggle Red LED                         **
 **                                                                           **
 ******************************************************************************/

#include "devLED.h"
#include "devRedLED.h"
#include "TT4USB_Board.h"
#include "asf.h"


static devLED_t RedLED = {	.gpio	= RED_LED_GPIO,
							.flags	= RED_LED_ALL_FLAG,
							.pin	= RED_LED_PIN,
							.isOn	= false};

status_code_t	devRedLED_init(void)
{
	return devLED_init(&RedLED);
}

status_code_t	devRedLED_on(void)
{
	return devLED_on(&RedLED);
}

status_code_t	devRedLED_off(void)
{
	
	return devLED_off(&RedLED);
}

status_code_t	devRedLED_toggle(void)
{
	
	return devLED_toggle(&RedLED);
}
