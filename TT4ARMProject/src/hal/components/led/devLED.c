/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm Variant                                    **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc.															  **
 **                                                                           **
 **  Copryright(c) 2015. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Sensitech Inc.									  **
 **  Project#            :  Issue #865                                        **
 **  Creation date       :  2/5/2015 2:32:52 PM                               **
 **  Programmer          :  Jon Waisnor										  **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : devLED.c                                                          **
 **                                                                           **
 **  Description :                                                            **
 **    - Routines to enable, disable, toggle SAMG55 driven LEDS               **
 **                                                                           **
 ******************************************************************************/
 
#include "devLED.h"
#include "ioport.h"

/*******************************************************************************
**  Function     : devLED_init(parameters)
**  Description  : Creates data structure to support a LED and initializes the
**					peripherals to support it.
**  PreCondition : Pass it a valid devLED_t structure.
**  Side Effects : The LED is initialized and placed in the Off state.
**  Input        : none
**  Output       : status_code
** _____________________________________________________________________________
**  Date    Author                Changes
**  05FEB2015	JWaisnor		First release
*******************************************************************************/

status_code_t	devLED_init( devLED_t* pled)
{
	status_code_t rc = ERR_UNSUPPORTED_DEV;
	
	if( pled )
	{
		ioport_set_pin_dir(pled->gpio, IOPORT_DIR_OUTPUT);
		ioport_set_pin_level(pled->gpio, IOPORT_PIN_LEVEL_LOW);
		
		pled->isOn = false;
		rc = STATUS_OK;
	}

	return(rc);
}
/*******************************************************************************
**  Function     :
**  Description  :
**  PreCondition :
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  05FEB2015	JWaisnor		First release
*******************************************************************************/
	
status_code_t	devLED_on( devLED_t* pled)
{
	status_code_t rc = STATUS_OK;
	
	if( pled )
	{
		// Turn the led on
		ioport_set_pin_level(pled->gpio, IOPORT_PIN_LEVEL_LOW);

		// mark status as such
		pled->isOn = true;
	}
	else
	{
		rc = ERR_UNSUPPORTED_DEV; 
	}
	
	return(rc);
}

/*******************************************************************************
**  Function     :
**  Description  :
**  PreCondition :
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  05FEB2015	JWaisnor		First release
*******************************************************************************/

status_code_t	devLED_off( devLED_t* pled)
{
	status_code_t rc = STATUS_OK;
	
	if( pled )
	{
		// turn the led off
		ioport_set_pin_level(pled->gpio, IOPORT_PIN_LEVEL_HIGH);

		// mark it as such
		pled->isOn = false;
	}
	else
	{
		rc = ERR_UNSUPPORTED_DEV;
	}
	
	return(rc);
}

/*******************************************************************************
**  Function     :
**  Description  :
**  PreCondition :
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  05FEB2015	JWaisnor		First release
*******************************************************************************/

status_code_t	devLED_toggle( devLED_t* pled)
{
	status_code_t rc = STATUS_OK;
	
	if( pled )
	{
		if( pled->isOn )
		{
			if( STATUS_OK == devLED_off(pled) )
				pled->isOn = false;
		}
		else
		{
			if( STATUS_OK == devLED_on(pled) )
				pled->isOn = true;
		}
	}
	else
	{
		rc = ERR_UNSUPPORTED_DEV;
	}
	
	return(rc);
}


