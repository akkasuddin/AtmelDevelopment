/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm Variant                                    **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc.															  **
 **                                                                           **
 **  Copryright(c) 2015. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Sensitech Inc.									  **
 **  Project#            :  Issue #865                                        **
 **  Creation date       :  2/5/2015 2:11:56 PM                              **
 **  Programmer          :  Jon Waisnor										  **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : devLED.c                                                          **
 **                                                                           **
 **  Description :                                                            **
 **    - Routines to enable, disable, toggle SAMG55 driven LEDS               **
 **                                                                           **
 ******************************************************************************/

#ifndef DEVLED_H_
#define DEVLED_H_

#include "status_codes.h"
#include "stdint-gcc.h"
#include "stdbool.h"

typedef struct {
					uint32_t  gpio;
					uint32_t  flags;
					uint32_t  pin;
	
					bool	isOn;
} devLED_t;

status_code_t	devLED_init( devLED_t* led);
status_code_t	devLED_on( devLED_t* led);
status_code_t	devLED_off( devLED_t* led);
status_code_t	devLED_toggle( devLED_t* led);





#endif /* LED_H_ */