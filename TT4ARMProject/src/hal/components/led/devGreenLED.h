/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm Variant                                    **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc.															  **
 **                                                                           **
 **  Copryright(c) 2015. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Sensitech Inc.									  **
 **  Project#            :  Issue #865                                        **
 **  Creation date       :  2/6/2015 2:22:24 PM                               **
 **  Programmer          :  Jon Waisnor										  **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : devGreenLED.c                                                     **
 **                                                                           **
 **  Description :                                                            **
 **    - Routines to init, on, off and toggle Green LED                       **
 **                                                                           **
 ******************************************************************************/

#ifndef DEVGREENLED_H_
#define DEVGREENLED_H_

#include "devLED.h"

status_code_t	devGreenLED_init(void);
status_code_t	devGreenLED_on(void);
status_code_t	devGreenLED_off(void);
status_code_t	devGreenLED_toggle(void);

#endif /* DEVGREENLED_H_ */