/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm Variant                                    **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc.															  **
 **                                                                           **
 **  Copryright(c) 2015. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Sensitech Inc.									  **
 **  Project#            :  Issue #865                                        **
 **  Creation date       :  2/6/2015 2:22:24 PM                               **
 **  Programmer          :  Jon Waisnor										  **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : devGreenLED.c                                                     **
 **                                                                           **
 **  Description :                                                            **
 **    - Routines to init, on, off and toggle Green LED                       **
 **                                                                           **
 ******************************************************************************/

#include "devLED.h"
#include "devGreenLED.h"
#include "TT4USB_Board.h"
#include "asf.h"


static devLED_t GreenLED = {.gpio	= GREEN_LED_GPIO,
							.flags	= GREEN_LED_ALL_FLAG,
							.pin	= GREEN_LED_PIN,
							.isOn	= false};

status_code_t	devGreenLED_init(void)
{
	return devLED_init(&GreenLED);
}

status_code_t	devGreenLED_on(void)
{
	return devLED_on(&GreenLED);
}

status_code_t	devGreenLED_off(void)
{
	
	return devLED_off(&GreenLED);
}

status_code_t	devGreenLED_toggle(void)
{
	
	return devLED_toggle(&GreenLED);
}
