/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm                                            **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc. and Blue Ocean Innovation Ltd.                            **
 **                                                                           **
 **  Copryright(c) 2010. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Blue Ocean Innovation Ltd.                        **
 **  Project#            :  449                                               **
 **  Creation date       :  08/20/2010                                        **
 **  Programmer          :  Michael J. Mariveles                              **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : appPDF.c                                                          **
 **                                                                           **
 **  Description :                                                            **
 **    - application routines for PDF such as coordinate maaping and plotting.**
 **                                                                           **
 ******************************************************************************/
#include "hwPIC24F.h"
#include "hwSystem.h"
#include "hpdf.h"
#include "hpdf_types.h"
#include "appPDF.h"
#include "commTT4Config.h"
#include "drvRTC.h"
#include "devI2CTempSensor_TMP112.h"
#include "utility.h"
#include "devI2CEEPROM_24AA256.h"
#include <SetJmp.h>


// PDF Report files definitions.. 
#define PDF_ORIGINATOR_NOTES_MAX_CHARS  96  // 96 chars
#define PDF_MAX_MARKER_COUNT            100 // 100 markers that memory can handle..

//Time zone definitions

#define MAX_GMT_TIMEZONE                56 // 15 minute increments..Odd? GMT + 14:00 Kiribati Rawaki Islands
#define MIN_GMT_TIMEZONE               -48 // GMT -12:00
#define DST_SHIFT_SEC                   3600 //3600 seconds in an hour

#define SHIPPING_Y_SHIFT           -20.0f
#define SHIPPING_ALARM_CHK_SHIFT   -58.0f
#define SHIPPING_X_SHIFT             3.0f
#define SHIPPING_LOGO_SHIFT         11.0f

float shift_alarm = 0.0; //shifts the pdf around if shipping info is enabled
float shift_y_axis = 0.0;
float shift_x_axis = 0.0;

// PDF file local functions...
void __attribute__ ((interrupt, auto_psv)) _T2Interrupt (void);
void PDF_Timer2Interrupt_LED_Blinker_Enable(void);
void PDF_Timer2Interrupt_LED_Blinker_Disable(void);
void PDF_SetText( HPDF_Page page,  float x,  float y, const char *text);
void PDF_DrawLine( HPDF_Page page,  float x1,  float y1,  float x2,  float y2);
void PDF_DrawLogo( HPDF_Page par_hpdfPage,  HPDF_Font par_hpdfFont,  float par_fX,  float par_fY);
void PDF_DrawRectangle( HPDF_Page page,  float x,  float y,  float width,  float height);
void PDF_SetFileAttributes( HPDF_Doc par_hpdfdoc);
void PDF_DrawCheckMark( HPDF_Page par_hpdfPage,  BOOL par_bChk);
void PDF_DrawGraphGridLines( HPDF_Page page,  HPDF_Font def_font);
void PDF_Angle_SetText( HPDF_Page page,  HPDF_REAL xStar,  HPDF_REAL y,  HPDF_REAL angle, const char *label);
void PDF_CalculateGraphScaleValues(void);
void PDF_FormatTimeToString(UINT32 par_udwStartTimeStamp, char *par_strTimeStamps);
void PDF_FormatTimeSecsToDHMSString(UINT8 par_ubType, UINT32 par_udwSecs, char *par_strDayHrMinSec);
void PDF_FormatTimeTripLength(UINT32 par_udwSecs, char *par_strDayHrMin);
void PDF_FormatTimeSecsDHMSToString(UINT32 par_udwSecs, char *par_strDayHrMinSec);
void PDF_FormatTimeZoneToGMTString(INT8 hr, INT8 min, char* parGMTstr);
void PDF_DrawGraph_StartLine( HPDF_Page page,  float x1,  float y1);
void PDF_DrawGraph_EndLine( HPDF_Page page,  float x1,  float y1);
void PDF_DrawMarker(HPDF_Page page, float x, float y);
void PDF_GraphPlotterDisplay(HPDF_Page page, HPDF_Font def_font);
void PDF_GetHighLowExtremeSetPoints(void);
void PDF_Summary_Threshold(UINT8 par_ubAlarmNum, char *par_strValue);                                         
void PDF_Summary_Status(UINT8 par_ubAlarmNum, char *par_strValue);
void PDF_Summary_Triggered(UINT8 par_ubAlarmNum, char *par_strValue);
void PDF_Summary_NumberOfEvents(UINT8 par_ubAlarmNum, char *par_strValue);
void PDF_Summary_TotalTime(UINT8 par_ubAlarmNum, char *par_strValue);
void PDF_Summary_TemperatureTime(float par_fTemp, UINT32 par_udwTimeInSec, char *par_strValue);
void PDF_Summary_IdealSettings(char *par_strValue);
void PDF_Summary_AlarmSettings(UINT8 par_ubAlarmNum, char *par_strValue);
void PDF_Summary_StdDeviation(char *par_strValue);
void PDF_Summary_MeanKinetic(char *par_strValue);
void PDF_OriginatorNotes(char *par_strValue);
void PDF_Summary_Calculate_StdDevAndMKT(void);
void PDF_Format_ShippingLabel_Info(HPDF_Page par_hpdfPage, char* parShippingLabelstr);
// arrow marker utilities...
UINT8 CheckMarkPoints(UINT16 val);
short ChangeValToTemp(short val);
INT32 TimeZoneDSTShift;       //Will hold the timezone/DST shift in UTC seconds
char labeltemp[45];
char labelvaluetemp[45];


/*******************************************************************************
**  Function     : error_handler
**  Description  : PDF report memory error handler.
**  PreCondition : setjmp(env) must be set first as memory marker..
**  Side Effects : none
**  Input        : 
**                 error_no - unused
**                 detail_no - unused
**                 *user_data - unused
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
static jmp_buf env;
void error_handler (  HPDF_STATUS   error_no,
                      HPDF_STATUS   detail_no, 
                      void         *user_data)
{   
    longjmp(env, 1);
}

/*******************************************************************************
**  Function     : usb_unplugged_handler
**  Description  : USB nnplugged handler while PDF is generating the file unfinished.
**  PreCondition : setjmp(env) must be set first as memory marker..
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void usb_unplugged_handler (void)
{   
    longjmp(env, 1);
}



const HPDF_UINT16 DASH_MODE1[] = {1, 1};
const HPDF_UINT16 DASH_MODE2[] = {2, 1};
const HPDF_UINT16 DASH_MODE3[] = {8, 7, 2, 7};

const static char* owner_passwd = "*sensitech*owner";

// static var reset flag...
BOOL gb460PtsResetStaticDataFlag;

// download time storage...
extern volatile UINT32 gudwDeviceStoppedDownloadTime;

// the time when the device was plugged to the USB to download temperature datapoints...
extern volatile UINT32 gudwDeviceReadOnTime;


// LFN long filename storage var...
extern char gLfname[64];
// PDF global filename storage...
char gstrPDFFilename[64];

// number of events control byte...
extern __sTRIG_EVENTS_STAT _guwTrigEvents[7];

// EEPROM configuration control structure register...
extern __ALARM_CONFIGURATION_AREA       _gsAlarmConfig[6]; 
extern __USER_CONFIGURATION_AREA        _gsUser_Configuration_Area;
extern __SENSITECH_CONFIGURATION_AREA   _gsSensitech_Configuration_Area;


// EEPROM control bit manipulator structure register...
extern __sBITS_GENCFGOPT_BYTE           _gsGenCfgOptByte;
extern __sBITS_EXTOPT_BYTE              _gsExtdOptByte;

// extreme temperature points global storage....
extern _sLOGMINMAXTEMPDATA              _gsLogMinMaxTempData;

// ideal temparature limits data...
extern __sBITS_USERCFG_DATA             _gsUserCfgAreaData;

// Stop mode status register...
extern UINT8 gubStopDeviceStatus;

// PDF memory manager's free running allocation size...
extern UINT16 gAllocSize; // mjm2
// EDS stream memory accumulator...
extern UINT16 guwPdfStreamMemSize; // mjm2
// EDS stream data length accumulator...
extern UINT16 guwPdfStreamDataSize; // mjm2
// actual PDF stream data buffer accumulator...
extern UINT16 guwPdfDataSize; // mjm2
// EDS stream data buffer...
extern __eds__ UINT8 gubPDFStreamDataBuffer[23000u] __attribute__((space(eds))); //mjm2

// Running MKT calculation value....
extern volatile long double gudwMKT_Value;
//extern float gfMKT_RunningTemp;
// Standard deviation value....
long double gfTemperatureStdDev;

/*******************************************************************************
**  Function     : PDF_SetText
**  Description  : print's a text characters to the PDF report in rectangular format.
**  PreCondition : must initialize first the PDF memory allocation
**  Side Effects : none
**  Input        : 
**                 page - PDF page handler
**                 x - horizontal coordinate
**                 y - vertical coordonate
**                 *text - array of characters
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
******************************************************************************/
void PDF_SetText( HPDF_Page page,  float x,  float y, const char *text){
    
    HPDF_Page_TextOut(page, x, y, text);
}

/*******************************************************************************
**  Function     : PDF_Angle_SetText
**  Description  : print's a text characters to the PDF report in angular format.
**  PreCondition : must initialize first the PDF memory allocation
**  Side Effects : none
**  Input        : 
**                 page - PDF page handler
**                 x - horizontal coordinate
**                 y - vertical coordonate
**                 degree - angle in degrees
**                 *label - array of characters
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void PDF_Angle_SetText( HPDF_Page page,  HPDF_REAL x,  HPDF_REAL y,  HPDF_REAL degree, const char *label)
{
    HPDF_DOUBLE rad;
    /* Calcurate the radian value. */
    rad = degree / 180 * 3.141592f; 

    HPDF_Page_BeginText (page);
    HPDF_Page_MoveTextPos (page, x, y); 
    HPDF_Page_SetTextMatrix (page, cos(rad), sin(rad), -sin(rad), cos(rad), x, y);
    HPDF_Page_ShowText (page, label);
    HPDF_Page_EndText (page);
}    

/*******************************************************************************
**  Function     : PDF_DrawLine
**  Description  : draw a straight line
**  PreCondition : must initialize first the PDF memory allocation
**  Side Effects : none
**  Input        :  
**                 page - PDF page handler
**                 x1 - 1st horizontal coordinate
**                 y1 - 1st vertical coordonate
**                 x2 - 2nd horizontal coordinate
**                 y2 - 2nd horizontal coordinate
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void PDF_DrawLine( HPDF_Page page,  float x1,  float y1,  float x2,  float y2){

	HPDF_Page_MoveTo (page,x1,y1);
	HPDF_Page_LineTo (page,x2,y2);
	HPDF_Page_Stroke (page);    
}

/*******************************************************************************
**  Function     : PDF_DrawGraph_StartLine
**  Description  : first point coordinate marker
**  PreCondition : must initialize first the PDF memory allocation
**  Side Effects : none
**  Input        :  
**                 page - PDF page handler
**                 x1 - horizontal coordinate
**                 y1 - vertical coordonate
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void PDF_DrawGraph_StartLine( HPDF_Page page,  float x1,  float y1){
    HPDF_Page_MoveTo (page,x1,y1);
}

/*******************************************************************************
**  Function     : PDF_DrawGraph_EndLine
**  Description  : 2nd point coordinate marker
**  PreCondition : must initialize first the PDF memory allocation
**  Side Effects : none
**  Input        :  
**                 page - PDF page handler
**                 x1 - horizontal coordinate
**                 y1 - vertical coordonate
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void PDF_DrawGraph_EndLine( HPDF_Page page,  float x1,  float y1){
    HPDF_Page_LineTo (page,x1,y1);
}

/*******************************************************************************
**  Function     : PDF_DrawRectangle
**  Description  : draws rectangle shape to the PDF report file
**  PreCondition : must initialize first the PDF memory allocation
**  Side Effects : none
**  Input        :  
**                 page - PDF page handler
**                 x1 - horizontal coordinate
**                 y1 - vertical coordonate
**                 width - horizontal line lenght
**                 height - vertical line lenght
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void PDF_DrawRectangle( HPDF_Page page,  float x,  float y,  float width,  float height){
	
	HPDF_Page_Rectangle(page, x, y, width, height);
	HPDF_Page_Stroke(page);
}
  
/*******************************************************************************
**  Function     : PDF_DrawLogo
**  Description  : draws Senstitech logo
**  PreCondition : must initialize first the PDF memory allocation
**  Side Effects : none
**  Input        : 
**                 par_hpdfPage - PDF page handler
**                 par_hpdfFont - PDF font
**                 par_fX - horizontal coordinate
**                 par_fY - vertical coordinate
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/  
void PDF_DrawLogo( HPDF_Page par_hpdfPage,  HPDF_Font par_hpdfFont,  float par_fX,  float par_fY){

    float shift_logo = 0.0;

    if (_gsGenCfgOptByte.GenOpt.Byte0.ShippingInfoEna)
        shift_logo = SHIPPING_LOGO_SHIFT;
		
	// Sensitech logo...
    HPDF_Page_SetLineWidth (par_hpdfPage, 1.0f);

	  HPDF_Page_SetRGBStroke (par_hpdfPage, 0.0f, 0.4f, 0.8f);    
    HPDF_Page_SetRGBFill (par_hpdfPage, 0.0f, 0.4f, 0.8f);
    HPDF_Page_Circle (par_hpdfPage, par_fX, par_fY + shift_logo, 23.0f);
    HPDF_Page_Fill (par_hpdfPage);
    
    HPDF_Page_SetRGBStroke (par_hpdfPage, 1.0f, 1.0f, 1.0f);    
    HPDF_Page_SetRGBFill (par_hpdfPage, 1.0f, 1.0f, 1.0f);
    HPDF_Page_Circle (par_hpdfPage, par_fX+1, par_fY-0.5 + shift_logo, 22.3f);
    HPDF_Page_Fill (par_hpdfPage);
    
    HPDF_Page_SetRGBStroke (par_hpdfPage, 0.0f, 0.0f, 0.0f);    

    HPDF_Page_SetRGBFill (par_hpdfPage, 0.0f, 0.4f, 0.8f);
    HPDF_Page_SetFontAndSize (par_hpdfPage, par_hpdfFont, 15);
    
    HPDF_Page_BeginText (par_hpdfPage);    
    
	  PDF_SetText(par_hpdfPage, par_fX-20, par_fY-5 + shift_logo, "SENSI");
    PDF_SetText(par_hpdfPage, par_fX-19.4f, par_fY-5 + shift_logo, "SENSI");
    PDF_SetText(par_hpdfPage, par_fX-18.8f, par_fY-5 + shift_logo, "SENSI");
    PDF_SetText(par_hpdfPage, par_fX-20+45, par_fY-5 + shift_logo, "TECH");
    
    HPDF_Page_SetRGBFill (par_hpdfPage, 0.0f, 0.0f, 0.0f);
    HPDF_Page_SetFontAndSize (par_hpdfPage, par_hpdfFont, 5.5f);
    PDF_SetText(par_hpdfPage, par_fX+2, par_fY-5-6 + shift_logo, "COLD CHAIN VISIBILITY");
    PDF_SetText(par_hpdfPage, par_fX+1.8f, par_fY-5-6 + shift_logo, "COLD CHAIN VISIBILITY");
    
    HPDF_Page_EndText (par_hpdfPage);    
	
}

/*******************************************************************************
**  Function     : PDF_SetFileAttributes
**  Description  : load PDF file attributes
**  PreCondition : must initialize first the PDF memory allocation
**  Side Effects : none
**  Input        : par_hpdfdoc - PDF document handler
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void PDF_SetFileAttributes( HPDF_Doc par_hpdfdoc){
    
     // save the total stream data bytes excluding the header and footer information...
    guwPdfStreamDataSize = guwPdfStreamMemSize; // mjm2 deadly...   

    HPDF_Date lPdfDate;
    _UTIL_DATE_TIME_24HR _lsDateTime;
    
	// file attributes...
    HPDF_SetInfoAttr (par_hpdfdoc, HPDF_INFO_AUTHOR, "Sensitech Inc.");
    HPDF_SetInfoAttr (par_hpdfdoc, HPDF_INFO_TITLE, "TT4USB Multi-Alarm PDF Report");
    HPDF_SetInfoAttr (par_hpdfdoc, HPDF_INFO_SUBJECT, "TempTale4 USB");    
    
    // PDF report time stamp...
    //drvRTC_ConvSecsToDateTime(gudwDeviceStoppedDownloadTime,&_lsDateTime);
    drvRTC_ConvSecsToDateTime(gudwDeviceReadOnTime,&_lsDateTime); // debug mjm 05062013...
    
	  lPdfDate.year = _lsDateTime.Year;
	  lPdfDate.month = _lsDateTime.Mon;
	  lPdfDate.day = _lsDateTime.Day;
	  lPdfDate.hour = _lsDateTime.Hour;
	  lPdfDate.minutes = _lsDateTime.Min;
	  lPdfDate.seconds =  _lsDateTime.Sec;
	  lPdfDate.ind = 'Z';
	  lPdfDate.off_hour = 0;
	  lPdfDate.off_minutes = 0;    
    
    // update PDF attribute...
    HPDF_SetInfoDateAttr (par_hpdfdoc, HPDF_INFO_CREATION_DATE , lPdfDate);     
    HPDF_SetPassword (par_hpdfdoc, owner_passwd, NULL);
    HPDF_SetPermission (par_hpdfdoc, HPDF_ENABLE_READ | HPDF_ENABLE_PRINT);    
    HPDF_SetEncryptionMode (par_hpdfdoc, HPDF_ENCRYPT_R3, 16);     
}



/*******************************************************************************
**  Function     : PDF_DrawCheckMark
**  Description  : draws a X and Check mark at the top of PDF document.
**  PreCondition : must initialize first the PDF memory allocation
**  Side Effects : none
**  Input        : 
**                 par_hpdfPage - PDF page handler
**                 par_bChk - status 1-ok , 0-triggered
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void PDF_DrawCheckMark( HPDF_Page par_hpdfPage,  BOOL par_bChk){

    float shift = 0.0; //shifts the pdf around if shipping info is enabled

    if (_gsGenCfgOptByte.GenOpt.Byte0.ShippingInfoEna)
        shift = SHIPPING_ALARM_CHK_SHIFT;
   
    HPDF_Page_SetLineWidth (par_hpdfPage, 3.7f);
    
    if(par_bChk){
        // draw Check mark!!	    
        HPDF_Page_SetRGBStroke (par_hpdfPage, 0.2f, 0.6f, 0.0f);
        HPDF_Page_SetRGBFill (par_hpdfPage, 0.2f, 0.6f, 0.0f);
        HPDF_Page_SetLineJoin (par_hpdfPage, HPDF_BEVEL_JOIN);
        HPDF_Page_MoveTo (par_hpdfPage, 117.0f, 689.3f + shift);
        HPDF_Page_LineTo (par_hpdfPage, 125.3f, 683.0f + shift);
        HPDF_Page_LineTo (par_hpdfPage, 137.0f, 700.0f + shift);
        HPDF_Page_Stroke (par_hpdfPage);
    }
    else{    
        // draw X mark!!	    
        HPDF_Page_SetRGBStroke(par_hpdfPage, 1.0f, 0.0f, 0.0f);
        HPDF_Page_SetRGBFill (par_hpdfPage, 1.0f, 0.0f, 0.0f);
        PDF_DrawLine(par_hpdfPage, 117.0f, 699.5f + shift, 137.0f,682.5f + shift);
        PDF_DrawLine(par_hpdfPage, 117.0f, 682.5f + shift, 137.0f,699.5f + shift);
    }
	
}
  
/*******************************************************************************
**  Function     : appPDF_GenerateFile
**  Description  : main routine to create PDF report file
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : 
**                  0 - PDF file generation has an error encountered.
**                  1 - PDF file success
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
#define cXY  15  // text position for MKT Alarm feature ..  
//#define cMKT_DEFAULT   83144  // MKT activation energy is zero value then use this constant as default.. as per Peter Nunes requirement, 10312013 mjm
//#define cMKT_ACTIVE_ENERGY_MAX   125000ul
//#define cMKT_ACTIVE_ENERGY_MIN   42000ul

// temporary max min values requested by Peter ... 11/01/2013
//#define cMKT_ACTIVE_ENERGY_MAX   500000ul
//#define cMKT_ACTIVE_ENERGY_MIN   0ul

/*
extern long double temp_gudwMKT_Value;
extern long double temp_gudwMKT_Accumulator;
extern long double temp_gudwKelvinTemp;
extern long double temp_gudwActEngeryConst;
extern long double temp_gudwDataPoints;
extern long double temp_gudwRunTemp;
extern long double temp_gudwFtemp;
*/

UINT8 appPDF_GenerateFile(void){
    
    HPDF_Doc  gpdf;
    HPDF_Page gpage;
    HPDF_Font gdefault_font;            
    char lstrValInput0[12],lstrValInput1[20],lstrValInput2[25],lstrValInput3[100];
    register HPDF_REAL height, width;
    register char lcSymbolTol = 177; // ?
    register char lcSymbolR = 174; // R
    INT8 hour_shift = 0;
    INT8 minute_shift = 0; //For PDF/daylight savings time shift
    
 
    register UINT16 lVarSize = 0;   //mjm32

    register UINT16 luwCtr;
    for(luwCtr=0;luwCtr<23000;luwCtr++) gubPDFStreamDataBuffer[luwCtr] = 0;// NULL... mjm
    
    guwPdfStreamMemSize = 0; // mjm2
    gAllocSize = 0; // mjm2
    guwPdfDataSize = 0; // mjm2  
    guwPdfStreamDataSize = 0; // mjm2


	// allocate pdf doc...
    gpdf = HPDF_New (error_handler, NULL);
    
    if (!gpdf) {        
        // heap memory allocation ERROR, unable to generate PDF file!...mjm
        return(FALSE);
    } 

    if (setjmp(env)) {        						
        // in the middle of PDF file generation a heap memory allocation ERROR was encountered!...mjm
        lVarSize = guwPdfStreamMemSize;
        HPDF_Free (gpdf);
        return(FALSE);
    }

	// page settings!!
    gpage = HPDF_AddPage (gpdf);    
    HPDF_Page_SetHeight (gpage, 792.0f);
    HPDF_Page_SetWidth (gpage, 612.0f);    
    height = HPDF_Page_GetHeight (gpage);
    width = HPDF_Page_GetWidth (gpage);
    

    // font settings!!
    gdefault_font = HPDF_GetFont (gpdf, "Helvetica", "WinAnsiEncoding");
    
    HPDF_Page_SetRGBFill (gpage, 0.0f, 0.0f, 0.0f); // stream starts here!...mjm
    HPDF_Page_SetFontAndSize (gpage, gdefault_font, 14);
    HPDF_Page_SetLineWidth (gpage, 1.0f);	

    
    HPDF_Page_BeginText (gpage);   
     
   if (_gsGenCfgOptByte.GenOpt.Byte0.ShippingInfoEna)
   {
       shift_alarm = SHIPPING_ALARM_CHK_SHIFT; 
       shift_y_axis = SHIPPING_Y_SHIFT; 
       shift_x_axis = SHIPPING_X_SHIFT; 
   }
   else
   {
      shift_alarm = 0.0; //shifts the pdf around if shipping info is enabled
      shift_y_axis = 0.0;
      shift_x_axis = 0.0;
   }
	// draw text!!
	PDF_SetText(gpage, 215.0f, 750.0f, "TempTale  4 USB MultiAlarm");
	if(_gsUser_Configuration_Area.ubAlarmStatus & 0x3F) 
	    PDF_SetText(gpage, 63.4f, 685.9f + shift_alarm, "Alarm");
	else
	    PDF_SetText(gpage, 70.0f, 685.9f + shift_alarm, "OK");
	if (_gsGenCfgOptByte.GenOpt.Byte0.ShippingInfoEna)
      HPDF_Page_SetFontAndSize (gpage, gdefault_font, 12);
	utilULongNumToString(_gsSensitech_Configuration_Area.udwManufacturingSerialNumber,(UINT8 *)&lstrValInput1[0]);
	sprintf((char*)&lstrValInput3[0],"Serial #: %s",lstrValInput1);		
	PDF_SetText(gpage, 400.6f, 705.8f - shift_y_axis, lstrValInput3);	
	
    // check number of resets then if zero use manufacturer serial number w/ no trip number...
    if(_gsUser_Configuration_Area.udwTripNumber>0){        
        utilULongNumToString(_gsUser_Configuration_Area.udwTripNumber,(UINT8 *)&lstrValInput0[0]);                        
        sprintf((char*)&lstrValInput3[0],"Trip #:    %s",lstrValInput0);
    }    
    else{    
        // if trip number is zero then substitute the serial number as a trip number, advised by Peter in email.. 3/22/11
        sprintf((char*)&lstrValInput3[0],"Trip #:    %s",lstrValInput1);
    }
    
    if (_gsGenCfgOptByte.GenOpt.Byte0.ShippingInfoEna)
        PDF_SetText(gpage, 225.6f, 725.8f,lstrValInput3);
    else
        PDF_SetText(gpage, 400.6f, 681.8f,lstrValInput3);

    HPDF_Page_SetFontAndSize (gpage, gdefault_font, 9);

    if (_gsGenCfgOptByte.GenOpt.Byte0.ShippingInfoEna)
    {
        PDF_SetText(gpage, 51.8f, 721.8f, "Shipment Details");
        PDF_SetText(gpage, 51.8f, 648.3f, "Alarm Status");
        PDF_SetText(gpage, 149.6f, 648.3f, "Monitor Configuration");
    }
    else
    {
        PDF_SetText(gpage, 51.8f, 668.3f, "Monitor Configuration");
        PDF_SetText(gpage, 51.8f, 705.8f, "Alarm Status");
    }
	
    PDF_SetText(gpage, 306.0f + shift_x_axis, 668.3f + shift_y_axis, "Monitor Read");

    PDF_SetText(gpage, 51.8f, 625.0f + shift_y_axis, "Recorded Data"); // 608.1
    PDF_SetText(gpage, 306.0f + shift_x_axis, 625.0f + shift_y_axis, "Summary Data"); // 608.1

    PDF_SetText(gpage, 51.8f, 549.0f + shift_y_axis, "Alarm Summary");
    PDF_SetText(gpage, 306.0f, 549.0f + shift_y_axis, "Alarm Data");

    PDF_SetText(gpage, 51.8f, 442.0f + shift_y_axis, "Originator Notes:");

    HPDF_Page_SetFontAndSize (gpage, gdefault_font, 7.2);
    PDF_SetText(gpage, 51.8f, 536.8f + shift_y_axis, "Alarm");
    PDF_SetText(gpage, 95.0f, 536.8f + shift_y_axis, "Alarms Settings");
    PDF_SetText(gpage, 245.0f, 536.8f + shift_y_axis, "Threshold (Type)");
    PDF_SetText(gpage, 320.0f, 536.8f + shift_y_axis, "Total Time");
    PDF_SetText(gpage, 368.0f, 536.8f + shift_y_axis, "Number of Events");
    PDF_SetText(gpage, 434.0f, 536.8f + shift_y_axis, "Triggered");
    PDF_SetText(gpage, 522.0f, 536.8f + shift_y_axis, "Status");     
    
    
    ///////////////////////////   For deletion after use ///////////////////////////////////
    // for MKT feature debuggning only....
  //  PDF_SetText(gpage, 5.0f, 30.0f, "Note: ver 054-5 is a debug version, this is an unofficial release firmware to debug TurboValidatePlus 1.2.1311.1311 w/ MKT support.."); // for MKT debugging... mjm 11/08/2013
  //  PDF_SetText(gpage, 5.0f, 20.0f, "Debug Release date 04-30-2014 , Derrick..");
    //PDF_SetText(gpage, 5.0f, 20.0f, "Alarm 1 is forced to set MKT High Alarm.."); 
    //PDF_SetText(gpage, 5.0f, 10.0f, "Alarm 2 is forced to set MKT Low Alarm.."); 
    
    
    //sprintf((char*)&lstrValInput3[0],"%0.2f",(double)gfMKT_RunningTemp);
    //PDF_SetText(gpage, 5.0f, 50.0f,lstrValInput3);
   
 /*   sprintf((char*)&lstrValInput3[0],"After reset %ld",udwcounter_after_reset);
    PDF_SetText(gpage, 5.0f, 100.0f, lstrValInput3);    
    sprintf((char*)&lstrValInput3[0],"before meas %ld",udwcounter_before_measurement);
    PDF_SetText(gpage, 5.0f, 90.0f, lstrValInput3);    
    sprintf((char*)&lstrValInput3[0],"after meas %ld",udwcounter_after_measurement);
    PDF_SetText(gpage, 5.0f, 80.0f, lstrValInput3);        
    sprintf((char*)&lstrValInput3[0],"run: %.2f",(double)temp_gudwRunTemp);
    PDF_SetText(gpage, 5.0f, 70.0f, lstrValInput3);
    sprintf((char*)&lstrValInput3[0],"dp: %.0f",(double)temp_gudwDataPoints);
    PDF_SetText(gpage, 5.0f, 60.0f, lstrValInput3);
    sprintf((char*)&lstrValInput3[0],"mkt_acc: %f",(double)temp_gudwMKT_Accumulator);
    PDF_SetText(gpage, 5.0f, 50.0f, lstrValInput3);
    sprintf((char*)&lstrValInput3[0],"const: %.4f",(double)temp_gudwActEngeryConst);
    PDF_SetText(gpage, 5.0f, 40.0f, lstrValInput3);        
    */
    
    

    
    ///////////////////////////   For deletion after use ///////////////////////////////////

	HPDF_Page_SetFontAndSize (gpage, gdefault_font, 7);

    //==============================
    // Monitor Configuration....
    //==============================

       UINT8 spaces[31];
       float  programmer_shift = 657.0 - 645.0; //Shift everything up if there is no Programmer name;
       memset((char*)&spaces[0], 0x20, 30);
       spaces[30] = 0x00;

    if (_gsGenCfgOptByte.GenOpt.Byte0.ShippingInfoEna)
    {
       PDF_Format_ShippingLabel_Info(gpage, (char*)&lstrValInput3[0]);
        if ((strcmp((char*)&_gsUser_Configuration_Area.uwUserName[0], (char*)&spaces[0]) != 0) &&
                (_gsUser_Configuration_Area.uwUserName[0] != 0x00))
        {
            PDF_SetText(gpage, 149.6f, 637.0f, "Programmer:");
            PDF_SetText(gpage, 194.8f, 637.0f, (char*)&_gsUser_Configuration_Area.uwUserName[0]);
            programmer_shift = 0.0;
        }
      //PDF_SetText(gpage, 51.6f, 657.0f, "Start Up Delay:");
      //PDF_FormatTimeSecsDHMSToString(_gsUser_Configuration_Area.udwStartUpDelay, (char*)&lstrValInput3[0]);
      //PDF_SetText(gpage, 115.8f, 657.0f, lstrValInput3);
       PDF_SetText(gpage, 149.6f, 625.0f +  programmer_shift, "Start Up Delay:");
       PDF_FormatTimeSecsDHMSToString(_gsUser_Configuration_Area.udwStartUpDelay, (char*)&lstrValInput3[0]);
       PDF_SetText(gpage, 203.8f, 625.0f +  programmer_shift, lstrValInput3);
       PDF_SetText(gpage, 233.6f, 625.0f +  programmer_shift, "Interval:");
       PDF_FormatTimeSecsDHMSToString(_gsUser_Configuration_Area.udwMeasurementInterval, (char*)&lstrValInput3[0]);
       PDF_SetText(gpage, 262.8f, 625.0f +  programmer_shift, lstrValInput3);
    }
    else
    {
       if ((strcmp((char*)&_gsUser_Configuration_Area.uwUserName[0], (char*)&spaces[0]) != 0) &&
                (_gsUser_Configuration_Area.uwUserName[0] != 0x00))
       {
           PDF_SetText(gpage, 51.6f, 657.0f , "Programmer:");
           PDF_SetText(gpage, 115.8f, 657.0f, (char*)&_gsUser_Configuration_Area.uwUserName[0]);
           programmer_shift = 0.0;
       }
       //PDF_SetText(gpage, 51.6f, 657.0f, "Start Up Delay:");
       //PDF_FormatTimeSecsDHMSToString(_gsUser_Configuration_Area.udwStartUpDelay, (char*)&lstrValInput3[0]);
       //PDF_SetText(gpage, 115.8f, 657.0f, lstrValInput3);
       PDF_SetText(gpage, 51.6f, 645.0f +  programmer_shift, "Start Up Delay:");              
       PDF_FormatTimeSecsDHMSToString(_gsUser_Configuration_Area.udwStartUpDelay, (char*)&lstrValInput3[0]);
       PDF_SetText(gpage, 115.8f, 645.0f +  programmer_shift, lstrValInput3);
       PDF_SetText(gpage, 190.6f, 645.0f +  programmer_shift, "Interval:");
       PDF_FormatTimeSecsDHMSToString(_gsUser_Configuration_Area.udwMeasurementInterval, (char*)&lstrValInput3[0]);
       PDF_SetText(gpage, 249.8f, 645.0f +  programmer_shift, lstrValInput3);
    }
	
    //==============================
    // Monitor Read....
    //==============================
	  
    TimeZoneDSTShift = 0;
    if (_gsExtdOptByte.ExtOpt.Byte2.PdfTimezoneShift)
    {
       if (_gsSensitech_Configuration_Area.pdfTimeZone > MAX_GMT_TIMEZONE)
           _gsSensitech_Configuration_Area.pdfTimeZone = MAX_GMT_TIMEZONE;
       if (_gsSensitech_Configuration_Area.pdfTimeZone < MIN_GMT_TIMEZONE)
           _gsSensitech_Configuration_Area.pdfTimeZone = MIN_GMT_TIMEZONE;

       TimeZoneDSTShift = (INT32)(_gsSensitech_Configuration_Area.pdfTimeZone) * 15 * 60; //time zone shift in seconds
       hour_shift = _gsSensitech_Configuration_Area.pdfTimeZone / 4; //Calculates hour shift for GMT +/-XX:XX string
       minute_shift = (_gsSensitech_Configuration_Area.pdfTimeZone % 4) * 15; //15 minute increments
    }
    if (_gsExtdOptByte.ExtOpt.Byte2.PdfDaylightSTime)
    {
       TimeZoneDSTShift += DST_SHIFT_SEC; //add 3600 seconds (1 hr) for Daylight Saving Time
       hour_shift +=1;
    }

       

    PDF_SetText(gpage, 306.0f + shift_x_axis, 657.0f + shift_y_axis, "On :");
    //PDF_FormatTimeToString(gudwDeviceStoppedDownloadTime, (char*)&lstrValInput3[0]); // obsolete..
    PDF_FormatTimeToString(gudwDeviceReadOnTime + TimeZoneDSTShift, (char*)&lstrValInput3[0]);
  //PDF_FormatTimeToString( gudwDeviceStoppedDownloadTime + TimeZoneDSTShift, (char*)&lstrValInput3[0]);
    PDF_SetText(gpage, 336.0f + shift_x_axis, 657.0f + shift_y_axis, lstrValInput3);
    PDF_SetText(gpage, 433.0f, 657.0f + shift_y_axis, "By :");
    PDF_SetText(gpage, 458.0f + shift_x_axis, 657.0f + shift_y_axis, "TempTale  4 USB MultiAlarm");
    PDF_SetText(gpage, 306.0f + shift_x_axis, 645.0f + shift_y_axis, "Note:");

    if (TimeZoneDSTShift == 0)
      PDF_SetText(gpage, 325.0f + shift_x_axis, 645.0f + shift_y_axis, "All Times in GMT"); //No timezone shift
    else
    {  //Here there is a Timezone and/or DST shift...Let's format it for "All Times in GMT +/-XX:XX" string
      PDF_FormatTimeZoneToGMTString(hour_shift, minute_shift, (char*)&lstrValInput3[0]);
      PDF_SetText(gpage, 325.0f + shift_x_axis, 645.0f + shift_y_axis, lstrValInput3);
    }
    
    //==============================
    // Recorded Data....
    //==============================    	
    PDF_SetText(gpage, 51.6f, 597.6f + cXY + shift_y_axis, "First Point:");
    if(_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg != CREATE_PDF_WO_ALM_COND_ALM_STAT_LOG_RES){
        PDF_FormatTimeToString(_gsUser_Configuration_Area.udwUnit_Start_TimeStamp + TimeZoneDSTShift, (char*)&lstrValInput3[0]);
	PDF_SetText(gpage, 140.0f, 597.6f + cXY + shift_y_axis, lstrValInput3);
    }
    else{
        PDF_SetText(gpage, 140.0f, 597.6f + cXY + shift_y_axis, "View Off");
    }
	
	// change STOP text description in Multi-Drop mode suggested by Peter 032311... mjm
    if((_gsGenCfgOptByte.GenOpt.Byte1.MultDropFlag==1)&&(gubStopDeviceStatus!=_DEVICE_STOP_STATUS_SLEEP_))
        PDF_SetText(gpage, 51.6f, 586.0f + cXY + shift_y_axis, "Last Point:");
    else
        PDF_SetText(gpage, 51.6f, 586.0f + cXY + shift_y_axis, "Stop Time:");

    if(_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg != CREATE_PDF_WO_ALM_COND_ALM_STAT_LOG_RES){
        PDF_FormatTimeToString(_gsUser_Configuration_Area.udwUnit_Stop_TimeStamp + TimeZoneDSTShift, (char*)&lstrValInput3[0]);
        PDF_SetText(gpage, 140.0f, 586.0f + cXY + shift_y_axis, lstrValInput3);
    }

    PDF_SetText(gpage, 51.6f, 574.0f + cXY + shift_y_axis, "Number of Points:");
    if(_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg != CREATE_PDF_WO_ALM_COND_ALM_STAT_LOG_RES){
        sprintf((char*)&lstrValInput3[0],"%u",_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints);
        PDF_SetText(gpage, 140.0f, 574.0f + cXY + shift_y_axis, lstrValInput3);
    }
	
    PDF_SetText(gpage, 51.6f, 562.0f + cXY + shift_y_axis, "Trip Length:");
    if(_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg != CREATE_PDF_WO_ALM_COND_ALM_STAT_LOG_RES){
        PDF_FormatTimeTripLength(_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints * _gsUser_Configuration_Area.udwMeasurementInterval, (char*)&lstrValInput3[0]);
        PDF_SetText(gpage, 140.0f, 562.0f + cXY + shift_y_axis, lstrValInput3);
    }
	
	
    //==============================
    // Summary Data....
    //==============================	
    PDF_SetText(gpage, 306.0f + shift_x_axis, 597.0f + cXY + shift_y_axis, "Low Extreme:");
    if(_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg != CREATE_PDF_WO_ALM_COND_ALM_STAT_LOG_RES){
        PDF_Summary_TemperatureTime(_gsLogMinMaxTempData.fExtremeLowTemp_F_Deg , _gsUser_Configuration_Area.udwUnit_Start_TimeStamp + TimeZoneDSTShift + (_gsLogMinMaxTempData.uwExtremeLowDpNum * _gsUser_Configuration_Area.udwMeasurementInterval), (char*)&lstrValInput3[0]);
        PDF_SetText(gpage, 398.0f + shift_x_axis, 597.0f + cXY + shift_y_axis, lstrValInput3);
    }
    else{
        PDF_SetText(gpage, 398.0f + shift_x_axis, 597.0f + cXY + shift_y_axis, "View Off");
    }

    PDF_SetText(gpage, 306.0f + shift_x_axis, 586.0f + cXY + shift_y_axis, "High Extreme :");
    if(_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg != CREATE_PDF_WO_ALM_COND_ALM_STAT_LOG_RES){
        PDF_Summary_TemperatureTime(_gsLogMinMaxTempData.fExtremeHighTemp_F_Deg , _gsUser_Configuration_Area.udwUnit_Start_TimeStamp + TimeZoneDSTShift + (_gsLogMinMaxTempData.uwExtremeHighDpNum * _gsUser_Configuration_Area.udwMeasurementInterval), (char*)&lstrValInput3[0]);
        PDF_SetText(gpage, 398.0f + shift_x_axis, 586.0f + cXY + shift_y_axis, lstrValInput3);
    }
	
    sprintf((char*)&lstrValInput3[0],"Mean %c Std Deviation:",lcSymbolTol);
    if(_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg != CREATE_PDF_WO_ALM_COND_ALM_STAT_LOG_RES){
        // calculate standard deviation and mean kinetic tempearture....
    //PDF_Summary_Calculate_StdDevAndMKT();
        // display mean value....
        PDF_SetText(gpage, 306.0f + shift_x_axis, 574.0f + cXY + shift_y_axis,lstrValInput3);
        // display tolerance....
        PDF_Summary_StdDeviation(lstrValInput3);
        PDF_SetText(gpage, 398.0f + shift_x_axis, 574.0f + cXY + shift_y_axis, lstrValInput3);
    }
    else{
        PDF_SetText(gpage, 306.0f + shift_x_axis, 574.0f + cXY + shift_y_axis,lstrValInput3);
    }

    PDF_SetText(gpage, 306.0f + shift_x_axis, 562.0f + cXY + shift_y_axis, "Mean Kinetic Temperature:");
    if(_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg != CREATE_PDF_WO_ALM_COND_ALM_STAT_LOG_RES){
        PDF_Summary_MeanKinetic(lstrValInput3);
        PDF_SetText(gpage, 398.0f + shift_x_axis, 562.0f + cXY + shift_y_axis, lstrValInput3);
    }

    // added new marketing requirement MKT activation energy... requested Peter Nunes , 10312013 mjm..
    PDF_SetText(gpage, 306.0f + shift_x_axis, 551.0f + cXY + shift_y_axis, "Activation Energy:");

    // Activation Engery...
    sprintf((char*)&lstrValInput3[0],"%.3f kJ/mol",(double)_gsUser_Configuration_Area.fMKTActivationEnergy / 1000.0);

    //display to PDF MKT activation energy..
    PDF_SetText(gpage, 398.0f + shift_x_axis, 551.0f + cXY + shift_y_axis, lstrValInput3);
	


    //==============================
    // Alarm Summary Data....
    //==============================            
    

    //---------------
    // Ideal Range...
    //---------------
    
    PDF_SetText(gpage, 51.6f, 525.5f + shift_y_axis, "Ideal range");    
    
    if(!((_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg == CREATE_PDF_WO_ALM_COND_INFO)^
         (_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg == CREATE_PDF_WO_ALM_COND_ALM_STAT_INFO)^
         (_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg == CREATE_PDF_WO_ALM_COND_ALM_STAT_LOG_RES))){

        PDF_Summary_IdealSettings(lstrValInput3);                 
        PDF_SetText(gpage, 95.0f, 525.5f + shift_y_axis, lstrValInput3);
        
        if(_gsExtdOptByte.ExtOpt.Byte1.IdealTempRgViewEna==1)
            PDF_SetText(gpage, 245.0f, 525.5f + shift_y_axis, "Unlimited");
        else
            PDF_SetText(gpage, 245.0f, 525.5f + shift_y_axis, "--"); 

    }
    else{
        PDF_SetText(gpage, 95.0f,  525.5f + shift_y_axis, "View Off");
        PDF_SetText(gpage, 245.0f, 525.5f + shift_y_axis, "View Off");
	    
    }
    
    if(!((_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg == CREATE_PDF_WO_ALM_COND_ALM_STAT_INFO)^
         (_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg == CREATE_PDF_WO_ALM_COND_ALM_STAT_LOG_RES))){

        PDF_Summary_TotalTime(IDEAL_TEMP,(char*)&lstrValInput3[0]);	
        PDF_SetText(gpage, 320.0f, 525.5f + shift_y_axis, lstrValInput3);
        PDF_Summary_NumberOfEvents(IDEAL_TEMP,(char*)&lstrValInput3[0]);
        PDF_SetText(gpage, 385.0f, 525.5f + shift_y_axis, lstrValInput3);

        PDF_SetText(gpage, 434.0f, 525.5f + shift_y_axis, "--");
        PDF_SetText(gpage, 522.0f, 525.5f + shift_y_axis, "--");
    }
    else{
        PDF_SetText(gpage, 320.0f, 525.5f + shift_y_axis, "View Off");
        PDF_SetText(gpage, 385.0f, 525.5f + shift_y_axis, "View Off");
        PDF_SetText(gpage, 434.0f, 525.5f + shift_y_axis, "View Off");
        PDF_SetText(gpage, 522.0f, 525.5f + shift_y_axis, "View Off");
    }


    //---------
    // Alarm 1
    //---------

    PDF_SetText(gpage, 51.6f, 514.0f + shift_y_axis, "Alarm 1");                         
    
    if(!((_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg == CREATE_PDF_WO_ALM_COND_INFO)^
         (_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg == CREATE_PDF_WO_ALM_COND_ALM_STAT_INFO)^
         (_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg == CREATE_PDF_WO_ALM_COND_ALM_STAT_LOG_RES))){
    
        PDF_Summary_AlarmSettings(ALARM_1,(char*)&lstrValInput3[0]);
        PDF_SetText(gpage, 95.0f, 514.0f + shift_y_axis, lstrValInput3);    
    
        PDF_Summary_Threshold(ALARM_1,(char*)&lstrValInput3[0]);
	      PDF_SetText(gpage, 245.0f, 514.0f + shift_y_axis, lstrValInput3);
	}
	
    if(!((_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg == CREATE_PDF_WO_ALM_COND_ALM_STAT_INFO)^
       (_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg == CREATE_PDF_WO_ALM_COND_ALM_STAT_LOG_RES))){
	
        PDF_Summary_TotalTime(ALARM_1,(char*)&lstrValInput3[0]);	
        PDF_SetText(gpage, 320.0f, 514.0f + shift_y_axis, lstrValInput3);

        PDF_Summary_NumberOfEvents(ALARM_1,(char*)&lstrValInput3[0]);
        PDF_SetText(gpage, 385.0f, 514.0f + shift_y_axis, lstrValInput3);

        PDF_Summary_Triggered(ALARM_1,(char*)&lstrValInput3[0]);
        PDF_SetText(gpage, 434.0f, 514.0f + shift_y_axis, lstrValInput3);

        PDF_Summary_Status(ALARM_1,(char*)&lstrValInput3[0]);
        PDF_SetText(gpage, 522.0f, 514.0f + shift_y_axis, lstrValInput3);
    }
    
    //---------
    // Alarm 2
    //---------

    PDF_SetText(gpage, 51.6f, 502.0f + shift_y_axis, "Alarm 2");               
    
    if(!((_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg == CREATE_PDF_WO_ALM_COND_INFO)^
         (_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg == CREATE_PDF_WO_ALM_COND_ALM_STAT_INFO)^
         (_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg == CREATE_PDF_WO_ALM_COND_ALM_STAT_LOG_RES))){
        
        PDF_Summary_AlarmSettings(ALARM_2,(char*)&lstrValInput3[0]);       
        PDF_SetText(gpage, 95.0f, 502.0f + shift_y_axis, lstrValInput3);    
    
        PDF_Summary_Threshold(ALARM_2,(char*)&lstrValInput3[0]);
        PDF_SetText(gpage, 245.0f, 502.0f + shift_y_axis, lstrValInput3);
    }
	
    if(!((_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg == CREATE_PDF_WO_ALM_COND_ALM_STAT_INFO)^
         (_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg == CREATE_PDF_WO_ALM_COND_ALM_STAT_LOG_RES))){
	
	    PDF_Summary_TotalTime(ALARM_2,(char*)&lstrValInput3[0]);
	    PDF_SetText(gpage, 320.0f, 502.0f + shift_y_axis, lstrValInput3);
	
	    PDF_Summary_NumberOfEvents(ALARM_2,(char*)&lstrValInput3[0]);
	    PDF_SetText(gpage, 385.0f, 502.0f + shift_y_axis, lstrValInput3);
	
	    PDF_Summary_Triggered(ALARM_2,(char*)&lstrValInput3[0]);
	    PDF_SetText(gpage, 434.0f, 502.0f + shift_y_axis, lstrValInput3);  
	
	    PDF_Summary_Status(ALARM_2,(char*)&lstrValInput3[0]);
	    PDF_SetText(gpage, 522.0f, 502.0f + shift_y_axis, lstrValInput3);

    }
    
    //---------
    // Alarm 3
    //---------

    PDF_SetText(gpage, 51.6f, 491.0f + shift_y_axis, "Alarm 3");               
    
    if(!((_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg == CREATE_PDF_WO_ALM_COND_INFO)^
         (_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg == CREATE_PDF_WO_ALM_COND_ALM_STAT_INFO)^
         (_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg == CREATE_PDF_WO_ALM_COND_ALM_STAT_LOG_RES))){
        
        PDF_Summary_AlarmSettings(ALARM_3,(char*)&lstrValInput3[0]);       
        PDF_SetText(gpage, 95.0f, 491.0f + shift_y_axis, lstrValInput3);
    
        PDF_Summary_Threshold(ALARM_3,(char*)&lstrValInput3[0]);	
        PDF_SetText(gpage, 245.0f, 491.0f + shift_y_axis, lstrValInput3);
	}
	
    if(!((_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg == CREATE_PDF_WO_ALM_COND_ALM_STAT_INFO)^
         (_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg == CREATE_PDF_WO_ALM_COND_ALM_STAT_LOG_RES))){
	
        PDF_Summary_TotalTime(ALARM_3,(char*)&lstrValInput3[0]);
        PDF_SetText(gpage, 320.0f, 491.0f + shift_y_axis, lstrValInput3);

        PDF_Summary_NumberOfEvents(ALARM_3,(char*)&lstrValInput3[0]);
        PDF_SetText(gpage, 385.0f, 491.0f + shift_y_axis, lstrValInput3);

        PDF_Summary_Triggered(ALARM_3,(char*)&lstrValInput3[0]);
        PDF_SetText(gpage, 434.0f, 491.0f + shift_y_axis, lstrValInput3);

        PDF_Summary_Status(ALARM_3,(char*)&lstrValInput3[0]);
        PDF_SetText(gpage, 522.0f, 491.0f + shift_y_axis, lstrValInput3);

    }
    
    //---------
    // Alarm 4
    //---------

    PDF_SetText(gpage, 51.6f, 479.0f + shift_y_axis, "Alarm 4");     
    
    if(!((_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg == CREATE_PDF_WO_ALM_COND_INFO)^
         (_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg == CREATE_PDF_WO_ALM_COND_ALM_STAT_INFO)^
         (_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg == CREATE_PDF_WO_ALM_COND_ALM_STAT_LOG_RES))){
        
        PDF_Summary_AlarmSettings(ALARM_4,(char*)&lstrValInput3[0]);                        
        PDF_SetText(gpage, 95.0f, 479.0f + shift_y_axis, lstrValInput3);
    
        PDF_Summary_Threshold(ALARM_4,(char*)&lstrValInput3[0]);	
        PDF_SetText(gpage, 245.0f, 479.0f + shift_y_axis, lstrValInput3);
	}
	
    if(!((_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg == CREATE_PDF_WO_ALM_COND_ALM_STAT_INFO)^
         (_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg == CREATE_PDF_WO_ALM_COND_ALM_STAT_LOG_RES))){
	
        PDF_Summary_TotalTime(ALARM_4,(char*)&lstrValInput3[0]);
        PDF_SetText(gpage, 320.0f, 479.0f + shift_y_axis, lstrValInput3);

        PDF_Summary_NumberOfEvents(ALARM_4,(char*)&lstrValInput3[0]);
        PDF_SetText(gpage, 385.0f, 479.0f + shift_y_axis, lstrValInput3);

        PDF_Summary_Triggered(ALARM_4,(char*)&lstrValInput3[0]);
        PDF_SetText(gpage, 434.0f, 479.0f + shift_y_axis, lstrValInput3);

        PDF_Summary_Status(ALARM_4,(char*)&lstrValInput3[0]);
        PDF_SetText(gpage, 522.0f, 479.0f + shift_y_axis, lstrValInput3);
    }

    //---------
    // Alarm 5
    //---------

    PDF_SetText(gpage, 51.6f, 467.5f + shift_y_axis, "Alarm 5");    
    
    if(!((_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg == CREATE_PDF_WO_ALM_COND_INFO)^
         (_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg == CREATE_PDF_WO_ALM_COND_ALM_STAT_INFO)^
         (_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg == CREATE_PDF_WO_ALM_COND_ALM_STAT_LOG_RES))){
        
        PDF_Summary_AlarmSettings(ALARM_5,(char*)&lstrValInput3[0]);                  
        PDF_SetText(gpage, 95.0f, 467.5f + shift_y_axis, lstrValInput3);
    
        PDF_Summary_Threshold(ALARM_5,(char*)&lstrValInput3[0]);	
        PDF_SetText(gpage, 245.0f, 467.5f + shift_y_axis, lstrValInput3);
    }
	
    if(!((_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg == CREATE_PDF_WO_ALM_COND_ALM_STAT_INFO)^
         (_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg == CREATE_PDF_WO_ALM_COND_ALM_STAT_LOG_RES))){
	
	    PDF_Summary_TotalTime(ALARM_5,(char*)&lstrValInput3[0]);	
	    PDF_SetText(gpage, 320.0f, 467.5f + shift_y_axis, lstrValInput3);
	
	    PDF_Summary_NumberOfEvents(ALARM_5,(char*)&lstrValInput3[0]);	
	    PDF_SetText(gpage, 385.0f, 467.5f + shift_y_axis, lstrValInput3);
	
	    PDF_Summary_Triggered(ALARM_5,(char*)&lstrValInput3[0]);	
	    PDF_SetText(gpage, 434.0f, 467.5f + shift_y_axis, lstrValInput3);
	
	    PDF_Summary_Status(ALARM_5,(char*)&lstrValInput3[0]);	
	    PDF_SetText(gpage, 522.0f, 467.5f + shift_y_axis, lstrValInput3);
	}

    //---------
    // Alarm 6
    //---------

    PDF_SetText(gpage, 51.6f, 456.0f + shift_y_axis, "Alarm 6");    
    
    if(!((_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg == CREATE_PDF_WO_ALM_COND_INFO)^
         (_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg == CREATE_PDF_WO_ALM_COND_ALM_STAT_INFO)^
         (_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg == CREATE_PDF_WO_ALM_COND_ALM_STAT_LOG_RES))){
        
        PDF_Summary_AlarmSettings(ALARM_6,(char*)&lstrValInput3[0]);                  
        PDF_SetText(gpage, 95.0f, 456.0f + shift_y_axis, lstrValInput3);
    
        PDF_Summary_Threshold(ALARM_6,(char*)&lstrValInput3[0]);	
	    PDF_SetText(gpage, 245.0f, 456.0f + shift_y_axis, lstrValInput3);	
	}

    if(!((_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg == CREATE_PDF_WO_ALM_COND_ALM_STAT_INFO)^
         (_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg == CREATE_PDF_WO_ALM_COND_ALM_STAT_LOG_RES))){
	
	    PDF_Summary_TotalTime(ALARM_6,(char*)&lstrValInput3[0]);	
	    PDF_SetText(gpage, 320.0f, 456.0f + shift_y_axis, lstrValInput3);
	
	    PDF_Summary_NumberOfEvents(ALARM_6,(char*)&lstrValInput3[0]);	
	    PDF_SetText(gpage, 385.0f, 456.0f + shift_y_axis, lstrValInput3);
	
	    PDF_Summary_Triggered(ALARM_6,(char*)&lstrValInput3[0]);	
	    PDF_SetText(gpage, 434.0f, 456.0f + shift_y_axis, lstrValInput3);
	
	    PDF_Summary_Status(ALARM_6,(char*)&lstrValInput3[0]);	
	    PDF_SetText(gpage, 522.0f, 456.0f + shift_y_axis, lstrValInput3);
    }

    //==============================
    // Originator Notes....
    //==============================           
    PDF_OriginatorNotes((char*)&lstrValInput3[0]);	
    HPDF_Page_SetFontAndSize (gpage, gdefault_font, 5.8);
    PDF_SetText(gpage, 51.6f, 423.0f + shift_y_axis, lstrValInput3); // max 96 chars ... mjm
    

    //==============================
    // footer information...
    //==============================
    
    
    if(_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg != CREATE_PDF_WO_ALM_COND_ALM_STAT_LOG_RES){
        
        // check number of resets then if zero use manufacturer serial number w/ no trip number...
        if(_gsUser_Configuration_Area.udwTripNumber>0){        
            utilULongNumToString(_gsUser_Configuration_Area.udwTripNumber,(UINT8 *)&lstrValInput2[0]);                            
            sprintf((char*)&lstrValInput3[0],"TempTale4 Trip: %s  Serial: %s",lstrValInput2,lstrValInput1);
        }    
        else{    
            // if trip number is zero then substitute the serial number as a trip number, advised by Peter in email.. 3/22/11            
            sprintf((char*)&lstrValInput3[0],"TempTale4 Trip: %s  Serial: %s",lstrValInput1,lstrValInput1);
        }
        
        HPDF_Page_SetFontAndSize (gpage, gdefault_font, 7.0f);    
        //PDF_SetText(gpage, 230.0f, 385.0f, "VaxAlert Trip: 2710000000  Serial: 2710000000");
        PDF_SetText(gpage, 230.0f, 385.0f + shift_y_axis, lstrValInput3); 
    
        HPDF_Page_SetFontAndSize (gpage, gdefault_font, 8.0f);
        PDF_SetText(gpage, 276.0f, 90.0f + shift_y_axis, "Date/Time");         
        //PDF_SetText(gpage, 270.0f, 32.0f, "( Page 1 of 1 )"); 

        HPDF_Page_SetFontAndSize (gpage, gdefault_font, 6.0f);
        PDF_SetText(gpage, 480.0f, 85.0f + shift_y_axis, "Primary: AmbientSensor");     
    }

    // small text...
	HPDF_Page_SetFontAndSize (gpage, gdefault_font, 6.0f);	
	PDF_SetText(gpage, 50.0f, 62.0f + shift_y_axis, "Time and Temperature Report Created By: TempTale  4 USB MultiAlarm Monitor");	    

    //PDF_FormatTimeToString(gudwDeviceStoppedDownloadTime, (char*)&lstrValInput2[0]); 
    PDF_FormatTimeToString(gudwDeviceReadOnTime + TimeZoneDSTShift, (char*)&lstrValInput2[0]); // mjm debug 05062013...
    sprintf((char*)&lstrValInput3[0],"File Created: %s",lstrValInput2);
    PDF_SetText(gpage, 365.0f, 62.0f + shift_y_axis, lstrValInput3);

    // header small R
	HPDF_Page_SetFontAndSize (gpage, gdefault_font, 7);
	sprintf((char*)&lstrValInput3[0],"%c",lcSymbolR);
	PDF_SetText(gpage, 282.0f, 755.0f, lstrValInput3);
    
    // monitor read small R
	HPDF_Page_SetFontAndSize (gpage, gdefault_font, 3.5f);	
	PDF_SetText(gpage, 491.0f, 659.0f + shift_y_axis, lstrValInput3);

    // footer small R	
    HPDF_Page_SetFontAndSize (gpage, gdefault_font, 3);
	PDF_SetText(gpage, 193.5f, 64.0f + shift_y_axis, lstrValInput3);

	HPDF_Page_EndText (gpage);    
	

    //==========================================================                             
    // Start to draw graphics here after text....
    //==========================================================  
    
	
	////////////////////////////////////////////////////////////////
	
	// data point plotter grid box.... mjm
	//PDF_DrawRectangle(gpage, 49.0f, 75.0f, 510.0f, 325.0f);
	
    if(_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg != CREATE_PDF_WO_ALM_COND_ALM_STAT_LOG_RES){

        //==========================================================                             
        // Setup PDF Graph Gridlines and Text Values....
        //==========================================================  

        // Write scaled temperature values and time stamp intervals.....
        PDF_CalculateGraphScaleValues();

        // Draw the PDF Gridlines for Plotting Reference....
        PDF_DrawGraphGridLines(gpage,gdefault_font);
    
        //==========================================================                             
        // Draw PDF graphic lines ...      
        //==========================================================     
    
        PDF_GraphPlotterDisplay(gpage,gdefault_font);    
    }
    else{
        HPDF_Page_SetFontAndSize (gpage, gdefault_font, 20);   
        PDF_SetText(gpage, 270.0f, 220.0f + shift_y_axis, "View off");
    }


    //==========================================================                             
    // Draw PDF graphic boxes..      
    //==========================================================                             
		
    HPDF_Page_SetLineWidth (gpage, 1.3f);
    HPDF_Page_SetRGBStroke (gpage, 0.0f, 0.0f, 0.0f);           
    HPDF_Page_SetLineCap(gpage,HPDF_BUTT_END);

      if (_gsGenCfgOptByte.GenOpt.Byte0.ShippingInfoEna)
      {//Shipment Details Box
          PDF_DrawRectangle(gpage, 49.0f, 660.0f, 248.0f, 59.0f);  // x,y,w,h
       //Shipment Details Box
          PDF_DrawRectangle(gpage, 307.0f, 660.0f, 253.0f, 59.0f);  // x,y,w,h
          // Monitor Configuration Box...
          PDF_DrawRectangle(gpage, 147.0f, 616.0f, 150.0f, 28.0f);  // x,y,w,h ... 619,46 .. MKT Alarm box, mjm 10312013
           // Record data box...
          PDF_DrawRectangle(gpage, 49.0f, 541.0f, 248.0f, 60.0f);    // x,y,w,h .. 560.0,44.7 .. MKT Alarm box, mjm 10312013

      }
      else
      {    	// Monitor Configuration Box...
          PDF_DrawRectangle(gpage, 49.0f, 636.0f, 233.0f, 28.0f);  // x,y,w,h ... 619,46 .. MKT Alarm box, mjm 10312013
            // Record data box...
          PDF_DrawRectangle(gpage, 49.0f, 561.0f, 233.5, 60.0f);    // x,y,w,h .. 560.0,44.7 .. MKT Alarm box, mjm 10312013
       }


	// PDF graph box...
	PDF_DrawRectangle(gpage, 49.0f, 75.0f + shift_y_axis, 510.0f, 325.0f);  // x,y,w,h

	// Check box...
	PDF_DrawRectangle(gpage, 49.0f, 680.0f + shift_alarm , 90.0f, 22.0f);   // x,y,w,h
	PDF_DrawLine(gpage, 114.0f, 680.0f + shift_alarm, 114.0f,702.0f + shift_alarm);      // x,y,w,h 
	

	// Monitor Read Box...
	PDF_DrawRectangle(gpage, 304.0f + shift_x_axis, 636.0f + shift_y_axis, 253.0f, 28.0f); // x,y,w,h  ... 619,46 .. MKT Alarm box, mjm 10312013
	    
  
	// Summary Data Box...
	PDF_DrawRectangle(gpage, 304.0f + shift_x_axis, 561.0f + shift_y_axis, 253.0f, 60.0f);   // x,y,w,h .. 560.0,44.7 .. MKT Alarm box, mjm 10312013
	
	// Alarm Summary Box...
	PDF_DrawRectangle(gpage, 49.0f, 453.0f + shift_y_axis, 510.0, 92.0);    // x,y,w,h
	
	// Originator Notes Box...
	PDF_DrawRectangle(gpage, 49.0f, 415.0f + shift_y_axis, 510.0, 23.0);    // x,y,w,h

    if(_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg != CREATE_PDF_WO_ALM_COND_ALM_STAT_LOG_RES){
        // sensor line legend!!
	    HPDF_Page_SetLineWidth (gpage, 0.8f);
	    HPDF_Page_SetRGBStroke(gpage, 0.0f, 0.0f, 1.0f);
	    HPDF_Page_SetRGBFill (gpage, 0.0f, 0.0f, 1.0f);
	    PDF_DrawLine(gpage, 465.0f, 87.0f + shift_y_axis, 475.0f, 87.0f + shift_y_axis);	
	}

    // draw Sensitech logo..
    PDF_DrawLogo(gpage, gdefault_font, 70.0f, 747.0f);  
	
	// Check Alarm Status 
	if(_gsUser_Configuration_Area.ubAlarmStatus & 0x3F) 
	    // draw status X mark...
	    PDF_DrawCheckMark(gpage, FALSE);
	else
	    // draw status check mark...
	    PDF_DrawCheckMark(gpage, TRUE);
    
    // set file attributes...
    PDF_SetFileAttributes(gpdf);

	
	// create long filename...	
    utilULongNumToString(_gsSensitech_Configuration_Area.udwManufacturingSerialNumber,(UINT8 *)&lstrValInput1[0]);    
    // check number of resets then if zero use manufacturer serial number w/ no trip number...
    if(_gsUser_Configuration_Area.udwTripNumber==0){
        sprintf((char *)&gLfname[0],"%s_%s_%u.pdf",lstrValInput1,lstrValInput1,_gsSensitech_Configuration_Area.uwNumberOfCustomerResets);		
    }    
    else{    
        utilULongNumToString(_gsUser_Configuration_Area.udwTripNumber,(UINT8 *)&lstrValInput2[0]);        
        // the PDF file naming format is TripS/N_Manf.S/N_# of Customer resets... Peter amended this format as requested by Sensitech SQA group....040511 mjm 
        sprintf((char *)&gLfname[0],"%s_%s_%u.pdf",lstrValInput2,lstrValInput1,_gsSensitech_Configuration_Area.uwNumberOfCustomerResets);		
    }    
	
	// save pdf file..
    HPDF_SaveToFile (gpdf, (char *)&gLfname[0]);
    
    // save generated PDF filename... 
    util_SaveLongFileName((char *)&gstrPDFFilename[0],(const char *)&gLfname[0]);
    
	// free allocated memory...
    HPDF_Free(gpdf);
    
	return(TRUE);
	
}


/*****************************************************************************
*
* Function Name: PDF_DrawGraphGridLines
*
* Description  : Displays the PDF Graph Gridlines 
*
* Input        : page,def_font
*
* Return       : None
*
* History      : *    Date    *    Author   *   Descriptions
*
*                  9/9/2009    Michael J. Mariveles
******************************************************************************/

/* Constants */

#define TEMP_LABEL_VERT_DISP_Y              220.0 
#define TEMP_LABEL_VERT_DISP_X              58.0
#define TEMP_LABEL_VERT_DISP_ANGLE          90.0 
#define TEMP_HORIZ_LINES                    10

#define PDF_GRAPH_SKETCH_MAX_WIDTH          460.0  
#define PDF_GRAPH_SKETCH_MAX_HEIGHT         250.0
#define PDF_GRAPH_MAX_WIDTH_DOT_RESOLUTION  PDF_GRAPH_SKETCH_MAX_WIDTH

#define PLOT_X_ORIGIN                       90.0
#define PLOT_Y_ORIGIN                       124.0

#define NUM_OF_X_GRID_LINES                 8.0
#define NUM_OF_Y_GRID_LINES                 11.0
#define TIME_STAMPS_DIVISIONS               (NUM_OF_X_GRID_LINES - 1.0)

/* MACRO's */
/*
// Fahrenheit to Centigrade Temperature Conversion...
#define TEMP_C(F)   ((5.0/9.0) * (F - 32.0)) 
// Centigrade to Fahrenheit Temperature Conversion...
#define TEMP_F(C)   ((C*(9.0/5.0)) + 32.0) 
// A 1 unit temperature degree in C
#define ONE_DEG_C 1.0
// A 1 unit temperature degree in F
#define ONE_DEG_F 1.0
*/

/* Structure Variable */

// this is the Y-axis temperature value scaling parameters...
struct _SCALEVALUEPARAMETERS {
    float fExtremeHighSetpoint;
    float fExtremeLowSetpoint;
	float fTopMostTempLevel;
	float fLowerMostTempLevel;
	float fTempStepResolution;	
	float fTempPerDivisionFactor;
} _sPDFGridLinesScaledValues;

/*******************************************************************************
**  Function     : PDF_GetTemperatureUnitValue
**  Description  : converts temperature from C degrees to F degrees 
**  PreCondition : none
**  Side Effects : none
**  Input        : par_Temp - 0=C or 1=F
**  Output       : converted temp to C
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
float PDF_GetTemperatureUnitValue(float par_Temp){

    // 0-C or  1-F
    if(_gsGenCfgOptByte.GenOpt.Byte1.F_C_Unit_Select == 0) return (TEMP_C(par_Temp));
    
    return(par_Temp);
}

/*******************************************************************************
**  Function     : PDF_DrawGraphGridLines
**  Description  : draws gridlines and format the PDF form
**  PreCondition : none
**  Side Effects : none
**  Input        : 
**                 page - PDF page handler
**                 def_font - PDF font handler
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void PDF_DrawGraphGridLines(HPDF_Page page, HPDF_Font def_font)
{
    __sBITS_ALARM_CONFIG_BYTE0 _lsBitAlarm_ConfigB0;
    register float gd_X1,gd_Y1,gd_X2,gd_Y2;
    register short nCtr;
    register float X_Spacing,Y_Spacing,X_Factor,Y_Factor;
    register float fTempPerDivisionValue,fTempCVal;
    register UINT32 ludwTimeInterval, ludwTimeStamps;
    register UINT8 lubToggle;
    register float lfAltYCoordinate;
    register UINT8 lubAlarmNum;
    char TempValueStr[25];
    register float lfDataPointTrig;
    register float lfIndent,lfDdecVal;
    register char lcSymbolDeg = 176; // ?
    register float lfBoxY1,lfBoxY2;
    register double ldPointTrig;
    register char lcValue;
    register float lfR1LowTempVal,lfR1HighTempVal;
  
    UINT32 ludwTimeplot;
    
    unsigned char lucDoubleDecimalFlagEnable = 0;

    lfIndent = 0.0;
    
    // Note: double decimal digit will be available only in C Deg unit, ST marketing requirement... mjm 08222013..
    if((_gsGenCfgOptByte.GenOpt.Byte1.CDegDoubleDecimal == 1)&&(_gsGenCfgOptByte.GenOpt.Byte1.F_C_Unit_Select==0)) lucDoubleDecimalFlagEnable = 1;
         

    //=====================================
    // check Ideal temperature lines...
    //=====================================
    if(_gsExtdOptByte.ExtOpt.Byte1.IdealTempRgViewEna==1){
    
        //==========================================================================
        // plot first the color filled iDeal temp box.....
        //==========================================================================
        lfBoxY1 = PLOT_Y_ORIGIN  + shift_y_axis + (unsigned short)(((_gsUserCfgAreaData.fTempIdealRangeHighValue - _sPDFGridLinesScaledValues.fLowerMostTempLevel) * _sPDFGridLinesScaledValues.fTempStepResolution));
        lfBoxY2 = PLOT_Y_ORIGIN + shift_y_axis + (unsigned short)(((_gsUserCfgAreaData.fTempIdealRangeLowValue - _sPDFGridLinesScaledValues.fLowerMostTempLevel) * _sPDFGridLinesScaledValues.fTempStepResolution));
        HPDF_Page_SetRGBStroke (page, 0.6f, 0.8f, 1.0f);
        HPDF_Page_SetRGBFill (page, 0.6f, 0.8f, 1.0f);
        HPDF_Page_Rectangle(page, PLOT_X_ORIGIN, lfBoxY1, PDF_GRAPH_SKETCH_MAX_WIDTH, lfBoxY2 - lfBoxY1);            
        HPDF_Page_Fill (page);        
        
        HPDF_Page_SetLineWidth (page, 0.8f);    
        HPDF_Page_SetFontAndSize (page, def_font, 6.0f);
            
        // set line color..
        HPDF_Page_SetRGBStroke (page, 0.6f, 0.8f, 1.0f);
        HPDF_Page_SetRGBFill (page, 0.6f, 0.8f, 1.0f);
                
        // Single Line w/ High Limit only...              
        PDF_DrawLine(page, PLOT_X_ORIGIN, lfBoxY1, PLOT_X_ORIGIN + PDF_GRAPH_SKETCH_MAX_WIDTH + 5.0f, lfBoxY1);           

        // draw setpoint temperature label .. 
        if(lucDoubleDecimalFlagEnable==1){            
            lfDdecVal = PDF_GetTemperatureUnitValue(_gsUserCfgAreaData.fTempIdealRangeHighValue);             
            //sprintf((char *)&TempValueStr[0],"%6.2f",(double)PDF_GetTemperatureUnitValue(_gsUserCfgAreaData.fTempIdealRangeHighValue));        
            sprintf((char *)&TempValueStr[0],"%6.2f",(double)utilDbDec_RoundOFF(lfDdecVal));
        }    
        else
            sprintf((char *)&TempValueStr[0],"%6.1f",(double)PDF_GetTemperatureUnitValue(_gsUserCfgAreaData.fTempIdealRangeHighValue));        
        
        HPDF_Page_BeginText (page);    
        PDF_SetText(page, PLOT_X_ORIGIN + lfIndent, lfBoxY1+2.0f, TempValueStr);         
        HPDF_Page_EndText (page);    
        
        // Single Line w/ Low Limit only...                     
        PDF_DrawLine(page, PLOT_X_ORIGIN, lfBoxY2, PLOT_X_ORIGIN + PDF_GRAPH_SKETCH_MAX_WIDTH + 5.0f, lfBoxY2);            
        
        // draw setpoint temperature label .. 
        if(lucDoubleDecimalFlagEnable==1){
            lfDdecVal = PDF_GetTemperatureUnitValue(_gsUserCfgAreaData.fTempIdealRangeLowValue);            
            //sprintf((char *)&TempValueStr[0],"%6.2f",(double)PDF_GetTemperatureUnitValue(_gsUserCfgAreaData.fTempIdealRangeLowValue));        
            sprintf((char *)&TempValueStr[0],"%6.2f",(double)utilDbDec_RoundOFF(lfDdecVal));
        }    
        else
            sprintf((char *)&TempValueStr[0],"%6.1f",(double)PDF_GetTemperatureUnitValue(_gsUserCfgAreaData.fTempIdealRangeLowValue));        
        
        HPDF_Page_BeginText (page);    
        PDF_SetText(page, PLOT_X_ORIGIN + lfIndent, lfBoxY2-6.0f, TempValueStr);         
        HPDF_Page_EndText (page);	        
    }
    

    //==========================================================================
    // setup PDF text and grid line settings.....
    //==========================================================================
    
    HPDF_Page_SetDash (page, DASH_MODE1, 1, 1);
    HPDF_Page_SetLineWidth (page, 0.3f);    
    HPDF_Page_SetRGBStroke (page, 0.4f, 0.4f, 0.4f);        
    HPDF_Page_SetRGBFill (page, 0.0f, 0.0f, 0.0f);     
    HPDF_Page_SetFontAndSize (page, def_font, 6.0f);

    
    //==========================================================================
    // create grid columns ....
    //==========================================================================
    gd_X1 = PLOT_X_ORIGIN;
    gd_Y1 = PLOT_Y_ORIGIN + shift_y_axis;
    gd_X2 = PLOT_X_ORIGIN;
    gd_Y2 = PLOT_Y_ORIGIN + shift_y_axis + PDF_GRAPH_SKETCH_MAX_HEIGHT;
    
    X_Spacing = PDF_GRAPH_SKETCH_MAX_WIDTH / (NUM_OF_X_GRID_LINES - 1.0);
    X_Factor = 0.0;    
    
    for(nCtr = 0;nCtr < NUM_OF_X_GRID_LINES;nCtr++){
    
    	X_Factor = X_Spacing * nCtr;
        // print to PDF the vertical lines..  
        PDF_DrawLine(page, gd_X1 + X_Factor, gd_Y1, gd_X2 + X_Factor, gd_Y2);   
    }



    //==========================================================================    
    // draw scaled time interval text in each columns ....
    //==========================================================================

    ludwTimeInterval = ((UINT32)(_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints - 1u) * _gsUser_Configuration_Area.udwMeasurementInterval) / (UINT32)TIME_STAMPS_DIVISIONS;  
    ludwTimeStamps = _gsUser_Configuration_Area.udwUnit_Start_TimeStamp + TimeZoneDSTShift;
    
    lubToggle = 1;

    // note: this must be separated from the loop for the sake of saving pdf file memory.....mjm
    HPDF_Page_BeginText(page);
    
    for(nCtr = 0;nCtr < NUM_OF_X_GRID_LINES;nCtr++){    
        
        X_Factor = X_Spacing * nCtr;
        
        PDF_FormatTimeToString(ludwTimeStamps,(char *)&TempValueStr[0]);
        
        if(lubToggle) 
            lfAltYCoordinate = 0.0f;
        else
            lfAltYCoordinate = 7.0f;

        if(nCtr<7){
             PDF_SetText(page, (gd_X1-30.0f) + X_Factor, gd_Y1 - 12.0f - lfAltYCoordinate, TempValueStr);
        }     
        
		//increment time stamps from 1st point...
		ludwTimeStamps += ludwTimeInterval;

        lubToggle ^= 1;    
    }
    HPDF_Page_EndText(page);

    
    
    //==========================================================================
    // create grid rows ....
    //==========================================================================
    gd_X1 = PLOT_X_ORIGIN;
    gd_Y1 = PLOT_Y_ORIGIN + shift_y_axis;
    gd_X2 = PLOT_X_ORIGIN + PDF_GRAPH_SKETCH_MAX_WIDTH;
    gd_Y2 = PLOT_Y_ORIGIN + shift_y_axis;
    
    Y_Spacing = PDF_GRAPH_SKETCH_MAX_HEIGHT/ (NUM_OF_Y_GRID_LINES - 1.0);
    Y_Factor = 0.0;

    for(nCtr = 0;nCtr < NUM_OF_Y_GRID_LINES;nCtr++){
    
    	Y_Factor = (unsigned short)(Y_Spacing * nCtr);
    	
        // print to PDF the horizontal lines..  
        PDF_DrawLine(page, gd_X1, gd_Y1 + Y_Factor, gd_X2, gd_Y2 + Y_Factor);           
    }
    


    //==========================================================================
    // draw temperature setpoint lines ....
    //==========================================================================

    if(!((_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg == CREATE_PDF_WO_ALM_COND_INFO)^
         (_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg == CREATE_PDF_WO_ALM_COND_ALM_STAT_INFO)^
         (_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg == CREATE_PDF_WO_ALM_COND_ALM_STAT_LOG_RES))){
    
        HPDF_Page_SetLineWidth (page, 0.8f);    
        HPDF_Page_SetDash (page, DASH_MODE1, 0, 0);
        
        for(lubAlarmNum = ALARM_1;lubAlarmNum <= ALARM_6 ;lubAlarmNum++){
            
            // test each setpoint which one is the highest...
            _lsBitAlarm_ConfigB0.ubByte = _gsAlarmConfig[lubAlarmNum].ubConfigByte0;      
             
            if(_lsBitAlarm_ConfigB0.Bit.Enable == 1){
            
                lfIndent += 20.0;

                //====================================    
                // get setpoint lines color codes...
                //====================================
                switch(lubAlarmNum){
                case(ALARM_1):
                    HPDF_Page_SetRGBStroke (page, 0.8f, 0.0f, 0.0f);
                    HPDF_Page_SetRGBFill (page, 0.8f, 0.0f, 0.0f);
                    ludwTimeplot = _gsUser_Configuration_Area.udwAlarm1_TriggeredTimeStamp - _gsUser_Configuration_Area.udwUnit_Start_TimeStamp;
                    break;
                case(ALARM_2):
                    HPDF_Page_SetRGBStroke (page, 1.0f, 0.4f, 0.0f);
                    HPDF_Page_SetRGBFill (page, 1.0f, 0.4f, 0.0f);
                    ludwTimeplot = _gsUser_Configuration_Area.udwAlarm2_TriggeredTimeStamp - _gsUser_Configuration_Area.udwUnit_Start_TimeStamp;
                    break;
                case(ALARM_3):
                    HPDF_Page_SetRGBStroke (page, 0.4f, 0.4f, 0.0f);
                    HPDF_Page_SetRGBFill (page, 0.4f, 0.4f, 0.0f);
                    ludwTimeplot = _gsUser_Configuration_Area.udwAlarm3_TriggeredTimeStamp - _gsUser_Configuration_Area.udwUnit_Start_TimeStamp;
                    break;
                case(ALARM_4):
                    HPDF_Page_SetRGBStroke (page, 0.2f, 0.4f, 0.4f);        
                    HPDF_Page_SetRGBFill (page, 0.2f, 0.4f, 0.4f);
                    ludwTimeplot = _gsUser_Configuration_Area.udwAlarm4_TriggeredTimeStamp - _gsUser_Configuration_Area.udwUnit_Start_TimeStamp;
                    break;
                case(ALARM_5):
                    HPDF_Page_SetRGBStroke (page, 0.4f, 0.2f, 0.8f);
                    HPDF_Page_SetRGBFill (page, 0.4f, 0.2f, 0.8f);
                    ludwTimeplot = _gsUser_Configuration_Area.udwAlarm5_TriggeredTimeStamp - _gsUser_Configuration_Area.udwUnit_Start_TimeStamp;
                    break;
                case(ALARM_6):
                    HPDF_Page_SetRGBStroke (page, 0.6f, 0.4f, 0.6f);        
                    HPDF_Page_SetRGBFill (page, 0.6f, 0.4f, 0.6f);
                    ludwTimeplot = _gsUser_Configuration_Area.udwAlarm6_TriggeredTimeStamp - _gsUser_Configuration_Area.udwUnit_Start_TimeStamp;
                    break;
                }
    
        
                //=======================================================    
                // get setpoint coordinates and draw lines and text...
                //=======================================================            
                switch(_lsBitAlarm_ConfigB0.Bit.Type){
                case(HTL_SE): // fallthrough...
                case(HTL_CE): // fallthrough...
                            
  		            gd_Y1 = PLOT_Y_ORIGIN + shift_y_axis + (unsigned short)((((float)_gsAlarmConfig[lubAlarmNum].fRange1HighTempSetPoint - _sPDFGridLinesScaledValues.fLowerMostTempLevel) * _sPDFGridLinesScaledValues.fTempStepResolution));
                
                    // Single Line w/ High Limit only...  
                    PDF_DrawLine(page, gd_X1, gd_Y1, gd_X2 + 5.0f, gd_Y1);                
                
                    // draw setpoint temperature label .. 
                    if(lucDoubleDecimalFlagEnable==1){
                        lfDdecVal = PDF_GetTemperatureUnitValue(_gsAlarmConfig[lubAlarmNum].fRange1HighTempSetPoint);
                        if(_lsBitAlarm_ConfigB0.Bit.Type==MKT_HI){
                        	sprintf((char *)&TempValueStr[0],"%6.2f MKT",(double)utilDbDec_RoundOFF(lfDdecVal));
                        }	
                        else{
                        	sprintf((char *)&TempValueStr[0],"%6.2f",(double)utilDbDec_RoundOFF(lfDdecVal));
                        }	
                        
                    }    
                    else{
	                    if(_lsBitAlarm_ConfigB0.Bit.Type==MKT_HI){
                        	sprintf((char *)&TempValueStr[0],"%6.1f MKT",(double)PDF_GetTemperatureUnitValue(_gsAlarmConfig[lubAlarmNum].fRange1HighTempSetPoint));
                        }   	
                        else{
                        	sprintf((char *)&TempValueStr[0],"%6.1f",(double)PDF_GetTemperatureUnitValue(_gsAlarmConfig[lubAlarmNum].fRange1HighTempSetPoint));        
                        }	
                    }    
                        
                    HPDF_Page_BeginText (page);    
                    PDF_SetText(page, gd_X1 + lfIndent, gd_Y1+2.0f, TempValueStr);         
                    HPDF_Page_EndText (page);    
                
                    break;


                case(MKT_HI): // fallthrough...
                    
                    //Using a time threshold for MKT High
                    if(_gsUser_Configuration_Area.ubAlarmStatus & (0x1<<lubAlarmNum)){

                        // Pls note at this point the if the Time Only is triggered then its threshhold should always be greater than its datapoint count...

                        if (ludwTimeplot > 0)
                        {
                            ldPointTrig = (double)ludwTimeplot;
                            ldPointTrig /= (double)_gsUser_Configuration_Area.udwMeasurementInterval;
                            ldPointTrig /= (double)(_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints-1);
                            ldPointTrig *= PDF_GRAPH_MAX_WIDTH_DOT_RESOLUTION;
                            lfDataPointTrig = (float)ldPointTrig; // whole number...
                        }
                        // check if the line is at start time stamp!...
                        else{
                            lfDataPointTrig = 0;
                        }

                        if(lfDataPointTrig > PDF_GRAPH_SKETCH_MAX_WIDTH) lfDataPointTrig = PDF_GRAPH_MAX_WIDTH_DOT_RESOLUTION;

                        // Time only verical line...
                        PDF_DrawLine(page, PLOT_X_ORIGIN + lfDataPointTrig, PLOT_Y_ORIGIN + shift_y_axis- 5.0f, PLOT_X_ORIGIN + lfDataPointTrig, PLOT_Y_ORIGIN + shift_y_axis + PDF_GRAPH_SKETCH_MAX_HEIGHT);

                        //////////////////////////////////////////////////////
                        if(_gsGenCfgOptByte.GenOpt.Byte1.F_C_Unit_Select==0){
                             lcValue = 'C';
                             lfR1HighTempVal = TEMP_C(_gsAlarmConfig[lubAlarmNum].fRange1HighTempSetPoint);

                            if(lucDoubleDecimalFlagEnable==1){
                                 lfDdecVal = utilDbDec_RoundOFF(lfR1HighTempVal);
                                 lfR1HighTempVal = lfDdecVal;
                            }
                        }
                        else{
                            lcValue = 'F';
                            lfR1HighTempVal = _gsAlarmConfig[lubAlarmNum].fRange1HighTempSetPoint;
                         }

                        if(lucDoubleDecimalFlagEnable==1)
                            sprintf((char*)&TempValueStr[0],"MKT > %0.2f %c%c",(double)lfR1HighTempVal,lcSymbolDeg,lcValue); // double decimal...
                        else
                            sprintf((char*)&TempValueStr[0],"MKT > %0.1f %c%c",(double)lfR1HighTempVal,lcSymbolDeg,lcValue); // single decimal...




                        ///////////
                        // get time end string...
                      //  PDF_FormatTimeSecsDHMSToString(_gsAlarmConfig[lubAlarmNum].udwThreshhold * _gsUser_Configuration_Area.udwMeasurementInterval, (char*)&TempValueStr[0]);

                        // draw setpoint temperature label ..
                        PDF_Angle_SetText(page, PLOT_X_ORIGIN + lfDataPointTrig - 2.0f, PLOT_Y_ORIGIN + shift_y_axis + PDF_GRAPH_SKETCH_MAX_HEIGHT - 2.0f - (3 * strlen(TempValueStr)), TEMP_LABEL_VERT_DISP_ANGLE, TempValueStr);

                    }  /*   End of Time Only alarm type select   */

                    break;

            
                case(LTL_SE): // fallthrough...
                case(LTL_CE): // fallthrough...
                
	   	        
  		            gd_Y1 = PLOT_Y_ORIGIN + shift_y_axis + (unsigned short)((((float)_gsAlarmConfig[lubAlarmNum].fRange1LowTempSetPoint - _sPDFGridLinesScaledValues.fLowerMostTempLevel) * _sPDFGridLinesScaledValues.fTempStepResolution));
                
                    // Single Line w/ Low Limit only...  
                    PDF_DrawLine(page, gd_X1, gd_Y1, gd_X2 + 5.0f, gd_Y1);                

                    // draw setpoint temperature label .. 
                    if(lucDoubleDecimalFlagEnable==1){
                        lfDdecVal = PDF_GetTemperatureUnitValue(_gsAlarmConfig[lubAlarmNum].fRange1LowTempSetPoint);
                        if(_lsBitAlarm_ConfigB0.Bit.Type==MKT_LO){
                        	sprintf((char *)&TempValueStr[0],"%6.2f MKT",(double)utilDbDec_RoundOFF(lfDdecVal));
                        }
                        else{
                        	sprintf((char *)&TempValueStr[0],"%6.2f",(double)utilDbDec_RoundOFF(lfDdecVal));
                        }                        
                    }    
                    else{
	                    if(_lsBitAlarm_ConfigB0.Bit.Type==MKT_LO){
                        	sprintf((char *)&TempValueStr[0],"%6.1f MKT",(double)PDF_GetTemperatureUnitValue(_gsAlarmConfig[lubAlarmNum].fRange1LowTempSetPoint));        
                     	}
                     	else{
                     		sprintf((char *)&TempValueStr[0],"%6.1f",(double)PDF_GetTemperatureUnitValue(_gsAlarmConfig[lubAlarmNum].fRange1LowTempSetPoint));        
                     	}                        
                    }    
                    
                    
                    HPDF_Page_BeginText (page);    
                    PDF_SetText(page, gd_X1 + lfIndent, gd_Y1-6.0f, TempValueStr);         
                    HPDF_Page_EndText (page);    

                    break;

                case(MKT_LO): // fallthrough...

                    if(_gsUser_Configuration_Area.ubAlarmStatus & (0x1<<lubAlarmNum)){

                              if (ludwTimeplot > 0)
                            {
                                ldPointTrig = (double)ludwTimeplot;
                                ldPointTrig /= (double)_gsUser_Configuration_Area.udwMeasurementInterval;
                                ldPointTrig /= (double)(_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints-1);
                                ldPointTrig *= PDF_GRAPH_MAX_WIDTH_DOT_RESOLUTION;
                                lfDataPointTrig = (float)ldPointTrig; // whole number...
                            }
                            // check if the line is at start time stamp!...
                            else{
                                lfDataPointTrig = 0;
                            }

                            if(lfDataPointTrig > PDF_GRAPH_SKETCH_MAX_WIDTH) lfDataPointTrig = PDF_GRAPH_MAX_WIDTH_DOT_RESOLUTION;

                            // Time only verical line...
                            PDF_DrawLine(page, PLOT_X_ORIGIN + lfDataPointTrig, PLOT_Y_ORIGIN + shift_y_axis - 5.0f, PLOT_X_ORIGIN + lfDataPointTrig, PLOT_Y_ORIGIN + + shift_y_axis + PDF_GRAPH_SKETCH_MAX_HEIGHT);

                            //////////////////////////////////////////////////////
                            if(_gsGenCfgOptByte.GenOpt.Byte1.F_C_Unit_Select==0){
                                 lcValue = 'C';
                                 lfR1LowTempVal = TEMP_C(_gsAlarmConfig[lubAlarmNum].fRange1LowTempSetPoint);

                                if(lucDoubleDecimalFlagEnable==1){
                                     lfDdecVal = utilDbDec_RoundOFF(lfR1LowTempVal);
                                     lfR1LowTempVal = lfDdecVal;
                                }
                            }
                            else{
                                lcValue = 'F';
                                lfR1LowTempVal = _gsAlarmConfig[lubAlarmNum].fRange1LowTempSetPoint;
                             }

                            if(lucDoubleDecimalFlagEnable==1)
                                sprintf((char*)&TempValueStr[0],"MKT < %0.2f %c%c",(double)lfR1LowTempVal,lcSymbolDeg,lcValue); // double decimal...
                            else
                                sprintf((char*)&TempValueStr[0],"MKT < %0.1f %c%c",(double)lfR1LowTempVal,lcSymbolDeg,lcValue); // single decimal...




                            ///////////
                            // get time end string...
                          //  PDF_FormatTimeSecsDHMSToString(_gsAlarmConfig[lubAlarmNum].udwThreshhold * _gsUser_Configuration_Area.udwMeasurementInterval, (char*)&TempValueStr[0]);

                            // draw setpoint temperature label ..
                            PDF_Angle_SetText(page, PLOT_X_ORIGIN + lfDataPointTrig - 2.0f, PLOT_Y_ORIGIN + PDF_GRAPH_SKETCH_MAX_HEIGHT + shift_y_axis + - 2.0f - (3 * strlen(TempValueStr)), TEMP_LABEL_VERT_DISP_ANGLE, TempValueStr);

                        }  /*   End of Time Only alarm type select   */

                    break;

                    /////////////////////////////////////////////
                
                case(STR_SE): // fallthrough...
                case(STR_CE): // fallthrough...
            
                    //=======================
                    // Single Temp lines...                  
                    //=======================
                
                    //------------
                    // Range#1...
                    //------------
                
                    gd_Y1 = PLOT_Y_ORIGIN + shift_y_axis + (unsigned short)((((float)_gsAlarmConfig[lubAlarmNum].fRange1HighTempSetPoint - _sPDFGridLinesScaledValues.fLowerMostTempLevel) * _sPDFGridLinesScaledValues.fTempStepResolution));
                    // Single Line w/ High Limit only...  
                    PDF_DrawLine(page, gd_X1, gd_Y1, gd_X2 + 5.0f, gd_Y1);

                    // draw setpoint temperature label .. 
                    if(lucDoubleDecimalFlagEnable==1){
                        lfDdecVal = PDF_GetTemperatureUnitValue(_gsAlarmConfig[lubAlarmNum].fRange1HighTempSetPoint);
                        sprintf((char *)&TempValueStr[0],"%6.2f",(double)utilDbDec_RoundOFF(lfDdecVal));
                    }    
                    else{
                        sprintf((char *)&TempValueStr[0],"%6.1f",(double)PDF_GetTemperatureUnitValue(_gsAlarmConfig[lubAlarmNum].fRange1HighTempSetPoint));        
                    }    
                    
                    
                    HPDF_Page_BeginText (page);    
                    PDF_SetText(page, gd_X1 + lfIndent, gd_Y1+2.0f, TempValueStr);         
                    HPDF_Page_EndText (page);    
                
                    gd_Y1 = PLOT_Y_ORIGIN + shift_y_axis + (unsigned short)((((float)_gsAlarmConfig[lubAlarmNum].fRange1LowTempSetPoint - _sPDFGridLinesScaledValues.fLowerMostTempLevel) * _sPDFGridLinesScaledValues.fTempStepResolution));
                    // Single Line w/ Low Limit only...  
                    PDF_DrawLine(page, gd_X1, gd_Y1, gd_X2 + 5.0f, gd_Y1);

                    // draw setpoint temperature label .. 
                    if(lucDoubleDecimalFlagEnable==1){
                        lfDdecVal = PDF_GetTemperatureUnitValue(_gsAlarmConfig[lubAlarmNum].fRange1LowTempSetPoint);
                        sprintf((char *)&TempValueStr[0],"%6.2f",(double)utilDbDec_RoundOFF(lfDdecVal));
                    }    
                    else{
                        sprintf((char *)&TempValueStr[0],"%6.1f",(double)PDF_GetTemperatureUnitValue(_gsAlarmConfig[lubAlarmNum].fRange1LowTempSetPoint));        
                    }    
                    
                    HPDF_Page_BeginText (page);    
                    PDF_SetText(page, gd_X1 + lfIndent, gd_Y1-6.0f, TempValueStr);         
                    HPDF_Page_EndText (page);    

                    break;
                
                case(DTR_SE): // fallthrough...
                case(DTR_CE): // fallthrough...
            
                    //=======================
                    // Dual Temp lines...  
                    //=======================

                    //------------
                    // Range#1...
                    //------------
                    gd_Y1 = PLOT_Y_ORIGIN + shift_y_axis + (unsigned short)((((float)_gsAlarmConfig[lubAlarmNum].fRange1HighTempSetPoint - _sPDFGridLinesScaledValues.fLowerMostTempLevel) * _sPDFGridLinesScaledValues.fTempStepResolution));
                    // Single Line w/ High Limit only...  
                    PDF_DrawLine(page, gd_X1, gd_Y1, gd_X2 + 5.0f, gd_Y1);

                    // draw setpoint temperature label .. 
                    if(lucDoubleDecimalFlagEnable==1){
                        lfDdecVal = PDF_GetTemperatureUnitValue(_gsAlarmConfig[lubAlarmNum].fRange1HighTempSetPoint);                                                
                        sprintf((char *)&TempValueStr[0],"%6.2f",(double)utilDbDec_RoundOFF(lfDdecVal));        
                    }    
                    else
                        sprintf((char *)&TempValueStr[0],"%6.1f",(double)PDF_GetTemperatureUnitValue(_gsAlarmConfig[lubAlarmNum].fRange1HighTempSetPoint));        
                    
                    HPDF_Page_BeginText (page);    
                    PDF_SetText(page, gd_X1 + lfIndent, gd_Y1+2.0f, TempValueStr);         
                    HPDF_Page_EndText (page);    

                
                    gd_Y1 = PLOT_Y_ORIGIN + shift_y_axis + (unsigned short)((((float)_gsAlarmConfig[lubAlarmNum].fRange1LowTempSetPoint - _sPDFGridLinesScaledValues.fLowerMostTempLevel) * _sPDFGridLinesScaledValues.fTempStepResolution));
                    // Single Line w/ Low Limit only...  
                    PDF_DrawLine(page, gd_X1, gd_Y1, gd_X2 + 5.0f, gd_Y1);

                    // draw setpoint temperature label .. 
                    if(lucDoubleDecimalFlagEnable==1){
                        lfDdecVal = PDF_GetTemperatureUnitValue(_gsAlarmConfig[lubAlarmNum].fRange1LowTempSetPoint);                                                
                        sprintf((char *)&TempValueStr[0],"%6.2f",(double)utilDbDec_RoundOFF(lfDdecVal));
                    }    
                    else
                        sprintf((char *)&TempValueStr[0],"%6.1f",(double)PDF_GetTemperatureUnitValue(_gsAlarmConfig[lubAlarmNum].fRange1LowTempSetPoint));        
                    
                    HPDF_Page_BeginText (page);    
                    PDF_SetText(page, gd_X1 + lfIndent, gd_Y1-6.0f, TempValueStr);         
                    HPDF_Page_EndText (page);    
                

                    //------------
                    // Range#2...
                    //------------
                    gd_Y1 = PLOT_Y_ORIGIN + shift_y_axis + (unsigned short)((((float)_gsAlarmConfig[lubAlarmNum].fRange2HighTempSetPoint - _sPDFGridLinesScaledValues.fLowerMostTempLevel) * _sPDFGridLinesScaledValues.fTempStepResolution));
                    // Single Line w/ High Limit only...  
                    PDF_DrawLine(page, gd_X1, gd_Y1, gd_X2 + 5.0f, gd_Y1);
                
                    // draw setpoint temperature label .. 
                    if(lucDoubleDecimalFlagEnable==1){
                        lfDdecVal = PDF_GetTemperatureUnitValue(_gsAlarmConfig[lubAlarmNum].fRange2HighTempSetPoint);
                        sprintf((char *)&TempValueStr[0],"%6.2f",(double)utilDbDec_RoundOFF(lfDdecVal));
                    }    
                    else
                        sprintf((char *)&TempValueStr[0],"%6.1f",(double)PDF_GetTemperatureUnitValue(_gsAlarmConfig[lubAlarmNum].fRange2HighTempSetPoint));        
                    
                    
                    HPDF_Page_BeginText (page);    
                    PDF_SetText(page, gd_X1 + lfIndent, gd_Y1+2.0f, TempValueStr);         
                    HPDF_Page_EndText (page);    
                
                
                    gd_Y1 = PLOT_Y_ORIGIN + shift_y_axis + (unsigned short)((((float)_gsAlarmConfig[lubAlarmNum].fRange2LowTempSetPoint - _sPDFGridLinesScaledValues.fLowerMostTempLevel) * _sPDFGridLinesScaledValues.fTempStepResolution));
                    // Single Line w/ Low Limit only...  
                    PDF_DrawLine(page, gd_X1, gd_Y1, gd_X2 + 5.0f, gd_Y1);
                
                    // draw setpoint temperature label .. 
                    if(lucDoubleDecimalFlagEnable==1){
                        lfDdecVal = PDF_GetTemperatureUnitValue(_gsAlarmConfig[lubAlarmNum].fRange2LowTempSetPoint);                        
                        sprintf((char *)&TempValueStr[0],"%6.2f",(double)utilDbDec_RoundOFF(lfDdecVal)); 
                    }    
                    else
                        sprintf((char *)&TempValueStr[0],"%6.1f",(double)PDF_GetTemperatureUnitValue(_gsAlarmConfig[lubAlarmNum].fRange2LowTempSetPoint));        
                    
                    HPDF_Page_BeginText (page);    
                    PDF_SetText(page, gd_X1 + lfIndent, gd_Y1-6.0f, TempValueStr);          
                    HPDF_Page_EndText (page);    

                    break;
                case(TIME_ONLY):
                
                    //=======================
                    // Time only...  
                    //=======================
                         
                    // take note that threshold is considered to be a data point at the time reached...
                    // check if the time only is triggered!...
                    if(_gsUser_Configuration_Area.ubAlarmStatus & (0x1<<lubAlarmNum)){
                        
                        // Pls note at this point the if the Time Only is triggered then its threshhold should always be greater than its datapoint count...
                    
                        // check if the line is at start time stamp!...                    
                        if(_gsAlarmConfig[lubAlarmNum].udwThreshhold > 0){  // if threshold is not zero...                                	         
                            ldPointTrig = (double)_gsAlarmConfig[lubAlarmNum].udwThreshhold;                            
                            ldPointTrig /= (double)(_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints-1);                            
                            ldPointTrig *= PDF_GRAPH_MAX_WIDTH_DOT_RESOLUTION;                                                        
                            lfDataPointTrig = (float)ldPointTrig; // whole number...                            
                        }
                        else{                    
                            lfDataPointTrig = 0;
                        }
                        
                        if(lfDataPointTrig > PDF_GRAPH_SKETCH_MAX_WIDTH) lfDataPointTrig = PDF_GRAPH_MAX_WIDTH_DOT_RESOLUTION;
  
                        // Time only verical line...  
                        PDF_DrawLine(page, PLOT_X_ORIGIN + lfDataPointTrig, PLOT_Y_ORIGIN + shift_y_axis - 5.0f, PLOT_X_ORIGIN + lfDataPointTrig, PLOT_Y_ORIGIN + shift_y_axis + PDF_GRAPH_SKETCH_MAX_HEIGHT);
                    
                        // get time end string...                        
                        PDF_FormatTimeSecsDHMSToString(_gsAlarmConfig[lubAlarmNum].udwThreshhold * _gsUser_Configuration_Area.udwMeasurementInterval, (char*)&TempValueStr[0]);
                    
                        // draw setpoint temperature label ..                                                 
                        PDF_Angle_SetText(page, PLOT_X_ORIGIN + lfDataPointTrig - 2.0f, PLOT_Y_ORIGIN + shift_y_axis + PDF_GRAPH_SKETCH_MAX_HEIGHT - 2.0f - (3 * strlen(TempValueStr)), TEMP_LABEL_VERT_DISP_ANGLE, TempValueStr);
                        
                    }  /*   End of Time Only alarm type select   */
                    
                    break;
                    
                } /*   End of Switch statement   */                
            } /*   End of Alarm Enable check   */            
        } /*   End of scan alarm status   */                   
    } /* End of setpoint lines */
       

    // note: this must be separated for the sake of saving pdf file memory.....mjm
    
    
    
    HPDF_Page_BeginText (page);
    //==========================================================================    
    // create scaled temperature text labels ....
    //==========================================================================
    HPDF_Page_SetRGBFill (page, 0.0f, 0.0f, 0.0f);
    gd_Y1 = PLOT_Y_ORIGIN + shift_y_axis;
    for(nCtr = 0;nCtr < NUM_OF_Y_GRID_LINES;nCtr++){
        
        Y_Factor = (unsigned short)(Y_Spacing * nCtr);
        
        fTempPerDivisionValue = _sPDFGridLinesScaledValues.fLowerMostTempLevel + (_sPDFGridLinesScaledValues.fTempPerDivisionFactor * nCtr);
		if(_gsGenCfgOptByte.GenOpt.Byte1.F_C_Unit_Select == 0){  // 0-C .. 1-F    
    		
			//fTempPerDivisionValue = TEMP_C(fTempPerDivisionValue);
			fTempCVal = TEMP_C(fTempPerDivisionValue);
			
			fTempPerDivisionValue = fTempCVal;
			
			if(lucDoubleDecimalFlagEnable==1){			
			    lfDdecVal = utilDbDec_RoundOFF(fTempCVal);
			    fTempPerDivisionValue = lfDdecVal;
			}
		}
					
        // print temperature values .. 
        if(lucDoubleDecimalFlagEnable==1)
            sprintf((char *)&TempValueStr[0],"%6.2f",(double)fTempPerDivisionValue);
        else
            sprintf((char *)&TempValueStr[0],"%6.1f",(double)fTempPerDivisionValue);
            
        
        PDF_SetText(page, gd_X1 - 20.0f, (gd_Y1-2.0f) + Y_Factor, TempValueStr);         
    }
    HPDF_Page_EndText (page);
    
    //==========================================================================
    // draw vertical text Temperature Unit....
    //==========================================================================
    HPDF_Page_SetFontAndSize (page, def_font, 8);
    HPDF_Page_SetRGBFill (page, 0.2f, 0.0f, 1.0f);  // set to blue color text...
    
    if(_gsGenCfgOptByte.GenOpt.Byte1.F_C_Unit_Select == 1){  // 0-C .. 1-F    
        sprintf((char *)&TempValueStr[0],"Temperature(%cF)",lcSymbolDeg);
        PDF_Angle_SetText(page, TEMP_LABEL_VERT_DISP_X, TEMP_LABEL_VERT_DISP_Y + shift_y_axis, TEMP_LABEL_VERT_DISP_ANGLE, TempValueStr);
    }    
    else{
        sprintf((char *)&TempValueStr[0],"Temperature(%cC)",lcSymbolDeg);
        PDF_Angle_SetText(page, TEMP_LABEL_VERT_DISP_X, TEMP_LABEL_VERT_DISP_Y + shift_y_axis, TEMP_LABEL_VERT_DISP_ANGLE, TempValueStr);
    }    
     
    HPDF_Page_SetFontAndSize (page, def_font, 6);
    
}

/*******************************************************************************
**  Function     : PDF_SetUpMarker
**  Description  : setups the marker line
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void PDF_SetUpMarker(HPDF_Page page){
    HPDF_Page_SetRGBStroke(page, 0.0f, 0.6f, 0.0f); 
    HPDF_Page_SetLineWidth(page, 0.5f);
}

/*******************************************************************************
**  Function     : PDF_DrawMarker
**  Description  : draw the marker arrows
**  PreCondition : setup first the marker line
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void PDF_DrawMarker(HPDF_Page page, float x, float y)
{
	HPDF_Page_MoveTo (page,x-2, y);	// left
	HPDF_Page_LineTo (page,x, y-5); // left, down
	HPDF_Page_LineTo (page,x, y+5); // down, up
	HPDF_Page_LineTo (page,x, y-5); // up, down
	HPDF_Page_LineTo (page,x+2, y);	// down, right	
}



/*******************************************************************************
**  Function     : PDF_GetHighLowExtremeSetPoints
**  Description  : finds the high and low extreme points
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
// a flag to if any of the alarm is enabled....
BOOL gbHLExtremeAlarmEnaStatFlag;

void PDF_GetHighLowExtremeSetPoints(void){
    
    __sBITS_ALARM_CONFIG_BYTE0 _lsBitAlarm_ConfigB0; 
    register float lfTempBuffer;  
    register UINT8 lubAlarmNum;
    
    
    // init alarm enable status flag...
    gbHLExtremeAlarmEnaStatFlag = FALSE;


    //----------------------------------------------------------------
    // scan alarms settings and seek the highest extreme setpoint...
    //----------------------------------------------------------------


    // initialize extreme high low temperatures reference point....   
    _sPDFGridLinesScaledValues.fExtremeHighSetpoint = _gsLogMinMaxTempData.fExtremeHighTemp_F_Deg;    
    _sPDFGridLinesScaledValues.fExtremeLowSetpoint = _gsLogMinMaxTempData.fExtremeLowTemp_F_Deg;    
    
    for(lubAlarmNum = ALARM_1 ; lubAlarmNum <= ALARM_6 ; lubAlarmNum++){        
        
        // test each setpoint which one is the highest...
        _lsBitAlarm_ConfigB0.ubByte = _gsAlarmConfig[lubAlarmNum].ubConfigByte0;           
        
        if(_lsBitAlarm_ConfigB0.Bit.Enable == 1){        
            
            // alarm enabled is present!... 
            gbHLExtremeAlarmEnaStatFlag = TRUE;

            switch(_lsBitAlarm_ConfigB0.Bit.Type){
            case(HTL_SE) : // High Temp Limit     - Single Event        .. no need to evaluate high temp...
            case(HTL_CE) : // High Temp Limit     - Cummulative Event   .. no need to evaluate high temp...
            case(MKT_HI) : // High MKT Temp Limit - Single Event        .. no need to evaluate high temp...
            
                //-------------------------------------------
                // Range 1 high temperature setpoint only...
                //-------------------------------------------
                
                // check Range1 high...
                if(_gsAlarmConfig[lubAlarmNum].fRange1HighTempSetPoint >= _sPDFGridLinesScaledValues.fExtremeHighSetpoint)
                    _sPDFGridLinesScaledValues.fExtremeHighSetpoint = _gsAlarmConfig[lubAlarmNum].fRange1HighTempSetPoint;                                    
                
                // check Range1 high...
                if(_gsAlarmConfig[lubAlarmNum].fRange1HighTempSetPoint <= _sPDFGridLinesScaledValues.fExtremeLowSetpoint)
                    _sPDFGridLinesScaledValues.fExtremeLowSetpoint = _gsAlarmConfig[lubAlarmNum].fRange1HighTempSetPoint;                    
                    
                break;            
            case(LTL_SE) : // Low Temp Limit      - Single Event        .. no need to evaluate low temp...
            case(LTL_CE) : // Low Temp Limit      - Cummulative Event   .. no need to evaluate low temp...
            case(MKT_LO) : // Low MKT Temp Limit  - Single Event        .. no need to evaluate low temp...
            
                //-------------------------------------------
                // Range 1 low temperature setpoint only...
                //-------------------------------------------

                // check Range1 low...
                if(_gsAlarmConfig[lubAlarmNum].fRange1LowTempSetPoint >= _sPDFGridLinesScaledValues.fExtremeHighSetpoint)
                    _sPDFGridLinesScaledValues.fExtremeHighSetpoint = _gsAlarmConfig[lubAlarmNum].fRange1LowTempSetPoint;                                    
                
                // check Range1 low...
                if(_gsAlarmConfig[lubAlarmNum].fRange1LowTempSetPoint <= _sPDFGridLinesScaledValues.fExtremeLowSetpoint)
                    _sPDFGridLinesScaledValues.fExtremeLowSetpoint = _gsAlarmConfig[lubAlarmNum].fRange1LowTempSetPoint;                    

                break;
            case(STR_SE) : // Single Temp Range - Single Event       .. fallthrough...
            case(STR_CE) : // Single Temp Range - Cummulative Event  .. fallthrough...            
            
                //-------------------------------------------
                // Range 1 high temperature setpoint only...
                //-------------------------------------------

                // check Range1 high...
                if(_gsAlarmConfig[lubAlarmNum].fRange1HighTempSetPoint >= _sPDFGridLinesScaledValues.fExtremeHighSetpoint)
                    _sPDFGridLinesScaledValues.fExtremeHighSetpoint = _gsAlarmConfig[lubAlarmNum].fRange1HighTempSetPoint;                                    
                
                // check Range1 high...
                if(_gsAlarmConfig[lubAlarmNum].fRange1HighTempSetPoint <= _sPDFGridLinesScaledValues.fExtremeLowSetpoint)
                    _sPDFGridLinesScaledValues.fExtremeLowSetpoint = _gsAlarmConfig[lubAlarmNum].fRange1HighTempSetPoint;                    

                
                //-------------------------------------------
                // Range 1 low temperature setpoint only...
                //-------------------------------------------
                
                // check Range1 low...
                if(_gsAlarmConfig[lubAlarmNum].fRange1LowTempSetPoint >= _sPDFGridLinesScaledValues.fExtremeHighSetpoint)
                    _sPDFGridLinesScaledValues.fExtremeHighSetpoint = _gsAlarmConfig[lubAlarmNum].fRange1LowTempSetPoint;                                    
                
                // check Range1 low...
                if(_gsAlarmConfig[lubAlarmNum].fRange1LowTempSetPoint <= _sPDFGridLinesScaledValues.fExtremeLowSetpoint)
                    _sPDFGridLinesScaledValues.fExtremeLowSetpoint = _gsAlarmConfig[lubAlarmNum].fRange1LowTempSetPoint;                    

                break;
            case(DTR_SE) : // Dual Temp Range   - Single Event       .. fallthrough...
            case(DTR_CE) : // Dual Temp Range   - Cummulative Event  .. fallthrough...

                //-------------------------------------------
                // Range 1 high temperature setpoint only...
                //-------------------------------------------

                // check Range1 high...
                if(_gsAlarmConfig[lubAlarmNum].fRange1HighTempSetPoint >= _sPDFGridLinesScaledValues.fExtremeHighSetpoint)
                    _sPDFGridLinesScaledValues.fExtremeHighSetpoint = _gsAlarmConfig[lubAlarmNum].fRange1HighTempSetPoint;                                    
                
                // check Range1 high...
                if(_gsAlarmConfig[lubAlarmNum].fRange1HighTempSetPoint <= _sPDFGridLinesScaledValues.fExtremeLowSetpoint)
                    _sPDFGridLinesScaledValues.fExtremeLowSetpoint = _gsAlarmConfig[lubAlarmNum].fRange1HighTempSetPoint;                    

                
                //-------------------------------------------
                // Range 1 low temperature setpoint only...
                //-------------------------------------------
                
                // check Range1 low...
                if(_gsAlarmConfig[lubAlarmNum].fRange1LowTempSetPoint >= _sPDFGridLinesScaledValues.fExtremeHighSetpoint)
                    _sPDFGridLinesScaledValues.fExtremeHighSetpoint = _gsAlarmConfig[lubAlarmNum].fRange1LowTempSetPoint;                                    
                
                // check Range1 low...
                if(_gsAlarmConfig[lubAlarmNum].fRange1LowTempSetPoint <= _sPDFGridLinesScaledValues.fExtremeLowSetpoint)
                    _sPDFGridLinesScaledValues.fExtremeLowSetpoint = _gsAlarmConfig[lubAlarmNum].fRange1LowTempSetPoint;                    
                    
                    

                //-------------------------------------------
                // Range 2 high temperature setpoint only...
                //-------------------------------------------

                // check Range1 high...
                if(_gsAlarmConfig[lubAlarmNum].fRange2HighTempSetPoint >= _sPDFGridLinesScaledValues.fExtremeHighSetpoint)
                    _sPDFGridLinesScaledValues.fExtremeHighSetpoint = _gsAlarmConfig[lubAlarmNum].fRange2HighTempSetPoint;                                    
                
                // check Range1 high...
                if(_gsAlarmConfig[lubAlarmNum].fRange2HighTempSetPoint <= _sPDFGridLinesScaledValues.fExtremeLowSetpoint)
                    _sPDFGridLinesScaledValues.fExtremeLowSetpoint = _gsAlarmConfig[lubAlarmNum].fRange2HighTempSetPoint;                    

                
                //-------------------------------------------
                // Range 2 low temperature setpoint only...
                //-------------------------------------------
                
                // check Range1 low...
                if(_gsAlarmConfig[lubAlarmNum].fRange2LowTempSetPoint >= _sPDFGridLinesScaledValues.fExtremeHighSetpoint)
                    _sPDFGridLinesScaledValues.fExtremeHighSetpoint = _gsAlarmConfig[lubAlarmNum].fRange2LowTempSetPoint;                                    
                
                // check Range1 low...
                if(_gsAlarmConfig[lubAlarmNum].fRange2LowTempSetPoint <= _sPDFGridLinesScaledValues.fExtremeLowSetpoint)
                    _sPDFGridLinesScaledValues.fExtremeLowSetpoint = _gsAlarmConfig[lubAlarmNum].fRange2LowTempSetPoint;                    
                    
                    
                break;
                
            }        
        }
    }    



    //------------------------------------
    // check Ideal temperature points...
    //------------------------------------
    if(_gsExtdOptByte.ExtOpt.Byte1.IdealTempRgViewEna==1){
        
        // check Ideal High temperature point...
        if(_gsUserCfgAreaData.fTempIdealRangeHighValue >= _sPDFGridLinesScaledValues.fExtremeHighSetpoint)
            _sPDFGridLinesScaledValues.fExtremeHighSetpoint = _gsUserCfgAreaData.fTempIdealRangeHighValue;
            
        // check Ideal Low temperature point...
        if(_gsUserCfgAreaData.fTempIdealRangeLowValue <= _sPDFGridLinesScaledValues.fExtremeLowSetpoint)
            _sPDFGridLinesScaledValues.fExtremeLowSetpoint = _gsUserCfgAreaData.fTempIdealRangeLowValue;
            
    }
    
    
    //---------------------------------------------------------------------------------
    // Evaluate high low temperature points are not reversed...
    // conditions: - should be any of  the alarm is enabled or Ideal temp is enabled.    
    //---------------------------------------------------------------------------------
    if((gbHLExtremeAlarmEnaStatFlag == TRUE) || (_gsExtdOptByte.ExtOpt.Byte1.IdealTempRgViewEna == 1)){
        
        if(_sPDFGridLinesScaledValues.fExtremeHighSetpoint <= _sPDFGridLinesScaledValues.fExtremeLowSetpoint){
            // save highest value to buffer...
            lfTempBuffer = _sPDFGridLinesScaledValues.fExtremeHighSetpoint;
            // save lowest value to Extreme high value.. 
            _sPDFGridLinesScaledValues.fExtremeHighSetpoint = _sPDFGridLinesScaledValues.fExtremeLowSetpoint;
            // save highest value to Extreme low value.. 
            _sPDFGridLinesScaledValues.fExtremeLowSetpoint = lfTempBuffer;                
        }    
    }
    
}

/*******************************************************************************
**  Function     : PDF_CalculateGraphScaleValues
**  Description  : calculates the graph scale values  
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void PDF_CalculateGraphScaleValues(void)
{	
	
	register float fTempRange;
	register float fCheckTempDecimal;
	
	
	// temperature in decimal points degrees for y-scale calculation..
	register double dCheckTempDecimalPart,dAbsCDegTempDecimalPart;
	double dWholeDigitPart;
	
	
	// *** Note: ALL TEMPERATURE VALUE WERE IN F!!!
	
	//==================================================================================
	//
	// ** STEP - 0 : attain the high and low extreme temperature setpoints Alarm 1~6...
	//
	//==================================================================================
	PDF_GetHighLowExtremeSetPoints();
	
	//==================================================================================
	//
	// ** STEP - 1 : Determine the Topmost and Lowermost Temperature Level...
	//
	//==================================================================================

    if((gbHLExtremeAlarmEnaStatFlag == TRUE) || (_gsExtdOptByte.ExtOpt.Byte1.IdealTempRgViewEna==1)){
        
       	//-----------------------
   	    // Get Topmost value..
   	    //-----------------------    	
	 	    // Top most range is the hight extreme setpoint temperature... 
   		    _sPDFGridLinesScaledValues.fTopMostTempLevel = _sPDFGridLinesScaledValues.fExtremeHighSetpoint;   	
	
        //-----------------------
   	    // Get Lowermost value..
   	    //----------------------- 
		    // Top most range is the hight extreme setpoint temperature... 
   	        _sPDFGridLinesScaledValues.fLowerMostTempLevel = _sPDFGridLinesScaledValues.fExtremeLowSetpoint;   	   	
   	}
   	else{
       	    //------------------------------------------------------------------------------------------------
       	    // here! if all alarm is disabled then must use the extreme low and high temperature readings
       	    //       as reference point....  mjmariveles...
       	    //------------------------------------------------------------------------------------------------
       	    
   		    // Top most range is the high extreme temperature... 
   	   	    _sPDFGridLinesScaledValues.fTopMostTempLevel = _gsLogMinMaxTempData.fExtremeHighTemp_F_Deg;
   	
   		    // Top most range is the high extreme temperature... 
   	   	    _sPDFGridLinesScaledValues.fLowerMostTempLevel = _gsLogMinMaxTempData.fExtremeLowTemp_F_Deg;
   	}

	//==================================================================================
	//
	// ** STEP - 2 : After determining the topmost and lowermost temperature level then
	//               calculate the Y-Axis range by adding 1 deg at the topmost and 
	//               subtracting 1 deg at lowermost...
	//
	//==================================================================================
    
    //===================================================================================================
    //
    //        TEMPERATURE Y-AXIS TOPMOST and LOWERMOST SCALING ISSUES....    
    //
    //  Note: there was a problem of Y-axis scaling error due to decimal truncation and whole number display 
    //        plotting limitation. 
    //        This Y-axis scaling method will show the same scaling values presented in the TTMD PC software...
    //
    //===================================================================================================
   
    if(_gsGenCfgOptByte.GenOpt.Byte1.F_C_Unit_Select == 0){  // 0-C .. 1-F    
   
   		// Here is (C) Celsius then process all the high and low temperature coordinate scaling...
   		
        //-----------------------------------------------------------------------------
        // Lower Scale Calculation...  
        //-----------------------------------------------------------------------------
   		dAbsCDegTempDecimalPart = fabs(TEMP_C(_sPDFGridLinesScaledValues.fLowerMostTempLevel));
   		dCheckTempDecimalPart = modf(dAbsCDegTempDecimalPart,&dWholeDigitPart);
		
   		if(dCheckTempDecimalPart <= 0.4){
   			fCheckTempDecimal = TEMP_C(_sPDFGridLinesScaledValues.fLowerMostTempLevel) - ONE_DEG_C;
   		}
   		else{
   			fCheckTempDecimal = TEMP_C(_sPDFGridLinesScaledValues.fLowerMostTempLevel);
   		}
   
   		// note: this portion temperature value is in C deg...
   		fCheckTempDecimal = floor(fCheckTempDecimal);
   		
   		// again take note C deg must converted back to F deg because all calculation is based on F...
   		_sPDFGridLinesScaledValues.fLowerMostTempLevel = TEMP_F(fCheckTempDecimal);
   		
   		
        //-----------------------------------------------------------------------------
        // Upper Scale Calculation...  
        //----------------------------------------------------------------------------- 
   		dAbsCDegTempDecimalPart = fabs(TEMP_C(_sPDFGridLinesScaledValues.fTopMostTempLevel));
   		dCheckTempDecimalPart = modf(dAbsCDegTempDecimalPart,&dWholeDigitPart);
		
		
   		if(dCheckTempDecimalPart <= 0.4){
   			fCheckTempDecimal = TEMP_C(_sPDFGridLinesScaledValues.fTopMostTempLevel) + ONE_DEG_C;
   		}
   		else{
   			fCheckTempDecimal = TEMP_C(_sPDFGridLinesScaledValues.fTopMostTempLevel);
   		}
   		
   		// check C deg decimal if greater than 0.5, if so, then ceil it!... mjm   		
        fCheckTempDecimal = ceil(fCheckTempDecimal);
        
   		// again take note C deg must converted back to F deg because all calculation is based on F...
   		_sPDFGridLinesScaledValues.fTopMostTempLevel = TEMP_F(fCheckTempDecimal);
   
    }
    else{
   
    	// Here is (F) Fahrenheit then process all the high and low temperature coordinate scaling...
   
        //-----------------------------------------------------------------------------
        // Lower Scale Calculation...  
        //-----------------------------------------------------------------------------
   		dAbsCDegTempDecimalPart = fabs(_sPDFGridLinesScaledValues.fLowerMostTempLevel);
   		dCheckTempDecimalPart = modf(dAbsCDegTempDecimalPart,&dWholeDigitPart);
		
    	if(dCheckTempDecimalPart <= 0.4){	
   			fCheckTempDecimal = _sPDFGridLinesScaledValues.fLowerMostTempLevel - ONE_DEG_C;
   		}
   		else{
   			fCheckTempDecimal = _sPDFGridLinesScaledValues.fLowerMostTempLevel;
   		}
   
   		// again take note C deg must converted back to F deg because all calculation is based on F...
   		_sPDFGridLinesScaledValues.fLowerMostTempLevel = floor(fCheckTempDecimal);
   		
   		
        //-----------------------------------------------------------------------------
        // Upper Scale Calculation...  
        //----------------------------------------------------------------------------- 
   		dAbsCDegTempDecimalPart = fabs(_sPDFGridLinesScaledValues.fTopMostTempLevel);
   		dCheckTempDecimalPart = modf(dAbsCDegTempDecimalPart,&dWholeDigitPart);
				
    	if(dCheckTempDecimalPart <= 0.4){	
   			fCheckTempDecimal = _sPDFGridLinesScaledValues.fTopMostTempLevel + ONE_DEG_C;
   		}
   		else{
   			fCheckTempDecimal = _sPDFGridLinesScaledValues.fTopMostTempLevel;
   		}

   		// again take note C deg must converted back to F deg because all calculation is based on F...
   		_sPDFGridLinesScaledValues.fTopMostTempLevel = ceil(fCheckTempDecimal);
   		
    }
    
  	// below are the Y-axis temperature value scaling parameters...
	fTempRange = _sPDFGridLinesScaledValues.fTopMostTempLevel - _sPDFGridLinesScaledValues.fLowerMostTempLevel;	
	_sPDFGridLinesScaledValues.fTempStepResolution = PDF_GRAPH_SKETCH_MAX_HEIGHT / fTempRange;
	
	_sPDFGridLinesScaledValues.fTempPerDivisionFactor = fTempRange / (NUM_OF_Y_GRID_LINES - 1.0);

}



/*****************************************************************************
*
* Function Name: Get_W2Algorithm_MinMaxDataPoint
*
* Description  : will extract the extreme low and high in a single section 
*                group of datapoints. 1 Section = 2 Division = 2 Step Intervals.
*                Then, in 2 divisions must take the extreme low and high datapoint 
*                values. Please refer to the complete explanation and calculation 
*                was presented in the EXCEL file named: 
*                "TT4USB Graphing Method - 2DPSACT.xls"  
*
* Input        : page,*pfDataPoint,usDotNumber,usDotStepInterval
*
* Return       : None
*
* History      : *    Date    *    Author   *   Descriptions
*
*                  9/12/2009    Michael J. Mariveles
******************************************************************************/

/* Constants */

#define EEPROM_FIRST_DATAPOINT        0
#define EEPROM_NEXT_DATAPOINT         1
#define DATAPOINT_START_OFFSET        1

#define NUM_OF_DIVISIONS_PER_SECTION  2
#define _1ST_EXTREME_EVEN_DP          1
#define _2ND_EXTREME_EVEN_DP          2

#define EEPROM_OFFSET_FIRST_DATAPOINT 1
#define SET_FLAG                      0
#define RESET_FLAG                    1



void Get_W2Algorithm_MinMaxDataPoint(float *pfDataPoint, unsigned short usDotNumber, unsigned short usDotStepInterval)
{
    
    // extreme datapoint searching variables.. 
    float fPickNextDataPoint;
    float fDataPoint,fPickMinDataPoint,fPickMaxDataPoint;    
    
    // volatile variables will initialized every time this function is called..
    unsigned short usCtr;
    unsigned char ucTrackMinLevel=0, ucTrackMaxLevel=0;        
    unsigned short usDatapointSectionSize;
    unsigned short usIncDataPoint;    
    unsigned char ucPickUpDataPointFlag;
    
    // static variables saves and tracks down the previous loop state values...
    static unsigned char ucMinMaxNotCompletedFlag=RESET_FLAG, ucReadCycleCtr=0;
    static float f_1stDataPoint=0.0,f_2ndDataPoint=0.0;    
    static unsigned short usDataPointNumber=0;
    
    // fetch temp data marker status...
    unsigned char ubMarkerStatus = 0;
    
    
    if(gb460PtsResetStaticDataFlag==TRUE){
        // lock this after resetting the static data....
        gb460PtsResetStaticDataFlag = FALSE;
        ucMinMaxNotCompletedFlag=RESET_FLAG;
        ucReadCycleCtr=0;f_1stDataPoint=0.0;f_2ndDataPoint=0.0;
        usDataPointNumber=0;
    }    
    
	// this condition "ucMinMaxNotCompletedFlag" will be true if 2 loop cycles is completed in plotting the compressed datapoints..  
	if(ucMinMaxNotCompletedFlag){
		
		// get the total number of sections
    	usDatapointSectionSize = usDotStepInterval * NUM_OF_DIVISIONS_PER_SECTION;
    	
    	// set the EEPROM data fetching one's as 1st initial value to compare the extremes...
    	ucPickUpDataPointFlag = 1;

		// start the loop of 2 division group datapoints in the single section..  
	    for(usCtr=0;usCtr<usDatapointSectionSize;usCtr++){
	    
	    	//NOTES: RAW DATAPOINTS STORED IN EEPROM ARE in F FORMAT....
	    	
	    	// get the start address of group of datapoints belonging to the 1 section with 2 divisions... 
	    	usIncDataPoint = usDataPointNumber + usCtr;

			// here get the datapoints each loop cycle..			
			commTT4Cfg_CacheMem_RetrieveTempData(usIncDataPoint,&ubMarkerStatus,&fDataPoint);
			
			// the 1st datapoint must be used as 1st extreme reference...
			if(ucPickUpDataPointFlag){			
				fPickNextDataPoint = fPickMinDataPoint = fPickMaxDataPoint = fDataPoint;
				ucPickUpDataPointFlag = 0;
			}
			else{
				fPickNextDataPoint = fDataPoint;
			}
		
			//test if value is minimum...
			if(fPickNextDataPoint < fPickMinDataPoint){
				fPickMinDataPoint = fPickNextDataPoint;
				ucTrackMinLevel = usCtr;				
			}

			//test if value is maximum...
			if(fPickNextDataPoint > fPickMaxDataPoint){
				fPickMaxDataPoint = fPickNextDataPoint;
				ucTrackMaxLevel = usCtr;				
			}
		
		}
		
		// check level which is the 1st extreme datapoint occured..
		if(ucTrackMinLevel>ucTrackMaxLevel){				
			f_1stDataPoint = fPickMaxDataPoint;
			f_2ndDataPoint = fPickMinDataPoint;
		}
		else{				
			f_1stDataPoint = fPickMinDataPoint;
			f_2ndDataPoint = fPickMaxDataPoint;
		} 
		
		// set min/max data point assignment counter...
		ucMinMaxNotCompletedFlag = SET_FLAG;
		
		// this cycle will be reset after getting the 2 high/low extreme datapoints..
		ucReadCycleCtr = 0;
		
		// prepare to set the next step interval for the next cycle...
		usDataPointNumber += usDatapointSectionSize;
	}
		
	// every time this function is called will increment 1 DIVISION	per SECTION and to sends out the extreme DP....
	ucReadCycleCtr++;
	
	// 1st cycle - 1st selected EXTREME DATAPOINT....
	if(ucReadCycleCtr==_1ST_EXTREME_EVEN_DP) *pfDataPoint = f_1stDataPoint; // either extreme high or low which ever occured at 1st... 

	// 2nd cycle - 2nd selected EXTREME DATAPOINT....
	if(ucReadCycleCtr==_2ND_EXTREME_EVEN_DP){
	 	*pfDataPoint = f_2ndDataPoint;  // either extreme high or low which ever occured at 2nd... 
	 	ucMinMaxNotCompletedFlag = RESET_FLAG;
	}

}



/*******************************************************************************
**  Function     : BalanceTempVal
**  Description  : validates temperature limits
**  PreCondition : none
**  Side Effects : none
**  Input        : temperature value
**  Output       : returns min/max extreme temperature value
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/

#define TEMP_MAX_EXTREME        2040   //The limits for hign be +95C.
#define TEMP_MIN_EXTREME        -2040  //The limits for low be -95C.

short BalanceTempVal(short val)
{
	short tmp;
	
	if(val > TEMP_MAX_EXTREME) tmp = TEMP_MAX_EXTREME;
    else
	if(val < TEMP_MIN_EXTREME) tmp = TEMP_MIN_EXTREME;
	else                       tmp = val;
	
	return tmp;
}

/*******************************************************************************
**  Function     : CheckMarkPoints
**  Description  : masks 4bit MSB binary temperature value to get marker status 
**  PreCondition : none
**  Side Effects : none
**  Input        : 16bit binary temperature value
**  Output       : 
**                 1 - temperature data has mark
**                 0 - temperature data has no mark
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
UINT8 CheckMarkPoints(UINT16 val)
{
    if((val & 0xf000) == 0xf000) return 1;
    else                         return 0;
}

/*******************************************************************************
**  Function     : ChangeValToTemp
**  Description  : checks the binary temperatutre value and sign if has marker
**  PreCondition : none
**  Side Effects : none
**  Input        : Sensitech binary temperature value
**  Output       : returns the binary temperature value
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
short ChangeValToTemp(short val)
{
	if((val & 0x0800) == 0x0800) val |= 0xf000; //minus
	else                         val &= 0x0fff; //positive number
	
	return BalanceTempVal(val);
}



/*****************************************************************************
*
* Function Name: PDF_GraphPlotterDisplay
*
* Description  : data points PDF graph plotter
*
* Input        : page,def_font,font,xZero,yTop,yTop,*pDat,*pDat2
*                EEPROM data pointer, 1-16k bytes of data
*
* Notes      1#: the old module named TimpGraphDisp made by ALLEN was deleted 
*                totally due to the reason of inflexibility of changes to handle  
*                460 points resolution with new graphing algorithm. \mjm..       
*            2#: some function parameters were retained to avoid implication 
*                when this function called by create_pdf_file.   
*
* Return       : value of y point
*
* History      : *    Date       *    Author         *   Descriptions
*
*                  9/10/2009   Michael J. Mariveles    v70  revised graphing algorithm.
*                  9/06/2009   Michael J. Mariveles    v73  EXTRA_EVEN & EXTRA_DATAPOINTS 
*                                                           were merged to get high/low extremes.                                             
******************************************************************************/


/* Constants */
#define PLOT_X_LIMIT           540.0 
#define PLOT_Y_LIMIT           374.0
#define END_GRAPH_BOX          124.0

#define START_COUNT_VALUE      0 
#define ONE_POINT_ONLY         1
#define END_POINT              1
#define ONE_EXTRA_DATAPOINT    1
#define TWO_EXTRA_DATAPOINT    2
#define _1ST_EXTREME_EXTRA_DP  0
#define _2ND_EXTREME_EXTRA_DP  1


#define X_2Y_POINTS_LOW        461 
#define X_2Y_POINTS_HIGH       290 
#define X_3Y_POINTS_LOW        921 
#define X_3Y_POINTS_HIGH       1380 
#define X_4Y_POINTS_LOW        1381 
#define X_4Y_POINTS_HIGH       1840 


/* structure variable */

struct _DATAPOINTS {

	float fTemperatureValue;
	float fMeasurementInterval;
};


void PDF_GraphPlotterDisplay(HPDF_Page page, HPDF_Font def_font)                                         
{

    float fPlot_X1=0.0,fPlot_X2=0.0;  		// x-axis coordinates line plotter control variables...
    float fPlot_Y1=0.0,fPlot_Y2=0.0;  		// y-axis coordinates line plotter control variables...                
    float fX_TimeScaleDivider=0.0;    		// x-axis point to point scale factor...    
    unsigned short usCtrX=0;          		// x-axis step interval counter...      
         
    unsigned short nStepCtr;          		// arrow marks step counter... 
    unsigned char ucMarkerFlag;				// marker flag group enable... 
        
    float fDataPointValue;                	// pass by reference data point fetching variable...
    struct _DATAPOINTS _sDataPoint;       	// data point temperature and time interval values storage variable...  
    short sRawDataPoint;                  	// raw data point value in 1 word data format...
    
    
    // EXTRA DATAPOINTS variables that is used in searching extreme high/low values.. 
    float fPickNextDataPoint;
    float fPickMinDataPoint,fPickMaxDataPoint;  
	  unsigned char ucTrackMinLevel=0, ucTrackMaxLevel=0;       
	  unsigned char ucPickUpDataPointFlag;
	  float f_1stDataPoint=0.0,f_2ndDataPoint=0.0;
	  unsigned char ucExtraDataPointsLoopCount;
	  unsigned short usNumOfExtraDataPoints=0;


    unsigned short usDotStepCount;
    unsigned short usDataPointsLoopCount;    
    unsigned short usDataPointNumber;
	  unsigned short usCheckExtraEvenDotFlag;
	  unsigned short usEvenLoopCount;
	  unsigned short usExtraDataPoints;
	  unsigned short usCheckExtraDataPointsDotFlag;
	  unsigned short usEvenTimeIntervalRef;
	  unsigned short usDPNumber;
    unsigned char  ubMarkerStatus = 0;
    unsigned short usMarkerCtr = 0;
        
//==========================================================                             
// Setup PDF line plotter attributes..      
//==========================================================                             

    // select solid line...
	  HPDF_Page_SetDash (page, DASH_MODE1, 0, 0);
	  // select line width, note should be not real to prevent hang-up problem...      
	  HPDF_Page_SetLineWidth (page, 1.0f);
	  // set line color.. 
	  HPDF_Page_SetRGBStroke (page, 0.2f, 0.0f, 1.0f);
	  // set the pdf graphing lines to round end...      
    HPDF_Page_SetLineCap(page,HPDF_ROUND_END);
    // set the pdf graphing line to link each other smoothly by eliminating the broken line...      
  	HPDF_Page_SetLineJoin(page,HPDF_ROUND_JOIN);
    

    // red LED blink when reading EDS cache memory or EEPROM datapoint...
    PDF_Timer2Interrupt_LED_Blinker_Enable();
	
	// this will be true if Datapoints are less than or equal to 460 x-axis dot resolution..	
    if(_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints <= PDF_GRAPH_MAX_WIDTH_DOT_RESOLUTION){

		///////////////////////////////////////////////////////////////////////////
		// 
		//    ** This portion all points were plotted individually!!!
		//
		///////////////////////////////////////////////////////////////////////////
 

		// determine first how many datapoint to be plotted....
		
		// this will be true if only 1 datapoint exist in th EEPROM..		
		if(_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints==ONE_POINT_ONLY)
		{

			///////////////////////////////////////////////////////////////////////////
			// 
			//    ** plot one data point only....
			//
			///////////////////////////////////////////////////////////////////////////

		    // PDF point plotter....       			
			HPDF_Page_SetLineWidth (page, 4.0); 
			HPDF_Page_SetLineCap(page,HPDF_BUTT_END);
			
			// format raw temperature datapoint value to floating point.... 
			// get the first datapoint as X0 starting point...        	        	
        	commTT4Cfg_CacheMem_RetrieveTempData(EEPROM_FIRST_DATAPOINT, &ubMarkerStatus, &_sDataPoint.fTemperatureValue);


			// setup the coordinates for 1 data point square box indicator...
			
	    	// initialize X point of origin! 
			fPlot_X1 = PLOT_X_ORIGIN - 2.0;
			// initialize Y point of origin with temperature min/max resolution!
	        fPlot_Y1 = PLOT_Y_ORIGIN + shift_y_axis + (unsigned short)((_sDataPoint.fTemperatureValue - _sPDFGridLinesScaledValues.fLowerMostTempLevel) * _sPDFGridLinesScaledValues.fTempStepResolution);
	        
	        // setup y-axis coordinates to connect thick line...
	        fPlot_X2 = fPlot_X1 + 4.0; //move to 4 pixels to show a square dot...
	        fPlot_Y2 = fPlot_Y1;
	        
			//---------------------------------------------------------------- 
			//  **  Plot PDF Square Dot only for a single datapoint...
			//----------------------------------------------------------------
       		// here draw a square box as 1 point indicator..
       		PDF_DrawLine(page, fPlot_X1, fPlot_Y1, fPlot_X2, fPlot_Y2);   
			
		}
		else
		{

			///////////////////////////////////////////////////////////////////////////
			// 
			//    ** 1Y-pt/X-dot, Dot Plotter Range: 0 - 460 points....
			//
			//    NOTE: this portion will all Datapoints one by one without any losses
			//          because the total Datapoints that was retrieved from EEPROM 
			//          can be accomodated by X-Axis Maximum Resolution of 460 dots..
			//
			///////////////////////////////////////////////////////////////////////////

			// calculate the step interval factor....
			// initialize time scale divider by setting its scale reference factor.. 						
			fX_TimeScaleDivider = PDF_GRAPH_SKETCH_MAX_WIDTH / (float)(_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints - EEPROM_OFFSET_FIRST_DATAPOINT);

	 		//---------------------------------------------------------------------
			//  ** Setup and initialize the X1,Y1 coordinates as starting point...
			//---------------------------------------------------------------------

			// format raw temperature datapoint value to floating point.... 
			// get the first datapoint as X0 starting point...        	        	
        	commTT4Cfg_CacheMem_RetrieveTempData(EEPROM_FIRST_DATAPOINT, &ubMarkerStatus, &_sDataPoint.fTemperatureValue);

	    	// initialize X point of origin! 
			fPlot_X1 = PLOT_X_ORIGIN;
			// initialize Y point of origin with temperature min/max resolution!
	        fPlot_Y1 = PLOT_Y_ORIGIN + shift_y_axis + (unsigned short)((_sDataPoint.fTemperatureValue - _sPDFGridLinesScaledValues.fLowerMostTempLevel) * _sPDFGridLinesScaledValues.fTempStepResolution);

		    /////////////////////////////////////////////////////
	   		// start PDF line graph sketching...
	   	 	/////////////////////////////////////////////////////

	    	// setup number of loop counts...	    	
	    	usDataPointsLoopCount = _gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints - EEPROM_OFFSET_FIRST_DATAPOINT;    
                        
   	  		// START temperature graph line end markers...   
   	      	HPDF_Page_BeginText (page);    
            PDF_SetText(page,PLOT_X_ORIGIN - 4.0f, fPlot_Y1, "T");         
            HPDF_Page_EndText (page);    

            // initialize starting point...
            PDF_DrawGraph_StartLine(page, fPlot_X1, fPlot_Y1);     
    	 
		    for(usCtrX = START_COUNT_VALUE; usCtrX < usDataPointsLoopCount; usCtrX++){  
    		    
    		    // commented out to to avoid file error..
    		    if(TT4_USB_VBUS_IO == 0){
                    PDF_Timer2Interrupt_LED_Blinker_Disable();        
                    TT4_RED_LED_OFF;
        		    usb_unplugged_handler();
        		}    
	      
				//----------------------------------------------------------------
				//  ** Calculate X distance and get X2,Y2 coordinates...
				//----------------------------------------------------------------

			  	// Prepare to load this single data point to Y2 position...		  				  		
		  		commTT4Cfg_CacheMem_RetrieveTempData(usCtrX+EEPROM_OFFSET_FIRST_DATAPOINT,&ubMarkerStatus,&_sDataPoint.fTemperatureValue);
                
	      		// get x-axis time measurement step interval....		  
	     		_sDataPoint.fMeasurementInterval = fX_TimeScaleDivider * (usCtrX + EEPROM_OFFSET_FIRST_DATAPOINT);

    	 		// set X2,Y2 plotter coordinates first before the next step.... 
    	 		// make it sure that no decimal real numbers that causes calculation error in a minute distance of points..  
    	 		// so, a float decimal values in this porttion will be truncated to avoid hangup problem during boot-up..   
    	 		fPlot_X2 = PLOT_X_ORIGIN + (unsigned short)_sDataPoint.fMeasurementInterval; 
    	  		fPlot_Y2 = PLOT_Y_ORIGIN + shift_y_axis + (unsigned short)((_sDataPoint.fTemperatureValue - _sPDFGridLinesScaledValues.fLowerMostTempLevel) * _sPDFGridLinesScaledValues.fTempStepResolution);
    	  		
				//---------------------------------------------------------------- 
				//  **  Plot PDF line...
				//----------------------------------------------------------------
          		// here sketch the PDF connecting lines of X1,Y1 and X2,Y2...          		
          		PDF_DrawGraph_EndLine(page, fPlot_X2, fPlot_Y2);
          		
	    	}

	    	// load to pdf buffer..	    	
	    	HPDF_Page_Stroke (page);

   	  		// STOP temperature graph-line markers...   	  		
            HPDF_Page_BeginText (page);    
            PDF_SetText(page,fPlot_X2, fPlot_Y2, "T");         
            HPDF_Page_EndText (page);    
            
	    }
	    

		//////////////////////////////////////////////////////////////////
		//
	   	// ** Draw marking arrows to specified data points.....   
	    //
	    //////////////////////////////////////////////////////////////////
	    
	    PDF_SetUpMarker(page);
	    
	    usMarkerCtr = 0;
	        	
        for(usCtrX = START_COUNT_VALUE; usCtrX < _gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints; usCtrX++){	
    	
    	    // Get the 2Bytes format raw data doints...
    		sRawDataPoint = commTT4Cfg_CacheMem_GetDataPointNum(usCtrX);
    			
    		if(CheckMarkPoints(sRawDataPoint)){
        		
        		if(usMarkerCtr < PDF_MAX_MARKER_COUNT)
        		    ++usMarkerCtr;
        		else     
        		    break;     
				
			    //------------------------------------------------------------------------------------------------------
                // NOTE : Datapoints with Checked marks were destroyed after copying to the SPI then,  
                // suppress the 4bit MSB by masking to restore the original temperature data after copying to the SPI.
                //------------------------------------------------------------------------------------------------------

				// this is a raw datapoint fetched from EEPROM without decimal points.. 
				_sDataPoint.fTemperatureValue = ChangeValToTemp(sRawDataPoint);
				// divide by 10 to get the final F temperature value... 
				_sDataPoint.fTemperatureValue /= 10.0;
				    
				// get the time scale interval to locate the x-axis position..	
			    _sDataPoint.fMeasurementInterval = fX_TimeScaleDivider * usCtrX;				    
				    	
		   	 	fPlot_X1 = PLOT_X_ORIGIN + (unsigned short)_sDataPoint.fMeasurementInterval; 
    		  	fPlot_Y1 = PLOT_Y_ORIGIN + shift_y_axis + (unsigned short)((_sDataPoint.fTemperatureValue - _sPDFGridLinesScaledValues.fLowerMostTempLevel) * _sPDFGridLinesScaledValues.fTempStepResolution);

				// show the arrow markers here after its coodinates was determined... 
				PDF_DrawMarker(page, fPlot_X1, fPlot_Y1); 				
		    } 
		}
       	// load to pdf buffer..	    	
	  	HPDF_Page_Stroke (page);
		
	}
	else{
	
		///////////////////////////////////////////////////////////////////////////
		// 
		//    **This portion all Y-axis point compression is being handled here!!!
		//
		///////////////////////////////////////////////////////////////////////////
		
		///////////////////////////////////////////////////////////////////////////
		// 
		//    ** 1Y-pt/X-dot Range Value: 461 - 16k points....
		//
		//    NOTE: 
		//          This portion the 2 Divisions Per Section Algorithm 
		//          will used to plot the lines in the PDF file.         
		//          Please refer to the EXCEL table named:
		//          "TT4USB PDF Graphing Method - 2DPSACT.xls"       
		//          to understand clearly on how the calculation being done with 
		//          the working parameters below:
		//          1.) STEP_INTERVAL is handled by varaible named usDotStepCount.
		//          2.) EVEN_DATAPOINTS is handled by varaible named usEvenLoopCount.
		//          3.) EXTRA_EVEN_DATAPOINTS is equal to 1 STEP_INTERVAL.
		//          4.) EXTRA_DATAPOINTS is handled by variable named usExtraDataPoints. 
		//          5.) EXTREME_DATAPOINT_EXTRACTOR MODULE is handled by 
		//              function named Get_W2Algorithm_MinMaxDataPoint
		//
		//          \mjm
		//           
		///////////////////////////////////////////////////////////////////////////
        
	    // reset static data handled by 460pt...
	    gb460PtsResetStaticDataFlag = TRUE;    	    
	    
	    /////////////////////////////////////////////////////
	    // 
	    // ** Initialize X1,Y1 coordinates...
	    //
	    /////////////////////////////////////////////////////                
        commTT4Cfg_CacheMem_RetrieveTempData(EEPROM_FIRST_DATAPOINT,&ubMarkerStatus,&_sDataPoint.fTemperatureValue);
        
    	// initialize X point of origin! 
		fPlot_X1 = PLOT_X_ORIGIN;
		// initialize Y point of origin with temperature based on the topmost/lowermost values!
        fPlot_Y1 = PLOT_Y_ORIGIN + shift_y_axis + (unsigned short)((_sDataPoint.fTemperatureValue - _sPDFGridLinesScaledValues.fLowerMostTempLevel) * _sPDFGridLinesScaledValues.fTempStepResolution);


	    /////////////////////////////////////////////////////
	    // 
	    //   ** determine the STEP_INTERVAL = usDotStepCount...
	    //
	    /////////////////////////////////////////////////////	    
	    // calculate and setup the datapoint step count interval...	    
	    usDotStepCount = (_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints / PDF_GRAPH_SKETCH_MAX_WIDTH) + 1;
	    

	    //////////////////////////////////////////////////////////////////////////
	    // 
	    //   ** determine the total EVEN_DATAPOINTS = usEvenLoopCount!...
	    //
	    //////////////////////////////////////////////////////////////////////////
	    // calculate and setup the total datapoint loop count as total EVEN Datapoints...   
	    usDataPointsLoopCount = (_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints / usDotStepCount);	    
		
		// check if the loop count is has an extra even step which is not symetrical to the divided sections...  
		usCheckExtraEvenDotFlag = usDataPointsLoopCount % 2; 

		// check if loop counter is EVEN, should be symetrical to accommodate each sections with  2 divisions...  
		// its prerequisite is to calculate first the EXTRA_EVEN_DATAPOINTS if existent...  
		usEvenLoopCount = usDataPointsLoopCount - usCheckExtraEvenDotFlag;

		
		//////////////////////////////////////////////////////////////
		//
		//   ** setup and calculate EXTRA_DATAPOINTS!...
		//
		//////////////////////////////////////////////////////////////		
		// get the remaining datapoints after taking the total EVEN_DATAPOINTS divied into 2 sections... 

		// calculate and check if the extra datapoints if existent...  
		usExtraDataPoints = (_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints % (usEvenLoopCount / NUM_OF_DIVISIONS_PER_SECTION));
		
		// set the Extra datapoint flag...
		if(usExtraDataPoints>0){ 
		    // Extra datapoint has been detected...
			usCheckExtraDataPointsDotFlag = 1;
		}
		else{
			// There were no Extra datapoint detected...
			usCheckExtraDataPointsDotFlag = 0;
		}


		///////////////////////////////////////////////////////////////////
		//
		//   ** setup and calculate x-axis measurement interval scaler...
		//
		///////////////////////////////////////////////////////////////////
		// setup and calculate the timescale x-axis division factor...  		
		if(usCheckExtraDataPointsDotFlag){

			// determine the measurement step interval factor to plot at X-axis line in evenly distributed distance!
			if(usExtraDataPoints==ONE_EXTRA_DATAPOINT){ 			
				usNumOfExtraDataPoints = ONE_EXTRA_DATAPOINT;	 		
			}
			else{
				usNumOfExtraDataPoints = TWO_EXTRA_DATAPOINT;
			}
			
			fX_TimeScaleDivider = PDF_GRAPH_SKETCH_MAX_WIDTH/(float)(usDataPointsLoopCount + usNumOfExtraDataPoints - 1);
			
		}
		else{
			// determine the measurement step interval factor to plot at X-axis line in evenly distributed distance!
			fX_TimeScaleDivider = PDF_GRAPH_SKETCH_MAX_WIDTH/(float)(usDataPointsLoopCount - 1);
		}


  		// START temperature graph line end markers...   
      	HPDF_Page_BeginText (page);    
        PDF_SetText(page,PLOT_X_ORIGIN - 4.0f, fPlot_Y1, "T");         
        HPDF_Page_EndText (page);    


        // initialize starting point...
        PDF_DrawGraph_StartLine(page, fPlot_X1, fPlot_Y1);     

		//---------------------------------------------------------------------- 
		//
		//  ** Here begin to plot the EVEN datapoints...
		//
		//     EVEN Datapoint means  
		//     1 Section = 2 Divisions .. 
		//     1 Division = 1 Extreme datapoint either high or low..
		//     1 EVEN DATA POINTS = 1 Division = 1 Step Interval .. 
		//     Total EVEN DATA POINTS = Step Interval x Even Loop Count
		//
		//----------------------------------------------------------------------

	    // this loop will plot the EVEN datapoint only!...
	    for(usCtrX = 0; usCtrX < usEvenLoopCount; usCtrX++){  
    	    
    	    // commented out to to avoid file error.. 
    	    if(TT4_USB_VBUS_IO == 0){ 
                PDF_Timer2Interrupt_LED_Blinker_Disable();        
                TT4_RED_LED_OFF;     
        	    usb_unplugged_handler();
        	}    
	      
			//----------------------------------------------------------------
			//  ** Calculate X distance and get X2,Y2 coordinates...
			//----------------------------------------------------------------
			
		  	// Prepare to load this single data point to Y2 position... 
		  	// this function is the extreme datapoint extractor...
			Get_W2Algorithm_MinMaxDataPoint(&fDataPointValue, usCtrX, usDotStepCount);
			// uncomment below will use the regular step method#1...
			_sDataPoint.fTemperatureValue = fDataPointValue;
			
	      	// get x-axis time measurement step interval....		  
	     	_sDataPoint.fMeasurementInterval = fX_TimeScaleDivider * usCtrX;

    	 	// set X2,Y2 plotter coordinates first before the next step.... 
    	 	// make it sure that no decimal real numbers that causes calculation error in a minute distance of points..  
    	 	// so, a float decimal values in this porttion will be truncated to avoid hangup problem during boot-up..   
    	 	fPlot_X2 = PLOT_X_ORIGIN + (unsigned short)_sDataPoint.fMeasurementInterval; 
    	  	fPlot_Y2 = PLOT_Y_ORIGIN + shift_y_axis + (unsigned short)((_sDataPoint.fTemperatureValue - _sPDFGridLinesScaledValues.fLowerMostTempLevel) * _sPDFGridLinesScaledValues.fTempStepResolution);

			//----------------------------------------------------------------
			//  **  Plot PDF line...
			//----------------------------------------------------------------
          	// here sketch the PDF connecting lines of X1,Y1 and X2,Y2...
          	PDF_DrawGraph_EndLine(page, fPlot_X2, fPlot_Y2);
          	
	    } /* End of EXTRA_DATAPOINTS loop count*/ 
	    
	    
		//---------------------------------------------------------------------- 
		//
		//  ** Here, begin to plot the EXTRA DATAPOINTS.... 
		//
		//  NOTE: 2 EXTREME high/low datapoints will be chosen..
		//        To tests its extremity level is to make it absolute 
		//        regardless what sign (+ or -) it has then compare which is higher. 
		//----------------------------------------------------------------------
		
		// this will become true if an EXTRA datapoint is present...
		if(usCheckExtraDataPointsDotFlag){

			//---------------------------------------------------------------------- 
			//
			//  ** save the previous EVEN datapoints time interval position....
			//
			//----------------------------------------------------------------------			    
	   		 usEvenTimeIntervalRef = usCtrX;
			
			// take the extreme low or high last point only!!...

			//----------------------------------------------------------------
			//  ** Calculate X distance and get X2,Y2 coordinates...
			//----------------------------------------------------------------
			
			// NOTE: only 1 EXTRA DATAPOINT, since only 1 datapoint only then no extremity will be evaluated and should be plotted directly...
			if(usExtraDataPoints==ONE_EXTRA_DATAPOINT){
			
				// Prepare to load this single data point to Y2 position...        
				usDataPointNumber = (usEvenLoopCount*usDotStepCount);   								
				commTT4Cfg_CacheMem_RetrieveTempData(usDataPointNumber,&ubMarkerStatus,&fDataPointValue);
				
				// load one datapoint only!..
				f_1stDataPoint = fDataPointValue;			

				// setup the number DOTs to be plotted...
				ucExtraDataPointsLoopCount = ONE_EXTRA_DATAPOINT;
				
			}
			
			
			
			// NOTE: only 2 EXTRA DATAPOINT, since only 2 datapoint only then no extremity will be evaluated and should be plotted directly...
			if(usExtraDataPoints==TWO_EXTRA_DATAPOINT){

				// Prepare to load this single data point to Y2 position...        
				usDataPointNumber = (usEvenLoopCount*usDotStepCount);   								
				commTT4Cfg_CacheMem_RetrieveTempData(usDataPointNumber,&ubMarkerStatus,&fDataPointValue);
				
				// load 1st datapoint!..
				f_1stDataPoint = fDataPointValue;			

				// Prepare to load this single data point to Y2 position...        
				usDataPointNumber = (usEvenLoopCount*usDotStepCount) + 1;   								
				commTT4Cfg_CacheMem_RetrieveTempData(usDataPointNumber,&ubMarkerStatus,&fDataPointValue);
				
				// load 2nd datapoint!..
				f_2ndDataPoint = fDataPointValue;			

				// setup the number DOTs to be plotted...
				ucExtraDataPointsLoopCount = TWO_EXTRA_DATAPOINT;
				
			}
			
						
			// NOTE: morethen 2 EXTRA DATAPOINT then must evaluate and pickup its high and low extremes...
			if(usExtraDataPoints > TWO_EXTRA_DATAPOINT){
			
				//set 1st data point pickup value...
				ucPickUpDataPointFlag = 1;
				
				// search the High/Low Extreme Datapoitns within the EXTRA DATAPOINTS...
				for(usCtrX = 0; usCtrX < usExtraDataPoints; usCtrX++){  
				
					//NOTES: RAW DATAPOINTS STORED IN EEPROM ARE in F FORMAT....
				
					// Prepare to load this single data point to Y2 position...        
					usDataPointNumber = (usEvenLoopCount*usDotStepCount) + usCtrX;   					
					commTT4Cfg_CacheMem_RetrieveTempData(usDataPointNumber,&ubMarkerStatus,&fDataPointValue);

					// search the extreme datapoints...	
					// the 1st datapoint must be used as 1st extreme reference...
					if(ucPickUpDataPointFlag){			
						fPickNextDataPoint = fPickMinDataPoint = fPickMaxDataPoint = fDataPointValue;
						ucPickUpDataPointFlag = 0;
					}
					else{
						fPickNextDataPoint = fDataPointValue;
					}
		
					//test if value is minimum...
					if(fPickNextDataPoint < fPickMinDataPoint){
						fPickMinDataPoint = fPickNextDataPoint;
						ucTrackMinLevel = usCtrX;				
					}

					//test if value is maximum...
					if(fPickNextDataPoint > fPickMaxDataPoint){
						fPickMaxDataPoint = fPickNextDataPoint;
						ucTrackMaxLevel = usCtrX;				
					}
				}	
				
				// check level which is the 1st extreme datapoint occured..
				if(ucTrackMinLevel>ucTrackMaxLevel){				
					f_1stDataPoint = fPickMaxDataPoint;
					f_2ndDataPoint = fPickMinDataPoint;
				}
				else{				
					f_1stDataPoint = fPickMinDataPoint;
					f_2ndDataPoint = fPickMaxDataPoint;
				} 
				
				// setup the number DOTs to be plotted...
				ucExtraDataPointsLoopCount = TWO_EXTRA_DATAPOINT;
				
			}
			
			// this should be always 2 extreme since it picks up only 2 DP among the group of EXTRA DATAPOINTS... 
			for(usCtrX = 0; usCtrX < ucExtraDataPointsLoopCount; usCtrX++){
                
				if(ucExtraDataPointsLoopCount==TWO_EXTRA_DATAPOINT){
				
					// here loop twice and plot 2 extra datapoint...
					if(usCtrX==_1ST_EXTREME_EXTRA_DP) _sDataPoint.fTemperatureValue = f_1stDataPoint;
					if(usCtrX==_2ND_EXTREME_EXTRA_DP) _sDataPoint.fTemperatureValue = f_2ndDataPoint;
				}
				else{
					// here loop one's and plot 1 extra datapoint only...
					_sDataPoint.fTemperatureValue = f_1stDataPoint;				
				}

				// plot the end point directly!...
				_sDataPoint.fMeasurementInterval = (fX_TimeScaleDivider * (usEvenTimeIntervalRef + (1+usCtrX))); 

			   	// set X2,Y2 plotter coordinates first before the next step.... 
    			// make it sure that no decimal real numbers that causes calculation error in a minute distance of points..  
    			// so, a float decimal values in this porttion will be truncated to avoid hangup problem during boot-up..   
    			fPlot_X2 = PLOT_X_ORIGIN + (unsigned short)_sDataPoint.fMeasurementInterval; 
    			fPlot_Y2 = PLOT_Y_ORIGIN + shift_y_axis + (unsigned short)((_sDataPoint.fTemperatureValue - _sPDFGridLinesScaledValues.fLowerMostTempLevel) * _sPDFGridLinesScaledValues.fTempStepResolution);

				//----------------------------------------------------------------
				//  **  Plot PDF line...
				//----------------------------------------------------------------
        		// here sketch the PDF connecting lines of X1,Y1 and X2,Y2...
	       		PDF_DrawGraph_EndLine(page, fPlot_X2, fPlot_Y2);	
    		}    		

	    } /* End of EXTRA_DATAPOINTS loop count*/ 
	    
        // write to pdf buffer..
        HPDF_Page_Stroke (page);
        
 		// STOP temperature graph-line markers...   	  		
        HPDF_Page_BeginText (page);    
        PDF_SetText(page,fPlot_X2, fPlot_Y2, "T");         
        HPDF_Page_EndText (page);    


		//---------------------------------------------------------------------- 
		//
		//  ** Here, begin to plot the EVEN DATAPOINTS ARROW MARKERS.... 
		//
		//----------------------------------------------------------------------
    	
    	PDF_SetUpMarker(page);
    	
    	usMarkerCtr = 0;
    	
    	// search the arrow masked bit each datapoint...
    	for(usCtrX = 0; usCtrX < usEvenLoopCount; usCtrX++){
    	
    	    // each step interval presented as 1 datapoint... 
    		for(nStepCtr = 0; nStepCtr < usDotStepCount; nStepCtr++){
    			
    		    // calculate the datapoint EEPROM address...
    			usDPNumber = (usDotStepCount*usCtrX)+nStepCtr;
    				
    			// now get the datapoint from datapoint cache memory...
    		    sRawDataPoint = commTT4Cfg_CacheMem_GetDataPointNum(usDPNumber);
    			
    			// this will be true if the arrow masked bit portion of a certain datapoint is present...  
    		    if(CheckMarkPoints(sRawDataPoint)){

        		    if(usMarkerCtr < PDF_MAX_MARKER_COUNT)
        		        ++usMarkerCtr;
        		    else     
        		        break;     

				    
			    	//------------------------------------------------------------------------------------------------------
            	   	// NOTE : Datapoints with Checked marks were destroyed after copying to the SPI then,  
               		// suppress the 4bit MSB by masking to restore the original temperature data after copying to the SPI.
               		//------------------------------------------------------------------------------------------------------
                    
                    // this is a raw datapoint fetched from EEPROM without decimal points.. 
					_sDataPoint.fTemperatureValue = ChangeValToTemp(sRawDataPoint);
					// divide by 10 to get the final F temperature value... 
					_sDataPoint.fTemperatureValue /= 10.0;
						
					// get the time scale interval to locate the x-axis position..
	    			_sDataPoint.fMeasurementInterval = fX_TimeScaleDivider * usCtrX;			    
	    					
    		 		fPlot_X1 = PLOT_X_ORIGIN + (unsigned short)_sDataPoint.fMeasurementInterval; 
				  	fPlot_Y1 = PLOT_Y_ORIGIN + shift_y_axis + (unsigned short)((_sDataPoint.fTemperatureValue - _sPDFGridLinesScaledValues.fLowerMostTempLevel) * _sPDFGridLinesScaledValues.fTempStepResolution);
				
					// show the arrow markers here after its coodinates was determined...
					PDF_DrawMarker(page, fPlot_X1, fPlot_Y1); 
					
				}					 
			} 
		}
			
			
		//===============================================
		//
		// check EXTRA DATA POINTS arrow markers.... 
		//
		//===============================================
			
		// this will become true if an EXTRA datapoint is present...
		if(usCheckExtraDataPointsDotFlag){
			
		    ucMarkerFlag = 0; // set marker flag to 1 if present...

			// search the arrow masked bit each datapoint...
			for(nStepCtr = 0; nStepCtr < usExtraDataPoints; nStepCtr++){
				
			    // calculate the datapoint EEPROM address...
				usDPNumber = (usEvenLoopCount*usDotStepCount) + nStepCtr;   
					
				// now get the datapoint from datapoint cache memory...
                sRawDataPoint = commTT4Cfg_CacheMem_GetDataPointNum(usDPNumber);
    			
    			// this will be true if the arrow masked bit portion of a certain datapoint is present...  
    		    if(CheckMarkPoints(sRawDataPoint)){

        		    if(usMarkerCtr < PDF_MAX_MARKER_COUNT)
        		        ++usMarkerCtr;
        		    else     
        		        break;     
				    
		    	    //------------------------------------------------------------------------------------------------------
        	   	    // NOTE : Datapoints with Checked marks were destroyed after copying to the SPI then,  
             	    // suppress the 4bit MSB by masking to restore the original temperature data after copying to the SPI.
               	    //------------------------------------------------------------------------------------------------------
                    
                    // this is a raw datapoint fetched from EEPROM without decimal points..
				    _sDataPoint.fTemperatureValue = ChangeValToTemp(sRawDataPoint);
				    // divide by 10 to get the final F temperature value... 
				    _sDataPoint.fTemperatureValue /= 10.0;
							
				    // get the time scale interval to locate the x-axis position..
	    		    _sDataPoint.fMeasurementInterval = fX_TimeScaleDivider * (usCtrX + ucMarkerFlag);  				    
	    					
    		 	    fPlot_X1 = PLOT_X_ORIGIN + (unsigned short)_sDataPoint.fMeasurementInterval; 
				    fPlot_Y1 = PLOT_Y_ORIGIN + shift_y_axis + (unsigned short)((_sDataPoint.fTemperatureValue - _sPDFGridLinesScaledValues.fLowerMostTempLevel) * _sPDFGridLinesScaledValues.fTempStepResolution);
				
				    // get the time scale interval to locate the x-axis position..	
				    PDF_DrawMarker(page, fPlot_X1, fPlot_Y1); 
						
				    // set marker flag location to next last point if a marker still exist wihtin this extra datapoints...	
				    if(ucMarkerFlag==0) ucMarkerFlag=1;			
						
    			} 
    		}
		} 
		
        // write to pdf buffer..
        HPDF_Page_Stroke (page);

	}
	
    PDF_Timer2Interrupt_LED_Blinker_Disable();
        
    TT4_RED_LED_OFF;
}    




/*******************************************************************************
**  Function     : _T2Interrupt
**  Description  : PDF file generation LED blinker
**  PreCondition : must setup first the Timer 2 interrupt handler 
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void __attribute__ ((interrupt, auto_psv)) _T2Interrupt (void)
{
    // this Timer Event flag will toggle every 10msec...
    TT4_RED_LED ^= 1;    
    
    // clear the interrupt flag in exiting this function..
    IFS0bits.T2IF = 0;    	
}    

/*******************************************************************************
**  Function     : PDF_Timer2Interrupt_LED_Blinker_Enable
**  Description  : setup Timer 2 interrupt handler
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void PDF_Timer2Interrupt_LED_Blinker_Enable(void)
{    
    T2CONbits.TON = 1;    
    T2CONbits.TCKPS = 3; // prescaler set to div 256...
    T2CONbits.TGATE = 0;
    T2CONbits.TCS = 0;   // 0-fsys/2 .. 1-source SOSC 32khz     
    IPC1bits.T2IP = TIMER_ASSIGNED_INTERRUPT_PRIORITY;
    TMR2 = 0;
    PR2 = 1500u; // 2048 x 8 x (1/32Mhz) = ~10msecs 
                  
    IFS0bits.T2IF = 0;
    IEC0bits.T2IE = 1;    
}

/*******************************************************************************
**  Function     : PDF_Timer2Interrupt_LED_Blinker_Disable
**  Description  : disables Timer 2 interrupt
**  PreCondition : must initialize first the Timer 2 interrupt handler
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void PDF_Timer2Interrupt_LED_Blinker_Disable(void)
{
    IEC0bits.T2IE = 0;
    IFS0bits.T2IF = 0;

    T2CONbits.TON = 0;
    IPC1bits.T2IP = 0;
}


/*******************************************************************************
**  Function     : PDF_FormatTimeToString
**  Description  : converts 32bit time value to a formatted time string value
**  PreCondition : none
**  Side Effects : none
**  Input        : par_udwStartTimeStamp - 32bit time value in seces
**  Output       : par_strTimeStamps - formatted date and time string value
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void PDF_FormatTimeToString(UINT32 par_udwStartTimeStamp, char *par_strTimeStamps){
    
    _UTIL_DATE_TIME_24HR _lsDateTime;
    char lstrAmPm[3];
    
    drvRTC_ConvSecsToDateTime(par_udwStartTimeStamp,&_lsDateTime);
    
    if((_lsDateTime.Hour >= 0)&&(_lsDateTime.Hour < 12ul)){
        lstrAmPm[0] = 'A';
        lstrAmPm[1] = 'M';
    }
    else{
        lstrAmPm[0] = 'P';
        lstrAmPm[1] = 'M';
        _lsDateTime.Hour -= 12ul;
    }
    lstrAmPm[2] = 0;// terminator...
    
    if(_lsDateTime.Hour==0) _lsDateTime.Hour = 12ul; // time rollover.. mjm
    
    sprintf((char*)&par_strTimeStamps[0],"%2u/%1u/%4u %2u:%02u:%02u %s", (UINT16)_lsDateTime.Mon,
                                                                            (UINT16)_lsDateTime.Day,
                                                                            (UINT16)_lsDateTime.Year,
                                                                            (UINT16)_lsDateTime.Hour,
                                                                            (UINT16)_lsDateTime.Min,
                                                                            (UINT16)_lsDateTime.Sec,
                                                                            lstrAmPm);
}    


/*******************************************************************************
**  Function     : PDF_FormatTimeSecsDHMSToString
**  Description  : converts 32bit time value to a day,hr,min,sec string value
**  PreCondition : none
**  Side Effects : none
**  Input        : par_udwSecs - 32bit time value in secs
**  Output       : par_strDayHrMinSec - formatted day,hr,min,sec string value
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
#define SECS_PER_MIN   60ul
#define SECS_PER_HR    3600ul
#define SECS_PER_DAY   86400ul

void PDF_FormatTimeSecsDHMSToString(UINT32 par_udwSecs, char *par_strDayHrMinSec){    

    // hr,min long int calculator...
    register UINT32 ludwDay;
    register UINT32 ludwDayRem;
    register UINT32 ludwHr;
    register UINT32 ludwHrRem;
    register UINT32 ludwMin;    
    register UINT32 ludwSec;    
    
    // day,hr,min,sec result value..
    register UINT16 luwDay;    
    register UINT16 luwHr;
    register UINT16 luwMin; 
    register UINT16 luwSec;   
    
    // day,hr,min result formatted string value..
    char strTempTime[35];
    
    // calculate 32bit day value..
    ludwDay    = par_udwSecs / SECS_PER_DAY;
    // calculate 32bit hour value..
    ludwDayRem = par_udwSecs % SECS_PER_DAY;
           
    // get 16bit day result..
    luwDay    = (UINT16)ludwDay;    
    // get 16bit hour result..
    ludwHr    = ludwDayRem / SECS_PER_HR;
    luwHr     = (UINT16)ludwHr;
    // get 16bit minute result..
    ludwHrRem = ludwDayRem % SECS_PER_HR;            
    ludwMin   = ludwHrRem / SECS_PER_MIN;    
    luwMin    = (UINT16)ludwMin;
    // get 16bit seconds result..
    ludwSec   = ludwHrRem % SECS_PER_MIN;
    luwSec    = (UINT16)ludwSec;


    //-----------------------------------
    // day , hour , minute , seconds...
    //-----------------------------------    
    if(luwDay>0){
        if(luwMin>0){
            if(luwSec>0){
                if(luwHr>0)
                    sprintf((char*)&strTempTime[0],"%u day %u hr %u min %u sec",luwDay,luwHr,luwMin,luwSec);
                else
                    sprintf((char*)&strTempTime[0],"%u day %u min %u sec",luwDay,luwMin,luwSec);    
            }
            else{
                if(luwHr>0)
                    sprintf((char*)&strTempTime[0],"%u day %u hr %u min",luwDay,luwHr,luwMin);
                else
                    sprintf((char*)&strTempTime[0],"%u day %u min",luwDay,luwMin);                
            }
        }    
        else{
            if(luwSec>0){
                if(luwHr>0)
                    sprintf((char*)&strTempTime[0],"%u day %u hr %u sec",luwDay,luwHr,luwSec);
                else
                    sprintf((char*)&strTempTime[0],"%u day %u sec",luwDay,luwSec); 
            }
            else{
                if(luwHr>0)
                    sprintf((char*)&strTempTime[0],"%u day %u hr",luwDay,luwHr);  
                else
                    sprintf((char*)&strTempTime[0],"%u day",luwDay); 
            }
        }
        
        // send out formatted time ....
        sprintf((char*)&par_strDayHrMinSec[0],"%s",strTempTime);
                
        return;
    }


    //-----------------------------------
    // hour , minute , seconds...
    //-----------------------------------
    if(luwHr>0){
        if(luwMin>0){
            if(luwSec>0)
                sprintf((char*)&strTempTime[0],"%u hr %u min %u sec",luwHr,luwMin,luwSec);
            else
                sprintf((char*)&strTempTime[0],"%u hr %u min",luwHr,luwMin);
        }
        else{
            if(luwSec>0)
                sprintf((char*)&strTempTime[0],"%u hr %u sec",luwHr,luwSec);
            else
                sprintf((char*)&strTempTime[0],"%u hr",luwHr);
        }    
        
        // send out formatted time ....
        sprintf((char*)&par_strDayHrMinSec[0],"%s",strTempTime);
                
        return;
    }    


    //-----------------------------------
    // minute , seconds ...
    //-----------------------------------
    if(luwMin>0){        
        if(luwSec>0)
            sprintf((char*)&strTempTime[0],"%u min %u sec",luwMin,luwSec);            
        else
            sprintf((char*)&strTempTime[0],"%u min",luwMin);            
        
        // send out formatted time ....
        sprintf((char*)&par_strDayHrMinSec[0],"%s",strTempTime);
                
        return;
    }
    
    
    //-----------------------------------
    // seconds only...
    //-----------------------------------
    
    // send out formatted time ....
    sprintf((char*)&par_strDayHrMinSec[0],"%u sec",luwSec);
    
}

/*******************************************************************************
**  Function     : PDF_FormatTimeTripLength
**  Description  : Displays Hour,Min,Sec time format to the PDF file...
**  PreCondition : none
**  Side Effects : none
**  Input        : par_udwSecs - 32bit time value in secs
**  Output       : par_strDayHrMin - 
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void PDF_FormatTimeTripLength(UINT32 par_udwSecs, char *par_strDayHrMin){
    
    // hr,min long int calculator...
    register UINT32 ludwDay;
    register UINT32 ludwDayRem;
    register UINT32 ludwHr;
    register UINT32 ludwHrRem;
    register UINT32 ludwMin;    
    
    // day,hr,min result value..
    register UINT16 luwDay;    
    register UINT16 luwHr;
    register UINT16 luwMin;    
    // day,hr,min result formatted string value..
    char strTempTime[35];
    
    // calculate 32bit day value..
    ludwDay    = par_udwSecs / SECS_PER_DAY;
    // calculate 32bit hour value..
    ludwDayRem = par_udwSecs % SECS_PER_DAY;
    
    // get 16bit day result..
    luwDay    = (UINT16)ludwDay;    
    // get 16bit hour result..
    ludwHr    = ludwDayRem / SECS_PER_HR;
    luwHr     = (UINT16)ludwHr;
    // get 16bit minutes result..
    ludwHrRem = ludwDayRem % SECS_PER_HR;            
    ludwMin   = ludwHrRem / SECS_PER_MIN;    
    luwMin    = (UINT16)ludwMin;


    //-----------------------------------
    // day , hour , minute , seconds...
    //-----------------------------------    
    if(luwDay>0){
        
        if(luwMin>0){            
            
            if(luwHr>0)
                sprintf((char*)&strTempTime[0],"%u day %u hr %u min",luwDay,luwHr,luwMin);
            else
                sprintf((char*)&strTempTime[0],"%u day %u min",luwDay,luwMin);    
        }    
        else{
            if(luwHr>0){        
                sprintf((char*)&strTempTime[0],"%u day %u hr",luwDay,luwHr);
            }    
            else{
                sprintf((char*)&strTempTime[0],"%u day",luwDay);
            }    
        }    
        
        // send out formatted time ....
        sprintf((char*)&par_strDayHrMin[0],"%s",strTempTime);
                
        return;
    }


    //-----------------------------------
    // hour , minute ...
    //-----------------------------------
    if(luwHr>0){        
        
        if(luwMin>0)
            sprintf((char*)&strTempTime[0],"%u hr %u min",luwHr,luwMin);            
        else
            sprintf((char*)&strTempTime[0],"%u hr",luwHr);
        
        // send out formatted time ....
        sprintf((char*)&par_strDayHrMin[0],"%s",strTempTime);
                
        return;
        
    }    
        
    //-----------------------------------
    // minute only...
    //-----------------------------------
    if(luwMin>0)
        sprintf((char*)&strTempTime[0],"%u min",luwMin);            
    else
        sprintf((char*)&strTempTime[0],"0 min");  // display nothing ...
        
    // send out formatted time w/o seconds....
    sprintf((char*)&par_strDayHrMin[0],"%s",strTempTime);
    
}

/*******************************************************************************
**  Function     : PDF_FormatTimeSecsToDHMSString
**  Description  : Dasplays Day,Hour,Min time format to the PDF file...
**  PreCondition : none
**  Side Effects : none
**  Input        : par_ubType - string format 
**                              0 : NNd NNh NNm
**                              1 : NNDay NNHr NNMin
**                 par_udwSecs - accumulated seconds
**  Output       : par_strDayHrMinSec - Day,Hour,Min,Sec string data
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void PDF_FormatTimeSecsToDHMSString(UINT8 par_ubType, UINT32 par_udwSecs, char *par_strDayHrMinSec){

    // hr,min long int calculator...
    register UINT32 ludwDay;
    register UINT32 ludwDayRem;
    register UINT32 ludwHr;
    register UINT32 ludwHrRem;
    register UINT32 ludwMin;    
    
    // day,hr,min result value..
    register UINT16 luwDay;    
    register UINT16 luwHr;
    register UINT16 luwMin;    
    
    // calculate 32bit day value..
    ludwDay    = par_udwSecs / SECS_PER_DAY;
    // calculate 32bit hour value..
    ludwDayRem = par_udwSecs % SECS_PER_DAY;
    
    // get 16bit day result..
    luwDay    = (UINT16)ludwDay;    
    // get 16bit hour result..
    ludwHr    = ludwDayRem / SECS_PER_HR;
    luwHr     = (UINT16)ludwHr;
    // get 16bit minutes result..
    ludwHrRem = ludwDayRem % SECS_PER_HR;            
    ludwMin   = ludwHrRem / SECS_PER_MIN;    
    luwMin    = (UINT16)ludwMin;
    
    
    if(par_ubType==0)    
        sprintf((char*)&par_strDayHrMinSec[0],"%u d %u h %u m",luwDay,luwHr,luwMin);            
    else    
        sprintf((char*)&par_strDayHrMinSec[0],"%u Day %u Hr %u Min",luwDay,luwHr,luwMin);    
        
}

/*******************************************************************************
**  Function     : PDF_FormatTimeZoneToGMTString
**  Description  : Displays the Time Zone in terms of GMT +/-XX:XX
**  PreCondition : none
**  Side Effects : none
**  Input        : hr - integer, timezone shift in hours
**                 min - integer, -45 to +45 represents timezone shift within a hour
**                       in 15 minute increments
**  Output       : parGMTstr - "All Times in GMT +/- XX:XX " string data
** _____________________________________________________________________________
**  Date    Author                Changes
**  031114  Derrick L. Ezenwelu  1st release.
*******************************************************************************/

void PDF_FormatTimeZoneToGMTString(INT8 hr, INT8 min, char* parGMTstr)
{
    if (min == 0)
    {
        if (hr > 0)
        {
            if (hr < 10)
                sprintf((char*)&parGMTstr[0],"All Times in GMT +0%d:00", hr);
            else
                sprintf((char*)&parGMTstr[0],"All Times in GMT +%d:00", hr);
        }
        else
        {
            if (hr > -10)
                sprintf((char*)&parGMTstr[0],"All Times in GMT -0%d:00",(-1) * hr);
            else
                sprintf((char*)&parGMTstr[0],"All Times in GMT %d:00", hr);
        }
    }
    else
    {
        if (hr == 0)
        {
            if (min > 0) //Not a realistic case but I will accomodate it for now
                sprintf((char*)&parGMTstr[0],"All Times in GMT +00:%d", min);
            else
                sprintf((char*)&parGMTstr[0],"All Times in GMT -00:%d", min * (-1));
        }
        else if (hr > 0)
        {
            if (min < 0)
            {
                hr -=1;
                min = min + 60;
            }
            if (hr < 10)
                sprintf((char*)&parGMTstr[0],"All Times in GMT +0%d:%d",hr, min);
            else
                sprintf((char*)&parGMTstr[0],"All Times in GMT +%d:%d",hr, min);
        }
        else
        {
            if (hr > -10)
                sprintf((char*)&parGMTstr[0],"All Times in GMT -0%d:%d",(-1) * hr, min * (-1));
            else
                sprintf((char*)&parGMTstr[0],"All Times in GMT %d:%d",hr, min * (-1));
        }
    }
}

void PDF_Format_ShippingLabel_Info(HPDF_Page par_hpdfPage, char* parShippingLabelstr)
{
    memset((char*)&labeltemp[0], 0x00, sizeof(labeltemp));
    memset((char*)&labelvaluetemp[0], 0x00, sizeof(labelvaluetemp));

    HPDF_Point  pos;
    float pos_max_x = 55;

    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL1_LABEL,(void*)&labeltemp[0]);
    sprintf((char*)&parShippingLabelstr[0],"%s", labeltemp);
    PDF_SetText(par_hpdfPage, 51.8f, 710.8f, (char*)&parShippingLabelstr[0]);
    pos = HPDF_Page_GetCurrentTextPos(par_hpdfPage);
    if (pos_max_x < pos.x)
        pos_max_x = pos.x;
    
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL2_LABEL,(void*)&labeltemp[0]);
    sprintf((char*)&parShippingLabelstr[0],"%s", labeltemp);
    PDF_SetText(par_hpdfPage, 51.8f, 699.8f, (char*)&parShippingLabelstr[0]);
    pos = HPDF_Page_GetCurrentTextPos(par_hpdfPage);
    if (pos_max_x < pos.x)
        pos_max_x = pos.x;

    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL3_LABEL,(void*)&labeltemp[0]);
    sprintf((char*)&parShippingLabelstr[0],"%s", labeltemp);
    PDF_SetText(par_hpdfPage, 51.8f, 688.8f, (char*)&parShippingLabelstr[0]);
    pos = HPDF_Page_GetCurrentTextPos(par_hpdfPage);
    if (pos_max_x < pos.x)
        pos_max_x = pos.x;

    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL4_LABEL,(void*)&labeltemp[0]);
    sprintf((char*)&parShippingLabelstr[0],"%s", labeltemp);
    PDF_SetText(par_hpdfPage, 51.8f, 677.8f, (char*)&parShippingLabelstr[0]);
    pos = HPDF_Page_GetCurrentTextPos(par_hpdfPage);
    if (pos_max_x < pos.x)
        pos_max_x = pos.x;

    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL5_LABEL,(void*)&labeltemp[0]);
    sprintf((char*)&parShippingLabelstr[0],"%s", labeltemp);
    PDF_SetText(par_hpdfPage, 51.8f, 666.8f, (char*)&parShippingLabelstr[0]);
    pos = HPDF_Page_GetCurrentTextPos(par_hpdfPage);
    if (pos_max_x < pos.x)
        pos_max_x = pos.x;

    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL1_VALUE,(void*) &labelvaluetemp[0]);
    sprintf((char*)&parShippingLabelstr[0]," : %s", labelvaluetemp);
    PDF_SetText(par_hpdfPage, pos_max_x, 710.8f, (char*)&parShippingLabelstr[0]);
    
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL2_VALUE,(void*) &labelvaluetemp[0]);
    sprintf((char*)&parShippingLabelstr[0]," : %s", labelvaluetemp);
    PDF_SetText(par_hpdfPage, pos_max_x, 699.8f, (char*)&parShippingLabelstr[0]);
    
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL3_VALUE,(void*) &labelvaluetemp[0]);
    sprintf((char*)&parShippingLabelstr[0]," : %s", labelvaluetemp);
    PDF_SetText(par_hpdfPage, pos_max_x, 688.8f, (char*)&parShippingLabelstr[0]);
    
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL4_VALUE,(void*) &labelvaluetemp[0]);
    sprintf((char*)&parShippingLabelstr[0]," : %s", labelvaluetemp);
    PDF_SetText(par_hpdfPage, pos_max_x, 677.8f, (char*)&parShippingLabelstr[0]);
    
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL5_VALUE,(void*) &labelvaluetemp[0]);
    sprintf((char*)&parShippingLabelstr[0]," : %s", labelvaluetemp);
    PDF_SetText(par_hpdfPage, pos_max_x, 666.8f, (char*)&parShippingLabelstr[0]);
    
    //---------------------------------------------------------------------------
  
    pos_max_x = 313;
    
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL6_LABEL,(void*)&labeltemp[0]);
    sprintf((char*)&parShippingLabelstr[0],"%s", labeltemp);
    PDF_SetText(par_hpdfPage, 309.8f, 710.8f, (char*)&parShippingLabelstr[0]);
    pos = HPDF_Page_GetCurrentTextPos(par_hpdfPage);
    if (pos_max_x < pos.x)
        pos_max_x = pos.x;
    
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL7_LABEL,(void*)&labeltemp[0]);
    sprintf((char*)&parShippingLabelstr[0],"%s", labeltemp);
    PDF_SetText(par_hpdfPage, 309.8f, 699.8f, (char*)&parShippingLabelstr[0]);
    pos = HPDF_Page_GetCurrentTextPos(par_hpdfPage);
    if (pos_max_x < pos.x)
        pos_max_x = pos.x;
    
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL8_LABEL,(void*)&labeltemp[0]);
    sprintf((char*)&parShippingLabelstr[0],"%s", labeltemp);
    PDF_SetText(par_hpdfPage, 309.8f, 688.8f, (char*)&parShippingLabelstr[0]);
    pos = HPDF_Page_GetCurrentTextPos(par_hpdfPage);
    if (pos_max_x < pos.x)
        pos_max_x = pos.x;
    
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL9_LABEL,(void*)&labeltemp[0]);
    sprintf((char*)&parShippingLabelstr[0],"%s", labeltemp);
    PDF_SetText(par_hpdfPage, 309.8f, 677.8f, (char*)&parShippingLabelstr[0]);
    pos = HPDF_Page_GetCurrentTextPos(par_hpdfPage);
    if (pos_max_x < pos.x)
        pos_max_x = pos.x;
    
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL10_LABEL,(void*)&labeltemp[0]);
    sprintf((char*)&parShippingLabelstr[0],"%s", labeltemp);
    PDF_SetText(par_hpdfPage, 309.8f, 666.8f, (char*)&parShippingLabelstr[0]);
    pos = HPDF_Page_GetCurrentTextPos(par_hpdfPage);
    if (pos_max_x < pos.x)
        pos_max_x = pos.x;
    
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL6_VALUE,(void*) &labelvaluetemp[0]);
    sprintf((char*)&parShippingLabelstr[0]," : %s", labelvaluetemp);
    PDF_SetText(par_hpdfPage, pos_max_x, 710.8f, (char*)&parShippingLabelstr[0]);
    
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL7_VALUE,(void*) &labelvaluetemp[0]);
    sprintf((char*)&parShippingLabelstr[0]," : %s", labelvaluetemp);
    PDF_SetText(par_hpdfPage, pos_max_x, 699.8f, &parShippingLabelstr[0]);

    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL8_VALUE,(void*) &labelvaluetemp[0]);
    sprintf((char*)&parShippingLabelstr[0]," : %s", labelvaluetemp);
    PDF_SetText(par_hpdfPage, pos_max_x, 688.8f, &parShippingLabelstr[0]);

    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL9_VALUE,(void*) &labelvaluetemp[0]);
    sprintf((char*)&parShippingLabelstr[0]," : %s", labelvaluetemp);
    PDF_SetText(par_hpdfPage, pos_max_x, 677.8f, &parShippingLabelstr[0]);

   commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL10_VALUE,(void*) &labelvaluetemp[0]);
    sprintf((char*)&parShippingLabelstr[0]," : %s", labelvaluetemp);
    PDF_SetText(par_hpdfPage, pos_max_x, 666.8f, &parShippingLabelstr[0]);

 }


/*******************************************************************************
**  Function     : PDF_Summary_AlarmSettings
**  Description  : alarm settings string format
**  PreCondition : _gsAlarmConfig structure data must not be NULL..
**  Side Effects : none
**  Input        : par_ubAlarmNum - specify the alarm number from 0-5 
**  Output       : par_strValue - string value of alarm settings
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void PDF_Summary_AlarmSettings(UINT8 par_ubAlarmNum, char *par_strValue){
   
    __sBITS_ALARM_CONFIG_BYTE0 _lsBitAlarm_ConfigB0;    
    register char lcValue;
    register float lfR1LowTempVal,lfR1HighTempVal,lfR2LowTempVal,lfR2HighTempVal,lfDdecVal;
    register char lcSymbolDeg = 176; // ?
    unsigned char lucDoubleDecimalFlagEnable = 0;
    
    
    // Note: double decimal digit will be available only in C Deg unit, ST marketing requirement... mjm 08222013..
    if((_gsGenCfgOptByte.GenOpt.Byte1.CDegDoubleDecimal == 1)&&(_gsGenCfgOptByte.GenOpt.Byte1.F_C_Unit_Select==0)) lucDoubleDecimalFlagEnable = 1;
    
     _lsBitAlarm_ConfigB0.ubByte = _gsAlarmConfig[par_ubAlarmNum].ubConfigByte0;
     
    if(_lsBitAlarm_ConfigB0.Bit.Enable==1){ 
    
        switch(_lsBitAlarm_ConfigB0.Bit.Type){
        case(HTL_SE) : // High Temp Limit    - Single Event       .. fallthrough...
        case(HTL_CE) : // High Temp Limit    - Cummulative Event  .. fallthrough...            

            // 0-C or  1-F
            if(_gsGenCfgOptByte.GenOpt.Byte1.F_C_Unit_Select==0){        
                lcValue = 'C';
                lfR1HighTempVal = TEMP_C(_gsAlarmConfig[par_ubAlarmNum].fRange1HighTempSetPoint); 
                
                if(lucDoubleDecimalFlagEnable==1){
                     lfDdecVal = utilDbDec_RoundOFF(lfR1HighTempVal);
                     lfR1HighTempVal = lfDdecVal;
                }     
            }    
            else{
                lcValue = 'F';
                lfR1HighTempVal = _gsAlarmConfig[par_ubAlarmNum].fRange1HighTempSetPoint;
            }    
        
            if(lucDoubleDecimalFlagEnable==1)                
                sprintf((char*)&par_strValue[0],"Over %0.2f %c%c",(double)lfR1HighTempVal,lcSymbolDeg,lcValue); // double decimal...
            else
                sprintf((char*)&par_strValue[0],"Over %0.1f %c%c",(double)lfR1HighTempVal,lcSymbolDeg,lcValue); // single decimal...
            
            
            return; // End of High Temp Limits ...
            
        case(LTL_SE) : // Low Temp Limit     - Single Event       .. fallthrough...
        case(LTL_CE) : // Low Temp Limit     - Cummulative Event  .. fallthrough...

            // 0-C or  1-F
            if(_gsGenCfgOptByte.GenOpt.Byte1.F_C_Unit_Select==0){        
                lcValue = 'C';
                lfR1LowTempVal = TEMP_C(_gsAlarmConfig[par_ubAlarmNum].fRange1LowTempSetPoint); 
                
                if(lucDoubleDecimalFlagEnable==1){
                     lfDdecVal = utilDbDec_RoundOFF(lfR1LowTempVal);
                     lfR1LowTempVal = lfDdecVal;
                }     
                
            }    
            else{
                lcValue = 'F';
                lfR1LowTempVal = _gsAlarmConfig[par_ubAlarmNum].fRange1LowTempSetPoint;
            }    
        
            if(lucDoubleDecimalFlagEnable==1)
                sprintf((char*)&par_strValue[0],"Under %0.2f %c%c",(double)lfR1LowTempVal,lcSymbolDeg,lcValue);  // double decimal...
            else
                sprintf((char*)&par_strValue[0],"Under %0.1f %c%c",(double)lfR1LowTempVal,lcSymbolDeg,lcValue);  // single decimal...  
            
            return; // End of Low Temp Limits...
            
        case(STR_SE) : // Single Temp Range  - Single Event       .. fallthrough...
        case(STR_CE) : // Single Temp Range  - Cummulative Event  .. fallthrough...

            // 0-C or  1-F
            if(_gsGenCfgOptByte.GenOpt.Byte1.F_C_Unit_Select==0){        
                lcValue = 'C';
                lfR1LowTempVal = TEMP_C(_gsAlarmConfig[par_ubAlarmNum].fRange1LowTempSetPoint); 
                lfR1HighTempVal = TEMP_C(_gsAlarmConfig[par_ubAlarmNum].fRange1HighTempSetPoint); 
                
                if(lucDoubleDecimalFlagEnable==1){
                     lfDdecVal = utilDbDec_RoundOFF(lfR1LowTempVal);
                     lfR1LowTempVal = lfDdecVal;
                     
                     lfDdecVal = utilDbDec_RoundOFF(lfR1HighTempVal);
                     lfR1HighTempVal = lfDdecVal;
                }     
            }    
            else{
                lcValue = 'F';
                lfR1LowTempVal = _gsAlarmConfig[par_ubAlarmNum].fRange1LowTempSetPoint; 
                lfR1HighTempVal = _gsAlarmConfig[par_ubAlarmNum].fRange1HighTempSetPoint; 
            }    
        
            if(lucDoubleDecimalFlagEnable==1)
                sprintf((char*)&par_strValue[0],"%0.2f %c%c  to  %0.2f %c%c",(double)lfR1LowTempVal,lcSymbolDeg,lcValue,(double)lfR1HighTempVal,lcSymbolDeg,lcValue);    //  double decimal...
            else
                sprintf((char*)&par_strValue[0],"%0.1f %c%c  to  %0.1f %c%c",(double)lfR1LowTempVal,lcSymbolDeg,lcValue,(double)lfR1HighTempVal,lcSymbolDeg,lcValue);    //  single decimal...
            
            
            return; // End of Single Temp Range Limit...
            
        case(DTR_SE) : // Dual Temp Range    - Single Event       .. fallthrough...
        case(DTR_CE) : // Dual Temp Range    - Cummulative Event  .. fallthrough...

            // 0-C or  1-F
            if(_gsGenCfgOptByte.GenOpt.Byte1.F_C_Unit_Select==0){        
                lcValue = 'C';
                lfR1LowTempVal = TEMP_C(_gsAlarmConfig[par_ubAlarmNum].fRange1LowTempSetPoint); 
                lfR1HighTempVal = TEMP_C(_gsAlarmConfig[par_ubAlarmNum].fRange1HighTempSetPoint); 
                lfR2LowTempVal = TEMP_C(_gsAlarmConfig[par_ubAlarmNum].fRange2LowTempSetPoint); 
                lfR2HighTempVal = TEMP_C(_gsAlarmConfig[par_ubAlarmNum].fRange2HighTempSetPoint); 
                
                if(lucDoubleDecimalFlagEnable==1){
                     lfDdecVal = utilDbDec_RoundOFF(lfR1LowTempVal);
                     lfR1LowTempVal = lfDdecVal;
                     
                     lfDdecVal = utilDbDec_RoundOFF(lfR1HighTempVal);
                     lfR1HighTempVal = lfDdecVal;
                     
                     lfDdecVal = utilDbDec_RoundOFF(lfR2LowTempVal);
                     lfR2LowTempVal = lfDdecVal;
                     
                     lfDdecVal = utilDbDec_RoundOFF(lfR2HighTempVal);
                     lfR2HighTempVal = lfDdecVal;
                }     
                
            }    
            else{
                lcValue = 'F';
                lfR1LowTempVal = _gsAlarmConfig[par_ubAlarmNum].fRange1LowTempSetPoint; 
                lfR1HighTempVal = _gsAlarmConfig[par_ubAlarmNum].fRange1HighTempSetPoint; 
                lfR2LowTempVal = _gsAlarmConfig[par_ubAlarmNum].fRange2LowTempSetPoint; 
                lfR2HighTempVal = _gsAlarmConfig[par_ubAlarmNum].fRange2HighTempSetPoint; 
            }    
        
            if(lfR1HighTempVal<lfR2HighTempVal){
                
                if(lucDoubleDecimalFlagEnable==1){ // double decimal...
                    // Limit 2 is higher than Limit 1...
                    // Then, Alarm Settings should display in PDF from Limit 1 to Limit 2.... (Requested by Peter 072711, as required by Sensitech Marketing)
                    sprintf((char*)&par_strValue[0],"%0.2f %c%c to %0.2f %c%c  or  %0.2f %c%c to %0.2f %c%c",(double)lfR1LowTempVal,
                                                                                                             lcSymbolDeg,
                                                                                                             lcValue,
                                                                                                             (double)lfR1HighTempVal,
                                                                                                             lcSymbolDeg,
                                                                                                             lcValue,
                                                                                                             (double)lfR2LowTempVal,
                                                                                                             lcSymbolDeg,
                                                                                                             lcValue,
                                                                                                             (double)lfR2HighTempVal,
                                                                                                             lcSymbolDeg,
                                                                                                             lcValue);   
                }
                else{ // single decimal...
                    // Limit 2 is higher than Limit 1...
                    // Then, Alarm Settings should display in PDF from Limit 1 to Limit 2.... (Requested by Peter 072711, as required by Sensitech Marketing)
                    sprintf((char*)&par_strValue[0],"%0.1f %c%c to %0.1f %c%c  or  %0.1f %c%c to %0.1f %c%c",(double)lfR1LowTempVal,
                                                                                                             lcSymbolDeg,
                                                                                                             lcValue,
                                                                                                             (double)lfR1HighTempVal,
                                                                                                             lcSymbolDeg,
                                                                                                             lcValue,
                                                                                                             (double)lfR2LowTempVal,
                                                                                                             lcSymbolDeg,
                                                                                                             lcValue,
                                                                                                             (double)lfR2HighTempVal,
                                                                                                             lcSymbolDeg,
                                                                                                             lcValue);
                
                }
            }
            else{
                
                if(lucDoubleDecimalFlagEnable==1){ // double decimal...
                    // Limit 1 is higher than Limit 2...
                    // Then, Alarm Settings should display in PDF from Limit 2 to Limit 1.... (Requested by Peter 072711, as required by Sensitech Marketing)
                    sprintf((char*)&par_strValue[0],"%0.2f %c%c to %0.2f %c%c  or  %0.2f %c%c to %0.2f %c%c",(double)lfR2LowTempVal,
                                                                                                             lcSymbolDeg,
                                                                                                             lcValue,
                                                                                                             (double)lfR2HighTempVal,
                                                                                                             lcSymbolDeg,
                                                                                                             lcValue,
                                                                                                             (double)lfR1LowTempVal,
                                                                                                             lcSymbolDeg,
                                                                                                             lcValue,
                                                                                                             (double)lfR1HighTempVal,
                                                                                                             lcSymbolDeg,
                                                                                                             lcValue);
                }
                else{  // single decimal...
                    // Limit 1 is higher than Limit 2...
                    // Then, Alarm Settings should display in PDF from Limit 2 to Limit 1.... (Requested by Peter 072711, as required by Sensitech Marketing)
                    sprintf((char*)&par_strValue[0],"%0.1f %c%c to %0.1f %c%c  or  %0.1f %c%c to %0.1f %c%c",(double)lfR2LowTempVal,
                                                                                                             lcSymbolDeg,
                                                                                                             lcValue,
                                                                                                             (double)lfR2HighTempVal,
                                                                                                             lcSymbolDeg,
                                                                                                             lcValue,
                                                                                                             (double)lfR1LowTempVal,
                                                                                                             lcSymbolDeg,
                                                                                                             lcValue,
                                                                                                             (double)lfR1HighTempVal,
                                                                                                             lcSymbolDeg,
                                                                                                             lcValue);
                
                }
            }
            
            return; // End of Dual Temp Range Limits ... 
            
        case(TIME_ONLY) : // Time Only Event...            
        
            sprintf((char*)&par_strValue[0],"Time Only");    
            
            return;  // End of Time Only Event...        
        
        case(MKT_HI) : // MKT High Temp Limit    - Single Event

            // 0-C or  1-F
            if(_gsGenCfgOptByte.GenOpt.Byte1.F_C_Unit_Select==0){        
                lcValue = 'C';
                lfR1HighTempVal = TEMP_C(_gsAlarmConfig[par_ubAlarmNum].fRange1HighTempSetPoint); 
                
                if(lucDoubleDecimalFlagEnable==1){
                     lfDdecVal = utilDbDec_RoundOFF(lfR1HighTempVal);
                     lfR1HighTempVal = lfDdecVal;
                }     
            }    
            else{
                lcValue = 'F';
                lfR1HighTempVal = _gsAlarmConfig[par_ubAlarmNum].fRange1HighTempSetPoint;
            }    
        
            if(lucDoubleDecimalFlagEnable==1)                
                sprintf((char*)&par_strValue[0],"MKT > %0.2f %c%c",(double)lfR1HighTempVal,lcSymbolDeg,lcValue); // double decimal...
            else
                sprintf((char*)&par_strValue[0],"MKT > %0.1f %c%c",(double)lfR1HighTempVal,lcSymbolDeg,lcValue); // single decimal...            
            
            return; // End of MKT High Temp Limit    - Single Event
                        
        case(MKT_LO) : // MKT Low Temp Limit     - Single Event

            // 0-C or  1-F
            if(_gsGenCfgOptByte.GenOpt.Byte1.F_C_Unit_Select==0){        
                lcValue = 'C';
                lfR1LowTempVal = TEMP_C(_gsAlarmConfig[par_ubAlarmNum].fRange1LowTempSetPoint); 
                
                if(lucDoubleDecimalFlagEnable==1){
                     lfDdecVal = utilDbDec_RoundOFF(lfR1LowTempVal);
                     lfR1LowTempVal = lfDdecVal;
                }     
                
            }    
            else{
                lcValue = 'F';
                lfR1LowTempVal = _gsAlarmConfig[par_ubAlarmNum].fRange1LowTempSetPoint;
            }    
        
            if(lucDoubleDecimalFlagEnable==1)
                sprintf((char*)&par_strValue[0],"MKT < %0.2f %c%c",(double)lfR1LowTempVal,lcSymbolDeg,lcValue);  // double decimal...
            else
                sprintf((char*)&par_strValue[0],"MKT < %0.1f %c%c",(double)lfR1LowTempVal,lcSymbolDeg,lcValue);  // single decimal...  
            
            return; // End of MKT Low Temp Limit     - Single Event
    
            
            
        } // End of Switch statement..    
    
    }   
    else{
        // Alarm disabled...
        sprintf((char*)&par_strValue[0],"--");    
    } 
    
}

/*******************************************************************************
**  Function     : PDF_Summary_IdealSettings
**  Description  : formats string of ideal temp range value.
**  PreCondition : ideal temp range value must not NULL
**  Side Effects : none
**  Input        : par_strValue - string value 
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void PDF_Summary_IdealSettings(char *par_strValue){
    
    unsigned char lucDoubleDecimalFlagEnable = 0;
    register char lcValue;
    register float lfTempLowVal, lfTempHighVal;
    register char lcSymbolDeg = 176; // ?
    float lfDdecVal;

    if(_gsExtdOptByte.ExtOpt.Byte1.IdealTempRgViewEna==1){

        // 0-C or  1-F
        if(_gsGenCfgOptByte.GenOpt.Byte1.F_C_Unit_Select==0){        
            lcValue = 'C';
            lfTempLowVal = TEMP_C(_gsUserCfgAreaData.fTempIdealRangeLowValue); 
            lfTempHighVal = TEMP_C(_gsUserCfgAreaData.fTempIdealRangeHighValue); 
            
            // Note: double decimal digit will be available only in C Deg unit, ST marketing requirement... mjm 08222013..
            if(_gsGenCfgOptByte.GenOpt.Byte1.CDegDoubleDecimal == 1){
                 lucDoubleDecimalFlagEnable = 1;
                 
                 lfDdecVal = utilDbDec_RoundOFF(lfTempLowVal);
                 lfTempLowVal = lfDdecVal;
                 
                 lfDdecVal = utilDbDec_RoundOFF(lfTempHighVal);
                 lfTempHighVal = lfDdecVal;
            }     
            
        }    
        else{
            lcValue = 'F';
            lfTempLowVal = _gsUserCfgAreaData.fTempIdealRangeLowValue;
            lfTempHighVal = _gsUserCfgAreaData.fTempIdealRangeHighValue;
        }    
        
        if(lucDoubleDecimalFlagEnable==1)
            sprintf((char*)&par_strValue[0],">=%0.2f %c%c =<%0.2f %c%c",(double)lfTempLowVal,lcSymbolDeg,lcValue,(double)lfTempHighVal,lcSymbolDeg,lcValue); // double decimal...
        else
            sprintf((char*)&par_strValue[0],">=%0.1f %c%c =<%0.1f %c%c",(double)lfTempLowVal,lcSymbolDeg,lcValue,(double)lfTempHighVal,lcSymbolDeg,lcValue); // single decimal...   
        
    }
    else{
        //sprintf((char*)&par_strValue[0],"--");
        sprintf((char*)&par_strValue[0],"Disabled");
    }
}

/*******************************************************************************
**  Function     : PDF_Summary_Threshold
**  Description  : String format of Summay data..
**  PreCondition : _gsAlarmConfig structure data must not be NULL..
**  Side Effects : none
**  Input        : par_ubAlarmNum - alarm number from 0-5
**  Output       : par_strValue
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void PDF_Summary_Threshold(UINT8 par_ubAlarmNum, char *par_strValue){

    char lstrValue[30];
    __sBITS_ALARM_CONFIG_BYTE0 _lsBitAlarm_ConfigB0;    
    
     _lsBitAlarm_ConfigB0.ubByte = _gsAlarmConfig[par_ubAlarmNum].ubConfigByte0;
     
    if(_lsBitAlarm_ConfigB0.Bit.Enable==1){ 
    
        switch(_lsBitAlarm_ConfigB0.Bit.Type){
        case(HTL_SE) : // High Temp Limit    - Single Event       .. fallthrough...
        case(LTL_SE) : // Low Temp Limit     - Single Event       .. fallthrough...
        case(STR_SE) : // Single Temp Range  - Single Event       .. fallthrough...
        case(DTR_SE) : // Dual Temp Range    - Single Event       .. fallthrough...
            PDF_FormatTimeSecsToDHMSString(0,_gsAlarmConfig[par_ubAlarmNum].udwThreshhold * _gsUser_Configuration_Area.udwMeasurementInterval, (char*)&lstrValue[0]);
            sprintf((char*)&par_strValue[0],"%s (S/E)",lstrValue);    
            return;
        case(HTL_CE) : // High Temp Limit    - Cummulative Event  .. fallthrough...            
        case(LTL_CE) : // Low Temp Limit     - Cummulative Event  .. fallthrough...
        case(STR_CE) : // Single Temp Range  - Cummulative Event  .. fallthrough...
        case(DTR_CE) : // Dual Temp Range    - Cummulative Event  .. fallthrough...
            PDF_FormatTimeSecsToDHMSString(0,_gsAlarmConfig[par_ubAlarmNum].udwThreshhold * _gsUser_Configuration_Area.udwMeasurementInterval, (char*)&lstrValue[0]);
            sprintf((char*)&par_strValue[0],"%s (Cum.)",lstrValue);    
            return;
        case(TIME_ONLY) : // Time Only Event...
            PDF_FormatTimeSecsToDHMSString(0,_gsAlarmConfig[par_ubAlarmNum].udwThreshhold * _gsUser_Configuration_Area.udwMeasurementInterval, (char*)&lstrValue[0]);
            sprintf((char*)&par_strValue[0],"%s (Time)",lstrValue);    
            return;
        case(MKT_HI) : // MKT High Temp Limit    - Single Event       .. fallthrough...
        case(MKT_LO) : // MKT Low Temp Limit     - Single Event       .. fallthrough...
        	   sprintf((char*)&par_strValue[0],"N/A");    
            return;    
        }    
    
    }   
    else{
        // Alarm disabled...
        sprintf((char*)&par_strValue[0],"--");    
    } 

}


/*******************************************************************************
**  Function     : PDF_Summary_TotalTime
**  Description  : String format of total time 
**  PreCondition : none
**  Side Effects : none
**  Input        : par_ubAlarmNum - set alarm numbe rfrom 0 - 5
**  Output       : par_strValue - string output time format
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void PDF_Summary_TotalTime(UINT8 par_ubAlarmNum, char *par_strValue){
    
    char lstrValue[30];
    __sBITS_ALARM_CONFIG_BYTE0 _lsBitAlarm_ConfigB0;    
    register UINT32 ludwTimeTriggred;    
    
    if(par_ubAlarmNum == IDEAL_TEMP){        
        
        //----------------------------------------------
        // Total time for Ideal Temp Range only....
        //----------------------------------------------
        if(_gsExtdOptByte.ExtOpt.Byte1.IdealTempRgViewEna==1){
            ludwTimeTriggred = _guwTrigEvents[IDEAL_TEMP].udwTotalTime * _gsUser_Configuration_Area.udwMeasurementInterval;
        
            PDF_FormatTimeSecsToDHMSString(0 , ludwTimeTriggred, (char*)&lstrValue[0]); 
            sprintf((char*)&par_strValue[0],"%s",lstrValue);
        }
        else{
            sprintf((char*)&par_strValue[0],"--");
        }
    }                
    else{

        //----------------------------------------------
        // Total time for 1-6 Alarms only....
        //----------------------------------------------
        
        // check which Alarm is enabled....
        _lsBitAlarm_ConfigB0.ubByte = _gsAlarmConfig[par_ubAlarmNum].ubConfigByte0;
         
        // if Alarm is enabled then stamp total time based on their number of event occured.... mjm
        if(_lsBitAlarm_ConfigB0.Bit.Enable==1){


            if(_lsBitAlarm_ConfigB0.Bit.Type == TIME_ONLY){
                // calculate total time based on theie number of events occured!.. mjm        
                ludwTimeTriggred = (_guwTrigEvents[par_ubAlarmNum].udwTotalTime-1) * _gsUser_Configuration_Area.udwMeasurementInterval;
            }
            else{
                // calculate total time based on theie number of events occured!.. mjm        
                ludwTimeTriggred = _guwTrigEvents[par_ubAlarmNum].udwTotalTime * _gsUser_Configuration_Area.udwMeasurementInterval;            
            }
                
            PDF_FormatTimeSecsToDHMSString(0 , ludwTimeTriggred, (char*)&lstrValue[0]); 
            sprintf((char*)&par_strValue[0],"%s",lstrValue);
            
            if ((_lsBitAlarm_ConfigB0.Bit.Type == MKT_HI) || (_lsBitAlarm_ConfigB0.Bit.Type == MKT_LO))
                sprintf((char*)&par_strValue[0],"N/A");

        }
        else{
            sprintf((char*)&par_strValue[0],"--");    
        }        
    }
}    


/*******************************************************************************
**  Function     : PDF_Summary_NumberOfEvents
**  Description  : Format string of summary of numner of events
**  PreCondition : _gsAlarmConfig and _guwTrigEvents structure data must not be NULL..
**  Side Effects : none
**  Input        : par_ubAlarmNum - set alarm number from 0 - 5
**  Output       : par_strValue - output string value
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void PDF_Summary_NumberOfEvents(UINT8 par_ubAlarmNum, char *par_strValue){

    __sBITS_ALARM_CONFIG_BYTE0 _lsBitAlarm_ConfigB0;    
    
    if(par_ubAlarmNum == IDEAL_TEMP){       
        if(_gsExtdOptByte.ExtOpt.Byte1.IdealTempRgViewEna==1){ 
            sprintf((char*)&par_strValue[0],"%u",_guwTrigEvents[IDEAL_TEMP].uwNumOfEvents);
        }
        else{
            sprintf((char*)&par_strValue[0],"--");
        }    
    }
    else{    
        _lsBitAlarm_ConfigB0.ubByte = _gsAlarmConfig[par_ubAlarmNum].ubConfigByte0;    
     
        if(_lsBitAlarm_ConfigB0.Bit.Enable==1){         
        
            if(_lsBitAlarm_ConfigB0.Bit.Type!=TIME_ONLY){
	            if((_lsBitAlarm_ConfigB0.Bit.Type==MKT_HI)||(_lsBitAlarm_ConfigB0.Bit.Type==MKT_LO)){
		            sprintf((char*)&par_strValue[0],"N/A"); // valid only for MKT  Alarms, specified by Peter Nunes.. 10312013 mjm
	            }	            
	            else{
                	sprintf((char*)&par_strValue[0],"%u",_guwTrigEvents[par_ubAlarmNum].uwNumOfEvents);
             	}   	
            }    
            else{
                sprintf((char*)&par_strValue[0],"--");        
        	}        
        }    
        else{    
            sprintf((char*)&par_strValue[0],"--");    
        }    
    }
            
}    


/*******************************************************************************
**  Function     : PDF_Summary_Triggered
**  Description  : formats string data of alarm triggered status
**  PreCondition : _gsAlarmConfig structure data must not be NULL..
**  Side Effects : none
**  Input        : par_ubAlarmNum - set alarm number from 0 - 5
**  Output       : par_strValue - output string value
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void PDF_Summary_Triggered(UINT8 par_ubAlarmNum, char *par_strValue){
    
    __sBITS_ALARM_CONFIG_BYTE0 _lsBitAlarm_ConfigB0;    
    register UINT32 ludwTimeTriggred;
    char lstrValue[30];

    _lsBitAlarm_ConfigB0.ubByte = _gsAlarmConfig[par_ubAlarmNum].ubConfigByte0;
     
    if(_lsBitAlarm_ConfigB0.Bit.Enable==1){         
        
        if(_gsUser_Configuration_Area.ubAlarmStatus & (0x1<<par_ubAlarmNum)){
    
            switch(par_ubAlarmNum){
            case(ALARM_1) :
                ludwTimeTriggred = _gsUser_Configuration_Area.udwAlarm1_TriggeredTimeStamp + TimeZoneDSTShift;
                break;
            case(ALARM_2) :
                ludwTimeTriggred = _gsUser_Configuration_Area.udwAlarm2_TriggeredTimeStamp + TimeZoneDSTShift;
                break;
            case(ALARM_3) :
                ludwTimeTriggred = _gsUser_Configuration_Area.udwAlarm3_TriggeredTimeStamp + TimeZoneDSTShift;
                break;
            case(ALARM_4) :
                ludwTimeTriggred = _gsUser_Configuration_Area.udwAlarm4_TriggeredTimeStamp + TimeZoneDSTShift;
                break;
            case(ALARM_5) :
                ludwTimeTriggred = _gsUser_Configuration_Area.udwAlarm5_TriggeredTimeStamp + TimeZoneDSTShift;
                break;
            case(ALARM_6) :
                ludwTimeTriggred = _gsUser_Configuration_Area.udwAlarm6_TriggeredTimeStamp + TimeZoneDSTShift;
                break;
            }            
            
            PDF_FormatTimeToString(ludwTimeTriggred, (char*)&lstrValue[0]); 
            sprintf((char*)&par_strValue[0],"%s",lstrValue);    
        } 
        else{
            sprintf((char*)&par_strValue[0],"--");    
        }           
    }
    else{
        sprintf((char*)&par_strValue[0],"--");    
    }    
            
}    

/*******************************************************************************
**  Function     : PDF_Summary_Status
**  Description  : formats string of summary status
**  PreCondition : _gsAlarmConfig structure data must not be NULL..
**  Side Effects : none
**  Input        : par_ubAlarmNum - set alarm number from 0 - 5
**  Output       : par_strValue - output string value
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void PDF_Summary_Status(UINT8 par_ubAlarmNum, char *par_strValue){
    
    __sBITS_ALARM_CONFIG_BYTE0 _lsBitAlarm_ConfigB0;    
    
    _lsBitAlarm_ConfigB0.ubByte = _gsAlarmConfig[par_ubAlarmNum].ubConfigByte0;
     
    if(_lsBitAlarm_ConfigB0.Bit.Enable==1){         
        
        if(_gsUser_Configuration_Area.ubAlarmStatus & (0x1<<par_ubAlarmNum))
            sprintf((char*)&par_strValue[0],"Alarm");                        
        else
            sprintf((char*)&par_strValue[0],"OK");            
    }
    else{
        sprintf((char*)&par_strValue[0],"--");    
    }    
            
}    


/*******************************************************************************
**  Function     : PDF_Summary_TemperatureTime
**  Description  : format string of summary Extreme temperature & time...
**  PreCondition : none
**  Side Effects : none
**  Input        : par_fTemp - temperature data format
                               0 : C Degrees , 1 : F Degrees
                   par_udwTimeInSec - accumulated time in seconds
**  Output       : par_strValue - string time value
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void PDF_Summary_TemperatureTime(float par_fTemp, UINT32 par_udwTimeInSec, char *par_strValue){

    char lstrValue[25];
    unsigned char lucDoubleDecimalFlagEnable = 0;
    register char lcValue;
    register float lfTempVal,lfDdecVal;
    register char lcSymbolDeg = 176; // ?
    
    
    PDF_FormatTimeToString(par_udwTimeInSec, (char*)&lstrValue[0]); 
    
    // 0-C or  1-F
    if(_gsGenCfgOptByte.GenOpt.Byte1.F_C_Unit_Select==0){        
        lcValue = 'C';
        lfTempVal = TEMP_C(par_fTemp); 
        
        // Note: double decimal digit will be available only in C Deg unit, ST marketing requirement... mjm 08222013..
        if(_gsGenCfgOptByte.GenOpt.Byte1.CDegDoubleDecimal == 1){ 
            lfDdecVal = utilDbDec_RoundOFF(lfTempVal);
            lfTempVal = lfDdecVal;
            lucDoubleDecimalFlagEnable = 1;
        }    
    }    
    else{
        lcValue = 'F';
        lfTempVal = par_fTemp; 
    }
    
    if(lucDoubleDecimalFlagEnable)
        sprintf((char*)&par_strValue[0],"%0.2f %c%c @ %s",(double)lfTempVal,lcSymbolDeg,lcValue,lstrValue);  //double decimal digit...
    else
        sprintf((char*)&par_strValue[0],"%0.1f %c%c @ %s",(double)lfTempVal,lcSymbolDeg,lcValue,lstrValue);  //single decimal digit
}



/*******************************************************************************
**  Function     : PDF_Summary_Calculate_StdDevAndMKT
**  Description  : calculate the Mean Kinetic Temperature value..
**  PreCondition : reads data of _gsLogMinMaxTempData and _gsSensitech_Configuration_Area
**                 to calculate the Mean Kinetic Temperature effect value...
**  Side Effects : updates the gfMKT global variable 
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
/*
#define ACT_ENERGY_CONST (double)-9982.64
#define FAR_KELVIN_CONST 459.67    // Kelvin to Fahrenheit constant...
#define CEN_KELVIN_CONST 273.15    // Kelvin to Centigrade constant...

long double gfMKT;

void PDF_Summary_Calculate_StdDevAndMKT(void){    
	
    long double lfTempXM;
    long double lfTempXMTotal;
    long double lfKelvinTemp;
    long double lfExp;
    long double lfTempVal;
    long double lfDd_CAveTemp;
    
    register UINT16 luwCtr;
    UINT8 lubMark;
    float lfTempData;

    lfTempXMTotal = 0.0;
    lfTempXM = 0.0;
    lfExp = 0.0;    
    
    // turn on RED LED, in large datapoint e.g 16k then it looks the device is 
    // hanged-up because all LED's were turned off but a long calculation is 
    // running in somehow it takes about aleast a 2secs...
    TT4_RED_LED_ON;
    
    // Note: double decimal digit will be available only in C Deg unit, ST marketing requirement... mjm 09252013..
    lfDd_CAveTemp = TEMP_C(_gsLogMinMaxTempData.fAveTemp_F_Deg);

    for(luwCtr=0;luwCtr<_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints;luwCtr++){

        // retrieve F temperature data from EEPROM...        
        commTT4Cfg_CacheMem_RetrieveTempData(luwCtr,&lubMark,&lfTempData);

        //--------------------------------------------- 
        // calculate Temperature standard deviation...
        //--------------------------------------------- 
        
        lfTempVal = (long double)lfTempData;
        
        // Temperature Unit conversion 0-C or 1-F
        if(_gsGenCfgOptByte.GenOpt.Byte1.F_C_Unit_Select==0){
	        
            // X - M  C temperature data...
            
            // Note: double decimal digit will be available only in C Deg unit, ST marketing requirement... mjm 09252013..
            if(_gsGenCfgOptByte.GenOpt.Byte1.CDegDoubleDecimal == 1){
                // Double decimal C Deg Temp..
                lfTempXM = TEMP_C(lfTempVal) - lfDd_CAveTemp;
            }
            else{
                // Single decimal C Deg Temp..
                lfTempXM = TEMP_C(lfTempVal) - _gsLogMinMaxTempData.fAveTemp_C_Deg;
            }           
            
        }
        else{
            // X - M  F temperature data...
            lfTempXM = lfTempVal - _gsLogMinMaxTempData.fAveTemp_F_Deg;
        }    
        
        // take the square to eliminate negative sign...
        lfTempXM *= lfTempXM;
        
        // summation of sqaures..
        lfTempXMTotal += lfTempXM;    
        
        
        //---------------------------------------- 
        // calculate Mean Kinetic Temperature...
        //---------------------------------------- 
        
        lfKelvinTemp = (5.0/9.0) * (lfTempData + FAR_KELVIN_CONST);        
                
        // accumulate the natural logarithmic sum...
        lfExp += exp(ACT_ENERGY_CONST / lfKelvinTemp);
        
    }
    
    // calculate temperature's standard deviation value...    
    if(_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints>1)
        gfTemperatureStdDev = sqrt(lfTempXMTotal/(_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints-1));            
    else
        gfTemperatureStdDev = 0.0;
    
    gfMKT = ACT_ENERGY_CONST / log(lfExp/_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints);
    
    // Temperature Unit conversion 0-C or 1-F
    if(_gsGenCfgOptByte.GenOpt.Byte1.F_C_Unit_Select==0){        
        // Mean Kinetic Temperature to Centigrade Unit...
        gfMKT -= CEN_KELVIN_CONST;        
    }    
    else{
        // Mean Kinetic Temperature to Centigrade Unit...
        gfMKT = 1.8 * gfMKT - FAR_KELVIN_CONST;        
    }
    
}
*/
    
/*******************************************************************************
**  Function     : PDF_Summary_StdDeviation
**  Description  : calculate average temperature value formats into string
**  PreCondition : must have the _gsLogMinMaxTempData with ave data..
**  Side Effects : none
**  Input        : none
**  Output       : par_strValue - string value of average temperature value
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/    
void PDF_Summary_StdDeviation(char *par_strValue){

    unsigned char lucDoubleDecimalFlagEnable = 0;
    register char lcValue;
    register float lfTempVal,lfDdecVal;    
    register char lcSymbolDeg = 176; // ?
    register char lcSymbolTol = 177; // ?
    
    register long double lfDd_CAveTemp;
    register long double lfTempXM;
    register long double lfTempXMTotal;
    register UINT16 luwCtr;

    UINT8 lubMark;
    float lfTempData;

    
    //cd[0] = 176; // ?
    //cd[1] = 177; // ?
    //cd[2] = 174; // R
    
    
    lfTempXMTotal = 0.0;
    lfTempXM = 0.0;
    
    
    // turn on RED LED, in large datapoint e.g 16k then it looks the device is 
    // hanged-up because all LED's were turned off but a long calculation is 
    // running in somehow it takes about aleast a 2secs...
    TT4_RED_LED_ON;
    
    // Note: double decimal digit will be available only in C Deg unit, ST marketing requirement... mjm 09252013..
    lfDd_CAveTemp = TEMP_C(_gsLogMinMaxTempData.fAveTemp_F_Deg);

    for(luwCtr=0;luwCtr<_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints;luwCtr++){

        // retrieve F temperature data from EEPROM...        
        commTT4Cfg_CacheMem_RetrieveTempData(luwCtr,&lubMark,&lfTempData);

        //--------------------------------------------- 
        // calculate Temperature standard deviation...
        //--------------------------------------------- 
        
        lfTempVal = (long double)lfTempData;
        
        // Temperature Unit conversion 0-C or 1-F
        if(_gsGenCfgOptByte.GenOpt.Byte1.F_C_Unit_Select==0){
	        
            // X - M  C temperature data...
            
            // Note: double decimal digit will be available only in C Deg unit, ST marketing requirement... mjm 09252013..
            if(_gsGenCfgOptByte.GenOpt.Byte1.CDegDoubleDecimal == 1){
                // Double decimal C Deg Temp..
                lfTempXM = TEMP_C(lfTempVal) - lfDd_CAveTemp;
            }
            else{
                // Single decimal C Deg Temp..
                lfTempXM = TEMP_C(lfTempVal) - _gsLogMinMaxTempData.fAveTemp_C_Deg;
            }           
            
        }
        else{
            // X - M  F temperature data...
            lfTempXM = lfTempVal - _gsLogMinMaxTempData.fAveTemp_F_Deg;
        }    
        
        // take the square to eliminate negative sign...
        lfTempXM *= lfTempXM;
        
        // summation of sqaures..
        lfTempXMTotal += lfTempXM;    
        
    }
    
    // calculate temperature's standard deviation value...    
    if(_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints>1)
        gfTemperatureStdDev = sqrt(lfTempXMTotal/(_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints-1));            
    else
        gfTemperatureStdDev = 0.0;    
    
    
    
    
    // 0-C or  1-F  unit conversion..
    if(_gsGenCfgOptByte.GenOpt.Byte1.F_C_Unit_Select==0){        
        lcValue = 'C';                
        lfTempVal = _gsLogMinMaxTempData.fAveTemp_C_Deg;
        
        // Note: double decimal digit will be available only in C Deg unit, ST marketing requirement... mjm 08222013..
        if(_gsGenCfgOptByte.GenOpt.Byte1.CDegDoubleDecimal == 1){
             lucDoubleDecimalFlagEnable = 1;
             
             lfTempVal = TEMP_C(_gsLogMinMaxTempData.fAveTemp_F_Deg);
             
             lfDdecVal = utilDbDec_RoundOFF(lfTempVal);
             lfTempVal = lfDdecVal;
             
             lfDdecVal = utilDbDec_RoundOFF(gfTemperatureStdDev);
             gfTemperatureStdDev = lfDdecVal;
        }     
    }    
    else{
        lcValue = 'F';        
        lfTempVal = _gsLogMinMaxTempData.fAveTemp_F_Deg;
    }    
    
    if(_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints==1) gfTemperatureStdDev = 0.0;

    if(lucDoubleDecimalFlagEnable==1)
        sprintf((char*)&par_strValue[0],"%0.2f %c%c %c %0.2f %c%c",(double)lfTempVal,lcSymbolDeg,lcValue,lcSymbolTol,(double)gfTemperatureStdDev,lcSymbolDeg,lcValue); // double decimal...
    else
        sprintf((char*)&par_strValue[0],"%0.1f %c%c %c %0.1f %c%c",(double)lfTempVal,lcSymbolDeg,lcValue,lcSymbolTol,(double)gfTemperatureStdDev,lcSymbolDeg,lcValue); // single decimal...
    
}


/*******************************************************************************
**  Function     : PDF_Summary_MeanKinetic
**  Description  : format into string value of Mean Kinetic Temperature value
**  PreCondition : must call first the PDF_Summary_Calculate_StdDevAndMKT routine.
**  Side Effects : none
**  Input        : none
**  Output       : par_strValue - string format of Mean Kinetic Temperature value
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void PDF_Summary_MeanKinetic(char *par_strValue){

    unsigned char lucDoubleDecimalFlagEnable = 0;
    register char lcValue;
    register char lcSymbolDeg = 176; // ?   
    float lfDdecVal;
    
    // 0-C or  1-F  unit conversion..
    if(_gsGenCfgOptByte.GenOpt.Byte1.F_C_Unit_Select==0){        
        lcValue = 'C';                
        
        // Note: double decimal digit will be available only in C Deg unit, ST marketing requirement... mjm 08222013..
        if(_gsGenCfgOptByte.GenOpt.Byte1.CDegDoubleDecimal == 1){
             lucDoubleDecimalFlagEnable = 1;
             
             //lfDdecVal = utilDbDec_RoundOFF(gfMKT);
             //gfMKT = lfDdecVal;
             lfDdecVal = utilDbDec_RoundOFF(gudwMKT_Value);             
             gudwMKT_Value = lfDdecVal;
        }     
    }    
    else
        lcValue = 'F';        
    
    
    if(lucDoubleDecimalFlagEnable == 1)    
        sprintf((char*)&par_strValue[0],"%0.2f %c%c",(double)gudwMKT_Value,lcSymbolDeg,lcValue); // double decimal...
    else
        sprintf((char*)&par_strValue[0],"%0.1f %c%c",(double)gudwMKT_Value,lcSymbolDeg,lcValue); // single decimal...
    
}

/*******************************************************************************
**  Function     : PDF_OriginatorNotes
**  Description  : reads EEPROM originatior notes and format into text data..
**  PreCondition : EEPROM must have a readable ASCII text data ..
**  Side Effects : none
**  Input        : none
**  Output       : par_strValue - string data  
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void PDF_OriginatorNotes(char *par_strValue){
    
    register UINT16 luwCtr;    
    register UINT16 luwPageNumber;
    UINT8 lubData;
    
    //-----------------------------------------------------------
    // retrieve User Information Area from EEPROM .... 
    //-----------------------------------------------------------         
    luwPageNumber = 160; // user information page offset starts at 160...
    
    luwCtr = 0;
    do{
        devEEPROM_RandomRead(luwPageNumber+luwCtr,&lubData); 
        // Conditions
        // 1.) Maximum characters: 96 , Peter required to use 96 chars regardless the 48 chars stated in the FRS released last 9/21/2010..
        // 2.) Null character exits the loop.
        if((lubData==0)||(luwCtr>=PDF_ORIGINATOR_NOTES_MAX_CHARS)) break;
        
        par_strValue[luwCtr] = (UINT8)lubData;                
        
        ++luwCtr;
        
    }while(1);
    
    par_strValue[luwCtr] = 0; // null terminator...                                        
}





/*  End of File  */ 
