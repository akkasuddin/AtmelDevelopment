/*
 * << Haru Free PDF Library 2.0.7 >> -- hpdf_conf.h
 *
 * URL http://libharu.sourceforge.net/
 *
 * Copyright (c) 1999-2006 Takeshi Kanno
 *
 * Permission to use, copy, modify, distribute and sell this software
 * and its documentation for any purpose is hereby granted without fee,
 * provided that the above copyright notice appear in all copies and
 * that both that copyright notice and this permission notice appear
 * in supporting documentation.
 * It is provided "as is" without express or implied warranty.
 *
 */

#ifndef _HPDF_CONF_H
#define _HPDF_CONF_H

#include <stdlib.h>
#include <math.h>
#include "hwPIC24F.h"
#include "hwSystem.h"
#include "ff.h"
#include "appPDF.h"


/*----------------------------------------------------------------------------*/
/*----- standard C library functions -----------------------------------------*/

#define HPDF_FOPEN          f_open
#define HPDF_FCLOSE         f_close
#define HPDF_FREAD          f_read
#define HPDF_FWRITE         f_write
#define HPDF_FSEEK          f_lseek
#define HPDF_FTELL          f_tell
#define HPDF_FEOF           f_eof
#define HPDF_FERROR         f_error
#define HPDF_FILEP          FIL
#define HPDF_MALLOC         malloc
#define HPDF_FREE           free  
#define HPDF_SIN            sin
#define HPDF_COS            cos

#define HPDF_WRITE          FA_WRITE



/*----------------------------------------------------------------------------*/
/*----- parameters in relation to performance --------------------------------*/

/* default buffer size of memory-stream-object */
#define HPDF_STREAM_BUF_SIZ         512

/* default array size of list-object */
#define HPDF_DEF_ITEMS_PER_BLOCK    4

/* default array size of cross-reference-table */
#define HPDF_DEFALUT_XREF_ENTRY_NUM 4

/* default array size of page-list-tablef */
#define HPDF_DEF_PAGE_LIST_NUM      4

/* default array size of range-table of cid-fontdef */
#define HPDF_DEF_RANGE_TBL_NUM      4

/* default buffer size of memory-pool-object */
#define HPDF_MPOOL_BUF_SIZ          8192
#define HPDF_MIN_MPOOL_BUF_SIZ      256
#define HPDF_MAX_MPOOL_BUF_SIZ      1048576

/* alignment size of memory-pool-object
 */
#define HPDF_ALIGN_SIZ              sizeof int;


#endif /* _HPDF_CONF_H */

