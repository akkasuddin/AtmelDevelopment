/*
 * << Haru Free PDF Library 2.0.4 >> -- hpdf_fontdef.h
 *
 * Copyright (c) 1999-2006 Takeshi Kanno <takeshi_kanno@est.hi-ho.ne.jp>
 *
 * Permission to use, copy, modify, distribute and sell this software
 * and its documentation for any purpose is hereby granted without fee,
 * provided that the above copyright notice appear in all copies and
 * that both that copyright notice and this permission notice appear
 * in supporting documentation.
 * It is provided "as is" without express or implied warranty.
 *
 */

#ifndef _HPDF_FONTDEF_H
#define _HPDF_FONTDEF_H

#include "hpdf_objects.h"

#ifdef __cplusplus
extern "C" {
#endif


#define HPDF_FONTDEF_SIG_BYTES 0x464F4E54L

/*------ collection of flags for defining characteristics. ---*/

#define HPDF_FONT_FIXED_WIDTH    1
#define HPDF_FONT_SERIF          2
#define HPDF_FONT_SYMBOLIC       4
#define HPDF_FONT_SCRIPT         8
  /* Reserved                    16 */
#define HPDF_FONT_STD_CHARSET    32
#define HPDF_FONT_ITALIC         64
  /* Reserved                    128
     Reserved                    256
     Reserved                    512
     Reserved                    1024
     Reserved                    2048
     Reserved                    4096
     Reserved                    8192
     Reserved                    16384
     Reserved                    32768 */
#define HPDF_FONT_ALL_CAP        65536
#define HPDF_FONT_SMALL_CAP      131072
#define HPDF_FONT_FOURCE_BOLD    262144

#define HPDF_CID_W_TYPE_FROM_TO     0
#define HPDF_CID_W_TYPE_FROM_ARRAY  1

/*-- HPDF_FontDef ---------------------------------------*/

typedef struct _HPDF_CharData {
    HPDF_INT16     char_cd;
    HPDF_UNICODE   unicode;
    HPDF_INT16     width;
} HPDF_CharData;

typedef enum  _HPDF_FontDefType {
    HPDF_FONTDEF_TYPE_TYPE1,
    HPDF_FONTDEF_TYPE_TRUETYPE,
    HPDF_FONTDEF_TYPE_CID,
    HPDF_FONTDEF_TYPE_UNINITIALIZED,
    HPDF_FONTDEF_TYPE_EOF
} HPDF_FontDefType;

typedef struct _HPDF_CID_Width {
    HPDF_UINT16   cid;
    HPDF_INT16    width;
}  HPDF_CID_Width;

/*----------------------------------------------------------------------------*/
/*----- HPDF_FontDef ---------------------------------------------------------*/

typedef struct _HPDF_FontDef_Rec  *HPDF_FontDef;

typedef void  (*HPDF_FontDef_FreeFunc)  (HPDF_FontDef  fontdef);

typedef void  (*HPDF_FontDef_CleanFunc)  (HPDF_FontDef  fontdef);

typedef HPDF_STATUS  (*HPDF_FontDef_InitFunc)  (HPDF_FontDef  fontdef);

typedef struct _HPDF_FontDef_Rec {
    HPDF_UINT32              sig_bytes;
    char                base_font[HPDF_LIMIT_MAX_NAME_LEN + 1];
    HPDF_MMgr                mmgr;
    HPDF_Error               error;
    HPDF_FontDefType         type;
    HPDF_FontDef_CleanFunc   clean_fn;
    HPDF_FontDef_FreeFunc    free_fn;
    HPDF_FontDef_InitFunc    init_fn;

    HPDF_INT16    ascent;
    HPDF_INT16    descent;
    HPDF_UINT     flags;
    HPDF_Box      font_bbox;
    HPDF_INT16    italic_angle;
    HPDF_UINT16   stemv;
    HPDF_INT16    avg_width;
    HPDF_INT16    max_width;
    HPDF_INT16    missing_width;
    HPDF_UINT16   stemh;
    HPDF_UINT16   x_height;
    HPDF_UINT16   cap_height;

    /*  the initial value of descriptor entry is NULL.
     *  when first font-object besed on the fontdef object is created,
     *  the font-descriptor object is created and descriptor entry is set.
     */
    HPDF_Dict                descriptor;
    HPDF_Stream              data;

    HPDF_BOOL                valid;
    void                    *attr;
} HPDF_FontDef_Rec;


void
HPDF_FontDef_Free  (HPDF_FontDef  fontdef);


void
HPDF_FontDef_Cleanup  (HPDF_FontDef  fontdef);


HPDF_BOOL
HPDF_FontDef_Validate  (HPDF_FontDef  fontdef);


/*----------------------------------------------------------------------------*/
/*----- HPDF_Type1FontDef  ---------------------------------------------------*/

typedef struct _HPDF_Type1FontDefAttrRec   *HPDF_Type1FontDefAttr;

typedef struct _HPDF_Type1FontDefAttrRec {
    HPDF_BYTE       first_char;                               /* Required */
    HPDF_BYTE       last_char;                                /* Required */
    HPDF_CharData  *widths;                                   /* Required */
    HPDF_UINT       widths_count;

    HPDF_INT16      leading;
    char      *char_set;
    char       encoding_scheme[HPDF_LIMIT_MAX_NAME_LEN + 1];
    HPDF_UINT       length1;
    HPDF_UINT       length2;
    HPDF_UINT       length3;
    HPDF_BOOL       is_base14font;
    HPDF_BOOL       is_fixed_pitch;

    HPDF_Stream     font_data;
} HPDF_Type1FontDefAttr_Rec;



HPDF_FontDef
HPDF_Type1FontDef_New  (HPDF_MMgr  mmgr);



HPDF_FontDef
HPDF_Type1FontDef_Duplicate  (HPDF_MMgr     mmgr,
                              HPDF_FontDef  src);


HPDF_STATUS
HPDF_Type1FontDef_SetWidths  (HPDF_FontDef         fontdef,
                              const HPDF_CharData  *widths);


HPDF_INT16
HPDF_Type1FontDef_GetWidthByName  (HPDF_FontDef     fontdef,
                                   const char  *gryph_name);


HPDF_INT16
HPDF_Type1FontDef_GetWidth  (HPDF_FontDef  fontdef,
                             HPDF_UNICODE  unicode);


HPDF_FontDef
HPDF_Base14FontDef_New  (HPDF_MMgr        mmgr,
                         const char  *font_name);



/*----------------------------------------------------------------------------*/
/*----- HPDF_TTFontDef  ------------------------------------------------------*/

#define HPDF_TTF_FONT_TAG_LEN  6

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _HPDF_FONTDEF_H */

