/*
 * << Haru Free PDF Library 2.0.5 >> -- hpdf_doc.c
 *
 * Copyright (c) 1999-2006 Takeshi Kanno <takeshi_kanno@est.hi-ho.ne.jp>
 *
 * Permission to use, copy, modify, distribute and sell this software
 * and its documentation for any purpose is hereby granted without fee,
 * provided that the above copyright notice appear in all copies and
 * that both that copyright notice and this permission notice appear
 * in supporting documentation.
 * It is provided "as is" without express or implied warranty.
 *
 */

#include <time.h>
#include "hpdf_conf.h"
#include "hpdf_utils.h"
#include "hpdf_encryptdict.h"
#include "hpdf_info.h"
#include "hpdf_page_label.h"
#include "hpdf.h"
#include "hpdf_destination.h"


static const char *HPDF_VERSION_STR[5] = {
                "%PDF-1.2\012%\267\276\255\252\012",
                "%PDF-1.3\012%\267\276\255\252\012",
                "%PDF-1.4\012%\267\276\255\252\012",
                "%PDF-1.5\012%\267\276\255\252\012",
                "%PDF-1.6\012%\267\276\255\252\012"
};


static HPDF_STATUS
WriteHeader  (HPDF_Doc      pdf,
              HPDF_Stream   stream);


static HPDF_STATUS
PrepareTrailer  (HPDF_Doc   pdf);


static void
FreeEncoderList (HPDF_Doc  pdf);


static void
FreeFontDefList (HPDF_Doc  pdf);


static void
CleanupFontDefList (HPDF_Doc  pdf);


static HPDF_Dict
GetInfo  (HPDF_Doc  pdf);

static HPDF_STATUS
InternalSaveToStream  (HPDF_Doc      pdf,
                       HPDF_Stream   stream);

/*---------------------------------------------------------------------------*/

HPDF_EXPORT(const char *)
HPDF_GetVersion (void)
{
    return HPDF_VERSION_TEXT;
}


HPDF_BOOL
HPDF_Doc_Validate  (HPDF_Doc  pdf)
{
    //HPDF_PTRACE ((" HPDF_Doc_Validate\n"));

    if (!pdf || pdf->sig_bytes != HPDF_SIG_BYTES)
        return HPDF_FALSE;
    else
        return HPDF_TRUE;
}


HPDF_EXPORT(HPDF_BOOL)
HPDF_HasDoc  (HPDF_Doc  pdf)
{
    //HPDF_PTRACE ((" HPDF_HasDoc\n"));

    if (!pdf || pdf->sig_bytes != HPDF_SIG_BYTES)
        return HPDF_FALSE;

    if (!pdf->catalog || pdf->error.error_no != HPDF_NOERROR) {
        HPDF_RaiseError (&pdf->error, HPDF_INVALID_DOCUMENT, 0);
        return HPDF_FALSE;
    } else
        return HPDF_TRUE;
}


HPDF_EXPORT(HPDF_Doc)
HPDF_New  (HPDF_Error_Handler    user_error_fn,
           void                  *user_data)
{
    //HPDF_PTRACE ((" HPDF_New\n"));

    return HPDF_NewEx (user_error_fn, NULL, NULL, 0, user_data);
}


HPDF_EXPORT(HPDF_Doc)
HPDF_NewEx  (HPDF_Error_Handler    user_error_fn,
             HPDF_Alloc_Func       user_alloc_fn,
             HPDF_Free_Func        user_free_fn,
             HPDF_UINT             mem_pool_buf_size,
             void                 *user_data)
{
    HPDF_Doc pdf;
    HPDF_MMgr mmgr;
    HPDF_Error_Rec tmp_error;

    //HPDF_PTRACE ((" HPDF_NewEx\n"));

    /* initialize temporary-error object */
    HPDF_Error_Init (&tmp_error, user_data);

    /* create memory-manager object */
    mmgr = HPDF_MMgr_New (&tmp_error, mem_pool_buf_size, user_alloc_fn, user_free_fn);
    
    if (!mmgr) {
        HPDF_CheckError (&tmp_error);
        return NULL;
    }

    /* now create pdf_doc object */
    pdf = HPDF_GetMem (mmgr, sizeof (HPDF_Doc_Rec));
    if (!pdf) {
        HPDF_MMgr_Free (mmgr);
        HPDF_CheckError (&tmp_error);
        return NULL;
    }

    HPDF_MemSet (pdf, 0, sizeof (HPDF_Doc_Rec));
    pdf->sig_bytes = HPDF_SIG_BYTES;
    pdf->mmgr = mmgr;
    pdf->pdf_version = HPDF_VER_13;
    pdf->compression_mode = HPDF_COMP_NONE;

    /* copy the data of temporary-error object to the one which is
       included in pdf_doc object */
    pdf->error = tmp_error;

    /* switch the error-object of memory-manager */
    mmgr->error = &pdf->error;

    if (HPDF_NewDoc (pdf) != HPDF_OK) {
        HPDF_Free (pdf);
        HPDF_CheckError (&tmp_error);
        return NULL;
    }

    pdf->error.error_fn = user_error_fn;

    return pdf;
}


HPDF_EXPORT(void)
HPDF_Free  (HPDF_Doc  pdf)
{
    //HPDF_PTRACE ((" HPDF_Free\n"));

    if (pdf) {
        HPDF_MMgr mmgr = pdf->mmgr;

        HPDF_FreeDocAll (pdf);

        pdf->sig_bytes = 0;

        HPDF_FreeMem (mmgr, pdf);
        HPDF_MMgr_Free (mmgr);
    }
}


HPDF_EXPORT(HPDF_STATUS)
HPDF_NewDoc  (HPDF_Doc  pdf)
{
    char buf[HPDF_TMP_BUF_SIZ];
    char *ptr = buf;
    char *eptr = buf + HPDF_TMP_BUF_SIZ - 1;
    const char *version;

    //HPDF_PTRACE ((" HPDF_NewDoc\n"));

    if (!HPDF_Doc_Validate (pdf))
        return HPDF_DOC_INVALID_OBJECT;

    HPDF_FreeDoc (pdf);

    pdf->xref = HPDF_Xref_New (pdf->mmgr, 0);
    if (!pdf->xref)
        return HPDF_CheckError (&pdf->error);

    pdf->trailer = pdf->xref->trailer;

    pdf->font_mgr = HPDF_List_New (pdf->mmgr, HPDF_DEF_ITEMS_PER_BLOCK);
    if (!pdf->font_mgr)
        return HPDF_CheckError (&pdf->error);

    if (!pdf->fontdef_list) {
        pdf->fontdef_list = HPDF_List_New (pdf->mmgr,
                HPDF_DEF_ITEMS_PER_BLOCK);
        if (!pdf->fontdef_list)
            return HPDF_CheckError (&pdf->error);
    }

    if (!pdf->encoder_list) {
        pdf->encoder_list = HPDF_List_New (pdf->mmgr,
                HPDF_DEF_ITEMS_PER_BLOCK);
        if (!pdf->encoder_list)
            return HPDF_CheckError (&pdf->error);
    }

    pdf->catalog = HPDF_Catalog_New (pdf->mmgr, pdf->xref);
    if (!pdf->catalog)
        return HPDF_CheckError (&pdf->error);

    pdf->root_pages = HPDF_Catalog_GetRoot (pdf->catalog);
    if (!pdf->root_pages)
        return HPDF_CheckError (&pdf->error);

    pdf->page_list = HPDF_List_New (pdf->mmgr, HPDF_DEF_PAGE_LIST_NUM);
    if (!pdf->page_list)
        return HPDF_CheckError (&pdf->error);

    pdf->cur_pages = pdf->root_pages;

    ptr = (char *)HPDF_StrCpy ((char*)ptr, " ", (char*)eptr);
    version = HPDF_GetVersion ();
    HPDF_StrCpy (ptr, (const char*)version, 0);

    if (HPDF_SetInfoAttr (pdf, HPDF_INFO_PRODUCER, buf) != HPDF_OK)
        return HPDF_CheckError (&pdf->error);

    return HPDF_OK;
}


HPDF_EXPORT(void)
HPDF_FreeDoc  (HPDF_Doc  pdf)
{
    //HPDF_PTRACE ((" HPDF_FreeDoc\n"));

    if (HPDF_Doc_Validate (pdf)) {
        if (pdf->xref) {
           HPDF_Xref_Free (pdf->xref);
           pdf->xref = NULL;
        }

        if (pdf->font_mgr) {
            HPDF_List_Free (pdf->font_mgr);
            pdf->font_mgr = NULL;
        }

        if (pdf->fontdef_list)
            CleanupFontDefList (pdf);

        HPDF_MemSet(pdf->ttfont_tag, 0, 6);

        pdf->pdf_version = HPDF_VER_13;
        //pdf->outlines = NULL; // mjm
        pdf->catalog = NULL;
        pdf->root_pages = NULL;
        pdf->cur_pages = NULL;
        pdf->cur_page = NULL;
        pdf->encrypt_on = HPDF_FALSE;
        pdf->cur_page_num = 0;
        pdf->cur_encoder = NULL;
        pdf->def_encoder = NULL;
        pdf->page_per_pages = 0;

        if (pdf->page_list) {
            HPDF_List_Free (pdf->page_list);
            pdf->page_list = NULL;
        }

        pdf->encrypt_dict = NULL;
        pdf->info = NULL;

        HPDF_Error_Reset (&pdf->error);

        if (pdf->stream) {
            HPDF_Stream_Free (pdf->stream);
            pdf->stream = NULL;
        }
    }
}


HPDF_EXPORT(void)
HPDF_FreeDocAll  (HPDF_Doc  pdf)
{
    //HPDF_PTRACE ((" HPDF_FreeDocAll\n"));

    if (HPDF_Doc_Validate (pdf)) {
        HPDF_FreeDoc (pdf);

        if (pdf->fontdef_list)
            FreeFontDefList (pdf);

        if (pdf->encoder_list)
            FreeEncoderList (pdf);

        pdf->compression_mode = HPDF_COMP_NONE;

        HPDF_Error_Reset (&pdf->error);
    }
}


static HPDF_STATUS
WriteHeader  (HPDF_Doc      pdf,
              HPDF_Stream   stream)
{
    HPDF_UINT idx = (HPDF_INT)pdf->pdf_version;

    //HPDF_PTRACE ((" WriteHeader\n"));

    if (HPDF_Stream_WriteStr (stream, HPDF_VERSION_STR[idx]) != HPDF_OK)
        return pdf->error.error_no;

    return HPDF_OK;
}


static HPDF_STATUS
PrepareTrailer  (HPDF_Doc    pdf)
{
    //HPDF_PTRACE ((" PrepareTrailer\n"));

  /*  if (HPDF_Dict_Add (pdf->trailer, "Root", pdf->catalog) != HPDF_OK)
        return pdf->error.error_no;

    if (HPDF_Dict_Add (pdf->trailer, "Info", pdf->info) != HPDF_OK)
        return pdf->error.error_no;
*/
    if (HPDF_Dict_Add (pdf->trailer, "Info", pdf->info) != HPDF_OK)
        return pdf->error.error_no;


    if (HPDF_Dict_Add (pdf->trailer, "Root", pdf->catalog) != HPDF_OK)
        return pdf->error.error_no;



    return HPDF_OK;
}


HPDF_STATUS
HPDF_Doc_SetEncryptOn  (HPDF_Doc   pdf)
{
    //HPDF_PTRACE ((" HPDF_Doc_SetEncryptOn\n"));

    if (pdf->encrypt_on)
        return HPDF_OK;

    if (!pdf->encrypt_dict)
        return HPDF_SetError (&pdf->error, HPDF_DOC_ENCRYPTDICT_NOT_FOUND,
                0);

    if (pdf->encrypt_dict->header.obj_id == HPDF_OTYPE_NONE)
        if (HPDF_Xref_Add (pdf->xref, pdf->encrypt_dict) != HPDF_OK)
            return pdf->error.error_no;

    if (HPDF_Dict_Add (pdf->trailer, "Encrypt", pdf->encrypt_dict) != HPDF_OK)
        return pdf->error.error_no;

    pdf->encrypt_on = HPDF_TRUE;

    return HPDF_OK;
}

HPDF_EXPORT(HPDF_STATUS)
HPDF_SetPassword  (HPDF_Doc          pdf,
                   const char  *owner_passwd,
                   const char  *user_passwd)
{
    //HPDF_PTRACE ((" HPDF_SetPassword\n"));

    if (!HPDF_HasDoc (pdf))
        return HPDF_DOC_INVALID_OBJECT;

    if (!pdf->encrypt_dict) {
        pdf->encrypt_dict = HPDF_EncryptDict_New (pdf->mmgr, pdf->xref);

        if (!pdf->encrypt_dict)
            return HPDF_CheckError (&pdf->error);
    }

    if (HPDF_EncryptDict_SetPassword (pdf->encrypt_dict, owner_passwd,
                user_passwd) != HPDF_OK)
        return HPDF_CheckError (&pdf->error);

    return HPDF_Doc_SetEncryptOn (pdf);
}


HPDF_EXPORT(HPDF_STATUS)
HPDF_SetPermission  (HPDF_Doc    pdf,
                     HPDF_UINT   permission)
{
    HPDF_Encrypt e;

    //HPDF_PTRACE ((" HPDF_SetPermission\n"));

    if (!HPDF_HasDoc (pdf))
        return HPDF_DOC_INVALID_OBJECT;

    e = HPDF_EncryptDict_GetAttr (pdf->encrypt_dict);

    if (!e)
        return HPDF_RaiseError (&pdf->error,
                HPDF_DOC_ENCRYPTDICT_NOT_FOUND, 0);
    else
        e->permission = permission;

    return HPDF_OK;
}


HPDF_EXPORT(HPDF_STATUS)
HPDF_SetEncryptionMode  (HPDF_Doc           pdf,
                         HPDF_EncryptMode   mode,
                         HPDF_UINT          key_len)
{
    HPDF_Encrypt e;

    //HPDF_PTRACE ((" HPDF_SetEncryptionMode\n"));

    if (!HPDF_Doc_Validate (pdf))
        return HPDF_DOC_INVALID_OBJECT;

    e = HPDF_EncryptDict_GetAttr (pdf->encrypt_dict);

    if (!e)
        return HPDF_RaiseError (&pdf->error,
                HPDF_DOC_ENCRYPTDICT_NOT_FOUND, 0);
    else {
        if (mode == HPDF_ENCRYPT_R2)
            e->key_len = 5;
        else {
            /* if encryption mode is specified revision-3, the version of
             * pdf file is set to 1.4
             */
            pdf->pdf_version = HPDF_VER_14;

            if (key_len >= 5 && key_len <= 16)
                e->key_len = key_len;
            else if (key_len == 0)
                e->key_len = 16;
            else
                return HPDF_RaiseError (&pdf->error,
                        HPDF_INVALID_ENCRYPT_KEY_LEN, 0);
        }
        e->mode = mode;
    }

    return HPDF_OK;
}

HPDF_STATUS
HPDF_Doc_PrepareEncryption  (HPDF_Doc   pdf)
{
    HPDF_Encrypt e= HPDF_EncryptDict_GetAttr (pdf->encrypt_dict);
    HPDF_Dict info = GetInfo (pdf);
    HPDF_Array id;
    
    e = HPDF_EncryptDict_GetAttr (pdf->encrypt_dict);
    info = GetInfo (pdf);

    if (!e)
        return HPDF_DOC_ENCRYPTDICT_NOT_FOUND;

    if (!info)
        return pdf->error.error_no;

    if (HPDF_EncryptDict_Prepare (pdf->encrypt_dict, info, pdf->xref) !=
            HPDF_OK)
        return pdf->error.error_no;

    /* reset 'ID' to trailer-dictionary */
    id = HPDF_Dict_GetItem (pdf->trailer, "ID", HPDF_OCLASS_ARRAY);
    if (!id) {
        id = HPDF_Array_New (pdf->mmgr);

        if (!id || HPDF_Dict_Add (pdf->trailer, "ID", id) != HPDF_OK)
            return pdf->error.error_no;
    } else
        HPDF_Array_Clear (id);

    if (HPDF_Array_Add (id, HPDF_Binary_New (pdf->mmgr, e->encrypt_id,
                HPDF_ID_LEN)) != HPDF_OK)
        return pdf->error.error_no;

    if (HPDF_Array_Add (id, HPDF_Binary_New (pdf->mmgr, e->encrypt_id,
                HPDF_ID_LEN)) != HPDF_OK)
        return pdf->error.error_no;

    return HPDF_OK;
}

HPDF_STATUS
HPDF_PDFA_GenerateID(HPDF_Doc pdf)
{
    HPDF_Array id;
    HPDF_BYTE *currentTime;
    HPDF_BYTE idkey[HPDF_MD5_KEY_LEN];
    HPDF_MD5_CTX md5_ctx;
    time_t ltime;


    ltime = time(NULL);
    currentTime = (HPDF_BYTE *)ctime(&ltime);

    id = HPDF_Dict_GetItem(pdf->trailer, "ID", HPDF_OCLASS_ARRAY);
    if (!id) {
       id = HPDF_Array_New(pdf->mmgr);


       if (!id || HPDF_Dict_Add(pdf->trailer, "ID", id) != HPDF_OK)
         return pdf->error.error_no;

       HPDF_MD5Init(&md5_ctx);
       HPDF_MD5Update(&md5_ctx, (HPDF_BYTE *) "libHaru", sizeof("libHaru") - 1);
       HPDF_MD5Update(&md5_ctx, currentTime, HPDF_StrLen((const char *)currentTime, -1));
       HPDF_MD5Final(idkey, &md5_ctx);

       if (HPDF_Array_Add (id, HPDF_Binary_New (pdf->mmgr, idkey, HPDF_MD5_KEY_LEN)) != HPDF_OK)
         return pdf->error.error_no;


       if (HPDF_Array_Add (id, HPDF_Binary_New (pdf->mmgr,idkey,HPDF_MD5_KEY_LEN)) != HPDF_OK)
         return pdf->error.error_no;

       return HPDF_OK;
    }

    return HPDF_OK;
}



static HPDF_STATUS
InternalSaveToStream  (HPDF_Doc      pdf,
                       HPDF_Stream   stream)
{
    register HPDF_STATUS ret;
    
    if ((ret = WriteHeader (pdf, stream)) != HPDF_OK)
        return ret;

    /* prepare trailer */
    if ((ret = PrepareTrailer (pdf)) != HPDF_OK)
        return ret;

    /* prepare encription */
    if (pdf->encrypt_on) {
        HPDF_Encrypt e= HPDF_EncryptDict_GetAttr (pdf->encrypt_dict);

        if ((ret = HPDF_Doc_PrepareEncryption (pdf)) != HPDF_OK)
            return ret;

        if ((ret = HPDF_Xref_WriteToStream (pdf->xref, stream, e)) != HPDF_OK)
            return ret;
    } else {

        HPDF_PDFA_GenerateID(pdf);
        if ((ret = HPDF_Xref_WriteToStream (pdf->xref, stream, NULL)) !=
                HPDF_OK)
            return ret;
    }

    return HPDF_OK;
}

HPDF_EXPORT(HPDF_STATUS)
HPDF_SaveToFile  (HPDF_Doc     pdf,
                  const char  *file_name)
{
    HPDF_Stream stream;

    //HPDF_PTRACE ((" HPDF_SaveToFile\n"));

    if (!HPDF_HasDoc (pdf))
        return HPDF_INVALID_DOCUMENT;

    stream = HPDF_FileWriter_New (pdf->mmgr, file_name);
    
    if (!stream)
        return HPDF_CheckError (&pdf->error);

    InternalSaveToStream (pdf, stream);

    HPDF_Stream_Free (stream);

    return HPDF_CheckError (&pdf->error);
}

HPDF_EXPORT(HPDF_Page)
HPDF_AddPage  (HPDF_Doc  pdf)
{
    HPDF_Page page;
    HPDF_STATUS ret;

    //HPDF_PTRACE ((" HPDF_AddPage\n"));

    if (!HPDF_HasDoc (pdf))
        return NULL;

    if (pdf->page_per_pages) {
        if (pdf->page_per_pages <= pdf->cur_page_num) {
            pdf->cur_pages = HPDF_Doc_AddPagesTo (pdf, pdf->root_pages);
            if (!pdf->cur_pages)
                return NULL;
            pdf->cur_page_num = 0;
        }
    }

    page = HPDF_Page_New (pdf->mmgr, pdf->xref);
    if (!page) {
        HPDF_CheckError (&pdf->error);
        return NULL;
    }

    if ((ret = HPDF_Pages_AddKids (pdf->cur_pages, page)) != HPDF_OK) {
        HPDF_RaiseError (&pdf->error, ret, 0);
        return NULL;
    }

    if ((ret = HPDF_List_Add ((HPDF_List)pdf->page_list, page)) != HPDF_OK) {
        HPDF_RaiseError (&pdf->error, ret, 0);
        return NULL;
    }

    pdf->cur_page = page;

    if (pdf->compression_mode & HPDF_COMP_TEXT)
        HPDF_Page_SetFilter (page, HPDF_STREAM_FILTER_FLATE_DECODE);

    pdf->cur_page_num++;

    return page;
}


HPDF_Pages
HPDF_Doc_AddPagesTo  (HPDF_Doc     pdf,
                      HPDF_Pages   parent)
{
    HPDF_Pages pages;

    //HPDF_PTRACE ((" HPDF_AddPagesTo\n"));

    if (!HPDF_HasDoc (pdf))
        return NULL;

    if (!HPDF_Pages_Validate (parent)) {
        HPDF_RaiseError (&pdf->error, HPDF_INVALID_PAGES, 0);
        return NULL;
    }

    /* check whether the page belong to the pdf */
    if (pdf->mmgr != parent->mmgr) {
        HPDF_RaiseError (&pdf->error, HPDF_INVALID_PAGES, 0);
        return NULL;
    }

    pages = HPDF_Pages_New (pdf->mmgr, parent, pdf->xref);
    if (pages)
        pdf->cur_pages = pages;
    else
        HPDF_CheckError (&pdf->error);


    return  pages;
}

/*----- font handling -------------------------------------------------------*/


static void
FreeFontDefList  (HPDF_Doc   pdf)
{
    HPDF_List list = pdf->fontdef_list;
    HPDF_UINT i;

    //HPDF_PTRACE ((" HPDF_Doc_FreeFontDefList\n"));

    for (i = 0; i < list->count; i++) {
        HPDF_FontDef def = (HPDF_FontDef)HPDF_List_ItemAt (list, i);

        HPDF_FontDef_Free (def);
    }

    HPDF_List_Free (list);

    pdf->fontdef_list = NULL;
}

static void
CleanupFontDefList  (HPDF_Doc  pdf)
{
    HPDF_List list = pdf->fontdef_list;
    HPDF_UINT i;

    //HPDF_PTRACE ((" CleanupFontDefList\n"));

    for (i = 0; i < list->count; i++) {
        HPDF_FontDef def = (HPDF_FontDef)HPDF_List_ItemAt (list, i);

        HPDF_FontDef_Cleanup (def);
    }
}


HPDF_FontDef
HPDF_Doc_FindFontDef  (HPDF_Doc          pdf,
                       const char  *font_name)
{
    HPDF_List list = pdf->fontdef_list;
    HPDF_UINT i;

    //HPDF_PTRACE ((" HPDF_Doc_FindFontDef\n"));

    for (i = 0; i < list->count; i++) {
        HPDF_FontDef def = (HPDF_FontDef)HPDF_List_ItemAt (list, i);

        if (HPDF_StrCmp (font_name, def->base_font) == 0) {
            if (def->type == HPDF_FONTDEF_TYPE_UNINITIALIZED) {
                if (!def->init_fn ||
                    def->init_fn (def) != HPDF_OK)
                    return NULL;
            }

            return def;
        }
    }

    return NULL;
}

HPDF_FontDef
HPDF_GetFontDef  (HPDF_Doc          pdf,
                  const char  *font_name)
{
    HPDF_STATUS ret;
    HPDF_FontDef def;

    //HPDF_PTRACE ((" HPDF_GetFontDef\n"));

    if (!HPDF_HasDoc (pdf))
        return NULL;

    def = HPDF_Doc_FindFontDef (pdf, font_name);

    if (!def) {
        def = HPDF_Base14FontDef_New (pdf->mmgr, font_name);

        if (!def)
            return NULL;

        if ((ret = HPDF_List_Add ((HPDF_List)pdf->fontdef_list, def)) != HPDF_OK) {
            HPDF_FontDef_Free (def);
            HPDF_RaiseError (&pdf->error, ret, 0);
            def = NULL;
        }
    }

    return def;
}


/*----- encoder handling ----------------------------------------------------*/


HPDF_Encoder
HPDF_Doc_FindEncoder  (HPDF_Doc         pdf,
                       const char  *encoding_name)
{
    HPDF_List list = pdf->encoder_list;
    HPDF_UINT i;

    //HPDF_PTRACE ((" HPDF_Doc_FindEncoder\n"));

    for (i = 0; i < list->count; i++) {
        HPDF_Encoder encoder = (HPDF_Encoder)HPDF_List_ItemAt (list, i);

        if (HPDF_StrCmp (encoding_name, encoder->name) == 0) {

            /* if encoder is uninitialize, call init_fn() */
            if (encoder->type == HPDF_ENCODER_TYPE_UNINITIALIZED) {
                if (!encoder->init_fn ||
                    encoder->init_fn (encoder) != HPDF_OK)
                    return NULL;
            }

            return encoder;
        }
    }

    return NULL;
}

HPDF_EXPORT(HPDF_Encoder)
HPDF_GetEncoder  (HPDF_Doc         pdf,
                  const char  *encoding_name)
{
    HPDF_Encoder encoder;
    HPDF_STATUS ret;

    //HPDF_PTRACE ((" HPDF_GetEncoder\n"));

    if (!HPDF_HasDoc (pdf))
        return NULL;

    encoder = HPDF_Doc_FindEncoder (pdf, encoding_name);

    if (!encoder) {
        encoder = HPDF_BasicEncoder_New (pdf->mmgr, encoding_name);

        if (!encoder) {
            HPDF_CheckError (&pdf->error);
            return NULL;
        }

        if ((ret = HPDF_List_Add ((HPDF_List)pdf->encoder_list, encoder)) != HPDF_OK) {
            HPDF_Encoder_Free (encoder);
            HPDF_RaiseError (&pdf->error, ret, 0);
            return NULL;
        }
    }

    return encoder;
}

static void
FreeEncoderList  (HPDF_Doc  pdf)
{
    HPDF_List list = pdf->encoder_list;
    HPDF_UINT i;

    //HPDF_PTRACE ((" FreeEncoderList\n"));

    for (i = 0; i < list->count; i++) {
        HPDF_Encoder encoder = (HPDF_Encoder)HPDF_List_ItemAt (list, i);

        HPDF_Encoder_Free (encoder);
    }

    HPDF_List_Free (list);

    pdf->encoder_list = NULL;
}


/*----- font handling -------------------------------------------------------*/


HPDF_Font
HPDF_Doc_FindFont  (HPDF_Doc          pdf,
                    const char  *font_name,
                    const char  *encoding_name)
{
    HPDF_UINT i;
    HPDF_Font font;

    //HPDF_PTRACE ((" HPDF_Doc_FindFont\n"));

    for (i = 0; i < pdf->font_mgr->count; i++) {
        HPDF_FontAttr attr;

        font = (HPDF_Font)HPDF_List_ItemAt (pdf->font_mgr, i);
        attr = (HPDF_FontAttr) font->attr;

        if (HPDF_StrCmp (attr->fontdef->base_font, font_name) == 0 &&
                HPDF_StrCmp (attr->encoder->name, encoding_name) == 0)
            return font;
    }

    return NULL;
}


HPDF_EXPORT(HPDF_Font)
HPDF_GetFont  (HPDF_Doc          pdf,
               const char  *font_name,
               const char  *encoding_name)
{ 
    HPDF_FontDef fontdef = NULL;
    HPDF_Encoder encoder = NULL;
    HPDF_Font font;

    //HPDF_PTRACE ((" HPDF_GetFont\n"));

    if (!HPDF_HasDoc (pdf))
        return NULL;

    if (!font_name) {
        HPDF_RaiseError (&pdf->error, HPDF_INVALID_FONT_NAME, 0);
        return NULL;
    }

    /* if encoding-name is not specified, find default-encoding of fontdef
     */
    if (!encoding_name) {
        fontdef = HPDF_GetFontDef (pdf, font_name);

        if (fontdef) {
            HPDF_Type1FontDefAttr attr = (HPDF_Type1FontDefAttr)fontdef->attr;

            if (fontdef->type == HPDF_FONTDEF_TYPE_TYPE1 &&
                HPDF_StrCmp (attr->encoding_scheme,
                            HPDF_ENCODING_FONT_SPECIFIC) == 0)
                encoder = HPDF_GetEncoder (pdf, HPDF_ENCODING_FONT_SPECIFIC);
            else
                encoder = HPDF_GetEncoder (pdf, HPDF_ENCODING_STANDARD);
        } else {
            HPDF_CheckError (&pdf->error);
            return NULL;
        }

        font = HPDF_Doc_FindFont (pdf, font_name, encoder->name);
    } else {
        font = HPDF_Doc_FindFont (pdf, font_name, encoding_name);
    }

    if (font)
        return font;

    if (!fontdef) {
        fontdef = HPDF_GetFontDef (pdf, font_name);

        if (!fontdef) {
            HPDF_CheckError (&pdf->error);
            return NULL;
        }
    }

    if (!encoder) {
        encoder = HPDF_GetEncoder (pdf, encoding_name);

        if (!encoder)
            return NULL;
    }

    switch (fontdef->type) {
        case HPDF_FONTDEF_TYPE_TYPE1:
            font = HPDF_Type1Font_New (pdf->mmgr, fontdef, encoder, pdf->xref);

            if (font)
                HPDF_List_Add ((HPDF_List)pdf->font_mgr, font);

            break;
        case HPDF_FONTDEF_TYPE_TRUETYPE: // mjm removed! 

            break;
        case HPDF_FONTDEF_TYPE_CID: // mjm removed! 

            break;
        default:
            HPDF_RaiseError (&pdf->error, HPDF_UNSUPPORTED_FONT_TYPE, 0);
            return NULL;
    }

    if (!font)
        HPDF_CheckError (&pdf->error);

    if (font && (pdf->compression_mode & HPDF_COMP_METADATA))
        font->filter = HPDF_STREAM_FILTER_FLATE_DECODE;

    return font;
}

/*----- Info ---------------------------------------------------------------*/

static HPDF_Dict
GetInfo  (HPDF_Doc  pdf)
{
    if (!HPDF_HasDoc (pdf))
        return NULL;

    if (!pdf->info) {
        pdf->info = HPDF_Dict_New (pdf->mmgr);

        if (!pdf->info || HPDF_Xref_Add (pdf->xref, pdf->info) != HPDF_OK)
            pdf->info = NULL;
    }

    return pdf->info;
}


HPDF_EXPORT(HPDF_STATUS)
HPDF_SetInfoAttr  (HPDF_Doc          pdf,
                   HPDF_InfoType     type,
                   const char  *value)
{
    HPDF_STATUS ret;
    HPDF_Dict info = GetInfo (pdf);

    //HPDF_PTRACE((" HPDF_SetInfoAttr\n"));

    if (!info)
        return HPDF_CheckError (&pdf->error);

    ret = HPDF_Info_SetInfoAttr (info, type, value, pdf->cur_encoder);
    if (ret != HPDF_OK)
        return HPDF_CheckError (&pdf->error);

    return ret;
}

HPDF_EXPORT(HPDF_STATUS)
HPDF_SetInfoDateAttr  (HPDF_Doc        pdf,
                       HPDF_InfoType   type,
                       HPDF_Date       value)
{
    HPDF_STATUS ret;
    HPDF_Dict info = GetInfo (pdf);

    //HPDF_PTRACE((" HPDF_SetInfoDateAttr\n"));

    if (!info)
        return HPDF_CheckError (&pdf->error);

    ret = HPDF_Info_SetInfoDateAttr (info, type, value);
    if (ret != HPDF_OK)
        return HPDF_CheckError (&pdf->error);

    return ret;
}

HPDF_EXPORT(HPDF_STATUS)
HPDF_SetCompressionMode  (HPDF_Doc    pdf,
                          HPDF_UINT   mode)
{
    if (!HPDF_Doc_Validate (pdf))
        return HPDF_INVALID_DOCUMENT;

    if (mode != (mode & HPDF_COMP_MASK))
        return HPDF_RaiseError (&pdf->error, HPDF_INVALID_COMPRESSION_MODE, 0);

#ifndef HPDF_NOZLIB
    pdf->compression_mode = mode;

    return HPDF_OK;

#else /* HPDF_NOZLIB */

    return HPDF_INVALID_COMPRESSION_MODE;

#endif /* HPDF_NOZLIB */
}


HPDF_EXPORT(HPDF_STATUS)
HPDF_GetError  (HPDF_Doc   pdf)
{
    if (!HPDF_Doc_Validate (pdf))
        return HPDF_INVALID_DOCUMENT;

    return HPDF_Error_GetCode (&pdf->error);
}

/*   End of file   */

