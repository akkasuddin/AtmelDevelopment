/*
 * << Haru Free PDF Library 2.0.1 >> -- HPDF_FontDef_base14.c
 *
 * Copyright (c) 1999-2004 Takeshi Kanno <takeshi_kanno@est.hi-ho.ne.jp>
 *
 * Permission to use, copy, modify, distribute and sell this software
 * and its documentation for any purpose is hereby granted without fee,
 * provided that the above copyright notice appear in all copies and
 * that both that copyright notice and this permission notice appear
 * in supporting documentation.
 * It is provided "as is" without express or implied warranty.
 *
 * 2006.07.25 modified.
 */

#include "hpdf_conf.h"
#include "hpdf_utils.h"
#include "hpdf_fontdef.h"

static const HPDF_CharData CHAR_DATA_COURIER[1] = {//[316]
    {32, 0x0020, 600} 
    };

static const HPDF_CharData CHAR_DATA_COURIER_BOLD[1] = {//[316]
    {32, 0x0020, 600}
    };

static const HPDF_CharData CHAR_DATA_COURIER_BOLD_OBLIQUE[1] = {//[316]
    {32, 0x0020, 600}
    };

static const HPDF_CharData CHAR_DATA_COURIER_OBLIQUE[1] = {//[316]
    {32, 0x0020, 600}
    };

static const HPDF_CharData CHAR_DATA_HELVETICA[316] = {//[316]
    {32, 0x0020, 278} ,
    {33, 0x0021, 278},
    {34, 0x0022, 355},
    {35, 0x0023, 556},
    {36, 0x0024, 556},
    {37, 0x0025, 889},
    {38, 0x0026, 667},
    {39, 0x2019, 222},
    {40, 0x0028, 333},
    {41, 0x0029, 333},
    {42, 0x002A, 389},
    {43, 0x002B, 584},
    {44, 0x002C, 278},
    {45, 0x002D, 333},
    {46, 0x002E, 278},
    {47, 0x002F, 278},
    {48, 0x0030, 556},
    {49, 0x0031, 556},
    {50, 0x0032, 556},
    {51, 0x0033, 556},
    {52, 0x0034, 556},
    {53, 0x0035, 556},
    {54, 0x0036, 556},
    {55, 0x0037, 556},
    {56, 0x0038, 556},
    {57, 0x0039, 556},
    {58, 0x003A, 278},
    {59, 0x003B, 278},
    {60, 0x003C, 584},
    {61, 0x003D, 584},
    {62, 0x003E, 584},
    {63, 0x003F, 556},
    {64, 0x0040, 1015},
    {65, 0x0041, 667},
    {66, 0x0042, 667},
    {67, 0x0043, 722},
    {68, 0x0044, 722},
    {69, 0x0045, 667},
    {70, 0x0046, 611},
    {71, 0x0047, 778},
    {72, 0x0048, 722},
    {73, 0x0049, 278},
    {74, 0x004A, 500},
    {75, 0x004B, 667},
    {76, 0x004C, 556},
    {77, 0x004D, 833},
    {78, 0x004E, 722},
    {79, 0x004F, 778},
    {80, 0x0050, 667},
    {81, 0x0051, 778},
    {82, 0x0052, 722},
    {83, 0x0053, 667},
    {84, 0x0054, 611},
    {85, 0x0055, 722},
    {86, 0x0056, 667},
    {87, 0x0057, 944},
    {88, 0x0058, 667},
    {89, 0x0059, 667},
    {90, 0x005A, 611},
    {91, 0x005B, 278},
    {92, 0x005C, 278},
    {93, 0x005D, 278},
    {94, 0x005E, 469},
    {95, 0x005F, 556},
    {96, 0x2018, 222},
    {97, 0x0061, 556},
    {98, 0x0062, 556},
    {99, 0x0063, 500},
    {100, 0x0064, 556},
    {101, 0x0065, 556},
    {102, 0x0066, 278},
    {103, 0x0067, 556},
    {104, 0x0068, 556},
    {105, 0x0069, 222},
    {106, 0x006A, 222},
    {107, 0x006B, 500},
    {108, 0x006C, 222},
    {109, 0x006D, 833},
    {110, 0x006E, 556},
    {111, 0x006F, 556},
    {112, 0x0070, 556},
    {113, 0x0071, 556},
    {114, 0x0072, 333},
    {115, 0x0073, 500},
    {116, 0x0074, 278},
    {117, 0x0075, 556},
    {118, 0x0076, 500},
    {119, 0x0077, 722},
    {120, 0x0078, 500},
    {121, 0x0079, 500},
    {122, 0x007A, 500},
    {123, 0x007B, 334},
    {124, 0x007C, 260},
    {125, 0x007D, 334},
    {126, 0x007E, 584},
    {161, 0x00A1, 333},
    {162, 0x00A2, 556},
    {163, 0x00A3, 556},
    {164, 0x2044, 167},
    {165, 0x00A5, 556},
    {166, 0x0192, 556},
    {167, 0x00A7, 556},
    {168, 0x00A4, 556},
    {169, 0x0027, 191},
    {170, 0x201C, 333},
    {171, 0x00AB, 556},
    {172, 0x2039, 333},
    {173, 0x203A, 333},
    {174, 0xFB01, 500},
    {175, 0xFB02, 500},
    {177, 0x2013, 556},
    {178, 0x2020, 556},
    {179, 0x2021, 556},
    {180, 0x00B7, 278},
    {182, 0x00B6, 537},
    {183, 0x2022, 350},
    {184, 0x201A, 222},
    {185, 0x201E, 333},
    {186, 0x201D, 333},
    {187, 0x00BB, 556},
    {188, 0x2026, 1000},
    {189, 0x2030, 1000},
    {191, 0x00BF, 611},
    {193, 0x0060, 333},
    {194, 0x00B4, 333},
    {195, 0x02C6, 333},
    {196, 0x02DC, 333},
    {197, 0x00AF, 333},
    {198, 0x02D8, 333},
    {199, 0x02D9, 333},
    {200, 0x00A8, 333},
    {202, 0x02DA, 333},
    {203, 0x00B8, 333},
    {205, 0x02DD, 333},
    {206, 0x02DB, 333},
    {207, 0x02C7, 333},
    {208, 0x2014, 1000},
    {225, 0x00C6, 1000},
    {227, 0x00AA, 370},
    {232, 0x0141, 556},
    {233, 0x00D8, 778},
    {234, 0x0152, 1000},
    {235, 0x00BA, 365},
    {241, 0x00E6, 889},
    {245, 0x0131, 278},
    {248, 0x0142, 222},
    {249, 0x00F8, 611},
    {250, 0x0153, 944},
    {251, 0x00DF, 611},
    {-1, 0x00CF, 278},
    {-1, 0x00E9, 556},
    {-1, 0x0103, 556},
    {-1, 0x0171, 556},
    {-1, 0x011B, 556},
    {-1, 0x0178, 667},
    {-1, 0x00F7, 584},
    {-1, 0x00DD, 667},
    {-1, 0x00C2, 667},
    {-1, 0x00E1, 556},
    {-1, 0x00DB, 722},
    {-1, 0x00FD, 500},
    {-1, 0x0219, 500},
    {-1, 0x00EA, 556},
    {-1, 0x016E, 722},
    {-1, 0x00DC, 722},
    {-1, 0x0105, 556},
    {-1, 0x00DA, 722},
    {-1, 0x0173, 556},
    {-1, 0x00CB, 667},
    {-1, 0x0110, 722},
    {-1, 0xF6C3, 250},
    {-1, 0x00A9, 737},
    {-1, 0x0112, 667},
    {-1, 0x010D, 500},
    {-1, 0x00E5, 556},
    {-1, 0x0145, 722},
    {-1, 0x013A, 222},
    {-1, 0x00E0, 556},
    {-1, 0x0162, 611},
    {-1, 0x0106, 722},
    {-1, 0x00E3, 556},
    {-1, 0x0116, 667},
    {-1, 0x0161, 500},
    {-1, 0x015F, 500},
    {-1, 0x00ED, 278},
    {-1, 0x25CA, 471},
    {-1, 0x0158, 722},
    {-1, 0x0122, 778},
    {-1, 0x00FB, 556},
    {-1, 0x00E2, 556},
    {-1, 0x0100, 667},
    {-1, 0x0159, 333},
    {-1, 0x00E7, 500},
    {-1, 0x017B, 611},
    {-1, 0x00DE, 667},
    {-1, 0x014C, 778},
    {-1, 0x0154, 722},
    {-1, 0x015A, 667},
    {-1, 0x010F, 643},
    {-1, 0x016A, 722},
    {-1, 0x016F, 556},
    {-1, 0x00B3, 333},
    {-1, 0x00D2, 778},
    {-1, 0x00C0, 667},
    {-1, 0x0102, 667},
    {-1, 0x00D7, 584},
    {-1, 0x00FA, 556},
    {-1, 0x0164, 611},
    {-1, 0x2202, 476},
    {-1, 0x00FF, 500},
    {-1, 0x0143, 722},
    {-1, 0x00EE, 278},
    {-1, 0x00CA, 667},
    {-1, 0x00E4, 556},
    {-1, 0x00EB, 556},
    {-1, 0x0107, 500},
    {-1, 0x0144, 556},
    {-1, 0x016B, 556},
    {-1, 0x0147, 722},
    {-1, 0x00CD, 278},
    {-1, 0x00B1, 584},
    {-1, 0x00A6, 260},
    {-1, 0x00AE, 737},
    {-1, 0x011E, 778},
    {-1, 0x0130, 278},
    {-1, 0x2211, 600},
    {-1, 0x00C8, 667},
    {-1, 0x0155, 333},
    {-1, 0x014D, 556},
    {-1, 0x0179, 611},
    {-1, 0x017D, 611},
    {-1, 0x2265, 549},
    {-1, 0x00D0, 722},
    {-1, 0x00C7, 722},
    {-1, 0x013C, 222},
    {-1, 0x0165, 316},
    {-1, 0x0119, 556},
    {-1, 0x0172, 722},
    {-1, 0x00C1, 667},
    {-1, 0x00C4, 667},
    {-1, 0x00E8, 556},
    {-1, 0x017A, 500},
    {-1, 0x012F, 222},
    {-1, 0x00D3, 778},
    {-1, 0x00F3, 556},
    {-1, 0x0101, 556},
    {-1, 0x015B, 500},
    {-1, 0x00EF, 278},
    {-1, 0x00D4, 778},
    {-1, 0x00D9, 722},
    {-1, 0x0394, 612},
    {-1, 0x00FE, 556},
    {-1, 0x00B2, 333},
    {-1, 0x00D6, 778},
    {-1, 0x00B5, 556},
    {-1, 0x00EC, 278},
    {-1, 0x0151, 556},
    {-1, 0x0118, 667},
    {-1, 0x0111, 556},
    {-1, 0x00BE, 834},
    {-1, 0x015E, 667},
    {-1, 0x013E, 299},
    {-1, 0x0136, 667},
    {-1, 0x0139, 556},
    {-1, 0x2122, 1000},
    {-1, 0x0117, 556},
    {-1, 0x00CC, 278},
    {-1, 0x012A, 278},
    {-1, 0x013D, 556},
    {-1, 0x00BD, 834},
    {-1, 0x2264, 549},
    {-1, 0x00F4, 556},
    {-1, 0x00F1, 556},
    {-1, 0x0170, 722},
    {-1, 0x00C9, 667},
    {-1, 0x0113, 556},
    {-1, 0x011F, 556},
    {-1, 0x00BC, 834},
    {-1, 0x0160, 667},
    {-1, 0x0218, 667},
    {-1, 0x0150, 778},
    {-1, 0x00B0, 400},
    {-1, 0x00F2, 556},
    {-1, 0x010C, 722},
    {-1, 0x00F9, 556},
    {-1, 0x221A, 453},
    {-1, 0x010E, 722},
    {-1, 0x0157, 333},
    {-1, 0x00D1, 722},
    {-1, 0x00F5, 556},
    {-1, 0x0156, 722},
    {-1, 0x013B, 556},
    {-1, 0x00C3, 667},
    {-1, 0x0104, 667},
    {-1, 0x00C5, 667},
    {-1, 0x00D5, 778},
    {-1, 0x017C, 500},
    {-1, 0x011A, 667},
    {-1, 0x012E, 278},
    {-1, 0x0137, 500},
    {-1, 0x2212, 584},
    {-1, 0x00CE, 278},
    {-1, 0x0148, 556},
    {-1, 0x0163, 278},
    {-1, 0x00AC, 584},
    {-1, 0x00F6, 556},
    {-1, 0x00FC, 556},
    {-1, 0x2260, 549},
    {-1, 0x0123, 556},
    {-1, 0x00F0, 556},
    {-1, 0x017E, 500},
    {-1, 0x0146, 556},
    {-1, 0x00B9, 333},
    {-1, 0x012B, 278},
    {-1, 0x20AC, 556},
    {-1, 0xFFFF, 0}
    };

static const HPDF_CharData CHAR_DATA_HELVETICA_BOLD[1] = {//[316]
    {32, 0x0020, 278} 
    };

static const HPDF_CharData CHAR_DATA_HELVETICA_BOLD_OBLIQUE[1] = {//[316]
    {32, 0x0020, 278} 
    };

static const HPDF_CharData CHAR_DATA_HELVETICA_OBLIQUE[1] = {//[316]
    {32, 0x0020, 278} 
    };

static const HPDF_CharData CHAR_DATA_TIMES_ROMAN[1] = {//[316]
    {32, 0x0020, 250} 
    };

static const HPDF_CharData CHAR_DATA_TIMES_BOLD[1] = {//[316]
    {32, 0x0020, 250} 
    };

static const HPDF_CharData CHAR_DATA_TIMES_BOLD_ITALIC[1] = {//[316]
    {32, 0x0020, 250} 
    };

static const HPDF_CharData CHAR_DATA_TIMES_ITALIC[1] = {//[316]
    {32, 0x0020, 250} 
    };

static const HPDF_CharData CHAR_DATA_ZAPF_DINGBATS[1] = {//203
    {32, 0x0020, 278} 
    };

static const HPDF_CharData CHAR_DATA_SYMBOL[1] = {//190
    {32, 0x0020, 250} 
    };


/*----------------------------------------------------------------------------*/
/*------ base14 fonts --------------------------------------------------------*/

#define  HPDF_FONT_COURIER                 "Courier"
#define  HPDF_FONT_COURIER_BOLD            "Courier-Bold"
#define  HPDF_FONT_COURIER_OBLIQUE         "Courier-Oblique"
#define  HPDF_FONT_COURIER_BOLD_OBLIQUE    "Courier-BoldOblique"
#define  HPDF_FONT_HELVETICA               "Helvetica"
#define  HPDF_FONT_HELVETICA_BOLD          "Helvetica-Bold"
#define  HPDF_FONT_HELVETICA_OBLIQUE       "Helvetica-Oblique"
#define  HPDF_FONT_HELVETICA_BOLD_OBLIQUE  "Helvetica-BoldOblique"
#define  HPDF_FONT_TIMES_ROMAN             "Times-Roman"
#define  HPDF_FONT_TIMES_BOLD              "Times-Bold"
#define  HPDF_FONT_TIMES_ITALIC            "Times-Italic"
#define  HPDF_FONT_TIMES_BOLD_ITALIC       "Times-BoldItalic"
#define  HPDF_FONT_SYMBOL                  "Symbol"
#define  HPDF_FONT_ZAPF_DINGBATS           "ZapfDingbats"


typedef struct _HPDF_Base14FontDefData {
    const char      *font_name;
    const HPDF_CharData  *widths_table;
    HPDF_BOOL             is_font_specific;
    HPDF_INT16            ascent;
    HPDF_INT16            descent;
    HPDF_UINT16           x_height;
    HPDF_UINT16           cap_height;
    HPDF_Box              bbox;
} HPDF_Base14FontDefData;


static const HPDF_Base14FontDefData  HPDF_BUILTIN_FONTS[] = {
    {
        HPDF_FONT_HELVETICA,
        CHAR_DATA_HELVETICA,
        HPDF_FALSE,
        718,
        -207,
        523,
        718,
        {-166, -225, 1000, 931}
    },
    {
        NULL,
        NULL,
        HPDF_FALSE,
        0,
        0,
        0,
        0,
        {0, 0, 0, 0}
    } 
};


/*---------------------------------------------------------------------------*/

const HPDF_Base14FontDefData*
HPDF_Base14FontDef_FindBuiltinData  (const char  *font_name);


/*---------------------------------------------------------------------------*/
/*----- PDF_Base14FontDef ---------------------------------------------------*/

const HPDF_Base14FontDefData*
HPDF_Base14FontDef_FindBuiltinData  (const char  *font_name)
{
    HPDF_UINT i = 0;

    while (HPDF_BUILTIN_FONTS[i].font_name) {
        if (HPDF_StrCmp (HPDF_BUILTIN_FONTS[i].font_name, font_name) == 0)
            break;

        i++;
    }

    return &HPDF_BUILTIN_FONTS[i];
}

HPDF_FontDef
HPDF_Base14FontDef_New  (HPDF_MMgr        mmgr,
                         const char  *font_name)
{
    HPDF_FontDef                   fontdef;
    HPDF_STATUS                    ret;
    const HPDF_Base14FontDefData   *data;
    char                      *eptr;
    HPDF_Type1FontDefAttr          attr;

    fontdef = HPDF_Type1FontDef_New (mmgr);
    if (!fontdef)
        return NULL;

    data = HPDF_Base14FontDef_FindBuiltinData (font_name);

    if (!data->font_name) {
        HPDF_SetError (mmgr->error, HPDF_INVALID_FONT_NAME, 0);
        HPDF_FontDef_Free (fontdef);
        return NULL;
    }

    eptr = fontdef->base_font + HPDF_LIMIT_MAX_NAME_LEN;
    HPDF_StrCpy ((char*)fontdef->base_font, (const char*)data->font_name, eptr);

    attr = (HPDF_Type1FontDefAttr)fontdef->attr;
    attr->is_base14font = HPDF_TRUE;

    if (data->is_font_specific)
        HPDF_StrCpy ((char*)attr->encoding_scheme, HPDF_ENCODING_FONT_SPECIFIC,
                (char*)attr->encoding_scheme + HPDF_LIMIT_MAX_NAME_LEN);

    ret = HPDF_Type1FontDef_SetWidths (fontdef, data->widths_table);

    if (ret != HPDF_OK) {
        HPDF_FontDef_Free (fontdef);
        return NULL;
    }

    fontdef->font_bbox = data->bbox;
    fontdef->ascent = data->ascent;
    fontdef->descent = data->descent;
    fontdef->x_height = data->x_height;
    fontdef->cap_height = data->cap_height;

    fontdef->valid = HPDF_TRUE;

    return fontdef;
}

