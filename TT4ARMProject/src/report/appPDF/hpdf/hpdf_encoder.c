/*
 * << Haru Free PDF Library 2.0.0 >> -- hpdf_encoder.c
 *
 * Copyright (c) 1999-2004 Takeshi Kanno <takeshi_kanno@est.hi-ho.ne.jp>
 *
 * Permission to use, copy, modify, distribute and sell this software
 * and its documentation for any purpose is hereby granted without fee,
 * provided that the above copyright notice appear in all copies and
 * that both that copyright notice and this permission notice appear
 * in supporting documentation.
 * It is provided "as is" without express or implied warranty.
 *
 */

#include "hpdf_conf.h"
#include "hpdf_utils.h"
#include "hpdf_encoder.h"
#include "hpdf.h"

typedef struct _HPDF_UnicodeGryphPair {
    HPDF_UNICODE     unicode;
    const char  *gryph_name;
} HPDF_UnicodeGryphPair;

#define HPDF_BASIC_ENCODER_FIRST_CHAR  32
#define HPDF_BASIC_ENCODER_LAST_CHAR   255

static const HPDF_UnicodeGryphPair HPDF_UNICODE_GRYPH_NAME_MAP[] = {
    {0x0000, char_NOTDEF},
    {0x0020, "space"},
    {0x0021, "exclam"},
    {0x0022, "quotedbl"},
    {0x0023, "numbersign"},
    {0x0024, "dollar"},
    {0x0025, "percent"},
    {0x0026, "ampersand"},
    {0x0027, "quotesingle"},
    {0x0028, "parenleft"},
    {0x0029, "parenright"},
    {0x002A, "asterisk"},
    {0x002B, "plus"},
    {0x002C, "comma"},
    {0x002D, "hyphen"},
    {0x002E, "period"},
    {0x002F, "slash"},
    {0x0030, "zero"},
    {0x0031, "one"},
    {0x0032, "two"},
    {0x0033, "three"},
    {0x0034, "four"},
    {0x0035, "five"},
    {0x0036, "six"},
    {0x0037, "seven"},
    {0x0038, "eight"},
    {0x0039, "nine"},
    {0x003A, "colon"},
    {0x003B, "semicolon"},
    {0x003C, "less"},
    {0x003D, "equal"},
    {0x003E, "greater"},
    {0x003F, "question"},
    {0x0040, "at"},
    {0x0041, "A"},
    {0x0042, "B"},
    {0x0043, "C"},
    {0x0044, "D"},
    {0x0045, "E"},
    {0x0046, "F"},
    {0x0047, "G"},
    {0x0048, "H"},
    {0x0049, "I"},
    {0x004A, "J"},
    {0x004B, "K"},
    {0x004C, "L"},
    {0x004D, "M"},
    {0x004E, "N"},
    {0x004F, "O"},
    {0x0050, "P"},
    {0x0051, "Q"},
    {0x0052, "R"},
    {0x0053, "S"},
    {0x0054, "T"},
    {0x0055, "U"},
    {0x0056, "V"},
    {0x0057, "W"},
    {0x0058, "X"},
    {0x0059, "Y"},
    {0x005A, "Z"},
    {0x005B, "bracketleft"},
    {0x005C, "backslash"},
    {0x005D, "bracketright"},
    {0x005E, "asciicircum"},
    {0x005F, "underscore"},
    {0x0060, "grave"},
    {0x0061, "a"},
    {0x0062, "b"},
    {0x0063, "c"},
    {0x0064, "d"},
    {0x0065, "e"},
    {0x0066, "f"},
    {0x0067, "g"},
    {0x0068, "h"},
    {0x0069, "i"},
    {0x006A, "j"},
    {0x006B, "k"},
    {0x006C, "l"},
    {0x006D, "m"},
    {0x006E, "n"},
    {0x006F, "o"},
    {0x0070, "p"},
    {0x0071, "q"},
    {0x0072, "r"},
    {0x0073, "s"},
    {0x0074, "t"},
    {0x0075, "u"},
    {0x0076, "v"},
    {0x0077, "w"},
    {0x0078, "x"},
    {0x0079, "y"},
    {0x007A, "z"},
    {0x007B, "braceleft"},
    {0x007C, "bar"},
    {0x007D, "braceright"},
    {0x007E, "asciitilde"},
    {0x00A0, "space"},
    {0x00A1, "exclamdown"},
    {0x00A2, "cent"},
    {0x00A3, "sterling"},
    {0x00A4, "currency"},
    {0x00A5, "yen"},
    {0x00A6, "brokenbar"},
    {0x00A7, "section"},
    {0x00A8, "dieresis"},
    {0x00A9, "copyright"},
    {0x00AA, "ordfeminine"},
    {0x00AB, "guillemotleft"},
    {0x00AC, "logicalnot"},
    {0x00AD, "hyphen"},
    {0x00AE, "registered"},
    {0x00AF, "macron"},
    {0x00B0, "degree"},
    {0x00B1, "plusminus"},
    {0x00B2, "twosuperior"},
    {0x00B3, "threesuperior"},
    {0x00B4, "acute"},
    {0x00B5, "mu"},
    {0x00B6, "paragraph"},
    {0x00B7, "periodcentered"},
    {0x00B8, "cedilla"},
    {0x00B9, "onesuperior"},
    {0x00BA, "ordmasculine"},
    {0x00BB, "guillemotright"},
    {0x00BC, "onequarter"},
    {0x00BD, "onehalf"},
    {0x00BE, "threequarters"},
    {0x00BF, "questiondown"},
    {0x00C0, "Agrave"},
    {0x00C1, "Aacute"},
    {0x00C2, "Acircumflex"},
    {0x00C3, "Atilde"},
    {0x00C4, "Adieresis"},
    {0x00C5, "Aring"},
    {0x00C6, "AE"},
    {0x00C7, "Ccedilla"},
    {0x00C8, "Egrave"},
    {0x00C9, "Eacute"},
    {0x00CA, "Ecircumflex"},
    {0x00CB, "Edieresis"},
    {0x00CC, "Igrave"},
    {0x00CD, "Iacute"},
    {0x00CE, "Icircumflex"},
    {0x00CF, "Idieresis"},
    {0x00D0, "Eth"},
    {0x00D1, "Ntilde"},
    {0x00D2, "Ograve"},
    {0x00D3, "Oacute"},
    {0x00D4, "Ocircumflex"},
    {0x00D5, "Otilde"},
    {0x00D6, "Odieresis"},
    {0x00D7, "multiply"},
    {0x00D8, "Oslash"},
    {0x00D9, "Ugrave"},
    {0x00DA, "Uacute"},
    {0x00DB, "Ucircumflex"},
    {0x00DC, "Udieresis"},
    {0x00DD, "Yacute"},
    {0x00DE, "Thorn"},
    {0x00DF, "germandbls"},
    {0x00E0, "agrave"},
    {0x00E1, "aacute"},
    {0x00E2, "acircumflex"},
    {0x00E3, "atilde"},
    {0x00E4, "adieresis"},
    {0x00E5, "aring"},
    {0x00E6, "ae"},
    {0x00E7, "ccedilla"},
    {0x00E8, "egrave"},
    {0x00E9, "eacute"},
    {0x00EA, "ecircumflex"},
    {0x00EB, "edieresis"},
    {0x00EC, "igrave"},
    {0x00ED, "iacute"},
    {0x00EE, "icircumflex"},
    {0x00EF, "idieresis"},
    {0x00F0, "eth"},
    {0x00F1, "ntilde"},
    {0x00F2, "ograve"},
    {0x00F3, "oacute"},
    {0x00F4, "ocircumflex"},
    {0x00F5, "otilde"},
    {0x00F6, "odieresis"},
    {0x00F7, "divide"},
    {0x00F8, "oslash"},
    {0x00F9, "ugrave"},
    {0x00FA, "uacute"},
    {0x00FB, "ucircumflex"},
    {0x00FC, "udieresis"},
    {0x00FD, "yacute"},
    {0x00FE, "thorn"},
    {0x00FF, "ydieresis"},
    {0xFFFF, NULL}
};

static const HPDF_UNICODE HPDF_UNICODE_MAP_STANDARD[] = {
    0x0020
};

static const HPDF_UNICODE HPDF_UNICODE_MAP_WIN_ANSI[] = {
    0x0020, 0x0021, 0x0022, 0x0023, 0x0024, 0x0025, 0x0026, 0x0027,
    0x0028, 0x0029, 0x002A, 0x002B, 0x002C, 0x002D, 0x002E, 0x002F,
    0x0030, 0x0031, 0x0032, 0x0033, 0x0034, 0x0035, 0x0036, 0x0037,
    0x0038, 0x0039, 0x003A, 0x003B, 0x003C, 0x003D, 0x003E, 0x003F,
    0x0040, 0x0041, 0x0042, 0x0043, 0x0044, 0x0045, 0x0046, 0x0047,
    0x0048, 0x0049, 0x004A, 0x004B, 0x004C, 0x004D, 0x004E, 0x004F,
    0x0050, 0x0051, 0x0052, 0x0053, 0x0054, 0x0055, 0x0056, 0x0057,
    0x0058, 0x0059, 0x005A, 0x005B, 0x005C, 0x005D, 0x005E, 0x005F,
    0x0060, 0x0061, 0x0062, 0x0063, 0x0064, 0x0065, 0x0066, 0x0067,
    0x0068, 0x0069, 0x006A, 0x006B, 0x006C, 0x006D, 0x006E, 0x006F,
    0x0070, 0x0071, 0x0072, 0x0073, 0x0074, 0x0075, 0x0076, 0x0077,
    0x0078, 0x0079, 0x007A, 0x007B, 0x007C, 0x007D, 0x007E, 0x0000,
    0x20AC, 0x0000, 0x201A, 0x0192, 0x201E, 0x2026, 0x2020, 0x2021,
    0x02C6, 0x2030, 0x0160, 0x2039, 0x0152, 0x0000, 0x017D, 0x0000,
    0x0000, 0x2018, 0x2019, 0x201C, 0x201D, 0x2022, 0x2013, 0x2014,
    0x02DC, 0x2122, 0x0161, 0x203A, 0x0153, 0x0000, 0x017E, 0x0178,
    0x0000, 0x00A1, 0x00A2, 0x00A3, 0x00A4, 0x00A5, 0x00A6, 0x00A7,
    0x00A8, 0x00A9, 0x00AA, 0x00AB, 0x00AC, 0x0000, 0x00AE, 0x00AF,
    0x02DA, 0x00B1, 0x00B2, 0x00B3, 0x00B4, 0x00B5, 0x00B6, 0x00B7,
    0x00B8, 0x00B9, 0x00BA, 0x00BB, 0x00BC, 0x00BD, 0x00BE, 0x00BF,
    0x00C0, 0x00C1, 0x00C2, 0x00C3, 0x00C4, 0x00C5, 0x00C6, 0x00C7,
    0x00C8, 0x00C9, 0x00CA, 0x00CB, 0x00CC, 0x00CD, 0x00CE, 0x00CF,
    0x00D0, 0x00D1, 0x00D2, 0x00D3, 0x00D4, 0x00D5, 0x00D6, 0x00D7,
    0x00D8, 0x00D9, 0x00DA, 0x00DB, 0x00DC, 0x00DD, 0x00DE, 0x00DF,
    0x00E0, 0x00E1, 0x00E2, 0x00E3, 0x00E4, 0x00E5, 0x00E6, 0x00E7,
    0x00E8, 0x00E9, 0x00EA, 0x00EB, 0x00EC, 0x00ED, 0x00EE, 0x00EF,
    0x00F0, 0x00F1, 0x00F2, 0x00F3, 0x00F4, 0x00F5, 0x00F6, 0x00F7,
    0x00F8, 0x00F9, 0x00FA, 0x00FB, 0x00FC, 0x00FD, 0x00FE, 0x00FF
};

static const HPDF_UNICODE HPDF_UNICODE_MAP_MAC_ROMAN[] = {
    0x0020 
};

static const HPDF_UNICODE HPDF_UNICODE_MAP_FONT_SPECIFIC[] = {
    0x0020 
};

#define HPDF_ISO8859_FIRST_CHAR       32
#define HPDF_ISO8859_LAST_CHAR        255

/*---------------------------------------------------------------------------*/
/*------ ISO8859-2 Encoding -------------------------------------------------*/

static const HPDF_UNICODE HPDF_UNICODE_MAP_ISO8859_2[] = {
    0x0020 
};

/*---------------------------------------------------------------------------*/
/*------ ISO8859-3 Encoding -------------------------------------------------*/

static const HPDF_UNICODE HPDF_UNICODE_MAP_ISO8859_3[] = {
    0x0020     
};

/*---------------------------------------------------------------------------*/
/*------ ISO8859-4 Encoding -------------------------------------------------*/

static const HPDF_UNICODE HPDF_UNICODE_MAP_ISO8859_4[] = {
    0x0020 
};

/*---------------------------------------------------------------------------*/
/*------ ISO8859-5 Encoding -------------------------------------------------*/

static const HPDF_UNICODE HPDF_UNICODE_MAP_ISO8859_5[] = {
    0x0020 
};

/*---------------------------------------------------------------------------*/
/*------ ISO8859-6 Encoding -------------------------------------------------*/

static const HPDF_UNICODE HPDF_UNICODE_MAP_ISO8859_6[] = {
    0x0020 
};

/*---------------------------------------------------------------------------*/
/*------ ISO8859-7 Encoding -------------------------------------------------*/

static const HPDF_UNICODE HPDF_UNICODE_MAP_ISO8859_7[] = {
    0x0020 
};

/*---------------------------------------------------------------------------*/
/*------ ISO8859-8 Encoding -------------------------------------------------*/

static const HPDF_UNICODE HPDF_UNICODE_MAP_ISO8859_8[] = {
    0x0020 
};

/*---------------------------------------------------------------------------*/
/*------ ISO8859-9 Encoding -------------------------------------------------*/

static const HPDF_UNICODE HPDF_UNICODE_MAP_ISO8859_9[] = {
    0x0020 
};

/*---------------------------------------------------------------------------*/
/*------ ISO8859-10 Encoding ------------------------------------------------*/

static const HPDF_UNICODE HPDF_UNICODE_MAP_ISO8859_10[] = {
    0x0020 
};

/*---------------------------------------------------------------------------*/
/*------ ISO8859-11 Encoding ------------------------------------------------*/

static const HPDF_UNICODE HPDF_UNICODE_MAP_ISO8859_11[] = {
    0x0020 
};

/*---------------------------------------------------------------------------*/
/*------ ISO8859-13 Encoding ------------------------------------------------*/

static const HPDF_UNICODE HPDF_UNICODE_MAP_ISO8859_13[] = {
    0x0020 
};

/*---------------------------------------------------------------------------*/
/*------ ISO8859-14 Encoding ------------------------------------------------*/

static const HPDF_UNICODE HPDF_UNICODE_MAP_ISO8859_14[] = {
    0x0020 
};

/*---------------------------------------------------------------------------*/
/*------ ISO8859-15 Encoding ------------------------------------------------*/

static const HPDF_UNICODE HPDF_UNICODE_MAP_ISO8859_15[] = {
    0x0020 
};

/*---------------------------------------------------------------------------*/
/*------ ISO8859-16 Encoding ------------------------------------------------*/

static const HPDF_UNICODE HPDF_UNICODE_MAP_ISO8859_16[] = {
    0x0020 
};

#define HPDF_MSCP_FIRST_CHAR       32
#define HPDF_MSCP_LAST_CHAR        255


/*---------------------------------------------------------------------------*/
/*------ CP1250 Encoding ----------------------------------------------------*/

static const HPDF_UNICODE HPDF_UNICODE_MAP_CP1250[] = {
    0x0020 
};

/*---------------------------------------------------------------------------*/
/*------ CP1251 Encoding ----------------------------------------------------*/

static const HPDF_UNICODE HPDF_UNICODE_MAP_CP1251[] = {
    0x0020 
};

/*---------------------------------------------------------------------------*/
/*------ CP1252 Encoding ----------------------------------------------------*/

static const HPDF_UNICODE HPDF_UNICODE_MAP_CP1252[] = {
    0x0020 
};

/*---------------------------------------------------------------------------*/
/*------ CP1253 Encoding ----------------------------------------------------*/

static const HPDF_UNICODE HPDF_UNICODE_MAP_CP1253[] = {
    0x0020 
};

/*---------------------------------------------------------------------------*/
/*------ CP1254 Encoding ----------------------------------------------------*/

static const HPDF_UNICODE  HPDF_UNICODE_MAP_CP1254[] = {
    0x0020 
};

/*---------------------------------------------------------------------------*/
/*------ CP1255 Encoding ----------------------------------------------------*/

static const HPDF_UNICODE  HPDF_UNICODE_MAP_CP1255[] = {
    0x0020 
};

/*---------------------------------------------------------------------------*/
/*------ CP1256 Encoding ----------------------------------------------------*/

static const HPDF_UNICODE  HPDF_UNICODE_MAP_CP1256[] = {
    0x0020 
};

/*---------------------------------------------------------------------------*/
/*------ CP1257 Encoding ----------------------------------------------------*/

static const HPDF_UNICODE  HPDF_UNICODE_MAP_CP1257[] = {
    0x0020 
};

/*---------------------------------------------------------------------------*/
/*------ CP1258 Encoding ----------------------------------------------------*/

static const HPDF_UNICODE  HPDF_UNICODE_MAP_CP1258[] = {
    0x0020 
};

/*---------------------------------------------------------------------------*/
/*------ KOI8-R Encoding ----------------------------------------------------*/

static const HPDF_UNICODE  HPDF_UNICODE_MAP_KOI8_R[] = {
    0x0020
};

typedef struct _HPDF_BuiltinEncodingData {
    const char     *encoding_name;
    HPDF_BaseEncodings  base_encoding;
    const HPDF_UNICODE  *ovewrride_map;
} HPDF_BuiltinEncodingData;


static const HPDF_BuiltinEncodingData  HPDF_BUILTIN_ENCODINGS[] = {
    {
        HPDF_ENCODING_FONT_SPECIFIC,
        HPDF_BASE_ENCODING_FONT_SPECIFIC,
        NULL},
    {
        HPDF_ENCODING_STANDARD,
        HPDF_BASE_ENCODING_STANDARD,
        NULL},
    {
        HPDF_ENCODING_MAC_ROMAN,
        HPDF_BASE_ENCODING_MAC_ROMAN,
        NULL},
    {
        HPDF_ENCODING_WIN_ANSI,
        HPDF_BASE_ENCODING_WIN_ANSI,
        NULL
    },
    {
        HPDF_ENCODING_ISO8859_2,
        HPDF_BASE_ENCODING_WIN_ANSI,
        HPDF_UNICODE_MAP_ISO8859_2
    },
    {
        HPDF_ENCODING_ISO8859_3,
        HPDF_BASE_ENCODING_WIN_ANSI,
        HPDF_UNICODE_MAP_ISO8859_3
    },
    {
        HPDF_ENCODING_ISO8859_4,
        HPDF_BASE_ENCODING_WIN_ANSI,
        HPDF_UNICODE_MAP_ISO8859_4
    },
    {
        HPDF_ENCODING_ISO8859_5,
        HPDF_BASE_ENCODING_WIN_ANSI,
        HPDF_UNICODE_MAP_ISO8859_5
    },
    {
        HPDF_ENCODING_ISO8859_6,
        HPDF_BASE_ENCODING_WIN_ANSI,
        HPDF_UNICODE_MAP_ISO8859_6
    },
    {
        HPDF_ENCODING_ISO8859_7,
        HPDF_BASE_ENCODING_WIN_ANSI,
        HPDF_UNICODE_MAP_ISO8859_7
    },
    {
        HPDF_ENCODING_ISO8859_8,
        HPDF_BASE_ENCODING_WIN_ANSI,
        HPDF_UNICODE_MAP_ISO8859_8
    },
    {
        HPDF_ENCODING_ISO8859_9,
        HPDF_BASE_ENCODING_WIN_ANSI,
        HPDF_UNICODE_MAP_ISO8859_9
    },
    {
        HPDF_ENCODING_ISO8859_10,
        HPDF_BASE_ENCODING_WIN_ANSI,
        HPDF_UNICODE_MAP_ISO8859_10
    },
    {
        HPDF_ENCODING_ISO8859_11,
        HPDF_BASE_ENCODING_WIN_ANSI,
        HPDF_UNICODE_MAP_ISO8859_11
    },
    {
        HPDF_ENCODING_ISO8859_13,
        HPDF_BASE_ENCODING_WIN_ANSI,
        HPDF_UNICODE_MAP_ISO8859_13
    },
    {
        HPDF_ENCODING_ISO8859_14,
        HPDF_BASE_ENCODING_WIN_ANSI,
        HPDF_UNICODE_MAP_ISO8859_14
    },
    {
        HPDF_ENCODING_ISO8859_15,
        HPDF_BASE_ENCODING_WIN_ANSI,
        HPDF_UNICODE_MAP_ISO8859_15
    },
    {
        HPDF_ENCODING_ISO8859_16,
        HPDF_BASE_ENCODING_WIN_ANSI,
        HPDF_UNICODE_MAP_ISO8859_16
    },
    {
        HPDF_ENCODING_CP1250,
        HPDF_BASE_ENCODING_WIN_ANSI,
        HPDF_UNICODE_MAP_CP1250
    },
    {
        HPDF_ENCODING_CP1251,
        HPDF_BASE_ENCODING_WIN_ANSI,
        HPDF_UNICODE_MAP_CP1251
    },
    {
        HPDF_ENCODING_CP1252,
        HPDF_BASE_ENCODING_WIN_ANSI,
        HPDF_UNICODE_MAP_CP1252
    },
    {
        HPDF_ENCODING_CP1253,
        HPDF_BASE_ENCODING_WIN_ANSI,
        HPDF_UNICODE_MAP_CP1253
    },
    {
        HPDF_ENCODING_CP1254,
        HPDF_BASE_ENCODING_WIN_ANSI,
        HPDF_UNICODE_MAP_CP1254
    },
    {
        HPDF_ENCODING_CP1255,
        HPDF_BASE_ENCODING_WIN_ANSI,
        HPDF_UNICODE_MAP_CP1255
    },
    {
        HPDF_ENCODING_CP1256,
        HPDF_BASE_ENCODING_WIN_ANSI,
        HPDF_UNICODE_MAP_CP1256
    },
    {
        HPDF_ENCODING_CP1257,
        HPDF_BASE_ENCODING_WIN_ANSI,
        HPDF_UNICODE_MAP_CP1257
    },
    {
        HPDF_ENCODING_CP1258,
        HPDF_BASE_ENCODING_WIN_ANSI,
        HPDF_UNICODE_MAP_CP1258
    },
    {
        HPDF_ENCODING_KOI8_R,
        HPDF_BASE_ENCODING_WIN_ANSI,
        HPDF_UNICODE_MAP_KOI8_R
    },
    {
        NULL,
        HPDF_BASE_ENCODING_EOF,
        NULL
    }
};


/*---------------------------------------------------------------------------*/

const HPDF_BuiltinEncodingData*
HPDF_BasicEncoder_FindBuiltinData  (const char  *encoding_name);


void
HPDF_BasicEncoder_CopyMap  (HPDF_Encoder        encoder,
                            const HPDF_UNICODE  *map);


HPDF_STATUS
HPDF_BasicEncoder_OverrideMap  (HPDF_Encoder        encoder,
                                const HPDF_UNICODE  *map);


/*-- HPDF_Encoder ---------------------------------------*/

HPDF_Encoder
HPDF_BasicEncoder_New  (HPDF_MMgr        mmgr,
                        const char  *encoding_name)
{
    HPDF_Encoder encoder;
    HPDF_BasicEncoderAttr encoder_attr;
    const HPDF_BuiltinEncodingData *data;
    char *eptr;

    //HPDF_PTRACE((" HPDF_BasicEncoder_New\n"));

    if (mmgr == NULL)
        return NULL;

    data = HPDF_BasicEncoder_FindBuiltinData (encoding_name);
    if (!data->encoding_name) {
        HPDF_SetError (mmgr->error, HPDF_INVALID_ENCODING_NAME, 0);
        return NULL;
    }

    encoder = HPDF_GetMem (mmgr, sizeof(HPDF_Encoder_Rec));
    if (!encoder)
        return NULL;

    HPDF_MemSet (encoder, 0, sizeof(HPDF_Encoder_Rec));

    eptr = encoder->name + HPDF_LIMIT_MAX_NAME_LEN;
    HPDF_StrCpy (encoder->name, data->encoding_name, eptr);

    encoder->mmgr = mmgr;
    encoder->error = mmgr->error;
    encoder->type = HPDF_ENCODER_TYPE_SINGLE_BYTE;
    encoder->to_unicode_fn = HPDF_BasicEncoder_ToUnicode;
    encoder->write_fn = HPDF_BasicEncoder_Write;
    encoder->free_fn = HPDF_BasicEncoder_Free;

    encoder_attr = HPDF_GetMem(mmgr, sizeof(HPDF_BasicEncoderAttr_Rec));
    if (!encoder_attr) {
        HPDF_FreeMem (encoder->mmgr, encoder);
        return NULL;
    }

    encoder->sig_bytes = HPDF_ENCODER_SIG_BYTES;
    encoder->attr = encoder_attr;
    HPDF_MemSet (encoder_attr, 0, sizeof(HPDF_BasicEncoderAttr_Rec));

    encoder_attr->first_char = HPDF_BASIC_ENCODER_FIRST_CHAR;
    encoder_attr->last_char = HPDF_BASIC_ENCODER_LAST_CHAR;
    encoder_attr->has_differences = HPDF_FALSE;

    eptr = encoder_attr->base_encoding + HPDF_LIMIT_MAX_NAME_LEN;

    switch (data->base_encoding) {
        case HPDF_BASE_ENCODING_STANDARD:
            HPDF_StrCpy (encoder_attr->base_encoding,
                     HPDF_ENCODING_STANDARD,  eptr);
            HPDF_BasicEncoder_CopyMap (encoder, HPDF_UNICODE_MAP_STANDARD);
            break;
        case HPDF_BASE_ENCODING_WIN_ANSI:
            HPDF_StrCpy (encoder_attr->base_encoding,
                     HPDF_ENCODING_WIN_ANSI,  eptr);
            HPDF_BasicEncoder_CopyMap (encoder, HPDF_UNICODE_MAP_WIN_ANSI);
            break;
        case HPDF_BASE_ENCODING_MAC_ROMAN:
            HPDF_StrCpy (encoder_attr->base_encoding,
                     HPDF_ENCODING_MAC_ROMAN,  eptr);
            HPDF_BasicEncoder_CopyMap (encoder, HPDF_UNICODE_MAP_MAC_ROMAN);
            break;
        default:
            HPDF_StrCpy (encoder_attr->base_encoding,
                     HPDF_ENCODING_FONT_SPECIFIC,  eptr);
            HPDF_BasicEncoder_CopyMap (encoder,
                    HPDF_UNICODE_MAP_FONT_SPECIFIC);
    }

    if (data->ovewrride_map)
        HPDF_BasicEncoder_OverrideMap  (encoder, data->ovewrride_map);

    return encoder;
}




const HPDF_BuiltinEncodingData*
HPDF_BasicEncoder_FindBuiltinData  (const char  *encoding_name)
{
    HPDF_UINT i = 0;

    //HPDF_PTRACE((" HPDF_BasicEncoder_FindBuiltinData\n"));

    while (HPDF_BUILTIN_ENCODINGS[i].encoding_name) {
        if (HPDF_StrCmp (HPDF_BUILTIN_ENCODINGS[i].encoding_name,
                encoding_name) == 0)
            break;

        i++;
    }

    return &HPDF_BUILTIN_ENCODINGS[i];
}


HPDF_UNICODE
HPDF_BasicEncoder_ToUnicode (HPDF_Encoder     encoder,
                             HPDF_UINT16      code)
{
    HPDF_BasicEncoderAttr attr = (HPDF_BasicEncoderAttr)encoder->attr;

    if (code > 255)
        return 0;

    return attr->unicode_map[code];
}


HPDF_UNICODE
HPDF_Encoder_ToUnicode  (HPDF_Encoder     encoder,
                         HPDF_UINT16      code)
{
    return encoder->to_unicode_fn (encoder, code);
}


void
HPDF_BasicEncoder_CopyMap  (HPDF_Encoder        encoder,
                            const HPDF_UNICODE  *map)
{
    HPDF_UINT i;
    HPDF_UNICODE* dst = ((HPDF_BasicEncoderAttr)encoder->attr)->unicode_map +
        HPDF_BASIC_ENCODER_FIRST_CHAR;

    //HPDF_PTRACE((" HPDF_BasicEncoder_CopyMap\n"));

    for (i = 0; i <= HPDF_BASIC_ENCODER_LAST_CHAR -
            HPDF_BASIC_ENCODER_FIRST_CHAR; i++)
        *dst++ = *map++;
}

HPDF_STATUS
HPDF_BasicEncoder_OverrideMap  (HPDF_Encoder        encoder,
                                const HPDF_UNICODE  *map)
{
    HPDF_UINT i;
    HPDF_BasicEncoderAttr data = (HPDF_BasicEncoderAttr)encoder->attr;
    HPDF_UNICODE* dst;
    HPDF_BYTE* flgs;

    //HPDF_PTRACE ((" HPDF_BasicEncoder_OverrideMap\n"));

    if (data->has_differences)
        return HPDF_SetError (encoder->error, HPDF_INVALID_OPERATION, 0);

    dst = data->unicode_map + HPDF_BASIC_ENCODER_FIRST_CHAR;
    flgs = data->differences + HPDF_BASIC_ENCODER_FIRST_CHAR;

    for (i = 0; i <= HPDF_BASIC_ENCODER_LAST_CHAR -
            HPDF_BASIC_ENCODER_FIRST_CHAR; i++) {
        if (*map != *dst) {
            *dst = *map;
            *flgs = 1;
        }
        map++;
        dst++;
        flgs++;
    }
    data->has_differences = HPDF_TRUE;

    return HPDF_OK;
}

void
HPDF_Encoder_Free  (HPDF_Encoder  encoder)
{
    //HPDF_PTRACE ((" HPDF_Encoder_Free\n"));

    if (!encoder)
        return;

    if (encoder->free_fn)
        encoder->free_fn (encoder);
    HPDF_FreeMem (encoder->mmgr, encoder);
}


const char*
HPDF_UnicodeToGryphName  (HPDF_UNICODE  unicode)
{
    const HPDF_UnicodeGryphPair* map = HPDF_UNICODE_GRYPH_NAME_MAP;

    //HPDF_PTRACE ((" HPDF_UnicodeToGryphName\n"));

    while (map->unicode <= unicode) {
        if (map->unicode == unicode)
            return map->gryph_name;
        map++;
    }

    return HPDF_UNICODE_GRYPH_NAME_MAP[0].gryph_name;
}

HPDF_UNICODE
HPDF_GryphNameToUnicode  (const char  *gryph_name)
{
    const HPDF_UnicodeGryphPair* map = HPDF_UNICODE_GRYPH_NAME_MAP;

    //HPDF_PTRACE ((" HPDF_GryphNameToUnicode\n"));

    while (map->unicode != 0xFFFF) {
        if (HPDF_StrCmp (gryph_name, map->gryph_name) == 0)
            return map->unicode;
        map++;
    }

    return 0x0000;
}

void
HPDF_BasicEncoder_Free (HPDF_Encoder  encoder)
{
    //HPDF_PTRACE ((" HPDF_BasicEncoder_Free\n"));

    HPDF_FreeMem (encoder->mmgr, encoder->attr);
    encoder->attr = NULL;
}

HPDF_STATUS
HPDF_BasicEncoder_Write  (HPDF_Encoder  encoder,
                          HPDF_Stream   out)
{
    HPDF_STATUS ret;
    HPDF_BasicEncoderAttr attr = (HPDF_BasicEncoderAttr)encoder->attr;

    //HPDF_PTRACE ((" HPDF_BasicEncoder_Write\n"));

    /*  if HPDF_ENCODING_FONT_SPECIFIC is selected, no Encoding object will be "
     *  written.
     */
    if (HPDF_StrCmp (attr->base_encoding, HPDF_ENCODING_FONT_SPECIFIC) == 0)
        return HPDF_OK;

    /* if encoder has differences-data, encoding object is written as
       dictionary-object, otherwise it is written as name-object. */
    if (attr->has_differences == HPDF_TRUE) {
        ret = HPDF_Stream_WriteStr (out,
                "/Encoding <<\012"
                "/Type /Encoding\012"
                "/BaseEncoding ");
        if (ret != HPDF_OK)
            return ret;
    } else {
        ret = HPDF_Stream_WriteStr (out, "/Encoding ");
        if (ret != HPDF_OK)
            return ret;
    }

    ret = HPDF_Stream_WriteEscapeName (out, attr->base_encoding);
    if (ret != HPDF_OK)
        return ret;

    ret = HPDF_Stream_WriteStr (out, "\012");
    if (ret != HPDF_OK)
        return ret;

    /* write differences data */
    if (attr->has_differences == HPDF_TRUE) {
        HPDF_INT i;

        ret = HPDF_Stream_WriteStr (out, "/Differences [");
        if (ret != HPDF_OK)
            return ret;

        for (i = attr->first_char; i <= attr->last_char; i++) {
            if (attr->differences[i] == 1) {
                char tmp[HPDF_TEXT_DEFAULT_LEN];
                char* ptmp = &tmp[0]; // mjm
                const char* char_name = HPDF_UnicodeToGryphName (attr->unicode_map[i]);

                ptmp = HPDF_IToA (ptmp, i, tmp + HPDF_TMP_BUF_SIZ - 1);
                *ptmp++ = ' ';
                *ptmp++ = '/';
                ptmp = HPDF_StrCpy (ptmp, char_name, tmp + HPDF_TMP_BUF_SIZ - 1); // deadly! mjm
                *ptmp++ = ' ';
                *ptmp = 0;

                ret = HPDF_Stream_WriteStr (out, tmp);
                if (ret != HPDF_OK)
                    return ret;
            }
        }

        ret = HPDF_Stream_WriteStr (out, "]\012>>\012");
    }

    return ret;
}


HPDF_STATUS
HPDF_Encoder_Validate  (HPDF_Encoder  encoder)
{
    //HPDF_PTRACE ((" HPDF_Encoder_Validate\n"));

    if (!encoder || encoder->sig_bytes != HPDF_ENCODER_SIG_BYTES)
        return HPDF_FALSE;
    else
        return HPDF_TRUE;
}

void
HPDF_Encoder_SetParseText  (HPDF_Encoder        encoder,
                            HPDF_ParseText_Rec  *state,
                            const char          *text, // mjm  HPDF_BYTE
                            HPDF_UINT           len)
{
    //HPDF_PTRACE ((" HPDF_CMapEncoder_SetParseText\n"));

    state->text = text;
    state->index = 0;
    state->len = len;
    state->byte_type = HPDF_BYTE_TYPE_SINGLE;
}

HPDF_ByteType
HPDF_Encoder_ByteType  (HPDF_Encoder        encoder,
                        HPDF_ParseText_Rec  *state)
{
    //HPDF_PTRACE ((" HPDF_Encoder_ByteType\n"));

    if (encoder->byte_type_fn)
        return encoder->byte_type_fn (encoder, state);
    else
        return HPDF_BYTE_TYPE_SINGLE;
}

/*   End of file   */


