/*
 * << Haru Free PDF Library 2.0.0 >> -- hpdf_fontdef_type1.c
 *
 * Copyright (c) 1999-2004 Takeshi Kanno <takeshi_kanno@est.hi-ho.ne.jp>
 *
 * Permission to use, copy, modify, distribute and sell this software
 * and its documentation for any purpose is hereby granted without fee,
 * provided that the above copyright notice appear in all copies and
 * that both that copyright notice and this permission notice appear
 * in supporting documentation.
 * It is provided "as is" without express or implied warranty.
 *
 */

#include "hpdf_conf.h"
#include "hpdf_utils.h"
#include "hpdf_fontdef.h"

static void
FreeWidth  (HPDF_FontDef  fontdef);


static void
FreeFunc (HPDF_FontDef  fontdef);


/*---------------------------------------------------------------------------*/

static void
FreeWidth  (HPDF_FontDef  fontdef)
{
    HPDF_Type1FontDefAttr attr = (HPDF_Type1FontDefAttr)fontdef->attr;

    //HPDF_PTRACE ((" FreeWidth\n"));

    HPDF_FreeMem (fontdef->mmgr, attr->widths);
    attr->widths = NULL;

    fontdef->valid = HPDF_FALSE;
}

HPDF_FontDef
HPDF_Type1FontDef_New  (HPDF_MMgr  mmgr)
{
    HPDF_FontDef fontdef;
    HPDF_Type1FontDefAttr fontdef_attr;

    //HPDF_PTRACE ((" HPDF_Type1FontDef_New\n"));

    if (!mmgr)
        return NULL;

    fontdef = HPDF_GetMem (mmgr, sizeof(HPDF_FontDef_Rec));
    if (!fontdef)
        return NULL;

    fontdef->sig_bytes = HPDF_FONTDEF_SIG_BYTES;
    fontdef->base_font[0] = 0;
    fontdef->mmgr = mmgr;
    fontdef->error = mmgr->error;
    fontdef->type = HPDF_FONTDEF_TYPE_TYPE1;
    fontdef->clean_fn = NULL;
    fontdef->free_fn = FreeFunc;
    fontdef->descriptor = NULL;
    fontdef->valid = HPDF_FALSE;

    fontdef_attr = HPDF_GetMem (mmgr, sizeof(HPDF_Type1FontDefAttr_Rec));
    if (!fontdef_attr) {
        HPDF_FreeMem (fontdef->mmgr, fontdef);
        return NULL;
    }

    fontdef->attr = fontdef_attr;
    HPDF_MemSet ((HPDF_BYTE *)fontdef_attr, 0, sizeof(HPDF_Type1FontDefAttr_Rec));
    fontdef->flags = HPDF_FONT_STD_CHARSET;

    return fontdef;
}

HPDF_STATUS
HPDF_Type1FontDef_SetWidths  (HPDF_FontDef          fontdef,
                              const HPDF_CharData*  widths)
{
    const HPDF_CharData* src = widths;
    HPDF_Type1FontDefAttr attr = (HPDF_Type1FontDefAttr)fontdef->attr;
    HPDF_CharData* dst;
    HPDF_UINT i = 0;

    //HPDF_PTRACE ((" HPDF_Type1FontDef_SetWidths\n"));

    FreeWidth (fontdef);

    while (src->unicode != 0xFFFF) {
        src++;
        i++;
    }

    attr->widths_count = i;
    //printuf("HPDF_CharData=0x%x\n",sizeof(HPDF_CharData));
    //printuf("i=0x%x\n",i);
    
    dst = (HPDF_CharData*)HPDF_GetMem (fontdef->mmgr, sizeof(HPDF_CharData) * attr->widths_count);
    if (dst == NULL)
        return HPDF_Error_GetCode (fontdef->error);

    HPDF_MemSet (dst, 0, sizeof(HPDF_CharData) * attr->widths_count);
    attr->widths = dst;

    src = widths;
    for (i = 0; i < attr->widths_count; i++) {
        dst->char_cd = src->char_cd;
        dst->unicode = src->unicode;
        dst->width = src->width;
        if (dst->unicode == 0x0020) {
            fontdef->missing_width = src->width;
        }

        src++;
        dst++;
    }

    return HPDF_OK;
}

HPDF_INT16
HPDF_Type1FontDef_GetWidth  (HPDF_FontDef  fontdef,
                             HPDF_UNICODE  unicode)
{
    HPDF_Type1FontDefAttr attr = (HPDF_Type1FontDefAttr)fontdef->attr;
    HPDF_CharData *cdata = attr->widths;
    HPDF_UINT i;

    //HPDF_PTRACE ((" HPDF_Type1FontDef_GetWidth\n"));

    for (i = 0; i < attr->widths_count; i++) {
        if (cdata->unicode == unicode)
            return cdata->width;
        cdata++;
    }

    return fontdef->missing_width;
}

static void
FreeFunc (HPDF_FontDef  fontdef)
{
    HPDF_Type1FontDefAttr attr = (HPDF_Type1FontDefAttr)fontdef->attr;

    //HPDF_PTRACE ((" FreeFunc\n"));

    if (attr->char_set)
        HPDF_FreeMem (fontdef->mmgr, attr->char_set);

    if (attr->font_data)
        HPDF_Stream_Free (attr->font_data);

    HPDF_FreeMem (fontdef->mmgr, attr->widths);
    HPDF_FreeMem (fontdef->mmgr, attr);
}

