/*
 * << Haru Free PDF Library 2.0.3 >> -- hpdf_types.h
 *
 * URL http://libharu.sourceforge.net/
 *
 * Copyright (c) 1999-2006 Takeshi Kanno
 *
 * Permission to use, copy, modify, distribute and sell this software
 * and its documentation for any purpose is hereby granted without fee,
 * provided that the above copyright notice appear in all copies and
 * that both that copyright notice and this permission notice appear
 * in supporting documentation.
 * It is provided "as is" without express or implied warranty.
 *
 */

#ifndef _HPDF_TYPES_H
#define _HPDF_TYPES_H

#ifndef HPDF_STDCALL
#ifdef HPDF_DLL_MAKE
#define HPDF_STDCALL __stdcall
#else
#ifdef HPDF_DLL
#define HPDF_STDCALL __stdcall
#else
#define HPDF_STDCALL
#endif
#endif
#endif

#include <stdlib.h>

#include "GenericTypeDefs.h"
#include "Compiler.h"

#ifdef __cplusplus
extern "C" {
#endif

/*----------------------------------------------------------------------------*/
/*----- type definition ------------------------------------------------------*/
/*  native OS integer types */
typedef  INT16               HPDF_INT;
typedef  UINT16              HPDF_UINT;


/*  32bit integer types
 */
typedef  INT32               HPDF_INT32;
typedef  UINT32              HPDF_UINT32;
typedef  HPDF_INT32          int32;
typedef  HPDF_UINT32         uint32;


/*  16bit integer types
 */
typedef  INT16               HPDF_INT16;
typedef  UINT16              HPDF_UINT16;
typedef  HPDF_INT16          int16;
typedef  HPDF_UINT16         uint16;

/*  8bit integer types
 */
typedef  INT8                HPDF_INT8;
typedef  UINT8               HPDF_UINT8;
typedef  HPDF_INT8           int8;
typedef  HPDF_UINT8          uint8;

/*  8bit binary types
 */
typedef  UINT8               HPDF_BYTE; //mjm char


/*  float type (32bit IEEE754)
 */
typedef  float               HPDF_REAL;


/*  double type (64bit IEEE754)
 */
typedef  double              HPDF_DOUBLE;


/*  boolean type (0: False, !0: True)
 */
typedef  INT32          HPDF_BOOL;


/*  error-no type (32bit unsigned integer)
 */
typedef  INT32          HPDF_STATUS;


/*  charactor-code type (16bit)
 */
typedef  HPDF_UINT16         HPDF_CID;
typedef  HPDF_UINT16         HPDF_UNICODE;


/*  HPDF_Point struct
 */
typedef  struct  _HPDF_Point {
    HPDF_REAL  x;
    HPDF_REAL  y;
} HPDF_Point;

typedef  struct _HPDF_Rect {
    HPDF_REAL  left;
    HPDF_REAL  bottom;
    HPDF_REAL  right;
    HPDF_REAL  top;
} HPDF_Rect;

typedef struct _HPDF_Rect HPDF_Box;

/* HPDF_Date struct
 */
typedef  struct  _HPDF_Date {
    HPDF_INT    year;
    HPDF_INT    month;
    HPDF_INT    day;
    HPDF_INT    hour;
    HPDF_INT    minutes;
    HPDF_INT    seconds;
    char        ind;
    HPDF_INT    off_hour;
    HPDF_INT    off_minutes;
} HPDF_Date;


typedef enum _HPDF_InfoType {
    /* date-time type parameters */
    HPDF_INFO_CREATION_DATE = 0,
    HPDF_INFO_MOD_DATE,

    /* string type parameters */
    HPDF_INFO_AUTHOR,
    HPDF_INFO_CREATOR,
    HPDF_INFO_PRODUCER,
    HPDF_INFO_TITLE,
    HPDF_INFO_SUBJECT,
    HPDF_INFO_KEYWORDS,
    HPDF_INFO_EOF
} HPDF_InfoType;


typedef enum _HPDF_PdfVer {
    HPDF_VER_12 = 0,
    HPDF_VER_13,
    HPDF_VER_14,
    HPDF_VER_15,
    HPDF_VER_16,
    HPDF_VER_EOF
} HPDF_PDFVer;

typedef enum  _HPDF_EncryptMode {
    HPDF_ENCRYPT_R2    = 2,
    HPDF_ENCRYPT_R3    = 3
} HPDF_EncryptMode;


typedef void
(HPDF_STDCALL *HPDF_Error_Handler)  (HPDF_STATUS   error_no,
                                     HPDF_STATUS   detail_no,
                                     void         *user_data);

typedef void*
(HPDF_STDCALL *HPDF_Alloc_Func)  (HPDF_UINT  size);


typedef void
(HPDF_STDCALL *HPDF_Free_Func)  (void  *aptr);


/*---------------------------------------------------------------------------*/
/*------ text width struct --------------------------------------------------*/

typedef struct _HPDF_TextWidth {
    HPDF_UINT numchars;

    /* don't use this value (it may be change in the feature).
       use numspace as alternated. */
    HPDF_UINT numwords;

    HPDF_UINT width;
    HPDF_UINT numspace;
} HPDF_TextWidth;


/*---------------------------------------------------------------------------*/
/*------ dash mode ----------------------------------------------------------*/

typedef struct _HPDF_DashMode {
    HPDF_UINT16  ptn[8];
    HPDF_UINT    num_ptn;
    HPDF_UINT    phase;
} HPDF_DashMode;


/*---------------------------------------------------------------------------*/
/*----- HPDF_TransMatrix struct ---------------------------------------------*/

typedef struct _HPDF_TransMatrix {
    HPDF_REAL   a;
    HPDF_REAL   b;
    HPDF_REAL   c;
    HPDF_REAL   d;
    HPDF_REAL   x;
    HPDF_REAL   y;
} HPDF_TransMatrix;


/*---------------------------------------------------------------------------*/

typedef enum _HPDF_ColorSpace {
    HPDF_CS_DEVICE_GRAY = 0,
    HPDF_CS_DEVICE_RGB,
    HPDF_CS_DEVICE_CMYK,
    HPDF_CS_CAL_GRAY,
    HPDF_CS_CAL_RGB,
    HPDF_CS_LAB,
    HPDF_CS_ICC_BASED,
    HPDF_CS_SEPARATION,
    HPDF_CS_DEVICE_N,
    HPDF_CS_INDEXED,
    HPDF_CS_PATTERN,
    HPDF_CS_EOF
} HPDF_ColorSpace;

/*---------------------------------------------------------------------------*/
/*----- HPDF_RGBColor struct ------------------------------------------------*/

typedef struct _HPDF_RGBColor {
    HPDF_REAL   r;
    HPDF_REAL   g;
    HPDF_REAL   b;
} HPDF_RGBColor;

/*---------------------------------------------------------------------------*/
/*----- HPDF_CMYKColor struct -----------------------------------------------*/

typedef struct _HPDF_CMYKColor {
    HPDF_REAL   c;
    HPDF_REAL   m;
    HPDF_REAL   y;
    HPDF_REAL   k;
} HPDF_CMYKColor;

/*---------------------------------------------------------------------------*/
/*------ The line cap style -------------------------------------------------*/

typedef enum _HPDF_LineCap {
    HPDF_BUTT_END = 0,
    HPDF_ROUND_END,
    HPDF_PROJECTING_SCUARE_END,
    HPDF_LINECAP_EOF
} HPDF_LineCap;

/*----------------------------------------------------------------------------*/
/*------ The line join style -------------------------------------------------*/

typedef enum _HPDF_LineJoin {
    HPDF_MITER_JOIN = 0,
    HPDF_ROUND_JOIN,
    HPDF_BEVEL_JOIN,
    HPDF_LINEJOIN_EOF
} HPDF_LineJoin;

/*----------------------------------------------------------------------------*/
/*------ The text rendering mode ---------------------------------------------*/

typedef enum _HPDF_TextRenderingMode {
    HPDF_FILL = 0,
    HPDF_STROKE,
    HPDF_FILL_THEN_STROKE,
    HPDF_INVISIBLE,
    HPDF_FILL_CLIPPING,
    HPDF_STROKE_CLIPPING,
    HPDF_FILL_STROKE_CLIPPING,
    HPDF_CLIPPING,
    HPDF_RENDERING_MODE_EOF
} HPDF_TextRenderingMode;


typedef enum _HPDF_WritingMode {
    HPDF_WMODE_HORIZONTAL = 0,
    HPDF_WMODE_VERTICAL,
    HPDF_WMODE_EOF
} HPDF_WritingMode;


typedef enum _HPDF_PageLayout {
    HPDF_PAGE_LAYOUT_SINGLE = 0,
    HPDF_PAGE_LAYOUT_ONE_COLUMN,
    HPDF_PAGE_LAYOUT_TWO_COLUMN_LEFT,
    HPDF_PAGE_LAYOUT_TWO_COLUMN_RIGHT,
    HPDF_PAGE_LAYOUT_EOF
} HPDF_PageLayout;


typedef enum _HPDF_PageMode {
    HPDF_PAGE_MODE_USE_NONE = 0,
    HPDF_PAGE_MODE_USE_OUTLINE,
    HPDF_PAGE_MODE_USE_THUMBS,
    HPDF_PAGE_MODE_FULL_SCREEN,
    HPDF_PAGE_MODE_EOF
} HPDF_PageMode;


typedef enum _HPDF_PageNumStyle {
    HPDF_PAGE_NUM_STYLE_DECIMAL = 0,
    HPDF_PAGE_NUM_STYLE_UPPER_ROMAN,
    HPDF_PAGE_NUM_STYLE_LOWER_ROMAN,
    HPDF_PAGE_NUM_STYLE_UPPER_LETTERS,
    HPDF_PAGE_NUM_STYLE_LOWER_LETTERS,
    HPDF_PAGE_NUM_STYLE_EOF
} HPDF_PageNumStyle;


/*----- blend modes ----------------------------------------------------------*/

typedef enum _HPDF_BlendMode {
    HPDF_BM_NORMAL,
    HPDF_BM_MULTIPLY,
    HPDF_BM_SCREEN,
    HPDF_BM_OVERLAY,
    HPDF_BM_DARKEN,
    HPDF_BM_LIGHTEN,
    HPDF_BM_COLOR_DODGE,
    HPDF_BM_COLOR_BUM,
    HPDF_BM_HARD_LIGHT,
    HPDF_BM_SOFT_LIGHT,
    HPDF_BM_DIFFERENCE,
    HPDF_BM_EXCLUSHON,
    HPDF_BM_EOF
} HPDF_BlendMode;


typedef enum  _HPDF_EncoderType {
    HPDF_ENCODER_TYPE_SINGLE_BYTE,
    HPDF_ENCODER_TYPE_DOUBLE_BYTE,
    HPDF_ENCODER_TYPE_UNINITIALIZED,
    HPDF_ENCODER_UNKNOWN
} HPDF_EncoderType;


typedef enum _HPDF_ByteType {
    HPDF_BYTE_TYPE_SINGLE = 0,
    HPDF_BYTE_TYPE_LEAD,
    HPDF_BYTE_TYPE_TRIAL,
    HPDF_BYTE_TYPE_UNKNOWN
} HPDF_ByteType;


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _HPDF_TYPES_H */

