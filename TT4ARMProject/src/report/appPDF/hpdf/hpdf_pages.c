/*
 * << Haru Free PDF Library 2.0.3 >> -- hpdf_pages.c
 *
 * Copyright (c) 1999-2006 Takeshi Kanno <takeshi_kanno@est.hi-ho.ne.jp>
 *
 * Permission to use, copy, modify, distribute and sell this software
 * and its documentation for any purpose is hereby granted without fee,
 * provided that the above copyright notice appear in all copies and
 * that both that copyright notice and this permission notice appear
 * in supporting documentation.
 * It is provided "as is" without express or implied warranty.
 *
 */

#include "hpdf_conf.h"
#include "hpdf_utils.h"
#include "hpdf.h"
#include "hpdf_destination.h"

/*----------------------------------------------------------------------------*/

typedef struct _HPDF_PageSizeValue {
    HPDF_REAL   x;
    HPDF_REAL   y;
} HPDF_PageSizeValue;


static const HPDF_RGBColor DEF_RGB_COLOR = {0, 0, 0};

static const HPDF_CMYKColor DEF_CMYK_COLOR = {0, 0, 0, 0};

static HPDF_STATUS
Pages_BeforeWrite  (HPDF_Dict    obj);


static HPDF_STATUS
Page_BeforeWrite  (HPDF_Dict    obj);


static void
Page_OnFree  (HPDF_Dict  obj);


static HPDF_STATUS
AddResource  (HPDF_Page  page);



static HPDF_UINT
GetPageCount  (HPDF_Dict    pages);

static const char *HPDF_INHERITABLE_ENTRIES[5] = {
                        "Resources",
                        "MediaBox",
                        "CropBox",
                        "Rotate",
                        NULL
                        };


/*----------------------------------------------------------------------------*/
/*----- HPDF_Pages -----------------------------------------------------------*/

HPDF_Pages
HPDF_Pages_New  (HPDF_MMgr   mmgr,
                 HPDF_Pages  parent,
                 HPDF_Xref   xref)
{
    HPDF_STATUS ret = HPDF_OK;
    HPDF_Pages pages;


    pages = HPDF_Dict_New (mmgr);
    if (!pages)
        return NULL;

    pages->header.obj_class |= HPDF_OSUBCLASS_PAGES;
    pages->before_write_fn = Pages_BeforeWrite;

    if (HPDF_Xref_Add (xref, pages) != HPDF_OK)
        return NULL;

    /* add requiered elements */
    ret += HPDF_Dict_AddName (pages, "Type", "Pages");
    ret += HPDF_Dict_Add (pages, "Kids", HPDF_Array_New (pages->mmgr));
    ret += HPDF_Dict_Add (pages, "Count", HPDF_Number_New (pages->mmgr, 0));

    if (ret == HPDF_OK && parent)
        ret += HPDF_Pages_AddKids (parent, pages);

    if (ret != HPDF_OK)
        return NULL;

    return pages;
}


HPDF_STATUS
HPDF_Pages_AddKids  (HPDF_Pages  parent,
                     HPDF_Dict   kid)
{
    HPDF_Array kids;
    HPDF_STATUS ret;

    //HPDF_PTRACE((" HPDF_Pages_AddKids\n"));

    if (HPDF_Dict_GetItem (kid, "Parent", HPDF_OCLASS_DICT))
        return HPDF_SetError (parent->error, HPDF_PAGE_CANNOT_SET_PARENT, 0);

    if ((ret = HPDF_Dict_Add (kid, "Parent", parent)) != HPDF_OK)
        return ret;

    kids = (HPDF_Array )HPDF_Dict_GetItem (parent, "Kids", HPDF_OCLASS_ARRAY);
    if (!kids)
        return HPDF_SetError (parent->error, HPDF_PAGES_MISSING_KIDS_ENTRY, 0);

    if (kid->header.obj_class == (HPDF_OCLASS_DICT | HPDF_OSUBCLASS_PAGE)) {
        HPDF_PageAttr attr = (HPDF_PageAttr)kid->attr;

        attr->parent = parent;
    }

    return HPDF_Array_Add (kids, kid);
}


HPDF_STATUS
HPDF_Page_InsertBefore  (HPDF_Page   page,
                         HPDF_Page   target)
{
    HPDF_Page parent;
    HPDF_Array kids;
    HPDF_STATUS ret;
    HPDF_PageAttr attr;

    //HPDF_PTRACE((" HPDF_Page_InsertBefore\n"));

    attr = (HPDF_PageAttr )target->attr;
    parent = attr->parent;

    if (!parent)
        return HPDF_SetError (parent->error, HPDF_PAGE_CANNOT_SET_PARENT, 0);

    if (HPDF_Dict_GetItem (page, "Parent", HPDF_OCLASS_DICT))
        return HPDF_SetError (parent->error, HPDF_PAGE_CANNOT_SET_PARENT, 0);

    if ((ret = HPDF_Dict_Add (page, "Parent", parent)) != HPDF_OK)
        return ret;

    kids = (HPDF_Array )HPDF_Dict_GetItem (parent, "Kids", HPDF_OCLASS_ARRAY);
    if (!kids)
        return HPDF_SetError (parent->error, HPDF_PAGES_MISSING_KIDS_ENTRY, 0);

    attr = (HPDF_PageAttr)page->attr;
    attr->parent = parent;

    return HPDF_Array_Insert (kids, target, page);
}


HPDF_STATUS
Pages_BeforeWrite  (HPDF_Dict    obj)
{
    HPDF_Array kids = (HPDF_Array )HPDF_Dict_GetItem (obj, "Kids",
                    HPDF_OCLASS_ARRAY);
    HPDF_Number count = (HPDF_Number)HPDF_Dict_GetItem (obj, "Count",
                    HPDF_OCLASS_NUMBER);
    HPDF_STATUS ret;

    //HPDF_PTRACE((" HPDF_Pages_BeforeWrite\n"));

    if (!kids)
        return HPDF_SetError (obj->error, HPDF_PAGES_MISSING_KIDS_ENTRY, 0);

    if (count)
        count->value = GetPageCount (obj);
    else {
        count = HPDF_Number_New (obj->mmgr, GetPageCount (obj));
        if (!count)
            return HPDF_Error_GetCode (obj->error);

        if ((ret = HPDF_Dict_Add (obj, "Count", count)) != HPDF_OK)
            return ret;
    }

    return HPDF_OK;
}


static HPDF_STATUS
Page_BeforeWrite  (HPDF_Dict    obj)
{
    HPDF_STATUS ret;
    HPDF_Page page = (HPDF_Page)obj;
    HPDF_PageAttr attr = (HPDF_PageAttr)obj->attr;

    //HPDF_PTRACE((" HPDF_Page_BeforeWrite\n"));

    if (attr->gmode == HPDF_GMODE_PATH_OBJECT) {
        //HPDF_PTRACE((" HPDF_Page_BeforeWrite warning path object is not" " end\n"));

        if ((ret = HPDF_Page_EndPath (page)) != HPDF_OK)
           return ret;
    }

    if (attr->gmode == HPDF_GMODE_TEXT_OBJECT) {
        //HPDF_PTRACE((" HPDF_Page_BeforeWrite warning text block is not end\n"));

        if ((ret = HPDF_Page_EndText (page)) != HPDF_OK)
            return ret;
    }

    if (attr->gstate)
        while (attr->gstate->prev) {
            if ((ret = HPDF_Page_GRestore (page)) != HPDF_OK)
                return ret;
        }

    return HPDF_OK;
}


static HPDF_UINT
GetPageCount  (HPDF_Dict    pages)
{
    HPDF_UINT i;
    HPDF_UINT count = 0;
    HPDF_Array kids = (HPDF_Array)HPDF_Dict_GetItem (pages, "Kids",
            HPDF_OCLASS_ARRAY);

    //HPDF_PTRACE((" GetPageCount\n"));

    if (!kids)
        return 0;

    for (i = 0; i < kids->list->count; i++) {
        void *obj = HPDF_Array_GetItem (kids, i, HPDF_OCLASS_DICT);
        HPDF_Obj_Header *header = (HPDF_Obj_Header *)obj;

        if (header->obj_class == (HPDF_OCLASS_DICT | HPDF_OSUBCLASS_PAGES))
            count += GetPageCount ((HPDF_Dict)obj);
        else if (header->obj_class == (HPDF_OCLASS_DICT | HPDF_OSUBCLASS_PAGE))
            count += 1;
    }

    return count;
}


HPDF_BOOL
HPDF_Pages_Validate  (HPDF_Pages  pages)
{
    HPDF_Obj_Header *header = (HPDF_Obj_Header *)pages;

    //HPDF_PTRACE((" HPDF_Pages_Validate\n"));

    if (!pages || header->obj_class != (HPDF_OCLASS_DICT |
                HPDF_OSUBCLASS_PAGES))
        return HPDF_FALSE;

    return HPDF_TRUE;
}


/*----------------------------------------------------------------------------*/
/*----- HPDF_Page ------------------------------------------------------------*/


HPDF_Page
HPDF_Page_New  (HPDF_MMgr   mmgr,
                HPDF_Xref   xref)
{
    HPDF_STATUS ret;
    HPDF_PageAttr attr;
    HPDF_Page page;

    //HPDF_PTRACE((" HPDF_Page_New\n"));

    page = HPDF_Dict_New (mmgr);
    if (!page)
        return NULL;

    page->header.obj_class |= HPDF_OSUBCLASS_PAGE;
    page->free_fn = Page_OnFree;
    page->before_write_fn = Page_BeforeWrite;

    attr = (HPDF_PageAttr)HPDF_GetMem (page->mmgr, sizeof(HPDF_PageAttr_Rec));
    if (!attr) {
        HPDF_Dict_Free (page);
        return NULL;
    }

    page->attr = attr;
    HPDF_MemSet (attr, 0, sizeof(HPDF_PageAttr_Rec));
    attr->gmode = HPDF_GMODE_PAGE_DESCRIPTION;
    attr->cur_pos = HPDF_ToPoint (0, 0);
    attr->text_pos = HPDF_ToPoint (0, 0);

    ret = HPDF_Xref_Add (xref, page);
    if (ret != HPDF_OK)
        return NULL;

    attr->gstate = HPDF_GState_New (page->mmgr, NULL);
    attr->contents = HPDF_DictStream_New (page->mmgr, xref);

    if (!attr->gstate || !attr->contents)
        return NULL;

    attr->stream = attr->contents->stream;
    attr->xref = xref;

    /* add requiered elements */
    ret += HPDF_Dict_AddName (page, "Type", "Page");
    ret += HPDF_Dict_Add (page, "MediaBox", HPDF_Box_Array_New (page->mmgr,
                HPDF_ToBox (0, 0, HPDF_DEF_PAGE_WIDTH, HPDF_DEF_PAGE_HEIGHT)));
    ret += HPDF_Dict_Add (page, "Contents", attr->contents);

    ret += AddResource (page);

    if (ret != HPDF_OK)
        return NULL;

    return page;
}




static void
Page_OnFree  (HPDF_Dict  obj)
{
    HPDF_PageAttr attr = (HPDF_PageAttr)obj->attr;

    //HPDF_PTRACE((" HPDF_Page_OnFree\n"));

    if (attr) {
        if (attr->gstate)
            HPDF_GState_Free (obj->mmgr, attr->gstate);

        HPDF_FreeMem (obj->mmgr, attr);
    }
}


HPDF_STATUS
HPDF_Page_CheckState  (HPDF_Page  page,
                       HPDF_UINT  mode)
{
    if (!page)
        return HPDF_INVALID_OBJECT;

    if (page->header.obj_class != (HPDF_OSUBCLASS_PAGE | HPDF_OCLASS_DICT))
        return HPDF_INVALID_PAGE;

    if (!(((HPDF_PageAttr)page->attr)->gmode & mode))
        return HPDF_RaiseError (page->error, HPDF_PAGE_INVALID_GMODE, 0);

    return HPDF_OK;
}


void*
HPDF_Page_GetInheritableItem  (HPDF_Page          page,
                               const char   *key,
                               HPDF_UINT16        obj_class)
{
    HPDF_BOOL chk = HPDF_FALSE;
    HPDF_INT i = 0;
    void * obj;

    //HPDF_PTRACE((" HPDF_Page_GetInheritableItem\n"));

    /* check whether the specified key is valid */
    while (HPDF_INHERITABLE_ENTRIES[i]) {
        if (HPDF_StrCmp (key, HPDF_INHERITABLE_ENTRIES[i]) == 0) {
            chk = HPDF_TRUE;
            break;
        }
        i++;
    }

    /* the key is not inheritable */
    if (chk != HPDF_TRUE) {
        HPDF_SetError (page->error, HPDF_INVALID_PARAMETER, 0);
        return NULL;
    }

    obj = HPDF_Dict_GetItem (page, key, obj_class);

    /* if resources of the object is NULL, search resources of parent
     * pages recursivly
     */
    if (!obj) {
        HPDF_Pages pages = HPDF_Dict_GetItem (page, "Parent", HPDF_OCLASS_DICT);
        while (pages) {
            obj = HPDF_Dict_GetItem (page, key, obj_class);

            if (obj)
                break;

            pages = HPDF_Dict_GetItem (pages, "Parent", HPDF_OCLASS_DICT);
        }
    }

    return obj;
}


HPDF_STATUS
AddResource  (HPDF_Page  page)
{
    HPDF_STATUS ret = HPDF_OK;
    HPDF_Dict resource;
    HPDF_Array procset;

    //HPDF_PTRACE((" HPDF_Page_AddResource\n"));

    resource = HPDF_Dict_New (page->mmgr);
    if (!resource)
        return HPDF_Error_GetCode (page->error);

    /* althoth ProcSet-entry is obsolete, add it to resouce for
     * compatibility
     */

    ret += HPDF_Dict_Add (page, "Resources", resource);

    procset = HPDF_Array_New (page->mmgr);
    if (!procset)
        return HPDF_Error_GetCode (page->error);

    ret += HPDF_Dict_Add (resource, "ProcSet", procset);
    ret += HPDF_Array_Add (procset, HPDF_Name_New (page->mmgr, "PDF"));
    ret += HPDF_Array_Add (procset, HPDF_Name_New (page->mmgr, "Text"));
    ret += HPDF_Array_Add (procset, HPDF_Name_New (page->mmgr, "ImageB"));
    ret += HPDF_Array_Add (procset, HPDF_Name_New (page->mmgr, "ImageC"));
    ret += HPDF_Array_Add (procset, HPDF_Name_New (page->mmgr, "ImageI"));

    return ret;
}

HPDF_EXPORT(HPDF_Point)
HPDF_Page_GetCurrentTextPos (HPDF_Page  page)
{
    HPDF_Point pos = {0, 0};

//    HPDF_PTRACE((" HPDF_Page_GetCurrentTextPos\n"));

    if (HPDF_Page_Validate (page)) {
        HPDF_PageAttr attr = (HPDF_PageAttr)page->attr;

        if (attr->gmode & HPDF_GMODE_TEXT_OBJECT)
            pos = attr->text_pos;
    }

    return pos;
}

const char*
HPDF_Page_GetLocalFontName  (HPDF_Page  page,
                             HPDF_Font  font)
{
    HPDF_PageAttr attr = (HPDF_PageAttr )page->attr;
    const char *key;

    //HPDF_PTRACE((" HPDF_Page_GetLocalFontName\n"));

    /* whether check font-resource exists.  when it does not exists,
     * create font-resource
     * 2006.07.21 Fixed a problem which may cause a memory leak.
     */
    if (!attr->fonts) {
        HPDF_Dict resources;
        HPDF_Dict fonts;

        resources = HPDF_Page_GetInheritableItem (page, "Resources",
                        HPDF_OCLASS_DICT);
        if (!resources)
            return NULL;

        fonts = HPDF_Dict_New (page->mmgr);
        if (!fonts)
            return NULL;

        if (HPDF_Dict_Add (resources, "Font", fonts) != HPDF_OK)
            return NULL;

        attr->fonts = fonts;
    }

    /* search font-object from font-resource */
    key = HPDF_Dict_GetKeyByObj (attr->fonts, font);
    if (!key) {
        /* if the font is not resisterd in font-resource, register font to
         * font-resource.
         */
        char fontName[HPDF_LIMIT_MAX_NAME_LEN + 1];
        char *ptr;
        char *end_ptr = fontName + HPDF_LIMIT_MAX_NAME_LEN;

        ptr = (char*)HPDF_StrCpy (fontName, "F", end_ptr);
        HPDF_IToA (ptr, attr->fonts->list->count + 1, end_ptr);

        if (HPDF_Dict_Add (attr->fonts, fontName, font) != HPDF_OK)
            return NULL;

        key = HPDF_Dict_GetKeyByObj (attr->fonts, font);
    }

    return key;
}


HPDF_Box
HPDF_Page_GetMediaBox  (HPDF_Page   page)
{
    HPDF_Box media_box = {0, 0, 0, 0};

    //HPDF_PTRACE((" HPDF_Page_GetMediaBox\n"));

    if (HPDF_Page_Validate (page)) {
        HPDF_Array array = HPDF_Page_GetInheritableItem (page, "MediaBox",
                        HPDF_OCLASS_ARRAY);

        if (array) {
            HPDF_Real r;

            r = HPDF_Array_GetItem (array, 0, HPDF_OCLASS_REAL);
            if (r)
                media_box.left = r->value;

            r = HPDF_Array_GetItem (array, 1, HPDF_OCLASS_REAL);
            if (r)
                media_box.bottom = r->value;

            r = HPDF_Array_GetItem (array, 2, HPDF_OCLASS_REAL);
            if (r)
                media_box.right = r->value;

            r = HPDF_Array_GetItem (array, 3, HPDF_OCLASS_REAL);
            if (r)
                media_box.top = r->value;

            HPDF_CheckError (page->error);
        } else HPDF_RaiseError (page->error, HPDF_PAGE_CANNOT_FIND_OBJECT, 0);
    }

    return media_box;
}

HPDF_EXPORT(HPDF_REAL)
HPDF_Page_TextWidth  (HPDF_Page        page,
                      const char      *text)
{
    HPDF_PageAttr attr;
    HPDF_TextWidth tw;
    HPDF_REAL ret = 0;
    HPDF_UINT32 len = HPDF_StrLen(text, HPDF_LIMIT_MAX_STRING_LEN + 1); // mjm HPDF_UINT

    //HPDF_PTRACE((" HPDF_Page_TextWidth\n"));

    if (!HPDF_Page_Validate (page) || len == 0)
        return 0;

    attr = (HPDF_PageAttr )page->attr;

    /* no font exists */
    if (!attr->gstate->font) {
        HPDF_RaiseError (page->error, HPDF_PAGE_FONT_NOT_FOUND, 0);
        return 0;
    }

    tw = HPDF_Font_TextWidth (attr->gstate->font, text, len); // mjm const HPDF_BYTE

    ret += attr->gstate->word_space * tw.numspace;
    ret += tw.width * attr->gstate->font_size  / 1000;
    ret += attr->gstate->char_space * tw.numchars;

    HPDF_CheckError (page->error);

    return ret;
}


HPDF_EXPORT(HPDF_REAL)
HPDF_Page_GetWidth  (HPDF_Page    page)
{
    return HPDF_Page_GetMediaBox (page).right;
}


HPDF_EXPORT(HPDF_REAL)
HPDF_Page_GetHeight  (HPDF_Page    page)
{
    return HPDF_Page_GetMediaBox (page).top;
}


HPDF_STATUS
HPDF_Page_SetBoxValue (HPDF_Page          page,
                       const char   *name,
                       HPDF_UINT          index,
                       HPDF_REAL          value)
{
    HPDF_Real r;
    HPDF_Array array;

    //HPDF_PTRACE((" HPDF_Page_SetBoxValue\n"));

    if (!HPDF_Page_Validate (page))
        return HPDF_INVALID_PAGE;

    array = HPDF_Page_GetInheritableItem (page, name, HPDF_OCLASS_ARRAY);
    if (!array)
        return HPDF_SetError (page->error, HPDF_PAGE_CANNOT_FIND_OBJECT, 0);

    r = HPDF_Array_GetItem (array, index, HPDF_OCLASS_REAL);
    if (!r)
        return HPDF_SetError (page->error, HPDF_PAGE_INVALID_INDEX, 0);

    r->value = value;

    return HPDF_OK;
}

HPDF_EXPORT(HPDF_STATUS)
HPDF_Page_SetWidth  (HPDF_Page    page,
                     HPDF_REAL    value)
{
    //HPDF_PTRACE((" HPDF_Page_SetWidth\n"));

    if (value < 3 || value > 14400)
        return HPDF_RaiseError (page->error, HPDF_PAGE_INVALID_SIZE, 0);

    if (HPDF_Page_SetBoxValue (page, "MediaBox", 2, value) != HPDF_OK)
        return HPDF_CheckError (page->error);

    return HPDF_OK;
}


HPDF_EXPORT(HPDF_STATUS)
HPDF_Page_SetHeight  (HPDF_Page    page,
                      HPDF_REAL    value)
{
    //HPDF_PTRACE((" HPDF_Page_SetWidth\n"));

    if (value < 3 || value > 14400)
        return HPDF_RaiseError (page->error, HPDF_PAGE_INVALID_SIZE, 0);

    if (HPDF_Page_SetBoxValue (page, "MediaBox", 3, value) != HPDF_OK)
        return HPDF_CheckError (page->error);

    return HPDF_OK;
}

HPDF_BOOL
HPDF_Page_Validate  (HPDF_Page  page)
{
    HPDF_Obj_Header *header = (HPDF_Obj_Header *)page;

    //HPDF_PTRACE((" HPDF_Page_Validate\n"));

    if (!page || !page->attr)
        return HPDF_FALSE;

    if (header->obj_class != (HPDF_OCLASS_DICT | HPDF_OSUBCLASS_PAGE))
        return HPDF_FALSE;

    return HPDF_TRUE;
}


void
HPDF_Page_SetFilter  (HPDF_Page    page,
                      HPDF_UINT    filter)
{
    HPDF_PageAttr attr;

    //HPDF_PTRACE((" HPDF_Page_SetFilter\n"));

    attr = (HPDF_PageAttr)page->attr;
    attr->contents->filter = filter;
}

/*    End of file    */
