/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm                                            **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc. and Blue Ocean Innovation Ltd.                            **
 **                                                                           **
 **  Copryright(c) 2010. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Blue Ocean Innovation Ltd.                        **
 **  Project#            :  449                                               **
 **  Creation date       :  08/20/2010                                        **
 **  Programmer          :  Michael J. Mariveles                              **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : apiDES.h                                                          **
 **                                                                           **
 **  Description :                                                            **
 **    - all soley for DES encrypion routines.                                **
 **                                                                           **
 ******************************************************************************/
 
#ifndef __APPDES_H
#define __APPDES_H


/********************************************************
 * brief          DES context structure
*********************************************************/

typedef struct
{
    int mode;                   /*!<  encrypt/decrypt   */
    unsigned long sk[32];       /*!<  DES subkeys       */
}
des_context;

/********************************************************
 * brief          Triple-DES context structure
*********************************************************/
typedef struct
{
    int mode;                   /*!<  encrypt/decrypt   */
    unsigned long sk[96];       /*!<  3DES subkeys      */
}
des3_context;


/********************************************************
 * brief          DES key schedule (56-bit, encryption)
 *
 * param ctx      DES context to be initialized
 * param key      8-byte secret key
*********************************************************/
void des_setkey_enc( des_context *ctx, unsigned char key[8] );

/********************************************************
 * brief          DES key schedule (56-bit, decryption)
 *
 * param ctx      DES context to be initialized
 * param key      8-byte secret key
*********************************************************/
void des_setkey_dec( des_context *ctx, unsigned char key[8] );

void apiDES_DataEncrypt_Process(des_context *ctx, UINT8 *par_ubIV, UINT8 *par_DataStreamIn, UINT8 *par_DataStreamOut);
void apiDES_DataEncryptDecrypt_ECB(des_context *ctx, UINT8 input[8], UINT8 output[8]);
void apiDES_DataEncrypt_InitCodeVectors(des_context *ctx);

#endif /* __APPDES_H */

/*  End of File  */

