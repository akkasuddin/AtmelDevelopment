/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm                                            **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc. and Blue Ocean Innovation Ltd.                            **
 **                                                                           **
 **  Copryright(c) 2010. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Blue Ocean Innovation Ltd.                        **
 **  Project#            :  449                                               **
 **  Creation date       :  08/20/2010                                        **
 **  Programmer          :  Michael J. Mariveles                              **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : apiTTV.h                                                          **
 **                                                                           **
 **  Description :                                                            **
 **    - TTV file format writer.                                              **
 **                                                                           **
 ******************************************************************************/
 
#ifndef __APPTTX_H
#define __APPTTX_H

UINT8 appTTV_GenerateFile(void);
void appTTV_TextDataLoadToRAM(const char *par_String, UINT16 *par_uwIncrementalOffset);
void appTTV_GetTextDataFromRAM(UINT8 *par_String, UINT16 *par_uwIncrementalOffset);
void appTTV_TextData_LoadAllToRAM(UINT16 *par_uwTotalBytesInRAM);
void appTTV_QuickString_TrueOrFalse(UINT8 par_ubCond1,UINT8 par_ubCond2, char *par_cStr);

void appTTV_QuickString_AlarmType(UINT8 par_ubAlarmNum, 
                                  UINT8 par_ubAlarmType, 
                                  UINT32 par_udwThreshold, 
                                  UINT8 par_ubEnabled, 
                                  UINT8 par_ubTriggered, 
                                  float par_fRange1High, 
                                  float par_fRange1Low, 
                                  float par_fRange2High, 
                                  float par_fRange2Low,
                                  char *par_cStr);
                                  
UINT16 appTTV_QuickString_GetStrLength(char *par_cStr);
void TTV_OriginatorNotes(char *par_strValue);
void TTV_3PointCertClearEEPROMData(void);

#endif /* __APPTTX_H */

/* End Of File */

