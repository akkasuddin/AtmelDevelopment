/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm                                            **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc. and Blue Ocean Innovation Ltd.                            **
 **                                                                           **
 **  Copryright(c) 2010. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Blue Ocean Innovation Ltd.                        **
 **  Project#            :  449                                               **
 **  Creation date       :  08/20/2010                                        **
 **  Programmer          :  Michael J. Mariveles                              **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : apiTTV.c                                                          **
 **                                                                           **
 **  Description :                                                            **
 **    - TTV file format writer.                                              ** 
 **                                                                           **
 ******************************************************************************/
 
#include "hwPIC24F.h"
#include "hwSystem.h"
#include "devI2CEEPROM_24AA256.h"
#include "utility.h"
#include "commTT4Config.h"
#include "apiDES.h"
#include "appTTV.h"
#include "drvFileSystemCfg.h"
#include "ff.h"
#include "drvRTC.h"
#include <SetJmp.h>



// TTV Report files definitions.. 
#define TTV_ORIGINATOR_NOTES_MAX_CHARS      96   // 96 chars
#define TTV_MAX_MARKER_COUNT                100  // maximum marker allowed that memory can handle..

// DES code global storage...
extern  UINT8 gubDesCodeVector[8];


// EEPROM configuration control structure register...
extern __SENSITECH_CONFIGURATION_AREA  _gsSensitech_Configuration_Area;  
extern __USER_CONFIGURATION_AREA       _gsUser_Configuration_Area;
extern __ALARM_CONFIGURATION_AREA      _gsAlarmConfig[6];  

// EEPROM control bit manipulator structure register...
extern __sBITS_GENCFGOPT_BYTE _gsGenCfgOptByte;
extern __sBITS_EXTOPT_BYTE    _gsExtdOptByte;
extern __sBITS_USERCFG_DATA   _gsUserCfgAreaData;

// PDF read/write LED blinking mode...
extern void __attribute__ ((interrupt, auto_psv)) _T2Interrupt (void);
extern void PDF_Timer2Interrupt_LED_Blinker_Enable(void);
extern void PDF_Timer2Interrupt_LED_Blinker_Disable(void);

// EDS memory array copy...
void MemCpyEdsToMem(UINT8 *par_ubDest, __eds__ UINT8 *par_ubEdsSource, UINT16 par_uwSize);
void MemCpyMemToEds(__eds__ UINT8 *par_ubEdsDest, const UINT8 *par_ubSource, UINT16 par_uwSize);

// EDS data buffer for TTV storage...
__eds__ UINT8 gubEdsMem1Buffer[5120u] __attribute__((space(eds)));

// TTV global filename storage...
char gstrTTVFilename[64];

// download time storage...
extern volatile UINT32 gudwDeviceStoppedDownloadTime;

// the time when the device was plugged to the USB to download temperature datapoints...
extern volatile UINT32 gudwDeviceReadOnTime;


// Stop mode status register...
extern UINT8 gubStopDeviceStatus;

// LFN long filename storage var...
extern char gLfname[64];

// TTV local function declaration...
void appTTV_TextData_Region1(UINT16 *par_uwTotalBytesInRAM);
void appTTV_TextData_Region3(UINT16 *par_uwTotalBytesInRAM);



/*******************************************************************************
**  Function     : appTTV_TextData_Region1
**  Description  : region 1 as header text info encryption process rotuine.
**  PreCondition : must initialize first the DES code vectors and file handler. 
**  Side Effects : none.
**  Input        : stream uncrypted text character bytes
**  Output       : stream encrypted text character bytes
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void appTTV_TextData_Region1(UINT16 *par_uwTotalBytesInRAM){

    UINT16 luwOffsetCtr;
    _UTIL_DATE_TIME_24HR _sDateTime;
    
    char lubStrNotes[100];
    char lubStr[170];
    char lstrVal1[12],lstrVal2[12];
    register double dData;    
     
    luwOffsetCtr = 0;   
    
    // <MonitorData Culture="en-US">
    appTTV_TextDataLoadToRAM("<MonitorData Culture=\"en-US\">",&luwOffsetCtr);
    //---------------------------------------------------------------------------------------------------
    
    utilULongNumToString(_gsSensitech_Configuration_Area.udwManufacturingSerialNumber,(UINT8 *)&lstrVal1[0]);    
    // check number of resets then if zero use manufacturer serial number w/ no trip number...
    if(_gsUser_Configuration_Area.udwTripNumber==0){
        sprintf((char*)&lstrVal2[0],"%s",lstrVal1);
    }    
    else{
        utilULongNumToString(_gsUser_Configuration_Area.udwTripNumber,(UINT8 *)&lstrVal2[0]);
    }    

    
    //  <Monitor Trip="752" Serial="3710815864" ResetCount="3195" Type="TEMPTALE4MA">
    sprintf((char*)&lubStr[0],"<Monitor Trip=\"%s\" Serial=\"%s\" ResetCount=\"%u\" Type=\"TEMPTALE4MA\">",
                                                                  lstrVal2,
                                                                  lstrVal1, 
                                                                  (UINT16)_gsSensitech_Configuration_Area.uwNumberOfCustomerResets);
    appTTV_TextDataLoadToRAM(lubStr,&luwOffsetCtr);
    
    //---------------------------------------------------------------------------------------------------
    
    if((_gsGenCfgOptByte.GenOpt.Byte1.MultDropFlag==1)&&(gubStopDeviceStatus!=_DEVICE_STOP_STATUS_SLEEP_)){
        // multi-drop mode w/ running mode bit ...
        sprintf((char*)&lubStr[0],"<MonitorStopped V=\"\" />");
    }
    else{    
        // multi-drop mode or single w/ stop mode bit ...
        drvRTC_ConvSecsToDateTime(_gsUser_Configuration_Area.udwUnit_Stop_TimeStamp,&_sDateTime);       
        //    <DownloadedOn V="12/09/2008 15:36:23" />    
        sprintf((char*)&lubStr[0],"<MonitorStopped V=\"%02u/%02u/%02u %02u:%02u:%02u\" />",(UINT16)_sDateTime.Mon,
                                                                                           (UINT16)_sDateTime.Day,
                                                                                           (UINT16)_sDateTime.Year,
                                                                                           (UINT16)_sDateTime.Hour,
                                                                                           (UINT16)_sDateTime.Min,
                                                                                           (UINT16)_sDateTime.Sec);
    }                                                                                       
    //  <MonitorStopped V="" />
    appTTV_TextDataLoadToRAM(lubStr,&luwOffsetCtr);
    
    //---------------------------------------------------------------------------------------------------
    
    // get the RTCC system clock actual time...
    // Note: 1.) the global current time in seconds counter will be updated
    //           and synchronized in RTCC system clock....
    //       2.) this is the starting reference time from the Sleep Mode...
    //
    // \mjmariveles.....
    
    
    //drvRTC_ConvSecsToDateTime(gudwDeviceStoppedDownloadTime,&_sDateTime);     
    drvRTC_ConvSecsToDateTime(gudwDeviceReadOnTime,&_sDateTime);         
    //    <DownloadedOn V="12/09/2008 15:36:23" />
    sprintf((char*)&lubStr[0],"<DownloadedOn V=\"%02u/%02u/%02u %02u:%02u:%02u\" />",(UINT16)_sDateTime.Mon,
                                                                                     (UINT16)_sDateTime.Day,
                                                                                     (UINT16)_sDateTime.Year,
                                                                                     (UINT16)_sDateTime.Hour,
                                                                                     (UINT16)_sDateTime.Min,
                                                                                     (UINT16)_sDateTime.Sec);
    appTTV_TextDataLoadToRAM(lubStr,&luwOffsetCtr);                                                                         
    
    //---------------------------------------------------------------------------------------------------
    
    //    <DownloadTimeZone V="Eastern Standard Time" />
    appTTV_TextDataLoadToRAM("<DownloadTimeZone V=\"GMT Standard Time\" />",&luwOffsetCtr);    
    
    //---------------------------------------------------------------------------------------------------
    
    //    <DownloadedBy V="admin" />
    appTTV_TextDataLoadToRAM("<DownloadedBy V=\"TempTale4 USB MA\" />",&luwOffsetCtr);        
    
    //---------------------------------------------------------------------------------------------------
    
    //    <DownloadNotes V="" />
    appTTV_TextDataLoadToRAM("<DownloadNotes V=\"\" />",&luwOffsetCtr); 
    
    //---------------------------------------------------------------------------------------------------
    
    //    <ConfigurationNotes V="" />
    TTV_OriginatorNotes((char *)&lubStrNotes[0]);
    sprintf((char*)&lubStr[0],"<ConfigurationNotes V=\"%s\" />",lubStrNotes);
    appTTV_TextDataLoadToRAM(lubStr,&luwOffsetCtr);

    //---------------------------------------------------------------------------------------------------
    
    //    <ProgrammerName V="" />
    sprintf((char*)&lubStr[0],"<ProgrammerName V=\"%s\" />",(char*)&_gsUser_Configuration_Area.uwUserName[0]);
    appTTV_TextDataLoadToRAM(lubStr,&luwOffsetCtr);
    //---------------------------------------------------------------------------------------------------
    
    //    <AlarmedState V="" />
    appTTV_TextDataLoadToRAM("<AlarmedState V=\"\" />",&luwOffsetCtr); 
    
    //---------------------------------------------------------------------------------------------------
    
    //    <Sensors>
    appTTV_TextDataLoadToRAM("<Sensors>",&luwOffsetCtr); 
    
    //---------------------------------------------------------------------------------------------------
    
    //      <Sensor Num="1" Type="AmbientSensor" Units="Fahrenheit">
    appTTV_TextDataLoadToRAM("<Sensor Num=\"1\" Type=\" AmbientSensor\" Units=\"Fahrenheit\">",&luwOffsetCtr);
    
    //---------------------------------------------------------------------------------------------------
    
    drvRTC_ConvSecsToDateTime(_gsUser_Configuration_Area.udwUnit_Start_TimeStamp,&_sDateTime);
    sprintf((char*)&lubStr[0],"<FirstPointRecorded V=\"%02u/%02u/%02u %02u:%02u:%02u\" />", (UINT16)_sDateTime.Mon,
                                                                                            (UINT16)_sDateTime.Day,
                                                                                            (UINT16)_sDateTime.Year,
                                                                                            (UINT16)_sDateTime.Hour,
                                                                                            (UINT16)_sDateTime.Min,
                                                                                            (UINT16)_sDateTime.Sec);
    //        <FirstPointRecorded V="05/28/2008 03:10:24" />
    appTTV_TextDataLoadToRAM(lubStr,&luwOffsetCtr); 
    
    //---------------------------------------------------------------------------------------------------
    
    sprintf((char*)&lubStr[0],"<NumberOfPoints V=\"%u\" />",_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints);
    //        <NumberOfPoints V="25" />
    appTTV_TextDataLoadToRAM(lubStr,&luwOffsetCtr); 
    
    //---------------------------------------------------------------------------------------------------
    
    dData = (double)_gsUser_Configuration_Area.udwStartUpDelay;
    sprintf((char*)&lubStr[0],"<StartupDelay V=\"%0.0f\" />",dData);
    //        <StartupDelay V="0" />
    appTTV_TextDataLoadToRAM(lubStr,&luwOffsetCtr); 
    
    //---------------------------------------------------------------------------------------------------    
    
    dData = (double)_gsUser_Configuration_Area.udwMeasurementInterval;
    sprintf((char*)&lubStr[0],"<Interval V=\"%0.0f\" />",dData);
    //        <Interval V="60" />
    appTTV_TextDataLoadToRAM(lubStr,&luwOffsetCtr); 
    
    //---------------------------------------------------------------------------------------------------
    
    //        <DownloadData>
    appTTV_TextDataLoadToRAM("<DownloadData>",&luwOffsetCtr); 
    
    //---------------------------------------------------------------------------------------------------  
    
    *par_uwTotalBytesInRAM = luwOffsetCtr;

}


/*******************************************************************************
**  Function     : appTTV_TextData_Region3
**  Description  : region 3 as footer text info encryption process rotuine.
**  PreCondition : must initialize first the DES code vectors and file handler. 
**  Side Effects : none.
**  Input        : stream uncrypted text character bytes
**  Output       : stream encrypted text character bytes
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void appTTV_TextData_Region3(UINT16 *par_uwTotalBytesInRAM){

    UINT16 luwOffsetCtr;
    
    char lubStr[600];
    char lstrVal1[12];
    UINT16 luwMarkerCtr;

    UINT8 lubMark;
    UINT16 luwCtr;
    float lfTempData;

    
    // Alarm Configuration Byte0 control variables...
    __sBITS_ALARM_CONFIG_BYTE0  _lsBitAlarm_ConfigB0;

    luwOffsetCtr = *par_uwTotalBytesInRAM;
    
    
    //---------------------------------------------------------------------------------------------------
    
    //        </DownloadData>
    appTTV_TextDataLoadToRAM("</DownloadData>",&luwOffsetCtr);
    
    //---------------------------------------------------------------------------------------------------    
    
    //        <MarkPoints>
    appTTV_TextDataLoadToRAM("<MarkPoints>",&luwOffsetCtr); 
    
    //---------------------------------------------------------------------------------------------------
    
    // init marker counter...
    luwMarkerCtr = 0;
    
    // get markers....
    for(luwCtr=0;luwCtr<_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints;luwCtr++){        
        
        commTT4Cfg_CacheMem_RetrieveTempData(luwCtr,&lubMark,&lfTempData);
        
        if(lubMark==0xF){        
            
        	if(luwMarkerCtr < TTV_MAX_MARKER_COUNT)
        	    ++luwMarkerCtr;
        	else     
        	    break;     
                        
            sprintf((char*)&lubStr[0],"<M V=\"%u\" />",luwCtr);
            //          <M V="1" />
            appTTV_TextDataLoadToRAM(lubStr,&luwOffsetCtr); 
        }
    }
    
    //---------------------------------------------------------------------------------------------------
    
    //        </MarkPoints>
    appTTV_TextDataLoadToRAM("</MarkPoints>",&luwOffsetCtr); 
    
    //---------------------------------------------------------------------------------------------------
    
    appTTV_QuickString_TrueOrFalse(_gsExtdOptByte.ExtOpt.Byte1.IdealTempRgViewEna,1,(char *)&lstrVal1[0]);
    
    sprintf((char*)&lubStr[0],"<MultiAlarms IdealLow=\"%0.1f\" IdealHigh=\"%0.1f\" IdealEnable=\"%s\" ActEnergy=\"%.0f\">", (double)_gsUserCfgAreaData.fTempIdealRangeLowValue,
                                                                                                         (double)_gsUserCfgAreaData.fTempIdealRangeHighValue,
                                                                                                         lstrVal1,(double)_gsUser_Configuration_Area.fMKTActivationEnergy);
    
    //        <MultiAlarms IdealLow="10" IdealHigh="20" IdealEnable="True">    
    appTTV_TextDataLoadToRAM(lubStr,&luwOffsetCtr); 
    
    //---------------------------------------------------------------------------------------------------    

    // ALARM 1 settings status....   mjm            
    _lsBitAlarm_ConfigB0.ubByte = _gsAlarmConfig[ALARM_1].ubConfigByte0;

    appTTV_QuickString_AlarmType(ALARM_1,
                                 _lsBitAlarm_ConfigB0.Bit.Type,
                                 _gsAlarmConfig[ALARM_1].udwThreshhold,
                                 _lsBitAlarm_ConfigB0.Bit.Enable,
                                 (_gsUser_Configuration_Area.ubAlarmStatus & 1),
                                 _gsAlarmConfig[ALARM_1].fRange1HighTempSetPoint,
                                 _gsAlarmConfig[ALARM_1].fRange1LowTempSetPoint,
                                 _gsAlarmConfig[ALARM_1].fRange2HighTempSetPoint,
                                 _gsAlarmConfig[ALARM_1].fRange2LowTempSetPoint,
                                 (char *)&lubStr[0]);

    appTTV_TextDataLoadToRAM(lubStr,&luwOffsetCtr); 
    
    //---------------------------------------------------------------------------------------------------

    // ALARM 2 settings status....   mjm                
    _lsBitAlarm_ConfigB0.ubByte = _gsAlarmConfig[ALARM_2].ubConfigByte0;
    
    appTTV_QuickString_AlarmType(ALARM_2,
                                 _lsBitAlarm_ConfigB0.Bit.Type,
                                 _gsAlarmConfig[ALARM_2].udwThreshhold,
                                 _lsBitAlarm_ConfigB0.Bit.Enable,
                                 ((_gsUser_Configuration_Area.ubAlarmStatus & 2)>>1),
                                 _gsAlarmConfig[ALARM_2].fRange1HighTempSetPoint,
                                 _gsAlarmConfig[ALARM_2].fRange1LowTempSetPoint,
                                 _gsAlarmConfig[ALARM_2].fRange2HighTempSetPoint,
                                 _gsAlarmConfig[ALARM_2].fRange2LowTempSetPoint,
                                 (char *)&lubStr[0]);
                                 
    appTTV_TextDataLoadToRAM(lubStr,&luwOffsetCtr);
       
    //---------------------------------------------------------------------------------------------------

    // ALARM 3 settings status....   mjm            
    _lsBitAlarm_ConfigB0.ubByte = _gsAlarmConfig[ALARM_3].ubConfigByte0;
    
    appTTV_QuickString_AlarmType(ALARM_3,
                                 _lsBitAlarm_ConfigB0.Bit.Type,
                                 _gsAlarmConfig[ALARM_3].udwThreshhold,
                                 _lsBitAlarm_ConfigB0.Bit.Enable,
                                 ((_gsUser_Configuration_Area.ubAlarmStatus & 4)>>2),
                                 _gsAlarmConfig[ALARM_3].fRange1HighTempSetPoint,
                                 _gsAlarmConfig[ALARM_3].fRange1LowTempSetPoint,
                                 _gsAlarmConfig[ALARM_3].fRange2HighTempSetPoint,
                                 _gsAlarmConfig[ALARM_3].fRange2LowTempSetPoint,
                                 (char *)&lubStr[0]);
                                 
    appTTV_TextDataLoadToRAM(lubStr,&luwOffsetCtr);
    
    //---------------------------------------------------------------------------------------------------

    // ALARM 4 settings status....   mjm        
    _lsBitAlarm_ConfigB0.ubByte = _gsAlarmConfig[ALARM_4].ubConfigByte0;
    
    appTTV_QuickString_AlarmType(ALARM_4,
                                 _lsBitAlarm_ConfigB0.Bit.Type,
                                 _gsAlarmConfig[ALARM_4].udwThreshhold,
                                 _lsBitAlarm_ConfigB0.Bit.Enable,
                                 ((_gsUser_Configuration_Area.ubAlarmStatus & 8)>>3),
                                 _gsAlarmConfig[ALARM_4].fRange1HighTempSetPoint,
                                 _gsAlarmConfig[ALARM_4].fRange1LowTempSetPoint,
                                 _gsAlarmConfig[ALARM_4].fRange2HighTempSetPoint,
                                 _gsAlarmConfig[ALARM_4].fRange2LowTempSetPoint,
                                 (char *)&lubStr[0]);
                                 
    appTTV_TextDataLoadToRAM(lubStr,&luwOffsetCtr);
      
    //---------------------------------------------------------------------------------------------------

    // ALARM 5 settings status....   mjm        
    _lsBitAlarm_ConfigB0.ubByte = _gsAlarmConfig[ALARM_5].ubConfigByte0;

    appTTV_QuickString_AlarmType(ALARM_5,
                                 _lsBitAlarm_ConfigB0.Bit.Type,
                                 _gsAlarmConfig[ALARM_5].udwThreshhold,
                                 _lsBitAlarm_ConfigB0.Bit.Enable,
                                 ((_gsUser_Configuration_Area.ubAlarmStatus & 16)>>4),
                                 _gsAlarmConfig[ALARM_5].fRange1HighTempSetPoint,
                                 _gsAlarmConfig[ALARM_5].fRange1LowTempSetPoint,
                                 _gsAlarmConfig[ALARM_5].fRange2HighTempSetPoint,
                                 _gsAlarmConfig[ALARM_5].fRange2LowTempSetPoint,
                                 (char *)&lubStr[0]);
                                 
    appTTV_TextDataLoadToRAM(lubStr,&luwOffsetCtr);

    //---------------------------------------------------------------------------------------------------
    
    // ALARM 6 settings status....   mjm
    _lsBitAlarm_ConfigB0.ubByte = _gsAlarmConfig[ALARM_6].ubConfigByte0;
    
    appTTV_QuickString_AlarmType(ALARM_6,
                                 _lsBitAlarm_ConfigB0.Bit.Type,
                                 _gsAlarmConfig[ALARM_6].udwThreshhold,
                                 _lsBitAlarm_ConfigB0.Bit.Enable,
                                 ((_gsUser_Configuration_Area.ubAlarmStatus & 32)>>5),
                                 _gsAlarmConfig[ALARM_6].fRange1HighTempSetPoint,
                                 _gsAlarmConfig[ALARM_6].fRange1LowTempSetPoint,
                                 _gsAlarmConfig[ALARM_6].fRange2HighTempSetPoint,
                                 _gsAlarmConfig[ALARM_6].fRange2LowTempSetPoint,
                                 (char *)&lubStr[0]);
                                 
    appTTV_TextDataLoadToRAM(lubStr,&luwOffsetCtr);

    //---------------------------------------------------------------------------------------------------

    //        </VaxAlarmEvents>
    appTTV_TextDataLoadToRAM("<VaxAlarmEvents>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<VaxAlarmEvent EventNumber=\"\" EventType=\"\" EventElapsedTime=\"\" EventTemperature=\"\" EventRTCTime=\"\" />",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<VaxAlarmEvent EventNumber=\"\" EventType=\"\" EventElapsedTime=\"\" EventTemperature=\"\" EventRTCTime=\"\" />",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<VaxAlarmEvent EventNumber=\"\" EventType=\"\" EventElapsedTime=\"\" EventTemperature=\"\" EventRTCTime=\"\" />",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<VaxAlarmEvent EventNumber=\"\" EventType=\"\" EventElapsedTime=\"\" EventTemperature=\"\" EventRTCTime=\"\" />",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<VaxAlarmEvent EventNumber=\"\" EventType=\"\" EventElapsedTime=\"\" EventTemperature=\"\" EventRTCTime=\"\" />",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<VaxAlarmEvent EventNumber=\"\" EventType=\"\" EventElapsedTime=\"\" EventTemperature=\"\" EventRTCTime=\"\" />",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<VaxAlarmEvent EventNumber=\"\" EventType=\"\" EventElapsedTime=\"\" EventTemperature=\"\" EventRTCTime=\"\" />",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<VaxAlarmEvent EventNumber=\"\" EventType=\"\" EventElapsedTime=\"\" EventTemperature=\"\" EventRTCTime=\"\" />",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<VaxAlarmEvent EventNumber=\"\" EventType=\"\" EventElapsedTime=\"\" EventTemperature=\"\" EventRTCTime=\"\" />",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</VaxAlarmEvents>",&luwOffsetCtr);
    
    
    //---------------------------------------------------------------------------------------------------
    
    //        </MultiAlarms>
    appTTV_TextDataLoadToRAM("</MultiAlarms>",&luwOffsetCtr);



    
    //---------------------------------------------------------------------------------------------------

    //      </Sensor>
    appTTV_TextDataLoadToRAM("</Sensor>",&luwOffsetCtr);     
    
    //---------------------------------------------------------------------------------------------------
    
    //    </Sensors>
    appTTV_TextDataLoadToRAM("</Sensors>",&luwOffsetCtr); 
    
    //---------------------------------------------------------------------------------------------------
    
    //----------------------------
    // 3 point Certificate data
    //---------------------------=
    
    // 3 point Certificate bit enable..
    appTTV_QuickString_TrueOrFalse((_gsSensitech_Configuration_Area.ubExtendedOptionByte1&16),16,(char *)&lstrVal1[0]);
    
    //    <MonitorCertification Enabled="True">
    sprintf((char*)&lubStr[0],"<MonitorCertification Enabled=\"%s\">",lstrVal1);
    appTTV_TextDataLoadToRAM(lubStr,&luwOffsetCtr); 
    
    //---------------------------------------------------------------------------------------------------
        
    //      <Sensors>
    appTTV_TextDataLoadToRAM("<Sensors>",&luwOffsetCtr); 
    
    //---------------------------------------------------------------------------------------------------
    
    //        <Sensor number="1" SensorType="" TesterName="Sensitech" DateValidated="02/17/2010 12:00 PM" NistDataName=" NIST Chamber# 2 C0430 Logger Name: Logger#:A7B842/Probe:800611" Passed="True">

    UINT8 lstrTesterName_1[25];
    UINT8 lstrDate_1[12];
    UINT8 lstrTime_1[15];
    UINT8 lubChamStnNum_1;
    UINT8 lstrChamSN_1[9];
    UINT8 lstrLoggerName_1[21];
    UINT8 lubPassFail_1;
    char lstrDateTime_1[30];
    char lstrNISTDataName_1[85];
    
    // Tester name...
    utilReadEEPROM_DataToString(448u,22,(char*)&lstrTesterName_1[0]);
    
    // Date validated...
    utilReadEEPROM_DataToString(470u,10,(char*)&lstrDate_1[0]); // date..
    utilReadEEPROM_DataToString(480u,11,(char*)&lstrTime_1[0]); // time..
    sprintf((char*)&lstrDateTime_1[0],"%s %s",lstrDate_1,lstrTime_1);
    
    // NIST data name...
    devEEPROM_RandomRead(544u,&lubChamStnNum_1); 
    utilReadEEPROM_DataToString(545u,8,(char*)&lstrChamSN_1[0]);
    utilReadEEPROM_DataToString(553u,20,(char*)&lstrLoggerName_1[0]);    
    sprintf((char*)&lstrNISTDataName_1[0],"NIST Chamber# %u Logger Name: Logger# %s Probe# %s",lubChamStnNum_1,lstrLoggerName_1,lstrChamSN_1);
    
    // Pass or Fail..
    devEEPROM_RandomRead(542u,&lubPassFail_1); // 0-fail / 1-pass
    appTTV_QuickString_TrueOrFalse(lubPassFail_1,1,(char *)&lstrVal1[0]);
    
        
    sprintf((char*)&lubStr[0],"<Sensor number=\"1\" SensorType=\"\" TesterName=\"%s\" DateValidated=\"%s\" NistDataName=\" %s\" Passed=\"%s\">",lstrTesterName_1,
                                                                                                                                                lstrDateTime_1,
                                                                                                                                                lstrNISTDataName_1,
                                                                                                                                                 lstrVal1);
    // write to EDS RAM..
    appTTV_TextDataLoadToRAM(lubStr,&luwOffsetCtr);     
    
    //---------------------------------------------------------------------------------------------------
    
    //          <StandardReading Point1="59.4" Point2="-1.9" Point3="139.2" />
    
    float lfPt1,lfPt2,lfPt3;
    UINT16 luwPt;

    // standard reading point 1..    
    utilReadEEPROM_ByteValue(497u,2,(void *)&luwPt);
    lfPt1 = commTT4Cfg_ConvertToFinalTempData(luwPt);
    
    // standard reading point 2..    
    utilReadEEPROM_ByteValue(499u,2,(void *)&luwPt);
    lfPt2 = commTT4Cfg_ConvertToFinalTempData(luwPt);
    
    // standard reading point 3..    
    utilReadEEPROM_ByteValue(501u,2,(void *)&luwPt);
    lfPt3 = commTT4Cfg_ConvertToFinalTempData(luwPt);
   
    
    sprintf((char*)&lubStr[0],"<StandardReading Point1=\"%0.1f\" Point2=\"%0.1f\" Point3=\"%0.1f\" />",(double)lfPt1,(double)lfPt2,(double)lfPt3); 
    appTTV_TextDataLoadToRAM(lubStr,&luwOffsetCtr); 
    
    //---------------------------------------------------------------------------------------------------
    
    //          <Tolerance Point1="1" Point2="2" Point3="2" />
    //appTTV_TextDataLoadToRAM("<Tolerance Point1=\"1\" Point2=\"2\" Point3=\"2\" />",&luwOffsetCtr); 
    
    float lfTolPt1,lfTolPt2,lfTolPt3;
    UINT8 lubTolPt;

    // tolerance point 1..    
    utilReadEEPROM_ByteValue(491u,1,(void *)&lubTolPt);    
    lfTolPt1 = (float)lubTolPt / 10.0;
    
    // tolerance point 2..    
    utilReadEEPROM_ByteValue(492u,1,(void *)&lubTolPt);    
    lfTolPt2 = (float)lubTolPt / 10.0;
    
    // tolerance point 3..    
    utilReadEEPROM_ByteValue(493u,1,(void *)&lubTolPt);
    lfTolPt3 = (float)lubTolPt / 10.0;
    

    sprintf((char*)&lubStr[0],"<Tolerance Point1=\"%0.1f\" Point2=\"%0.1f\" Point3=\"%0.1f\" />",(double)lfTolPt1,(double)lfTolPt2,(double)lfTolPt3); 
    appTTV_TextDataLoadToRAM(lubStr,&luwOffsetCtr); 
    
    //---------------------------------------------------------------------------------------------------
    
    //          <TestResult Point="1" Reading="59.4" Variance="0.0" />
    
    float lfReading,lfVariance,lfTempVariance;
    UINT8 lub1Byte;
    
    // reading..    
    utilReadEEPROM_ByteValue(503u,2,(void *)&luwPt);
    lfReading = commTT4Cfg_ConvertToFinalTempData(luwPt);

    // variance..    
    utilReadEEPROM_ByteValue(494u,1,(void *)&lub1Byte);
    lfTempVariance = (float)lub1Byte;
    lfVariance = lfTempVariance / 10.0;
    
    sprintf((char*)&lubStr[0],"<TestResult Point=\"1\" Reading=\"%0.1f\" Variance=\"%0.1f\" />",(double)lfReading,(double)lfVariance); 
    appTTV_TextDataLoadToRAM(lubStr,&luwOffsetCtr); 
    
    //---------------------------------------------------------------------------------------------------
    
    //          <TestResult Point="2" Reading="-0.9" Variance="1.0" />
    
    // reading..    
    utilReadEEPROM_ByteValue(505u,2,(void *)&luwPt);
    lfReading = commTT4Cfg_ConvertToFinalTempData(luwPt);

    // variance..    
    utilReadEEPROM_ByteValue(495u,1,(void *)&lub1Byte);
    lfTempVariance = (float)lub1Byte;
    lfVariance = (float)lfTempVariance / 10.0;
    
    sprintf((char*)&lubStr[0],"<TestResult Point=\"2\" Reading=\"%0.1f\" Variance=\"%0.1f\" />",(double)lfReading,(double)lfVariance); 
    appTTV_TextDataLoadToRAM(lubStr,&luwOffsetCtr); 
    
    //---------------------------------------------------------------------------------------------------
    
    //          <TestResult Point="3" Reading="137.8" Variance="1.4" />
    
    // reading..    
    utilReadEEPROM_ByteValue(507u,2,(void *)&luwPt);
    lfReading = commTT4Cfg_ConvertToFinalTempData(luwPt);

    // variance..    
    utilReadEEPROM_ByteValue(496u,1,(void *)&lub1Byte);
    lfTempVariance = (float)lub1Byte;
    lfVariance = (float)lfTempVariance / 10.0;
    
    sprintf((char*)&lubStr[0],"<TestResult Point=\"3\" Reading=\"%0.1f\" Variance=\"%0.1f\" />",(double)lfReading,(double)lfVariance); 
    appTTV_TextDataLoadToRAM(lubStr,&luwOffsetCtr); 
    
    
    //---------------------------------------------------------------------------------------------------
    
    //        </Sensor>
    appTTV_TextDataLoadToRAM("</Sensor>",&luwOffsetCtr); 
    
    //---------------------------------------------------------------------------------------------------
    
    //      </Sensors>
    appTTV_TextDataLoadToRAM("</Sensors>",&luwOffsetCtr); 
    
    //---------------------------------------------------------------------------------------------------
    
    //    </MonitorCertification>
    appTTV_TextDataLoadToRAM("</MonitorCertification>",&luwOffsetCtr); 
    
    //---------------------------------------------------------------------------------------------------
    
    //  </Monitor>
    appTTV_TextDataLoadToRAM("</Monitor>",&luwOffsetCtr); 
    
    //---------------------------------------------------------------------------------------------------
    char shipstr_mem[50];
    char shipstr[50];

    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));


    if (_gsGenCfgOptByte.GenOpt.Byte0.ShippingInfoEna)
        sprintf((char*)&shipstr[0], "<Shipment ShipmentInfoEnabled=\"true\">");
    else
        sprintf((char*)&shipstr[0], "<Shipment ShipmentInfoEnabled=\"false\">");

    appTTV_TextDataLoadToRAM(shipstr,&luwOffsetCtr);
    
    appTTV_TextDataLoadToRAM("<ShipmentTags>",&luwOffsetCtr); 
    
    appTTV_TextDataLoadToRAM("<ShipmentTag>",&luwOffsetCtr);
    //---------------------------------------------------------------------------------------------------
    // Shipping Info1
    appTTV_TextDataLoadToRAM("<Label>",&luwOffsetCtr);
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL1_LABEL,(void *)&shipstr_mem[0]);
    sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</Label>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<Value>",&luwOffsetCtr);
    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL1_VALUE,(void *)&shipstr_mem[0]);
    sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</Value>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<TagDefId>",&luwOffsetCtr);
    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL1_TAG,(void *)&shipstr_mem[0]);
    sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</TagDefId>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<LOVPossibleValueId>",&luwOffsetCtr);
    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL1_LOV,(void *)&shipstr_mem[0]);
    sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</LOVPossibleValueId>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<FlexTagType>",&luwOffsetCtr);
    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL1_FLEX_TAG_TYPE,(void *)&shipstr_mem[0]);
    if (shipstr_mem[0] == 0x00)
        sprintf((char*)&shipstr[0],"\"0\"");
    else
        sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</FlexTagType>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM(" </ShipmentTag>",&luwOffsetCtr);

    //---------------------------------------------------------------------------------------------------
    // Shipping Info2
    appTTV_TextDataLoadToRAM("<ShipmentTag>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<Label>",&luwOffsetCtr);
    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL2_LABEL,(void *)&shipstr_mem[0]);
    sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</Label>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<Value>",&luwOffsetCtr);
    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL2_VALUE,(void *)&shipstr_mem[0]);
    sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</Value>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<TagDefId>",&luwOffsetCtr);
    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL2_TAG,(void *)&shipstr_mem[0]);
    sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</TagDefId>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<LOVPossibleValueId>",&luwOffsetCtr);
    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL2_LOV,(void *)&shipstr_mem[0]);
    sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</LOVPossibleValueId>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<FlexTagType>",&luwOffsetCtr);
    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL2_FLEX_TAG_TYPE,(void *)&shipstr_mem[0]);
    if (shipstr_mem[0] == 0x00)
        sprintf((char*)&shipstr[0],"\"0\"");
    else
        sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</FlexTagType>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM(" </ShipmentTag>",&luwOffsetCtr);

    //---------------------------------------------------------------------------------------------------
    // Shipping Info3
    appTTV_TextDataLoadToRAM("<ShipmentTag>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<Label>",&luwOffsetCtr);
    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL3_LABEL,(void *)&shipstr_mem[0]);
    sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</Label>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<Value>",&luwOffsetCtr);
    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL3_VALUE,(void *)&shipstr_mem[0]);
    sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</Value>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<TagDefId>",&luwOffsetCtr);
    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL3_TAG,(void *)&shipstr_mem[0]);
    sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</TagDefId>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<LOVPossibleValueId>",&luwOffsetCtr);
    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL3_LOV,(void *)&shipstr_mem[0]);
    sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</LOVPossibleValueId>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<FlexTagType>",&luwOffsetCtr);
    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL3_FLEX_TAG_TYPE,(void *)&shipstr_mem[0]);
    if (shipstr_mem[0] == 0x00)
        sprintf((char*)&shipstr[0],"\"0\"");
    else
        sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</FlexTagType>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM(" </ShipmentTag>",&luwOffsetCtr);
    //---------------------------------------------------------------------------------------------------
    // Shipping Info4
    appTTV_TextDataLoadToRAM("<ShipmentTag>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<Label>",&luwOffsetCtr);
    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL4_LABEL,(void *)&shipstr_mem[0]);
    sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</Label>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<Value>",&luwOffsetCtr);
    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL4_VALUE,(void *)&shipstr_mem[0]);
    sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</Value>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<TagDefId>",&luwOffsetCtr);
    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL4_TAG,(void *)&shipstr_mem[0]);
    sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</TagDefId>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<LOVPossibleValueId>",&luwOffsetCtr);
    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL4_LOV,(void *)&shipstr_mem[0]);
    sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</LOVPossibleValueId>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<FlexTagType>",&luwOffsetCtr);
    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL4_FLEX_TAG_TYPE,(void *)&shipstr_mem[0]);
    if (shipstr_mem[0] == 0x00)
        sprintf((char*)&shipstr[0],"\"0\"");
    else
        sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</FlexTagType>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM(" </ShipmentTag>",&luwOffsetCtr);
    //---------------------------------------------------------------------------------------------------
    // Shipping Info5
    appTTV_TextDataLoadToRAM("<ShipmentTag>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<Label>",&luwOffsetCtr);
    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL5_LABEL,(void *)&shipstr_mem[0]);
    sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</Label>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<Value>",&luwOffsetCtr);
    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL5_VALUE,(void *)&shipstr_mem[0]);
    sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</Value>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<TagDefId>",&luwOffsetCtr);
    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL5_TAG,(void *)&shipstr_mem[0]);
    sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</TagDefId>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<LOVPossibleValueId>",&luwOffsetCtr);
    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL5_LOV,(void *)&shipstr_mem[0]);
    sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</LOVPossibleValueId>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<FlexTagType>",&luwOffsetCtr);
    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL5_FLEX_TAG_TYPE,(void *)&shipstr_mem[0]);
    if (shipstr_mem[0] == 0x00)
        sprintf((char*)&shipstr[0],"\"0\"");
    else
        sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</FlexTagType>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM(" </ShipmentTag>",&luwOffsetCtr);
    //---------------------------------------------------------------------------------------------------
    // Shipping Info6
    appTTV_TextDataLoadToRAM("<ShipmentTag>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<Label>",&luwOffsetCtr);
    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL6_LABEL,(void *)&shipstr_mem[0]);
    sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</Label>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<Value>",&luwOffsetCtr);
    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL6_VALUE,(void *)&shipstr_mem[0]);
    sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</Value>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<TagDefId>",&luwOffsetCtr);
    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL6_TAG,(void *)&shipstr_mem[0]);
    sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</TagDefId>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<LOVPossibleValueId>",&luwOffsetCtr);
    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL6_LOV,(void *)&shipstr_mem[0]);
    sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</LOVPossibleValueId>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<FlexTagType>",&luwOffsetCtr);
    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL6_FLEX_TAG_TYPE,(void *)&shipstr_mem[0]);
    if (shipstr_mem[0] == 0x00)
        sprintf((char*)&shipstr[0],"\"0\"");
    else
        sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</FlexTagType>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM(" </ShipmentTag>",&luwOffsetCtr);
    //---------------------------------------------------------------------------------------------------
    // Shipping Info7
    appTTV_TextDataLoadToRAM("<ShipmentTag>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<Label>",&luwOffsetCtr);
    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL7_LABEL,(void *)&shipstr_mem[0]);
    sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</Label>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<Value>",&luwOffsetCtr);
    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL7_VALUE,(void *)&shipstr_mem[0]);
    sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</Value>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<TagDefId>",&luwOffsetCtr);
    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL7_TAG,(void *)&shipstr_mem[0]);
    sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</TagDefId>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<LOVPossibleValueId>",&luwOffsetCtr);
    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL7_LOV,(void *)&shipstr_mem[0]);
    sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</LOVPossibleValueId>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<FlexTagType>",&luwOffsetCtr);
    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL7_FLEX_TAG_TYPE,(void *)&shipstr_mem[0]);
    if (shipstr_mem[0] == 0x00)
        sprintf((char*)&shipstr[0],"\"0\"");
    else
        sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</FlexTagType>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM(" </ShipmentTag>",&luwOffsetCtr);
    //---------------------------------------------------------------------------------------------------
    // Shipping Info8
    appTTV_TextDataLoadToRAM("<ShipmentTag>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<Label>",&luwOffsetCtr);
    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL8_LABEL,(void *)&shipstr_mem[0]);
    sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</Label>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<Value>",&luwOffsetCtr);
    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL8_VALUE,(void *)&shipstr_mem[0]);
    sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</Value>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<TagDefId>",&luwOffsetCtr);
    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL8_TAG,(void *)&shipstr_mem[0]);
    sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</TagDefId>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<LOVPossibleValueId>",&luwOffsetCtr);
    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL8_LOV,(void *)&shipstr_mem[0]);
    sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</LOVPossibleValueId>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<FlexTagType>",&luwOffsetCtr);
    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL8_FLEX_TAG_TYPE,(void *)&shipstr_mem[0]);
    if (shipstr_mem[0] == 0x00)
        sprintf((char*)&shipstr[0],"\"0\"");
    else
        sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</FlexTagType>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM(" </ShipmentTag>",&luwOffsetCtr);
    //---------------------------------------------------------------------------------------------------
    // Shipping Info9
    appTTV_TextDataLoadToRAM("<ShipmentTag>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<Label>",&luwOffsetCtr);
    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL9_LABEL,(void *)&shipstr_mem[0]);
    sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</Label>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<Value>",&luwOffsetCtr);
    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL9_VALUE,(void *)&shipstr_mem[0]);
    sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</Value>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<TagDefId>",&luwOffsetCtr);
    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL9_TAG,(void *)&shipstr_mem[0]);
    sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</TagDefId>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<LOVPossibleValueId>",&luwOffsetCtr);
    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL9_LOV,(void *)&shipstr_mem[0]);
    sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</LOVPossibleValueId>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<FlexTagType>",&luwOffsetCtr);
    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL9_FLEX_TAG_TYPE,(void *)&shipstr_mem[0]);
    if (shipstr_mem[0] == 0x00)
        sprintf((char*)&shipstr[0],"\"0\"");
    else
        sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</FlexTagType>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM(" </ShipmentTag>",&luwOffsetCtr);
 //----------------------------------------------------------------------------------------------------------------
     // Shipping Info10
    appTTV_TextDataLoadToRAM("<ShipmentTag>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<Label>",&luwOffsetCtr);
    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL10_LABEL,(void *)&shipstr_mem[0]);
    sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</Label>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<Value>",&luwOffsetCtr);
    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL10_VALUE,(void *)&shipstr_mem[0]);
    sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</Value>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<TagDefId>",&luwOffsetCtr);
    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL10_TAG,(void *)&shipstr_mem[0]);
    sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</TagDefId>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<LOVPossibleValueId>",&luwOffsetCtr);
    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL10_LOV,(void *)&shipstr_mem[0]);
    sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</LOVPossibleValueId>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM("<FlexTagType>",&luwOffsetCtr);
    memset((char*)&shipstr_mem[0], 0x00, sizeof(shipstr_mem));
    memset((char*)&shipstr[0], 0x00, sizeof(shipstr));
    commTT4Cfg_FastReadCfgEeprom(OFFSET_LABEL10_FLEX_TAG_TYPE,(void *)&shipstr_mem[0]);
    if (shipstr_mem[0] == 0x00)
        sprintf((char*)&shipstr[0],"\"0\"");
    else
        sprintf((char*)&shipstr[0],"\"%s\"",shipstr_mem);
    appTTV_TextDataLoadToRAM(shipstr, &luwOffsetCtr);
    appTTV_TextDataLoadToRAM("</FlexTagType>",&luwOffsetCtr);
    appTTV_TextDataLoadToRAM(" </ShipmentTag>",&luwOffsetCtr);
    
    //---------------------------------------------------------------------------------------------------
    appTTV_TextDataLoadToRAM(" </ShipmentTags>",&luwOffsetCtr);
    //---------------------------------------------------------------------------------------------------
    appTTV_TextDataLoadToRAM(" </Shipment>",&luwOffsetCtr);
    //</MonitorData>
    appTTV_TextDataLoadToRAM("</MonitorData>",&luwOffsetCtr); 
     
    *par_uwTotalBytesInRAM = luwOffsetCtr;

}



/*******************************************************************************
**  Function     : MoveEdsMemFromTopToIndex0
**  Description  : copies array EDS data to linear memory.
**  PreCondition : must be array of EDS memory data only
**  Side Effects : none
**  Input        : EDS array memory
**  Output       : linear array memory
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void MoveEdsMemFromTopToIndex0(__eds__ UINT8 *par_ubEdsDest, UINT16 par_uwFromIndex , UINT16 par_uwExtraByte){

    register UINT16 luwCtr;
    
    luwCtr = 0;
    do{
        par_ubEdsDest[luwCtr] = par_ubEdsDest[par_uwFromIndex + luwCtr]; 
        ++luwCtr;
    }while(luwCtr < par_uwExtraByte);
}        


/*******************************************************************************
**  Function     : appTTV_GenerateFile
**  Description  : main TTV file creator.
**  PreCondition : initialize first the DES encryption and file handler. 
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
UINT8 appTTV_GenerateFile(void){  
    
    // file system control vars...    
    DRVFS_FILEP logFile;    
    UINT luwByteToWrite = 0;
    UINT8 lub512TempBuffer[512u];
    // DES encrytion vars...
    des_context lctx;
    UINT8 lubTTxDataIn[8];
    UINT8 lubTTxDataOut[8]; 
    UINT8 lubIV[8];
    UINT8 lubPadding;
    UINT16 luwTotalBytesInRAM;
    UINT16 luwRAMByteCtr;
    UINT16 luwDataOffset; // luwDataOffset was added by mjm for tony's minimal code change implementation.. 09082011 
    // byte count variable...
    UINT16 luwCtr;
    UINT16 luwNumOfLoops;
    UINT16 luwExtraByte;
    UINT16 luwDpCtr;    
    // temperature text control vars...
    char lubStr[20];
    float lfTempData;
    UINT8 lubMark;
    UINT8 lstrValInput1[12],lstrValInput2[12];

     
    PDF_Timer2Interrupt_LED_Blinker_Enable();
        
    // get the subkeys and load code vector...
    apiDES_DataEncrypt_InitCodeVectors(&lctx);
    
    memcpy((void *)lubIV, (void*)gubDesCodeVector, 8 );    
 
    utilULongNumToString(_gsSensitech_Configuration_Area.udwManufacturingSerialNumber,(UINT8 *)&lstrValInput1[0]);    
    // check number of resets then if zero use manufacturer serial number w/ no trip number...
    if(_gsUser_Configuration_Area.udwTripNumber==0){
        sprintf((char *)&gLfname[0],"%s_%s_%u.ttv",lstrValInput1,lstrValInput1,_gsSensitech_Configuration_Area.uwNumberOfCustomerResets);		
    }    
    else{
        utilULongNumToString(_gsUser_Configuration_Area.udwTripNumber,(UINT8 *)&lstrValInput2[0]);                        
        // the PDF file naming format is TripS/N_Manf.S/N_# of Customer resets... Peter amend this format requested by Sensitech SQA group....040511 mjm 
        sprintf((char *)&gLfname[0],"%s_%s_%u.ttv",lstrValInput2,lstrValInput1,_gsSensitech_Configuration_Area.uwNumberOfCustomerResets);		
    }    
    
    DRVFS_FOPEN(&logFile,(char *)&gLfname[0],FA_CREATE_ALWAYS | FA_WRITE);


    // save generated TTV filename... 
    util_SaveLongFileName((char *)&gstrTTVFilename[0],(const char *)&gLfname[0]);

    //--------------------------------------------------
    // Region 1 : Header Encrypt and Save to file 
    //--------------------------------------------------
    
    // retrieve the accumulated text data string from a 4096 EDS buffer RAM and assign to region 1...
    appTTV_TextData_Region1(&luwTotalBytesInRAM);    
    
    // set to zero, if region 1 does not reached 512 bytes then the extra byte array must be at 0 index position... 
    luwDataOffset = 0; // luwDataOffset was added by mjm for tony's minimal code change implementation.. 09082011 
    
    // get the number of loops per 512 bytes...
    luwNumOfLoops = luwTotalBytesInRAM / 512u;
    // get the extra text data size from the 4096 EDS buffer memory... 
    luwExtraByte = luwTotalBytesInRAM % 512u;

    // Region 1 process 512 bytes for encryption and write to flash....
    if(luwNumOfLoops!=0){ // luwDataOffset was added by mjm for tony's minimal code change implementation.. 09082011 
        
        // set extra bytes array index on topmost position...         
        luwDataOffset = 512u; // luwDataOffset was added by mjm for tony's minimal code change implementation.. 09082011 
        
        // 64 x 8 = 512
        for(luwCtr=0;luwCtr<64;luwCtr++){        
            // load text information to 8 bytes encrypter buffer...
            MemCpyEdsToMem((UINT8*)&lubTTxDataIn[0], (__eds__ UINT8*)&gubEdsMem1Buffer[luwCtr * 8],8);
            // encrypt 8 bytes to DES format...
            apiDES_DataEncrypt_Process(&lctx,lubIV,lubTTxDataIn,lubTTxDataOut);                
            // transfer per 8 bytes to 512 bytes encryption data buffer... 
            MemCpyMemToEds((UINT8*)&lub512TempBuffer[luwCtr * 8],(const UINT8*)&lubTTxDataOut[0],8);                 
                
            // check USB user status...
            if(TT4_USB_VBUS_IO == 0){
                PDF_Timer2Interrupt_LED_Blinker_Disable();
                TT4_RED_LED_OFF;
                DRVFS_FCLOSE(&logFile);
                return(0);
            }     
        } // end 64 loops..
    
        // write to file IO stream...    
        DRVFS_FWRITE(&logFile,(const void *)lub512TempBuffer,luwDataOffset,&luwByteToWrite); // luwDataOffset was added by mjm for tony's minimal code change implementation.. 09082011 
    
    }// end of Region 1 w/ 512 byte encryption...
    
    // collect uncrypted text data string as extra bytes from Region 1...
    if(luwExtraByte>0){
        // get and save extra bytes...   
        MemCpyEdsToMem((UINT8*)&lub512TempBuffer[0], (__eds__ UINT8*)&gubEdsMem1Buffer[512u],luwExtraByte);
        // shift the top most remaining data to index 0...        
        MoveEdsMemFromTopToIndex0((__eds__ UINT8*)&gubEdsMem1Buffer[0],luwDataOffset,luwExtraByte);
    } // end of Region 1 extra bytes handler...
    
    // next starting index of EDS RAM from the top of Region 1 extra bytes...
    luwTotalBytesInRAM = luwExtraByte;

    
    //-----------------------------------------------------
    // Region 2 : Datapoints Encrypt and Save to file 
    //-----------------------------------------------------

    // fetch datapoint accumulator loop...
    for(luwDpCtr=0;luwDpCtr<_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints;luwDpCtr++){
        
        // get temperature the saved temperature values from datapoints 32k EDS cache memory...
        commTT4Cfg_CacheMem_RetrieveTempData(luwDpCtr,&lubMark,&lfTempData);        
        // convert temperature point to a formatted text string...
        sprintf((char*)&lubStr[0],"<D V=\"%0.1f\" />",(double)lfTempData);        
        // load and accumulate to 4096 global EDS RAM buffer array...
        appTTV_TextDataLoadToRAM(lubStr,&luwTotalBytesInRAM); 
        
      	/// check USB user status...
        if(TT4_USB_VBUS_IO == 0){
            PDF_Timer2Interrupt_LED_Blinker_Disable();
            TT4_RED_LED_OFF;            
            DRVFS_FCLOSE(&logFile);
            return(0);
        }     
        
        // if the total text string eguals or exceeds 512 bytes then encrypt and write to the flash..
        if(luwTotalBytesInRAM>=512u){
            
            // get the datapoint formattted string extra bytes...
            luwExtraByte = luwTotalBytesInRAM % 512u;

            // 64 x 8 = 512
            for(luwCtr=0;luwCtr<64;luwCtr++){        
                // load text information to 8 bytes encrypter buffer...
                MemCpyEdsToMem((UINT8*)&lubTTxDataIn[0], (__eds__ UINT8*)&gubEdsMem1Buffer[luwCtr * 8],8);
                // encrypt 8 bytes to DES format...
                apiDES_DataEncrypt_Process(&lctx,lubIV,lubTTxDataIn,lubTTxDataOut);        
                // transfer per 8 bytes to 512 bytes encryption data buffer... 
                MemCpyMemToEds((UINT8*)&lub512TempBuffer[luwCtr * 8],(const UINT8*)&lubTTxDataOut[0],8);                         
                
               	// check USB user status...
                if(TT4_USB_VBUS_IO == 0){
                    PDF_Timer2Interrupt_LED_Blinker_Disable();
                    TT4_RED_LED_OFF;                    
                    DRVFS_FCLOSE(&logFile);
                    return(0);
                }     
            }
            
            // write to file IO stream...            
            DRVFS_FWRITE(&logFile,(const void *)lub512TempBuffer,512u,&luwByteToWrite); // load 512 bytes..
    
            // get the excess bytes from the accumulated formatted text string datapoint...
            if(luwExtraByte>0){
                // shift the topmost remaining data down to index 0...        
                MoveEdsMemFromTopToIndex0((__eds__ UINT8*)&gubEdsMem1Buffer[0],512u,luwExtraByte);
            }

            // next starting index of EDS RAM...
            luwTotalBytesInRAM = luwExtraByte;            
        }    
    } // end of fetch datapoint accumulator loop...
    
    
    //------------------------------------------------
    // Region 3 : Footer Encrypt and Save to file 
    //------------------------------------------------
    
    // retrieve the accumulated text data string from a 4096 EDS buffer RAM and assign to region 3...
    appTTV_TextData_Region3(&luwTotalBytesInRAM);    

    // get the number of loops per 8 bytes...
    luwNumOfLoops = luwTotalBytesInRAM / 8u;
    // get the last extra bytes that must be less than 8 bytes...
    luwExtraByte = luwTotalBytesInRAM % 8u;
    
    // reset to zero RAM byte counter....   
    luwRAMByteCtr = 0;
    // Region 3 process for encryption and write to flash....
    for(luwCtr=0;luwCtr<luwNumOfLoops;luwCtr++){
        // get 8 bytes from EDS RAM for encryption...
        appTTV_GetTextDataFromRAM(lubTTxDataIn,&luwRAMByteCtr);
        // encrypt 8 bytes to DES format...
        apiDES_DataEncrypt_Process(&lctx,lubIV,lubTTxDataIn,lubTTxDataOut);

        // write to file IO stream...        
        DRVFS_FWRITE(&logFile,(const void *)lubTTxDataOut,8u,&luwByteToWrite); // load 512 bytes..        
        
       	// check USB user status...
        if(TT4_USB_VBUS_IO == 0){
            PDF_Timer2Interrupt_LED_Blinker_Disable();
            TT4_RED_LED_OFF;            
            DRVFS_FCLOSE(&logFile);
            return(0);
        }     
    } // end of Region 3 loop...       
    
    // get the last extra bytes for padding....
    if(luwExtraByte > 0){            
        // have extra byte..
        appTTV_GetTextDataFromRAM(lubTTxDataIn,&luwRAMByteCtr);        
        lubPadding = 8 - luwExtraByte % 8;        
    }
    else{        
        // no extra byte..
        lubPadding = 8;
    } // end of last extra bytes...
    
    // fill padding values...
    memset((void *)&lubTTxDataIn[luwExtraByte],lubPadding,lubPadding);

    // encrypt 8 byte data...
    apiDES_DataEncrypt_Process(&lctx,lubIV,lubTTxDataIn,lubTTxDataOut);
        
    // write to file IO stream...    
    DRVFS_FWRITE(&logFile,(const void *)lubTTxDataOut,8u,&luwByteToWrite); // load 512 bytes..    
    
    // flose file....
    DRVFS_FCLOSE(&logFile);    
    
    // terminate timer interrupt RED LED blinker..
    PDF_Timer2Interrupt_LED_Blinker_Disable();
    // forced turned off RED LED...
    TT4_RED_LED_OFF;
    
    return(TRUE);
}



/*******************************************************************************
**  Function     : appTTV_QuickString_TrueOrFalse
**  Description  : boolean result converts to text string as true or false.
**  PreCondition : none
**  Side Effects : none
**  Input        : boolean 1 or 0
**  Output       : text result as "True" or "False"
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void appTTV_QuickString_TrueOrFalse(UINT8 par_ubCond1,UINT8 par_ubCond2, char *par_cStr){
    
    char lstrTF[6];

    if(par_ubCond1 == par_ubCond2)
        strcpy(lstrTF,"True");    
    else
        strcpy(lstrTF,"False");    
        
    strcpy(par_cStr,lstrTF);
}    

/*******************************************************************************
**  Function     : appTTV_QuickString_GetStrLength
**  Description  : counts a number of character to a string
**  PreCondition : none
**  Side Effects : none
**  Input        : string text data
**  Output       : returns number of characters
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
UINT16 appTTV_QuickString_GetStrLength(char *par_cStr){
    
    register UINT16 lubCtr = 0;
    
    while(par_cStr[lubCtr])++lubCtr;
    
    return(lubCtr);
}


/*******************************************************************************
**  Function     : appTTV_QuickString_AlarmType
**  Description  : converts alarm data raw information text string
**  PreCondition : none
**  Side Effects : none
**  Input        : various alarm data 
**  Output       : pass by reference character string
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void appTTV_QuickString_AlarmType(UINT8 par_ubAlarmNum, 
                                  UINT8 par_ubAlarmType, 
                                  UINT32 par_udwThreshold, 
                                  UINT8 par_ubEnabled, 
                                  UINT8 par_ubTriggered, 
                                  float par_fRange1High, 
                                  float par_fRange1Low, 
                                  float par_fRange2High, 
                                  float par_fRange2Low,
                                  char *par_cStr){
    
    char lstrEnabled[7],lstrTriggered[7],lstrEndSting[3];
    UINT16 luwStrLength;
    
    appTTV_QuickString_TrueOrFalse(par_ubEnabled,1,lstrEnabled);
    appTTV_QuickString_TrueOrFalse(par_ubTriggered,1,lstrTriggered);
   
    // for last Alarm format only, exclusively in Alarm 6 to comply the TTV formatting... 
    if((par_ubAlarmType == TIME_ONLY)&&(par_ubAlarmNum == ALARM_6))
        strcpy(lstrEndSting," /"); // 
    else
        strcpy(lstrEndSting,"");
      
    // starting string address 0... mjm
    // Alarm settings..    
    sprintf(par_cStr,"<MultiAlarm AlarmNumber=\"%u\" Type=\"%u\" Threshold=\"%u\" Enabled=\"%s\" LogicalEqual=\"\" Triggered=\"%s\"%s>",par_ubAlarmNum+1,
                                                                                                                    par_ubAlarmType,
                                                                                                                    (UINT16)par_udwThreshold,
                                                                                                                    lstrEnabled,
                                                                                                                    lstrTriggered,
                                                                                                                    lstrEndSting);
    // concatenate to next string starting address ..... mjm
    luwStrLength = appTTV_QuickString_GetStrLength((char*)&par_cStr[0]);
    
    // Alarm Ranges..    
    switch(par_ubAlarmType){
    case(HTL_SE): // alarm type 1
    case(HTL_CE): // alarm type 2
    case(MKT_HI): // alarm type 10
        sprintf((char*)&par_cStr[luwStrLength],"<AlarmRanges><AlarmRange RangeNumber=\"1\" LowRange=\"\" HighRange=\"%.1f\" /></AlarmRanges>",(double)par_fRange1High);
        luwStrLength += appTTV_QuickString_GetStrLength((char*)&par_cStr[luwStrLength]);
        break;
    case(LTL_SE): // alarm type 3
    case(LTL_CE): // alarm type 4
    case(MKT_LO): // alarm type 11
        sprintf((char*)&par_cStr[luwStrLength],"<AlarmRanges><AlarmRange RangeNumber=\"1\" LowRange=\"%.1f\" HighRange=\"\" /></AlarmRanges>",(double)par_fRange1Low);
        luwStrLength += appTTV_QuickString_GetStrLength((char*)&par_cStr[luwStrLength]);
        break;
    case(STR_SE): // alarm type 5
    case(STR_CE): // alarm type 6
        sprintf((char*)&par_cStr[luwStrLength],"<AlarmRanges><AlarmRange RangeNumber=\"1\" LowRange=\"%.1f\" HighRange=\"%.1f\" /></AlarmRanges>",(double)par_fRange1Low,(double)par_fRange1High);
        luwStrLength += appTTV_QuickString_GetStrLength((char*)&par_cStr[luwStrLength]);
        break;
    case(DTR_SE): // alarm type 7
    case(DTR_CE): // alarm type 8
        sprintf((char*)&par_cStr[luwStrLength],"<AlarmRanges><AlarmRange RangeNumber=\"1\" LowRange=\"%.1f\" HighRange=\"%.1f\" /><AlarmRange RangeNumber=\"2\" LowRange=\"%.1f\" HighRange=\"%.1f\" /></AlarmRanges>",(double)par_fRange1Low,(double)par_fRange1High,(double)par_fRange2Low,(double)par_fRange2High);       
        luwStrLength += appTTV_QuickString_GetStrLength((char*)&par_cStr[luwStrLength]);
        break;
    case(TIME_ONLY): // alarm type 9       
        // nothing to print!!... mjm
        break;
    /*    
    case(MKT_HI): // alarm type 10
        sprintf((char*)&par_cStr[luwStrLength],"<AlarmRanges><AlarmRange RangeNumber=\"1\" LowRange=\"\" HighRange=\"%.1f\" /></AlarmRanges>",(double)par_fRange1High);
        luwStrLength += appTTV_QuickString_GetStrLength((char*)&par_cStr[luwStrLength]);
        break; 
    case(MKT_LO): // alarm type 11
        sprintf((char*)&par_cStr[luwStrLength],"<AlarmRanges><AlarmRange RangeNumber=\"1\" LowRange=\"%.1f\" HighRange=\"\" /></AlarmRanges>",(double)par_fRange1Low);
        luwStrLength += appTTV_QuickString_GetStrLength((char*)&par_cStr[luwStrLength]);
        break;    
    */    
    }
    
    luwStrLength += appTTV_QuickString_GetStrLength((char*)&par_cStr[luwStrLength]);
    // for last Alarm format only, exclusively in Alarm 6 to comply the TTV formatting... 
    if(!((par_ubAlarmType == TIME_ONLY)&&(par_ubAlarmNum == ALARM_6))){
        // end string address...
        luwStrLength += appTTV_QuickString_GetStrLength((char*)&par_cStr[luwStrLength]);    
        sprintf((char*)&par_cStr[luwStrLength],"</MultiAlarm>");    
    }
}


/*******************************************************************************
**  Function     : TTV_OriginatorNotes
**  Description  : retreives EEPROM data originator notes.
**  PreCondition : none
**  Side Effects : none
**  Input        : pass by reference string data.
**  Output       : string result with originator notes information.
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void TTV_OriginatorNotes(char *par_strValue){
    
    register UINT16 luwCtr;    
    register UINT16 luwPageNumber;
    UINT8 lubData;
    
    //-----------------------------------------------------------
    // retrieve User Information Area from EEPROM .... 
    //-----------------------------------------------------------         
    luwPageNumber = 160; // user information page offset starts at 160...
    
    luwCtr = 0;
    do{
        devEEPROM_RandomRead(luwPageNumber+luwCtr,&lubData); 
        // Conditions
        // 1.) Maximum characters: 96, Peter required 96 chars regardless the 48 chars stated in the FRS released last 9/21/10... mjm
        // 2.) Null character exits the loop.
        if((lubData==0)||(luwCtr>=TTV_ORIGINATOR_NOTES_MAX_CHARS)) break;
        
        par_strValue[luwCtr] = (UINT8)lubData;                
        
        ++luwCtr;
        
    }while(1);
    
    par_strValue[luwCtr] = 0; // null terminator...                                        
}


/*******************************************************************************
**  Function     : TTV_3PointCertClearEEPROMData
**  Description  : clears 3 point certificate data information in EEPROM.
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void TTV_3PointCertClearEEPROMData(void){
    
    // three point certificate...    
    char lstrBuffer[25];    
    UINT8  lubTemp = 0;
    UINT16 luwTemp = 0;
    
    
    utilArrayVarInitializer((UINT8 *)&lstrBuffer[0],24,32); // 25 bytes set to 32 equiv to space in ASCII.....
        
    
    // Tester name...
    utilWriteEEPROM_ByteValue(448u,22,(const void*)&lstrBuffer[0]);
    
    // Date validated...
    utilWriteEEPROM_ByteValue(470u,10,(const void*)&lstrBuffer[0]); // date..
    utilWriteEEPROM_ByteValue(480u,11,(const void*)&lstrBuffer[0]); // time..
    
    
    // NIST Tester Station name...
    utilWriteEEPROM_ByteValue(544u,1,(const void*)&lubTemp); 
    // NIST Chamber name
    utilWriteEEPROM_ByteValue(545u,8,(const void*)&lstrBuffer[0]);
    // NIST Logger name
    utilWriteEEPROM_ByteValue(553u,20,(const void*)&lstrBuffer[0]);    
    
    
    // Pass or Fail..    
    utilWriteEEPROM_ByteValue(542u,1,(const void*)&lubTemp); // 0-fail / 1-pass               
    
    
    // standard reading point 1..    
    utilWriteEEPROM_ByteValue(497u,2,(const void*)&luwTemp); 
    // standard reading point 2..    
    utilWriteEEPROM_ByteValue(499u,2,(const void*)&luwTemp); 
    // standard reading point 3..    
    utilWriteEEPROM_ByteValue(501u,2,(const void*)&luwTemp); 
      

    // tolerance point 1..    
    utilWriteEEPROM_ByteValue(491u,1,(const void*)&lubTemp); 
    // tolerance point 2..    
    utilWriteEEPROM_ByteValue(492u,1,(const void*)&lubTemp); 
    // tolerance point 3..    
    utilWriteEEPROM_ByteValue(493u,1,(const void*)&lubTemp); 
        
    
    // POINT 1 reading..    
    utilWriteEEPROM_ByteValue(503u,2,(const void*)&luwTemp); 
    // variance..    
    utilWriteEEPROM_ByteValue(494u,1,(const void*)&lubTemp); 
        
        
    // POINT 2 reading..    
    utilWriteEEPROM_ByteValue(505u,2,(const void*)&luwTemp); 
    // variance..    
    utilWriteEEPROM_ByteValue(495u,1,(const void*)&lubTemp); 
    
        
    // POINT 3 reading..    
    utilWriteEEPROM_ByteValue(507u,2,(const void*)&luwTemp); 
    // variance..    
    utilWriteEEPROM_ByteValue(496u,1,(const void*)&lubTemp); 
    
    
}


/*******************************************************************************
**  Function     : appTTV_TextDataLoadToRAM
**  Description  : writes array string data to EDS memory buffer.
**  PreCondition : none
**  Side Effects : none
**  Input        : array string data and number of string characters.
**  Output       : outputs an accumulated number of string characters.
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void appTTV_TextDataLoadToRAM(const char *par_String, UINT16 *par_uwIncrementalOffset){
    
    register UINT16 luwCtr;
    UINT16 luwOffset;
    UINT8 lubData;
    
    luwOffset = *par_uwIncrementalOffset;
    luwCtr = 0;
    while(par_String[luwCtr]!=0){
        lubData = (UINT8)par_String[luwCtr];        
        gubEdsMem1Buffer[luwOffset+luwCtr] = lubData;
        ++luwCtr;
    }
    
    // update offset location for the next start byte...
    *par_uwIncrementalOffset += luwCtr;
}


/*******************************************************************************
**  Function     : appTTV_GetTextDataFromRAM
**  Description  : retrieves array string data from EDS memory buffer.
**  PreCondition : none
**  Side Effects : none
**  Input        : array string data and number of string characters.
**  Output       : outputs an accumulated number of string characters.
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void appTTV_GetTextDataFromRAM(UINT8 *par_String, UINT16 *par_uwIncrementalOffset){
    
    UINT16 luwCtr;
    UINT8 lubData;
    UINT16 luwOffset;
    
    luwOffset = *par_uwIncrementalOffset;
    luwCtr = 0;
    do{         
        lubData = gubEdsMem1Buffer[luwOffset+luwCtr];
        par_String[luwCtr] = lubData;
        ++luwCtr;
    }while(luwCtr<8);
    
    // update offset location for the next start byte...
    *par_uwIncrementalOffset += luwCtr;
}

/*******************************************************************************
**  Function     : MemCpyEdsToMem
**  Description  : copies EDS array string data to linear memory buffer.
**  PreCondition : none
**  Side Effects : none
**  Input        : array string data and number of string characters.
**  Output       : outputs an accumulated number of string characters.
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void MemCpyEdsToMem(UINT8 *par_ubDest, __eds__ UINT8 *par_ubEdsSource, UINT16 par_uwSize){
    
    register UINT16 luwCtr;
    
    luwCtr = 0;
    while(luwCtr < par_uwSize){
         par_ubDest[luwCtr] = par_ubEdsSource[luwCtr];
         ++luwCtr;
    }     

}

/*******************************************************************************
**  Function     : MemCpyMemToEds
**  Description  : copies linear array string data to EDS memory buffer.
**  PreCondition : none
**  Side Effects : none
**  Input        : array string data and number of string characters.
**  Output       : outputs an accumulated number of string characters.
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void MemCpyMemToEds(__eds__ UINT8 *par_ubEdsDest, const UINT8 *par_ubSource, UINT16 par_uwSize){

    register UINT16 luwCtr;
    
    luwCtr = 0;
    while(luwCtr < par_uwSize){
         par_ubEdsDest[luwCtr] = par_ubSource[luwCtr];
         ++luwCtr;
    }     
}


/*   End Of File   */


