/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm                                            **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc. and Blue Ocean Innovation Ltd.                            **
 **                                                                           **
 **  Copryright(c) 2010. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Blue Ocean Innovation Ltd.                        **
 **  Project#            :  449                                               **
 **  Creation date       :  08/20/2010                                        **
 **  Programmer          :  Michael J. Mariveles                              **
 **                                                                           **
 **  -------------                                                            **
 **  CPU Details:                                                             **
 **  -------------                                                            **
 **  MCU Manufacturer    :  Microchip                                         **
 **  MCU P/N             :  PIC24FJ256GB210                                   **
 **  Pin count           :  64                                                **
 **  Core                :  16 Bit, PIC24F series                             **
 **  Program Memory Type :  Flash                                             **
 **  Program Memory Size :  256kB                                             **
 **  RAM size            :  32Kb Linear MEM, 64kB EDS MEM                     **
 **  Language            :  C                                                 **
 **  Dev't Environtment  :  MPLAB IDE v8.6                                    **
 **  Compiler            :  MPLAB C30 v3.24                                   **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : main.c                                                            **
 **                                                                           **
 **  Description :                                                            **
 **    - main program to launch and execute applications routines.            **
 **                                                                           **
 **  History :                                                                ** 
 **    ver 1.00.038  -  04/19/2011                                            **
 **    ver 1.00.039  -  07/13/2011                                            ** 
 **                                                                           **
 ******************************************************************************/

#include "hwPIC24F.h"
#include "hwSystem.h"
#include "devI2CLCD_BU9796FS.h"
#include "utility.h"
#include "commTT4Config.h"
#include "USB.h"                      
#include "hwUSB_TT4Tasks.h"
#include "appTT4_MA.h"
#include "drvRTC.h"
#include "drvUSB.h"
#include "coreTT4USBMA.h"

#define RESET_RECOVERY      1               //1 = enable reset recovery code

//==============================================================================
// Hardware Configuration Bits
//==============================================================================

    //---------------------------------
    // Configuration Bit 3...
    //---------------------------------            
    _CONFIG3( WPEND_WPSTARTMEM  & 
              WPCFG_WPCFGDIS    &                 
              WPDIS_WPDIS       &              
              SOSCSEL_LPSOSC    )

    //---------------------------------
    // Configuration Bit 2...
    //---------------------------------            
    
    _CONFIG2( IESO_OFF     &  
              PLLDIV_DIV5  &  
              PLL96MHZ_ON  &  
              FNOSC_PRIPLL &
              FCKSM_CSECMD &  
              OSCIOFNC_ON  &
              IOL1WAY_ON   &                            
              POSCMOD_XT   )  
                  
    //---------------------------------
    // Configuration Bit 1...
    //---------------------------------            
    _CONFIG1( JTAGEN_OFF & 
              GCP_OFF    & 
              GWRP_OFF   &
             // 0xF7FF     &
              ICS_PGx3   &  
              FWDTEN_OFF & 
              WINDIS_OFF &
              FWPSA_PR32 &              
              WDTPS_PS1  )   


//==============================================================================
// global variable declaration....
//==============================================================================

// temperary storage for memory transfer buffer...
UINT8   gubMem;
UINT16  guwMem;
UINT32  gudwMem;


// EEPROM configuration control structure register...
extern __SENSITECH_CONFIGURATION_AREA  _gsSensitech_Configuration_Area;
extern __USER_CONFIGURATION_AREA       _gsUser_Configuration_Area;

// EEPROM control bit manipulator structure register...
extern __sBITS_GENCFGOPT_BYTE _gsGenCfgOptByte;
extern __sBITS_EXTOPT_BYTE    _gsExtdOptByte;
// command byte bit manipulator control register....
extern __sBITS_COMMAND_BYTE   _gsCommandByte;

//for debug
extern __ALARM_CONFIGURATION_AREA      _gsAlarmConfig[6];

// Timer Interrupt Toggle Flag...
extern volatile UINT8 gubTimerEventFlag;
// Button depress seconds counter..
extern volatile UINT8 gubButtonEventFlag;
// download time storage...
extern volatile UINT32 gudwDeviceStoppedDownloadTime;
extern UINT32 gudwTT4MA_PCTime;

// Stop mode status register...
extern UINT8 gubStopDeviceStatus;
// core tasking control byte... mjm
extern __sDEVICETASK _gsTT4Core; 
// LCD summary status register...
extern UINT8 gubLCDSummaryStatus;
// Blink flag global status flag...
extern BOOL gbUpdateLCDFlag;


// PDF and TTV file generation status...
extern BOOL gbPDFFileResetStatusFlag;
extern BOOL gbTTVFileResetStatusFlag;

// PDF file generation success status flag...
extern BOOL gbPDFFileGenSuccessStatusFlag;
// TTV file generation success status flag...
extern BOOL gbTTVFileGenSuccessStatusFlag;


// Global USB Descriptor control..
extern _eUSB_Descriptors _geUSB_SetDescriptor;

// configuration was monitoring var...
extern BOOL gbTT4USB_Config_OK;

extern BOOL gbTT4AutoStartActive;

//----------------------------------------------------
// Timer1 counter timer global storage registers..
//----------------------------------------------------
// measurement interval timer counter...
extern volatile UINT32 gudwMeasurementIntervalTimer;
// Triggered Alarm cycle timer counter...
extern volatile UINT8 gubTrigAlarmCycleTimer;
// Heartbeat Icon timer counter ...
extern volatile UINT8 gubHeartIconTimer;
// LCD summary timer counter register...
extern volatile UINT8 gubLCDSummaryTimer;
// Stop button depress delay timer counter register...
extern volatile UINT8 gubStopButtonDelayTimer;
// Stop button depress for re-configuration...
extern volatile UINT8 gubStopButtonReConfigTimer;
// Configuration mode timer counter register...
extern volatile UINT8 gubCFModeTimer;


//----------------------------------------------------
// MKT math varaibles..
//----------------------------------------------------
extern volatile long double gudwMKT_Accumulator;
extern volatile long double gudwMKT_Value;
extern volatile float gfMKT_RunningTemp;
//----------------------------------------------------
// reset recovery variables
//----------------------------------------------------
BOOL gbResetRecoveryFlag;
UINT8 gubHeartBeatOn;
extern UINT32 gudwTT4MA_CurrentTime;
extern BOOL gbInitMBRStatusFlag;
extern BOOL gbPDFFileResetStatusFlag;
extern BOOL gbTTVFileResetStatusFlag;
extern BOOL gbUSBFileGenSuccessStatusFlag;
// USB MSD read/write control register...
extern volatile UINT32 gudwReadBlock;
extern volatile UINT32 gudwWriteBlock;
extern volatile UINT16 guwWriteSector;
extern volatile __eds__ UINT16 guwEdsCacheMem[16001u] __attribute__((space(eds)));
extern UINT8 gubAlarmDisplayStatus;
extern UINT8 gbBlinkToggleBit;
extern volatile UINT8 gubStartButtonDelayTimer;
//==============================================================================
  #pragma code c_main
//==============================================================================

    /*--------------------------------
     |    Main Program Starts Here!  |
     |                               |
     |    \\ mjmariveles...          |
     -------------------------------*/     

/*******************************************************************************
**  Function     : main
**  Description  : Main Core Loop, this is the program starting point.
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/     
int main(void){

    //--------------------------------------------------------------------------
    //  main variables declaration...   
    //--------------------------------------------------------------------------
    
    //reset source storage
    UINT8 lubResetSource;

    // button input status controls...
    UINT8 lubButtonStatus;
    BOOL lbButtonOnePass;
    
    // LCD display controls...
    UINT8 lubMarkerNextCycle;    
    BOOL lbButtonDrepressFromStart;

    //for raw temperature data recovery
    UINT16 luwTempValue;
    UINT16 par_uwDataPointNumber;

    UINT16 luwData;

    


    //--------------------------------------------------------------------------
    //  general system register initialization...   
    //--------------------------------------------------------------------------
    coreHwSystemInit();
    utilDelay_ms(100); 
     
    /*
    float ltest = -123.99565,lResult;    
    lResult = utilDbDec_RoundOFF(ltest);
    Nop();
    Nop();
    */

    // Set up RD4 - new reset pin
    TRISDbits.TRISD4 = 1;
    EnablePullUpCN13;Nop();
    
    //new TRUE RESET - START button and MCLR should be low
    //lubButtonStatus = appTT4MA_GetButtonStatus();
    //if((lubButtonStatus == BUTTON_STATUS_START_DEPRESSED) && (RCONbits.EXTR)){
    
    //RD4 and MCLR must be both low for a true reset
    if((PORTDbits.RD4 == 0) && (RCONbits.EXTR)){
        _gsSensitech_Configuration_Area.ubCommandByte = CMD_BYTE_UNCONFIG; // 16 - unconfigured mode bit value...
        gubMem = _gsSensitech_Configuration_Area.ubCommandByte;
        commTT4Cfg_FastWriteCfgEeprom(OFFSET_COMMAND_BYTE,(const void *)&gubMem);
        _gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints = 0;
        luwData = _gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints = 0;
        commTT4Cfg_WriteCfgEeprom(OFFSET_NUMBER_OF_STORED_DATAPOINTS,(UINT8 *)&luwData);
        gbResetRecoveryFlag = FALSE;
    }

    //===================== START OF RESET RECOVERY ==========================//
#if RESET_RECOVERY
    //Save reset source in byte 24 of Configuration Memory for debug/test
    lubResetSource = ( ((RCONbits.BOR) <<0) |           // Bit 0: Power On Reset
                       ((RCONbits.POR) <<1) |           // Bit 1: Brown-out Reset
                       ((RCONbits.WDTO)<<2) |           // Bit 2: Watchdog Time-out Reset
                       ((RCONbits.SWR) <<3) |           // Bit 3: SW RESET Instruction
                       ((RCONbits.EXTR)<<4) |           // Bit 4: External Reset
                       ((RCONbits.CM)  <<5) |           // Bit 5: Configuration Mismatch Reset
                       ((RCONbits.IOPUWR) << 6) |       // Bit 6: Illegal Opcode or Uninitialized W Register Access
                       ((RCONbits.TRAPR) << 7)          // Bit 7: Trap Conflict event
                     );
    commTT4Cfg_WriteCfgEeprom(OFFSET_NOT_USED_1,(UINT8 *)&lubResetSource);

    RCON = 0x00;

    gbResetRecoveryFlag = TRUE;

    //Comment out the next line if we want to recover from all resets
    //if(!(RCONbits.BOR || RCONbits.POR))
    {
        // set system oscillator control register...
        OSCCON = 0x3302;
        // set clock divisor 2 to operate at 16Mhz system clock...
        CLKDIV = 0x0040; // 32MHz / 2 = 16Mhz System Clock...
        Nop();Nop();Nop();
        guwUtilSysClk_mDly = UTIL_MDELAY_SYSCLK_16MHZ;

        // get configuration status from EEPROM and load to TT4USB-MA memory..
        commTT4Cfg_LoadMem_EEPROM_ConfigurationStatus();
        utilDelay_ms(10);
         
        //re-initialize RTC registers except for YEAR, MTHDY, WKDYHR, MINSEC, ALMTHDY, ALWDHR and ALMINSEC registers
        //because their values are preserved during reset (except for POR)
        drvRTC_SetupRTCC();

        //update current time
        gudwTT4MA_CurrentTime = drvRTC_ReadRTCC_CurrentTime();
        utilDelay_ms(10);
        //for brand new unit that has not been through "true reset" number of data points may be set to 0xFFFF
        //so clear number of data points if value is greater than maximum possible data points
        if( _gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints > 16000u)
            _gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints = 0;
        
        //cache data gets cleared by reset
        //re-populate cache with raw temperature data from EEPROM
        for(par_uwDataPointNumber=0;par_uwDataPointNumber<_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints;par_uwDataPointNumber++)
        {
            commTT4Cfg_ReadEeprom_RawDataPoint(par_uwDataPointNumber,&luwTempValue);
            guwEdsCacheMem[par_uwDataPointNumber] = luwTempValue;
        }

        // MSD file transfer control vars...
        guwWriteSector = 0;
        gudwWriteBlock = gudwReadBlock = 0xFFFFFFFF; // reset block mjm...
        hwInit_CPUCtrlRegister_USBMode();
        gubMem = _gsSensitech_Configuration_Area.ubCommandByte;
        switch(gubMem)
        {
            case CMD_BYTE_STOPPED:
                gubStopDeviceStatus = _DEVICE_STOP_STATUS_SLEEP_;
                gbInitMBRStatusFlag = TRUE;
                appTT4MA_Timer1Interrupt_Enable();          //re-enable timer interrupts
                utilDelay_ms(10);                       
                // Enable Start/Stop Button and Vbus detection WakeUp Interrupt for fast response....
                appTT4MA_WakeUpInterrupts_Enable();
                goto LABEL_DEVICE_STOP_MODE;
                break;
            case CMD_BYTE_SLEEP:
                goto LABEL_DEVICE_SLEEP_MODE_STATUS;
                break;
            case CMD_BYTE_RUNNING:
                devLCD_Display_AllSegments(OFF_SEGS);       //clear display
                devLCD_Display_SystemRunningIcons();        //update system running icons
                devLCD_Display_ReadTemperature();           //display last temperature reading
                devLCD_UpdateDisplay();                     //update display
                appTT4MA_Timer1Interrupt_Enable();          //re-enable timer interrupts
                utilDelay_ms(10);
                // Enable Start/Stop Button and Vbus detection WakeUp Interrupt for fast response....
                appTT4MA_WakeUpInterrupts_Enable();
                // Set default to MSD mode...
                _geUSB_SetDescriptor = _eMSD_;
                gubStopDeviceStatus = _DEVICE_STOP_STATUS_GO_USB_;

                // clear LCD summary task flags
                gubLCDSummaryStatus = 0;
                _gsTT4Core.Task.Req_LCDSummary = REQ_DONE;
                _gsTT4Core.Task.Proc_LCDSummary = PROC_DONE;
                // clear marker task flag status
                _gsTT4Core.Task.Req_Marker = REQ_DONE;
                _gsTT4Core.Task.Proc_Marker = PROC_DONE;
                 // reset CF just in case unit is in CF mode when reset occurred
                _gsTT4Core.Task.Req_CfgDevice = REQ_DONE;
                _gsTT4Core.Task.Proc_CfgDevice = PROC_DONE;

                
                goto LABEL_DEVICE_RUNNING_MODE_STATUS;
                break;
            case CMD_BYTE_STRDLY:
                goto LABEL_AUTO_START_RUNNING;
                break;
            case CMD_BYTE_UNCONFIG:
            default:
                break;
        }
    }

    //====================== END OF RESET RECOVERY ===========================//
#endif
 //=================================
    LABEL_DEVICE_UNCONFIG_MODE_STATUS:
 //=================================

    // before going to unconfigured state must initialize according to their 
    // unconfigured state system requirement....
    coreInitUnconfiguredState();

    //--------------------------------------------------------------------------
    //  Level 1 Device Status - Device Unconfigured Mode State loop...
    //--------------------------------------------------------------------------
    coreDeviceUnconfiguredState(); // uncomment this if preset values for debugging is not used...
    utilDelay_ms(10);
    
    
    ///////////////////////////////////////////////////////////////////////////
    // Test debug offset.... mjm 10052012...
    /*
    
    UINT16 lubDataA,lubDataB,lubDataC,luA,luB,luC;
    INT8   lA,lB,lC;
    
    lubDataA = 0;//0.5
    commTT4Cfg_WriteCfgEeprom(SENSOR1_CAL_OFFSET_A,(UINT8 *)&lubDataA);
    lubDataB = 0;//1.5
    commTT4Cfg_WriteCfgEeprom(SENSOR1_CAL_OFFSET_B,(UINT8 *)&lubDataB);
    lubDataC = 0;//11.1
    commTT4Cfg_WriteCfgEeprom(SENSOR1_CAL_OFFSET_C,(UINT8 *)&lubDataC);
    
    commTT4Cfg_FastReadCfgEeprom(SENSOR1_CAL_OFFSET_A,(void *)&luA);
    lA = (INT8)luA;
    Nop();
    commTT4Cfg_FastReadCfgEeprom(SENSOR1_CAL_OFFSET_B,(void *)&luB);
    Nop();
    lB = (INT8)luB;
    commTT4Cfg_FastReadCfgEeprom(SENSOR1_CAL_OFFSET_C,(void *)&luC);
    lC = (INT8)luC;
    Nop();
    */
    
    ///////////////////////////////////////////////////////////////////////////

 //=================================
    LABEL_DEVICE_SLEEP_MODE_STATUS:
 //=================================   

    // before taking sleep mode must initialize its sleep mode state.....
    coreInitSleepModeState();

    //---------------------------------------------------------------------------------------------        
    //  Level 2 Device Status - Device Sleep Mode State loop...
    //---------------------------------------------------------------------------------------------        
    // intercept the command byte result from unconfigured state if Running or not!...
    if(_gsSensitech_Configuration_Area.ubCommandByte != CMD_BYTE_RUNNING){ 
        gbTT4AutoStartActive = FALSE;
        coreDeviceSleepModeState();
    }    
    else{
        gbTT4AutoStartActive = TRUE;
        goto LABEL_AUTO_START_RUNNING;   
    }    

    // check the device mode from exiting StartUp status if the user just 
    // configured the device then to back to TOP at device sleep mode state... mjm
    if(gbTT4USB_Config_OK == TRUE) goto LABEL_DEVICE_SLEEP_MODE_STATUS;


    //============================
       LABEL_AUTO_START_RUNNING:
    //============================

    //---------------------------------------------------------------------------------------------        
    //  Level 3 Device Status - StartUp Delay value if not equal to zero!...  mjm
    //---------------------------------------------------------------------------------------------        
    if(_gsUser_Configuration_Area.udwStartUpDelay > 0){
        
        // since the StartUp Delay is greater then zero then update command byte status to StartUp delay mode...
        _gsSensitech_Configuration_Area.ubCommandByte = CMD_BYTE_STRDLY; // 8 - start-up delay mode bit value...    
        
        // update EEPROM and write command byte start-up delay bit !...
        gubMem = _gsSensitech_Configuration_Area.ubCommandByte;
        commTT4Cfg_FastWriteCfgEeprom(OFFSET_COMMAND_BYTE,(const void *)&gubMem);
        
        // go to StartUp delay state entry and start counting down.....
        coreDeviceStartUpDelayState();
        
    }     
    else{
        
        // get the RTCC system clock actual time...
        // Note: 1.) the global current time in seconds counter will be updated
        //           and synchronized in RTCC system clock....
        //       2.) this is the starting reference time from the Sleep Mode...
        //
        // \mjmariveles.....
        drvRTC_ReadRTCC_UpdateCurrentTimeSecs();    
        
        // initialize Timer Interrupt...
        // Note: do not turn OFF this timer interrupt till the unit is stopped!
        //       otherwise it will cause a timing error in not synchronizing 
        //       the system clock RTCC...... mjmariveles     
        appTT4MA_Timer1Interrupt_Enable();  
        utilDelay_ms(10);      
    }
    
    // check the device mode from exiting StartUp status if the user just 
    // configured the device then to back to TOP at device sleep mode state... mjm
    if(gbTT4USB_Config_OK == TRUE) goto LABEL_DEVICE_SLEEP_MODE_STATUS;
        
    //===========================================================    
    //
    //  TT4USB Activated and now is in running mode status..
    //
    //===========================================================    


    utilDelay_ms(10);// preventing timing overrun if any previous EEPROM activity launched!...

    //---------------------------------------------------------------------------
    // decrement customer number of reset everytime at the 1st datapoint...
    //
    // Note: this resets decrementation will be taken after StartUp delay and before  
    //       the 1st temperature point has been taken. This procedure was advised
    //       by Peter of Sensitech the time he was is China to debug units.  
    //
    //       \mjmariveles.... 
    //---------------------------------------------------------------------------
    if(_gsSensitech_Configuration_Area.uwNumberOfCustomerResets>0)
        --_gsSensitech_Configuration_Area.uwNumberOfCustomerResets;
    else
        _gsSensitech_Configuration_Area.uwNumberOfCustomerResets = 0;
        
    guwMem = _gsSensitech_Configuration_Area.uwNumberOfCustomerResets;    
    commTT4Cfg_FastWriteCfgEeprom(OFFSET_CUSTOMER_RESETS,(void *)&guwMem);

    
    utilDelay_ms(10);// preventing timing overrun if any previous EEPROM activity launched!...
    
    //----------------------------------------------------------------------------------------
    // before taking 1st data point as datapoint 0 at start time then must initialize 
    // global vars and update command byte status as system pre-startup initialization....
    //----------------------------------------------------------------------------------------
    coreInitStartTT4();

    // Enable Start/Stop Button and Vbus detection WakeUp Interrupt for fast response....
    appTT4MA_WakeUpInterrupts_Enable();
    
    //----------------------------------------------------------------------------------------
    // here is the first data point activation status..... 
    //----------------------------------------------------------------------------------------
    
    // initialize MKT variables prior calling first point activation...
    gfMKT_RunningTemp = 0;
    gudwMKT_Accumulator = 0;     
    gudwMKT_Value = 0;

    coreFirstPointActivationStatus();

 //=================================
    LABEL_DEVICE_RUNNING_MODE_STATUS:
 //=================================
                
    // init button status to not depressed before entering the core loop...
    lubButtonStatus = BUTTON_STATUS_NOT_DEPRESSED;    
    // tracks if the user persistenly depress the buttom from sleep or startup delay...
    lbButtonDrepressFromStart = TRUE;
    // Enable Button detection Interrupt....    
    lbButtonOnePass = TRUE;
    // marker status bits...    
    lubMarkerNextCycle = 0;    
    
    // pre-delay before sleep mode...
    utilDelay_ms(10);
    //-----------------------------------------------------------------------
    // temperature acquisition infinite loop!...
    //-----------------------------------------------------------------------
    gudwTT4MA_CurrentTime = drvRTC_ReadRTCC_CurrentTime();
    gbResetRecoveryFlag = FALSE;
    do{
        //-----------------------------------------------------------------------------
        // to ensure the effecient RTC timing accuracy must clear all interrupt flags 
        // before going to sleep mode...
        // 
        // by: mjmariveles... 12162012..
        //
        //-----------------------------------------------------------------------------        
        // clear button CN interrupt flag...
        IFS1bits.CNIF = 0;    
        // clear button interrupt toggle flag confirmation.. 
        gubButtonEventFlag = 0;         
         
        // clear RTC Timer1 event interrupt flag...
        IFS0bits.T1IF = 0; 
        // clear RTC Timer1 event interrupt toggle flag confirmation.. 
        gubTimerEventFlag = 0; 
        //-----------------------------------------------------------------------------
        
        // clear reset control registers preparation for wakeUp...
        RCON = 0;
        
        // sleep MCU and wakessUp if any interrupt occuring events...
        // such as Button depression and Timer events..
        Sleep();
        // post-delay after wakeup
        Nop();Nop();Nop();        
       
        // put some delay till oscillator stabilizes at 10ms from sleep to wake UP...
        //utilDelay_ms(10);  // this was removed         
        
        //------------------------------------------------------------------------------
        // this is an interrupt trapping technique to wait until the interrupt toggle flag is set..
        // wait till have sufficient timing to received the interrupt timer flag is set...
        //
        // by: mjmariveles... 12162012
        while((gubTimerEventFlag==0)&&(gubButtonEventFlag==0));
            
        //========================================================================
        //  Timer Event Interrupt Flag... 
        //========================================================================
        if(gubTimerEventFlag == 1){     
            
            // update RTC system counters....
            appTT4MA_RTCSystemCounterUpdate();  
            
            // clear timer task flag....
            gubTimerEventFlag = 0;
            
            // this debug port determines the duration of process time before sleep mode... mjm..
            MI_TOGGLE_PORT = 1; // debug port high...    
            
            // check the blink mode if enabled!...
            devLCD_GetBlinkStatus();
            
            //==================================================================
            //
            // Process System Request 1Hz Periodic Tasks...
            //
            //==================================================================
            
            //------------------------------------
            // Temperature Measurement Interval....
            //------------------------------------                
            if(gudwMeasurementIntervalTimer >= _gsUser_Configuration_Area.udwMeasurementInterval){    
                
                gudwMeasurementIntervalTimer = 0;// resets everytime when the measurement interval counter time-out occured!...mjm
                // turn-on heart icon...
                if(_gsTT4Core.Task.Proc_CfgDevice != PROC_BUSY)
                    gubHeartBeatOn = devLCD_Display_HeartBeat(1);
                // re-start heartbeat icon display timer...
                gubHeartIconTimer = 0;                
             
                // read temperature and calculate running totals status...
                if(appTT4MA_TempAcquisitionMode() == MEMORY_FULL){
                    
                     gubStopDeviceStatus = _DEVICE_STOP_STATUS_SLEEP_;
                        
                     goto LABEL_DEVICE_STOP_MODE;
                }
                
                // in Config Display mode must skip the Temperature updating display mode...                                 
                if((_gsTT4Core.Task.Proc_CfgDevice  ||
                    _gsTT4Core.Task.Proc_LCDSummary ||
                    _gsTT4Core.Task.Proc_Marker) != PROC_BUSY){
                        
                    devLCD_Display_ReadTemperature();
                }
                
            } /*  End of Time Measurement Interval Periodic Task!... mjm */    
            
            
            // CF is dominant so if this is enabled then all periodic should be disabled temporarily!...
            if(_gsTT4Core.Task.Proc_CfgDevice != PROC_BUSY){
                
                if((gubHeartIconTimer>=HEART_BEAT_EXIT_TIMEOUT) && (gubHeartBeatOn)){
                    gubHeartBeatOn = devLCD_Display_HeartBeat(0);
                  
                }    

                //------------------------------
                // Arrow Mark....
                //------------------------------
                if(_gsTT4Core.Task.Req_Marker == REQ_ACTIVE){
                    
                    if(_gsTT4Core.Task.Req_LCDSummary != REQ_ACTIVE){
                            
                        switch(lubMarkerNextCycle){
                        case(0):
                            // set command byte marker flag.... 
                            _gsCommandByte.Bit.MarkerFlag = 1;
                            // set marker display...
                            devLCD_Display_ArrowMark(1);
                            //next cycle display... 
                            lubMarkerNextCycle = 1; 
                            // marker task flag status....
                            _gsTT4Core.Task.Proc_Marker = PROC_BUSY;
                        break;
                        case(1):
                            // clear marker display...
                            devLCD_Display_ArrowMark(0);
                            
                            // LCD summary is enabled then send task request...
                            if(_gsGenCfgOptByte.GenOpt.Byte1.LCDSumDatDispEna == 1){ 
                                // clear counter...
                                gubLCDSummaryTimer = 0;
                                // LCD summary information steps...
                                gubLCDSummaryStatus = 0;
                                // display the 1st level information...
                                devLCD_Display_Summary();   
                                // set task flag...
                                _gsTT4Core.Task.Req_LCDSummary = REQ_ACTIVE;
                                _gsTT4Core.Task.Proc_LCDSummary = PROC_BUSY;
                            }    
                            
                            //reset cycle display...
                            lubMarkerNextCycle = 0; 
                            // marker task flag status....
                            _gsTT4Core.Task.Req_Marker = REQ_DONE;
                            _gsTT4Core.Task.Proc_Marker = PROC_DONE;
                         break;  
                        }
                    }
                }
                            
                //------------------------------
                // LCD Summary time-out....
                //------------------------------
                if(_gsTT4Core.Task.Req_LCDSummary == REQ_ACTIVE){
                    
                    if(_gsGenCfgOptByte.GenOpt.Byte1.LCDSumDatDispEna == 1){
                        
                        _gsTT4Core.Task.Proc_LCDSummary = PROC_BUSY;
                
                        if(gubLCDSummaryTimer >= LCD_SUMMARY_EXIT_TIMEOUT){	
                    
                            // restore to Main LCD status display.... mjm                                
                            // clear all segments...    
                            devLCD_Display_AllSegments(OFF);                                        
                            // reset global LCD summary to start...
                            gubLCDSummaryStatus = 0;                  
                            // go to main display....
                            devLCD_Display_ReadTemperature();    
                            
                            // clear LCD summary task flag... 
                            _gsTT4Core.Task.Req_LCDSummary = REQ_DONE;
                            _gsTT4Core.Task.Proc_LCDSummary = PROC_DONE;
                        }
                    }   
                }     
            
                //------------------------------------------
                // Triggered Alarm Cycling time-out....
                //------------------------------------------                                
                if(_gsExtdOptByte.ExtOpt.Byte2.DispAlrmNumTrgAlrm == 1){
                    
                    if(_gsTT4Core.Task.Req_LCDSummary == REQ_DONE){
                        
                        if(gubTrigAlarmCycleTimer >= ALARM_CYCLE_TIMEOUT){    
                            gubTrigAlarmCycleTimer = 0;                        
                            devLCD_Display_CyclingTriggeredAlarm();
                        }                
                    }
                }
            
            } /* CDC dominant control region... */

            //------------------------------
            // CDC Mode time-out
            //------------------------------
            if(_gsTT4Core.Task.Req_CfgDevice == REQ_ACTIVE){
                
                _gsTT4Core.Task.Proc_CfgDevice = PROC_BUSY;
                
                if(gubCFModeTimer >= CDC_EXIT_TIMEOUT){
                
                    //reset button event interrupt flag...
                    gubButtonEventFlag = 0;
                    lubButtonStatus = BUTTON_STATUS_NOT_DEPRESSED;

                    // set Global USB Descriptor control..
                    _geUSB_SetDescriptor = _eMSD_;               
                    // clear display...
                    devLCD_Display_AllSegments(OFF_SEGS);        
                    
                    // display temperature reading...
                    devLCD_Display_ReadTemperature();
                    
                    // reset CF ...                 
                    _gsTT4Core.Task.Req_CfgDevice = REQ_DONE;
                    _gsTT4Core.Task.Proc_CfgDevice = PROC_DONE;                    
                }   
            }
            
            //MI_TOGGLE_PORT = 0; // debug port low...         
            
        }  /*  - End of 1Hz periodic tasks -  */
        
        
        

        //========================================================================
        //
        //  User Request Button Event or USB Event ... < Real Time Tasks > ....
        //
        //========================================================================     
            
        // while in CF mode all button event were blocked!!
        if(_gsTT4Core.Task.Proc_CfgDevice != PROC_BUSY){        
        
            // read button input status...
            if(gubButtonEventFlag==1) lubButtonStatus = appTT4MA_GetButtonStatus(); 
            
        
            //==================================================================
            // Detect Button Events...
            //==================================================================

            //-----------------------------
            // Button Not Depressed...
            //-----------------------------
            if(lubButtonStatus==BUTTON_STATUS_NOT_DEPRESSED){
                lbButtonDrepressFromStart = FALSE;
                //reset button event interrupt flag...
                gubButtonEventFlag = 0;
            }
                        
            //-----------------------------
            // START_STOP Button Depress...
            //-----------------------------
            if(lubButtonStatus == BUTTON_STATUS_STR_STP_DEPRESSED){

                if((_gsTT4Core.Task.Proc_LCDSummary!=PROC_BUSY)&&(_gsTT4Core.Task.Proc_Marker!=PROC_BUSY)){
                
                    //----------------------------------------------------
                    // Set USB to CDC mode ready for configuration...
                    //----------------------------------------------------                

                    
                    // set Global USB Descriptor control..
                    _geUSB_SetDescriptor = _eCDC_;
                    // clear all segments...    
                    devLCD_Display_AllSegments(OFF);                    
                    // display CF...
                    devLCD_Display_CF(1);
                    // reset timer...
                    gubCFModeTimer = 0;
                    // if CF is active then prevent all button depression event...                
                    // set tasks flag...
                    _gsTT4Core.Task.Req_CfgDevice = REQ_ACTIVE;
                    _gsTT4Core.Task.Proc_CfgDevice = PROC_BUSY;
                }    
                
            }  /* End of START/STOP button depress control events */          
            
            
            //-----------------------------
            // START Button Depressed...
            //-----------------------------
            if(lubButtonStatus == BUTTON_STATUS_START_DEPRESSED){
                
                if((lbButtonOnePass==TRUE)&&(lbButtonDrepressFromStart==FALSE)){
                    lbButtonOnePass=FALSE;
                    
                    if(_gsTT4Core.Task.Req_LCDSummary != REQ_ACTIVE){
                        _gsTT4Core.Task.Req_Marker = REQ_ACTIVE;                        
                    }
                    else{
                        // LCD summary bit if enabled!...
                        if(_gsGenCfgOptByte.GenOpt.Byte1.LCDSumDatDispEna == 1){
                            
                            // set task status flag...    
                            _gsTT4Core.Task.Proc_LCDSummary = PROC_BUSY;
                            
                            // reset LCD summary timer every time when button is depressed...
                            gubLCDSummaryTimer = 0;
    
                   	        if(devLCD_Display_Summary() == LCD_SUMMARY_DONE){
                                    
                                // restore to Main LCD status display.... mjm
                                // clear all segments...    
                                devLCD_Display_AllSegments(OFF);                    
                                // go to main display....
                                devLCD_Display_ReadTemperature();    
                                // reset global LCD summary to start...
                                gubLCDSummaryStatus = 0;
                                
                                // clear LCD summary task flag... 
                                _gsTT4Core.Task.Proc_LCDSummary = PROC_DONE;
                                _gsTT4Core.Task.Req_LCDSummary = REQ_DONE;
                            }                
                        } 
                    }  
                }
                
            } /* End of LCD summary button depress control events */
            
                        
            //-----------------------------
            // START Button Released...
            //-----------------------------
            if(lubButtonStatus == BUTTON_STATUS_START_RELEASED){                
                lbButtonOnePass = TRUE;                
                //reset button event interrupt flag...
                gubButtonEventFlag = 0;   
                lubButtonStatus = BUTTON_STATUS_NOT_DEPRESSED;             
            } /* End of LCD summary button release control events */
            
            
            //-----------------------------
            // STOP Button Depressed...
            //-----------------------------
            if(lubButtonStatus == BUTTON_STATUS_STOP_DEPRESSED){                
                
                if(_gsGenCfgOptByte.GenOpt.Byte0.StopKeyEna == 1){   
                    
                    if(_gsGenCfgOptByte.GenOpt.Byte1.StopKeyDlyEna == 1){
                    
                        if(_gsTT4Core.Task.Req_StopDevice != REQ_ACTIVE){
                            
                            _gsTT4Core.Task.Req_StopDevice = REQ_ACTIVE;
                            // init timer to zero...
                            gubStopButtonDelayTimer = 0;
                        }                 
                        
                        // 3 secs delay...                
                        if(gubStopButtonDelayTimer >= STOP_BUTTON_DELAY_TIMEOUT){
                         
                            gubStopDeviceStatus = _DEVICE_STOP_STATUS_SLEEP_;

                            //reset button event interrupt flag...
                            gubButtonEventFlag = 0;
                            lubButtonStatus = BUTTON_STATUS_NOT_DEPRESSED;
                               
                            // reset task flags...
                            _gsTT4Core.Task.Req_StopDevice = REQ_DONE;    
                            _gsTT4Core.Task.Proc_StopDevice = PROC_DONE;
                        
                            goto LABEL_DEVICE_STOP_MODE;
                        }
                                                        
                    }
                    else{         

                        //reset button event interrupt flag...
                        gubButtonEventFlag = 0;
                        lubButtonStatus = BUTTON_STATUS_NOT_DEPRESSED;                               
                        
                        gubStopDeviceStatus = _DEVICE_STOP_STATUS_SLEEP_;
                        
                        goto LABEL_DEVICE_STOP_MODE;
                    }   
                }
                
            }/* End of STOP button depress control events */
            

            //-----------------------------
            // STOP Button Released...
            //-----------------------------
            if(lubButtonStatus == BUTTON_STATUS_STOP_RELEASED){

                //reset button event interrupt flag...
                gubButtonEventFlag = 0;
                lubButtonStatus = BUTTON_STATUS_NOT_DEPRESSED;
                
                // reset task flags...
                _gsTT4Core.Task.Req_StopDevice = REQ_DONE;    
                _gsTT4Core.Task.Proc_StopDevice = PROC_DONE;                
                
            }/* End of STOP button release control events */
            
            
        } /*  End of Button Events... */    
    
        //  Update LCD Display Task...         
        if(gbUpdateLCDFlag == TRUE){
            if(_gsTT4Core.Task.Proc_CfgDevice != PROC_BUSY){ 
                // system running icons...
                devLCD_Display_SystemRunningIcons();
            }
            // load Segment register and send to I2C ...
            devLCD_UpdateDisplay();
        }    
        
        //========================================================================
        //  USB Event Tasks... 
        //========================================================================                
        if(TT4_USB_VBUS_IO == USB_PORT_PLUGGED){
          
            //reset button event interrupt flag...
            gubButtonEventFlag = 0;
            lubButtonStatus = BUTTON_STATUS_NOT_DEPRESSED;
            lbButtonOnePass = TRUE; 

            if(_geUSB_SetDescriptor == _eCDC_){    
                    
                //-------------------
                //  USB CDC Mode...
                //-------------------

                // reset CF ...                 
                _gsTT4Core.Task.Req_CfgDevice = REQ_DONE;
                _gsTT4Core.Task.Proc_CfgDevice = PROC_DONE;                    
                
                // activate timer interrupt....    
                appTT4MA_Timer1Interrupt_Disable();        

                // Enable Start/Stop Button and Vbus detection WakeUp Interrupt for fast response....
                appTT4MA_WakeUpInterrupts_Disable();
        
                // USB CDC mode...
                hwUSBTT4Tasks();        
        
                // Note: There are 2 conditions will happen after configuration mode...
                //       1 - if the configuration is successful then it will goes back to Device Sleep Mode status...
                //       2 - if the configuration is not successful then it will reset the device and goes back to Device Unconfigured status...
                // \ mjmariveles...        
                
                if(gbTT4USB_Config_OK == TRUE){
 
                    if( (_gsSensitech_Configuration_Area.ubCommandByte == CMD_BYTE_SLEEP) || (_gsSensitech_Configuration_Area.ubCommandByte == CMD_BYTE_RUNNING) ){
                
                        // exit STOP mode loop and go back to Device Sleep Mode status....
                        goto LABEL_DEVICE_SLEEP_MODE_STATUS;
                    }
                }
                
                // make it sure the residual voltage at the Vbus should be discharged....
                while(TT4_USB_VBUS_IO != USB_PORT_UNPLUGGED);
                
                // 0 - clear CF display...    
                devLCD_Display_CF(0);

                // activate timer interrupt....
                appTT4MA_Timer1Interrupt_Enable();        

                // Enable Start/Stop Button and Vbus detection WakeUp Interrupt for fast response....
                appTT4MA_WakeUpInterrupts_Enable();
                
                // update LCD display after unplugging USB fom CDC mode...
                devLCD_Display_ReadTemperature();

                // set Global USB Descriptor control..
                _geUSB_SetDescriptor = _eMSD_;                
                                  
                
            } /*  End of CDC Mode */            
            else{       

                //-------------------
                //  USB MSD Mode...
                //-------------------
                
                // get RTCC value for time stamping...              
                // at this point the monitor was stopped and plugged to the USB, this is the monitor read download time...
                //gudwDeviceStoppedDownloadTime = drvRTC_ReadRTCC_CurrentTime();
                gudwDeviceStoppedDownloadTime = _gsUser_Configuration_Area.udwUnit_Start_TimeStamp + ((_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints-1) * _gsUser_Configuration_Area.udwMeasurementInterval) + gudwMeasurementIntervalTimer;

                // set the USB mode status flag to MSD mode....
            
                // MSD mode conditions...
                // 0-Single Drop   
                // 1-Multi-Drop
                
                if(_gsGenCfgOptByte.GenOpt.Byte1.MultDropFlag==1){
                 
                    //-------------------
                    // Multi-Drop!...
                    //-------------------
                    
                    // if dispaly is in LCD summary current state then exit and restore 
                    // to the normal temperature display mode!... mjm
                    if(_gsTT4Core.Task.Req_LCDSummary == (REQ_ACTIVE | PROC_BUSY)){
                        // restore to Main LCD status display.... mjm                                
                        // clear all segments...    
                        devLCD_Display_AllSegments(OFF);                                        
                        // reset global LCD summary to start...
                        gubLCDSummaryStatus = 0;                  
                        // go to main display....
                        devLCD_Display_ReadTemperature();    
                            
                        // clear LCD summary task flag... 
                        _gsTT4Core.Task.Req_LCDSummary = REQ_DONE;
                        _gsTT4Core.Task.Proc_LCDSummary = PROC_DONE;
                    }   
                    
                    // set flag to update PDF file everytime...
                    gbPDFFileResetStatusFlag = TRUE;
                    // set flag to update TTV file everytime...
                    gbTTVFileResetStatusFlag = TRUE;
                
                    // disable timer interrupt prior entering USB mode...    
                    appTT4MA_Timer1Interrupt_Disable();        
                    // Enable Start/Stop Button and Vbus detection WakeUp Interrupt for fast response....
                    appTT4MA_WakeUpInterrupts_Disable();
                    
                    // log all current bit status to EEPROM...
                    commTT4Cfg_EERPOM_PostDataLogging();
                    
                    utilDelay_ms(50);
                    
                    // set Global USB Descriptor control..
                    _geUSB_SetDescriptor = _eMSD_;        
        
                    // USB MSD mode...
                    hwUSBTT4Tasks();        
                    
                    // make it sure the residual voltage at the Vbus should be discharged....
                    while(TT4_USB_VBUS_IO != USB_PORT_UNPLUGGED);
                    
                    // if Memory full occured in the USB multi-drop domain then terminate measurement immediately!...
                    if(gubStopDeviceStatus == _DEVICE_STOP_STATUS_SLEEP_) goto LABEL_DEVICE_STOP_MODE;
            
                    // activate timer interrupt....                    
                    appTT4MA_Timer1Interrupt_Enable();
                    // Enable Start/Stop Button and Vbus detection WakeUp Interrupt for fast response....
                    appTT4MA_WakeUpInterrupts_Enable();
                    utilDelay_ms(10);
             
                }
                else{
                
                    //-------------------
                    // Single-Drop!...
                    //-------------------
                    
                    gubStopDeviceStatus = _DEVICE_STOP_STATUS_GO_USB_;
            
                    goto LABEL_DEVICE_STOP_MODE;
                        
                } 
                           
            }  /*  End of MSD Mode */         
                 
        } /*  End of USB Tasks  */    
        
        MI_TOGGLE_PORT = 0; // debug port low...         
        
    }while(TT4USBMA_CORE_LOOP); /* - End of Core Loop  - */
   
   
    //===========================================================    
    //
    //  TT4USB in MSD and CDC Mode..
    //
    //===========================================================    
  
    //-----------------------------------------------
    // anytime if the device if plugged to USB will 
    // stop in the rest of its operation unless 
    // re-configuration is asserted then it will 
    // begin again to Temperature Acquisition Mode...
    //-----------------------------------------------
    
    //=================================== 
      LABEL_DEVICE_STOP_MODE:   
    //=================================== 
    
    if(gbResetRecoveryFlag == TRUE)
    {// if coming from reset recovery, use the stored stop time stamp
      gudwDeviceStoppedDownloadTime =  _gsUser_Configuration_Area.udwUnit_Stop_TimeStamp;
      gbResetRecoveryFlag = FALSE;    //reset flag
        // get RTCC value for time stamping...
        // at this point the monitor was stopped and plugged to the USB, this is the monitor read download time...
    }
    else
        gudwDeviceStoppedDownloadTime = _gsUser_Configuration_Area.udwUnit_Start_TimeStamp + ((_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints-1) * _gsUser_Configuration_Area.udwMeasurementInterval) + gudwMeasurementIntervalTimer;

   
      
    // set flag to update PDF file everytime...
    gbPDFFileResetStatusFlag = TRUE;
    // set flag to update TTV file everytime...
    gbTTVFileResetStatusFlag = TRUE;
    
    // disable timer interrupt prior entering USB mode...    
    appTT4MA_Timer1Interrupt_Disable();        
    // disable Start/Stop Button and Vbus detection WakeUp Interrupt for fast response....
    appTT4MA_WakeUpInterrupts_Disable();
    utilDelay_ms(10);
                       
    // if plugged to USB then stop...
    _gsSensitech_Configuration_Area.ubCommandByte = CMD_BYTE_STOPPED;    
    gubMem = _gsSensitech_Configuration_Area.ubCommandByte;
    commTT4Cfg_FastWriteCfgEeprom(OFFSET_COMMAND_BYTE,(const void *)&gubMem);    
                       
    // log all memory variable status to EEPROM...    
    commTT4Cfg_EERPOM_PostDataLogging();
    // Stop Mode is for Single Drop Mode only.... mjm
    coreDeviceStopMode();
    
    // check the device mode from exiting StartUp status if the user just 
    // configured the device then to back to TOP at device sleep mode state... mjm
    if(gbTT4USB_Config_OK == TRUE){ 
        
        goto LABEL_DEVICE_SLEEP_MODE_STATUS;
    }
    else{  
        // if the configuration settings not successful then reset and go back to Unconfigured State!...mjm                                    
        RCON = 0x00;
        __asm__("reset");                
    }      
       

    // Note here!
    // when exits from USB mode its program counter jumps to address 0x00000000
    // to initiate system reset with full initialization... mjm    
    return(0);    
}    




/* ------------ End Of Main Program  ------------ */

