/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm                                            **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc. and Relisource Technology Ltd.                            **
 **                                                                           **
 **  Copryright(c) 2015. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Relisource Technology Ltd.                        **
 **  Creation date       :  02/18/2015                                        **
 **  Programmer          :  Faijur Rahman		                              **
 **                                                                           **
 **  -------------                                                            **
 **  CPU Details:                                                             **
 **  -------------                                                            **
 **  MCU Manufacturer    :  Atmel	                                          **
 **  MCU P/N             :  ATSAMG55J19										  **
 **  Architecture        :  ARM Cortex-M4									  **
 **  Program Memory Type :  Flash                                             **
 **  Program Memory Size :  512KB                                             **
 **  RAM size            :  160KB											  **
 **  Pin count           :  49                                                **
 **  Language            :  C                                                 **
 **  Dev't Environtment  :  Atmel Studio 6                                    **
 **  Compiler            :  GCC				                                  **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : main.c                                                            **
 **                                                                           **
 **  Description :                                                            **
 **    - main program to launch and execute applications routines.            **
 **                                                                           **
 **  History :                                                                ** 
 **    ver 1.00.001  -  02/18/2015                                            **
 **                                                                           **
 ******************************************************************************/

#ifndef Zeeshan
	#define Zeeshan
#endif 

#include "asf.h"
#include "hal.h"
#include "com.h"
#include "asf.h"
#include "report.h"
#include "services.h"
#include "tt4.h"
#include "usb.h"
#include "drvI2C_Wrapper.h" //only for testing the EEPROM

#ifdef Zeeshan
#include "conf_usb.h"
#include "usb_ui.h"
#include "hal/drivers/usb/cdc/uart_cdc.h"

static volatile bool main_b_msc_enable = false;
static volatile bool main_b_cdc_enable = false;
#endif

//****************************************************************
// Flash Chip related
volatile UINT8 gubFlashBlockSizeBuffer[FLASH_ERASE_BLOCK_SIZE + 1] __attribute__((far));
volatile UINT8 gubFlashTemporaryBuffer[FLASH_ERASE_BLOCK_SIZE + 1] __attribute__((far));

int check_mismatch()
{
	uint8_t mismatch_count = 0;
	for(int i = 0; i < FLASH_ERASE_BLOCK_SIZE; i++)
	{
		if (gubFlashBlockSizeBuffer[i] != gubFlashTemporaryBuffer[i])
		{
			mismatch_count++;
		}
	}
	
	return mismatch_count;
}

//****************************************************************
__SENSITECH_CONFIGURATION_AREA  _gsSensitech_Configuration_Area; 
 __USER_CONFIGURATION_AREA    _gsUser_Configuration_Area;
__ALARM_CONFIGURATION_AREA   _gsAlarmConfig[6];
// EEPROM control bit manipulator structure register...
__sBITS_GENCFGOPT_BYTE       _gsGenCfgOptByte;
__sBITS_EXTOPT_BYTE          _gsExtdOptByte;

// extreme temperature points global storage....
_sLOGMINMAXTEMPDATA _gsLogMinMaxTempData;



// EEPROM control bit manipulator structure register...
__sBITS_GENCFGOPT_BYTE _gsGenCfgOptByte;

// Sensor 1 Temperature Calibration Offset...
__sSENSOR1_TEMP_CAL_OFFSET  _gsSensor1TempCalOffset;

bool gbUpdateLCDFlag;

//****************************************************************
volatile uint16_t millisecond_count;
void SysTick_Handler(void)
{
	millisecond_count++;
}

enum MONITOR_STATES
{
	UNCONFIGURED,
	START_UP_DLY,
	RUNNING,
	STOPPED, 
	SLEEP
};

enum MONITOR_STATES STATE;
volatile int monitorState;
volatile bool buttonEvent;

//This function will be called from interrupt of Start Button
static void Start_Btn_Callback(void)
{
	if(monitorState == UNCONFIGURED)
	{	
		monitorState = RUNNING;
		devRedLED_off();
		devGreenLED_on();	
		devUnRegister_Button_Callback(START_BUTTON_ID);
		buttonEvent = true;		
	}
}

//This function will be called from interrupt of Stop Button
volatile static void Stop_Btn_Callback(void)
{
	if(monitorState == RUNNING)
	{
		monitorState = STOPPED;
		devGreenLED_off();
		devRedLED_on();
		devUnRegister_Button_Callback(STOP_BUTTON_ID);
		buttonEvent = true;		
	}
}


#define  ARRAY_SIZE 16
uint8_t missmatch_count;
int missmatch_pos[ARRAY_SIZE];
uint8_t missmatch_read[ARRAY_SIZE];

void check_block_erase()
{
	missmatch_count = 0;
	for(int i = 0; i < FLASH_ERASE_BLOCK_SIZE; i++)
	{
		if (0xff != gubFlashBlockSizeBuffer[i])
		{
			missmatch_count++;
		}
	}
}

void copy_buffer_to_temp()
{
	int i;
	for (i = 0; i < FLASH_ERASE_BLOCK_SIZE; i++)
	{
		gubFlashTemporaryBuffer[i] = gubFlashBlockSizeBuffer[i];
	}
}

void block_write_read_missmatch()
{
	missmatch_count = 0;
	int i;
	for(i = 0; i < FLASH_ERASE_BLOCK_SIZE; i++)
	{
		if (gubFlashBlockSizeBuffer[i] != gubFlashTemporaryBuffer[i])
		{
			if (ARRAY_SIZE == missmatch_count)
			{
				break;
			}
			missmatch_pos[missmatch_count] = i;
			missmatch_read[missmatch_count] = gubFlashTemporaryBuffer[i];
			missmatch_count++;
		}
	}
}

int main2(void)
{
	irq_initialize_vectors();
	cpu_irq_enable();

	// Initialize the sleep manager
	sleepmgr_init();
	#if !SAMD21 && !SAMR21 && !SAML21
	sysclk_init();
	board_init();
	#else
	system_init();
	#endif
	ui_init();
	ui_powerdown();

	memories_initialization();

	// Start USB stack to authorize VBus monitoring
	udc_start();

	// The main loop manages only the power mode
	// because the USB management is done by interrupt
	while (true) {

		if (main_b_msc_enable) {
			if (!udi_msc_process_trans()) {
				sleepmgr_enter_sleep();
			}
			}else{
			sleepmgr_enter_sleep();
		}
	}
}

int main (void)
{
#ifdef Zeeshan
	irq_initialize_vectors();
	cpu_irq_enable();
#endif
	
	
	sysclk_init();
	TT4USB_Board_init();
	configure_console();
	sleepmgr_init();
	
#ifdef Zeeshan
	board_init();
	ui_init();
	ui_powerdown();

	memories_initialization();

	// Start USB stack to authorize VBus monitoring
	udc_start();
#endif	
	
	
	millisecond_count=0;
	SysTick_Config(sysclk_get_cpu_hz()/100); //10 millisecond tick
	
	devLCD_Init();		// Initialize LCD
	devEEPROM_Init();   // Initialize EEPROM
	devFLASH_Init();	// Initialize Flash Chip
	devSensor_Init();	// Initialize Temperature Sensor
	devButton_Init();	// Initialize Buttons
	
	
	puts("****************************************************************\n");
	puts("EEPROM testing\n");	
	uint8_t addr=0, len=20, write_data[101], read_data[101], mismatch=0;
	for(int i=0; i<len; i++)
	{
		write_data[i] = i+10;
		read_data[i] = 0;
	}
	
	eeprom_i2c_write(write_data, len, addr);
	delay_ms(5);//TODO: Check why delay is necessary
	eeprom_i2c_read(read_data, len, addr);
	delay_ms(5);
	
	for(int i=0; i<len; i++)
	{
		if(read_data[i] != write_data[i])
		{
			mismatch++;
		}	
	}
	
	if(mismatch == 0 )
	{
		puts("EEPROM Multi byte Read-Write Ok!");
	}
	else
	{
		puts("Error in Multi byte Read-Write of EEPROM");
	}
	
	mismatch=0;
	for(addr=0; addr<len; addr++)
	{
		devEEPROM_ByteWrite(addr, write_data[0]);
		delay_ms(5);
		devEEPROM_RandomRead(addr, &read_data[0]);	
		delay_ms(5);
		if(read_data[0] != write_data[0])	
		{
			mismatch++;
		}
		
		write_data[0]++;
	}
	
	if(mismatch == 0 )
	{
		puts("EEPROM Single byte Read-Write Ok!");
	}
	else
	{
		puts("Error in Single byte Read-Write of EEPROM");
	}
	puts("****************************************************************\n");
	
	
	puts("****************************************************************\n");
	puts("Flash chip Erase, Write and Read testing\n");
	int block_no = 10;
	devFLASH_Buffer_EraseBlock(block_no);
	devFLASH_Buffer_ReadBlock(block_no);
	memset((void *)&gubFlashTemporaryBuffer[0],0,4096u);
	if(check_mismatch() == 0 )
	{
		puts("Flash Chip Erase Ok!");
	}
	else
	{
		puts("Error in Erase function of Flash Chip");
	}
	
	devFLASH_BootSectorFill_Block0();
	devFLASH_Buffer_WriteBlock(block_no);
	for(int i=0; i<FLASH_ERASE_BLOCK_SIZE; i++)
	{
		gubFlashTemporaryBuffer[i] = gubFlashBlockSizeBuffer[i]; //Copy the write data to temporary buffer
		gubFlashBlockSizeBuffer[i] = 0;//Now clear the write buffer
	}	
	devFLASH_Buffer_ReadBlock(block_no);
	if(check_mismatch() == 0 )
	{
		puts("Flash Chip Read-Write Ok!");	
	}
	else
	{
		puts("Error in Read-Write function of Flash Chip");	
	}
	puts("****************************************************************\n");
	
			
	//Register button callback functions
	devRegister_Button_Callback(START_BUTTON_ID, Start_Btn_Callback);
	devRegister_Button_Callback(STOP_BUTTON_ID, Stop_Btn_Callback); 
	
	_gsGenCfgOptByte.GenOpt.Byte1.DispCurntTempEna=1; //Enable showing temperature data
	_gsGenCfgOptByte.GenOpt.Byte0.BlinkModeEna=1; //Enable SUN icon Toggling
	monitorState = UNCONFIGURED;
	buttonEvent = false;
	
	
	while (1)//Infinite loop
	{	
		if(buttonEvent==true || millisecond_count>=50)//Button Event or 0.5 seconds elapsed
		{
			//Clear counter
			millisecond_count=0;
			buttonEvent=false;
			
			switch(monitorState)
			{
				case RUNNING:
				{
					//Toggle Green LED
					devGreenLED_toggle();
					
					//Measure Temperature
					devSensor_ReadTemp_OneShotMode();
					devSensor_AquireFinalTemperatureValues();
					devLCD_Display_ReadTemperature();
					
					//Show Sun Icon
					devLCD_Display_RunningIcon();
					
					//Send Running status to console
					puts("Monitor is running");
					
					if (!udi_cdc_is_tx_ready()) {
						// Fifo full
						udi_cdc_signal_overrun();
						ui_com_overflow();
						} else {
						udi_cdc_write_buf("Hello CDC ",10);
					}
					
					break;
				}
				case STOPPED:
				{
					//Toggle RED LED
					devRedLED_toggle();
					
					//Show Stop Icon
					devLCD_Display_StopAlarmDevice(true);
					
					//Send Running status to console
					puts("Monitor is stopped");
					
					break;
				}
				case UNCONFIGURED:
				{
					//Toggle Green LED
					devGreenLED_toggle();
					
					//Toggle RED LED
					devRedLED_toggle();
					
					//Start LCD Blinking
					devLCD_AllSegBlinkOn();
					
					//Send Running status to console
					//puts("Monitor is unconfigured");
					
					break;
				}
				default:
				{
					//Nothing to do
				}
				
			}//End switch()
			
			
			//Update display data
			if(gbUpdateLCDFlag == true)
			{
				devLCD_UpdateDisplay();	
			}
			
		}//End if()
		
		if (main_b_msc_enable) {
			if (!udi_msc_process_trans()) {
				sleepmgr_enter_sleep();
			}
			}else{
			sleepmgr_enter_sleep();
		}
		//sleepmgr_enter_sleep();
		
	}//End While()
	
}//End main()






#ifdef Zeeshan

void main_suspend_action(void)
{
	ui_powerdown();
}

void main_resume_action(void)
{
	ui_wakeup();
}

void main_sof_action(void)
{
	if ((!main_b_msc_enable) ||
		(!main_b_cdc_enable))
		return;
	ui_process(udd_get_frame_number());
}

/*! \brief Example of extra USB string management
 * This feature is available for single or composite device
 * which want implement additional USB string than
 * Manufacture, Product and serial number ID.
 *
 * return true, if the string ID requested is know and managed by this functions
 */
bool main_extra_string(void)
{
	static uint8_t udi_cdc_name[] = "CDC interface";
	static uint8_t udi_msc_name[] = "MSC interface";

	struct extra_strings_desc_t{
		usb_str_desc_t header;
		le16_t string[Max(sizeof(udi_cdc_name)-1, sizeof(udi_msc_name)-1)];
	};
	static UDC_DESC_STORAGE struct extra_strings_desc_t extra_strings_desc = {
		.header.bDescriptorType = USB_DT_STRING
	};

	uint8_t i;
	uint8_t *str;
	uint8_t str_lgt=0;

	// Link payload pointer to the string corresponding at request
	switch (udd_g_ctrlreq.req.wValue & 0xff) {
	case UDI_CDC_IAD_STRING_ID:
		str_lgt = sizeof(udi_cdc_name)-1;
		str = udi_cdc_name;
		break;
	case UDI_MSC_STRING_ID:
		str_lgt = sizeof(udi_msc_name)-1;
		str = udi_msc_name;
		break;
	default:
		return false;
	}

	if (str_lgt!=0) {
		for( i=0; i<str_lgt; i++) {
			extra_strings_desc.string[i] = cpu_to_le16((le16_t)str[i]);
		}
		extra_strings_desc.header.bLength = 2+ (str_lgt)*2;
		udd_g_ctrlreq.payload_size = extra_strings_desc.header.bLength;
		udd_g_ctrlreq.payload = (uint8_t *) &extra_strings_desc;
	}

	// if the string is larger than request length, then cut it
	if (udd_g_ctrlreq.payload_size > udd_g_ctrlreq.req.wLength) {
		udd_g_ctrlreq.payload_size = udd_g_ctrlreq.req.wLength;
	}
	return true;
}

bool main_msc_enable(void)
{
	main_b_msc_enable = true;
	return true;
}

void main_msc_disable(void)
{
	main_b_msc_enable = false;
}

bool main_cdc_enable(uint8_t port)
{
	main_b_cdc_enable = true;
	// Open communication
	//uart_open(port);
	return true;
}

void main_cdc_disable(uint8_t port)
{
	main_b_cdc_enable = false;
	// Close communication
	//uart_close(port);
}

void main_cdc_set_dtr(uint8_t port, bool b_enable)
{
	if (b_enable) {
		// Host terminal has open COM
		ui_com_open(port);
	}else{
		// Host terminal has close COM
		ui_com_close(port);
	}
}


#endif