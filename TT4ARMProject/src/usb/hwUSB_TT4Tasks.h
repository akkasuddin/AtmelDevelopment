/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm                                            **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc. and Blue Ocean Innovation Ltd.                            **
 **                                                                           **
 **  Copryright(c) 2010. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Blue Ocean Innovation Ltd.                        **
 **  Project#            :  449                                               **
 **  Creation date       :  08/20/2010                                        **
 **  Programmer          :  Michael J. Mariveles                              **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : hwUSB_TT4Task.h                                                   **
 **                                                                           **
 **  Description :                                                            **
 **    - USB core to handle the PC and device high level interface.           **
 **    - MSD and CDC routines will be launched here for USB transactions.     **
 **    - PDF and TTV file writter will be launched here.                      **
 **    - Multi-drop temperature recording secondary core loop.                **
 **                                                                           **
 ******************************************************************************/
 
#ifndef __HWUSB_TT4TASKS_H
#define __HWUSB_TT4TASKS_H

void USBDeviceTasks(void);
void ProcessIO(void);
WORD_VAL ReadPOT(void);
void YourHighPriorityISRCode(void);
void YourLowPriorityISRCode(void);

// TT4 custom modules...
void hwUSBTT4Tasks(void);


#endif /* __HWUSB_TT4TASKS_H */

/* ------------ End Of File ------------ */

