/*
 * usb_ui.c
 *
 * Created: 4/2/2015 12:50:04 PM
 *  Author: Zeeshan
 */ 

#include <asf.h>
#include "usb_ui.h"

void ui_init(void)
{
	/* Initialize LED */
	//LED_Off(LED0);
}

void ui_powerdown(void)
{
//	LED_Off(LED0);
}

void ui_wakeup(void)
{
//	LED_On(LED0);
}

void ui_com_open(uint8_t port)
{
	UNUSED(port);
}

void ui_com_close(uint8_t port)
{
	UNUSED(port);
}

void ui_com_rx_start(void)
{
}

void ui_com_rx_stop(void)
{
}

void ui_com_tx_start(void)
{
}

void ui_com_tx_stop(void)
{
}

void ui_com_error(void)
{
}

void ui_com_overflow(void)
{
}

void ui_start_read(void)
{
}

void ui_stop_read(void)
{
}

void ui_start_write(void)
{
}

void ui_stop_write(void)
{
}

void ui_process(uint16_t framenumber)
{
	if ((framenumber % 1000) == 0) {
	//	LED_On(LED0);
	}
	if ((framenumber % 1000) == 500) {
	//	LED_Off(LED0);
	}
}


/**
 * \defgroup UI User Interface
 *
 * Human interface on SAMG55-XPRO:
 * - LED0 blinks when USB host has checked and enabled CDC and MSC interface
 *
 */
