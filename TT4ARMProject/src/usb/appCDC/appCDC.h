/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm                                            **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc. and Blue Ocean Innovation Ltd.                            **
 **                                                                           **
 **  Copryright(c) 2010. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Blue Ocean Innovation Ltd.                        **
 **  Project#            :  449                                               **
 **  Creation date       :  08/20/2010                                        **
 **  Programmer          :  Michael J. Mariveles                              **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : appCDC.h                                                          **
 **                                                                           **
 **  Description :                                                            ** 
 **    - application routines for TT4USB configuration.                       **
 **    - USB CDC communication that read/writes EEPROM data in the device.    **
 **                                                                           **
 ******************************************************************************/
 
#ifndef __APPCDC_H
#define __APPCDC_H


void hwUSB_DumpEepromContentsToFile(char par_strFilename[20]);
void hwUSB_TT4MA_Communications(void);
UINT8 hwUSB_TT4MA_Comm_Login(void);
void hwUSB_TT4MA_Comm_Req(void);
UINT8 hwUSB_TT4MA_CommandTask(void);
UINT8 hwUSB_TT4MA_CheckValidCommandValue(UINT8 par_lubReceivedCommandValue);
UINT8 hwUSB_TT4MA_ProcessCommandReq(UINT8 par_ubCommandValue, UINT8 par_ubParameterValue, UINT8 par_ubNumBytesRead);
void hwUSB_TT4MA_SendACKToHost(UINT8 par_ubCommandStatusCode);
UINT8 hwUSB_TT4MA_CalculateChksum(UINT8 *par_ubBytes, UINT16 par_uwTotalBytes);
void hwUSB_TT4MA_SendACKToHostNextFrame(UINT16 par_uwFrameNumber);

//------------------------
// TT4 ORIGINAL ROUTINES
//------------------------
UINT8 hwUSB_TT4MA_ProcReq_0x20(UINT8 par_ubParameterValue);
UINT8 hwUSB_TT4MA_ProcReq_0x2A(UINT8 par_ubParameterValue);
UINT8 hwUSB_TT4MA_ProcReq_0x2B(UINT8 par_ubParameterValue, UINT8 par_ubNumBytesRead);
UINT8 hwUSB_TT4MA_ProcReq_0x2C(UINT8 par_ubParameterValue);
UINT8 hwUSB_TT4MA_ProcReq_0x2D(UINT8 par_ubParameterValue);
void hwUSB_TT4MA_ProcReq_0x33(UINT8 par_ubParameterValue);
void hwUSB_TT4MA_ProcReq_0x37(UINT8 par_ubParameterValue);
void hwUSB_TT4MA_ProcReq_0x38(UINT8 par_ubParameterValue);
void hwUSB_TT4MA_ProcReq_0x44(UINT8 par_ubParameterValue);
UINT8 hwUSB_TT4MA_ProcReq_0x45(UINT8 par_ubParameterValue);
void hwUSB_TT4MA_ProcReq_0x46(UINT8 par_ubParameterValue);
void hwUSB_TT4MA_ProcReq_0x47(UINT8 par_ubParameterValue);
void hwUSB_TT4MA_ProcReq_0x48(UINT8 par_ubParameterValue);
void hwUSB_TT4MA_ProcReq_0x50(UINT8 par_ubParameterValue);
void hwUSB_TT4MA_ProcReq_0x51(UINT8 par_ubParameterValue);
void hwUSB_TT4MA_ProcReq_0x53(UINT8 par_ubParameterValue);
void hwUSB_TT4MA_ProcReq_0x60(UINT8 par_ubParameterValue);
void hwUSB_TT4MA_ProcReq_0x61(UINT8 par_ubParameterValue);
void hwUSB_TT4MA_ProcReq_0x62(UINT8 par_ubParameterValue);
void hwUSB_TT4MA_ProcReq_0x63(UINT8 par_ubParameterValue);
void hwUSB_TT4MA_ProcReq_0x64(UINT8 par_ubParameterValue);
UINT8 hwUSB_TT4MA_ProcReq_0x65(UINT8 par_ubParameterValue);
void hwUSB_TT4MA_ProcReq_0x66(UINT8 par_ubParameterValue);
void hwUSB_TT4MA_ProcReq_0x67(UINT8 par_ubParameterValue);
void hwUSB_TT4MA_ProcReq_0x68(UINT8 par_ubParameterValue);
void hwUSB_TT4MA_ProcReq_0x69(UINT8 par_ubParameterValue);
void hwUSB_TT4MA_ProcReq_0x6A(UINT8 par_ubParameterValue);
UINT8 hwUSB_TT4MA_ProcReq_0x6E(UINT8 par_ubParameterValue);

//---------------------
// TT4USB-MA ROUTINES
//---------------------
void hwUSB_TT4MA_ProcReq_0x54(UINT8 par_ubParameterValue);
void hwUSB_TT4MA_ProcReq_0x55(UINT8 par_ubParameterValue);
void hwUSB_TT4MA_ProcReq_0x56(UINT8 par_ubParameterValue);
void hwUSB_TT4MA_ProcReq_0x57(UINT8 par_ubParameterValue);
void hwUSB_TT4MA_ProcReq_0x58(UINT8 par_ubParameterValue);
void hwUSB_TT4MA_ProcReq_0x59(UINT8 par_ubParameterValue);
void hwUSB_TT4MA_ProcReq_0x72(UINT8 par_ubParameterValue);
void hwUSB_TT4MA_ProcReq_0x73(UINT8 par_ubParameterValue);
UINT8 hwUSB_TT4MA_ProcReq_0x74(UINT8 par_ubParameterValue);
UINT8 hwUSB_TT4MA_ProcReq_0x75(UINT8 par_ubParameterValue);
UINT8 hwUSB_TT4MA_ProcReq_0x76(UINT8 par_ubParameterValue);


#endif /* __APPCDC_H */

/*  End of File  */

