/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm                                            **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc. and Blue Ocean Innovation Ltd.                            **
 **                                                                           **
 **  Copryright(c) 2010. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Blue Ocean Innovation Ltd.                        **
 **  Project#            :  449                                               **
 **  Creation date       :  08/20/2010                                        **
 **  Programmer          :  Michael J. Mariveles                              **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : appCDC.c                                                          **
 **                                                                           **
 **  Description :                                                            **
 **    - application routines for TT4USB configuration.                       **
 **    - USB CDC communication that read/writes EEPROM data in the device.    **
 **                                                                           **
 ******************************************************************************/

#include "hwPIC24F.h"
#include "hwSystem.h"
#include "usb_function_cdc.h"
#include "devSPIFLASH_W25Q80.h"
#include "devI2CEEPROM_24AA256.h"
#include "commTT4Config.h"
#include "devI2CTempSensor_TMP112.h"
#include "utility.h"
#include "appCDC.h"
#include "drvRTC.h"

// temperary storage for EDS memory transfer buffer...
extern UINT8   gubMem;
extern UINT16  guwMem;
extern UINT32  gudwMem;

// EEPROM configuration storage variables....
UINT8 *gubTT4Cfg_DataBuffer;
UINT8 *gubTT4Cfg_EEBuffer;
UINT8 *gubTT4Cfg_ScratchPadMemory;

// USB data packet buffer variable...
char *USB_Out_Buffer;
char *USB_In_Buffer;

// RAM Memory Current Time global storage...
extern volatile UINT32 gudwTT4MA_CurrentTime;
extern UINT32 gudwTT4MA_PCTime;

// EEPROM configuration control structure register...
extern __SENSITECH_CONFIGURATION_AREA  _gsSensitech_Configuration_Area;  
extern __USER_CONFIGURATION_AREA       _gsUser_Configuration_Area;

// tt4 communication login status global var
UINT8 gubHdSkOkLock;

// configuration was monitoring var...
extern BOOL gbTT4USB_Config_OK;

// RTCC routine....
extern void appTT4MA_RTC_Set_CurrentTimeSecs(UINT32 par_udwTimeInSecs);

/*******************************************************************************
**  Function     : void hwUSB_TT4MA_Communications(void)
**  Description  : CDC serial communication manager 
**  PreCondition : none 
**  Side Effects : updates global var named gubHdSkOkLock 
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void hwUSB_TT4MA_Communications(void){
    
    register UINT8 lubCommandStatus; 
    
    if(gubHdSkOkLock == COMM_HDSK_TASK_NOT_DONE){
        // Get first the Host loggin status...
        if(hwUSB_TT4MA_Comm_Login()==HOST_LOGIN_SUCCESSFUL) gubHdSkOkLock = COMM_HDSK_TASK_DONE;
    }
    else{
        // Host successfuly establish a connection and logged in...
        // awaiting PC host command to execute...        
        lubCommandStatus = hwUSB_TT4MA_CommandTask();        
    }
    
}


/*******************************************************************************
**  Function     : UINT8 hwUSB_TT4MA_CommandTask(void)
**  Description  : manages CDC commands
**  PreCondition : should be establish login succesful before using this function.
**  Side Effects : none.
**  Input        : none
**  Output       : 
**                 0 - if task is incomplete...
**                 1 - if task is done 
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
UINT8 hwUSB_TT4MA_CommandTask(void){
    
    register UINT8   lubNumBytesRead;
    register UINT8   lubTotalBytesToSend;
    register UINT8   lubRecievedCommand = 0;
    
    // declare and init command variables...
    static UINT8 lubCommandStatus = TT4CMD_STATUS_LEVEL1_OPEN;
    static UINT8 lubCommandValue = 0;
    // declare and init parameter variables...
    static UINT8 lubParameterStatus = TT4CMD_STATUS_LEVEL2_CLOSE;
    static UINT8 lubParameterValue = 0;
    
    // declare and init Execute Command variables...
    static UINT8 lubExecCommandStatus = TT4CMD_STATUS_LEVEL3_CLOSE;    
    static UINT8 lubProcCommandStatus = 0;
    
    // specifies the direction of data..
    // 0 - received data from host (default)
    // 1 - send data to host
    static UINT8 lubSendSeriesOfDataToHost = 0;
    
    
    if(lubSendSeriesOfDataToHost==0){
        
        //----------------------------------
        // fetch host command request...
        //----------------------------------
        lubNumBytesRead = getsUSBUSART(USB_Out_Buffer,64);        
    
        // the buffer must not empty...at least greater the 1!
        if(lubNumBytesRead==0) return(0);
    }
    
    
    //-------------------------------------------------------------------------------------
    // once valid this level will be skipped until the communication successfully ends...
    //-------------------------------------------------------------------------------------
    if(lubCommandStatus == TT4CMD_STATUS_LEVEL1_OPEN){
        
        lubRecievedCommand = (UINT8)USB_Out_Buffer[0];
        
        // validate TT4 commands
        if(hwUSB_TT4MA_CheckValidCommandValue(lubRecievedCommand) == TT4_CMD_UNKNOWN){ 
            // send back echo unknown command....
            hwUSB_TT4MA_SendACKToHost(TT4_CMD_UNKNOWN); 
            return(0);
        }    
        
        // update value to valid command status...
        lubCommandStatus = TT4CMD_STATUS_LEVEL1_CLOSE;
        // since the command is successful then open the 2nd level access task...
        lubParameterStatus = TT4CMD_STATUS_LEVEL2_OPEN;
        
        // save command value...
        lubCommandValue = lubRecievedCommand;
        
         // send back echo....
         USB_In_Buffer[0] = (char)lubCommandValue;  
         lubTotalBytesToSend = 1;
         putUSBUSART(USB_In_Buffer,lubTotalBytesToSend);         
         
         return(0);
    }
    

    //-------------------------------------------------------------------------------------
    // get parameter value after sending back echo to host...
    //-------------------------------------------------------------------------------------
    if(lubParameterStatus == TT4CMD_STATUS_LEVEL2_OPEN){
        
        lubRecievedCommand = (UINT8)USB_Out_Buffer[0];
       
        // update value to valid parameter status...
        lubParameterStatus = TT4CMD_STATUS_LEVEL2_CLOSE;
        // since the command is successful then open the 3rd Level access task...
        lubExecCommandStatus = TT4CMD_STATUS_LEVEL3_OPEN;
        
        // update parameter value..
        lubParameterValue = lubRecievedCommand;        
        
    }
    
    
    //-------------------------------------------------------------------------------------
    // Execute command requested by host...
    //-------------------------------------------------------------------------------------
    if(lubExecCommandStatus == TT4CMD_STATUS_LEVEL3_OPEN){
        
        lubRecievedCommand = (UINT8)USB_Out_Buffer[0];
        
        lubProcCommandStatus = hwUSB_TT4MA_ProcessCommandReq(lubCommandValue,lubParameterValue,lubNumBytesRead);
        
        switch(lubProcCommandStatus){
            case(TT4C_PROC_CMD_STAT_CLOSE):
                // reset all TT4 communication status...
                lubCommandStatus = TT4CMD_STATUS_LEVEL1_OPEN;    
                lubParameterStatus = TT4CMD_STATUS_LEVEL2_CLOSE;   
                lubExecCommandStatus = TT4CMD_STATUS_LEVEL3_CLOSE;
                // tt4 not yet logged in...
                gubHdSkOkLock = COMM_HDSK_TASK_NOT_DONE;
                
                lubSendSeriesOfDataToHost = 0; // set to input mode for sure!....  
                break;
            
            case(TT4C_PROC_CMD_STAT_OPEN):
            
                break;

            case(TT4C_PROC_CMD_STAT_OUT):  // series of 64 bytes output as frames....
                                           // because CDC has only 64 bytes maximum payload.... noted by! mjm
                                           
                //-------------------------------------------------    
                // since the command was successfully done then
                // reset again to command tasks status to grant 
                // any new request entries... 
                //-------------------------------------------------
                lubCommandStatus = TT4CMD_STATUS_LEVEL1_CLOSE;    
                lubParameterStatus = TT4CMD_STATUS_LEVEL2_CLOSE;   
                lubExecCommandStatus = TT4CMD_STATUS_LEVEL3_OPEN; // still acquires data from the host...

                // this will handle only if the data is morethan 64 bytes that will be sent out to host..
                lubSendSeriesOfDataToHost = 1; // set to output mode....  
                break;

            case(TT4C_PROC_CMD_STAT_IN): // series of 64 bytes input as frames 
                                         // because CDC has only 64 bytes maximum payload.... noted by! mjm

                //-------------------------------------------------    
                // since the command was successfully done then
                // reset again to command tasks status to grant 
                // any new request entries... 
                //-------------------------------------------------
                lubCommandStatus = TT4CMD_STATUS_LEVEL1_CLOSE;    
                lubParameterStatus = TT4CMD_STATUS_LEVEL2_CLOSE;   
                lubExecCommandStatus = TT4CMD_STATUS_LEVEL3_OPEN; // still acquires data from the host...

                // this will handle only if the data is morethan 64 bytes that will be sent out to host..
                lubSendSeriesOfDataToHost = 0; // set to input mode for sure!....  
                
                break;


            case(TT4C_PROC_CMD_STAT_NOT_DONE):

                //-------------------------------------------------    
                // since the command was successfully done then
                // reset again to command tasks status to grant 
                // any new request entries... 
                //-------------------------------------------------
                
                break;

            case(TT4C_PROC_CMD_STAT_DONE):
                    
                //-------------------------------------------------    
                // since the command was successfully done then
                // reset again to command tasks status to grant 
                // any new command request entry... 
                //
                // NOTE: command status is still in command stage 
                //       to execute another new TT4 comamnds...
                //-------------------------------------------------
                lubCommandStatus = TT4CMD_STATUS_LEVEL1_OPEN;    
                lubParameterStatus = TT4CMD_STATUS_LEVEL2_CLOSE;   
                lubExecCommandStatus = TT4CMD_STATUS_LEVEL3_CLOSE;
                
                lubSendSeriesOfDataToHost = 0;// set default input mode..   
                
                break;
            
        }
        
    }
    
    return(1);
}



/*******************************************************************************
**  Function     : UINT8 hwUSB_TT4MA_CheckValidCommandValue(UINT8 par_lubReceivedCommandValue)
**  Description  : command search and validate's if command is successful.
**  PreCondition : login must be succesful before calling this sub function.
**  Side Effects : none
**  Input        : par_lubReceivedCommandValue - TT4 command value
**  Output       : return's successful or unknown command
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
UINT8 hwUSB_TT4MA_CheckValidCommandValue(UINT8 par_lubReceivedCommandValue){
    
    //----------------------------------------
    // return's 1 if TT4 command is valid...
    // return's 0 if TT4 command is invalid...
    //----------------------------------------

    // check in the command valid list...
    switch(par_lubReceivedCommandValue){                
        // TT4 Command Set... (Original)
        case(TT4CMD_PACKETZED_DWNLD) :              return(TT4_CMD_SUCCESSFUL);
        case(TT4CMD_PRI_SEC_DATA_ONLY_DWNLD)  :     return(TT4_CMD_SUCCESSFUL);
        case(TT4CMD_WRITE_CERT_DATA) :              return(TT4_CMD_SUCCESSFUL);
        case(TT4CMD_READ_CERT_DATA) :               return(TT4_CMD_SUCCESSFUL);
        case(TT4CMD_PACKETZED_DWNLD_MA) :           return(TT4_CMD_SUCCESSFUL);
        case(TT4CMD_GET_ID) :                       return(TT4_CMD_SUCCESSFUL);
        case(TT4CMD_SKIP_ID) :                      return(TT4_CMD_SUCCESSFUL);
        case(TT4CMD_TERMINATE_COMM) :               return(TT4_CMD_SUCCESSFUL);
        case(TT4CMD_MOVE_UNIT_STAT_SP) :            return(TT4_CMD_SUCCESSFUL);
        case(TT4CMD_WRITE_SP) :                     return(TT4_CMD_SUCCESSFUL);
        case(TT4CMD_READ_SP) :                      return(TT4_CMD_SUCCESSFUL);
        case(TT4CMD_LOGIN_SENSITECH) :              return(TT4_CMD_SUCCESSFUL);
        case(TT4CMD_LOGIN_USER) :                   return(TT4_CMD_SUCCESSFUL);
        case(TT4CMD_C0PY_SP_TO_ST_CFG) :            return(TT4_CMD_SUCCESSFUL);
        case(TT4CMD_C0PY_SP_TO_DAT_STOR) :          return(TT4_CMD_SUCCESSFUL);
        case(TT4CMD_RESET_UNIT) :                   return(TT4_CMD_SUCCESSFUL);
        case(TT4CMD_COPY_SP_TO_USER_CFG) :          return(TT4_CMD_SUCCESSFUL);
        case(TT4CMD_COPY_USER_CFG_TO_SP) :          return(TT4_CMD_SUCCESSFUL);
        case(TT4CMD_COPY_SP_TO_USER_INFO) :         return(TT4_CMD_SUCCESSFUL);
        case(TT4CMD_COPY_USER_INFO_TO_SP) :         return(TT4_CMD_SUCCESSFUL);
        case(TT4CMD_COPY_TEMP_DATA_PG_TO_SP) :      return(TT4_CMD_SUCCESSFUL);
        case(TT4CMD_SET_CLOCK) :                    return(TT4_CMD_SUCCESSFUL);
        case(TT4CMD_CLR_DATA_MEM_PTR) :             return(TT4_CMD_SUCCESSFUL);
        case(TT4CMD_INIT_UNIT) :                    return(TT4_CMD_SUCCESSFUL);
        case(TT4CMD_START_UNIT) :                   return(TT4_CMD_SUCCESSFUL);
        case(TT4CMD_STOP_UNIT) :                    return(TT4_CMD_SUCCESSFUL);
        case(TT4CMD_COPY_ST_CFG_PG_TO_SP) :         return(TT4_CMD_SUCCESSFUL);
        case(TT4CMD_HI_SPEED_DATA_DWNLD) :          return(TT4_CMD_SUCCESSFUL);
        // TT4 Multi-Alarm Command Set... (new commands)
        case(TT4CMD_COPY_ALM_CFG_PG_TO_SP) :        return(TT4_CMD_SUCCESSFUL);
        case(TT4CMD_COPY_SP_CONT_TO_ALM_CFG_PG) :   return(TT4_CMD_SUCCESSFUL);
        case(TT4CMD_COPY_PSWD_PG_TO_SP) :           return(TT4_CMD_SUCCESSFUL);
        case(TT4CMD_COPY_SP_CONT_TP_PSWD_PG) :      return(TT4_CMD_SUCCESSFUL);
        case(TT4CMD_COPY_3PT_CERT_PG_TO_SP) :       return(TT4_CMD_SUCCESSFUL);
        case(TT4CMD_SP_CONT_TO_3PT_CERT_PG) :       return(TT4_CMD_SUCCESSFUL);
        case(TT4CMD_READ_1B_FROM_RAM_MEM) :         return(TT4_CMD_SUCCESSFUL);
        case(TT4CMD_WRITE_1B_TO_RAM_MEM) :          return(TT4_CMD_SUCCESSFUL);
        case(TT4CMD_EEPROM_READ_COMMAND) :          return(TT4_CMD_SUCCESSFUL);
        case(TT4CMD_EEPROM_WRITE_COMMAND) :         return(TT4_CMD_SUCCESSFUL);
        case(TT4CMD_GET_MONI_INFO_COMMAND) :        return(TT4_CMD_SUCCESSFUL);
    }           
    
    return(TT4_CMD_UNKNOWN);                 
}


/*******************************************************************************
**  Function     : hwUSB_TT4MA_ProcessCommandReq
**  Description  : CDC input/output command processor
**  PreCondition : succesfull command can only call this sub function.. 
**  Side Effects : none
**  Input        : 
**                   par_ubCommandValue - command value
**                   par_ubParameterValue - parameter value
**                   par_ubNumBytesRead - number of bytes read
**  Output       : 
**                   TT4C_PROC_CMD_STAT_IN - accepts incoming data.
**                   TT4C_PROC_CMD_STAT_OUT - send's out data. 
**                   TT4C_PROC_CMD_STAT_DONE - overall task complete.
**                   TT4C_PROC_CMD_STAT_CLOSE - specific command task complete.
**                   TT4C_PROC_CMD_STAT_OPEN - specific command task not yet complete.
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
UINT8 hwUSB_TT4MA_ProcessCommandReq(UINT8 par_ubCommandValue, UINT8 par_ubParameterValue, UINT8 par_ubNumBytesRead){
    
    register UINT8 lubInputStatus;
    
    // check in the command valid list...
    switch(par_ubCommandValue){                
        //---------------------------------
        // TT4 Command Set... (Original)
        //---------------------------------
        case(TT4CMD_PACKETZED_DWNLD) :              
        
            // Note: there might be a possibility of irregular frame size...        
            lubInputStatus = hwUSB_TT4MA_ProcReq_0x20(par_ubParameterValue);
            
            // this is provided since the max CDC buffer is 64 bytes per transaction...
            if(lubInputStatus==1){
                return(TT4C_PROC_CMD_STAT_OUT); // send data packet from host...
            }    
            else{
                return(TT4C_PROC_CMD_STAT_DONE);
            }

        case(TT4CMD_PACKETZED_DWNLD_MA) :
             lubInputStatus = hwUSB_TT4MA_ProcReq_0x2D(par_ubParameterValue);

            // this is provided since the max CDC buffer is 64 bytes per transaction...
            if(lubInputStatus==1){
                return(TT4C_PROC_CMD_STAT_OUT); // send data packet from host...
            }
            else{
                return(TT4C_PROC_CMD_STAT_DONE);
            }
            
        case(TT4CMD_PRI_SEC_DATA_ONLY_DWNLD) :   
            
            // Note: there might be a possibility of irregular frame size...
            
            lubInputStatus = hwUSB_TT4MA_ProcReq_0x2A(par_ubParameterValue);
            
            // this is provided since the max CDC buffer is 64 bytes per transaction...
            if(lubInputStatus==1){
                return(TT4C_PROC_CMD_STAT_OUT); // send data packet from host...
            }    
            else{
                return(TT4C_PROC_CMD_STAT_DONE);
            }    
            
        case(TT4CMD_WRITE_CERT_DATA) :      
        
            lubInputStatus = hwUSB_TT4MA_ProcReq_0x2B(par_ubParameterValue,par_ubNumBytesRead);
            
            // this is provided since the max CDC buffer is 64 bytes per transaction...
            if(lubInputStatus==1){
                return(TT4C_PROC_CMD_STAT_IN); // receive data packet from host...
            }    
            else{
                return(TT4C_PROC_CMD_STAT_DONE);
            }    
        
        case(TT4CMD_READ_CERT_DATA) :               
        
            lubInputStatus = hwUSB_TT4MA_ProcReq_0x2C(par_ubParameterValue);
            // this is provided since the max CDC buffer is 64 bytes per transaction...
            if(lubInputStatus==1){// send data packet to host...
                return(TT4C_PROC_CMD_STAT_OUT); // send data packet to host...
            }    
            else{
                return(TT4C_PROC_CMD_STAT_DONE);
            }    
            
        case(TT4CMD_GET_ID) :                       
            hwUSB_TT4MA_ProcReq_0x33(par_ubParameterValue);
            return(TT4C_PROC_CMD_STAT_DONE);
            
        case(TT4CMD_SKIP_ID) :                      
        
            hwUSB_TT4MA_ProcReq_0x37(par_ubParameterValue);
            return(TT4C_PROC_CMD_STAT_DONE);
        
        case(TT4CMD_TERMINATE_COMM) :      
            
            hwUSB_TT4MA_ProcReq_0x38(par_ubParameterValue);                 
            return(TT4C_PROC_CMD_STAT_CLOSE); // terminate TT4 communication!!
            
        case(TT4CMD_MOVE_UNIT_STAT_SP) :            
        
            hwUSB_TT4MA_ProcReq_0x44(par_ubParameterValue);
            return(TT4C_PROC_CMD_STAT_DONE);
            
        case(TT4CMD_WRITE_SP) :                     
            
            lubInputStatus = hwUSB_TT4MA_ProcReq_0x45(par_ubParameterValue);
            
            // this is provided since the max CDC buffer is 64 bytes per transfer...
            if(lubInputStatus==1){// acquire data packet from host...
                return(TT4C_PROC_CMD_STAT_IN);
            }    
            else{
                return(TT4C_PROC_CMD_STAT_DONE);
            }    
            
        case(TT4CMD_READ_SP) :                      
            
            hwUSB_TT4MA_ProcReq_0x46(par_ubParameterValue);
            return(TT4C_PROC_CMD_STAT_DONE);
            
        case(TT4CMD_LOGIN_SENSITECH) :              
            
            hwUSB_TT4MA_ProcReq_0x47(par_ubParameterValue);
            return(TT4C_PROC_CMD_STAT_DONE);
            
        case(TT4CMD_LOGIN_USER) :                   
            
            hwUSB_TT4MA_ProcReq_0x48(par_ubParameterValue);
            return(TT4C_PROC_CMD_STAT_DONE);
            
        case(TT4CMD_C0PY_SP_TO_ST_CFG) :            
            
            hwUSB_TT4MA_ProcReq_0x50(par_ubParameterValue);
            return(TT4C_PROC_CMD_STAT_DONE);
            
        case(TT4CMD_C0PY_SP_TO_DAT_STOR) :          
            
            hwUSB_TT4MA_ProcReq_0x51(par_ubParameterValue);
            return(TT4C_PROC_CMD_STAT_DONE);
            
        case(TT4CMD_RESET_UNIT) :                       
            
            hwUSB_TT4MA_ProcReq_0x53(par_ubParameterValue);
            return(TT4C_PROC_CMD_STAT_DONE);
            
        case(TT4CMD_COPY_SP_TO_USER_CFG) :          
            
            hwUSB_TT4MA_ProcReq_0x60(par_ubParameterValue);
            return(TT4C_PROC_CMD_STAT_DONE);
            
        case(TT4CMD_COPY_USER_CFG_TO_SP) :          
            
            hwUSB_TT4MA_ProcReq_0x61(par_ubParameterValue);
            return(TT4C_PROC_CMD_STAT_DONE);
            
        case(TT4CMD_COPY_SP_TO_USER_INFO) :             
            
            hwUSB_TT4MA_ProcReq_0x62(par_ubParameterValue);
            return(TT4C_PROC_CMD_STAT_DONE);
            
        case(TT4CMD_COPY_USER_INFO_TO_SP) :         
            
            hwUSB_TT4MA_ProcReq_0x63(par_ubParameterValue);
            return(TT4C_PROC_CMD_STAT_DONE);
            
        case(TT4CMD_COPY_TEMP_DATA_PG_TO_SP) :      
            
            hwUSB_TT4MA_ProcReq_0x64(par_ubParameterValue);
            return(TT4C_PROC_CMD_STAT_DONE);
                
        case(TT4CMD_SET_CLOCK) :                    
             
            drvRTC_SetupRTCC();

            lubInputStatus = hwUSB_TT4MA_ProcReq_0x65(par_ubParameterValue);
            
            // this is provided since the max CDC buffer is 64 bytes per transaction...
            if(lubInputStatus==1){// acquire data packet from host...
                return(TT4C_PROC_CMD_STAT_IN);
            }    
            else{
                return(TT4C_PROC_CMD_STAT_DONE);
            }    
            
        case(TT4CMD_CLR_DATA_MEM_PTR) :             
            
            hwUSB_TT4MA_ProcReq_0x66(par_ubParameterValue);
            return(TT4C_PROC_CMD_STAT_DONE);
            
        case(TT4CMD_INIT_UNIT) :                    
            
            hwUSB_TT4MA_ProcReq_0x67(par_ubParameterValue);
            return(TT4C_PROC_CMD_STAT_DONE);
            
        case(TT4CMD_START_UNIT) :                   
            
            hwUSB_TT4MA_ProcReq_0x68(par_ubParameterValue);
            return(TT4C_PROC_CMD_STAT_DONE);
            
        case(TT4CMD_STOP_UNIT) :                    
            
            hwUSB_TT4MA_ProcReq_0x69(par_ubParameterValue);
            return(TT4C_PROC_CMD_STAT_DONE);
            
        case(TT4CMD_COPY_ST_CFG_PG_TO_SP) :         
            
            hwUSB_TT4MA_ProcReq_0x6A(par_ubParameterValue);
            return(TT4C_PROC_CMD_STAT_DONE);
            
        case(TT4CMD_HI_SPEED_DATA_DWNLD) :          
            
            lubInputStatus = hwUSB_TT4MA_ProcReq_0x6E(par_ubParameterValue);
            
            // this is provided since the max CDC buffer is 64 bytes per transaction...
            if(lubInputStatus==1){// send data packet to host...
                return(TT4C_PROC_CMD_STAT_OUT);
            }    
            else{
                return(TT4C_PROC_CMD_STAT_DONE);
            }    
         
        //-----------------------------------------------  
        // TT4 Multi-Alarm Command Set... (new commands)
        //-----------------------------------------------
        case(TT4CMD_COPY_ALM_CFG_PG_TO_SP) :        
            
            hwUSB_TT4MA_ProcReq_0x54(par_ubParameterValue);
            return(TT4C_PROC_CMD_STAT_DONE);
            
        case(TT4CMD_COPY_SP_CONT_TO_ALM_CFG_PG) :   
            
            hwUSB_TT4MA_ProcReq_0x55(par_ubParameterValue);
            return(TT4C_PROC_CMD_STAT_DONE);
            
        case(TT4CMD_COPY_PSWD_PG_TO_SP) :           
        
            hwUSB_TT4MA_ProcReq_0x56(par_ubParameterValue);
            return(TT4C_PROC_CMD_STAT_DONE);
            
        case(TT4CMD_COPY_SP_CONT_TP_PSWD_PG) :      
            
            hwUSB_TT4MA_ProcReq_0x57(par_ubParameterValue);
            return(TT4C_PROC_CMD_STAT_DONE);
            
        case(TT4CMD_COPY_3PT_CERT_PG_TO_SP) :       
        
            hwUSB_TT4MA_ProcReq_0x58(par_ubParameterValue);
            return(TT4C_PROC_CMD_STAT_DONE);
            
        case(TT4CMD_SP_CONT_TO_3PT_CERT_PG) :       
            
            hwUSB_TT4MA_ProcReq_0x59(par_ubParameterValue);
            return(TT4C_PROC_CMD_STAT_DONE);
            
        case(TT4CMD_READ_1B_FROM_RAM_MEM) :         
            
            hwUSB_TT4MA_ProcReq_0x72(par_ubParameterValue);
            return(TT4C_PROC_CMD_STAT_DONE);
            
        case(TT4CMD_WRITE_1B_TO_RAM_MEM) :          
            
            hwUSB_TT4MA_ProcReq_0x73(par_ubParameterValue);
            return(TT4C_PROC_CMD_STAT_DONE);
        
        case(TT4CMD_EEPROM_READ_COMMAND) :          
            
            lubInputStatus = hwUSB_TT4MA_ProcReq_0x74(par_ubParameterValue);

            // this is provided since the max CDC buffer is 64 bytes per transaction...
            if(lubInputStatus==1){// send data packet to host...
                return(TT4C_PROC_CMD_STAT_OUT);
            }    
            else{
                return(TT4C_PROC_CMD_STAT_DONE);
            }    
            
        case(TT4CMD_EEPROM_WRITE_COMMAND) :         
            
            lubInputStatus = hwUSB_TT4MA_ProcReq_0x75(par_ubParameterValue);
            
            // this is provided since the max CDC buffer is 64 bytes per transaction...
            if(lubInputStatus==1){// receive data packet from host...
                return(TT4C_PROC_CMD_STAT_IN);
            }    
            else{
                return(TT4C_PROC_CMD_STAT_DONE);
            }    
            
        case(TT4CMD_GET_MONI_INFO_COMMAND) :        
            
            lubInputStatus = hwUSB_TT4MA_ProcReq_0x76(par_ubParameterValue);

            // this is provided since the max CDC buffer is 64 bytes per transaction...
            if(lubInputStatus==1){// send data packet to host...
                return(TT4C_PROC_CMD_STAT_OUT);
            }    
            else{
                return(TT4C_PROC_CMD_STAT_DONE);
            }    

    }           
    
    return(TT4C_PROC_CMD_STAT_OPEN);
}


/*******************************************************************************
**  Function     : hwUSB_TT4MA_SendACKToHost
**  Description  : send's acknowledgement to HOST
**  PreCondition : none
**  Side Effects : none
**  Input        : par_ubCommandStatusCode - command status code
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void hwUSB_TT4MA_SendACKToHost(UINT8 par_ubCommandStatusCode){
    
    // send back command success....
    USB_In_Buffer[0] = par_ubCommandStatusCode;      
    putUSBUSART(USB_In_Buffer,1);         
}            


/*******************************************************************************
**  Function     : hwUSB_TT4MA_CalculateChksum
**  Description  : checksum calculator of packetized download
**  PreCondition : none
**  Side Effects : none
**  Input        : *par_ubBytes - array of data bytes
**                  par_uwTotalBytes - number of byte to be calculated
**  Output       : checksum value
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
UINT8 hwUSB_TT4MA_CalculateChksum(UINT8 *par_ubBytes, UINT16 par_uwTotalBytes){
    
    register _sDWORDBITDATA _sl4Byte;
    register UINT16 luwCtr = 0;
    
    _sl4Byte.DwData = 0;

    do{        
        // get pointer data...
        _sl4Byte.DwData += *par_ubBytes;
        
        // increment pointer address...
        ++par_ubBytes;
        
        // increment byte index...
        ++luwCtr;
        
    }while(luwCtr < par_uwTotalBytes);
    
    // as checksum requirement just 
    // take only the LSB byte value..
    return(_sl4Byte.Byte.B0);
    
}



/*******************************************************************************
**  Function     : hwUSB_TT4MA_ProcReq_0x20
**  Description  : packetized download - TT4 Command 0x20
**  PreCondition : none
**  Side Effects : none
**  Input        : par_ubParameterValue - parameter value
**  Output       : 0 - transaction done  
**                 1 - transaction unfinished
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
UINT8 hwUSB_TT4MA_ProcReq_0x20(UINT8 par_ubParameterValue){

    // working status level..
    static UINT8 lubGetData = 0;
    static UINT8 lubDataPacketStatus = 0;
    
    // checksum storage..
    static UINT16 luwTotalBytes = 0; // excluding checksum...   
    static UINT32 ludwSumUp = 0; // open checksum accummulator...
    static UINT8 lubCheckSumValue = 0;
    
    // datapoint framing and extra byte..
    static UINT16 luwFrameCtr = 0;
    static UINT16 luwFrameExtraBytes = 0;
    static UINT16 luwNumberOfFrames = 0;
    static UINT16 luwDataPointNumber = 0;
    
    // status entry flags...
    static UINT8 lubSendBasicData = 0;
    static UINT8 lubMaxFrameDone = 0;
    static UINT8 lubSingleFrame = 0;
    
    
    register _sDWORDBITDATA _sl4Byte;
    register UINT16 luwCtr;
    register UINT8  lubBytesToSend;
    UINT16 luwDataPointData;
    UINT8  lubEEByteData;
    
    // TT4 Legacy frame control vars...
    static BOOL lbTT4LegacyCfgData = FALSE;
    static BOOL lbTT4PacketizedData = TRUE;
    static UINT16 luwTT4LegacyFrames = 0;
    
    // check received parameter if valid..
    if(par_ubParameterValue!=0x00){
         hwUSB_TT4MA_SendACKToHost(TT4_BAD_PARAM);    
         return(0);
    }     


    ///////////////////////////////////////////////////////////////
    //  process comamnd here first before sending ackowledgement!.. 
    ///////////////////////////////////////////////////////////////    

    // get 1st the main data header information..
    if(lubGetData == 0){        
        
        // 32 bytes is the basic memory data...
        // since datapoint is an integer value then should be multiplied by 2... 


        // protocol ID..
        commTT4Cfg_ReadCfgEeprom(OFFSET_PROTOCOL_ID,(UINT8 *)&gubTT4Cfg_EEBuffer[0]);
        _sl4Byte.DwData = 0;
        _sl4Byte.Byte.B0 = gubTT4Cfg_EEBuffer[0];
        _sl4Byte.Byte.B1 = gubTT4Cfg_EEBuffer[1];        
        _gsSensitech_Configuration_Area.uwProtocolID = _sl4Byte.DwData;        
        gubTT4Cfg_DataBuffer[0] = _sl4Byte.Byte.B0;   
        gubTT4Cfg_DataBuffer[1] = _sl4Byte.Byte.B1;   
        
        // firmware version..
        commTT4Cfg_ReadCfgEeprom(OFFSET_FIRMWARE_VER,(UINT8 *)&gubTT4Cfg_EEBuffer[0]);
        _sl4Byte.DwData = 0;
        _sl4Byte.Byte.B0 = gubTT4Cfg_EEBuffer[0];
        _sl4Byte.Byte.B1 = gubTT4Cfg_EEBuffer[1];        
        _gsSensitech_Configuration_Area.uwFirmwareVer = _sl4Byte.DwData;
        gubTT4Cfg_DataBuffer[2] = _sl4Byte.Byte.B0;   
        gubTT4Cfg_DataBuffer[3] = _sl4Byte.Byte.B1;   
    
        // manufacturing serial number...
        commTT4Cfg_ReadCfgEeprom(OFFSET_MANUFACTURING_SERIAL_NUM,(UINT8 *)&gubTT4Cfg_EEBuffer[0]);
        _sl4Byte.DwData = 0;
        _sl4Byte.Byte.B0 = gubTT4Cfg_EEBuffer[0];
        _sl4Byte.Byte.B1 = gubTT4Cfg_EEBuffer[1];        
        _sl4Byte.Byte.B2 = gubTT4Cfg_EEBuffer[2];
        _sl4Byte.Byte.B3 = gubTT4Cfg_EEBuffer[3];        
        _gsSensitech_Configuration_Area.udwManufacturingSerialNumber = _sl4Byte.DwData;
        gubTT4Cfg_DataBuffer[4] = _sl4Byte.Byte.B0;   
        gubTT4Cfg_DataBuffer[5] = _sl4Byte.Byte.B1;       
        gubTT4Cfg_DataBuffer[6] = _sl4Byte.Byte.B2;   
        gubTT4Cfg_DataBuffer[7] = _sl4Byte.Byte.B3;  
     
        // current time..        
        gudwTT4MA_CurrentTime = drvRTC_ReadRTCC_CurrentTime();
        _sl4Byte.DwData = gudwTT4MA_CurrentTime; 
        gubTT4Cfg_DataBuffer[8] = _sl4Byte.Byte.B0;   
        gubTT4Cfg_DataBuffer[9] = _sl4Byte.Byte.B1;   
        gubTT4Cfg_DataBuffer[10] = _sl4Byte.Byte.B2;   
        gubTT4Cfg_DataBuffer[11] = _sl4Byte.Byte.B3;   
        
        // Last Temperature Measured...
        _sl4Byte.DwData = 0;
        _sl4Byte.DwData = devSensor_GetSensitechBinaryTempData();
        gubTT4Cfg_DataBuffer[12] = _sl4Byte.Byte.B0;   
        gubTT4Cfg_DataBuffer[13] = _sl4Byte.Byte.B1;   

        // command byte.. 
        commTT4Cfg_ReadCfgEeprom(OFFSET_COMMAND_BYTE,(UINT8 *)&gubTT4Cfg_EEBuffer[0]);
        _gsSensitech_Configuration_Area.ubCommandByte = gubTT4Cfg_EEBuffer[0]; // status of monitor; 
        gubTT4Cfg_DataBuffer[14] = gubTT4Cfg_EEBuffer[0];           
        
        // alarm status byte..
        commTT4Cfg_ReadCfgEeprom(OFFSET_ALARM_STATUS,(UINT8 *)&gubTT4Cfg_EEBuffer[0]);
        _gsUser_Configuration_Area.ubAlarmStatus = gubTT4Cfg_EEBuffer[0];
        gubTT4Cfg_DataBuffer[15] = gubTT4Cfg_EEBuffer[0];
    
        // number of strored data points..
        commTT4Cfg_ReadCfgEeprom(OFFSET_NUMBER_OF_STORED_DATAPOINTS,(UINT8 *)&gubTT4Cfg_EEBuffer[0]);
        _sl4Byte.DwData = 0;
        _sl4Byte.Byte.B0 = gubTT4Cfg_EEBuffer[0];
        _sl4Byte.Byte.B1 = gubTT4Cfg_EEBuffer[1];   
        _gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints = _sl4Byte.Word.Lo; 
        gubTT4Cfg_DataBuffer[16] = _sl4Byte.Byte.B0;   
        gubTT4Cfg_DataBuffer[17] = _sl4Byte.Byte.B1;   
        // 32 bytes plus calculated number of stored data points..
        luwTotalBytes = 32 + (_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints * 2);        

        // 14 bytes unused...
        utilArrayVarInitializer((UINT8 *)&gubTT4Cfg_DataBuffer[18],14,0); // 14 bytes, set to 0 value
        
        // initial sump up...
        ludwSumUp = 0;
        luwCtr = 0;
        do{
            
            ludwSumUp += gubTT4Cfg_DataBuffer[luwCtr];
            ++luwCtr;
            
        }while(luwCtr<32);
        
        //----------------------------------------------------------------------------
        // Note: this procedure was not explained in the TT4 Multi-Alarm Comm Doc, 
        // this information was explained by Peter verbally during his visit in BOI 
        // last 3/11/11. This method taken from the old TT4 code.
        //----------------------------------------------------------------------------
        // fetch address offset 0 - 255 from EEPROM....
        for(luwCtr=0;luwCtr<256;luwCtr++){
            
            // read all EERPOM data from offset 0 - 760...
            devEEPROM_RandomRead(luwCtr,&lubEEByteData);
            
            // load current EEPROM byte data to the data buffer array starting from array offset 32...
            gubTT4Cfg_DataBuffer[32 + luwCtr] = lubEEByteData;
            
            // continue to accumulate running checksum value!...
            ludwSumUp += lubEEByteData;        
        }
        
        // accumulate total bytes to send....
        // add 256 additional bytes from EEPROM ...
        luwTotalBytes +=256;      
        
        luwTotalBytes +=1; // 1byte checksum...
        
        // load the total number of bytes to bitfield..
        _sl4Byte.DwData = 0;        
        _sl4Byte.DwData = luwTotalBytes; 
      
        // add total bytes and take the checksum....  
        ludwSumUp += _sl4Byte.Byte.B0; // send LSB first
        ludwSumUp += _sl4Byte.Byte.B1; 
        ludwSumUp += _sl4Byte.Byte.B2; // send MSB first
        
        
    
        //------------------------------------------------------------
        // Calculate Datapoint total frames and extra bytes...
        //------------------------------------------------------------                
        if(_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints>0){
            luwNumberOfFrames = (_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints * 2) / 64; // max data payload frame size is 64 bytes..
            //check the number frame status..
            if(luwNumberOfFrames==0)
                lubMaxFrameDone=1; // less then 1 frame
            else
                lubMaxFrameDone=0;
        
            luwFrameExtraBytes = (_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints * 2) % 64; // take the extra bytes if any!        
            // check the extra byte status..
            if(luwFrameExtraBytes==0) 
                lubSingleFrame = 1; // no! extra bytes...
            else 
                lubSingleFrame = 0; // have extra bytes...    
        }
        else{
            lubMaxFrameDone = 1;
            lubSingleFrame = 1;
        }    

            
        lubGetData = 1;// finished acquiring data from TT4 device..
        
        lubDataPacketStatus = 0; // reset here the starting at level 0...
        luwFrameCtr = 0; // reset the frame counter for sure!..
    }    


    // start sending data to Host..    

    switch(lubDataPacketStatus){
        
        case(0) : // send successful command status...
        
            lubDataPacketStatus = 1; // go to next step..
            
            // send back command success to host....
            hwUSB_TT4MA_SendACKToHost(TT4_CMD_SUCCESSFUL);
        
            return(1); // transaction not end!..
        
        case(1) : // send first the 3 bytes info as total bytes to send...
            
            lubDataPacketStatus = 2; // go to next step..
            
            _sl4Byte.DwData = luwTotalBytes; 
            USB_In_Buffer[0] = _sl4Byte.Byte.B0; // send LSB first
            USB_In_Buffer[1] = _sl4Byte.Byte.B1;
            USB_In_Buffer[2] = _sl4Byte.Byte.B2; // send MSB last          
        
            lubBytesToSend = 3; // this is the total bytes to be sent out...
            // send out data packet...   
            putUSBUSART(USB_In_Buffer,lubBytesToSend); 
        
            return(1); // transaction not end!..
        
        case(2) : // send irregular number of data bytes that will not fit into 64 bytes frame...            
            
            
            if(lubSendBasicData == 0){
            
                if(lbTT4PacketizedData == TRUE){
                    
                    lubBytesToSend = 32; // this is the total bytes to be sent out...
                
                    memcpy((void *)&USB_In_Buffer[0],(const void *)&gubTT4Cfg_DataBuffer[0],lubBytesToSend);

                    // send out data packet...   
                    putUSBUSART(USB_In_Buffer,lubBytesToSend); 
                    
                    lbTT4PacketizedData = FALSE;
                    
                    return(1); // transaction not end!..                
                    
                }
                else{
                    
                    lubBytesToSend = 64; // this is the total bytes to be sent out...
                
                    memcpy((void *)&USB_In_Buffer[0],(const void *)&gubTT4Cfg_DataBuffer[32 + luwTT4LegacyFrames],lubBytesToSend);

                    // send out data packet...   
                    putUSBUSART(USB_In_Buffer,lubBytesToSend); 
                    
                    luwTT4LegacyFrames += 64;
                    
                    if(luwTT4LegacyFrames>=256) lbTT4LegacyCfgData = TRUE;
                
                }
                
                
                if(lbTT4LegacyCfgData == TRUE){                
                    // this will be sent onetime only..
                    lubSendBasicData = 1;
                }
                
                return(1); // transaction not end!..                
            
            }
            else{
            
                if(lubMaxFrameDone==0){
            
                    luwCtr = 0;
                    do{
                        
                        // retrieve raw datapoint, raw means the datapoint in binary form 
                        // with 4bit mark status at the MSB portion..
                        commTT4Cfg_ReadEeprom_RawDataPoint(luwDataPointNumber,&luwDataPointData);
                        
                        _sl4Byte.DwData = (UINT32)luwDataPointData;
                        
                        USB_In_Buffer[luwCtr*2] = (char)_sl4Byte.Byte.B0;                        
                        ludwSumUp += (char)_sl4Byte.Byte.B0; // accummulate checksum value...
                        
                        USB_In_Buffer[1+(luwCtr*2)] = (char)_sl4Byte.Byte.B1;                        
                        ludwSumUp += (char)_sl4Byte.Byte.B1; // accummulate checksum value...
                        
                        // increment data point number...
                        // this value will be preserved everytime it enter's in this routine...
                        ++luwDataPointNumber;
                        
                        // increment counter, it was 32 only because the datapoint is an integer format..
                        // if multiplied by 2 then it reaches to 64 bytes size...
                        ++luwCtr;
                        
                    }while(luwCtr<32);                       
                
                    lubBytesToSend = 64; // this is the total bytes to be sent out...
                    // send out data packet with fixed 64 bytes frame size...   
                    putUSBUSART(USB_In_Buffer,lubBytesToSend); 
            
                    // increment frame
                    // Note: 64 bytes per frame
                    ++luwFrameCtr;            
                    
                    if(luwFrameCtr<luwNumberOfFrames) return(1); // transaction not end!..                
                    
                    lubMaxFrameDone = 1; // all frames were sent already!... till here!
                    
                    return(1); // transaction still not end!..                
                
                }
                else{
                            
                    if(lubSingleFrame==0){
                   
                        lubSingleFrame=1; //close after sending the last single irregular size frame...

                        luwCtr = 0;
                        do{
                        
                            // retrieve raw datapoint, raw datapoint means is a binary data format 
                            // with 4bit mark status at the MSB portion..
                            commTT4Cfg_ReadEeprom_RawDataPoint(luwDataPointNumber,&luwDataPointData);
                        
                            _sl4Byte.DwData = (UINT32)luwDataPointData;
                        
                            USB_In_Buffer[luwCtr*2] = (char)_sl4Byte.Byte.B0;                        
                            ludwSumUp += (char)_sl4Byte.Byte.B0; // accummulate checksum value...
                        
                            USB_In_Buffer[1+(luwCtr*2)] = (char)_sl4Byte.Byte.B1;                        
                            ludwSumUp += (char)_sl4Byte.Byte.B1; // accummulate checksum value...
                        
                            // increment data point number...
                            // this value will be preserved everytime it enter's in this routine...
                            ++luwDataPointNumber;
                        
                            // increment counter, it was 32 only because the datapoint is an integer format..
                            // if multiplied by 2 then it reaches to 64 bytes size...
                            ++luwCtr;
                        
                        // note that this is an integer counter then extra bytes must be divided by 2 ...                      
                        }while(luwCtr<(luwFrameExtraBytes/2)); 
                
                        lubBytesToSend = luwFrameExtraBytes; // this is the total bytes to be sent out...
                        // send out data packet with fixed 64 bytes frame size...   
                        putUSBUSART(USB_In_Buffer,lubBytesToSend); 
                        
                    } 
                }
            }
            
            
            lubDataPacketStatus = 3; // go to next step..
            
            return(1); // transaction still not end!..
            
        case(3) : // send at the lst portion the check
            
            _sl4Byte.DwData = (UINT32)ludwSumUp; 
            lubCheckSumValue = _sl4Byte.Byte.B0; // take only the LSB byte value...
            USB_In_Buffer[0] = (char)lubCheckSumValue;
            
            lubBytesToSend = 1; // this is the total bytes to be sent out...
            // send out data packet...   
            putUSBUSART(USB_In_Buffer,lubBytesToSend); 
        
            break;
            
    }

    // reset all status flags to zero...
    lubDataPacketStatus = 0; 
    lubGetData = 0;
    lubSendBasicData = 0;
    lubMaxFrameDone = 0;
    lubSingleFrame = 0;
    
    //accummulator and counter must be set to zero..
    luwFrameCtr = 0;
    luwDataPointNumber = 0;    
    ludwSumUp = 0;

    // reset TT4 legacy config data....    
    luwTT4LegacyFrames = 0;
    lbTT4LegacyCfgData = FALSE;
    lbTT4PacketizedData = TRUE;
    
    
    return(0); // data sending transaction finished!
    
} 


/*******************************************************************************
**  Function     : UINT8 hwUSB_TT4MA_ProcReq_0x2A(UINT8 par_ubParameterValue)
**  Description  : Primary/Secondary Data download - TT4 Command 0x2A
**  PreCondition : none
**  Side Effects : none
**  Input        : par_ubParameterValue - parameter value
**  Output       : 
**                 0 - transaction done 
**                 1 - transaction undone
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
UINT8 hwUSB_TT4MA_ProcReq_0x2A(UINT8 par_ubParameterValue){

    // status entry flags...    
    static UINT8 lubMaxFrameDone = 0;
    static UINT8 lubGetData = 0;
    static UINT8 lubDataPacketStatus = 0;
    
    // checksum storage..
    static UINT16 luwTotalBytes = 0; // excluding checksum...   
    static UINT32 ludwSumUp = 0; // open checksum accummulator...
    static UINT8 lubCheckSumValue = 0;
    
    // datapoint framing and extra byte..
    static UINT16 luwFrameCtr = 0;
    static UINT16 luwNumberOfFrames = 0;
    static UINT16 luwEepromOffsetCtr = 0; // starting EEPROM offset zero!!.. mjm
    UINT8  lubByteData = 0;
    static UINT16 luwFrameExtraBytes = 0;
    static UINT8  luwExtraBytesFlag = 0;
    register UINT8 lubCtr = 0;        
    
    register _sDWORDBITDATA _sl4Byte;    
    register UINT8 lubBytesToSend;    
    
    // check received parameter if valid..
    if(par_ubParameterValue!=0x00){
         hwUSB_TT4MA_SendACKToHost(TT4_BAD_PARAM);    
         return(0);
    }     


    ///////////////////////////////////////////////////////////////
    //  process comamnd here first before sending ackowledgement!.. 
    ///////////////////////////////////////////////////////////////    

    // get 1st the main data header information..
    if(lubGetData == 0){        
        
        // retrieve number of datapoints from EEPROM address offset 16 & 17...        
        commTT4Cfg_FastReadCfgEeprom(OFFSET_NUMBER_OF_STORED_DATAPOINTS,(void *)&guwMem);
        _gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints = guwMem;
        
        // since datapoint is an integer value then should be multiplied by 2 plus 768 bytes total configuration byte data... 
        luwTotalBytes = (_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints * 2) + 768u;        
        
    
        //-----------------------------------------------------------------------------------
        // Calculate the total Configuration pages and Datapoint frames and extra bytes...
        //-----------------------------------------------------------------------------------                
        
        // check the number frame status..
        luwNumberOfFrames =  luwTotalBytes / 64;
        
        // check if there is extra bytes...   
        luwFrameExtraBytes =  luwTotalBytes % 64;
        
        // check the extra byte status..
        if(luwFrameExtraBytes != 0) 
            luwExtraBytesFlag = 1; // have extra bytes...    
        else 
            luwExtraBytesFlag = 0; // no! extra bytes...     
        
        /* pre-initiliaze all control vars */    
        lubGetData = 1;// set to 1 after finished acquiring data from TT4 device..
        ludwSumUp = 0; // initialize to zero checksum accumulator ...       
        lubDataPacketStatus = 0; // reset the data sending status starting at level 0...
        luwFrameCtr = 0; // reset the frame counter for sure!..                
        luwEepromOffsetCtr = 0; // set starting point at EEPROM offset zero..        
        lubMaxFrameDone = 0; 
    }    


    // start sending data to Host....    

    switch(lubDataPacketStatus){ // level 0...
        
        case(0) : // send successful command status...
        
            lubDataPacketStatus = 1; // go to next step..
            
            // send back command success to host....
            hwUSB_TT4MA_SendACKToHost(TT4_CMD_SUCCESSFUL);
        
            return(1); // transaction not end!..
        
        case(1) : // send first the 3 bytes info as total bytes to send...
            
            lubDataPacketStatus = 2; // go to next step..
            
            _sl4Byte.DwData = luwTotalBytes; 
            USB_In_Buffer[0] = _sl4Byte.Byte.B0; // send LSB first
            USB_In_Buffer[1] = _sl4Byte.Byte.B1; 
            USB_In_Buffer[2] = _sl4Byte.Byte.B2; // send MSB last          
        
            lubBytesToSend = 3; // this is the total bytes to be sent out...
            // send out data packet...   
            putUSBUSART(USB_In_Buffer,lubBytesToSend); 
        
            return(1); // transaction not end!..
        
        case(2) : // start sending the 64 bytes per frame...            
                        
            if(lubMaxFrameDone==0){
                
                lubCtr = 0;
                do{                        
                    // fetch EEPROM data.....
                    devEEPROM_RandomRead(luwEepromOffsetCtr, &lubByteData);
                        
                    USB_In_Buffer[lubCtr] = (char)lubByteData; 
                    
                    ludwSumUp += (UINT32)lubByteData;                    
                    
                    ++luwEepromOffsetCtr;
                    
                    ++lubCtr;
                
                }while(lubCtr < 64);
                
                lubBytesToSend = 64; // this is the total bytes to be sent out...
                // send out data packet with fixed 64 bytes frame size...   
                putUSBUSART(USB_In_Buffer,lubBytesToSend); 
            
                // increment frame
                // Note: 64 bytes per frame
                ++luwFrameCtr;            
                    
                if(luwFrameCtr < luwNumberOfFrames) return(1); // transaction not end!..                
                    
                lubMaxFrameDone = 1; // all frames were sent already!... till here!                
                    
                // if no extra bytes then go to next step..
                if(luwExtraBytesFlag == 0) lubDataPacketStatus = 3;

                return(1); // transaction still not end!..                                            
            }
            else{
                
                // send out to serial CDC if there is any extra bytes
                // after sending all frames..... mjm
                lubCtr = 0;
                do{                        
                    // fetch EEPROM data.....                    
                    devEEPROM_RandomRead(luwEepromOffsetCtr, &lubByteData);
                        
                    USB_In_Buffer[lubCtr] = (char)lubByteData; 
                    
                    ludwSumUp += (UINT32)lubByteData;                    
                    
                    ++luwEepromOffsetCtr;
                    
                    ++lubCtr;
                
                }while(lubCtr < luwFrameExtraBytes);
                
                lubBytesToSend = luwFrameExtraBytes; // this is the total bytes to be sent out...
                
                // send out data packet less than 64 bytes frame size...   
                putUSBUSART(USB_In_Buffer,lubBytesToSend);  

                lubDataPacketStatus = 3; // go to next step....
            
                return(1); // transaction still not end!....
            }            
            
        case(3) : // send at the lst portion the check
            
            _sl4Byte.DwData = (UINT32)ludwSumUp;
            lubCheckSumValue = _sl4Byte.Byte.B0; // take only the LSB byte value...
            USB_In_Buffer[0] = (char)lubCheckSumValue;
            
            lubBytesToSend = 1; // this is the total bytes to be sent out...
            // send out data packet...   
            putUSBUSART(USB_In_Buffer,lubBytesToSend); 
        
            break;
            
    }

    // reset all status flags to zero...
    lubDataPacketStatus = 0; 
    lubGetData = 0;    
    lubMaxFrameDone = 0;
    
    //accummulator and counter must be set to zero..
    luwFrameCtr = 0;    
    ludwSumUp = 0;
    luwEepromOffsetCtr = 0;
    
    return(0); // data sending transaction finished!
   
}


/*******************************************************************************
**  Function     : hwUSB_TT4MA_ProcReq_0x2B
**  Description  : Write Certification Data - TT4 Command 0x2B
**  PreCondition : none
**  Side Effects : none
**  Input        : 
**                 par_ubParameterValue - parameter value
**                 par_ubNumBytesRead - number of byte to be read
**  Output       : 
**                 0 - transaction done  
**                 1 - transaction undone
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
UINT8 hwUSB_TT4MA_ProcReq_0x2B(UINT8 par_ubParameterValue, UINT8 par_ubNumBytesRead){

    static UINT16 luwPageNumber = 0;  
    static UINT16 luwPageCtr = 0; // 64 bytes per transfer.. 320/64 = 5 tranfer cycles...
    static UINT8 lubFetchHostData = 0;
    static UINT16 luwCtr = 0;
    static UINT8 lubDataComplete = 0;
    
    if(lubFetchHostData == 0){
        // check received parameter if valid..
        if(par_ubParameterValue!=0x00){ // parameter code...
            hwUSB_TT4MA_SendACKToHost(TT4_BAD_PARAM);    
            return(0);
        }
        else{
            hwUSB_TT4MA_SendACKToHost(TT4_CMD_SUCCESSFUL); 
            lubFetchHostData = 1; // do not allow to enter here when it comes back to receive data...
            return(1); // send request to acquire data...
        }
    }
    
    
    //--------------------------------------------
    // 
    //--------------------------------------------        
    if(lubDataComplete==0){
        
        // here 6x...
        memcpy((void *)&gubTT4Cfg_DataBuffer[64 * luwCtr],(const void *)&USB_Out_Buffer[0],par_ubNumBytesRead);
    
        ++luwCtr;   
    
        // till 5 cycles only...
        if(luwCtr<6){
            return(1); // send request to acquire data...
        }
        
        luwCtr = 0;        
        lubDataComplete = 1;             
    }
    
    
    
    // save to 320 bytes to EEPROM at 3point Certificate data memory location starting offset 448...
    luwPageCtr = 0;
    do{
        
        luwPageNumber = (32 * 14) + (luwPageCtr * 64); // 3 point certificate page... 5x only..    
        
        // note: the data information starts at offset 3..
        devEEPROM_PageWrite(luwPageNumber,(UINT8 *)&gubTT4Cfg_DataBuffer[3 + (64 * luwPageCtr)],64); // write 64 bytes to eeprom
        utilDelay_ms(7);
    
        ++luwPageCtr;
        
    }while(luwPageCtr<5);
    
        
    // reset host data to zero to enable again, to enter this module...    
    lubFetchHostData = 0;
    luwPageNumber = 0;  
    luwPageCtr = 0;
    luwCtr = 0;
    
    return(0); // acquiring data end...

}


/*******************************************************************************
**  Function     : hwUSB_TT4MA_ProcReq_0x2C
**  Description  : Read Certification Data - TT4 Command 0x2C
**  PreCondition : none
**  Side Effects : none
**  Input        : 
**                 par_ubParameterValue - parameter value
**  Output       : 
**                 0 - transaction done  
**                 1 - transaction undone
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
UINT8 hwUSB_TT4MA_ProcReq_0x2C(UINT8 par_ubParameterValue){

    static UINT8 lubGetData = 0;
    static UINT8 lubDataPacketStatus = 0;
    static UINT8 lubCheckSumValue = 0;
    static UINT16 luwFrameCtr = 0;
    static UINT16 luwTotalBytes = 0;
    
    register _sDWORDBITDATA _sl4Byte;
    UINT8  lubData;
    register UINT16 luwCtr;
    register UINT16 luwPageNumber;
    register UINT8  lubBytesToSend;
    

    // check received parameter if valid..
    if(par_ubParameterValue!=0x00){
         hwUSB_TT4MA_SendACKToHost(TT4_BAD_PARAM);    
         return(0);
    }     


    //////////////////////////////////////////////////////////
    //  process comamnd here first before send ackowledge!.. 
    //////////////////////////////////////////////////////////    

    if(lubGetData == 0){
        
        //-----------------------------------------------------------
        // copy User Configuration from EEPROM Page to Scratch pad 
        //-----------------------------------------------------------     
    
    
        // Total data: 32 bytes x 10 pages = 320 bytes
        luwTotalBytes = 32 * 10; // 320   
    
        // Starting offset address : 448
        // bytes per page : 32
        // page location : 14
        luwPageNumber = 32 * 14; // 3 point certification area offset location...
    
    
        luwCtr = 0;
    
        do{
            // fetch data from EEPROM starting to 448 offset address... 
            devEEPROM_RandomRead(luwPageNumber+luwCtr,&lubData); 
    
            // load to data buffer...    
            gubTT4Cfg_DataBuffer[luwCtr] = lubData;
        
            // increment starting offset address...
            ++luwCtr;
        
        }while(luwCtr<luwTotalBytes);

        // total bytes to send..
        _sl4Byte.DwData = luwTotalBytes; 
        gubTT4Cfg_DataBuffer[luwCtr+0] = _sl4Byte.Byte.B0; // should start at 320!..  
        gubTT4Cfg_DataBuffer[luwCtr+1] = _sl4Byte.Byte.B1; // 321 
        gubTT4Cfg_DataBuffer[luwCtr+2] = _sl4Byte.Byte.B2; // 322  

        //------------------------------------------------------------
        // Calculate Checksum...
        //------------------------------------------------------------
        //luwTotalBytes = 320 + 3;
        // returned checksum value...
        lubCheckSumValue = hwUSB_TT4MA_CalculateChksum((UINT8 *)&gubTT4Cfg_DataBuffer[0],(luwTotalBytes + 3));    
                
        lubGetData = 1;// finished acquiring data from TT4 device..
        
        lubDataPacketStatus = 0; // reset here the starting at level 0...
        luwFrameCtr = 0; // reset the frame counter for sure!..
    }    


    // start sending data to Host..    

    switch(lubDataPacketStatus){
        
        case(0) : // send successful command status...
        
            lubDataPacketStatus = 1; // go to next step..
            
            // send back command success to host....
            hwUSB_TT4MA_SendACKToHost(TT4_CMD_SUCCESSFUL);
        
            return(1); // transaction not end!..
        
        case(1) : // send first the 3 bytes info as total bytes to send...
            
            lubDataPacketStatus = 2; // go to next step..
            
            _sl4Byte.DwData = luwTotalBytes; 
            USB_In_Buffer[0] = _sl4Byte.Byte.B0; // send LSB first
            USB_In_Buffer[1] = _sl4Byte.Byte.B1;
            USB_In_Buffer[2] = _sl4Byte.Byte.B2; // send MSB last          
        
            lubBytesToSend = 3; // this is the total bytes to be sent out...
            // send out data packet...   
            putUSBUSART(USB_In_Buffer,lubBytesToSend); 
        
            return(1); // transaction not end!..
        
        case(2) : // send 320 data bytes...            
            
            // here 5x...
            memcpy((void *)&USB_In_Buffer[0],(const void *)&gubTT4Cfg_DataBuffer[64 * luwFrameCtr],64);
            
            lubBytesToSend = 64; // this is the total bytes to be sent out...
            // send out data packet...   
            putUSBUSART(USB_In_Buffer,lubBytesToSend); 
            
            // increment frame
            // Note: 64 bytes per frame
            ++luwFrameCtr;
            
            if(luwFrameCtr<5){
                //lubDataPacketStatus = 2; // still in status level 2...
                return(1); // transaction not end!..
            }
            
            lubDataPacketStatus = 3; // go to next step..
            
            return(1); // transaction still not end!..
        
        case(3) : // send at the lst portion the check

            
            USB_In_Buffer[0] = (char)lubCheckSumValue;
            
            lubBytesToSend = 1; // this is the total bytes to be sent out...
            // send out data packet...   
            putUSBUSART(USB_In_Buffer,lubBytesToSend); 
        
            break;
    }

    // reset all status to zero...
    lubDataPacketStatus = 0; 
    lubGetData = 0;
    luwFrameCtr = 0;
    
    return(0); // data sending transaction finished!
    
}

UINT8 hwUSB_TT4MA_ProcReq_0x2D(UINT8 par_ubParameterValue){

    // working status level..
    static UINT8 lubGetData = 0;
    static UINT8 lubDataPacketStatus = 0;

    // checksum storage..
    static UINT16 luwTotalBytes = 0; // excluding checksum...
    static UINT32 ludwSumUp = 0; // open checksum accummulator...
    static UINT8 lubCheckSumValue = 0;

    // datapoint framing and extra byte..
    static UINT16 luwFrameCtr = 0;
    static UINT16 luwFrameExtraBytes = 0;
    static UINT16 luwNumberOfFrames = 0;
    static UINT16 luwDataPointNumber = 0;

    // status entry flags...
    static UINT8 lubSendBasicData = 0;
    static UINT8 lubMaxFrameDone = 0;
    static UINT8 lubSingleFrame = 0;


    register _sDWORDBITDATA _sl4Byte;
    register UINT16 luwCtr;
    register UINT8  lubBytesToSend;
    UINT16 luwDataPointData;
    UINT8  lubEEByteData;

    // TT4 Legacy frame control vars...
    static BOOL lbTT4LegacyCfgData = FALSE;
    static BOOL lbTT4PacketizedData = TRUE;
    static UINT16 luwTT4LegacyFrames = 0;

    // check received parameter if valid..
    if(par_ubParameterValue!=0x00){
         hwUSB_TT4MA_SendACKToHost(TT4_BAD_PARAM);
         return(0);
    }


    ///////////////////////////////////////////////////////////////
    //  process comamnd here first before sending ackowledgement!..
    ///////////////////////////////////////////////////////////////

    // get 1st the main data header information..
    if(lubGetData == 0){

        // 32 bytes is the basic memory data...
        // since datapoint is an integer value then should be multiplied by 2...


        // protocol ID..
        commTT4Cfg_ReadCfgEeprom(OFFSET_PROTOCOL_ID,(UINT8 *)&gubTT4Cfg_EEBuffer[0]);
        _sl4Byte.DwData = 0;
        _sl4Byte.Byte.B0 = gubTT4Cfg_EEBuffer[0];
        _sl4Byte.Byte.B1 = gubTT4Cfg_EEBuffer[1];
        _gsSensitech_Configuration_Area.uwProtocolID = _sl4Byte.DwData;
        gubTT4Cfg_DataBuffer[0] = _sl4Byte.Byte.B0;
        gubTT4Cfg_DataBuffer[1] = _sl4Byte.Byte.B1;

        // firmware version..
        commTT4Cfg_ReadCfgEeprom(OFFSET_FIRMWARE_VER,(UINT8 *)&gubTT4Cfg_EEBuffer[0]);
        _sl4Byte.DwData = 0;
        _sl4Byte.Byte.B0 = gubTT4Cfg_EEBuffer[0];
        _sl4Byte.Byte.B1 = gubTT4Cfg_EEBuffer[1];
        _gsSensitech_Configuration_Area.uwFirmwareVer = _sl4Byte.DwData;
        gubTT4Cfg_DataBuffer[2] = _sl4Byte.Byte.B0;
        gubTT4Cfg_DataBuffer[3] = _sl4Byte.Byte.B1;

        // manufacturing serial number...
        commTT4Cfg_ReadCfgEeprom(OFFSET_MANUFACTURING_SERIAL_NUM,(UINT8 *)&gubTT4Cfg_EEBuffer[0]);
        _sl4Byte.DwData = 0;
        _sl4Byte.Byte.B0 = gubTT4Cfg_EEBuffer[0];
        _sl4Byte.Byte.B1 = gubTT4Cfg_EEBuffer[1];
        _sl4Byte.Byte.B2 = gubTT4Cfg_EEBuffer[2];
        _sl4Byte.Byte.B3 = gubTT4Cfg_EEBuffer[3];
        _gsSensitech_Configuration_Area.udwManufacturingSerialNumber = _sl4Byte.DwData;
        gubTT4Cfg_DataBuffer[4] = _sl4Byte.Byte.B0;
        gubTT4Cfg_DataBuffer[5] = _sl4Byte.Byte.B1;
        gubTT4Cfg_DataBuffer[6] = _sl4Byte.Byte.B2;
        gubTT4Cfg_DataBuffer[7] = _sl4Byte.Byte.B3;

        // current time..
        gudwTT4MA_CurrentTime = drvRTC_ReadRTCC_CurrentTime();
        _sl4Byte.DwData = gudwTT4MA_CurrentTime;
        gubTT4Cfg_DataBuffer[8] = _sl4Byte.Byte.B0;
        gubTT4Cfg_DataBuffer[9] = _sl4Byte.Byte.B1;
        gubTT4Cfg_DataBuffer[10] = _sl4Byte.Byte.B2;
        gubTT4Cfg_DataBuffer[11] = _sl4Byte.Byte.B3;

        // Last Temperature Measured...
        _sl4Byte.DwData = 0;
        _sl4Byte.DwData = devSensor_GetSensitechBinaryTempData();
        gubTT4Cfg_DataBuffer[12] = _sl4Byte.Byte.B0;
        gubTT4Cfg_DataBuffer[13] = _sl4Byte.Byte.B1;

        // command byte..
        commTT4Cfg_ReadCfgEeprom(OFFSET_COMMAND_BYTE,(UINT8 *)&gubTT4Cfg_EEBuffer[0]);
        _gsSensitech_Configuration_Area.ubCommandByte = gubTT4Cfg_EEBuffer[0]; // status of monitor;
        gubTT4Cfg_DataBuffer[14] = gubTT4Cfg_EEBuffer[0];

        // alarm status byte..
        commTT4Cfg_ReadCfgEeprom(OFFSET_ALARM_STATUS,(UINT8 *)&gubTT4Cfg_EEBuffer[0]);
        _gsUser_Configuration_Area.ubAlarmStatus = gubTT4Cfg_EEBuffer[0];
        gubTT4Cfg_DataBuffer[15] = gubTT4Cfg_EEBuffer[0];

        // number of strored data points..
        commTT4Cfg_ReadCfgEeprom(OFFSET_NUMBER_OF_STORED_DATAPOINTS,(UINT8 *)&gubTT4Cfg_EEBuffer[0]);
        _sl4Byte.DwData = 0;
        _sl4Byte.Byte.B0 = gubTT4Cfg_EEBuffer[0];
        _sl4Byte.Byte.B1 = gubTT4Cfg_EEBuffer[1];
        _gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints = _sl4Byte.Word.Lo;
        gubTT4Cfg_DataBuffer[16] = _sl4Byte.Byte.B0;
        gubTT4Cfg_DataBuffer[17] = _sl4Byte.Byte.B1;
        // 32 bytes plus calculated number of stored data points..
        luwTotalBytes = 32 + (_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints * 2);

        // 14 bytes unused...
        utilArrayVarInitializer((UINT8 *)&gubTT4Cfg_DataBuffer[18],14,0); // 14 bytes, set to 0 value

        // initial sump up...
        ludwSumUp = 0;
        luwCtr = 0;
        do{

            ludwSumUp += gubTT4Cfg_DataBuffer[luwCtr];
            ++luwCtr;

        }while(luwCtr<32);

        //----------------------------------------------------------------------------
        // Note: this procedure was not explained in the TT4 Multi-Alarm Comm Doc,
        // this information was explained by Peter verbally during his visit in BOI
        // last 3/11/11. This method taken from the old TT4 code.
        //----------------------------------------------------------------------------
        // fetch address offset 0 - 255 from EEPROM....
        for(luwCtr=0;luwCtr<768;luwCtr++){

            // read all EERPOM data from offset 0 - 760...
            devEEPROM_RandomRead(luwCtr,&lubEEByteData);

            // load current EEPROM byte data to the data buffer array starting from array offset 32...
            gubTT4Cfg_DataBuffer[32 + luwCtr] = lubEEByteData;

            // continue to accumulate running checksum value!...
            ludwSumUp += lubEEByteData;
        }

        luwTotalBytes +=768;

        for(luwCtr=0;luwCtr<1920;luwCtr++){

            // read all EERPOM data from offset 0 - 760...
            devEEPROM_RandomRead(luwCtr + 63616,&lubEEByteData);

            // load current EEPROM byte data to the data buffer array starting from array offset 32...
            gubTT4Cfg_DataBuffer[32 + 768 + luwCtr] = lubEEByteData;

            // continue to accumulate running checksum value!...
            ludwSumUp += lubEEByteData;
        }

        // accumulate total bytes to send....
        // add 256 additional bytes from EEPROM ...
        
        luwTotalBytes +=1920;

        luwTotalBytes +=1; // 1byte checksum...

        // load the total number of bytes to bitfield..
        _sl4Byte.DwData = 0;
        _sl4Byte.DwData = luwTotalBytes;

        // add total bytes and take the checksum....
        ludwSumUp += _sl4Byte.Byte.B0; // send LSB first
        ludwSumUp += _sl4Byte.Byte.B1;
        ludwSumUp += _sl4Byte.Byte.B2; // send MSB first



        //------------------------------------------------------------
        // Calculate Datapoint total frames and extra bytes...
        //------------------------------------------------------------
        if(_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints>0){
            luwNumberOfFrames = (_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints * 2) / 64; // max data payload frame size is 64 bytes..
            //check the number frame status..
            if(luwNumberOfFrames==0)
                lubMaxFrameDone=1; // less then 1 frame
            else
                lubMaxFrameDone=0;

            luwFrameExtraBytes = (_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints * 2) % 64; // take the extra bytes if any!
            // check the extra byte status..
            if(luwFrameExtraBytes==0)
                lubSingleFrame = 1; // no! extra bytes...
            else
                lubSingleFrame = 0; // have extra bytes...
        }
        else{
            lubMaxFrameDone = 1;
            lubSingleFrame = 1;
        }


        lubGetData = 1;// finished acquiring data from TT4 device..

        lubDataPacketStatus = 0; // reset here the starting at level 0...
        luwFrameCtr = 0; // reset the frame counter for sure!..
    }


    // start sending data to Host..

    switch(lubDataPacketStatus){

        case(0) : // send successful command status...

            lubDataPacketStatus = 1; // go to next step..

            // send back command success to host....
            hwUSB_TT4MA_SendACKToHost(TT4_CMD_SUCCESSFUL);

            return(1); // transaction not end!..

        case(1) : // send first the 3 bytes info as total bytes to send...

            lubDataPacketStatus = 2; // go to next step..

            _sl4Byte.DwData = luwTotalBytes;
            USB_In_Buffer[0] = _sl4Byte.Byte.B0; // send LSB first
            USB_In_Buffer[1] = _sl4Byte.Byte.B1;
            USB_In_Buffer[2] = _sl4Byte.Byte.B2; // send MSB last

            lubBytesToSend = 3; // this is the total bytes to be sent out...
            // send out data packet...
            putUSBUSART(USB_In_Buffer,lubBytesToSend);

            return(1); // transaction not end!..

        case(2) : // send irregular number of data bytes that will not fit into 64 bytes frame...


            if(lubSendBasicData == 0){

                if(lbTT4PacketizedData == TRUE){

                    lubBytesToSend = 32; // this is the total bytes to be sent out...

                    memcpy((void *)&USB_In_Buffer[0],(const void *)&gubTT4Cfg_DataBuffer[0],lubBytesToSend);

                    // send out data packet...
                    putUSBUSART(USB_In_Buffer,lubBytesToSend);

                    lbTT4PacketizedData = FALSE;

                    return(1); // transaction not end!..

                }
                else{

                    lubBytesToSend = 64; // this is the total bytes to be sent out...

                    memcpy((void *)&USB_In_Buffer[0],(const void *)&gubTT4Cfg_DataBuffer[32 + luwTT4LegacyFrames],lubBytesToSend);

                    // send out data packet...
                    putUSBUSART(USB_In_Buffer,lubBytesToSend);

                    luwTT4LegacyFrames += 64;

                //    if(luwTT4LegacyFrames>=256) lbTT4LegacyCfgData = TRUE;

                    if(luwTT4LegacyFrames>=2688) lbTT4LegacyCfgData = TRUE;

                }


                if(lbTT4LegacyCfgData == TRUE){
                    // this will be sent onetime only..
                    lubSendBasicData = 1;
                }

                return(1); // transaction not end!..

            }
            else{

                if(lubMaxFrameDone==0){

                    luwCtr = 0;
                    do{

                        // retrieve raw datapoint, raw means the datapoint in binary form
                        // with 4bit mark status at the MSB portion..
                        commTT4Cfg_ReadEeprom_RawDataPoint(luwDataPointNumber,&luwDataPointData);

                        _sl4Byte.DwData = (UINT32)luwDataPointData;

                        USB_In_Buffer[luwCtr*2] = (char)_sl4Byte.Byte.B0;
                        ludwSumUp += (char)_sl4Byte.Byte.B0; // accummulate checksum value...

                        USB_In_Buffer[1+(luwCtr*2)] = (char)_sl4Byte.Byte.B1;
                        ludwSumUp += (char)_sl4Byte.Byte.B1; // accummulate checksum value...

                        // increment data point number...
                        // this value will be preserved everytime it enter's in this routine...
                        ++luwDataPointNumber;

                        // increment counter, it was 32 only because the datapoint is an integer format..
                        // if multiplied by 2 then it reaches to 64 bytes size...
                        ++luwCtr;

                    }while(luwCtr<32);

                    lubBytesToSend = 64; // this is the total bytes to be sent out...
                    // send out data packet with fixed 64 bytes frame size...
                    putUSBUSART(USB_In_Buffer,lubBytesToSend);

                    // increment frame
                    // Note: 64 bytes per frame
                    ++luwFrameCtr;

                    if(luwFrameCtr<luwNumberOfFrames) return(1); // transaction not end!..

                    lubMaxFrameDone = 1; // all frames were sent already!... till here!

                    return(1); // transaction still not end!..

                }
                else{

                    if(lubSingleFrame==0){

                        lubSingleFrame=1; //close after sending the last single irregular size frame...

                        luwCtr = 0;
                        do{

                            // retrieve raw datapoint, raw datapoint means is a binary data format
                            // with 4bit mark status at the MSB portion..
                            commTT4Cfg_ReadEeprom_RawDataPoint(luwDataPointNumber,&luwDataPointData);

                            _sl4Byte.DwData = (UINT32)luwDataPointData;

                            USB_In_Buffer[luwCtr*2] = (char)_sl4Byte.Byte.B0;
                            ludwSumUp += (char)_sl4Byte.Byte.B0; // accummulate checksum value...

                            USB_In_Buffer[1+(luwCtr*2)] = (char)_sl4Byte.Byte.B1;
                            ludwSumUp += (char)_sl4Byte.Byte.B1; // accummulate checksum value...

                            // increment data point number...
                            // this value will be preserved everytime it enter's in this routine...
                            ++luwDataPointNumber;

                            // increment counter, it was 32 only because the datapoint is an integer format..
                            // if multiplied by 2 then it reaches to 64 bytes size...
                            ++luwCtr;

                        // note that this is an integer counter then extra bytes must be divided by 2 ...
                        }while(luwCtr<(luwFrameExtraBytes/2));

                        lubBytesToSend = luwFrameExtraBytes; // this is the total bytes to be sent out...
                        // send out data packet with fixed 64 bytes frame size...
                        putUSBUSART(USB_In_Buffer,lubBytesToSend);

                    }
                }
            }


            lubDataPacketStatus = 3; // go to next step..

            return(1); // transaction still not end!..

        case(3) : // send at the lst portion the check

            _sl4Byte.DwData = (UINT32)ludwSumUp;
            lubCheckSumValue = _sl4Byte.Byte.B0; // take only the LSB byte value...
            USB_In_Buffer[0] = (char)lubCheckSumValue;

            lubBytesToSend = 1; // this is the total bytes to be sent out...
            // send out data packet...
            putUSBUSART(USB_In_Buffer,lubBytesToSend);

            break;

    }

    // reset all status flags to zero...
    lubDataPacketStatus = 0;
    lubGetData = 0;
    lubSendBasicData = 0;
    lubMaxFrameDone = 0;
    lubSingleFrame = 0;

    //accummulator and counter must be set to zero..
    luwFrameCtr = 0;
    luwDataPointNumber = 0;
    ludwSumUp = 0;

    // reset TT4 legacy config data....
    luwTT4LegacyFrames = 0;
    lbTT4LegacyCfgData = FALSE;
    lbTT4PacketizedData = TRUE;


    return(0); // data sending transaction finished!

}


/*******************************************************************************
**  Function     : hwUSB_TT4MA_ProcReq_0x33
**  Description  : Get I.D - TT4 command 0x33
**  PreCondition : none
**  Side Effects : none
**  Input        : par_ubParameterValue - parameter value
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void hwUSB_TT4MA_ProcReq_0x33(UINT8 par_ubParameterValue){
    
    register _sDWORDBITDATA _sl4Byte;
    register UINT16 luwTotalBytes;    
    
    // check received parameter if valid..
    if(par_ubParameterValue!=0x00){
         hwUSB_TT4MA_SendACKToHost(TT4_BAD_PARAM);    
         return;
    }     
    
    //------------------------------------------------------------
    // send back command success....
    // this must always at index 0 as command successful ack!
    //------------------------------------------------------------
    USB_In_Buffer[0] = TT4_CMD_SUCCESSFUL; 
    
    // total bytes to send..
    _sl4Byte.DwData = 7; // as required to send out 7 bytes...  
    USB_In_Buffer[1] = _sl4Byte.Byte.B0;   
    USB_In_Buffer[2] = _sl4Byte.Byte.B1;   
    USB_In_Buffer[3] = _sl4Byte.Byte.B2;       
    
    //------------------------------------------------------------
    
    USB_In_Buffer[4] = 0x55; // 
    USB_In_Buffer[5] = 0x55; //
    USB_In_Buffer[6] = 0x55; //
    USB_In_Buffer[7] = 0x55; //
    USB_In_Buffer[8] = 0x55; //
    USB_In_Buffer[9] = 0x55; //
    USB_In_Buffer[10] = 0x55; //
    USB_In_Buffer[11] = 0x0C; // +1 byte CRC

    //------------------------------------------------------------
    // Calculate Checksum...
    //------------------------------------------------------------
    luwTotalBytes = 12; // sum of bytes to calculate checksum...
    _sl4Byte.DwData = hwUSB_TT4MA_CalculateChksum((UINT8 *)&USB_In_Buffer[0],luwTotalBytes);    
    
    // 1 byte checksum value...
    USB_In_Buffer[12] = _sl4Byte.Byte.B0;
    //------------------------------------------------------------    

    luwTotalBytes = 13; // this is the total bytes to be sent out...
    // send out data packet...   
    putUSBUSART(USB_In_Buffer,luwTotalBytes); // plus 1 is the checksume byte value...         

}



/*******************************************************************************
**  Function     : hwUSB_TT4MA_ProcReq_0x37
**  Description  : Skip I.D
**  PreCondition : none
**  Side Effects : none
**  Input        : par_ubParameterValue - parameter value
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void hwUSB_TT4MA_ProcReq_0x37(UINT8 par_ubParameterValue){

    // check received parameter if valid..
    if(par_ubParameterValue!=0x00){
         hwUSB_TT4MA_SendACKToHost(TT4_BAD_PARAM);    
         return;
    }     
    
    // send back command success to host....
    hwUSB_TT4MA_SendACKToHost(TT4_CMD_SUCCESSFUL);       
         
}

/*******************************************************************************
**  Function     : hwUSB_TT4MA_ProcReq_0x38
**  Description  : Terminate Communications -- TT4 Command 0x38
**  PreCondition : none
**  Side Effects : none
**  Input        : par_ubParameterValue - parameter value
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void hwUSB_TT4MA_ProcReq_0x38(UINT8 par_ubParameterValue){
    
    // check received parameter if valid..
    if(par_ubParameterValue!=0x00){
         hwUSB_TT4MA_SendACKToHost(TT4_BAD_PARAM);    
         return;
    }     
    
    // send back command success to host....
    hwUSB_TT4MA_SendACKToHost(TT4_CMD_SUCCESSFUL);
    
}


/*******************************************************************************
**  Function     : hwUSB_TT4MA_ProcReq_0x44
**  Description  : Move Unit Status to Scratchpad - TT4 Command 0x44
**  PreCondition : none
**  Side Effects : none
**  Input        : par_ubParameterValue - parameter value
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void hwUSB_TT4MA_ProcReq_0x44(UINT8 par_ubParameterValue){

    register _sDWORDBITDATA _sl4Byte;

    
    // check received parameter if valid..
    if(par_ubParameterValue!=0x00){
         hwUSB_TT4MA_SendACKToHost(TT4_BAD_PARAM);    
         return;
    }     
    
    //--------------------------------------------
    // load operational data to scratch pad..
    //--------------------------------------------
    
    
    // protocol ID..
    commTT4Cfg_ReadCfgEeprom(OFFSET_PROTOCOL_ID,(UINT8 *)&gubTT4Cfg_EEBuffer[0]);
    _sl4Byte.DwData = 0;
    _sl4Byte.Byte.B0 = gubTT4Cfg_EEBuffer[0];
    _sl4Byte.Byte.B1 = gubTT4Cfg_EEBuffer[1];        
    _gsSensitech_Configuration_Area.uwProtocolID = _sl4Byte.DwData;        
    gubTT4Cfg_ScratchPadMemory[0] = _sl4Byte.Byte.B0;   
    gubTT4Cfg_ScratchPadMemory[1] = _sl4Byte.Byte.B1;   
        
    // firmware version..
    commTT4Cfg_ReadCfgEeprom(OFFSET_FIRMWARE_VER,(UINT8 *)&gubTT4Cfg_EEBuffer[0]);
    _sl4Byte.DwData = 0;
    _sl4Byte.Byte.B0 = gubTT4Cfg_EEBuffer[0];
    _sl4Byte.Byte.B1 = gubTT4Cfg_EEBuffer[1];        
    _gsSensitech_Configuration_Area.uwFirmwareVer = _sl4Byte.DwData;
    gubTT4Cfg_ScratchPadMemory[2] = _sl4Byte.Byte.B0;   
    gubTT4Cfg_ScratchPadMemory[3] = _sl4Byte.Byte.B1;   
    
    // manufacturing serial number...
    commTT4Cfg_ReadCfgEeprom(OFFSET_MANUFACTURING_SERIAL_NUM,(UINT8 *)&gubTT4Cfg_EEBuffer[0]);
    _sl4Byte.DwData = 0;
    _sl4Byte.Byte.B0 = gubTT4Cfg_EEBuffer[0];
    _sl4Byte.Byte.B1 = gubTT4Cfg_EEBuffer[1];        
    _sl4Byte.Byte.B2 = gubTT4Cfg_EEBuffer[2];
    _sl4Byte.Byte.B3 = gubTT4Cfg_EEBuffer[3];        
    _gsSensitech_Configuration_Area.udwManufacturingSerialNumber = _sl4Byte.DwData;
    gubTT4Cfg_ScratchPadMemory[4] = _sl4Byte.Byte.B0;   
    gubTT4Cfg_ScratchPadMemory[5] = _sl4Byte.Byte.B1;       
    gubTT4Cfg_ScratchPadMemory[6] = _sl4Byte.Byte.B2;   
    gubTT4Cfg_ScratchPadMemory[7] = _sl4Byte.Byte.B3;  
     
    // current time..        
    gudwTT4MA_CurrentTime = drvRTC_ReadRTCC_CurrentTime();        
    _sl4Byte.DwData = gudwTT4MA_CurrentTime; 
    gubTT4Cfg_ScratchPadMemory[8] = _sl4Byte.Byte.B0;   
    gubTT4Cfg_ScratchPadMemory[9] = _sl4Byte.Byte.B1;   
    gubTT4Cfg_ScratchPadMemory[10] = _sl4Byte.Byte.B2;   
    gubTT4Cfg_ScratchPadMemory[11] = _sl4Byte.Byte.B3;   
        
    // Last Temperature Measured...
    _sl4Byte.DwData = 0;
    _sl4Byte.DwData = devSensor_GetSensitechBinaryTempData();
    gubTT4Cfg_ScratchPadMemory[12] = _sl4Byte.Byte.B0;   
    gubTT4Cfg_ScratchPadMemory[13] = _sl4Byte.Byte.B1;   

    // command byte.. 
    commTT4Cfg_ReadCfgEeprom(OFFSET_COMMAND_BYTE,(UINT8 *)&gubTT4Cfg_EEBuffer[0]);
    _gsSensitech_Configuration_Area.ubCommandByte = gubTT4Cfg_EEBuffer[0]; // status of monitor; 
    gubTT4Cfg_ScratchPadMemory[14] = gubTT4Cfg_EEBuffer[0];           
        
    // alarm status byte..
    commTT4Cfg_ReadCfgEeprom(OFFSET_ALARM_STATUS,(UINT8 *)&gubTT4Cfg_EEBuffer[0]);
    _gsUser_Configuration_Area.ubAlarmStatus = gubTT4Cfg_EEBuffer[0];
    gubTT4Cfg_ScratchPadMemory[15] = gubTT4Cfg_EEBuffer[0];
    
    // number of strored data points..
    commTT4Cfg_ReadCfgEeprom(OFFSET_NUMBER_OF_STORED_DATAPOINTS,(UINT8 *)&gubTT4Cfg_EEBuffer[0]);
    _sl4Byte.DwData = 0;
    _sl4Byte.Byte.B0 = gubTT4Cfg_EEBuffer[0];
    _sl4Byte.Byte.B1 = gubTT4Cfg_EEBuffer[1];   
    _gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints = _sl4Byte.DwData; 
    gubTT4Cfg_ScratchPadMemory[16] = _sl4Byte.Byte.B0;   
    gubTT4Cfg_ScratchPadMemory[17] = _sl4Byte.Byte.B1;   

    // 14 bytes unused...
    utilArrayVarInitializer((UINT8 *)&gubTT4Cfg_ScratchPadMemory[18],14,0); // 14 bytes, set to 0 value
    
    //------------------------------------------------------------
    // send back command success....
    // this must always at index 0 as command successful ack!
    //------------------------------------------------------------
    hwUSB_TT4MA_SendACKToHost(TT4_CMD_SUCCESSFUL);
    
}

/*******************************************************************************
**  Function     : hwUSB_TT4MA_ProcReq_0x45
**  Description  : Move Unit Status To Scratchpad - TT4 Command 0x45
**  PreCondition : none
**  Side Effects : none
**  Input        : par_ubParameterValue - parameter value
**  Output       : 0 - acquiring data end
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
UINT8 hwUSB_TT4MA_ProcReq_0x45(UINT8 par_ubParameterValue){
  
    register UINT8 lubCtr = 0;
  
    //--------------------------------------------
    // load host data to scratch pad..
    //--------------------------------------------
    lubCtr = 0;
    do{            
        gubTT4Cfg_ScratchPadMemory[lubCtr] = USB_Out_Buffer[lubCtr]; 
        ++lubCtr;
    }while(lubCtr<32);    
    
    hwUSB_TT4MA_SendACKToHost(TT4_CMD_SUCCESSFUL); 
    
    return(0); // acquiring data end...

}

/*******************************************************************************
**  Function     : hwUSB_TT4MA_ProcReq_0x46
**  Description  : Read Scratchpad - TT4 Command 0x46
**  PreCondition : none
**  Side Effects : none
**  Input        : par_ubParameterValue - parameter value
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void hwUSB_TT4MA_ProcReq_0x46(UINT8 par_ubParameterValue){
   
    register UINT16 luwTotalBytes;
    register UINT8 lubCtr;
    
    // check received parameter if valid..
    if(par_ubParameterValue!=0x00){
         hwUSB_TT4MA_SendACKToHost(TT4_BAD_PARAM);    
         return;
    }     
    
    
    //------------------------------------------------------------
    // send back command success....
    // this must always at index 0 as command successful ack!
    //------------------------------------------------------------
    USB_In_Buffer[0] = TT4_CMD_SUCCESSFUL; 
    
    //------------------------------------------------------------ 
    
    //--------------------------------------------
    // load host data to scratch pad..
    //--------------------------------------------
    lubCtr = 0;
    do{            
        USB_In_Buffer[1 + lubCtr] = gubTT4Cfg_ScratchPadMemory[lubCtr]; 
        ++lubCtr;
    }while(lubCtr<32);    

    luwTotalBytes = 33; // this is the total bytes to be sent out...
    // send out data packet...   
    putUSBUSART(USB_In_Buffer,luwTotalBytes); 
    
}



/*******************************************************************************
**  Function     : hwUSB_TT4MA_ProcReq_0x47
**  Description  : Login Sensitech - TT4 Command 0x47
**  PreCondition : none
**  Side Effects : none
**  Input        : par_ubParameterValue - parameter value
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void hwUSB_TT4MA_ProcReq_0x47(UINT8 par_ubParameterValue){

    // not necessary to use in TT4USB-MA as required in doc, just send ack..

    // send back command success to host....
    hwUSB_TT4MA_SendACKToHost(TT4_CMD_SUCCESSFUL);
}

/*******************************************************************************
**  Function     : hwUSB_TT4MA_ProcReq_0x48
**  Description  : Login User - TT4 command 0x48
**  PreCondition : none
**  Side Effects : none
**  Input        : par_ubParameterValue - parameter value
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void hwUSB_TT4MA_ProcReq_0x48(UINT8 par_ubParameterValue){

    // not necessary to use in TT4USB-MA as required in doc, just send ack..

    // send back command success to host....
    hwUSB_TT4MA_SendACKToHost(TT4_CMD_SUCCESSFUL);
}

/*******************************************************************************
**  Function     : hwUSB_TT4MA_ProcReq_0x50
**  Description  : Copy Scratchpad to Sensitech - TT4 Command 0x50
**  PreCondition : none
**  Side Effects : none
**  Input        : par_ubParameterValue - parameter value
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void hwUSB_TT4MA_ProcReq_0x50(UINT8 par_ubParameterValue){

    register UINT16 luwPageNumber;
    register _sWORDBITDATA _slWord;
    
    // get the 2byte parameter value...    
    _slWord.Byte.Lo = USB_Out_Buffer[0];
    _slWord.Byte.Hi = USB_Out_Buffer[1];
        
    // check received parameter if valid..
    if(_slWord.Word>1){
         hwUSB_TT4MA_SendACKToHost(TT4_BAD_PARAM);    
         return;
    }
    
    //---------------------------------------------------------------------
    // load scratch pad data to EEPROM at Sensitech Configuration Page...
    //---------------------------------------------------------------------     
    luwPageNumber = 32 * _slWord.Word; // starting Offset EEPROM 0... mjm
    
    devEEPROM_PageWrite(luwPageNumber, (UINT8 *)&gubTT4Cfg_ScratchPadMemory[0],32); // write 32 bytes to eeprom    
    utilDelay_ms(10);
     
    // send back command success to host....
    hwUSB_TT4MA_SendACKToHost(TT4_CMD_SUCCESSFUL);

}

/*******************************************************************************
**  Function     : hwUSB_TT4MA_ProcReq_0x51
**  Description  : Copy Scratchpad to Data Storage 
**  PreCondition : none
**  Side Effects : none
**  Input        : par_ubParameterValue - parameter value
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void hwUSB_TT4MA_ProcReq_0x51(UINT8 par_ubParameterValue){
    
    register UINT16 luwPageNumber;
    register _sWORDBITDATA _slWord;
    
    // get the 2byte parameter value...    
    _slWord.Byte.Lo = USB_Out_Buffer[0];
    _slWord.Byte.Hi = USB_Out_Buffer[1];
    
    // check received parameter if valid..
    // for 16k EEPROM, its available page range is 0-9999 as specified by TT4MA Communications Specs...mjm
    if(_slWord.Word>1000u){
         hwUSB_TT4MA_SendACKToHost(TT4_BAD_PARAM);    
         return;
    }
    
    //-----------------------------------------------
    // load scratch pad data to any page in EEPROM...
    //-----------------------------------------------     
    luwPageNumber = 32 * _slWord.Word;
    
    devEEPROM_PageWrite(luwPageNumber, (UINT8 *)&gubTT4Cfg_ScratchPadMemory[0],32); // write 32 bytes to eeprom    
    utilDelay_ms(10);
    
    // send back command success to host....
    hwUSB_TT4MA_SendACKToHost(TT4_CMD_SUCCESSFUL);
}
 

/*******************************************************************************
**  Function     : hwUSB_TT4MA_ProcReq_0x53
**  Description  : Reset Unit - TT4 command 0x53
**  PreCondition : none
**  Side Effects : none
**  Input        : par_ubParameterValue - parameter value
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void hwUSB_TT4MA_ProcReq_0x53(UINT8 par_ubParameterValue){
    
    // check received parameter if valid..
    if(par_ubParameterValue!=0x00){
         hwUSB_TT4MA_SendACKToHost(TT4_BAD_PARAM);    
         return;
    }
    
    // not necessary to use in TT4USB-MA as required in doc, just send ack..
    // peter nunes confirmed this issue in phone call..

    // send back command success to host....
    hwUSB_TT4MA_SendACKToHost(TT4_CMD_SUCCESSFUL);
    
    utilDelay_ms(100);
    
    //part of RESET RECOVERY
    _gsSensitech_Configuration_Area.ubCommandByte = CMD_BYTE_UNCONFIG; // 16 - unconfigured mode bit value...
    gubMem = _gsSensitech_Configuration_Area.ubCommandByte;
    commTT4Cfg_FastWriteCfgEeprom(OFFSET_COMMAND_BYTE,(const void *)&gubMem);

    // initiate software reset...
    RCON = 0x00;
    __asm__("reset");                
    
}

/*******************************************************************************
**  Function     : hwUSB_TT4MA_ProcReq_0x60
**  Description  : Copy Scratchpad to User Configuration - TT4 command 0x60
**  PreCondition : none
**  Side Effects : none
**  Input        : par_ubParameterValue - parameter value
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void hwUSB_TT4MA_ProcReq_0x60(UINT8 par_ubParameterValue){

    register UINT16 luwPageNumber;
    register _sWORDBITDATA _slWord;
    
    // get the 2byte parameter value...    
    _slWord.Byte.Lo = USB_Out_Buffer[0];
    _slWord.Byte.Hi = USB_Out_Buffer[1];
        
    // check received parameter if valid..
    if(_slWord.Word>2){
         hwUSB_TT4MA_SendACKToHost(TT4_BAD_PARAM);    
         return;
    }    
    
    //---------------------------------------------------------------------
    // load scratch pad data to EEPROM at User Configuration Page...
    //---------------------------------------------------------------------     
    luwPageNumber = 64 + (32 * _slWord.Word); // user configuration page starts at offset 64...
    
    devEEPROM_PageWrite(luwPageNumber, (UINT8 *)&gubTT4Cfg_ScratchPadMemory[0],32); // write 32 bytes to eeprom    
    utilDelay_ms(10);
    
    // send back command success to host....
    hwUSB_TT4MA_SendACKToHost(TT4_CMD_SUCCESSFUL);
}


/*******************************************************************************
**  Function     : hwUSB_TT4MA_ProcReq_0x61
**  Description  : Copy User Configuration to Scratchpad - TT4 Command 0x61
**  PreCondition : none
**  Side Effects : none
**  Input        : par_ubParameterValue - parameter value
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void hwUSB_TT4MA_ProcReq_0x61(UINT8 par_ubParameterValue){

    register UINT16 luwCtr;
    register UINT16 luwPageNumber;
    UINT8  lubData;
    register _sWORDBITDATA _slWord;
    
    // get the 2byte parameter value...    
    _slWord.Byte.Lo = USB_Out_Buffer[0];
    _slWord.Byte.Hi = USB_Out_Buffer[1];

    // check received parameter if valid..
    if(_slWord.Word>2){
         hwUSB_TT4MA_SendACKToHost(TT4_BAD_PARAM);    
         return;
    }    
    
    //-----------------------------------------------------------
    // copy User Configuration from EEPROM Page to Scratch pad 
    //-----------------------------------------------------------     
    
    luwPageNumber = 64 + (32 * _slWord.Word); // user configuration page starts at offset 0...
    
    luwCtr = 0;
    do{
        devEEPROM_RandomRead(luwPageNumber+luwCtr,&lubData); 
        gubTT4Cfg_ScratchPadMemory[luwCtr] = lubData;        
        
        utilDelay_ms(2);
        
        ++luwCtr;
    }while(luwCtr<32);

    // send back command success to host....
    hwUSB_TT4MA_SendACKToHost(TT4_CMD_SUCCESSFUL);
    
}

/*******************************************************************************
**  Function     : hwUSB_TT4MA_ProcReq_0x62
**  Description  : Copy Scratchpad to User Information - TT4 Command 0x62
**  PreCondition : none
**  Side Effects : none
**  Input        : par_ubParameterValue - parameter value
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void hwUSB_TT4MA_ProcReq_0x62(UINT8 par_ubParameterValue){

    register UINT16 luwPageNumber;
    register _sWORDBITDATA _slWord;
    
    // get the 2byte parameter value...    
    _slWord.Byte.Lo = USB_Out_Buffer[0];
    _slWord.Byte.Hi = USB_Out_Buffer[1];

    // check received parameter if valid..
    if(_slWord.Word>2){
         hwUSB_TT4MA_SendACKToHost(TT4_BAD_PARAM);    
         return;
    }  
        
    //------------------------------------------------------------
    // load scratch pad data to EEPROM at User Information Area...
    //------------------------------------------------------------     
    luwPageNumber = 160 + (32 * _slWord.Word); // user information page offset starts at 160...
    
    devEEPROM_PageWrite(luwPageNumber, (UINT8 *)&gubTT4Cfg_ScratchPadMemory[0],32); // write 32 bytes to eeprom    
    utilDelay_ms(10);

    // send back command success to host....
    hwUSB_TT4MA_SendACKToHost(TT4_CMD_SUCCESSFUL);
}

/*******************************************************************************
**  Function     : hwUSB_TT4MA_ProcReq_0x63
**  Description  : Copy User Information to Scratchpad - TT4 Command 0x63
**  PreCondition : none
**  Side Effects : none
**  Input        : par_ubParameterValue - parameter value
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void hwUSB_TT4MA_ProcReq_0x63(UINT8 par_ubParameterValue){

    register UINT16 luwCtr;
    register UINT16 luwPageNumber;
    UINT8  lubData;
    register _sWORDBITDATA _slWord;
    
    // get the 2byte parameter value...    
    _slWord.Byte.Lo = USB_Out_Buffer[0];
    _slWord.Byte.Hi = USB_Out_Buffer[1];

    // check received parameter if valid..
    if(_slWord.Word>2){
         hwUSB_TT4MA_SendACKToHost(TT4_BAD_PARAM);    
         return;
    }  
    
    //-----------------------------------------------------------
    // copy User Information Area from EEPROM to Scratch pad.... 
    //-----------------------------------------------------------     
    
    luwPageNumber = 160 + (32 * _slWord.Word); // user information page offset starts at 160...
    
    luwCtr = 0;
    do{
        devEEPROM_RandomRead(luwPageNumber+luwCtr,&lubData); 
        gubTT4Cfg_ScratchPadMemory[luwCtr] = lubData;        
        
        utilDelay_ms(2);
        
        ++luwCtr;
    }while(luwCtr<32);

    // send back command success to host....
    hwUSB_TT4MA_SendACKToHost(TT4_CMD_SUCCESSFUL);
    
}



/*******************************************************************************
**  Function     : hwUSB_TT4MA_ProcReq_0x64
**  Description  : Copy Temperature Data Page to Scratchpad - TT4 Command 0x64
**  PreCondition : none
**  Side Effects : none
**  Input        : par_ubParameterValue - parameter value
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void hwUSB_TT4MA_ProcReq_0x64(UINT8 par_ubParameterValue){

    register UINT16 luwCtr;
    register UINT16 luwPageNumber;
    UINT8  lubData;
    register _sWORDBITDATA _slWord;
    
    // get the 2byte parameter value...    
    _slWord.Byte.Lo = USB_Out_Buffer[0];
    _slWord.Byte.Hi = USB_Out_Buffer[1];
    
    // check received parameter if valid..
    // for 16k EEPROM, its available page range is 0-9999 as specified by TT4MA Communications Specs...mjm
    if(_slWord.Word>1000u){
         hwUSB_TT4MA_SendACKToHost(TT4_BAD_PARAM);    
         return;
    }

    //-----------------------------------------------------------
    // copy Temperature Data Page from EEPROM to Scratch pad... 
    //-----------------------------------------------------------     
    
    luwPageNumber = 768u + (32 * _slWord.Word); // 16k temperature data memory area...
    
    luwCtr = 0;
    do{
        devEEPROM_RandomRead(luwPageNumber+luwCtr,&lubData); 
        gubTT4Cfg_ScratchPadMemory[luwCtr] = lubData;       
        
        utilDelay_ms(2);
         
        ++luwCtr;
    }while(luwCtr<32);

    // send back command success to host....
    hwUSB_TT4MA_SendACKToHost(TT4_CMD_SUCCESSFUL);
    
}



/*******************************************************************************
**  Function     : hwUSB_TT4MA_ProcReq_0x65
**  Description  : Set Clock - TT4 Command 0x65
**  PreCondition : none
**  Side Effects : none
**  Input        : par_ubParameterValue - parameter value
**  Output       : 0 - acquiring data end
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
UINT8 hwUSB_TT4MA_ProcReq_0x65(UINT8 par_ubParameterValue){
    
    register _sDWORDBITDATA _sl4Byte;
    
    //--------------------------------------------
    // load host data to scratch pad..
    //--------------------------------------------
    _sl4Byte.Byte.B0 = USB_Out_Buffer[0]; 
    _sl4Byte.Byte.B1 = USB_Out_Buffer[1]; 
    _sl4Byte.Byte.B2 = USB_Out_Buffer[2]; 
    _sl4Byte.Byte.B3 = USB_Out_Buffer[3];   
    
    // validate the time entry....
    // if less then Jan 1, 2000 then set it to this time....
    if(_sl4Byte.DwData <= 946684800ul) _sl4Byte.DwData = 946684800ul;
    
    // update time...
    gudwTT4MA_PCTime = gudwTT4MA_CurrentTime = _sl4Byte.DwData;          

    
    // update RTCC... mjm
    drvRTC_Set_CurrentTimeSecs(_sl4Byte.DwData);           
    
    // response CDC command successful....
    hwUSB_TT4MA_SendACKToHost(TT4_CMD_SUCCESSFUL); 
    
    return(0); // acquiring data end...
        
}



/*******************************************************************************
**  Function     : hwUSB_TT4MA_ProcReq_0x66
**  Description  : Clear Data Memory Pointer - TT4 Command 0x66
**  PreCondition : none 
**  Side Effects : none
**  Input        : par_ubParameterValue - parameter value
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void hwUSB_TT4MA_ProcReq_0x66(UINT8 par_ubParameterValue){
    
    UINT16 luwData;

    // check received parameter if valid..
    if(par_ubParameterValue!=0x00){
         hwUSB_TT4MA_SendACKToHost(TT4_BAD_PARAM);    
         return;
    }
    
    luwData = _gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints = 0;    
    commTT4Cfg_WriteCfgEeprom(OFFSET_NUMBER_OF_STORED_DATAPOINTS,(UINT8 *)&luwData);
    
    // send back command success to host....
    hwUSB_TT4MA_SendACKToHost(TT4_CMD_SUCCESSFUL);
}


/*******************************************************************************
**  Function     : hwUSB_TT4MA_ProcReq_0x67
**  Description  : Initialize Unit - TT4 Command 0x67
**  PreCondition : none
**  Side Effects : none
**  Input        : par_ubParameterValue - parameter value
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void hwUSB_TT4MA_ProcReq_0x67(UINT8 par_ubParameterValue){

       
    // check received parameter if valid..
    if(par_ubParameterValue!=0x00){
         hwUSB_TT4MA_SendACKToHost(TT4_BAD_PARAM);    
         return;    
    }
    
    utilDelay_ms(10);// preventing timing overrun if any previous EEPROM activity launched!...
    
    // load customer number of resets....
    commTT4Cfg_FastReadCfgEeprom(OFFSET_CUSTOMER_RESETS,(void *)&guwMem);    
    _gsSensitech_Configuration_Area.uwNumberOfCustomerResets = guwMem;
    
    
    if(_gsSensitech_Configuration_Area.uwNumberOfCustomerResets == 0){
        hwUSB_TT4MA_SendACKToHost(TT4_BAD_INIT);    
        return;
    }
    
    
    utilDelay_ms(10);// preventing timing overrun if any previous EEPROM activity launched!...
    
    gbTT4USB_Config_OK = TRUE;
    
    // initialized to unconfigured as specified in the doc...    
    _gsSensitech_Configuration_Area.ubCommandByte = CMD_BYTE_SLEEP; // 4 - sleep mode bit value...    
    
    gubMem = _gsSensitech_Configuration_Area.ubCommandByte;
    commTT4Cfg_FastWriteCfgEeprom(OFFSET_COMMAND_BYTE,(const void *)&gubMem);

    // send back command success to host....
    hwUSB_TT4MA_SendACKToHost(TT4_CMD_SUCCESSFUL);

}


/*******************************************************************************
**  Function     : hwUSB_TT4MA_ProcReq_0x68
**  Description  : Start Unit - TT4 Command 0x68
**  PreCondition : none
**  Side Effects : none
**  Input        : par_ubParameterValue - parameter value
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void hwUSB_TT4MA_ProcReq_0x68(UINT8 par_ubParameterValue){

    UINT16 luwData;
    
    // check received parameter if valid..
    if(par_ubParameterValue!=0x00){
         hwUSB_TT4MA_SendACKToHost(TT4_BAD_PARAM);    
         return;
    }
    
    
    switch(_gsSensitech_Configuration_Area.ubCommandByte){
    case(CMD_BYTE_SLEEP) : // unit is sleep!...
    
        gbTT4USB_Config_OK = TRUE;
    
        // initialized to unconfigured as specified in the doc...UNIT Start!
        luwData = _gsSensitech_Configuration_Area.ubCommandByte = CMD_BYTE_RUNNING; // unit running...
        commTT4Cfg_WriteCfgEeprom(OFFSET_COMMAND_BYTE,(UINT8 *)&luwData);        
        // send back command success to host....
        hwUSB_TT4MA_SendACKToHost(TT4_CMD_SUCCESSFUL);
        
    break;
    case(CMD_BYTE_RUNNING) : // unit is running...
    
        gbTT4USB_Config_OK = TRUE;
    
        devSensor_MarkSensitechTempData();
        // send back command success to host....
        hwUSB_TT4MA_SendACKToHost(TT4_CMD_SUCCESSFUL);
        
    break;
    default:
        gbTT4USB_Config_OK = FALSE;
        // invalid data then return back the command!...
        hwUSB_TT4MA_SendACKToHost(_gsSensitech_Configuration_Area.ubCommandByte);        
    }
    
    
}


/*******************************************************************************
**  Function     : hwUSB_TT4MA_ProcReq_0x69
**  Description  : Stop Unit - TT4 Command 0x69
**  PreCondition : none
**  Side Effects : none
**  Input        : par_ubParameterValue - parameter value
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void hwUSB_TT4MA_ProcReq_0x69(UINT8 par_ubParameterValue){

    UINT16 luwData;
    
    // check received parameter if valid..
    if(par_ubParameterValue!=0x00){
         hwUSB_TT4MA_SendACKToHost(TT4_BAD_PARAM);    
         return;
    }
    
    
    if(_gsSensitech_Configuration_Area.ubCommandByte == CMD_BYTE_RUNNING){
    
        // initialized to unconfigured as specified in the doc...UNIT Stop!
        luwData = _gsSensitech_Configuration_Area.ubCommandByte = CMD_BYTE_STOPPED;
        commTT4Cfg_WriteCfgEeprom(OFFSET_COMMAND_BYTE,(UINT8 *)&luwData);
        // send back command success to host....
        hwUSB_TT4MA_SendACKToHost(TT4_CMD_SUCCESSFUL);
    }
    else{
        // send back command success to host....
        hwUSB_TT4MA_SendACKToHost(_gsSensitech_Configuration_Area.ubCommandByte);    
    }
    
}

/*******************************************************************************
**  Function     : hwUSB_TT4MA_ProcReq_0x6A
**  Description  : Copy Sensitech Configuration Page To Scratchpad - TT4 Command 0x6A
**  PreCondition : none
**  Side Effects : none
**  Input        : par_ubParameterValue - parameter value
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void hwUSB_TT4MA_ProcReq_0x6A(UINT8 par_ubParameterValue){

    register UINT16 luwCtr;
    register UINT16 luwPageNumber;
    UINT8  lubData;
    register _sWORDBITDATA _slWord;
    
    // get the 2byte parameter value...    
    _slWord.Byte.Lo = USB_Out_Buffer[0];
    _slWord.Byte.Hi = USB_Out_Buffer[1];
        
    // check received parameter if valid..
    if(_slWord.Word>1){
         hwUSB_TT4MA_SendACKToHost(TT4_BAD_PARAM);    
         return;
    }
    
    utilDelay_ms(10);

    //-----------------------------------------------------------
    // copy Sensitech Configuration from EEPROM Page to Scratch pad 
    //-----------------------------------------------------------         
    luwPageNumber = 32 * _slWord.Word; // sensitech configuration page...
    
    luwCtr = 0;
    do{
        devEEPROM_RandomRead(luwPageNumber+luwCtr,&lubData); 
        gubTT4Cfg_ScratchPadMemory[luwCtr] = lubData;        
        
        utilDelay_ms(2);
        
        ++luwCtr;
    }while(luwCtr<32);
    
    utilDelay_ms(10);

    // send back command success to host....
    hwUSB_TT4MA_SendACKToHost(TT4_CMD_SUCCESSFUL);
    
}


/*******************************************************************************
**  Function     : hwUSB_TT4MA_ProcReq_0x6E
**  Description  : High Speed Data Download - TT4 Command 0x6E
**  PreCondition : none
**  Side Effects : none
**  Input        : par_ubParameterValue - parameter value
**  Output       : 0 - transaction done  
**                 1 - transaction unfinished
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
UINT8 hwUSB_TT4MA_ProcReq_0x6E(UINT8 par_ubParameterValue){

    register _sDWORDBITDATA _sl4Byte;
    register UINT16 luwCtr;
    UINT8  lubData;
    
    static UINT16 luwNumberOfPages = 0;
    static UINT8  lubStartingPage = 0;
    static UINT16 luwPageCtr = 0;
    
    static UINT8 lubGetPageStatus = 0;

    if(lubGetPageStatus == 0){
        
        // close this task after calculation...
        lubGetPageStatus = 1;    
        
        // get the starting page...
        lubStartingPage = (UINT8)USB_Out_Buffer[0]; // note: USB_Out_Buffer[1] as hight byte is always zero as specified in ST comm doc
    
        // get the number of pages...
        _sl4Byte.Byte.B0 = (UINT8)USB_Out_Buffer[2];
        _sl4Byte.Byte.B1 = (UINT8)USB_Out_Buffer[3];
        _sl4Byte.Byte.B2 = 0;
        _sl4Byte.Byte.B3 = 0;
    
        luwNumberOfPages = (UINT16)_sl4Byte.DwData; 
        
        luwPageCtr = 0;   
        
        // after the parameters above then send back ack to host..
        
        // send back command success to host....
        hwUSB_TT4MA_SendACKToHost(TT4_CMD_SUCCESSFUL);
        
        return(1); // task not done yet..
    }
    
    
    luwCtr = 0;
    do{
        devEEPROM_RandomRead(lubStartingPage+luwCtr,&lubData); 
        USB_In_Buffer[luwCtr] = lubData;        
        ++luwCtr;
    }while(luwCtr<32);
    
    // send out data packet...   
    putUSBUSART(USB_In_Buffer,32); // always 32 as per specs as high speed download..
    
    ++luwPageCtr;
    
    lubStartingPage += 32; // increment page by 32 bytes as described in the specs!
    
    if(luwPageCtr < luwNumberOfPages) return(1); // task not done yet..

    // reset vars..   
    lubStartingPage = 0; 
    luwNumberOfPages = 0;
    luwPageCtr = 0;
    lubGetPageStatus = 0;
    
    return(0); // task done!..
    
}

/*******************************************************************************
**  Function     : hwUSB_TT4MA_ProcReq_0x54
**  Description  : Copy Alarm Configuration Page To Scratchpad - TT4 Command 0x54
**  PreCondition : none
**  Side Effects : none
**  Input        : par_ubParameterValue - parameter value
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void hwUSB_TT4MA_ProcReq_0x54(UINT8 par_ubParameterValue){

    register UINT16 luwCtr;
    register UINT16 luwPageNumber;
    UINT8  lubData;
    register _sWORDBITDATA _slWord;
    
    // get the 2byte parameter value...    
    _slWord.Byte.Lo = USB_Out_Buffer[0];
    _slWord.Byte.Hi = USB_Out_Buffer[1];


    // check received parameter if valid..
    if(_slWord.Word>2){
         hwUSB_TT4MA_SendACKToHost(TT4_BAD_PARAM);    
         return;
    }
    
    utilDelay_ms(10);
    
    //-----------------------------------------------------------
    // copy EEPROM Alarm Configuration Page to scratchpad...
    //-----------------------------------------------------------     
        
    luwPageNumber = (32 * 9) + (32 * _slWord.Word); // Alarm configuration page...
    
    luwCtr = 0;
    do{
        devEEPROM_RandomRead(luwPageNumber+luwCtr,&lubData); 
        gubTT4Cfg_ScratchPadMemory[luwCtr] = lubData;        
        
        utilDelay_ms(2);
        
        ++luwCtr;
    }while(luwCtr<32);
    
    utilDelay_ms(10);    

    // send back command success to host....
    hwUSB_TT4MA_SendACKToHost(TT4_CMD_SUCCESSFUL);
        
}


/*******************************************************************************
**  Function     : hwUSB_TT4MA_ProcReq_0x55
**  Description  : Copy Scratchpad Content to Alarm Configuration Page - TT4 Command 0x55
**  PreCondition : none
**  Side Effects : none
**  Input        : par_ubParameterValue - parameter value
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void hwUSB_TT4MA_ProcReq_0x55(UINT8 par_ubParameterValue){

    register UINT16 luwPageNumber;
    register _sWORDBITDATA _slWord;
    
    // get the 2byte parameter value...    
    _slWord.Byte.Lo = USB_Out_Buffer[0];
    _slWord.Byte.Hi = USB_Out_Buffer[1];
    
    // check received parameter if valid..
    if(_slWord.Word>2){
         hwUSB_TT4MA_SendACKToHost(TT4_BAD_PARAM);    
         return;
    }

    //-----------------------------------------------------------
    // copy scratchpad contents to Alarm Configuration Page... 
    //-----------------------------------------------------------     
    luwPageNumber = (32 * 9) + (32*_slWord.Word); // Alarm configuration page...
    
    devEEPROM_PageWrite(luwPageNumber, (UINT8 *)&gubTT4Cfg_ScratchPadMemory[0],32); // write 32 bytes to eeprom    
    
    utilDelay_ms(10);    

    // send back command success to host....
    hwUSB_TT4MA_SendACKToHost(TT4_CMD_SUCCESSFUL);
    
}


/*******************************************************************************
**  Function     : hwUSB_TT4MA_ProcReq_0x56
**  Description  : Copy Password Page To Scratchpad - TT4 Command 0x56 
**  PreCondition : none
**  Side Effects : none
**  Input        : par_ubParameterValue - parameter value
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void hwUSB_TT4MA_ProcReq_0x56(UINT8 par_ubParameterValue){

    register UINT16 luwCtr;
    register UINT16 luwPageNumber;
    UINT8  lubData;
    register _sWORDBITDATA _slWord;
    
    // get the 2byte parameter value...    
    _slWord.Byte.Lo = USB_Out_Buffer[0];
    _slWord.Byte.Hi = USB_Out_Buffer[1];   
   
    // check received parameter if valid..
    if(_slWord.Word!=0x00){
         hwUSB_TT4MA_SendACKToHost(TT4_BAD_PARAM);    
         return;
    }
    
    //-----------------------------------------------------------
    // copy EEPROM Password Page to scratchpad...
    //-----------------------------------------------------------     
    
    luwPageNumber = (32 * 12) + (32 * _slWord.Word); // password page....
    
    luwCtr = 0;
    do{
        devEEPROM_RandomRead(luwPageNumber+luwCtr,&lubData); 
        gubTT4Cfg_ScratchPadMemory[luwCtr] = lubData;        
        ++luwCtr;
    }while(luwCtr<32);

    // send back command success to host....
    hwUSB_TT4MA_SendACKToHost(TT4_CMD_SUCCESSFUL);
    
}


/*******************************************************************************
**  Function     : hwUSB_TT4MA_ProcReq_0x57
**  Description  : Copy Scrathcpad Content to Password Page - TT4 Command 0x57
**  PreCondition : none
**  Side Effects : none
**  Input        : par_ubParameterValue - parameter value
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void hwUSB_TT4MA_ProcReq_0x57(UINT8 par_ubParameterValue){

    register UINT16 luwPageNumber;
    register _sWORDBITDATA _slWord;
    
    // get the 2byte parameter value...    
    _slWord.Byte.Lo = USB_Out_Buffer[0];
    _slWord.Byte.Hi = USB_Out_Buffer[1]; 
      
    // check received parameter if valid..
    if(_slWord.Word!=0x00){
         hwUSB_TT4MA_SendACKToHost(TT4_BAD_PARAM);    
         return;
    }
    
    //---------------------------------------------------
    // copy scratchpad content to EEPROM Password Page...
    //---------------------------------------------------     
    luwPageNumber = (32 * 12) + (32 * _slWord.Word); // password page...
    
    devEEPROM_PageWrite(luwPageNumber, (UINT8 *)&gubTT4Cfg_ScratchPadMemory[0],32); // write 32 bytes to eeprom    
    utilDelay_ms(10);
    
    // send back command success to host....
    hwUSB_TT4MA_SendACKToHost(TT4_CMD_SUCCESSFUL);
    
}


/*******************************************************************************
**  Function     : hwUSB_TT4MA_ProcReq_0x58
**  Description  : Copy 3-point Certificate Page To Scratchpad - TT4 Command 0x58
**  PreCondition : none
**  Side Effects : none
**  Input        : par_ubParameterValue - parameter value
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void hwUSB_TT4MA_ProcReq_0x58(UINT8 par_ubParameterValue){

    register UINT16 luwCtr;
    register UINT16 luwPageNumber;
    UINT8  lubData;
    register _sWORDBITDATA _slWord;
    
    // get the 2byte parameter value...    
    _slWord.Byte.Lo = USB_Out_Buffer[0];
    _slWord.Byte.Hi = USB_Out_Buffer[1];     
    
    // check received parameter if valid..
    if(_slWord.Word>9){
         hwUSB_TT4MA_SendACKToHost(TT4_BAD_PARAM);    
         return;
    }
    
    //-----------------------------------------------------------
    //  copy 3 Point Certificate Area from EEPROM to scratchpad..
    //-----------------------------------------------------------     
    
    luwPageNumber = (32 * 14) + (32 * _slWord.Word); // 3 point certificate area...
    
    luwCtr = 0;
    do{
        devEEPROM_RandomRead(luwPageNumber+luwCtr,&lubData); 
        gubTT4Cfg_ScratchPadMemory[luwCtr] = lubData;        
        ++luwCtr;
    }while(luwCtr<32);

    // send back command success to host....
    hwUSB_TT4MA_SendACKToHost(TT4_CMD_SUCCESSFUL);
    
}

/*******************************************************************************
**  Function     : hwUSB_TT4MA_ProcReq_0x59
**  Description  : Copy Scratchpad Content to 3-Point Certificate Page - TT4 Command 0x59
**  PreCondition : none
**  Side Effects : none
**  Input        : par_ubParameterValue - parameter value
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void hwUSB_TT4MA_ProcReq_0x59(UINT8 par_ubParameterValue){

    register UINT16 luwPageNumber;
    register _sWORDBITDATA _slWord;
    
    // get the 2byte parameter value...    
    _slWord.Byte.Lo = USB_Out_Buffer[0];
    _slWord.Byte.Hi = USB_Out_Buffer[1]; 
        
    // check received parameter if valid..
    if(_slWord.Word>9){
         hwUSB_TT4MA_SendACKToHost(TT4_BAD_PARAM);    
         return;
    }
    
    //-------------------------------------------------------
    // Copy scratchpad content to EEPROM 3 point certificate.
    //-------------------------------------------------------     
    luwPageNumber = (32 * 14) + (32 * _slWord.Word); // 3 point certificate...
    
    devEEPROM_PageWrite(luwPageNumber, (UINT8 *)&gubTT4Cfg_ScratchPadMemory[0],32); // write 32 bytes to eeprom    
    utilDelay_ms(10);    
    
    // send back command success to host....
    hwUSB_TT4MA_SendACKToHost(TT4_CMD_SUCCESSFUL);
    
}


/*******************************************************************************
**  Function     : hwUSB_TT4MA_ProcReq_0x72
**  Description  : Read 1 Byte from RAM Memory Location - TT4 Command 0x72
**  PreCondition : none
**  Side Effects : none
**  Input        : par_ubParameterValue - parameter value
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void hwUSB_TT4MA_ProcReq_0x72(UINT8 par_ubParameterValue){

    // valid only for TT4 OKI...
    
    // send back command success to host....
    hwUSB_TT4MA_SendACKToHost(TT4_CMD_SUCCESSFUL);
}


/*******************************************************************************
**  Function     : hwUSB_TT4MA_ProcReq_0x73
**  Description  : Write 1 Byte To RAM Memory Location - TT4 Command 0x73
**  PreCondition : none
**  Side Effects : none
**  Input        : par_ubParameterValue - parameter value
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void hwUSB_TT4MA_ProcReq_0x73(UINT8 par_ubParameterValue){

    // valid only for TT4 OKI...
    
    // send back command success to host....
    hwUSB_TT4MA_SendACKToHost(TT4_CMD_SUCCESSFUL);
}


/*******************************************************************************
**  Function     : hwUSB_TT4MA_ProcReq_0x74
**  Description  : EEProm Read Command - TT4 Command 0x74
**  PreCondition : none
**  Side Effects : none
**  Input        : par_ubParameterValue - parameter value
**  Output       : 0 - transaction done  
**                 1 - transaction unfinished
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
UINT8 hwUSB_TT4MA_ProcReq_0x74(UINT8 par_ubParameterValue){    

    // working status level..
    static UINT8 lubGetData = 0;
    static UINT8 lubDataPacketStatus = 0;
    
    // checksum storage..
    static UINT16 luwTotalBytes = 0; // excluding checksum...   
    static UINT32 ludwSumUp = 0; // open checksum accummulator...
    static UINT8 lubCheckSumValue = 0;
    
    // datapoint framing and extra byte..
    static UINT16 luwFrameCtr = 0;
    static UINT16 luwFrameExtraBytes = 0;
    static UINT16 luwNumberOfFrames = 0;
    static UINT16 luwDataPointNumber = 0;
    
    // status entry flags...
    static UINT8 lubSendBasicData = 0;
    static UINT8 lubMaxFrameDone = 0;
    static UINT8 lubSingleFrame = 0;
    static UINT16 luwStartingAddress = 0;
    
    
    register _sDWORDBITDATA _sl4Byte;
    register UINT16 luwCtr;
    UINT8  lubData;
    register UINT8  lubBytesToSend;


    //////////////////////////////////////////////////////////
    //  process comamnd here first before send ackowledge!.. 
    //////////////////////////////////////////////////////////    

    // get 1st the main data header information..
    if(lubGetData == 0){        
        
        // these are the parameters were sent from the host!
        _sl4Byte.DwData = 0;
        // _sl4Byte.Word.Lo : EEPROM starting address...
        _sl4Byte.Byte.B0 = USB_Out_Buffer[0];
        _sl4Byte.Byte.B1 = USB_Out_Buffer[1];
        // _sl4Byte.Word.Hi : number of bytes to read...
        _sl4Byte.Byte.B2 = USB_Out_Buffer[2];
        _sl4Byte.Byte.B3 = USB_Out_Buffer[3];
        
        // since datapoint is an integer value then should be multiplied by 2... 
        luwTotalBytes = _sl4Byte.Word.Hi;        
        luwStartingAddress = _sl4Byte.Word.Lo;
        
        // initialize checksum accumulator ...
        ludwSumUp = 0;
    
        //------------------------------------------------------------
        // Calculate Datapoint total frames and extra bytes...
        //------------------------------------------------------------                
        luwNumberOfFrames =  luwTotalBytes / 64; // max data payload frame size is 64 bytes..
        //check the number frame status..
        if(luwNumberOfFrames==0)
            lubMaxFrameDone=1;
        else
            lubMaxFrameDone=0;
        
        luwFrameExtraBytes = luwTotalBytes % 64; // take the extra bytes if any!        
        // check the extra byte status..
        if(luwFrameExtraBytes==0) 
            lubSingleFrame = 1; // no! extra bytes...
        else 
            lubSingleFrame = 0; // have extra bytes...    

            
        lubGetData = 1;// finished acquiring data from TT4 device..
        
        lubDataPacketStatus = 0; // reset here the starting at level 0...
        luwFrameCtr = 0; // reset the frame counter for sure!..
    }    


    // start sending data to Host..    

    switch(lubDataPacketStatus){
        
        case(0) : // send successful command status...
        
            lubDataPacketStatus = 1; // go to next step..
            
            // send back command success to host....
            hwUSB_TT4MA_SendACKToHost(TT4_CMD_SUCCESSFUL);
        
            return(1); // transaction not end!..
        
        case(1) : // send first the 3 bytes info as total bytes to send...
            
            lubDataPacketStatus = 2; // go to next step..
            
            _sl4Byte.DwData = luwTotalBytes; 
            USB_In_Buffer[0] = _sl4Byte.Byte.B0; // send LSB first
            USB_In_Buffer[1] = _sl4Byte.Byte.B1;
            USB_In_Buffer[2] = _sl4Byte.Byte.B2; // send MSB last          
        
            lubBytesToSend = 3; // this is the total bytes to be sent out...
            // send out data packet...   
            putUSBUSART(USB_In_Buffer,lubBytesToSend); 
        
            return(1); // transaction not end!..
        
        case(2) : // send irregular number of data bytes that will not fit into 64 bytes frame...            
                        
            if(lubMaxFrameDone==0){
                
                luwCtr = 0;
                do{   
                    
                    devEEPROM_RandomRead(luwStartingAddress+luwCtr,&lubData); 
                    USB_In_Buffer[luwCtr] = lubData;        
                    
                    ludwSumUp += lubData;
                    
                    ++luwCtr;
                                        
                }while(luwCtr<64);                       
                
                lubBytesToSend = 64; // this is the total bytes to be sent out...
                // send out data packet with fixed 64 bytes frame size...   
                putUSBUSART(USB_In_Buffer,lubBytesToSend); 
            
                // increment frame
                // Note: 64 bytes per frame
                ++luwFrameCtr;           
                
                luwStartingAddress += 64; 
                    
                if(luwFrameCtr<luwNumberOfFrames) return(1); // transaction not end!..                
                    
                lubMaxFrameDone = 1; // all frames were sent already!... till here!
                    
                return(1); // transaction still not end!..                
                            
            }
            else{
                            
                if(lubSingleFrame==0){
                   
                    lubSingleFrame=1; //close after sending the last single irregular size frame...

                    luwCtr = 0;
                    do{   
                    
                        devEEPROM_RandomRead(luwStartingAddress+luwCtr,&lubData); 
                        USB_In_Buffer[luwCtr] = lubData;        
                    
                        ludwSumUp += lubData;
                    
                        ++luwCtr;
                                        
                    }while(luwCtr<luwFrameExtraBytes);                       
                
                    lubBytesToSend = luwFrameExtraBytes; // this is the total bytes to be sent out...
                    // send out data packet with fixed 64 bytes frame size...   
                    putUSBUSART(USB_In_Buffer,lubBytesToSend); 
                        
                }
            }
            
            lubDataPacketStatus = 3; // go to next step..
            
            return(1); // transaction still not end!..
            
        case(3) : // send at the lst portion the check
            
            _sl4Byte.DwData = (UINT32)ludwSumUp;
            lubCheckSumValue = _sl4Byte.Byte.B0; // take only the LSB byte value...
            USB_In_Buffer[0] = (char)lubCheckSumValue;
            
            lubBytesToSend = 1; // this is the total bytes to be sent out...
            // send out data packet...   
            putUSBUSART(USB_In_Buffer,lubBytesToSend); 
        
            break;
            
    }

    // reset all status flags to zero...
    lubDataPacketStatus = 0; 
    lubGetData = 0;
    lubSendBasicData = 0;
    lubMaxFrameDone = 0;
    lubSingleFrame = 0;
    
    //accummulator and counter must be set to zero..
    luwFrameCtr = 0;
    luwDataPointNumber = 0;    
    ludwSumUp = 0;
    
    return(0); // data sending transaction finished!
  
}




/*******************************************************************************
**  Function     : hwUSB_TT4MA_ProcReq_0x75
**  Description  : EEProm Write Command - TT4 Command 0x75
**  PreCondition : none
**  Side Effects : none
**  Input        : par_ubParameterValue - parameter value
**  Output       : 0 - transaction done  
**                 1 - transaction unfinished
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
UINT8 hwUSB_TT4MA_ProcReq_0x75(UINT8 par_ubParameterValue){

    static UINT8 lubFetchHostData = 0;
    
    _sDWORDBITDATA _sl4Byte;
    static UINT16 luwStartingAddress = 0;
    static UINT16 lubNumberOfBytes = 0; // 1 - 32 only...
    
    if(lubFetchHostData == 0){

        _sl4Byte.DwData = 0;
        _sl4Byte.Byte.B0 = USB_Out_Buffer[0];
        _sl4Byte.Byte.B1 = USB_Out_Buffer[1];
        luwStartingAddress = _sl4Byte.Word.Lo;
        
        lubNumberOfBytes = USB_Out_Buffer[2];
        
        // note: must be not be greater than 32 bytes length...
        if(lubNumberOfBytes>32){
            // send back command success to host....
            hwUSB_TT4MA_SendACKToHost(TT4_BAD_PARAM);
            return(0);
        }    

        lubFetchHostData = 1;
    }           

    utilDelay_ms(10);        
    devEEPROM_PageWrite(luwStartingAddress,(UINT8 *)&USB_Out_Buffer[3],lubNumberOfBytes); // max 64 bytes to write     
    utilDelay_ms(10);

    // send back command success to host....
    hwUSB_TT4MA_SendACKToHost(TT4_CMD_SUCCESSFUL);
    
    lubFetchHostData = 0;
    
    return(0); // acquiring data end...
    
}


/*******************************************************************************
**  Function     : hwUSB_TT4MA_ProcReq_0x76
**  Description  : Get Monitor Information Command - TT4 Command 0x76
**  PreCondition : none
**  Side Effects : none
**  Input        : par_ubParameterValue - parameter value
**  Output       : 0 - transaction done  
**                 1 - transaction unfinished
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
UINT8 hwUSB_TT4MA_ProcReq_0x76(UINT8 par_ubParameterValue){

    register _sDWORDBITDATA _sl4Byte;
    register UINT16 luwTotalBytes;    
    register UINT16 luwCtr;
    register UINT16 luwPageNumber;
    UINT8  lubData;
    register UINT8  lubCheckSumValue;
    
    static UINT8 lubGetDataOk = 0;
    static UINT8 lub1stFrame = 0;
    
    
    // check received parameter if valid..
    if(par_ubParameterValue!=0x00){
         hwUSB_TT4MA_SendACKToHost(TT4_BAD_PARAM);    
         return(0);
    }     
    
    if(lubGetDataOk==0){
        
        lubGetDataOk = 1;
        
        //------------------------------------------------------------
        // send back command success....
        // this must always at index 0 as command successful ack!
        //------------------------------------------------------------
        gubTT4Cfg_DataBuffer[0] = TT4_CMD_SUCCESSFUL; 
    
        // total bytes to send..
        _sl4Byte.DwData = 61; // 61 bytes t obe send out per spec requirement! 
        gubTT4Cfg_DataBuffer[1] = _sl4Byte.Byte.B0;   
        gubTT4Cfg_DataBuffer[2] = _sl4Byte.Byte.B1;   
        gubTT4Cfg_DataBuffer[3] = _sl4Byte.Byte.B2;       
    
        //------------------------------------------------------------
    
        commTT4Cfg_ReadCfgEeprom(OFFSET_PROTOCOL_ID,(UINT8 *)&gubTT4Cfg_EEBuffer[0]);
        _sl4Byte.DwData = 0;
        _sl4Byte.Byte.B0 = gubTT4Cfg_EEBuffer[0]; 
        _sl4Byte.Byte.B1 = gubTT4Cfg_EEBuffer[1]; 
        _gsSensitech_Configuration_Area.uwProtocolID = (UINT16)_sl4Byte.DwData;
        gubTT4Cfg_DataBuffer[4] = gubTT4Cfg_EEBuffer[0]; 
        gubTT4Cfg_DataBuffer[5] = gubTT4Cfg_EEBuffer[1]; 
      
        commTT4Cfg_ReadCfgEeprom(OFFSET_FIRMWARE_VER,(UINT8 *)&gubTT4Cfg_EEBuffer[0]);
        _sl4Byte.DwData = 0;
        _sl4Byte.Byte.B0 = gubTT4Cfg_EEBuffer[0]; 
        _sl4Byte.Byte.B1 = gubTT4Cfg_EEBuffer[1]; 
        _gsSensitech_Configuration_Area.uwFirmwareVer = _sl4Byte.DwData;
        gubTT4Cfg_DataBuffer[6] = gubTT4Cfg_EEBuffer[0];
        gubTT4Cfg_DataBuffer[7] = gubTT4Cfg_EEBuffer[1];
    
        commTT4Cfg_ReadCfgEeprom(OFFSET_MANUFACTURING_SERIAL_NUM,(UINT8 *)&gubTT4Cfg_EEBuffer[0]);
        _sl4Byte.DwData = 0;
        _sl4Byte.Byte.B0 = gubTT4Cfg_EEBuffer[0]; 
        _sl4Byte.Byte.B1 = gubTT4Cfg_EEBuffer[1]; 
        _sl4Byte.Byte.B2 = gubTT4Cfg_EEBuffer[2]; 
        _sl4Byte.Byte.B3 = gubTT4Cfg_EEBuffer[3]; 
        _gsSensitech_Configuration_Area.udwManufacturingSerialNumber = _sl4Byte.DwData;
        gubTT4Cfg_DataBuffer[8] = gubTT4Cfg_EEBuffer[0];
        gubTT4Cfg_DataBuffer[9] = gubTT4Cfg_EEBuffer[1];
        gubTT4Cfg_DataBuffer[10] = gubTT4Cfg_EEBuffer[2];
        gubTT4Cfg_DataBuffer[11] = gubTT4Cfg_EEBuffer[3];
     
        commTT4Cfg_ReadCfgEeprom(OFFSET_TRIP_NUMBER,(UINT8 *)&gubTT4Cfg_EEBuffer[0]);
        _sl4Byte.DwData = 0;
        _sl4Byte.Byte.B0 = gubTT4Cfg_EEBuffer[0]; 
        _sl4Byte.Byte.B1 = gubTT4Cfg_EEBuffer[1]; 
        _sl4Byte.Byte.B2 = gubTT4Cfg_EEBuffer[2]; 
        _sl4Byte.Byte.B3 = gubTT4Cfg_EEBuffer[3]; 
        _gsUser_Configuration_Area.udwTripNumber = _sl4Byte.DwData;
        gubTT4Cfg_DataBuffer[12] = gubTT4Cfg_EEBuffer[0];
        gubTT4Cfg_DataBuffer[13] = gubTT4Cfg_EEBuffer[1];
        gubTT4Cfg_DataBuffer[14] = gubTT4Cfg_EEBuffer[2];
        gubTT4Cfg_DataBuffer[15] = gubTT4Cfg_EEBuffer[3];
    
        commTT4Cfg_ReadCfgEeprom(OFFSET_EEPROM_SIZE,(UINT8 *)&gubTT4Cfg_EEBuffer[0]);
        _gsSensitech_Configuration_Area.ubSizeOfEEProm = gubTT4Cfg_EEBuffer[0];
        gubTT4Cfg_DataBuffer[16] = gubTT4Cfg_EEBuffer[0];
    
        commTT4Cfg_ReadCfgEeprom(OFFSET_COMMAND_BYTE,(UINT8 *)&gubTT4Cfg_EEBuffer[0]);
        _gsSensitech_Configuration_Area.ubCommandByte = gubTT4Cfg_EEBuffer[0];
        gubTT4Cfg_DataBuffer[17] = gubTT4Cfg_EEBuffer[0];

        commTT4Cfg_ReadCfgEeprom(OFFSET_CUSTOMER_RESETS,(UINT8 *)&gubTT4Cfg_EEBuffer[0]);
        _sl4Byte.DwData = 0;
        _sl4Byte.Byte.B0 = gubTT4Cfg_EEBuffer[0]; 
        _sl4Byte.Byte.B1 = gubTT4Cfg_EEBuffer[1]; 
        _gsSensitech_Configuration_Area.uwNumberOfCustomerResets = _sl4Byte.DwData;    
        gubTT4Cfg_DataBuffer[18] = gubTT4Cfg_EEBuffer[0];
        gubTT4Cfg_DataBuffer[19] = gubTT4Cfg_EEBuffer[1];
        
        commTT4Cfg_ReadCfgEeprom(OFFSET_MEASUREMENT_INTERVAL,(UINT8 *)&gubTT4Cfg_EEBuffer[0]);
        _sl4Byte.DwData = 0;
        _sl4Byte.Byte.B0 = gubTT4Cfg_EEBuffer[0]; 
        _sl4Byte.Byte.B1 = gubTT4Cfg_EEBuffer[1]; 
        _sl4Byte.Byte.B2 = gubTT4Cfg_EEBuffer[2]; 
        _gsUser_Configuration_Area.udwMeasurementInterval = _sl4Byte.DwData;    
        gubTT4Cfg_DataBuffer[20] = gubTT4Cfg_EEBuffer[0];
        gubTT4Cfg_DataBuffer[21] = gubTT4Cfg_EEBuffer[1];
        gubTT4Cfg_DataBuffer[22] = gubTT4Cfg_EEBuffer[2];    
        
        commTT4Cfg_ReadCfgEeprom(OFFSET_STARTUP_DELAY,(UINT8 *)&gubTT4Cfg_EEBuffer[0]);
        _sl4Byte.DwData = 0;
        _sl4Byte.Byte.B0 = gubTT4Cfg_EEBuffer[0]; 
        _sl4Byte.Byte.B1 = gubTT4Cfg_EEBuffer[1]; 
        _sl4Byte.Byte.B2 = gubTT4Cfg_EEBuffer[2]; 
        _gsUser_Configuration_Area.udwStartUpDelay = _sl4Byte.DwData;    
        gubTT4Cfg_DataBuffer[23] = gubTT4Cfg_EEBuffer[0];
        gubTT4Cfg_DataBuffer[24] = gubTT4Cfg_EEBuffer[1];
        gubTT4Cfg_DataBuffer[25] = gubTT4Cfg_EEBuffer[2];    

        commTT4Cfg_ReadCfgEeprom(OFFSET_NUMBER_OF_STORED_DATAPOINTS,(UINT8 *)&gubTT4Cfg_EEBuffer[0]);
        _sl4Byte.DwData = 0;
        _sl4Byte.Byte.B0 = gubTT4Cfg_EEBuffer[0]; 
        _sl4Byte.Byte.B1 = gubTT4Cfg_EEBuffer[1]; 
        _gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints = _sl4Byte.DwData;    
        gubTT4Cfg_DataBuffer[26] = gubTT4Cfg_EEBuffer[0];
        gubTT4Cfg_DataBuffer[27] = gubTT4Cfg_EEBuffer[1];
    
        commTT4Cfg_ReadCfgEeprom(OFFSET_ALARM_STATUS,(UINT8 *)&gubTT4Cfg_EEBuffer[0]);
        _gsUser_Configuration_Area.ubAlarmStatus = gubTT4Cfg_EEBuffer[0];        
        gubTT4Cfg_DataBuffer[28] = gubTT4Cfg_EEBuffer[0];
        
        luwPageNumber = 32 * 12; // password pages
        luwCtr = 0;
        do{
            devEEPROM_RandomRead(luwPageNumber+luwCtr,&lubData); 
            gubTT4Cfg_DataBuffer[29 + luwCtr] = lubData;        
            ++luwCtr;
        }while(luwCtr<32);
    
        commTT4Cfg_ReadCfgEeprom(OFFSET_TIMESTAMP_CONFIG_TIME,(UINT8 *)&gubTT4Cfg_EEBuffer[0]);
        gubTT4Cfg_DataBuffer[61] = gubTT4Cfg_EEBuffer[0];
        gubTT4Cfg_DataBuffer[62] = gubTT4Cfg_EEBuffer[1];
        gubTT4Cfg_DataBuffer[63] = gubTT4Cfg_EEBuffer[2];
        gubTT4Cfg_DataBuffer[64] = gubTT4Cfg_EEBuffer[3];

        //------------------------------------------------------------
        // Calculate Checksum...
        //------------------------------------------------------------
        luwTotalBytes = 65; // sum of bytes to calculate checksum...
        lubCheckSumValue = hwUSB_TT4MA_CalculateChksum((UINT8 *)&gubTT4Cfg_DataBuffer[0],luwTotalBytes);    
    
        // 1 byte checksum value...
        gubTT4Cfg_DataBuffer[65] = lubCheckSumValue;
        //------------------------------------------------------------    

    }    

    
    if(lub1stFrame == 0){
        
        lub1stFrame = 1;

        luwTotalBytes = 32; // send 1st half, 32 bytes...
        
        memcpy((void *)&USB_In_Buffer[0],(const void *)&gubTT4Cfg_DataBuffer[0],luwTotalBytes);
        
        
        Nop();
        
        // send out data packet...   
        putUSBUSART(USB_In_Buffer,luwTotalBytes); // plus 1 is the checksume byte value... 
        
        return(1); // send 1st group .... 
    }
    else{

        luwTotalBytes = 66 - 32; // send the 2nd half, 34 bytes...
        
        memcpy((void *)&USB_In_Buffer[0],(const void *)&gubTT4Cfg_DataBuffer[32],luwTotalBytes);
        
        // send out data packet...   
        putUSBUSART(USB_In_Buffer,luwTotalBytes); // plus 1 is the checksume byte value... 
    
    }
    
    
    // reset status flags..
    lub1stFrame = 0;
    lubGetDataOk = 0;

    return(0);    
    
}



/*******************************************************************************
**  Function     : hwUSB_TT4MA_Comm_Login
**  Description  : TT4 login processor
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : 0 - HOST_NOT_YET_LOGIN
**                 1 - HOST_LOGIN_SUCCESSFUL
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
UINT8 hwUSB_TT4MA_Comm_Login(void){
    
    register UINT8   lubHdskStatus = 0;
    register UINT8   lubNumBytesRead;
    register UINT8   lubTotalBytesToSend;
    
    static UINT8   lubLoginByte[4] = {0,0,0,0};
    static UINT8 lubLoginByteCtr = 0;
    
    _sDWORDBITDATA ls4ByteData;
    static UINT8 lubCommStatusLevel = HDSK_ACCESS_LEVEL1;

    lubNumBytesRead = getsUSBUSART(USB_Out_Buffer,64);        
    
    // the buffer must not empty...at least greater the 1!
    if(lubNumBytesRead==0) return(0);
    
    switch(lubCommStatusLevel){
    case(HDSK_ACCESS_LEVEL1):  // check handshake...
        // should receive 0x00 from Host PC... mjm
        if(USB_Out_Buffer[0] == HDSK_RCV_OK){
            // send Protocol ID....
            USB_In_Buffer[0] = (UINT8)(HDSK_SEND_PROTOCOL_ID & 0x00FF); // 1st send low byte
            USB_In_Buffer[1] = (UINT8)(HDSK_SEND_PROTOCOL_ID >> 8);     // 2nd send high byte
            lubTotalBytesToSend = 2;
            putUSBUSART(USB_In_Buffer,lubTotalBytesToSend);
            lubCommStatusLevel = HDSK_ACCESS_LEVEL2; // set to the next level requirement...
        }

        lubHdskStatus = HOST_NOT_YET_LOGIN; // not yet complete...

        break;
        
    case(HDSK_ACCESS_LEVEL2):  // check protocol ack...
        // should receive 0x55 acknowledgement from Host PC... mjm
        if(USB_Out_Buffer[0] == HDSK_RCV_ACK){
            // send receive successfull to host....
            USB_In_Buffer[0] = TT4_CMD_SUCCESSFUL;
            lubTotalBytesToSend = 1;
            putUSBUSART(USB_In_Buffer,lubTotalBytesToSend);
            lubCommStatusLevel = HDSK_ACCESS_LEVEL3; // set to the next level requirement...            
            lubLoginByteCtr = 0; // reset Login byte counter
            lubLoginByte[0] = 0;
            lubLoginByte[1] = 0;
            lubLoginByte[2] = 0;
            lubLoginByte[3] = 0;            
        }
        else{
            // back to Level 0...
            lubCommStatusLevel = HDSK_ACCESS_LEVEL1; // set to the next level requirement...            
        }
           
        lubHdskStatus = HOST_NOT_YET_LOGIN; // not yet complete... 
        
        break;
        
    case(HDSK_ACCESS_LEVEL3):  // check tt4-ma login value...    
        
        lubLoginByte[lubLoginByteCtr] = (UINT8)USB_Out_Buffer[0];
        
        ++lubLoginByteCtr;
        
        if(lubLoginByteCtr<4){
            lubHdskStatus = HOST_NOT_YET_LOGIN; // not yet complete... 
            lubCommStatusLevel = HDSK_ACCESS_LEVEL3; // make it sure to set at this level..
            break;
        }     
        
        ls4ByteData.Byte.B3 = lubLoginByte[0]; // 7Ah
        ls4ByteData.Byte.B2 = lubLoginByte[1]; // 5Eh
        ls4ByteData.Byte.B1 = lubLoginByte[2]; // 8Ah
        ls4ByteData.Byte.B0 = lubLoginByte[3]; // A1h

        
        // get 4bytes login value from Host PC... mjm
        if(ls4ByteData.DwData == HDSK_RCV_4B_LOGIN_VAL){
            // send receive successfull to host....
            USB_In_Buffer[0] = TT4_CMD_SUCCESSFUL;
            lubTotalBytesToSend = 1;
            putUSBUSART(USB_In_Buffer,lubTotalBytesToSend);
            lubCommStatusLevel = HDSK_ACCESS_LEVEL1; //3; // success HDSK...
            lubHdskStatus = HOST_LOGIN_SUCCESSFUL; // ok...
            lubLoginByteCtr = 0;
            lubLoginByte[0] = 0;
            lubLoginByte[1] = 0;
            lubLoginByte[2] = 0;
            lubLoginByte[3] = 0;
        }
        else{
            // back to Level 1...
            lubLoginByteCtr = 0;
            lubLoginByte[0] = 0;
            lubLoginByte[1] = 0;
            lubLoginByte[2] = 0;
            lubLoginByte[3] = 0;            
            lubCommStatusLevel = HDSK_ACCESS_LEVEL1; // set to the next level requirement...
        }       
        
        
        
        break;        
    }
    
    return(lubHdskStatus);

}

/*  End Of File  */



