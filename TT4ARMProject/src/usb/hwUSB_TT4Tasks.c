/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm                                            **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc. and Blue Ocean Innovation Ltd.                            **
 **                                                                           **
 **  Copryright(c) 2010. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Blue Ocean Innovation Ltd.                        **
 **  Project#            :  449                                               **
 **  Creation date       :  08/20/2010                                        **
 **  Programmer          :  Michael J. Mariveles                              **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : hwUSB_TT4Task.c                                                   **
 **                                                                           **
 **  Description :                                                            **
 **    - USB core to handle the PC and device high level interface.           **
 **    - MSD and CDC routines will be launched here for USB transactions.     **
 **    - PDF and TTV file writter will be launched here.                      **
 **    - Multi-drop temperature recording secondary core loop.                **
 **                                                                           **
 ******************************************************************************/
 
#include "hwPIC24F.h"
#include "hwSystem.h"
#include "USB.h"
#include "drvExternalSpiFlash.h"
#include "drvFileSystemCfg.h"
#include "usb_function_msd.h"
#include "usb_function_cdc.h"
#include "hwUSB_TT4Tasks.h"
#include "devSPIFLASH_W25Q80.h"
#include "devI2CEEPROM_24AA256.h"
#include "commTT4Config.h"
#include "devI2CTempSensor_TMP112.h"
#include "utility.h"
#include "appTTV.h"
#include "appPDF.h"
#include "appCDC.h"
#include "drvUSB.h"
#include "drvRTC.h"
#include "devI2CLCD_BU9796FS.h"
#include "appTT4_MA.h"
#include "diskio.h"
#include "ff.h"


static void InitializeSystem(void);
// note: USB shutdown should be called when it return back to Core Main Loop... mjm 073111
void USBDevice_SetPortShutdownStatus(void); 

// Timer Interrupt assinged to TMR1...
extern void __attribute__ ((interrupt, auto_psv)) _T1Interrupt (void); 
// Timer Interrupt Toggle Flag...
extern volatile UINT8 gubTimerEventFlag;
// RAM Memory Current Time global storage...
extern volatile UINT32 gudwTT4MA_CurrentTime;


// tt4 communication login status global var
extern UINT8 gubHdSkOkLock;

// EEPROM configuration control structure register...
extern __SENSITECH_CONFIGURATION_AREA   _gsSensitech_Configuration_Area;  
extern __USER_CONFIGURATION_AREA        _gsUser_Configuration_Area;
extern __sBITS_GENCFGOPT_BYTE           _gsGenCfgOptByte;
// EEPROM control bit manipulator structure register...
extern __sBITS_EXTOPT_BYTE    _gsExtdOptByte;


// TTV/PDF file write flag... mjm
UINT8 gubStartDeviceStatusFlag __attribute__((persistent));     //set as persistent for reset recovery
// PDF and TTV file generation status...
BOOL gbPDFFileResetStatusFlag;
BOOL gbTTVFileResetStatusFlag; 
BOOL gbUSBFileGenSuccessStatusFlag;

// PDF file generation success status flag...
BOOL gbPDFFileGenSuccessStatusFlag;
// TTV file generation success status flag...
BOOL gbTTVFileGenSuccessStatusFlag;


// monitors the file existence after configuration ....
extern BOOL gbConfigFileStatusFlag; // invoking TT4 Config state will initialize MBR..
extern BOOL gbInitMBRStatusFlag; // from reset state will initialize MBR..

// Global USB Descriptor control..
extern _eUSB_Descriptors _geUSB_SetDescriptor;

// download time storage...
extern volatile UINT32 gudwDeviceStoppedDownloadTime;

// the time when the device was plugged to the USB to download temperature datapoints...
extern volatile UINT32 gudwDeviceReadOnTime;

// command byte control bits...
extern __sBITS_COMMAND_BYTE _gsCommandByte;
// Stop mode status register...
extern UINT8 gubStopDeviceStatus;
// Heartbeat Icon timer counter ...
extern volatile UINT8 gubHeartIconTimer;
// Triggered Alarm cycle timer counter...
extern volatile UINT8 gubTrigAlarmCycleTimer;
// LCD blink control flag...
extern BOOL gbUpdateLCDFlag;



// CDC working variables....
extern UINT8 *gubTT4Cfg_DataBuffer;
extern UINT8 *gubTT4Cfg_EEBuffer;
extern UINT8 *gubTT4Cfg_ScratchPadMemory;
extern char *USB_Out_Buffer;
extern char *USB_In_Buffer;

// temperary storage for memory transfer buffer...
extern UINT32  gudwMem;
extern UINT8   gubMem;
extern UINT16  guwMem;



//The LUN variable definition is critical to the MSD function driver.  This
//  array is a structure of function pointers that are the functions that 
//  will take care of each of the physical media.  For each additional LUN
//  that is added to the system, an entry into this array needs to be added
//  so that the stack can know where to find the physical layer functions.
//  In this example the media initialization function is named 
//  "MediaInitialize", the read capacity function is named "ReadCapacity",
//  etc.  
LUN_FUNCTIONS LUN[MAX_LUN + 1] = 
{
    {
        &MDD_ExtSPIFlash_MediaInitialize,
        &MDD_ExtSPIFlash_ReadCapacity,
        &MDD_ExtSPIFlash_ReadSectorSize,
        &MDD_ExtSPIFlash_MediaDetect,
        &MDD_ExtSPIFlash_SectorRead,
        &MDD_ExtSPIFlash_WriteProtectState,
        &MDD_ExtSPIFlash_SectorWrite
    }
};

/* Standard Response to INQUIRY command stored in ROM 	*/
const ROM InquiryResponse inq_resp = {
	0x00,		// peripheral device is connected, direct access block device
	0x80,           // removable
	0x04,	 	// version = 00=> does not conform to any standard, 4=> SPC-2
	0x02,		// response is in format specified by SPC-2
	0x20,		// n-4 = 36-4=32= 0x20
	0x00,		// sccs etc.
	0x00,		// bque=1 and cmdque=0,indicates simple queueing 00 is obsolete,
			// but as in case of other device, we are just using 00
	0x00,		// 00 obsolete, 0x80 for basic task queueing
	{'T','T','4','U','S','B','M','A'
    },
	// this is the T10 assigned Vendor ID
	{'S','e','n','s','i','t','e','c','h',' ','I','n','c','.',' ',' '
    },
	{'0','0','0','1'
    }
};



/** PRIVATE PROTOTYPES *********************************************/
static void InitializeSystem(void);
void USBDeviceTasks(void);
void ProcessIO(void);

void BlinkUSBStatus(void);
void UserInit(void);
void USBCBSuspend(void);

// TT4 USB configuration serial number...
#define TT4USB_CONFIG_SN 1234567890ul

#define MSD_RESERVED_FREE_DISKSPACE  586ul // ~300k bytes free, calc: 512 x 586 = 300kb , 2001 is 1Mb ... mjmariveles

// debug modes...
#define FILE_ERROR_ENA  0 // do not enable PDF and TTV file error handler...
#define EEPROM_FILE_ENA 0 // do not generate EEPROM file....

#define PDF_FILENAME "PDF Error.txt"
#define PDF_FILE_ERROR_MESSAGE "PDF File Creation ERROR, memory allocation fault or maybe Disk is Full!..."

#define TTV_FILENAME "TTV Error.txt"
#define TTV_FILE_ERROR_MESSAGE "TTV File Creation ERROR, encryption error or maybe Disk is Full!..."

void FileCreationError(const char *par_strFilename, const char *par_strMessage);


/** USB CONTROL VARIABLES ******************************************************/

USB_HANDLE  lastTransmission;

USB_HANDLE USBOutHandle = 0;
USB_HANDLE USBInHandle = 0;
BOOL blinkStatusValid = TRUE;


/*******************************************************************************
**  Function     : hwUSBTT4Tasks
**  Description  : USB core handler for MSD or CDC modes
**  PreCondition : must initialize USB registers prior calling this routine.
**  Side Effects : some global variables will be affected after calling this routine.
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/

// measurement interval timer counter...
extern volatile UINT32 gudwMeasurementIntervalTimer;

// USB plugged/Unplugged status flag...
BOOL gbUSBActiveModeStatusFlag __attribute__((persistent));     //set as persistent for reset recovery

// tt file system control variables...
DWORD gacc_size;			/* Work register for fs command */
WORD gacc_files;
WORD gacc_dirs;

FILINFO Finfo;
char gLfname[64];

FATFS gFatFs, *gpFatFs;			/* File system object for each logical drive */

// PDF global filename storage...
extern char gstrPDFFilename[64];
// TTV global filename storage...
extern char gstrTTVFilename[64];

void hwUSBTT4Tasks(void)
{
 
    UINT32 ludwTotalTimeTempAcqDis;    
    UINT32 ludwQuotientValue;
    UINT32 ludwRemainderValue;
    UINT8  lubCtr; 
	FRESULT lFileResult;	
	UINT32 ludwCurrentDiskSpace;	
	
	// anytime if USB is plugged then must set USB Active Mode status flag...
    gbUSBActiveModeStatusFlag = TRUE;
    
    // set the clock divider to 1:1 in USB mode to attain the 32Mhz PLL mode... mjm     
    CLKDIV = 0x0000;
    Nop();Nop();Nop();
    
    // set delay clock speed at 32Mhz...
    guwUtilSysClk_mDly = UTIL_MDELAY_SYSCLK_32MHZ;

    
    utilDelay_ms(100);
    devFLASH_PowerUp();
    utilDelay_ms(50);

    //------------------------------------------
    // Initializing File System primary tasks...
    //------------------------------------------
    
     // from reset state, write protocol ID number to EEPROM at offset 0 ...
    guwMem = HDSK_SEND_PROTOCOL_ID;
    commTT4Cfg_FastWriteCfgEeprom(OFFSET_PROTOCOL_ID,(const void *)&guwMem);    
    utilDelay_ms(50);
    
    // from reset state, write firmware version number to EEPROM at offset 2 ...
    guwMem = (TT4USB_MA_SW_VERSION_1*100) + TT4USB_MA_SW_VERSION_2;
    commTT4Cfg_FastWriteCfgEeprom(OFFSET_FIRMWARE_VER,(const void *)&guwMem);    
    utilDelay_ms(50);
   
    
    // tt4 not yet logged in...
    gubHdSkOkLock = COMM_HDSK_TASK_NOT_DONE;

    InitializeSystem();


    //-----------------
    // exec CDC mode...
    //-----------------

    if(_geUSB_SetDescriptor == _eCDC_){
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Pls note this serial number is only a dummy assignment in order to initialize the USB descriptor array data...
        // the device serial number information was already disabled!... 
        _gsSensitech_Configuration_Area.udwManufacturingSerialNumber = TT4USB_CONFIG_SN;  
        commTT4Cfg_USBLoadSerialNumber(_gsSensitech_Configuration_Area.udwManufacturingSerialNumber);
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        

        // allocate working vairables for CDC memory storage...
        gubTT4Cfg_DataBuffer = (UINT8 *)malloc(512u * 6 * sizeof(UINT8));
        gubTT4Cfg_EEBuffer  = (UINT8 *)malloc(64u * sizeof(UINT8));        
        gubTT4Cfg_ScratchPadMemory  = (UINT8 *)malloc(32u * sizeof(UINT8));        
        USB_Out_Buffer  = (char *)malloc(64u * sizeof(char));        
        USB_In_Buffer  = (char *)malloc(64u * sizeof(char));        
        
    }
    
    
    //-----------------
    // exec MSD mode...
    //-----------------
    
    if(_geUSB_SetDescriptor == _eMSD_){
        
        //----------------------------------------------------------
        // clear residual files...
        //----------------------------------------------------------
        if((gbConfigFileStatusFlag == TRUE) || (gbInitMBRStatusFlag == TRUE)){
            gbInitMBRStatusFlag = gbConfigFileStatusFlag = FALSE;
            // reset boot sector to clear all file from reset and configured states....            
            utilDelay_ms(50);
            devFLASH_CreateMBR();
        }

        //----------------------------------------------------------
        // turn on RED LED...
        //----------------------------------------------------------
        TT4_RED_LED_ON;
        
        //----------------------------------------------------------    
        // use TT4USB-MA unique manufacturers serial number...
        // get manufacturers serial number from EEPROM offset 4...
        //----------------------------------------------------------
        commTT4Cfg_FastReadCfgEeprom(OFFSET_MANUFACTURING_SERIAL_NUM,(void *)&gudwMem);    
        _gsSensitech_Configuration_Area.udwManufacturingSerialNumber = gudwMem;                
        commTT4Cfg_USBLoadSerialNumber(_gsSensitech_Configuration_Area.udwManufacturingSerialNumber);

        
        //----------------------------------------------------------
        // check if device has been started...
        //----------------------------------------------------------
        if(gubStartDeviceStatusFlag==1){            
            
            // get RTCC value for time stamping...              
            // this is the monitor stopped time, monitor read download time...
            gudwDeviceReadOnTime = drvRTC_ReadRTCC_CurrentTime();            
        
            // initialize long filename pointer vars....
	        Finfo.lfname = gLfname;
	        Finfo.lfsize = sizeof(gLfname); 
    
            // mount file system driver... mjm
            f_mount(0,&gFatFs); 
            
            // check available disk space....
            lFileResult = f_getfree("0:",&ludwCurrentDiskSpace,&gpFatFs);
            
            if(ludwCurrentDiskSpace <= MSD_RESERVED_FREE_DISKSPACE){                
                utilDelay_ms(50);
                // clear all disk files for preparing to write!... mjm
                devFLASH_CreateMBR();
            }


            //--------------------------------------------------------------------------
            // note: you can enable this feature if set to 1 ontop define settings...
            //--------------------------------------------------------------------------
            if(EEPROM_FILE_ENA) hwUSB_DumpEepromContentsToFile("EEPROM_CFG.txt"); // for debbugging only... mjm
    






            /////////////////////   DEBUG MODE  ////////////////////////////////////////////////////            
            
            // PDF Pasword enabled: 270 datapoint only w/ 3 markers
            // PDF Pasword disabled: 460 datapoint  w/ 3 markers
           
            // for debugging purposes only to test 16k dp @ 10s interval!...mjm
            //_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints = 15950u;
            //_gsUser_Configuration_Area.udwUnit_Stop_TimeStamp += 159500u;
        /*
            _gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints = 16000u;            
            _gsUser_Configuration_Area.udwUnit_Stop_TimeStamp = _gsUser_Configuration_Area.udwUnit_Start_TimeStamp + ((UINT32)_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints * _gsUser_Configuration_Area.udwMeasurementInterval);
            commTT4Cfg_EERPOM_PostDataLogging();
            //_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints = 600u;
            //_gsUser_Configuration_Area.udwUnit_Stop_TimeStamp += 6000u;            
            /////////////////////////////////////////////////////////////////////////
            // copy all datapoint fromm EEPROM to 16k EDS memory of integers...
            commTT4Cfg_CacheMem_UploadEepromDatapoints(); // for debugging only... mjm   
        
        */
            /////////////////////   DEBUG MODE  ////////////////////////////////////////////////////            







           //-----------------------------------------------------------------
           // detect and secure if the USB port is plugged at this stage...            
           //-----------------------------------------------------------------                
            if(TT4_USB_VBUS_IO != USB_PORT_UNPLUGGED){
                
                //----------------------------------------------------------
                // check if PDF file generation is requested...            
                //----------------------------------------------------------
                if(_gsExtdOptByte.ExtOpt.Byte2.PDFReptCreaCfg > 0){                    
                    
                    //----------------------------------------------------------
                    // check if the PDF file exist...
                    //----------------------------------------------------------
                    if(gbPDFFileResetStatusFlag == FALSE){
                        // search PDF file if existent...
                        sprintf((char *)&gLfname[0],"%s",gstrPDFFilename);
                        lFileResult = f_stat((const TCHAR *)&gLfname[0], &Finfo);                  
                    }
                    else{
                        lFileResult = FR_NO_FILE;
                    }
                    
                    //----------------------------------------------------------
                    // check if previous PDF file generation is successful... 
                    //----------------------------------------------------------
                    if(gbPDFFileGenSuccessStatusFlag==FALSE) lFileResult = FR_NO_FILE;
                    
                    //----------------------------------------------------------
                    // create PDF file...
                    //----------------------------------------------------------
                    if((lFileResult == FR_NO_FILE) && (lFileResult != FR_NO_FILESYSTEM)){                       
                        // reset PDF file generation flag...
                        gbPDFFileResetStatusFlag = FALSE;
                        // generate PDF file...
                        gbUSBFileGenSuccessStatusFlag = appPDF_GenerateFile();                        
                        // update PDF file generation success status flag...
                        gbPDFFileGenSuccessStatusFlag = gbUSBFileGenSuccessStatusFlag;
                        
                        //---------------------------------------------------
                        //  if any of the following conditions below the 
                        //  device will return back to Main Core Loop...... 
                        //  1. gbPDFFileGenSuccessStatusFlag = FALSE , PDF file unsuccessful...            
                        //  2. if the USB Vbus is zero due to the user unplugged the device  
                        //---------------------------------------------------
                        if((gbPDFFileGenSuccessStatusFlag==FALSE)||(TT4_USB_VBUS_IO == USB_PORT_UNPLUGGED)){
                            // restoring all registers before going to Main Core Loop...            
                            USBDevice_SetPortShutdownStatus();
                            // return back to Main Core Loop...
                            return;
                        } /*  End of Reset MSD mode Registers  */
                        
                    } /*  End of PDF file processing... */                        
                    
                } /*   End Of PDF File creation...  */  
                
            } /*   End of USB plugged detection....   */
            
            
            
            if((TT4_USB_VBUS_IO != USB_PORT_UNPLUGGED) && (lFileResult != FR_NO_FILESYSTEM)){

                //----------------------------------------------------------
                // check if TTV file generation is requested...
                //----------------------------------------------------------
                if(_gsExtdOptByte.ExtOpt.Byte2.DisTTVFileCreat == 0){
                    
                    //----------------------------------------------------------
                    // check if the TTV file exist...
                    //----------------------------------------------------------
                    if(gbTTVFileResetStatusFlag == FALSE){                        
                        // search PDF file if existent...
                        sprintf((char *)&gLfname[0],"%s",gstrTTVFilename);
                        lFileResult = f_stat((const TCHAR *)&gLfname[0], &Finfo);
                    }
                    else{
                        lFileResult = FR_NO_FILE;
                    }
                    
                    //----------------------------------------------------------
                    // check if previous TTV file generation is successful... 
                    //----------------------------------------------------------
                    if(gbTTVFileGenSuccessStatusFlag==FALSE) lFileResult = FR_NO_FILE;
                    
                    //----------------------------------------------------------
                    // create TTV file...
                    //----------------------------------------------------------
                    if((lFileResult == FR_NO_FILE) && (lFileResult != FR_NO_FILESYSTEM)){    
                        // reset TTV file generation flag...
                        gbTTVFileResetStatusFlag = FALSE;  
                        // generate PDF file...
                        gbUSBFileGenSuccessStatusFlag = appTTV_GenerateFile(); 
                        // update PDF file generation success status flag...
                        gbTTVFileGenSuccessStatusFlag = gbUSBFileGenSuccessStatusFlag;
                        
                        //---------------------------------------------------
                        //  if any of the following conditions below the 
                        //  device will return back to Main Core Loop......                 
                        //  1. gbTTVFileGenSuccessStatusFlag = FALSE , TTV file unsuccessful...
                        //  2. if the USB Vbus is zero due to the user unplugged the device  
                        //---------------------------------------------------
                        if((gbTTVFileGenSuccessStatusFlag==FALSE)||(TT4_USB_VBUS_IO == USB_PORT_UNPLUGGED)){
                            // restoring all registers before going to Main Core Loop...            
                            USBDevice_SetPortShutdownStatus();
                            // return back to Main Core Loop...
                            return;
                        } /*  End of Reset MSD mode Registers  */
                        
                    } /*   End Of TTV File processing...  */   
                    
                } /*   End Of TTV File creation...  */
                
            } /*   End of USB plugged detection....   */  
            
        } /*    End of Start Device File creation...    */
        

        // open the LUN after writing internal files so the PC can read/write in the spi flash w/o file write conflict w/ the filesystem library...  \mjmariveles
        LUNSoftAttach(0);
        
        
        //-----------------------------------------
        // Setting#1 - multi-drop mode only!
        //-----------------------------------------
        if(gubStopDeviceStatus!=_DEVICE_STOP_STATUS_SLEEP_){ 
            
            if((_gsGenCfgOptByte.GenOpt.Byte1.MultDropFlag==1)&&(_geUSB_SetDescriptor == _eMSD_)&&(gubStartDeviceStatusFlag==1)){  
                
        
                // put a little bit delay to stabilize after the external flash devices operation was done...
                utilDelay_ms(10);

                //-----------------------------------------------------------------------------------------
                // calculate idle time on how long is the temperature acquisition were stopped!... mjm
                //-----------------------------------------------------------------------------------------

                // read RTCC value time after the report files were created!...  
                drvRTC_ReadRTCC_UpdateCurrentTimeSecs();
                // updated RTCC currrent time :  gudwTT4MA_CurrentTime...
                
                
                //////////////////////////////////////// debug 05032013 ///////////////////////////////////////////
                //drvRTC_ReadRTCC_CurrentTimeInSecs();
                //////////////////////////////////////// debug 05032013 ///////////////////////////////////////////
                
                utilDelay_ms(10);
        
                //-----------------------------------------------------------------------------------------
                // Quick Notes:
                //
                // take the difference between time before the files were created and then after the files were created!...
                // note: download_time  = Trip_lenght + remaining_measurement_interval_time 
                //       Trip_lenght    = gudwDeviceStoppedDownloadTime - gudwMeasurementIntervalTimer
                // Total_ReportFileTime = Trip_lenght + remaining_measurement_interval_time + File_creation_time 
                //
                // note by : mjmariveles 12032012...
                //-----------------------------------------------------------------------------------------                


                // We shouldn't need this line. Current time should always be equal to or greater than the time of the last point collected.
                // I put it in to help debug a RTCC clock start/stop issue. This insures the unit doesn't hang in case of a real time clock stoppage
                // I will leave it in for now...DLE 10/04/2014
                //---------------------------------------------------------------------------------------------
                if (gudwTT4MA_CurrentTime >= (_gsUser_Configuration_Area.udwUnit_Start_TimeStamp + ((_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints-1) * _gsUser_Configuration_Area.udwMeasurementInterval)))
                // a 1 measurement interval was added to compensate the advanced 1 measurement interval at datapoint zero otherweise a negative value will result... 
                   ludwTotalTimeTempAcqDis = gudwTT4MA_CurrentTime - (_gsUser_Configuration_Area.udwUnit_Start_TimeStamp + ((_gsSensitech_Configuration_Area.uwNumberOfStoredDataPoints-1) * _gsUser_Configuration_Area.udwMeasurementInterval));
                else
                {
                   ludwTotalTimeTempAcqDis = 0;
                }
                   
                // set marker flag...
                _gsCommandByte.Bit.MarkerFlag = 1;
                        
                // check if the total time covered during file generation is greater than preset Measurement Interval!...                
                if(ludwTotalTimeTempAcqDis >= _gsUser_Configuration_Area.udwMeasurementInterval){
            
                    ludwQuotientValue = ludwTotalTimeTempAcqDis / _gsUser_Configuration_Area.udwMeasurementInterval;
                    ludwRemainderValue = ludwTotalTimeTempAcqDis % _gsUser_Configuration_Area.udwMeasurementInterval;

                    //-----------------------------------------------------------------------------------------
                    // fill the missed datapoints positions in the eeprom and increment its datapoints...
                    //-----------------------------------------------------------------------------------------
            
                    for(lubCtr=0;lubCtr<ludwQuotientValue;lubCtr++){                    
                        // write marker bits to MSB of the first temperature data entry generated by Multi-Drop mode... 
                        if(appTT4MA_TempAcquisitionMode() == MEMORY_FULL){                            
                            
                            // global indicator of memory is full during multi-drop mode...
                            gubStopDeviceStatus = _DEVICE_STOP_STATUS_SLEEP_;
                            
                            // disable timer interrupt....                    
                            appTT4MA_Timer1Interrupt_Disable();                                        
                            utilDelay_ms(10);
                            
                            // clear timer event flag...
                            gubTimerEventFlag = 0;      
                            
                            // clear all segments...    
                            devLCD_Display_AllSegments(OFF);
                
                            // USB flag in MSD mode...                  
                            devLCD_Display_StopAlarmDevice(1); 
                            
                            // break the loop if memory full occured!...
                            break;
                                        
                        } /*  End of Temperature Measurement... mjm  */

                        _gsCommandByte.Bit.MarkerFlag = 0;
                    }                    
            
                    // continue to start the remaining measurement interval timer value to record temperatures...
                    gudwMeasurementIntervalTimer = ludwRemainderValue;

                }
                else{            
                    // since it is less than its preset measurement interval then reload a new value to its 
                    // free running measurement interval timer.... mjm
                    gudwMeasurementIntervalTimer = ludwTotalTimeTempAcqDis;
                }
                                
            }  /*   End of Multi-Drop initial measurement interval time calculation...... */
            
        } /*  End of Device is in STOP mode and calculate multi-drop MI time....  */ 
        
    } /*   End of USB MSD Mode initialization Tasks....   */   
        

    
    //-------------------------------------
    // Initializing USB primary tasks...
    //-------------------------------------
    
    // If using USB Interrupt then must set USB interrupt priority is 7...mjm
    IPC21bits.USB1IP  = USB_ASSIGNED_INTERRUPT_PRIORITY; 

    // Load USB Device Descriptors... mjmariveles
    drvUSB_LoadDeviceDescriptors();

    // retrieve 1st the EEPROM table and write to EEPROM.txt file to see the current contents
    // before enumerating and connected to USB MSD......mjm
    USBDeviceInit();   
    
    // use only the polling method to minimize interrupt usage....mjm 09252010
    #if defined(USB_INTERRUPT)
        USBDeviceAttach();
    #endif


    //------------------------------------------
    // Setting#2 - multi-drop mode only!    
    //------------------------------------------
    
    if(gubStopDeviceStatus!=_DEVICE_STOP_STATUS_SLEEP_){ 
        // enable timer interrupt after USB configuration...
        if((_gsGenCfgOptByte.GenOpt.Byte1.MultDropFlag==1)&&(_geUSB_SetDescriptor == _eMSD_)&&(gubStartDeviceStatusFlag==1)){          
            // clear Arrow if marker was asserted...
            devLCD_Display_ArrowMark(0);
            // clear flag...
            gubTimerEventFlag = 0;                    
            // activate timer interrupt....                    
            appTT4MA_Timer1Interrupt_Enable();            
        }        
    }

    //------------------------------------------
    // USB Core Loop........    
    //------------------------------------------    
    while(1)
    {
        #if defined(USB_POLLING)
		// Check bus status and service USB interrupts.
        USBDeviceTasks(); // Interrupt or polling method.  If using polling, must call
        				  // this function periodically.  This function will take care
        				  // of processing and responding to SETUP transactions 
        				  // (such as during the enumeration process when you first
        				  // plug in).  USB hosts require that USB devices should accept
        				  // and process SETUP packets in a timely fashion.  Therefore,
        				  // when using polling, this function should be called 
        				  // frequently (such as once about every 100 microseconds) at any
        				  // time that a SETUP packet might reasonably be expected to
        				  // be sent by the host to your device.  In most cases, the
        				  // USBDeviceTasks() function does not take very long to
        				  // execute (~50 instruction cycles) before it returns.
        #endif
        
        
        //---------------------------------
        //  check USB port events...
        //---------------------------------
        if(TT4_USB_VBUS_IO == USB_PORT_UNPLUGGED){             
            
            if(_geUSB_SetDescriptor == _eCDC_){
                
                // deallocate working vairables for CDC memory storage...
                free((void*)USB_In_Buffer);                
                free((void*)USB_Out_Buffer);                
                free((void*)gubTT4Cfg_EEBuffer);                
                free((void*)gubTT4Cfg_ScratchPadMemory);                
                free((void*)gubTT4Cfg_DataBuffer);
            }



            //--------------------------------------------------
            // multi-drop mode temperature acquisition...
            //--------------------------------------------------            
            if(gubStopDeviceStatus!=_DEVICE_STOP_STATUS_SLEEP_){ 
                if((_gsGenCfgOptByte.GenOpt.Byte1.MultDropFlag==1)&&(_geUSB_SetDescriptor == _eMSD_)){            
                    // disable timer interrupt prior exit from USB domain....                    
                    appTT4MA_Timer1Interrupt_Disable();            
                    utilDelay_ms(10);
                }
            }

            //---------------------------------------------------------------------            
            // disable all USB control registers before going to Main Core Loop...
            //---------------------------------------------------------------------
         
            // disabling all USB control register and turn-off the module...
            hwInit_USBControlRegisters();
            // set low power...  
            USBDevice_SetPortShutdownStatus();
            
            // return to Main Ccre Loop...
            return;
            
        } /* End of USB Unplugged */
        
        

        //--------------------------------------------------
        // multi-drop mode temperature acquisition...
        //--------------------------------------------------
        if((gubStopDeviceStatus!=_DEVICE_STOP_STATUS_SLEEP_)&&(_geUSB_SetDescriptor != _eCDC_)){
            
            if((_gsGenCfgOptByte.GenOpt.Byte1.MultDropFlag==1)&&(_geUSB_SetDescriptor == _eMSD_)){
                
                if(gubTimerEventFlag == 1){
                    
                    // update RTC system counters....
                    appTT4MA_RTCSystemCounterUpdate();  
                    
                    // clear timer event flag...
                    gubTimerEventFlag = 0;                          
                    
                    // set debug port high...    
                  //  MI_TOGGLE_PORT = 1; 
                    
                    // check the blink mode if enabled!...
                    devLCD_GetBlinkStatus();
                    
                    //------------------------------------
                    // Temperature Measurement Interval....
                    //------------------------------------                
                    
                    if(gudwMeasurementIntervalTimer >= _gsUser_Configuration_Area.udwMeasurementInterval){    

                        gudwMeasurementIntervalTimer = 0;// resets everytime when the measurement interval counter time-out occured!...mjm
                
                        // turn-on heart icon...
                        devLCD_Display_HeartBeat(1);                
                        // re-start heartbeat icon display timer...
                        gubHeartIconTimer = 0;                
                
                        // read temperature and calculate running totals status...
                        if(appTT4MA_TempAcquisitionMode() == MEMORY_FULL){                            
                            
                            // global indicator of memory is full during multi-drop mode...
                            gubStopDeviceStatus = _DEVICE_STOP_STATUS_SLEEP_;
                            
                            // disable timer interrupt....                    
                            appTT4MA_Timer1Interrupt_Disable();                                        
                            utilDelay_ms(10);
                            
                            // clear timer event flag...
                            gubTimerEventFlag = 0;      
                            
                            // clear all segments...    
                            devLCD_Display_AllSegments(OFF);
                
                            // USB flag in MSD mode...                  
                            devLCD_Display_StopAlarmDevice(1); 
                            
                            // update LCD display... 
                            devLCD_UpdateDisplay();
                            
                            // loop back...
                            continue;
                                        
                        } /*  End of Temperature Measurement... mjm  */
                
                        // in Config Display mode must skip the Temperature updating display mode...                                                         
                        devLCD_Display_ReadTemperature();
                
                    } /*  End of Time Measurement Interval Periodic Task!... mjm */    
                    
                    //------------------------------------------
                    // Heart beat icon cleard!....
                    //------------------------------------------                                
                    if(gubHeartIconTimer>=HEART_BEAT_EXIT_TIMEOUT){                    
                        devLCD_Display_HeartBeat(0);
                    }    
                    
                    //------------------------------------------
                    // Triggered Alarm Cycling time-out....
                    //------------------------------------------                                
                    if(_gsExtdOptByte.ExtOpt.Byte2.DispAlrmNumTrgAlrm == 1){
                            
                        if(gubTrigAlarmCycleTimer >= ALARM_CYCLE_TIMEOUT){    
                            gubTrigAlarmCycleTimer = 0;                        
                            devLCD_Display_CyclingTriggeredAlarm();
                        }                
                        
                    } /*  End of Display Triggered Alarm... */                    
                    
                    //  Update LCD Display Task...         
                    if(gbUpdateLCDFlag == TRUE){ 
                        // system running icons...
                        devLCD_Display_SystemRunningIcons();
                        // load Segment register and send to I2C ...
                        devLCD_UpdateDisplay();
                    }                        
                    
                    // reset debug port low...    
                    MI_TOGGLE_PORT = 0; 
                
                } /*  End of Timer Event Tasks!..  */
                
            } /*  Enf of Multi-drop tasks!..  */

        } /*  End of Multi-drop Temperature Measurement Failed Memory full or Device is in the state of stop mode... mjm  */ 


		// Application-specific tasks.
		// Application related code may be added here, or in the ProcessIO() function.
        ProcessIO();        // See msd.c & msd.h

    } //end while
    
} /*   End of USB Core Loop   */ 


/*******************************************************************************
**  Function     : USBDevice_SetPortShutdownStatus
**  Description  : setups USB shutdown register status
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : none
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void USBDevice_SetPortShutdownStatus(void){

    register UINT16 lubCtr;

    Nop();
    TT4_GREEN_LED_OFF;
    Nop();
    TT4_RED_LED_OFF;
    Nop();

    utilDelay_ms(10);
    devFLASH_PowerDown();
    utilDelay_ms(10);
    
    Nop();Nop();Nop();
    CLKDIV = 0x0040; // 32MHz / 2 = 16Mhz System Clock...
    Nop();Nop();Nop();
    // set delay clock speed at 16Mhz...
    guwUtilSysClk_mDly = UTIL_MDELAY_SYSCLK_16MHZ;
                
    // if USB is unplugged then reset USB Active Mode status flag...
    gbUSBActiveModeStatusFlag = FALSE;
            
    // make it sure the residual voltage at the Vbus should be discharged....
    while(TT4_USB_VBUS_IO != USB_PORT_UNPLUGGED){
        // time-out trapping to break the loop!...
        for(lubCtr=0;lubCtr<1000u;lubCtr++) break;       
    }

            
} /*    End of USBDevice_SetPortShutdownStatus.....   */



/*******************************************************************************
**  Function     : FileCreationError
**  Description  : file creation error handler
**  PreCondition : none
**  Side Effects : none
**  Input        : par_strFilename - filename to write the status message
**  Output       : par_strMessage - status message to be written to the text file. 
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void FileCreationError(const char *par_strFilename, const char *par_strMessage){

    DRVFS_FILEP logFile;        
    UINT8  lubChar[100];
    UINT8  lubSize;     
    UINT   luwByteToWrite;
    
    //The log file is not open so let's make sure the pointer is NULLed out
    DRVFS_FOPEN(&logFile, (const char*)par_strFilename,FA_CREATE_ALWAYS | FA_WRITE);

    // format the EERPOM data with its offset and data...
    sprintf((char*)&lubChar[0],"%s",par_strMessage);

    // determine the string length...
    lubSize = 0;
    while(lubChar[lubSize]!=0) ++lubSize;
        
    // write to file IO stream...        
    DRVFS_FWRITE(&logFile,(const void *)&lubChar[0],(UINT)lubSize,&luwByteToWrite);
    
    DRVFS_FCLOSE(&logFile);    
}

/*******************************************************************************
**  Function     : hwUSB_DumpEepromContentsToFile
**  Description  : dumps EEPROM data to a text file for debugging..
**  PreCondition : none
**  Side Effects : none
**  Input        : none
**  Output       : par_strFilename - filename to be written in the USB
** _____________________________________________________________________________
**  Date    Author                Changes
**  092210  Michael J. Mariveles  1st release.    
*******************************************************************************/
void hwUSB_DumpEepromContentsToFile(char par_strFilename[20]){
    
    DRVFS_FILEP logFile;    
    UINT16 luwCtr;
    UINT8  lubChar[30];
    UINT8  lubSize; 
    UINT8  lubEEData;     
    UINT   luwByteToWrite;
    
    //The log file is not open so let's make sure the pointer is NULLed out
    DRVFS_FOPEN(&logFile, (const char*)par_strFilename,FA_CREATE_ALWAYS | FA_WRITE);
    
    // note beyond from this maximum offset is the data points!... mjm
    for(luwCtr=0;luwCtr<=767u;luwCtr++){ // 767 bytes total memory area in configuration table
        
        // read all EERPOM data from offset 0 - 760...
        devEEPROM_RandomRead(luwCtr,&lubEEData);
        
        // format the EERPOM data with its offset and data...
        sprintf((char*)&lubChar[0],"[%u]:%u \r\n",luwCtr,lubEEData);
                
        // determine the string length...
        lubSize = 0;
        while(lubChar[lubSize]!=0) ++lubSize;
        
        // write to file IO stream...        
        DRVFS_FWRITE(&logFile,(const void *)lubChar,(UINT)lubSize,&luwByteToWrite); // load 512 bytes..
        
    }
    
    DRVFS_FCLOSE(&logFile);
    
}


/********************************************************************
 * Function:        static void InitializeSystem(void)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        InitializeSystem is a centralize initialization
 *                  routine. All required USB initialization routines
 *                  are called from here.
 *
 *                  User application initialization routine should
 *                  also be called from here.                  
 *
 * Note:            None
 *******************************************************************/
static void InitializeSystem(void)
{
 
//	The USB specifications require that USB peripheral devices must never source
//	current onto the Vbus pin.  Additionally, USB peripherals should not source
//	current on D+ or D- when the host/hub is not actively powering the Vbus line.
//	When designing a self powered (as opposed to bus powered) USB peripheral
//	device, the firmware should make sure not to turn on the USB module and D+
//	or D- pull up resistor unless Vbus is actively powered.  Therefore, the
//	firmware needs some means to detect when Vbus is being powered by the host.
//	A 5V tolerant I/O pin can be connected to Vbus (through a resistor), and
// 	can be used to detect when Vbus is high (host actively powering), or low
//	(host is shut down or otherwise not supplying power).  The USB firmware
// 	can then periodically poll this I/O pin to know when it is okay to turn on
//	the USB module/D+/D- pull up resistor.  When designing a purely bus powered
//	peripheral device, it is not possible to source current on D+ or D- when the
//	host is not actively providing power on Vbus. Therefore, implementing this
//	bus sense feature is optional.  This firmware can be made to use this bus
//	sense feature by making sure "USE_USB_BUS_SENSE_IO" has been defined in the
//	HardwareProfile.h file.    
    #if defined(USE_USB_BUS_SENSE_IO)
    tris_usb_bus_sense = INPUT_PIN; // See HardwareProfile.h
    #endif
    
//	If the host PC sends a GetStatus (device) request, the firmware must respond
//	and let the host know if the USB peripheral device is currently bus powered
//	or self powered.  See chapter 9 in the official USB specifications for details
//	regarding this request.  If the peripheral device is capable of being both
//	self and bus powered, it should not return a hard coded value for this request.
//	Instead, firmware should check if it is currently self or bus powered, and
//	respond accordingly.  If the hardware has been configured like demonstrated
//	on the PICDEM FS USB Demo Board, an I/O pin can be polled to determine the
//	currently selected power source.  On the PICDEM FS USB Demo Board, "RA2" 
//	is used for	this purpose.  If using this feature, make sure "USE_SELF_POWER_SENSE_IO"
//	has been defined in HardwareProfile.h, and that an appropriate I/O pin has been mapped
//	to it in HardwareProfile.h.
    #if defined(USE_SELF_POWER_SENSE_IO)
    tris_self_power = INPUT_PIN;	// See HardwareProfile.h
    #endif
    
    UserInit();

}//end InitializeSystem

/******************************************************************************
 * Function:        void UserInit(void)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        This routine should take care of all of the demo code
 *                  initialization that is required.
 *
 * Note:            
 *
 *****************************************************************************/
void UserInit(void)
{
    
}//end UserInit


/******************************************************************************
 * Function:        void mySetLineCodingHandler(void)
 *
 * PreCondition:    USB_CDC_SET_LINE_CODING_HANDLER is defined
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        This function gets called when a SetLineCoding command
 *                  is sent on the bus.  This function will evaluate the request
 *                  and determine if the application should update the baudrate
 *                  or not.
 *
 * Note:            
 *
 *****************************************************************************/
#if defined(USB_CDC_SET_LINE_CODING_HANDLER)
void mySetLineCodingHandler(void)
{
    //If the request is not in a valid range
    if(cdc_notice.GetLineCoding.dwDTERate.Val > 115200)
    {
        //NOTE: There are two ways that an unsupported baud rate could be
        //handled.  The first is just to ignore the request and don't change
        //the values.  That is what is currently implemented in this function.
        //The second possible method is to stall the STATUS stage of the request.
        //STALLing the STATUS stage will cause an exception to be thrown in the 
        //requesting application.  Some programs, like HyperTerminal, handle the
        //exception properly and give a pop-up box indicating that the request
        //settings are not valid.  Any application that does not handle the
        //exception correctly will likely crash when this requiest fails.  For
        //the sake of example the code required to STALL the status stage of the
        //request is provided below.  It has been left out so that this demo
        //does not cause applications without the required exception handling
        //to crash.
        //---------------------------------------
        //USBStallEndpoint(0,1);
    }
    else
    {
        //Update the baudrate info in the CDC driver
        CDCSetBaudRate(cdc_notice.GetLineCoding.dwDTERate.Val);
    }
}
#endif


/********************************************************************
 * Function:        void ProcessIO(void)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        This function is a place holder for other user
 *                  routines. It is a mixture of both USB and
 *                  non-USB tasks.
 *
 * Note:            None
 *******************************************************************/
void ProcessIO(void)
{   

    // User Application USB tasks
     //Blink the LEDs according to the USB device status
    if(blinkStatusValid)
    {
        BlinkUSBStatus();
    }
    // User Application USB tasks
    if((USBDeviceState < CONFIGURED_STATE)||(USBSuspendControl==1)) return;


    if(_geUSB_SetDescriptor == _eCDC_){
        
        if(mUSBUSARTIsTxTrfReady())
        {		
		    hwUSB_TT4MA_Communications();  		
	    }
	    
        // CDC tasks...    
        CDCTxService();
    }    
    else{
        // MSD tasks...    
        MSDTasks();    
    }
}//end ProcessIO


/********************************************************************
 * Function:        void BlinkUSBStatus(void)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        BlinkUSBStatus turns on and off LEDs 
 *                  corresponding to the USB device state.
 *
 * Note:            mLED macros can be found in HardwareProfile.h
 *                  USBDeviceState is declared and updated in
 *                  usb_device.c.
 *******************************************************************/
void BlinkUSBStatus(void)
{

    if(USBSuspendControl == 1)
    {
        
    }
    else
    {
        if(USBDeviceState == DETACHED_STATE)
        {            
            TT4_GREEN_LED_OFF;
            TT4_RED_LED_OFF;
        }
        else if(USBDeviceState == ATTACHED_STATE)
        {
           TT4_GREEN_LED_ON; // turns on after enumeration process is done.. mjm
        }
        else if(USBDeviceState == POWERED_STATE)
        {
            
        }
        else if(USBDeviceState == DEFAULT_STATE)
        {
            
        }
        else if(USBDeviceState == ADDRESS_STATE)
        {
            
        }
        else if(USBDeviceState == CONFIGURED_STATE)
        {
            
        }//end if(...)
    }//end if(UCONbits.SUSPND...)

}//end BlinkUSBStatus







// ******************************************************************************************************
// ************** USB Callback Functions ****************************************************************
// ******************************************************************************************************
// The USB firmware stack will call the callback functions USBCBxxx() in response to certain USB related
// events.  For example, if the host PC is powering down, it will stop sending out Start of Frame (SOF)
// packets to your device.  In response to this, all USB devices are supposed to decrease their power
// consumption from the USB Vbus to <2.5mA each.  The USB module detects this condition (which according
// to the USB specifications is 3+ms of no bus activity/SOF packets) and then calls the USBCBSuspend()
// function.  You should modify these callback functions to take appropriate actions for each of these
// conditions.  For example, in the USBCBSuspend(), you may wish to add code that will decrease power
// consumption from Vbus to <2.5mA (such as by clock switching, turning off LEDs, putting the
// microcontroller to sleep, etc.).  Then, in the USBCBWakeFromSuspend() function, you may then wish to
// add code that undoes the power saving things done in the USBCBSuspend() function.

// The USBCBSendResume() function is special, in that the USB stack will not automatically call this
// function.  This function is meant to be called from the application firmware instead.  See the
// additional comments near the function.

/******************************************************************************
 * Function:        void USBCBSuspend(void)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        Call back that is invoked when a USB suspend is detected
 *
 * Note:            None
 *****************************************************************************/
void USBCBSuspend(void)
{
	//Example power saving code.  Insert appropriate code here for the desired
	//application behavior.  If the microcontroller will be put to sleep, a
	//process similar to that shown below may be used:
	
	//ConfigureIOPinsForLowPower();
	//SaveStateOfAllInterruptEnableBits();
	//DisableAllInterruptEnableBits();
	//EnableOnlyTheInterruptsWhichWillBeUsedToWakeTheMicro();	//should enable at least USBActivityIF as a wake source
	//Sleep();
	//RestoreStateOfAllPreviouslySavedInterruptEnableBits();	//Preferrably, this should be done in the USBCBWakeFromSuspend() function instead.
	//RestoreIOPinsToNormal();									//Preferrably, this should be done in the USBCBWakeFromSuspend() function instead.

	//IMPORTANT NOTE: Do not clear the USBActivityIF (ACTVIF) bit here.  This bit is 
	//cleared inside the usb_device.c file.  Clearing USBActivityIF here will cause 
	//things to not work as intended.	
	
	

    #if defined(__C30__)
    #if 0
        U1EIR = 0xFFFF;
        U1IR = 0xFFFF;
        U1OTGIR = 0xFFFF;
        IFS5bits.USB1IF = 0;
        IEC5bits.USB1IE = 1;
        U1OTGIEbits.ACTVIE = 1;
        U1OTGIRbits.ACTVIF = 1;
        Sleep();
    #endif
    #endif
}


/******************************************************************************
 * Function:        void _USB1Interrupt(void)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        This function is called when the USB interrupt bit is set
 *					In this example the interrupt is only used when the device
 *					goes to sleep when it receives a USB suspend command
 *
 * Note:            None
 *****************************************************************************/
#if 0
void __attribute__ ((interrupt)) _USB1Interrupt(void)
{
    #if !defined(self_powered)
        if(U1OTGIRbits.ACTVIF)
        {
            IEC5bits.USB1IE = 0;
            U1OTGIEbits.ACTVIE = 0;
            IFS5bits.USB1IF = 0;
        
            //USBClearInterruptFlag(USBActivityIFReg,USBActivityIFBitNum);
            USBClearInterruptFlag(USBIdleIFReg,USBIdleIFBitNum);
            //USBSuspendControl = 0;
        }
    #endif
}
#endif

/******************************************************************************
 * Function:        void USBCBWakeFromSuspend(void)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        The host may put USB peripheral devices in low power
 *					suspend mode (by "sending" 3+ms of idle).  Once in suspend
 *					mode, the host may wake the device back up by sending non-
 *					idle state signalling.
 *					
 *					This call back is invoked when a wakeup from USB suspend 
 *					is detected.
 *
 * Note:            None
 *****************************************************************************/
void USBCBWakeFromSuspend(void)
{
	// If clock switching or other power savings measures were taken when
	// executing the USBCBSuspend() function, now would be a good time to
	// switch back to normal full power run mode conditions.  The host allows
	// a few milliseconds of wakeup time, after which the device must be 
	// fully back to normal, and capable of receiving and processing USB
	// packets.  In order to do this, the USB module must receive proper
	// clocking (IE: 48MHz clock must be available to SIE for full speed USB
	// operation).
}

/********************************************************************
 * Function:        void USBCB_SOF_Handler(void)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        The USB host sends out a SOF packet to full-speed
 *                  devices every 1 ms. This interrupt may be useful
 *                  for isochronous pipes. End designers should
 *                  implement callback routine as necessary.
 *
 * Note:            None
 *******************************************************************/
void USBCB_SOF_Handler(void)
{
    // No need to clear UIRbits.SOFIF to 0 here.
    // Callback caller is already doing that.
}

/*******************************************************************
 * Function:        void USBCBErrorHandler(void)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        The purpose of this callback is mainly for
 *                  debugging during development. Check UEIR to see
 *                  which error causes the interrupt.
 *
 * Note:            None
 *******************************************************************/
void USBCBErrorHandler(void)
{
    // No need to clear UEIR to 0 here.
    // Callback caller is already doing that.

	// Typically, user firmware does not need to do anything special
	// if a USB error occurs.  For example, if the host sends an OUT
	// packet to your device, but the packet gets corrupted (ex:
	// because of a bad connection, or the user unplugs the
	// USB cable during the transmission) this will typically set
	// one or more USB error interrupt flags.  Nothing specific
	// needs to be done however, since the SIE will automatically
	// send a "NAK" packet to the host.  In response to this, the
	// host will normally retry to send the packet again, and no
	// data loss occurs.  The system will typically recover
	// automatically, without the need for application firmware
	// intervention.
	
	// Nevertheless, this callback function is provided, such as
	// for debugging purposes.
}


/*******************************************************************
 * Function:        void USBCBCheckOtherReq(void)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        When SETUP packets arrive from the host, some
 * 					firmware must process the request and respond
 *					appropriately to fulfill the request.  Some of
 *					the SETUP packets will be for standard
 *					USB "chapter 9" (as in, fulfilling chapter 9 of
 *					the official USB specifications) requests, while
 *					others may be specific to the USB device class
 *					that is being implemented.  For example, a HID
 *					class device needs to be able to respond to
 *					"GET REPORT" type of requests.  This
 *					is not a standard USB chapter 9 request, and 
 *					therefore not handled by usb_device.c.  Instead
 *					this request should be handled by class specific 
 *					firmware, such as that contained in usb_function_hid.c.
 *
 * Note:            None
 *******************************************************************/
void USBCBCheckOtherReq(void)
{
    if(_geUSB_SetDescriptor == _eMSD_)
        USBCheckMSDRequest();
    else
        USBCheckCDCRequest();
}//end


/*******************************************************************
 * Function:        void USBCBStdSetDscHandler(void)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        The USBCBStdSetDscHandler() callback function is
 *					called when a SETUP, bRequest: SET_DESCRIPTOR request
 *					arrives.  Typically SET_DESCRIPTOR requests are
 *					not used in most applications, and it is
 *					optional to support this type of request.
 *
 * Note:            None
 *******************************************************************/
void USBCBStdSetDscHandler(void)
{
    // Must claim session ownership if supporting this request
}//end


/*******************************************************************
 * Function:        void USBCBInitEP(void)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        This function is called when the device becomes
 *                  initialized, which occurs after the host sends a
 * 					SET_CONFIGURATION (wValue not = 0) request.  This 
 *					callback function should initialize the endpoints 
 *					for the device's usage according to the current 
 *					configuration.
 *
 * Note:            None
 *******************************************************************/
void USBCBInitEP(void)
{
    #if (MSD_DATA_IN_EP == MSD_DATA_OUT_EP)
        USBEnableEndpoint(MSD_DATA_IN_EP,USB_IN_ENABLED|USB_OUT_ENABLED|USB_HANDSHAKE_ENABLED|USB_DISALLOW_SETUP);
    #else
        USBEnableEndpoint(MSD_DATA_IN_EP,USB_IN_ENABLED|USB_HANDSHAKE_ENABLED|USB_DISALLOW_SETUP);
        USBEnableEndpoint(MSD_DATA_OUT_EP,USB_OUT_ENABLED|USB_HANDSHAKE_ENABLED|USB_DISALLOW_SETUP);
    #endif

    if(_geUSB_SetDescriptor == _eMSD_)
        USBMSDInit();
    else
        CDCInitEP();
}

/********************************************************************
 * Function:        void USBCBSendResume(void)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        The USB specifications allow some types of USB
 * 					peripheral devices to wake up a host PC (such
 *					as if it is in a low power suspend to RAM state).
 *					This can be a very useful feature in some
 *					USB applications, such as an Infrared remote
 *					control	receiver.  If a user presses the "power"
 *					button on a remote control, it is nice that the
 *					IR receiver can detect this signalling, and then
 *					send a USB "command" to the PC to wake up.
 *					
 *					The USBCBSendResume() "callback" function is used
 *					to send this special USB signalling which wakes 
 *					up the PC.  This function may be called by
 *					application firmware to wake up the PC.  This
 *					function should only be called when:
 *					
 *					1.  The USB driver used on the host PC supports
 *						the remote wakeup capability.
 *					2.  The USB configuration descriptor indicates
 *						the device is remote wakeup capable in the
 *						bmAttributes field.
 *					3.  The USB host PC is currently sleeping,
 *						and has previously sent your device a SET 
 *						FEATURE setup packet which "armed" the
 *						remote wakeup capability.   
 *
 *					This callback should send a RESUME signal that
 *                  has the period of 1-15ms.
 *
 * Note:            Interrupt vs. Polling
 *                  -Primary clock
 *                  -Secondary clock ***** MAKE NOTES ABOUT THIS *******
 *                   > Can switch to primary first by calling USBCBWakeFromSuspend()
 *
 *                  The modifiable section in this routine should be changed
 *                  to meet the application needs. Current implementation
 *                  temporary blocks other functions from executing for a
 *                  period of 1-13 ms depending on the core frequency.
 *
 *                  According to USB 2.0 specification section 7.1.7.7,
 *                  "The remote wakeup device must hold the resume signaling
 *                  for at lest 1 ms but for no more than 15 ms."
 *                  The idea here is to use a delay counter loop, using a
 *                  common value that would work over a wide range of core
 *                  frequencies.
 *                  That value selected is 1800. See table below:
 *                  ==========================================================
 *                  Core Freq(MHz)      MIP         RESUME Signal Period (ms)
 *                  ==========================================================
 *                      48              12          1.05
 *                       4              1           12.6
 *                  ==========================================================
 *                  * These timing could be incorrect when using code
 *                    optimization or extended instruction mode,
 *                    or when having other interrupts enabled.
 *                    Make sure to verify using the MPLAB SIM's Stopwatch
 *                    and verify the actual signal on an oscilloscope.
 *******************************************************************/
void USBCBSendResume(void)
{
    static WORD delay_count;
    
    USBResumeControl = 1;                // Start RESUME signaling
    
    delay_count = 1800U;                // Set RESUME line for 1-13 ms
    do
    {
        delay_count--;
    }while(delay_count);
    USBResumeControl = 0;
}

/*******************************************************************
 * Function:        BOOL USER_USB_CALLBACK_EVENT_HANDLER(
 *                        USB_EVENT event, void *pdata, WORD size)
 *
 * PreCondition:    None
 *
 * Input:           USB_EVENT event - the type of event
 *                  void *pdata - pointer to the event data
 *                  WORD size - size of the event data
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        This function is called from the USB stack to
 *                  notify a user application that a USB event
 *                  occured.  This callback is in interrupt context
 *                  when the USB_INTERRUPT option is selected.
 *
 * Note:            None
 *******************************************************************/
BOOL USER_USB_CALLBACK_EVENT_HANDLER(USB_EVENT event, void *pdata, WORD size)
{
    switch(event)
    {
        case EVENT_CONFIGURED: 
            USBCBInitEP();
            break;
        case EVENT_SET_DESCRIPTOR:
            USBCBStdSetDscHandler();
            break;
        case EVENT_EP0_REQUEST:
            USBCBCheckOtherReq();
            break;
        case EVENT_SOF:
            USBCB_SOF_Handler();
            break;
        case EVENT_SUSPEND:
            USBCBSuspend();
            break;
        case EVENT_RESUME:
            USBCBWakeFromSuspend();
            break;
        case EVENT_BUS_ERROR:
            USBCBErrorHandler();
            break;
        case EVENT_TRANSFER:
            Nop();
            break;
        default:
            break;
    }      
    return TRUE; 
}

/*   End Of File   */

