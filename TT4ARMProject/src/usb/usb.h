/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm                                            **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc. and Relisource Technology Ltd.                            **
 **                                                                           **
 **  Copryright(c) 2015. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Relisource Technology Ltd.                        **
 **  Creation date       :  02/20/2015                                        **
 **  Programmer          :  Faijur Rahman		                              **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : usb.h                                                             **
 **                                                                           **
 **  Description :                                                            **
 **    - This file includes all API header files for the USB module.          **
 **                                                                           **
 **  History :                                                                ** 
 **    ver 1.00.001  -  02/20/2015                                            **
 **                                                                           **
 ******************************************************************************/ 

#ifndef USB_H
#define USB_H



#endif // USB_H
