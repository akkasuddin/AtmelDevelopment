/*
 * memory_init.c
 *
 * Created: 4/2/2015 1:10:45 PM
 *  Author: Zeeshan
 */ 
#include <asf.h>
#include "conf_board.h" /* To get on-board memories configurations */
#include "conf_access.h"
#include "main.h"

#ifdef CONF_BOARD_SMC_PSRAM
static void psram_init(void)
{
	pmc_enable_periph_clk(ID_SMC);
	/* complete SMC configuration between PSRAM and SMC waveforms. */
	smc_set_setup_timing(SMC, 0, SMC_SETUP_NWE_SETUP(0)
	| SMC_SETUP_NCS_WR_SETUP(2)
	| SMC_SETUP_NRD_SETUP(2)
	| SMC_SETUP_NCS_RD_SETUP(2));
	smc_set_pulse_timing(SMC, 0, SMC_PULSE_NWE_PULSE(6)
	| SMC_PULSE_NCS_WR_PULSE(6)
	| SMC_PULSE_NRD_PULSE(6)
	| SMC_PULSE_NCS_RD_PULSE(6));
	smc_set_cycle_timing(SMC, 0, SMC_CYCLE_NWE_CYCLE(8)
	| SMC_CYCLE_NRD_CYCLE(8));
	smc_set_mode(SMC, 0, SMC_MODE_READ_MODE | SMC_MODE_WRITE_MODE
	| SMC_MODE_DBW_BIT_16);
}
#endif

#ifdef CONF_BOARD_SRAM
static void ext_sram_init(void)
{
	pmc_enable_periph_clk(ID_SMC);
	smc_set_setup_timing(SMC, 0, SMC_SETUP_NWE_SETUP(1)
	| SMC_SETUP_NCS_WR_SETUP(1)
	| SMC_SETUP_NRD_SETUP(1)
	| SMC_SETUP_NCS_RD_SETUP(1));
	smc_set_pulse_timing(SMC, 0, SMC_PULSE_NWE_PULSE(6)
	| SMC_PULSE_NCS_WR_PULSE(6)
	| SMC_PULSE_NRD_PULSE(6)
	| SMC_PULSE_NCS_RD_PULSE(6));
	smc_set_cycle_timing(SMC, 0, SMC_CYCLE_NWE_CYCLE(7)
	| SMC_CYCLE_NRD_CYCLE(7));
	smc_set_mode(SMC, 0,
	SMC_MODE_READ_MODE | SMC_MODE_WRITE_MODE
	#if !defined(SAM4S)
	| SMC_MODE_DBW_8_BIT
	#endif
	);
	/* Configure LB, enable SRAM access */
	pio_configure_pin(PIN_EBI_NLB, PIN_EBI_NLB_FLAGS);
	/* Pull down LB, enable sram access */
	pio_set_pin_low(PIN_EBI_NLB);
}
#endif

void memories_initialization(void)
{
	#ifdef CONF_BOARD_SMC_PSRAM
	psram_init();
	#endif
	#ifdef CONF_BOARD_SRAM
	ext_sram_init();
	#endif
}
