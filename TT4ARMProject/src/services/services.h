/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm                                            **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc. and Relisource Technology Ltd.                            **
 **                                                                           **
 **  Copryright(c) 2015. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Relisource Technology Ltd.                        **
 **  Creation date       :  02/22/2015                                        **
 **  Programmer          :  Faijur Rahman		                              **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : services.h                                                        **
 **                                                                           **
 **  Description :                                                            **
 **    - This file includes all API header files for the service module.      **
 **                                                                           **
 **  History :                                                                ** 
 **    ver 1.00.001  -  02/22/2015                                            **
 **                                                                           **
 ******************************************************************************/ 

#ifndef SERVICES_H
#define SERVICES_H

#include "console.h"

#endif // SERVICES_H
