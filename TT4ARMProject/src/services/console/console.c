/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm                                            **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc. and Relisource Technology Ltd.                            **
 **                                                                           **
 **  Copryright(c) 2015. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Relisource Technology Ltd.                        **
 **  Creation date       :  02/22/2015                                        **
 **  Programmer          :  Faijur Rahman		                              **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : console.c                                                         **
 **                                                                           **
 **  Description :                                                            **
 **    - This file includes functions related to console.					  **
 **                                                                           **
 **  History :                                                                ** 
 **    ver 1.00.001  -  02/22/2015                                            **
 **                                                                           **
 ******************************************************************************/ 

#include "uart_serial.h"
#include "stdio_serial.h"
#include "console.h"


void configure_console(void)
{
	const usart_serial_options_t uart_serial_options = {
		.baudrate = CONF_UART_BAUDRATE,
#ifdef CONF_UART_CHAR_LENGTH
		.charlength = CONF_UART_CHAR_LENGTH,
#endif
		.paritytype = CONF_UART_PARITY,
#ifdef CONF_UART_STOP_BITS
		.stopbits = CONF_UART_STOP_BITS,
#endif
	};

	/* Configure console UART. */
	sysclk_enable_peripheral_clock(CONSOLE_UART_ID);
	stdio_serial_init(CONSOLE_UART_BASE, &uart_serial_options);
}
