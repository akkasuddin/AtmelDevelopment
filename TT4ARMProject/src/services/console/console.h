/*******************************************************************************
 **                                                                           **
 **                      S e n s i t e c h   I n c.                           **
 **            800 Cummings Center, Beverly Massachusetts, USA.               **
 ** __________________________________________________________________________**
 **                                                                           **
 **  SW Project: TT4USB-MultiAlarm                                            **
 **                                                                           **
 **  This software code is a proprietary confidential information to          **
 **  Sensitech Inc. and Relisource Technology Ltd.                            **
 **                                                                           **
 **  Copryright(c) 2015. All rights reserved to Sensitech Inc.                **
 ** __________________________________________________________________________**
 **                                                                           **
 **  Software Developer  :  Relisource Technology Ltd.                        **
 **  Creation date       :  02/22/2015                                        **
 **  Programmer          :  Faijur Rahman		                              **
 ** __________________________________________________________________________**
 **                                                                           **
 **  File : console.h                                                         **
 **                                                                           **
 **  Description :                                                            **
 **    - This file includes definitions and header files related to console.  **
 **                                                                           **
 **  History :                                                                ** 
 **    ver 1.00.001  -  02/22/2015                                            **
 **                                                                           **
 ******************************************************************************/ 

#ifndef CONSOLE_SUPPORT_H_
#define CONSOLE_SUPPORT_H_
#include "TT4USB_Board.h"

extern void configure_console(void);

#define CONSOLE_UART_BASE	CONSOLE_USART_BASE
#define CONSOLE_UART_ID		CONSOLE_USART_ID


#endif /* CONSOLE_SUPPORT_H_ */